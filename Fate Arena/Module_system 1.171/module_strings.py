﻿# #######################################################################
#	("string-id", "string"), That simple.
# #######################################################################


# #######################################################################
#		TODO:
#	I doubt these are hardcoded. Test and see if you can't replace them with quickstrings.
#	If so, change every string in the game to a quick string.
# #######################################################################

strings = [

# #######################################################################
#	These four strings are the only explicitly required strings
# #######################################################################

  ("no_string", "NO STRING!"),
  ("empty_string", " "),
  ("yes", "Yes."),
  ("no", "No."),
  
# #######################################################################
#	But, these are referenced, so, they should be left here.
# #######################################################################

  ("blank_string", " "),
  ("ERROR_string", "{!}ERROR!!!ERROR!!!!ERROR!!!ERROR!!!ERROR!!!ERROR!!!!ERROR!!!ERROR!!!!ERROR!!!ERROR!!!!ERROR!!!ERROR!!!!ERROR!!!ERROR!!!!ERROR!!!ERROR!!!!!"),
  ("noone", "no one"),
  ("s0", "{!}{s0}"),
  ("blank_s1", "{!} {s1}"),
  ("reg1", "{!}{reg1}"),
  ("s50_comma_s51", "{!}{s50}, {s51}"),
  ("s50_and_s51", "{s50} and {s51}"),
  ("s52_comma_s51", "{!}{s52}, {s51}"),
  ("s52_and_s51", "{s52} and {s51}"),
  ("s5_s_party", "{s5}'s Party"),

  ("msg_battle_won","Battle won! Press tab key to leave..."),

  ("randomize", "Randomize"),
  ("hold_fire", "Hold Fire"),
  ("blunt_hold_fire", "Blunt / Hold Fire"),

  ("finished", "(Finished)"),

  ("delivered_damage", "Delivered {reg60} damage."),

  ("cant_use_inventory_now", "Can't access inventory now."),
  ("cant_use_inventory_arena", "Can't access inventory in the arena."),
  ("cant_use_inventory_disguised", "Can't access inventory while you're disguised."),
  ("cant_use_inventory_tutorial", "Can't access inventory in the training camp."),
  
  ("cannot_leave_now", "You cannot leave now."),

# #######################################################################
# 	Feel free to add your strings here!
# #######################################################################

  #####################################################################
  #### FATE/ RIDER SABER STRINGS
  #####################################################################
  #Class Declarations
  ("class_generic", "Class"),
  ("class_saber", "Saber"),
  ("class_lancer", "Lancer"),
  ("class_archer", "Archer"),
  ("class_rider", "Rider"),
  ("class_caster", "Caster"),
  ("class_assassin", "Assassin"),
  ("class_berserker", "Berserker"),
  ("class_avenger", "Avenger"),
  ("class_ruler", "Ruler"),
  ("class_shielder", "Shielder"),
  ("class_foreigner", "Foreigner"),
  ("class_beast", "Beast"),
  
  ("class_unknown", "Unknown Class"), #Servant Class is obfuscated when you first meet them in Moon Cell Wars
  
  #Fate Parameters
  ("param_strength", "Strength"),
  ("param_constitution", "Constitution"),
  ("param_agility", "Agility"),
  ("param_magic", "Magic"),
  ("param_luck", "Luck"),
  ("param_noble_phantasm", "Noble Phantasm"),
  
  #Fate Parameters Descriptions
  ("param_strength_desc", "Strength Description"),
  ("param_constitution_desc", "Constitution Description"),
  ("param_agility_desc", "Agility Description"),
  ("param_magic_desc", "Magic Description"),
  ("param_luck_desc", "Luck Description"),
  ("param_noble_phantasm_desc", "Noble Phantasm Description"),
  
  #Alignments
  ("alignment_lawful_good", "Lawful Good"),
  ("alignment_lawful_neutral", "Lawful Neutral"),
  ("alignment_lawful_evil", "Lawful Evil"),
  ("alignment_neutral_good", "Neutral Good"),
  ("alignment_neutral", "True Neutral"),
  ("alignment_neutral_evil", "Neutral Evil"),
  ("alignment_chaotic_good", "Chaotic Good"),
  ("alignment_chaotic_neutral", "Chaotic Neutral"),
  ("alignment_chaotic_evil", "Chaotic Evil"),
  ("alignment_insane", "Insane"),
  
  #Classifications
  ("classification_master", "Master"), #0
  ("classification_servant", "Servant"), #1
  ("classification_summon", "Summon"), #2
  ("classification_magus", "Magus"), #3
  ("classification_golem", "Golem"), #4
  ("classification_nonmagus", "Non-Magus"),
  ("classification_nonmagus_master", "Non-Magus Master"),
  ("classification_puppet", "Puppet"),
  # ("classification_undead", "Undead"),
  # ("classification_ancestor", "True Ancestor"),
  
  #Master Names for Servant Profiles
  ("master_generic", "Master Name"),
  ("master_unknown", "Unknown Master"),
  
  #Personalities ##These apply to both player and servants, used in summoning compatibility
  ("personality_generic", "Personality not Set"), #No Set Personality Type
  ("personality_selfless", "Selfless"), #Selfless, Shirou
  ("personality_selfish", "Selfish"), #Selfish, Gilgamesh
  ("personality_honorable", "Honorable"), #Honorable, Artoria
  ("personality_pragmatic", "Pragmatic"), #Pragmatic, Kiritugu
  ("personality_realist", "Realist"), #Realist
  ("personality_sadist", "Sadist"), #Sadist, Kairi
  ("personality_zealot", "Zealot"), #Zealot, Assassins
  #Master Parent's Background ##From all of these we determine if the player is building a canon master
  ("parent_bg_generic", "Parental Background not Set"), #No Set Parental Background
  ("parent_bg_regular", "Regular People"),#Regular People, 
  ("parent_bg_magus", "Magus Family"),#Magister 
  ("parent_bg_assassin", "Mysterious"), #Assassin, Emiya-household
  ("parent_bg_church", "Leadership in the Church"), #Church, 
  ("parent_bg_travellers", "Travellers"), #Travellers, immense wealth
  #Master Background
  ("player_bg_generic", "Background not Set"), #No Set Background
  ("player_bg_magus", "Magister"), #Magister
  ("player_bg_church_combat", "Executor for the Church"), #Executor
  ("player_bg_church_leader", "Leader in the Church"), #Church Leadership
  ("player_bg_assassin", "Assassin"), #Assassin
  ("player_bg_educator", "Teacher"), 
  #Master Adult Education
  ("player_edu_generic", "Education not Set"), #No Set Education
  ("player_edu_high", "High School"), #High School
  ("player_edu_magus", "The Clock Tower"), #Clock Tower
  ("player_edu_church_magic", "Church-led Magus College"), #Church
  ("player_edu_church_combat", "Church-led Combat School"),  #Church
  ("player_edu_home", "Homeschooled"), 
  ("player_edu_soldier", "Combat Training"), 
  #Master Training
  ("player_train_generic", "Training not Set"), #No Set Training
  ("player_train_archery", "Archery Club"), #Archery
  ("player_train_sword", "Kendo Club"), #Sword Skills
  ("player_train_magic", "Magic"), #Magic Skills
  ("player_train_firearms", "Shooting"), #Guns Skills
  ("player_train_spy", "Subterfuge"), #Spycraft Skills
  ("player_train_leader", "School Politics"), #Leadership Skills
  ("player_train_religious", "Religion"), #Church Skills
  #Are you from a Mage Family
  ("family_history_generic", "Family History not Set"), #No Family Magus History Set
  ("family_history_ancient", "My family has been producing Mages since the Time of Gods."), #Lizzet, Matou
  ("family_history_more", "We're a proud Magus family with many generations of Mages under our lineage."), #Tousaka, Kiritugu Emiya, 
  ("family_history_some", "My Family only recently began producing mages."), #Waver
  ("family_history_none", "No, I have never even heard the term Magus."), #Ryuunoske, Shirou
  
  #Magic Types
  ("magic_generic", "Generic Magic"), #No Set Magic
  ("magic_fire", "Fire Magic"),
  ("magic_wind", "Wind Magic"),
  ("magic_earth", "Earth Magic"),
  ("magic_light", "Light Magic"),
  ("magic_time", "Time-Dilation Magic"),
  ("magic_jewel", "Jewelcraft Magic"),
  
  #Holy Grail War Type
  ("war_type", "Calradian Holy Grail War"), #How the game is right now, but with limited servants
  ("war_type_fuyuki", "Fuyuki Holy Grail War"), #F/ SN, F/ Zero
  ("war_type_tokyo", "Tokyo Holy Grail War"), #F/ Prototype
  ("war_type_great", "Great Holy Grail War"), #F/ Apocrypha
  ("war_type_false", "False Holy Grail War"), #F/ strange Fake -Maybe call it pseudo?
  ("war_type_moon", "Moon Cell Holy Grail War"), #F/ Ex-
  ("war_type_grand", "Grand Order"), #F/GO
  
  ("servant_unknown", "Unknown Servant"), #Servants aren't known until met. For Pages
  #Saber True Names
  ("servant_saber_generic", "Saber"),
  ("servant_saber_01","Artoria Pendragon"), #Avalon
  ("servant_saber_02","Nero Claudius"), #Fiddle* Or Whatever that is
  ("servant_saber_03","Siegfried"), #Bloodied Fig Leaf
  ("servant_saber_04","Fergus mac Róich"), #shard of Lia Fáil, Stone of Destiny
  ("servant_saber_05","Richard Lionheart"), #Red Avalon
  #Lancer True Names
  ("servant_lancer_generic", "Lancer"),
  ("servant_lancer_01","Cú Chulainn"), #Runed Jewel from this losers earring
  ("servant_lancer_02","Diarmuid Ua Duibhne"), #Cup of Healing for Loathy Lady Legend
  ("servant_lancer_03","Fionn mac Cumhaill"), #Dord Fiann, a boar shaped horn
  ("servant_lancer_04","Hector"), #
  ("servant_lancer_05","Brynhildr"), #Carbonized Debris from Building
  #Archer True Names
  ("servant_archer_generic", "Archer"),
  ("servant_archer_01","EMIYA"), #Rin's Pendant
  ("servant_archer_02","Gilgamesh"), #key to gate of babylon, or the first snake skin
  ("servant_archer_03","Atalanta"), #Argo's Mast
  ("servant_archer_04","Robin Hood"), #Yew Stump
  ("servant_archer_05","Arash"), #Arrowhead embedded in fossilized walnut
  #Rider True Names
  ("servant_rider_generic", "Rider"),
  ("servant_rider_01","Medusa"), #Mirrored bronze shield, Maybe a pertified hand
  ("servant_rider_02","Iskandar"), #Cloth from Mantle
  ("servant_rider_03","Achilles"), #Jar of Ambrosia Oil
  ("servant_rider_04","Astolfo"), #Glass Bottle with Senses
  ("servant_rider_05","Saint George"), # Dragon Scales
  #Caster True Names
  ("servant_caster_generic", "Caster"),
  ("servant_caster_01","Medea"), # Literature with ties to Medea, Golden Fleece
  ("servant_caster_02","Gilles de Rais"), # Le Mistère du Siège d'Orléans 
  ("servant_caster_03","William Shakespeare"), # broken quill
  ("servant_caster_04","Avicebron"), #Golem Bits
  ("servant_caster_05","Hohenheim"), #Hohenheim's Flask
  #Assassin True Names
  ("servant_assassin_generic", "Assassin"),
  ("servant_assassin_01","Hassan of the Cursed Arm"), # Arm Bandage
  ("servant_assassin_02","Hassan of the Hundred Faces"), # Cracked Mirror
  ("servant_assassin_03","Hassan of the Shadow Peeling"), # Scrawled Note
  ("servant_assassin_04","Hassan of Serenity"), # bottle of chemicals
  ("servant_assassin_05","King Hassan-i-Sabbah"), # Skull Helmet, apparently this fucker is GODTIER
  #Berserker True Names
  ("servant_berserker_generic", "Berserker"),
  ("servant_berserker_01","Heracles"), #the axe-sword, slab of stone from temple
  ("servant_berserker_02","Lancelot"), #petrified elm branch
  ("servant_berserker_03","Spartacus"), # Spatacus' Manacles
  ("servant_berserker_04","Frankenstein"), #Frankenstein's Blueprints
  ("servant_berserker_05","Asterios"), #fragment of aridne's thread
  
  ("servant_avenger_generic", "Avenger"),
  ("servant_avenger_01", "Angra Mainyu"), #Avesta, a holy book
  
  ("servant_ruler_generic", "Ruler"),
  ("servant_ruler_01", "Jeanne d'Arc",), #Ashes
  ("servant_ruler_02", "Amakusa Shirou Tokisada"), #
  
  ("servant_shielder_generic", "Shielder"),
  ("servant_shielder_01", "Mashu Kyrielight"), #Round Table 
  
  ("servant_extra_EMIYA", "Nameless"), # A special case for Moon Cell Wars
# ####################################################################
# ### Summon Conversation ############################################
# ####################################################################
  #Greetings Based on Personality
	  
  ("lawful_good_greet", "Master {playername}. It's good to see you in good health. I hope this Grail War is going as planned."),
  ("lawful_neutral_greet", "Aye, {playername}. Let's continue with our quest to win you the Grail!"),
  ("lawful_evil_greet", "{playername}. Let's not spend too much time chatting. There is a War to win."),
	  
  ("neutral_good_greet", "Master {playername}. We seem to be a nice fit, don't you agree?"),
  ("true_neutral_greet", "Well, hello, {playername}. I am happy with the way this bond is working for the both of us."),
  ("neutral_evil_greet", "Yes? {playername}? You keep asking me what I can do, maybe we should turn the tables?"),
	  
  ("chaotic_good_greet", "Well, well, well. Master {playername}. Why aren't we out there winning this War?"),
  ("chaotic_neutral_greet", "Oh. It's {playername}. I suppose I can take a break from fighting to share a few words with you."),
  ("chaotic_evil_greet", "{playername}. If it weren't for that Command Seal, I would be more weary of speaking to you."),
	  
  ("insane_greet", "Grraaahhhh. AAAAaaa!!!! {playername}!!!!"),
  
  #intial Greetings
  #("lawful_good_intial_greet", "Master, my true name is"),
  ("lawful_good_initial_greet", "{playername}. Are you my Master?"),
  ("lawful_neutral_initial_greet", "I've heard your plea to the Throne and answered. Might we get started?"),
  ("lawful_evil_initial_greet", "This is an initial greeting for a lawful evil summon."),
	  
  ("neutral_good_initial_greet", "Master {playername}. This is an initial greeting for a neutral good summon."),
  ("true_neutral_initial_greet", "This is an initial greeting for a true neutral summon."),
  ("neutral_evil_initial_greet", "This is an initial greeting for a neutral evil summon."),
	  
  ("chaotic_good_initial_greet", "This is an initial greeting for a chaotic good summon."),
  ("chaotic_neutral_initial_greet", "{playername}, This is an initial greeting for a chaotic neutral summon."),
  ("chaotic_evil_initial_greet", "This is an initial greeting for a chaotic evil summon."),
	  
  ("insane_initial_greet", "Uhhhggg. GGrrrAAAAaaa!!!! Myy N. n. . . name "),
  
  #Reveal Greetings
  #("lawful_good_reveal_greet", "Master, my true name is"),
  ("lawful_good_reveal_greet", "{playername}. I am your Servant for this infernal War. My True Name is"),
  ("lawful_neutral_reveal_greet", "My form, and my weapon was chosen by the Holy Grail to assist you. I am"),
  ("lawful_evil_reveal_greet", "You can't recognize me? I suppose fame isn't forever. You may know me as"),
	  
  ("neutral_good_reveal_greet", "Master {playername}. I enter this pact with you as"),
  ("true_neutral_reveal_greet", "I hope we can both get what we want from this War we find ourselves. Oh? Me? I'm"),
  ("neutral_evil_reveal_greet", "I suppose if it's required for us to win this war, you may call me"),
	  
  ("chaotic_good_reveal_greet", "Ah, aha, ahahaha. Surely you recognize the incredible form of"),
  ("chaotic_neutral_reveal_greet", "{playername}, you must be ready to kill for this War. Die for this War. Such is my duty as your Servant. As"),
  ("chaotic_evil_reveal_greet", "If it means we can get to the killing sooner than later, you may call me"),
	  
  ("insane_reveal_greet", "Uhhhggg. GGrrrAAAAaaa!!!! Myy N. n. . . name "),
  #Saber Summon Speech
  ("saber_generic_speech","I am a test servant and should not appear in regular play."),
  ("saber_01_speech","Servants exist to serve their Masters, but I hope we can work together in good faith."),
  ("saber_02_speech",", I have arrived in response to your call! Umu, you have chosen the right one! You sure know how to tell the difference!"),
  ("saber_03_speech","I have come in response to your summons."),
  ("saber_04_speech","I have been summoned by your request."),
  ("saber_05_speech","Hey! I do you know what's coming next?"), 
  #Lancer Summon Speech
  ("lancer_generic_speech","I am a test servant and should not appear in regular play."),
  ("lancer_01_speech","I have been summoned and come at your request. Let's have fun with this, Master!"),
  ("lancer_02_speech","I am the first spear of the Knights of Fianna. I have arrived. I am now the Servant who serves you."),
  ("lancer_03_speech","Erin's Savior. The captain of the glorious Knights of Fianna. The man who rose to victory over Nuadha."),
  ("lancer_04_speech","Hector's Speech"), 
  ("lancer_05_speech","My class is...Lancer. Please do not be kind to me...master."),
  #Archer Summon Speech
  ("archer_generic_speech","I am a test servant and should not appear in regular play."),
  ("archer_01_speech","A new master. No, a new Mage. I know how you feel. You must be frustrated with how powerless you are, but right now let's keep looking forward."), 
  ("archer_02_speech","Fufuhahahahaha! Being able to summon me means that you've used up all of your luck, mongrel."), 
  ("archer_03_speech","Are thou my master? Best Regards."), 
  ("archer_04_speech","Robin Hood"), 
  ("archer_05_speech","Persia's great hero is here! . . . But well, it's not such a magnificent thing."), 
  #Rider Summon Speech
  ("rider_generic_speech","I am a test servant and should not appear in regular play."),
  ("rider_01_speech","I would say, 'eyes up here' but that just might turn you to stone. I guess this will be fine for the time being."), 
  ("rider_02_speech","Ahh, another chance to see Ōkeanós. To walk again, to conquer again, TO LIVE!"), 
  ("rider_03_speech","Divinity means nothing in this infernal contest."), 
  ("rider_04_speech","Hey! Are you my Master?! I'm Astolfo! Wait, am I not supposed to tell people. . .?"), 
  ("rider_05_speech","First dragons, now this. . . yare yare"), 
  #Caster Summon Speech
  ("caster_generic_speech","I am a test servant and should not appear in regular play."),
  ("caster_01_speech","I am a Mage, I am well accustomed to rules. Whether your skills are worthy of mine or not, yet."), 
  ("caster_02_speech","Simply marvelous! I have come to you at your invitation. From here on, I shall be at your command."), 
  ("caster_03_speech","I have come to see you! So, shall we begin our story?"), 
  ("caster_04_speech","Avicebron"),
  ("caster_05_speech","Let me show you the kind of magecraft the Average One is capable of."),
  #Assassin Summon Speech
  ("assassin_generic_speech","I am a test servant and should not appear in regular play."),
  ("assassin_01_speech","We are but one part of a ruthless clan of assassins. If you have any dirty work that needs to be done, don't hesitate to ask."), 
  ("assassin_02_speech","I regret nothing! It is time for us to meet our deaths, Lord Mage!"), 
  ("assassin_03_speech","For as long as our shadow accompanies you, victory is certain. Rest assured, Master."), 
  ("assassin_04_speech","Everything, everything, everything as you would will it. I will offer all of myself to you. This body, this heart, all of it..."), 
  ("assassin_05_speech","Do not be afraid, O contractor. The Old Man of the Mountain hath come revealed, in answer to thy summons. I have no name. A name that is easy on the tongue would be good."),
  #Berserker Summon Speech
  ("berserker_generic_speech", "I am a test servant and should not appear in regular play."),
  ("berserker_01_speech",". . . HYYYYYRRRRRRRR"), 
  ("berserker_02_speech","I am the alienated, the ridiculed, the despised. No need to praise my name. No need to envy my body. I am the shadow under the radiance of heroic spirits. Birthed of the darkness of glorious legend. And so, I hate, I resent. Nourished by the sighs of the people precipitated within the darkness, people that curse the light. This is my disgrace. Because of her unsullied glory, I must forever be belittled. You are, the sacrifice. Good, give me more, your blood and flesh, your life. Let them ignite my hatred—!!"), 
  ("berserker_03_speech","What do I like? Rebellion! That is everything to me."), 
  ("berserker_04_speech",". . . Uuuu . .  AAAAaaa. . ."), 
  ("berserker_05_speech","K,i,ll!"), 
  #Avenger Summon Speech
  ("avenger_generic_speech", "I am a test servant and should not appear in regular play."),
  ("avenger_01_speech", "I’ve said this countless times but, the weakest Servant in the entire Servant world, that’s me. Don’t even think about letting me go into battle."), 
  #Ruler Summon Speech
  ("ruler_generic_speech", "I am a test servant and should not appear in regular play."),
  ("ruler_01_speech", "I'm really glad to meet you!",),
  ("ruler_02_speech", "It seems… even an inferior man like me can still walk a little further."),
  #Shielder Summon Speech
  ("shielder_generic_speech", "I am a test servant and should not appear in regular play."),
  ("shielder_01_speech", "The outside world is really something. There's so much I never learned in Chaldea. New discoveries every day. Is it the same for you, Senpai?"), 
  
  
  # Flavor Dialogue
  # Sabers
	# Artoria
  ("saber_01_flavor_01", "I do not battle for my own glory, but as the humble King of Knights, I will battle for yours!"),
  ("saber_01_flavor_02", "."),
  ("saber_01_flavor_03", "."),
  ("saber_01_flavor_04", "."),
  ("saber_01_flavor_05", "."),
	# Nero
  ("saber_02_flavor_01", "There is so much beauty in this age! UMU! I'm so glad to have experienced this."),
  ("saber_02_flavor_02", "."),
  ("saber_02_flavor_03", "."),
  ("saber_02_flavor_04", "."),
  ("saber_02_flavor_05", "."),
	# Siegfried
  ("saber_03_flavor_01", "It's probably best we never say my name in public. You never know who may be watching, and my weakness might get out."),
  ("saber_03_flavor_02", "The Rheingold is as much of a curse as it is a boon, should I share it with you freely, you would find yourself a slave to it as much as you would find the wealth freeing."),
  ("saber_03_flavor_03", "Fafnir's death would eventually lead to my own, our Fates eternally intertwined."),
  ("saber_03_flavor_04", "."),
  ("saber_03_flavor_05", "."),
	# Fergus
  ("saber_04_flavor_01", "Ahahaha! My dear {laddie/lassie}, isn't this a beautiful day for the Rainbow Swordsman and his Master to take in breath."),
  ("saber_04_flavor_02", "."),
  ("saber_04_flavor_03", "."),
  ("saber_04_flavor_04", "."),
  ("saber_04_flavor_05", "."),
	# King Richard
  ("saber_05_flavor_01", "."),
  ("saber_05_flavor_02", "."),
  ("saber_05_flavor_03", "."),
  ("saber_05_flavor_04", "."),
  ("saber_05_flavor_05", "."),
  # Lancers
	# Cu Chulainn
  ("lancer_01_flavor_01", "Buahahaha! Now, let me show you what the Hound of Chulainn is capable of."),
  ("lancer_01_flavor_02", "Do you think we can stop by the shop and grab some salmon?"),
  ("lancer_01_flavor_03", "My teacher? Now there was a fierce duelist. Her use of this Thorned Spear makes mine look like an amateur."),
  ("lancer_01_flavor_04", "I doubt you'll be happy to hear this, but I've picked up yet another part-time job. Why? Well, I suppose every dog has gotta run."),
  ("lancer_01_flavor_05", "My Runecraft? Well, a knight should take every advantage they can. Just don't make me make believe to be a Caster instead of a Lancer."),
	# Diarmuid
  ("lancer_02_flavor_01", "My curse? Ultimately, what would have been considered a blessing for some, didn't compliment my honorable lifestyle, I suppose."),
  ("lancer_02_flavor_02", "As a knight, I offer my twin lances to you."),
  ("lancer_02_flavor_03", "Swords? I mean, I have been known to use them, but my class container doesn't support that this time around."),
  ("lancer_02_flavor_04", "."),
  ("lancer_02_flavor_05", "."),
	# Fionn
  ("lancer_03_flavor_01", "."),
  ("lancer_03_flavor_02", "."),
  ("lancer_03_flavor_03", "."),
  ("lancer_03_flavor_04", "."),
  ("lancer_03_flavor_05", "."),
	# Hector
  ("lancer_04_flavor_01", "."),
  ("lancer_04_flavor_02", "."),
  ("lancer_04_flavor_03", "."),
  ("lancer_04_flavor_04", "."),
  ("lancer_04_flavor_05", "."),
	# Brynhildr
  ("lancer_05_flavor_01", "."),
  ("lancer_05_flavor_02", "."),
  ("lancer_05_flavor_03", "."),
  ("lancer_05_flavor_04", "."),
  ("lancer_05_flavor_05", "."),
  # Archers
	# EMIYA 
  ("archer_01_flavor_01", "Do you have a kitchen, Master?"),
  ("archer_01_flavor_02", "Heroic Spirit isn't quite the right term for me. Just keep calling me Servant."),
  ("archer_01_flavor_03", "I have lived so many lifetimes, so you'll have to excuse my forgetfullness."),
  ("archer_01_flavor_04", "I am a Hero of Alaya, Itself. Not of the Throne. The strength of my Legend is that I do not have one."),
  ("archer_01_flavor_05", "My aria? Oh, does it strike you as odd that it's not in my native tongue?"),
	# Gilgamesh
  ("archer_02_flavor_01", "All in this world is already mine."),
  ("archer_02_flavor_02", "This entire world is my garden until the very end of time. It would be wise to watch where you step."),
  ("archer_02_flavor_03", "We should always strive to protect the beauty in this world."),
  ("archer_02_flavor_04", "I need not retainers. I have only had one companion, and that is all I will ever hold."),
  ("archer_02_flavor_05", "The King will acknowledge it; the King will permit it. The King will bear the burden of the entire world."),
	# Atalanta
  ("archer_03_flavor_01", "."),
  ("archer_03_flavor_02", "."),
  ("archer_03_flavor_03", "."),
  ("archer_03_flavor_04", "."),
  ("archer_03_flavor_05", "."),
	# Robin Hood
  ("archer_04_flavor_01", "."),
  ("archer_04_flavor_02", "."),
  ("archer_04_flavor_03", "."),
  ("archer_04_flavor_04", "."),
  ("archer_04_flavor_05", "."),
	# Arash
  ("archer_05_flavor_01", "You've read that I lived and went on to become a decorated general, huh? As lovely as that would have been, it's not true. My sacrifice was necessary for my Country."),
  ("archer_05_flavor_02", "Where does the material for the bows and arrows I create come from? Just as my legend, I am the Archer of Sacrifice, my body provides what we need for victory."),
  ("archer_05_flavor_03", "."),
  ("archer_05_flavor_04", "."),
  ("archer_05_flavor_05", "."),
  # Riders
	# Medusa 
  ("rider_01_flavor_01", "My height is a point of much pain for me, master. Can we avoid drawing attention about it?"),
  ("rider_01_flavor_02", "My sisters and I were once in a state of perpetual youth, but I was removed from that cycle."),
  ("rider_01_flavor_03", "Mirrors? Oh how I despise them. And NOT for the reason you're thinking, I'm certain."),
  ("rider_01_flavor_04", "."),
  ("rider_01_flavor_05", "."),
	# Iskandar
  ("rider_02_flavor_01", "Behold my peerless army!"),
  ("rider_02_flavor_02", "I carry the Dreams of all my Companions and in turn they give their all to achieve mine."),
  ("rider_02_flavor_03", "I live for Conquest! May we find a challenge."),
  ("rider_02_flavor_04", "."),
  ("rider_02_flavor_05", "."),
	# Achilles
  ("rider_03_flavor_01", "."),
  ("rider_03_flavor_02", "."),
  ("rider_03_flavor_03", "."),
  ("rider_03_flavor_04", "."),
  ("rider_03_flavor_05", "."),
	# Astolfo
  ("rider_04_flavor_01", "."),
  ("rider_04_flavor_02", "."),
  ("rider_04_flavor_03", "."),
  ("rider_04_flavor_04", "."),
  ("rider_04_flavor_05", "."),
	# Saint George
  ("rider_05_flavor_01", "."),
  ("rider_05_flavor_02", "."),
  ("rider_05_flavor_03", "."),
  ("rider_05_flavor_04", "."),
  ("rider_05_flavor_05", "."),
  # Casters
	# Medea
  ("caster_01_flavor_01", "."),
  ("caster_01_flavor_02", "."),
  ("caster_01_flavor_03", "."),
  ("caster_01_flavor_04", "."),
  ("caster_01_flavor_05", "."),
	# Gilles de Rais
  ("caster_02_flavor_01", "Jeanne, my dear Jeanne, how cruel this World."),
  ("caster_02_flavor_02", "God allowed my dear Jeanne to die, He allowed my beautiful crimes to occur."),
  ("caster_02_flavor_03", "."),
  ("caster_02_flavor_04", "."),
  ("caster_02_flavor_05", "."),
	# William Shakespeare
  ("caster_03_flavor_01", "Doth thou find my manner of speech difficult?"),
  ("caster_03_flavor_02", "."),
  ("caster_03_flavor_03", "."),
  ("caster_03_flavor_04", "."),
  ("caster_03_flavor_05", "."),
	# Avicebron
  ("caster_04_flavor_01", "."),
  ("caster_04_flavor_02", "."),
  ("caster_04_flavor_03", "."),
  ("caster_04_flavor_04", "."),
  ("caster_04_flavor_05", "."),
	# Hohenheim
  ("caster_05_flavor_01", "."),
  ("caster_05_flavor_02", "."),
  ("caster_05_flavor_03", "."),
  ("caster_05_flavor_04", "."),
  ("caster_05_flavor_05", "."),
  # Assassins
	# Cursed Arm 
  ("assassin_01_flavor_01", "We Hassans all had to give up something to become the Old Man of the Mountain."),
  ("assassin_01_flavor_02", "My daggers always find their mark, and if that doesn't stop them, this Arm of mine will."),
  ("assassin_01_flavor_03", "My True Name? I gave that up for true power long ago."),
  ("assassin_01_flavor_04", "This mask? It does more than conceal."),
  ("assassin_01_flavor_05", "I eventually heard the Bell toll. Just as all Hassans eventually do."),
	# Hundred Faces
  ("assassin_02_flavor_01", "Individually we were weak. Together, we are unbreakable."),
  ("assassin_02_flavor_02", "Death to one will not kill the collective."),
  ("assassin_02_flavor_03", "We can collect any information you might need, Master."),
  ("assassin_02_flavor_04", "Where are the other 99 you ask? You're looking at them, {sir/madam}."),
  ("assassin_02_flavor_05", "."),
	# Shadow-Peeling
  ("assassin_03_flavor_01", "."),
  ("assassin_03_flavor_02", "."),
  ("assassin_03_flavor_03", "."),
  ("assassin_03_flavor_04", "."),
  ("assassin_03_flavor_05", "."),
	# Serenity
  ("assassin_04_flavor_01", "Poison is my flesh."),
  ("assassin_04_flavor_02", "."),
  ("assassin_04_flavor_03", "."),
  ("assassin_04_flavor_04", "."),
  ("assassin_04_flavor_05", "."),
	# King Hassan-i-Sabbah
  ("assassin_05_flavor_01", "Doth thou hear the Evening Bell?"),
  ("assassin_05_flavor_02", "The Lord Himself gave me my mission. The Grail, while not true, is His."),
  ("assassin_05_flavor_03", "I killed the Nineteen, yes. It is the way all things must be."),
  ("assassin_05_flavor_04", "I am The First of the Assassins, The Last of the Assassins. As dictated by Our Lord."),
  ("assassin_05_flavor_05", "Claiming this Grail isn't about Power, but about protecting the Faith."),
  # Berserkers
	# Heracles
  ("berserker_01_flavor_01", "...:::::::|||"),
  ("berserker_01_flavor_02", ".!!"),
  ("berserker_01_flavor_03", ".!"),
  ("berserker_01_flavor_04", "."),
  ("berserker_01_flavor_05", "."),
	# Lancelot
  ("berserker_02_flavor_01", "Arrrrthhur."),
  ("berserker_02_flavor_02", "AAAaaarr"),
  ("berserker_02_flavor_03", "ARRRRRRthURRR"),
  ("berserker_02_flavor_04", "Arrrrond-igh-t"),
  ("berserker_02_flavor_05", "guuuinevere"),
	# Spartacus
  ("berserker_03_flavor_01", "DEATH TO OPPRESSORS"),
  ("berserker_03_flavor_02", "FREEDOM OR DEATH, THERE ARE NO OTHER OPTIONS"),
  ("berserker_03_flavor_03", "PAIN IS MY STRENGTH"),
  ("berserker_03_flavor_04", "THESE BINDS SERVE TO MOTIVATE MY CAUSE"),
  ("berserker_03_flavor_05", "I. AM. SPARTACUS. (why does that sound familiar)"),
	# Frankenstein
  ("berserker_04_flavor_01", "uuu."),
  ("berserker_04_flavor_02", "ggrrruuh."),
  ("berserker_04_flavor_03", ". . . not monster."),
  ("berserker_04_flavor_04", "alone"),
  ("berserker_04_flavor_05", "huuuuhhhh"),
	# Asterios
  ("berserker_05_flavor_01", "grrAAAAHHHH"),
  ("berserker_05_flavor_02", "-groan-"),
  ("berserker_05_flavor_03", "-HUFF-"),
  ("berserker_05_flavor_04", "-SNORT-"),
  ("berserker_05_flavor_05", "*draws mazelike design in nearby dirt*"),
  # Avengers
  ("avenger_01_flavor_01", "."),
  ("avenger_01_flavor_02", "-.-"),
  ("avenger_01_flavor_03", "-.-"),
  ("avenger_01_flavor_04", "-.-"),
  ("avenger_01_flavor_05", "*.*"),
  # Shielders
  ("shielder_01_flavor_01", "."),
  ("shielder_01_flavor_02", "-.-"),
  ("shielder_01_flavor_03", "-.-"),
  ("shielder_01_flavor_04", "-.-"),
  ("shielder_01_flavor_05", "*.*"),
  # Rulers
   # Jeanne d'Arc
  ("ruler_01_flavor_01", "grrAAAAHHHH"),
  ("ruler_01_flavor_02", "-groan-"),
  ("ruler_01_flavor_03", "-HUFF-"),
  ("ruler_01_flavor_04", "-SNORT-"),
  ("ruler_01_flavor_05", "*draws mazelike design in nearby dirt*"),
   # Shirou Kotomine
  ("ruler_02_flavor_01", "grrAAAAHHHH"),
  ("ruler_02_flavor_02", "-groan-"),
  ("ruler_02_flavor_03", "-HUFF-"),
  ("ruler_02_flavor_04", "-SNORT-"),
  ("ruler_02_flavor_05", "*draws mazelike design in nearby dirt*"),
  
  # Lawful Good
  ("lawful_good_flavor_01", "."),
  ("lawful_good_flavor_02", "."),
  ("lawful_good_flavor_03", "."),
  ("lawful_good_flavor_04", "."),
  ("lawful_good_flavor_05", "."),
  ("lawful_good_flavor_06", "."),
  ("lawful_good_flavor_07", "."),
  ("lawful_good_flavor_08", "."),
  ("lawful_good_flavor_09", "."),
  ("lawful_good_flavor_10", "."),
  # Neutral Good
  ("neutral_good_flavor_01", "."),
  ("neutral_good_flavor_02", "."),
  ("neutral_good_flavor_03", "."),
  ("neutral_good_flavor_04", "."),
  ("neutral_good_flavor_05", "."),
  ("neutral_good_flavor_06", "."),
  ("neutral_good_flavor_07", "."),
  ("neutral_good_flavor_08", "."),
  ("neutral_good_flavor_09", "."),
  ("neutral_good_flavor_10", "."),
  # Chaotic Good
  ("chaotic_good_flavor_01", "."),
  ("chaotic_good_flavor_02", "."),
  ("chaotic_good_flavor_03", "."),
  ("chaotic_good_flavor_04", "."),
  ("chaotic_good_flavor_05", "."),
  ("chaotic_good_flavor_06", "."),
  ("chaotic_good_flavor_07", "."),
  ("chaotic_good_flavor_08", "."),
  ("chaotic_good_flavor_09", "."),
  ("chaotic_good_flavor_10", "."),
  
  # Lawful Neutral
  ("lawful_neutral_flavor_01", "."),
  ("lawful_neutral_flavor_02", "."),
  ("lawful_neutral_flavor_03", "."),
  ("lawful_neutral_flavor_04", "."),
  ("lawful_neutral_flavor_05", "."),
  ("lawful_neutral_flavor_06", "."),
  ("lawful_neutral_flavor_07", "."),
  ("lawful_neutral_flavor_08", "."),
  ("lawful_neutral_flavor_09", "."),
  ("lawful_neutral_flavor_10", "."),
  # True Neutral
  ("neutral_flavor_01", "."),
  ("neutral_flavor_02", "."),
  ("neutral_flavor_03", "."),
  ("neutral_flavor_04", "."),
  ("neutral_flavor_05", "."),
  ("neutral_flavor_06", "."),
  ("neutral_flavor_07", "."),
  ("neutral_flavor_08", "."),
  ("neutral_flavor_09", "."),
  ("neutral_flavor_10", "."),
  # Chaotic Neutral
  ("chaotic_neutral_flavor_01", "."),
  ("chaotic_neutral_flavor_02", "."),
  ("chaotic_neutral_flavor_03", "."),
  ("chaotic_neutral_flavor_04", "."),
  ("chaotic_neutral_flavor_05", "."),
  ("chaotic_neutral_flavor_06", "."),
  ("chaotic_neutral_flavor_07", "."),
  ("chaotic_neutral_flavor_08", "."),
  ("chaotic_neutral_flavor_09", "."),
  ("chaotic_neutral_flavor_10", "."),
  
  # Lawful Evil
  ("lawful_evil_flavor_01", "."),
  ("lawful_evil_flavor_02", "."),
  ("lawful_evil_flavor_03", "."),
  ("lawful_evil_flavor_04", "."),
  ("lawful_evil_flavor_05", "."),
  ("lawful_evil_flavor_06", "."),
  ("lawful_evil_flavor_07", "."),
  ("lawful_evil_flavor_08", "."),
  ("lawful_evil_flavor_09", "."),
  ("lawful_evil_flavor_10", "."),
  # Neutral Evil
  ("neutral_evil_flavor_01", "."),
  ("neutral_evil_flavor_02", "."),
  ("neutral_evil_flavor_03", "."),
  ("neutral_evil_flavor_04", "."),
  ("neutral_evil_flavor_05", "."),
  ("neutral_evil_flavor_06", "."),
  ("neutral_evil_flavor_07", "."),
  ("neutral_evil_flavor_08", "."),
  ("neutral_evil_flavor_09", "."),
  ("neutral_evil_flavor_10", "."),
  # Chaotic Evil
  ("chaotic_evil_flavor_01", "."),
  ("chaotic_evil_flavor_02", "."),
  ("chaotic_evil_flavor_03", "."),
  ("chaotic_evil_flavor_04", "."),
  ("chaotic_evil_flavor_05", "."),
  ("chaotic_evil_flavor_06", "."),
  ("chaotic_evil_flavor_07", "."),
  ("chaotic_evil_flavor_08", "."),
  ("chaotic_evil_flavor_09", "."),
  ("chaotic_evil_flavor_10", "."),
  
  # Insane
  ("insane_flavor_01", "."),
  ("insane_flavor_02", "...."),
  ("insane_flavor_03", "Erraauuuuhh!"),
  ("insane_flavor_04", "o k a y."),
  ("insane_flavor_05", "K,i,ll!"),
  ("insane_flavor_06", "Y...es..."),
  ("insane_flavor_07", "Bwahahahaha."),
  ("insane_flavor_08", "Huahahaha"),
  ("insane_flavor_09", "Grrhhhhrrrhhgg!"),
  ("insane_flavor_10", "...m.AS.t..e.R.."),
  
  ##################### Servant Class Skill Followed by Descriptions ##############################
  ## Remember skills go E- (9), E (8), D (7), C (6), B (5), A (4), A+ (3), A++ (2), EX (1) ########
  #################################################################################################
  ("skill_name", "Skill Name"),
  ("skill_name_desc", "Skill Description"),
  ("skill_independent_action", "Independent Action"), 									# Archers, some Assassin
  ("skill_item_construction", "Item Construction"), 									# Casters
  ("skill_territory_creation", "Territory Creation"), 									# Casters
  ("skill_mad_enhancement", "Mad Enhancement"), 										# Berserker
  ("skill_presence_concealment", "Presence Concealment"), 								# Assassin
  ("skill_riding", "Riding"), 															# Rider, Saber
  ("skill_magic_resistance", "Magic Resistance"), 										# The Knight Classes: Saber, Lancer, Archer
  
  ("skill_independent_action_desc", "Independent Action is the ability to remain independent even when rejecting the Magical Energy supply from one's Master; the ability that allows for action even in the absence of the Master. At higher ranks, it is also possible to remain for extended periods of time in this world without an established contract. It is both useful and troublesome depending on the disposition of the Servant and the rank of Independent Action. Acting in autonomy from the Master's Magical Energy supply, the Master can concentrate their own Magical Energy on large spells, or the Servant will be fine even in the case they cannot supply Magical Energy due to injury. The downside is that they can be harder to control and keep by their side, making the only true way to command them is by utilizing Command Spells."),
  ("skill_item_construction_desc", "Item Construction is a Caster-class Skill. It is the Skill to manufacture magical items, from implements of war to items for daily use. Also, this Skill requires time to gather components and manufacture items."),
  ("skill_territory_creation_desc", "Territory Creation is a Caster-class Skill. It is the Skill to build a special terrain that is advantageous to oneself as a magus, such as for the purpose of collecting mana."),
  ("skill_mad_enhancement_desc", "Mad Enhancement is the Class Skill that characterizes a Berserker, raising basic parameters and strengthens one's physical abilities in exchange of hindering mental capacities and/or in exchange for their sense of reason. In some cases, it also affects and/or seals away some techniques, Personal Skills and Noble Phantasms."),
  ("skill_presence_concealment_desc", "Presence Concealment is the capacity to hide one's presence as a Servant. It is a common Class Skill of the Assassin class."),
  ("skill_riding_desc", "Riding is a Class Skill of Rider and Saber class Servants, denoting the ability to ride mounts and vehicles. Rider-class Servants will typically possess a high rank. It is said that dragon type mounts are an exception, requiring a different ability other than Riding."),
  ("skill_magic_resistance_desc", "Magic Resistance grants protection against magical effects. Differing from the Resistance effect that merely rejects Magical Energy, this ability cancels the spells altogether."),
  
  ########################## Servant Personal Skill ################################################
  ## Keep in mind skills go E- (9), E (8), D (7), C (6), B (5), A (4), A+ (3), A++ (2), EX (1) #####
  ################################### also nasu, wtf with some of these ############################
  ("skill_aesthetics_of_the_last_spurt", "Aesthetics of the Last Spurt"), 					#Atalanta
  ("skill_affections_of_the_goddess", "Affection of the Goddess"), 							#Achilles
  ("skill_all_kinds_of_talents", "All Kinds of Talents"),									#Richard Lionheart
  ("skill_avyssos_of_labrys", "Avyssos of Labrys"), 										#Asterios
  # Has basic implementation, works okay. Part of Death Handling Script.
  # TODO: Confirm that the character does not die, add delay somehow
  ("skill_battle_continuation", "Battle Continuation"), 									#Cu Chulainn, King Hassan, Heracles, Achilles, Saint George
  ("skill_bow_and_arrow_creation", "Bow and Arrow Creation"), 								#Arash
  ("skill_bravery", "Bravery"), 															#Fergus, Achilles, Heracles
  ("skill_charisma", "Charisma"), 															#Artoria, Richard, Gilgamesh, Iskandar
  ("skill_clairvoyance", "Clairvoyance"), 													#Fionn, EMIYA, Arash
  ("skill_crossing_arcadia", "Crossing Arcadia"), 											#Atalanta
  ("skill_disengage", "Disengage"), 														#Cu Chulainn, Hector, Siegfried
  ("skill_divinity", "Divinity"), 															#Cu Chulainn, Gilgamesh, Heracles, Medusa, Iskandar, Achilles, Saint George
  ("skill_enchant", "Enchant"), 															#William Shakespeare
  ("skill_eternal_arms_mastery", "Eternal Arms Mastery"), 									#Lancelot
  ("skill_evaporation_of_sanity", "Evaporation of Sanity"), 								#Astolfo
  ("skill_evening_bell", "Evening Bell"), 													#King Hassan
  ("skill_expert_of_many_specializations", "Expert of Many Specializations"), 				#Hundred Faces
  ("skill_eye_for_art", "Eye for Art"), 													#Gilles de Rais
  ("skill_eye_of_the_mind_false", "Eye of the Mind (False)"), 								#Heracles
  ("skill_eye_of_the_mind_true", "Eye of the Mind (True)"), 								#Fergus, Diarmuid, EMIYA
  ("skill_galvanism", "Galvanism"), 														#Frankenstein
  ("skill_godspeed", "Godspeed"),															#Richard Lionheart
  ("skill_golden_fleece", "Golden Fleece"), 												#Medea
  # Has basic implementation, needs presentation work and balance tweaking
  ("skill_golden_rule", "Golden Rule"), 													#Siegfried, Gilgamesh, Robin Hood
  ("skill_guardian_knight", "Guardian Knight"), 											#Saint George
  ("skill_heros_bridesmaid", "Hero's Bridesmaid"), 											#Brynhildr
  ("skill_highspeed_divine_words", "High-Speed Divine Words"), 								#Medea
  ("skill_highspeed_incantation", "High-Speed Incantation"), 								#Hohenheim
  ("skill_hollow_lament_of_the_falsely_living", "Hollow Lament of the Falsely Living"), 	#Frankenstein
  ("skill_honor_of_the_battered", "Honor of the Battered"), 								#Spartacus
  ("skill_imperial_privilege", "Imperial Privilege"), 										#Nero
  ("skill_instinct", "Instinct"), 															#Artoria, Saint George
  ("skill_librarian_of_stored_knowledge", "Librarian of Stored Knowledge"), 				#Hundred Faces
  ("skill_lionheart", "Lionheart"),															#Richard Lionheart
  ("skill_love_spot", "Love Spot"), 														#Diarmuid
  ("skill_magecraft", "Magecraft"), 														#Fionn, EMIYA
  ("skill_mana_burst", "Mana Burst"), 														#Artoria
  ("skill_mana_burst_flames", "Mana Burst (Flames)"), 										#Brynhildr
  ("skill_mental_pollution", "Mental Pollution"), 											#Gilles de Rais
  ("skill_migraine", "Migraine"), 															#Nero, Debuff
  ("skill_military_tactics", "Military Tactics"), 											#Hector, Iskandar
  ("skill_monstrous_strength", "Monstrous Strength"), 										#Asterios, Medusa, Astolfo
  ("skill_mystic_eyes", "Mystic Eyes"), 													#Medusa
  ("skill_natural_body", "Natural Body"), 													#King Hassan
  ("skill_natural_monster", "Natural Monster"), 											#Asterios
  ("skill_nature_of_a_rebellious_spirit", "Nature of a Rebellious Spirit"), 				#Fergus
  ("skill_numerology", "Numerology"), 														#Avicebron
  ("skill_poison_res", "Poison Resistance"), 												#Serenity
  ("skill_primordial_rune", "Primordial Rune"), 											#Brynhildr
  ("skill_projectile_dagger", "Projectile (Dagger)"), 										#Cursed Arm, Serenity, Shadow Peeling
  ("skill_proof_of_friendship", "Proof of Friendship"), 									#Hector
  ("skill_protection_from_arrows", "Protection From Arrows"), 								#Cu Chulainn
  ("skill_protection_from_wind", "Protection from Wind"), 									#Cursed Arm
  ("skill_protection_of_the_fairies", "Protection of the Fairies"), 						#Lancelot
  ("skill_protection_of_the_faith", "Protection of the Faith"), 							#King Hassan
  ("skill_robust_health", "Robust Health"), 												#Arash
  ("skill_rune_magic", "Rune Magic"), 														#Cu Chulainn
  ("skill_selfmodification", "Self-Modification"), 											#Cursed Arm
  ("skill_selfpreservation", "Self-Preservation"), 											#William Shakespeare
  ("skill_shadow_lantern", "Shadow Lantern"), 												#Shadow Peeling
  ("skill_shapeshift", "Shapeshift"), 														#Serenity
  ("skill_soul_of_a_martyr", "Soul of a Martyr"), 											#Saint George
  ("skill_subversive_activities", "Subversive Activities"), 								#Robin Hood
  ("skill_uncrowned_martial_arts", "Uncrowned Martial Arts"), 								#King Hassan
  
  ########################## Servant Personal Skill Descriptions ###################################
  ##### Keep in mind that rank does change the actual implementation ###############################
  ############################ again nasu, wtf are with some of these ##############################
  
  ("skill_aotls_desc", "is a Skill that derives from Atalanta having always made her opponents run ahead in footraces that she ultimately won."),
  #Faster movement when facing losing odds
  ("skill_aotg_desc", "is a Skill that denotes one being loved by a goddess."), 
  #Skill rank up, except mana and luck
  ("skill_akot_desc", "RICHARDLIONHEART"),
  #TODO EDIT LATER
  ("skill_aol_desc", "is the pair of giant axes Asterios owns. It's the symbol of the Labyrinth and the root of that very word"), 
  #Just his equipment
  ("skill_bc_desc", "is a Skill that allows for the continuation of combat after sustaining mortal wounds. It will also reduce mortality rate from injury. This Skill represents the ability to survive and/or the mentality of one who doesn't know when to give up, consisting of one's strength of vitality in predicaments. It is also one of the powers of a vampire. The best result is achieved when a resilient body is combined with this Skill."), 
  #gain health on death
  ("skill_bac_desc", "is a Skill for designing and creating a bow and arrows."), 
  #infinite arrows
  ("skill_brv_desc", "is the ability to negate mental interference such as pressure, confusion and fascination. It also has the bonus effect of increasing melee damage. Not usable under the effects of Mad Enhancement."), 
  #melee buff that disallows any sort of mental debuffs
  ("skill_chrsma_desc", "is a composite Skill consisting of a person's charm as well as the natural talent to command or unify an army or country. Increases the ability of allies during group battles."), 
  #leadership, tactics boost, allows servant to convince others much more easily as well as boost your army size and recruitment numbers
  ("skill_clrvynce_desc", "is a visual ability that is also called 'Eagle Eye'. This is generally a must-have ability of the Archer class. It is also frequently used during scouting. Simply looking from a high location is sufficient to fully survey a town and search for enemies. In addition, Clairvoyance will affect the accuracy of bows. It connotes superior visual perception and dynamic occipital capture, such as to supplement the long-range aiming of projectile weapons. At higher ranks, it is possible that the bearers of this Skill have acquired abilities such as precognition ('future vision') and other forms of perception beyond standard eyesight (X-ray vision and so forth). It is a Skill furnished on the flesh."), 
  #increased perception, maybe boosted zoom, 
  ("skill_ca_desc", "is a Skill that allows for quickly moving over the field of battle."), 
  #increased footspeed and jump
  ("skill_disengage_desc", "is the ability to withdraw from the battlefield in the midst of combat or reset the battle conditions."), 
  #upon use combatants back to spawn with full health, ailments persist
  ("skill_divinity_desc", "is the measure of whether one has Divine Spirit aptitude or not."), 
  #Allows damage on certain opponents, takes bonus damage from anti-divine objects and a few other things
  ("skill_enchant_desc", "is the Skill to endow concepts to items."), 
  #grants weapon and stat buffs to allies and master
  ("skill_eternal_arms_mastery_desc", "prevents degradation of fighting skills when under the effect of mental hindrance."), 
  #doesn't take the WPN debuffs such as mad enhancement and debuffs
  ("skill_evaporation_of_sanity_desc", "is a Skill that denotes that one's reasoning is disappearing, making it impossible to keep any secrets"), 
  #upon meeting reveals their own skills and name, and random others on team
  ("skill_evening_bell_desc", "signals the announcement of funereal proceedings and the visitation of Death. This unique Skill manifests as a result of having become one with the rites of passing"), 
  #King Hassan is summoned to kill those who lost sight of their own mortality. Instant kill skill.
  ("skill_expert_of_many_specializations_desc", "is access to and use of many professional skills"), 
  #gives rank B stats on all skills
  ("skill_eye_for_art_desc", "is infatuation with works of art."), 
  #reveals true name if enemy's noble phantasm is weapon
  ("skill_eye_of_the_mind_false_desc", "is a natural talent to foresee/sense and avoid danger on the basis of an innate 6th sense, intuition, or prescience. The accuracy of this instinct can be augmented by experience. The ability also grants an effect of offering resistance against penalties caused by visual obstructions."), 
  #no blindness debuffs, increases dodge chance, increases defend animation speed
  ("skill_eye_of_the_mind_true_desc", "is a heightened capacity for observation, refined through training, discipline and experience. A danger-avoidance ability that utilizes the intelligence collected up to the current time as the basis in order to predict the opponent’s activity and change the current situation. This is not a result of talent, but an overwhelming amount of combat experience. A weapon wielded by none other than a mortal, gained through tenacious training. So long there is even a 1% chance of a comeback, this ability greatly improves the chances of winning."), 
  #increases dodge chance, increases defend animation speed
  ("skill_galvanism_desc", "is a Skill that allows unrestricted conversion and accumulation of magical energy and any form of electricity. Like Phlogiston and Ether, the concept of Galvanism, which explains organic activity as a bio-electric process, falls under the domain of Alchemy."), 
  #magic energy attacks recharge health and mana
  ("skill_godspeed_desc", "RICHARDLIONHEART"),
  #TODO EDIT LATER
  ("skill_golden_fleece_desc", "is a very valuable item that can summon a dragon when utilized properly. The pelt of the golden-furred winged ram of Colchis. It contains the legend, 'if thrown on the earth, a dragon will appear.' Though it is classified as a Noble Phantasm normally, it functions as a Skill in this case."), 
  #allows medea to summon a phantasmal beast
  ("skill_golden_rule_desc", "is a Skill that refers to the measurement of one's fortune to acquire wealth."), 
  #random chance of a daily income based on skill level, also increased chance of treasures spawning -for seig it ranks down luck-
  ("skill_guardian_knight_desc", "is a rare Skill that grants the Servant a temporary attribute bonus if acting in the defense of others."), 
  #boost skills if in combat with weaker allies
  ("skill_heros_bridesmaid_desc", "is a Skill that represents the nature of a Valkyrie that leads a hero to victory."), 
  #everytime ally lands critical hits, their chance and damage increases and their health is boosted
  ("skill_highspeed_divine_words_desc", "is a Skill that assists with magical incantations via a power to activate Thaumaturgy without the use of Magic Circuits. The language of the Age of Gods, back when words played a heavy role in spellcasting. As such, it is a power long lost by modern magi. It is particularly useful for direct attack magic."), 
  #Highlevel spells cast at single-action spell speeds
  ("skill_highspeed_incantation_desc", "is the ability to speak magical incantations at an accelerated speed."), 
  #Highlevel spells cast at single-action spell speeds
  ("skill_hollow_lament_of_the_falsely_living_desc", "is a Skill where the Servant is capable of emitting an immense scream, temporarily robbing enemies and allies alike of their ability to think and respond; those who do not expect it or are incapable of the mental resilience to withstand it may temporarily lose their ability to breathe. The effectiveness of this Skill is increased with the activation of Mad Enhancement."), 
  #Upon activation stun everyone in a range, if mad enhancement is active, double the range and stun duration
  ("skill_honor_of_the_battered_desc", "is a Skill that make those who have it become more powerful as they are inflicted more pain. The cost used to heal injuries via magecraft or similar methods is reduced."), 
  #increase stats upon being damaged, low self-heal rate and heal skills cost 1/4 and are more effective, cannot fight at full strength without pain
  ("skill_imperial_privilege_desc", "is an ability that, due to the insistence of the owner, Skills that are essentially impossible to possess can be obtained for a short period of time."), 
  #allows the taking of any skill
  ("skill_instinct_desc", "is the ability to instantly identify 'the best personal course of action' during combat. Because this Skill allows for the prediction of trajectory, it is possible to avoid attacks from firearms."), 
  #no visual or auditory stuns, increases dodge chance
  ("skill_librarian_of_stored_knowledge_desc", "is an ability that makes it possible for a clear recall of knowledge from memory with a successful Luck Check, even if the information perceived in the past was not consciously acknowledged at the time."), 
  #can know the unknowable based on luck check, maybe recognize servants based on nps
  ("skill_lionheart_desc", "RICHARDLIONHEART"),
  #TODO EDIT LATER
  ("skill_love_spot_desc", "is a spell (curse) inherent of one's facial features which is cast as soon as the target looks at the user's face; a Mystic Face. Enchants those of the user's opposite sex with magical energy."), 
  #decreased female enemy skills unless over a certain magic resistance. Works on masters as well 
  ("skill_magecraft_desc", "is knowledge about modern Thaumaturgy."),  
  #allows servants to use master sorceries. depends on unit.
  ("skill_mana_burst_desc", "is the increase in performance caused by infusing one's weapons and body with Magical Energy and instantly expelling it. Simply put, recreating the effect of a jet burst by expending large amounts of Magical Energy."), 
  #on activation the units attacks gain bonus damage, increased defense, or a sort of energy shot.
  ("skill_mana_burst_flames_desc", "is a version of Mana Burst that infuses weapons with Magical Energy that imparts a flame effect."), 
  #envelops weapons in flames that impart a dmg/time effect or a flame shield that imparts damage in an area around them
  ("skill_mental_pollution_desc", "is a Skill where due to possessing a distorted mentality, it is possible for one to shut out any mental interference thaumaturgy. However, at the same time, it becomes impossible for one to come to a mutual understanding with individuals who do not possess an equivalent rank of Mental Pollution."), 
  #cannot be confused, but is insane
  ("skill_migraine_desc", "is a curse inherited from the birthplace of one's previous life."), 
  #curse that inhibits abilities, comes and goes but locks skills when active
  ("skill_military_tactics_desc", "is tactical knowledge used not for one-on-one combat situations, but for battles where many are mobilized. Bonus modifiers are provided during use of one's own Anti-Army Noble Phantasm or when dealing against an enemy Anti-Army Noble Phantasm."), 
  #bonus tactic skills, maybe increased belfry speeds and defense
  ("skill_monstrous_strength_desc", "is a Skill that temporarily grants a rank-up to one's Strength parameter for a time limit determined by the ranking of this Skill. This Skill is borne from the holder's nature as a monster or Demonic Beast."), 
  #on activation increased strength, but will permenantly lower chance of listening based on active duration
  ("skill_mystic_eyes_desc", "is a Skill where one has possession of Mystic Eyes, capable of imparting magical effects upon a subject and interfering with the outside world."), 
  #in medusa's case Unconditionally petrifies those with Rank C Mana or lower and even those with B Rank Mana can be petrified if they are negligent. Those with Rank A or higher won’t be petrified, but will sustain 'pressure' from the Eyes and receive one Rank-Down to all parameters.
  ("skill_natural_body_desc", "is the possession of a perfect body as a living being since birth."), 
  #cannot age, cannot change morphs, temp increase in str
  ("skill_natural_monster_desc", "is a Skill endowed to those who were given birth as a monster rather than to a hero or god that has fallen down to the level of a Magical Beast."), 
  #impossible levels of STR and END
  ("skill_nature_of_a_rebellious_spirit_desc", "is the temperament to never remain at one location and never embrace a lord. A wandering star that does not have the capacity to be king nor is capable of finding his own king. Negates the effects of Charisma with the same rank."), 
  #cannot be led by other Charasmatic servants
  ("skill_numerology_desc", "is the magecraft system Kabbalah."), 
  #golem summoning
  ("skill_poison_res_desc", "is a Skill that makes the user resistant to poison."), 
  #cannot be poisoned, can heal people of poison in higher ranks 
  ("skill_primordial_rune_desc", "is the possession of and knowledge about Runes that came from an older era. Those with possession and knowledge of Primordial Runes can also make use of the classic Rune Magic."), 
  #can use rune magic as well as the rune magic of nordic gods
  ("skill_projectile_dagger_desc", "is the expertise for throwing projectile weapons; in this case, daggers."),  
  #increased throwing weapon damage, grant near instantanious dagger speeds
  ("skill_proof_of_friendship_desc", "is a Skill to suppress an enemy Servant's fighting spirit to a certain extent, where then a dialogue can begin. However, the enemy Servant must not possess a Mental Pollution Skill. In regard to a Holy Grail War, an advantageous check can be obtained when constructing temporary alliances."), 
  #can force AI to go passive in order to converse and ally
  ("skill_protection_from_arrows_desc", "is the ability to deal with projectiles, an increased defense against ranged attacks by predicting the projectiles' trajectories through exceptional means, such as hearing the sound of air being cut, or sensing the killing intent of the enemy."), 
  #guarantees dodge of ranged weapons chance if user can see enemy, increased chance if unseen, guarantee on thrown weapons no matter
  ("skill_protection_from_wind_desc", "is a chant that offers prayers to God, that oneself might receive a charm (spell) of protection against the calamity of evil spirits. This charm originates from the Middle East, used mainly for protection against sandstorms and evil spirits such as Jinns. To call it a 'charm' smacks of superstition, but the strength of its warding is tremendous."), 
  #protects from Artorias invisible Air attacks, maybe disables
  ("skill_protection_of_the_fairies_desc", "is a blessing from Elementals; the capacity to increase one's Luck in dangerous situations. Activation is limited to battlefields, where it is possible to perform feats of arms."), 
  #bonus LCK in battle
  ("skill_protection_of_the_faith_desc", "is a Skill possessed only by those who have sacrificed themselves for a religious view. Despite being a form of divine protection, it is not a blessing from a higher existence. It is the absoluteness of one’s body and mind that was born from their beliefs. But if it is too high, it causes abnormalities in the personality."), 
  #grants strong soak value
  ("skill_robust_health_desc", "is a Skill that are made up of anecdotes of 'not receiving a wound on the battlefield' and 'never becoming sick even once since birth'. It is also a composite Skill and includes Poison Resistance."), 
  #END boost and immunity to poison
  ("skill_rune_magic_desc", "is a Skill that denotes one having possession of Runes - the Magic Crest of Northern Europe."), 
  #can cast a single spell at a time to boost params 
  ("skill_selfmodification_desc", "is the aptitude to remold one's own body or merge one's own flesh with body parts of others. The higher the ranking in this Skill, the further away one is from being a proper hero."), 
  #Allows use of demonic arm
  ("skill_selfpreservation_desc", "is the Skill to preserve one's health so that the user can escape."), 
  #stays out of battle under most circumstances
  ("skill_shadow_lantern_desc", "is a Skill that allows one to become one with the shadows."), 
  #enables assassin to be invisible unless in a fully bright scenario
  ("skill_shapeshift_desc", "is a Skill that refers to both borrowing bodies and appearance change. It is also one of the powers of a vampire"), 
  #enables assassin to change apperance
  ("skill_soul_of_a_martyr_desc", "is a mental protection that negates mental interference."), 
  #immune to mental debuffs 
  ("skill_subversive_activities_desc", "is the talent to reduce the enemy forces in the preliminary stages before going into battle. An expert of traps. However, the higher the ranking of this Skill, the more one's spiritual status as a hero declines."), 
  #chance to kill up to 60% of mook enemies before battle starts
  ("skill_uncrowned_martial_arts_desc", "is arms competency that was not recognized by others due to various reasons."), 
  #until true name is revealed, stats appear lower than actual.
  
###############################################################################
####################### Noble Phantasms #######################################
###################### And Their Descriptions #################################
########## The rule that people only have one phantasm is mad bullshit ########
###############################################################################
  
  #Saber Noble Phantasms
  ("noble_phantasm_saber_01_01","Excalibur"), #Artoria Pendragon
  ("noble_phantasm_saber_01_02","Invisible Air"), #Artoria Pendragon
  ("noble_phantasm_saber_02_01","Aestus Domus Aurea"), #Nero Claudius
  ("noble_phantasm_saber_02_02","Laus Saint Claudius"), #Nero Claudius
  ("noble_phantasm_saber_03_01","Balmung"), #Siegfried
  ("noble_phantasm_saber_03_02","Armor of Fafnir"), #Siegfried
  ("noble_phantasm_saber_03_03","Das Rheingold"), #Siegfried
  ("noble_phantasm_saber_04","Caladbolg"), #Fergus mac Roich
  ("noble_phantasm_saber_05_01","Excalibur"), #Richard Lionheart
  ("noble_phantasm_saber_05_02","Wandering King Does Not Conquer Alone"), #Richard Lionheart
  #Lancer Noble Phantasms
  ("noble_phantasm_lancer_01_01","Gáe Bolg: Barbed Spear that Pierces with Death"), #Cu Chulainn
  ("noble_phantasm_lancer_01_02","Gáe Bolg: Soaring Spear that Strikes with Death"), #Cu Chulainn
  ("noble_phantasm_lancer_02_01","Gáe Dearg: Crimson Rose of Exorcism"), #Diarmuid Ua Duibhne
  ("noble_phantasm_lancer_02_02","Gáe Buidhe: Yellow Rose of Mortality"), #Diarmuid Ua Duibhne
  ("noble_phantasm_lancer_03_01","Mac an Luin"), #Fionn mac Cumhaill
  ("noble_phantasm_lancer_03_02","Fintan Ginegas"), #Fionn mac Cumhaill
  ("noble_phantasm_lancer_03_03","Uisce Beatha"), #Fionn mac Cumhaill
  ("noble_phantasm_lancer_04_01","Durindana Pilum"), #Hector
  ("noble_phantasm_lancer_04_02","Durindana Spada"), #Hector
  ("noble_phantasm_lancer_05_01","Brynhildr Romantia"), #Brynhildr
  ("noble_phantasm_lancer_05_02","Brynhildr Komédia"), #Brynhildr
  #Archer Noble Phantasms
  ("noble_phantasm_archer_01","Unlimited Blade Works"), #EMIYA
  ("noble_phantasm_archer_02_01","Enuma Elish: The Star of Creation that Split Heaven and Earth"), #Gilgamesh
  ("noble_phantasm_archer_02_02","Enkidu: Chains of Heaven"), #Gilgamesh
  ("noble_phantasm_archer_02_03","Potion of Youth"), #Gilgamesh
  ("noble_phantasm_archer_03_01","Phoebus Catastrophe"), #Atalanta
  ("noble_phantasm_archer_03_02","Agrius Metamorphosis"), #Atalanta
  
  # ("noble_phantasm_archer_03_03","The Seven Arrows of the Big Dipper"), #Atalanta
  
  ("noble_phantasm_archer_04_01","No Face May King"), #Robin Hood
  ("noble_phantasm_archer_04_02","Yew Bow"), #Robin Hood
  ("noble_phantasm_archer_05","Stella"), #Arash
  #Rider Noble Phantasms
  ("noble_phantasm_rider_01_01","Blood Fort Andromeda"), #Medusa
  ("noble_phantasm_rider_01_02","Breaker Gorgon"), #Medusa
  ("noble_phantasm_rider_01_03","Bellerophon"), #Medusa
  ("noble_phantasm_rider_01_04","Harpe"), #Medusa 
  # Has basic implementation, not happy with this. Need to make it work more like a traditional reinforcement
  ("noble_phantasm_rider_02_01","Ionioi Hetairoi"), #Iskandar
  ("noble_phantasm_rider_02_02","Gordius Wheel"), #Iskandar
  ("noble_phantasm_rider_02_03","Via Expugnatio"), #Iskandar
  ("noble_phantasm_rider_03_01","Andreias Amarantos"), #Achilles
  ("noble_phantasm_rider_03_02","Diatrekhon Aster Lonkhe"), #Achilles
  ("noble_phantasm_rider_03_03","Dromeus Kometes"), #Achilles
  ("noble_phantasm_rider_03_04","Akhilleus Kosmos"), #Achilles
  ("noble_phantasm_rider_03_05","Troias Tragoidia"), #Achilles
  ("noble_phantasm_rider_04_01","Hippogriff"), #Astolfo
  ("noble_phantasm_rider_04_02","Casseur de Logistille"), #Astolfo
  ("noble_phantasm_rider_04_03","La Black Luna"), #Astolfo
  ("noble_phantasm_rider_04_04","Trap of Argalia"), #Astolfo
  ("noble_phantasm_rider_05_01","Interfectum Dracones"), #Saint George
  ("noble_phantasm_rider_05_02","Bayard"), #Saint George
  ("noble_phantasm_rider_05_03","Ascalon"), #Saint George
  ("noble_phantasm_rider_05_04","Abyssus Draconis"), #Saint George
  #Caster Noble Phantasms
  ("noble_phantasm_caster_01","Rule Breaker"), #Medea
  ("noble_phantasm_caster_02","Prelati's Spellbook"), #Gilles de Rais
  ("noble_phantasm_caster_03_01","First Folio"), #William Shakespeare
  ("noble_phantasm_caster_03_02","The Globe"), #William Shakespeare
  ("noble_phantasm_caster_04","Golem Keter Malkuth"), #Avicebron
  ("noble_phantasm_caster_05","Sword of Paracelsus"), #Hohenheim
  #Assassin Noble Phantasms
  ("noble_phantasm_assassin_generic","Zabaniya"), #Cursed Arm
  ("noble_phantasm_assassin_01","Zabaniya: Delusional Heartbeat"), #Cursed Arm
  ("noble_phantasm_assassin_02","Zabaniya: Delusional Illusion"), #Hundred Faces
  ("noble_phantasm_assassin_03","Zabaniya: Febrile Inspiration"), #Shadow Peeling
  ("noble_phantasm_assassin_04","Zabaniya: Delusional Poison Body"), #Serenity
  ("noble_phantasm_assassin_05","Azrael: The Angel That Announces Death"), #King Hassan
  #Berserker Noble Phantasms
  ("noble_phantasm_berserker_01_01","God Hand"), #Heracles
  ("noble_phantasm_berserker_01_02","Nine Lives"), #Heracles
  ("noble_phantasm_berserker_02_01","For Someone's Glory"), #Lancelot
  ("noble_phantasm_berserker_02_02","Knight of Owner"), #Lancelot
  ("noble_phantasm_berserker_02_03","Arondight"), #Lancelot
  ("noble_phantasm_berserker_03","Crying Warmonger"), #Spartacus
  ("noble_phantasm_berserker_04_01","Blasted Tree"), #Frankenstein's Monster
  ("noble_phantasm_berserker_04_02","Bridal Chest"), #Frankenstein's Monster
  ("noble_phantasm_berserker_05","Chaos Labyrinthos"), #Asterios
  #Avenger Noble Phantasms
  ("noble_phantasm_avenger_01", "Verg Avesta"), #Angra Mainyu
  #Shielders Noble Phantasms
  ("noble_phantasm_shielder_01_01", "Lord Chaldeas"), #Mashu Kyrielight
  ("noble_phantasm_shielder_01_02", "Lord Camelot"), #Mashu Kyrielight
  #Rulers Noble Phantasms
  ("noble_phantasm_ruler_01_01", "La Pucelle",), #Jeanne d'Arc
  ("noble_phantasm_ruler_01_02", "Luminosité Eternelle",), #Jeanne d'Arc
  ("noble_phantasm_ruler_02_01", "Evil Eater"), #Amakusa Shirou Tokisada
  ("noble_phantasm_ruler_02_02", "Xanadu Matrix"), #Amakusa Shirou Tokisada
  ("noble_phantasm_ruler_02_03", "Big Crunch"), #Amakusa Shirou Tokisada
  
########## Noble Phantasm Classifications ############################
######### Only uncomment relevent ones ###############################
("noble_phantasm_unknown", "Unknown"),  
#Due to its variable nature, Nine Lives is not given a classification.
("noble_phantasm_antiunit", "Anti-Unit Noble Phantasm"),  
#Anti-Unit Noble Phantasms are those specialized against defeating other people in single combat. Compared to stronger weapons, a majority of Anti-Unit Noble Phantasms will not go beyond defeating other beings no matter how strong the magical energy or curse they wield. Saber's Invisible Air wouldn't increase the speed of an action like chopping wood, and Lancer's Gáe Bolg, which strikes the opponent's heart, would just be a strong lance if the target is a rock or a house. The advantage over stronger weapons is that they can be compared to guns with unlimited ammo while Anti-Army Noble Phantasms are one-time missiles. Missiles are more powerful, but an Anti-Unit can be continuous like Invisible Air. There are also those that are very cost effective like Gáe Bolg, so it can be called a sure-hit arrow that is better at killing single people than a cannonball.
("noble_phantasm_antiunit_self", "Anti-Unit (Self) Noble Phantasm"), 
#Anti-Unit Noble Phantasm used upon the wielder instead of another target.
("noble_phantasm_antiself", "Anti-Self Noble Phantasm"), 
#The classification of The Queen's Glass Game.
("noble_phantasm_antimind", "Anti-Mind Noble Phantasm"), 
#This classification of Noble Phantasms that targets the mind. While normally Anti-Mind, Sarasvati Meltout, within the cyberspace of the Moon Cell, becomes Anti-City and Anti-World.
("noble_phantasm_antiteam", "Anti-Team Noble Phantasm"), 
#The classification of Aestus Domus Aurea.
("noble_phantasm_labyrinth", "Labyrinth Noble Phantasm"), 
#The classification of Noble Phantasms that brings forth a Labyrinth or something similar to the concept of a maze.
("noble_phantasm_antiarmy", "Anti-Army Noble Phantasm"), 
#Anti-Army Noble Phantasms are those with a wide enough range to be specialized against armies.[6] They are more powerful weapons than Anti-Unit Noble Phantasms, capable of easily blowing them away, but they must be used more wisely because of their larger cost. Using an A Rank Noble Phantasm takes a great deal of mana, so they must wait before they can use it again.
("noble_phantasm_antiarmy_self", "Anti-Army (Self) Noble Phantasm"), 
#Anti-Army Noble Phantasms used upon the wielder's own army instead of another group.
("noble_phantasm_antigate", "Anti-Gate Noble Phantasm"), 
#The classification of Noble Phantasms that has power capable of blowing away sturdy walls, doors and gates.
("noble_phantasm_antifort", "Anti-Fortress Noble Phantasm"), 
#This rank has powerful Noble Phantasms that can even blow away solid fortified structures. While the difference between Anti-Unit and Anti-Army is the variation in area of effect, Anti-Fortress Noble Phantasms are distinguished from other categories by the great difference in power. Only Excalibur has this rank in all of the Noble Phantasms of the Fourth and Fifth Holy Grail Wars. God Force in Fate/Extra also possesses this rank.
("noble_phantasm_fort", "Fortress Noble Phantasm"), 
#Fortress Noble Phantasms are those that consists of solid fortified structures.
("noble_phantasm_antimountain", "Anti-Mountain Noble Phantasm"), 
#The classification of An Gal Ta Kigal She.
("noble_phantasm_antipop", "Anti-Populace Noble Phantasm"), 
#The classification of Noble Phantasms that targets the people, whether they are of the masses or they are that of a civilization.
("noble_phantasm_anticity", "Anti-City Noble Phantasm"), 
#The classification of Noble Phantasms that has enough range to potentially cover an entire city.
("noble_phantasm_anticountry", "Anti-Country Noble Phantasm"),
#The classification of Noble Phantasms that has enough range to potentially cover an entire country.
("noble_phantasm_antiworld", "Anti-World Noble Phantasm"), 
#The classification of Noble Phantasms that affect the World itself, such as Enuma Elish. While the actual output is around the level of Excalibur, Enuma Elish's effect of unquestionably being that of the legend of "ripping the world" puts the sword in a special category.
("noble_phantasm_antiplanet", "Anti-Planet Noble Phantasm"), 
#The classification of Angra Mainyu/CCC and Cursed Cutting Crater. According to Leonardo da Vinci, if one releases the full power of a Top Servant's Noble Phantasm, it still can't completely destroy the planet. Even if an Anti-Planet Noble Phantasm existed, according to her calculations, it's still not possible to destroy the planet, since there are already safety measures put into place by the Counter Force. Only creatures from outer space might be able to destroy the Earth.[19]
("noble_phantasm_antithaumaturgy", "Anti-Thaumaturgy Noble Phantasm"), 
#The classification of Noble Phantasms that are meant to be used for the dispelling and destruction of magecraft and other thaumaturgy.
("noble_phantasm_antitreasure", "Anti-Treasure Noble Phantasm"), 
#The classification of Noble Phantasms that targets other treasures, potentially including Noble Phantasms.
("noble_phantasm_divine", "Divine Noble Phantasm"), 
#The classification of Child of the Sun.
("noble_phantasm_antidivine", "Anti-Divine Noble Phantasm"),  
#The classification of Noble Phantasms that are effective against Divine Spirits. They are few in number, including Enkidu and Vasavi Shakti.
("noble_phantasm_antievil", "Anti-Evil Noble Phantasm"),
#The classification of Lord Camelot.
("noble_phantasm_antibanquet", "Anti-Banquet Noble Phantasm"),
#The classification of Bale of Inexhaustibility.
("noble_phantasm_antiwisdom", "Anti-Wisdom Noble Phantasm"), 
#The classification of Fintan Finegas.
("noble_phantasm_antiprinciple", "Anti-Principle Noble Phantasm"), 
#The classification of Prelati's Spellbook.
("noble_phantasm_antiking", "Anti-King Noble Phantasm"), 
#The classification of Alf Layla wa-Layla.
("noble_phantasm_antibeast", "Anti-Beast Noble Phantasm"),
#The classification of Mechanical Illusionary Art - Bull Swallowing.
("noble_phantasm_antidragon", "Anti-Dragon Noble Phantasm"), 
("noble_phantasm_antipurge", "Anti-Purge Noble Phantasm"), 
#The classification of Enkidu's Enuma Elish.
("noble_phantasm_antihuman", "Anti-Human Order Noble Phantasm"), 
#The classification of Ars Almadel Salomonis.
("noble_phantasm_antiwave", "Anti-Wave Noble Phantasm"), 
#The classification of Prydwen Tube Riding.
("noble_phantasm_barrier", "Barrier Noble Phantasm"), 
#The classification of Noble Phantasms used to create barriers, typically Bounded Fields, but it does not include those that are also used to attack. Avalon is purely defensive, so it is a typical example of a barrier. While other Noble Phantasms like Blood Fort Andromeda may create barriers, they will instead be classified by their ability to attack like Blood Fort Andromeda belonging to the Anti-Army category.
("noble_phantasm_magecraft", "Magecraft Noble Phantasm"),  
("noble_phantasm_conceptual", "Conceptual Noble Phantasm"),  
("noble_phantasm_covenant", "Covenant Noble Phantasm"),  
("noble_phantasm_suicide", "Suicide Attack Noble Phantasm"), 
#The classification of Noble Phantasms that sacrifice the user's life like La Pucelle.
("noble_phantasm_drive", "Drive Noble Phantasm"),  
# ####################################################################
# ##### Spell List ###################################################
# ####################################################################

("spell_projection", 			"Projection Magecraft: {s13}"),
("spell_projection_sword", 		"Projection Magecraft: Sword"),
("spell_projection_spear", 		"Projection Magecraft: Spear"),
("spell_projection_bow", 		"Projection Magecraft: Bow and Arrows"),
("spell_projection_shield", 	"Projection Magecraft: Shield"),

("spell_reinforcement_weapon", 	"Reinforcement Magecraft: Weapon"),
("spell_reinforcement_legs", 	"Reinforcement Magecraft: Legs"),
("spell_reinforcement_skin", 	"Reinforcement Magecraft: Skin"),
("spell_reinforcement_eyes", 	"Reinforcement Magecraft: Eyes"),

("spell_time_alter_slow", 		"Time Alter Magecraft: Double Accel"),
("spell_time_alter_dblslow",	"Time Alter Magecraft: Triple Accel"),
("spell_time_alter_trplslow",	"Time Alter Magecraft: Square Accel"),
("spell_time_alter_speed", 		"Time Alter Magecraft: Stagnate"),

("spell_familiar_dtooth", 		"Familiar Magecraft: Dragon Tooth Warrior (Melee)"),
("spell_familiar_dtarcher", 	"Familiar Magecraft: Dragon Tooth Warrior (Ranged)"),
("spell_golem_stone", 			"Golem Creation: Stone Golem"),
("spell_familiar_wire_horse", 	"Familiar Magecraft: Wire Horse"),
("spell_familiar_hornets",		"Familiar Magecraft: Demonic Bees"),
("spell_familiar_puppet",		"Familiar Magecraft: Puppet"),

("spell_heal_minor",			"Healing Magecraft: Minor"),
("spell_heal_major",			"Healing Magecraft: Major"),
("spell_heal_self",				"Healing Magecraft: Self Heal"),

("sacrament_purify",			"Church Sacrament: Purify"),
("sacrament_purify_major",		"Church Sacrament: Total Baptism"),
("sacrament_cure",				"Church Sacrament: Cure"),
("sacrament_wound",				"Church Sacrament: Wound"),
("sacrament_divine",			"Church Sacrament: Bless Weapon"),

("spell_rune_fire",				"Rune Magic: Ansuz"),
("spell_rune_battlecurse",		"Rune Magic: Ath nGabla"),
("spell_rune_tracking",			"Rune Magic: Berkana"),
("spell_rune_fistdmg",			"Rune Magic: Ehwaz"),
("spell_rune_gandr",			"Rune Magic: Gandr"),
("spell_rune_sight",			"Rune Magic: Kenaz"),
("spell_rune_speed",			"Rune Magic: Ansuz-Eihwaz"),
("spell_rune_heal",				"Rune Magic: Heal"),
("spell_rune_dragon",			"Rune Magic: Dragon Spirit"),

("spell_necro_aoe",				"Necromancy: Heart-Bomb"),
("spell_necro_res",				"Necromancy: Animate Corpse"),


("spell_flame_sea",				"Flame Magecraft: Sea of Flames"),
("spell_flame_combust",			"Flame Magecraft: Spontaneous Combustion"),

("spell_earth_pertify",			"Earth Magecraft: Petrification"),
("spell_earth_armor",			"Earth Magecraft: Stoneskin"),

("spell_wind_strike",			"Wind Magecraft: Aero Strike"), #basically excalibur but Wind

("spell_spatial_teleport", 		"Spatial Magecraft: Transportation"),

# New spell systems
#	To balance the elements, each one has a spell in each combat spell
# category, that way no player is left feeling robbed by their choice of
# affinity. Some will be better than others in certain categories, and
# some may have higher tiers available to the user, but hopefully they
# result in a more-or-less balanced tree. Each will also have a unique 
# spell tree as well, unavailable to anyone lacking the affinity as you
# can technically struggle and learn spells outside of your affinity.
#
#	In addition, there are specialized magic trees that can be covered
# by multiple different affinities allowing players with various affinities
# to learn the same spells. I.E. Energy Transfer aka Gem Magecraft can be
# learned with any elemental affinity, but Projection needs a strong Wind
# affinity
#	

#	Change effects with: Double, Triple, Square, Half, Quarter, Flip, Self, Other, 

("magecraft_spell_enhance_double", 	"Double"), # Double Strength
("magecraft_spell_enhance_triple", 	"Triple"), # Triple Strength
("magecraft_spell_enhance_square", 	"Square"), # Four Strength
("magecraft_spell_enhance_half", 	"Half"),	# Half Strength
("magecraft_spell_enhance_quarter",	"Quarter"),	# Quarter Strength

("magecraft_spell_enhance_flip", 	"Flip"),	# Reverse the effect?
("magecraft_spell_enhance_self", 	"Self"),	# Target Self
("magecraft_spell_enhance_other", 	"Other"),	# Target Someone else

#					Fire 	- 	Water 	- 	Wind 	- 	Earth
# Bolt			Fire Bolt	- Ice Shard - Lightning - Boulder Toss
# AOE		  Sea of Flames - Tidalwave	- Tornado	- Quake
# Boon/Buff		Flameweapon	- 	x		- Speed		- Armor
# Trap				x		- Liquify	- 	x 		- Quicksand

# Unique
#	Fire: Melt Armor, Flaming Hand (melee damage), Combustion, Mirage Clone
#	Water: Healing Mist, Thunderstorm, Fog, Icicle Missile
#	Wind: Aerostrike, Flight, Softfall, Blastwave, Mute  
#	Earth: Petrification, Stonewave, Meteorcall, Leglock

("magecraft_element_type", "Elemental Magecraft Spells"),	# Time to Cast, Energy to Cast

("magecraft_fire_bolt", 	"Firebolt"),		# Single Action, 10 # Firebolt
("magecraft_fire_aoe", 		"Sea of Flames"),	# 2 Turn Spell, 35 # Wave of flame eminates forward
("magecraft_fire_buff", 	"Ignite Weapon"),	# Single Action, 10 # Light weapon for boost
("magecraft_fire_unique1", 	"Melt Armor"),		# 2 Turn Spell, 40	# Damage physical armor via melting
("magecraft_fire_unique2", 	"Combustion"),		# 2 Turn Spell, 35 # Ignite target for time
("magecraft_fire_unique3", 	"Mirage Clone"),	# Single Action, 25 # Shadow clone technique using heat mirage
("magecraft_fire_unique4", 	"Lava Plume"),		# 3 Turn, 50	# Fire shoots from ground

("magecraft_water_bolt", 	"Iceshot"),			# Single Action, 10 # Shoots ice shot that creates small aoe
("magecraft_water_aoe", 	"Tidalwave"),		# 2 Turn Spell, 35 # Wave of water
("magecraft_water_trap", 	"Liquify Ground"),	# 2 Turn Spell, 20 # slow opponents by making the ground slick
("magecraft_water_unique1", "Healing Mist"),	# 2 Turn Spell, 35 # Heal allies via a cloud
("magecraft_water_unique2", "Storm Cloud"),		# 3 Turn Spell, 40	# Create overhead cloud that randomly causes lightening
("magecraft_water_unique3", "Fogburst"),		# Single Action, 20 # Halves visibility by raising fog

("magecraft_wind_bolt", 	"Thunderbolt"),		# Single Action, 10	# Shoots bolt of lightening, higher levels chain
("magecraft_wind_aoe", 		"Stormwind"),		# 2 Turn Spell, 35	# Large gust of wind pushes back enemies
("magecraft_wind_buff", 	"Fleetfoot"),		# Single Action, 10	# Increases base speed 
("magecraft_wind_unique1", 	"Aerostrike"),		# 2 Turn Spell, 20	# Swing weapon to push back enemies, cause damage
("magecraft_wind_unique2", 	"Featherfall"),		# Single Action, 10	# Eliminate fall damage entirely
("magecraft_wind_unique3", 	"Blastwave"),		# 2 Turn Spell, 40	# Wind clap in 360 aoe
("magecraft_wind_unique4", 	"Mute"),			# 2 Turn Spell, 20	# Silence opponents within range
("magecraft_wind_unique5", 	"Air Jump"),		# Single Action, 10	# Double jump

("magecraft_earth_bolt", 	"Stoneshot"),		# Single Action, 10	# Launch small boulder towards opponents
("magecraft_earth_aoe", 	"Earthquake"),		# 2 Turn Spell, 35	# Cause localized eathquake, stuns enemies
("magecraft_earth_buff", 	"Stoneskin"),		# Single Action, 10	# Harden skin to increase armor rank
("magecraft_earth_trap", 	"Quicksand"),		# 2 Turn Spell, 20	# slows enemies, stagnant enemies sink down
("magecraft_earth_unique1", "Petrification"),	# 2 Turn Spell, 30	# inflicts petrification on enemies
("magecraft_earth_unique2", "Stonewall"),		# 2 Turn Spell, 25	# Raise walls made of stone to protect yourself
("magecraft_earth_unique3", "Stonewave"),		# 2 Turn Spell, 45	# Wave of stone pushes forward
("magecraft_earth_unique4", "Meteorcall"),		# 3 Turn Spell, 45	# Meteors are pulled from the sky for big damage
("magecraft_earth_unique5", "Encase"),			# Single Action, 25	# Encase an enemies leg to temporarily trap them

# Specialized Magics

#	Alchemy
#		Flash Air (Weak, Damaging Teleportation)
#		Memory Partition
#		Thought Acceleration
#		Transmutation (Manipulates Noble Metals)
#		Potion Creation
#		Soulsealing
("magecraft_alchemy_flashair", 		"Flash Air"),				# 2 Turn Spell, Energy to Cast # Teleportation, swap locations for minor damage on both parties
("magecraft_alchemy_memory", 		"Memory Partition"),		# Single Action, Energy to Cast # Allow multiple tasks to be worked on at a time
("magecraft_alchemy_thought", 		"Thought Acceleration"),	# Single Action, Energy to Cast # Allows mental tasks to be sped along temporarily
("magecraft_alchemy_transmutation", "Transmutation"),			# Single Action, Energy to Cast # Turn iron to gold, viceversa
("magecraft_alchemy_soulseal", 		"Soulseal"),				# 2 Turn Spell, Energy to Cast # Traps soul inside current vessel. Disallows people with soultransfer skills to use.

#	Astromancy
#		Horoscope
#		Starview
#		Starfire
#		Starcall
("magecraft_astromancy_horoscope", 	"Horoscope"),	# Single Action, Energy to Cast # Increase skill checks and luck chances for the week
("magecraft_astromancy_starview", 	"Starview"),	# Single Action, Energy to Cast # 
("magecraft_astromancy_starfire", 	"Starfire"),	# 2 Turn Spell, Energy to Cast # Creates a mine that launches at enemy after sometime
("magecraft_astromancy_starcall", 	"Starcall"),	# 3 Turn Spell, Energy to Cast # Fires an energybolt from the sky

#	Church Sacraments
#		Baptism Sacrament
#		Bless
#		Wound
#		Purify
#		Miracle
("magecraft_sacrament_baptism", 	"Baptism Sacrament"),	# 2 Turn Spell, Energy to Cast # Causes intense damage to spirits, vampires and unholy creatures.
("magecraft_sacrament_bless", 		"Bless"),				# Time to Cast, Energy to Cast # Enhance any weapon with holy weapon
("magecraft_sacrament_wound", 		"Wound"),				# Time to Cast, Energy to Cast # Causes damage to limbs via sacrament
("magecraft_sacrament_purify", 		"Purify"),				# Time to Cast, Energy to Cast #
("magecraft_sacrament_miracle", 	"Miracle"),				# Time to Cast, Energy to Cast # Heals

#	Counter Magic (Harms Circuits)
#		Rebound
#		Block Crest
#		Freeze Mana
("magecraft_countermagic_rebound", 	"Rebound"),		# Single Action, Energy to Cast # Causes any spell to rebounce to caster, causing damage based on mana cost
("magecraft_countermagic_block", 	"Block Crest"),	# Time to Cast, Energy to Cast # Limits all spells from a crest
("magecraft_countermagic_mana", 	"Freeze Mana"),	# 3 Turn Spell, Energy to Cast # Freeze local area mana,halting mana restoration and NPs

#	Curses
#		Geis (Agreement Curse, Severs magic for anyone who violates agreement)
#		Self-Geis Scroll (Same as above, but ruins the violators magic crest)
#		Wound
#		Cripple
#		Blind
("magecraft_curse_geis", 	"Geis"),		# Formalcraft, 1 Turn, Energy to Cast
("magecraft_curse_sgeis", 	"Self-Geis"),	# Formalcraft, 2 Turns, Energy to Cast
("magecraft_curse_wound", 	"Wound"),		# Single Action, Energy to Cast	# Causes localized damage based on level
("magecraft_curse_cripple", "Cripple"),		# 3 Turn Action, Energy to Cast	# Utterly Breaks a limb
("magecraft_curse_blind", 	"Blind"),		# 2 Turn Action, Energy to Cast # Lowers accuracy and range of aggro based on level

#	Energy Transfer
#		Mana Transfer
#		Magic Crest Transfer
#		Magic Crest Addition
("magecraft_transfer_mana", 		"Mana Transfer"),			# Time to Cast, Energy to Cast # Pass mana to others
("magecraft_transfer_crest", 		"Magic Crest Transfer"),	# Time to Cast, Energy to Cast # Transfer crest a piece at a time
("magecraft_transfer_crest_add", 	"Magic Crest Modify"),		# Time to Cast, Energy to Cast # Add/Lose spell inside crest
#		Healing
#			Heal
#			Wound Treatment
("magecraft_healing", 		"Heal"),				# Time to Cast, Energy to Cast # Increase health
("magecraft_healing_wound", "Wound Treatment"),		# Time to Cast, Energy to Cast # Lower limb damage
("magecraft_healing_venom", "Antivenom"),			# Time to Cast, Energy to Cast # reduce poison level
("magecraft_healing_curse", "Countercurse"),		# Time to Cast, Energy to Cast # Reduce curse build up
("magecraft_healing_mind", "Cure Mind"), 			# Single Action, 15 # Lowers all mental debuffs 
#		Jewel/Minerology
#			Pendulum (magic fax)
#			Conductor (magic telephone)
#			Energy Storage (stores mana in gem)
#			Spell Binding (stores spell in gem)
("magecraft_jewel_storage", "Accumulate Energy"),	# Time to Cast, Energy to Cast # Dump mana into stones
("magecraft_jewel_bind", 	"Spell Binding"),		# Time to Cast, Energy to Cast # Store a spell inside a stone to be used as throwing weapon
#		Reincarnation
#			Become Familiar
#			Reborn Other
("magecraft_reincarnation_familiar", 	"Become Familiar"),	# Time to Cast, Energy to Cast # Take control of a familiar
("magecraft_reincarnation_reborn", 		"Reborn Other"),	# Time to Cast, Energy to Cast # Turn a willing person into a dead character
#		Shared Perception
#			Monitor Other
#			Share Senses
("magecraft_perception_other", "Monitor Other"),	# Time to Cast, Energy to Cast # Use magic to spy on someone else
("magecraft_perception_share", "Share Senses"),		# Time to Cast, Energy to Cast # Create a 2 way permenant communication with another. Summons over a certain level of connection automatically have this.
#		Transference of Consciousness
#			Remote Body
#			Spiritual Possession
("magecraft_consciousness_remote", 	"Remote Body"),				# Time to Cast, Energy to Cast # Temporarily control another being
("magecraft_consciousness_possess", "Spiritual Possession"),	# Time to Cast, Energy to Cast # Place a spirit into a body temporarily

#	Necromancy
#		Raise Corpse
#		Corpsebomb
("magecraft_necromancy_raise", 		"Raise Corpse"),	# Time to Cast, Energy to Cast # Raise a dead agent as a weakened zombie 
("magecraft_necromancy_bomb", 		"Corpsebomb"),		# Time to Cast, Energy to Cast # Detonate dead agents as a bomb
("magecraft_necromancy_commune", 	"Commune"),			# Time to Cast, Energy to Cast # Speak to dead people. 

#	Kabbalah
#		Golemancy
#			Golem Creation
#			Golem Collapse
#			Detonate Golem
("magecraft_golemancy_create", 		"Golem Creation"),	# Time to Cast, Energy to Cast # Create golem if you have proper ingredients
("magecraft_golemancy_collapse", 	"Golem Collapse"),	# Time to Cast, Energy to Cast # Collapse either your or others colems
("magecraft_golemancy_detonate", 	"Golem Detonate"),	# Time to Cast, Energy to Cast # Turn your golem into a bomb
#		Numerology
#			Starbow
#			Starmine
#			Fortunesight
#			Holynumber
("magecraft_numerology_starbow", 	"Starbow"),			# Time to Cast, Energy to Cast # Launch arrow of energy from finger
("magecraft_numerology_starmine", 	"Starmine"),		# Time to Cast, Energy to Cast # Place energymine that explode after a certain time
("magecraft_numerology_fortune", 	"Fortunesight"),	# Time to Cast, Energy to Cast # Read fortune, increasing luck and skillchecks
("magecraft_numerology_number", 	"Holynumber"),		# Time to Cast, Energy to Cast # 

#	Material Transmutation
#		Projection
#			Gradation Air
#			Trace
("magecraft_projection_gradation", 	"Gradation Air"),	# Time to Cast, Energy to Cast # Create any item, albiet a temporary version
("magecraft_projection_trace", 		"Trace"),			# Time to Cast, Energy to Cast # Specific form of gradation air, creates permenant version of the item
("magecraft_projection_analysis", "Composition Analysis"), # Learn an items composition for projection
#		Reinforcement
#			Purpose Enhancement
#			Strengthen Material
#			Body Enhancement
("magecraft_reinforcement_enhance", 	"Purpose Enhancement"),	# Time to Cast, Energy to Cast # Make it better in which ever way helps enhance the purpose of the item. I.E. Weapons are more powerful, Armor tougher, food, more nourishing.
("magecraft_reinforcement_strengthen", 	"Strengthen Material"),	# Time to Cast, Energy to Cast # Allows you to strength an item's materials, raising the item rank by one
#		Alteration (add aspects to an object)
#			Imbue Element
#			Imbue Spell
#			Add Effect
("magecraft_alteration_element", 	"Imbue Element"),	# Time to Cast, Energy to Cast # give an item an elemental effect
("magecraft_alteration_spell", 		"Imbue Spell"),		# Time to Cast, Energy to Cast # Implant a spell. Effect depends on the spell
("magecraft_alteration_effect", 	"Add Effect"),		# Time to Cast, Energy to Cast # Adds an effect to an item

#	Mental Interference
#		Memory Manipulation
#			Change Relationship
#			Alter Personality
#			Memory Alter
#			Read Thoughts
("magecraft_memory_relationship", 	"Change Relationship"),	# Time to Cast, Energy to Cast # Boost or lower relationship with an enemy
("magecraft_memory_personality", 	"Alter Personality"),	# Time to Cast, Energy to Cast # change the personality of target
("magecraft_memory_alter", 			"Memory Alter"),		# Time to Cast, Energy to Cast # change memory of target
("magecraft_memory_thought", 		"Read Thoughts"),		# Time to Cast, Energy to Cast # Tells you goals of a character
#		Command
#			Dictate
#			Restrict
("magecraft_command_dictate", 		"Dictate"),		# Time to Cast, Energy to Cast # Force a target to do a task
("magecraft_command_restrict", 		"Restrict"),	# Time to Cast, Energy to Cast # Limit the actions of a target

#	Runes
#		Ansuz (fire)
# 		Ath nGabla (duel curse)
# 		Berkana (enemy tracking)
# 		Ehwaz (punch damage)
# 		Gandr (damage curse)
# 		Kenaz (sight)
# 		Ansuz-Eihwaz (kick, jump, double)
("magecraft_runes_ansuz", 		"Ansuz"),			# Time to Cast, Energy to Cast # Create flames
("magecraft_runes_athngabla", 	"Ath nGabla"),		# Time to Cast, Energy to Cast # Force others away from a fight between you and a target
("magecraft_runes_berkana", 	"Berkana"),			# Time to Cast, Energy to Cast # orb finds an opponent
("magecraft_runes_ehwaz", 		"Ehwaz"),			# Time to Cast, Energy to Cast # Enhance melee damage
("magecraft_runes_gandr", 		"Gandr"),			# Time to Cast, Energy to Cast # Shoot curse bolt, if the target has magic circuits, inflict damage, if not, vomit animation and faint
("magecraft_runes_kenaz", 		"Kenaz"),			# Time to Cast, Energy to Cast, Enhance accuracy
("magecraft_runes_ansuzeihwaz", "Ansuz-Eihwaz"),	# Time to Cast, Energy to Cast, Enhance legs, kick damage, double jump 

#	Spatial
#		Teleportation
#		Ignore Gravity
("magecraft_spatial_teleport", 	"Teleportation"),	# Time to Cast, Energy to Cast # Teleport to location instantly
("magecraft_spatial_gravity", 	"Ignore Gravity"),	# Time to Cast, Energy to Cast # no clip?
("magecraft_spatial_warp", 		"Warp"),			# Time to Cast, Energy to Cast # Deletes distance between you and target

#	Spiritual Evocation
#		Invocation
#			Spirit Possession
("magecraft_invocation_possess", "Spirit Posession"),	# Time to Cast, Energy to Cast # Take over target
#		Evocation
#			Spirit Commune
("magecraft_evocation_commune", "Spirit Commune"),	# Time to Cast, Energy to Cast # Speak to dead locations

#	Time Alter
#		Stagnate
#		Accel
("magecraft_time_stagnate", "Stagnate"),	# Time to Cast, Energy to Cast # Speed time, feign death
("magecraft_time_accel", 	"Accel"),		# Time to Cast, Energy to Cast # Slow down time
("magecraft_time_haste", 	"Haste"),		# Time to Cast, Energy to Cast # Cause character to move faster without dilation
("magecraft_time_slow", 	"Slow"),		# Time to Cast, Energy to Cast # Slow an enemy via time dilation  

#	Puppet
#		Build Puppet
#		Automata
("magecraft_puppet_build", 		"Build Puppet"),	# Time to Cast, Energy to Cast # Create a puppet
("magecraft_puppet_automate", 	"Automate Puppet"),	# Time to Cast, Energy to Cast # Cause puppet to become alive

#	Formalcraft
#		Barriers
#		Traps
#		Workshop Creation
#		Any noncombat spell, but adds time, depending on skill in category.
("magecraft_formal_barrier", 	"Barrier Creation"),	# Time to Cast, Energy to Cast # Create barriers at a location
("magecraft_formal_trap", 		"Trap"),				# Time to Cast, Energy to Cast # Creates trap at a location
("magecraft_formal_workshop", 	"Workshop Creation"),	# Time to Cast, Energy to Cast # Turns location into a workshop
("magecraft_formal_ritual", 	"Ritual"),	# Time to Cast, Energy to Cast # Do a ritual, any spell can be ritualized

("special_dsakura_shadow", "Shadow Giant Manifestation"),
("special_dsakura_mine", "Hollow Space Mine"),

("special_emiya_projection_hrunting", "Broken Phantasm: Hrunting"),
("special_emiya_projection_caladbolg", "Broken Phantasm: Caladbolg II"),
("special_emiya_projection_rho_aias", "Rho Aias: The Seven Rings that Cover the Fiery Heavens"),
("special_emiya_projection_caliburn", "Caliburn: The Fated Sword of Choosing"),

("special_caster_01_01", "Flight"),					# Opens her cape and soars into the air, changing her spell slots, will cause mana drain
("special_caster_01_02", "Beam"),					# Fires Raycasting bolt, causes damage
("special_caster_01_03", "Threads of Fate"),		# Binds enemy inside wires
("special_caster_01_04", "Dragon Tooth Warriors"),	# Summons one Dragontooth Warrior

("special_caster_01_01_flying", "Land"),			# No longer flying, ceases mana drain, changes spell slots back
("special_caster_01_02_flying", "Beam"),			# similar to non-flying beam attack, but with larger range of effectiveness, but lower damage
("special_caster_01_03_flying", "Spacial Rend"),	# Petrifies everyone inside an range of effect
("special_caster_01_04_flying", "Barrage"),			# Fires four or five beams from more random locations, each are much weaker and less accurate than usual, but the ~spectacle~

("special_caster_02_01", "Horror"),				# Summon Starfish thingy
("special_caster_02_02", "Curse"),				# Shoots bolt that causes minimal damage, but devastating limb damage
("special_caster_02_03", "Holy Chant"),			# Heals team, causes damage to undead/unholy/spirit enemies
("special_caster_02_04", "Void Bolt"),			# Large, slow moving, energy bolt

("special_caster_02_01_prelati", "Giant Horror"),			# 10ft tall Starfish with energy attacks
("special_caster_02_02_prelati", "Abyssal Gaze"),			# Curse, but fires in a cone, not as much limb damage, but all limbs caught in it suffer instead
("special_caster_02_03_prelati", "Blasphemer's Chorus"),	# doesn't heal, but causes damage to holy enemies, inflicts deaf/mute/terror
("special_caster_02_04_prelati", "Void Barrage"),			# Fires four void bolts, slightly faster.

("special_caster_03_01", "Behold! His glorious visage!"), # Boosts Master by two attribute levels, giving their weapon rank-C NP status
("special_caster_03_02", "All the world's a stage."), # Summon actors, wooden dolls equipped with random servant equipment
("special_caster_03_03", "A plague upon them!"), # Curse damage to all limbs
("special_caster_03_04", "Get thee to a nunnery!"), # Silence, Rage,

("special_caster_04_01", "Golem Yesod: Foundation"),	# Defensive Golem, extremely slow, very high defensive stats
("special_caster_04_02", "Golem Tiferet: Adornment"),	# Ranged Golem, cannot move, but can unleash a barrage, very weak
("special_caster_04_03", "Golem Gevurah: Strength"),	# Power Golem, Melee based
("special_caster_04_04", "Golem Chokhmah: Wisdom"),		# Magic Golem, Casts spells in order to buff allies and 

("special_caster_05_01", "O, wind"),			# Wind Blast, 45degree shot with knockback
("special_caster_05_02", "O, water"),			# Wave, 3m wide long wave that shoots forward 15 m
("special_caster_05_03", "O, fire"),			# Flame Tornado, 1m diameter flame tornado shoots forward
("special_caster_05_04", "O, earth"),			# Fissure, fissures appear, causing damage 1.5m around the prop, if they are in the prop, they are trapped

# ######################################################################
# ######### Weapon Slot / Damage Type Enhancements #####################
# ######################################################################

# ## The List of Weapon Slots for Reference ############################
# ######### Fate Weapon Slots ##########################################

# fate_weapon_element = 125
# fate_weapon_rank = 128
# fate_weapon_description = 129
# fate_weapon_name = 131

# fate_weapon_mystic_code_spell_01 = 132
# fate_weapon_mystic_code_spell_02 = 133
# fate_weapon_mystic_code_spell_03 = 134

# fate_weapon_additional_attack = 135
# fate_weapon_additional_defense = 136
# fate_weapon_additional_mana = 137
# fate_weapon_additional_damage = 138



# ### Damage Types / Weapon Element
	
("dmg_type_normal", "Normal"),		# Normal (No Bonuses/Weaknesses)
("dmg_type_divine", "Divine"),		# Divine (Strong vs Undead and Divine, Weak vs Demonic)
("dmg_type_demonic", "Demonic"),	# Demonic (Strong vs Divine and Fire, Weak to Holy)
("dmg_type_dragon", "Dragon"),		# Dragon (Strong vs Fire, Earth)
("dmg_type_fire", "Fire"),			# Fire (Strong to Weak to Water)
("dmg_type_water", "Water"),		# Water
("dmg_type_wind", "Wind"),			# Wind
("dmg_type_earth", "Earth"),		# Earth
("dmg_type_holy", "Holy"),			# Holy
("dmg_type_undead", "Undead"),		# Undead (Weak to Holy, Divine, Fire. Resist Else)

# ### Ranged Attacker Types ###
("range_type_burst", "Multishot Archer"),	
("range_type_accurate", "Singleshot Archer"),

# ### Charge Attack Types ####
("chrge_melee_damage", "Enhanced Damage"),	# Basic, charge time is coverted to damage 	
("chrge_melee_beam", "Beam Attack"),	# Shoots a beam attack that increases damage with charge time
("chrge_melee_blast", "Blast Attack"),	# Inflicts knockback aoe, radius is charge dependent
("chrge_melee_sever", "Sever Magic"),	# Inflicts Silence, 2x Charge time

("chrge_range_damage", "Enhanced Damage"),
("chrge_range_barrage", "Rapid Fire Attack"), # Only available to accuracy archers
("chrge_range_explosive", "Explosive Arrows"),	# Causes an explosion at the location of the hit, radius expands with charge time
("chrge_range_freeze", "Freeze Enemy"),	# Inflicts freeze, time is dependent on charge
("chrge_range_cripple", "Wound Enemy"),	# Coverts charge time to wound

# ### Fate Rank Levels
("rank_0", "EX"),
("rank_1", "A"),
("rank_2", "B"),
("rank_3", "C"),
("rank_4", "D"),
("rank_5", "E"),
("rank_6", "F"),
("rank_7", "N/A"),
("rank_up", "+"),
("rank_down", "-"),

# ######################################################################
# ########## Servant Summoning Ritual Options ##########################
# ######################################################################

# ############ Kairi Sisigou's Ritual for Reference ####################
# ######################################################################
# Let silver and steel be the essence.
# Let stone and the archduke of contracts be the foundation.
# Let red be the color I pay tribute to.
# Let rise a wall against the wind that shall fall.
# Let the four cardinal gates close.
# Let the three-forked road from the crown reaching unto the Kingdom rotate."
# "Let it be filled. Again. Again. Again. Again.
# Let it be filled fivefold for every turn, simply breaking asunder with every filling.

# ########## Rin Tohsahka's Ritual for Reference #######################
# Ye first, O silver, O iron. 
# O stone of the foundation, O Archduke of the Contract. 
# Hear me in the name of our great teacher, the Archmagus Schweinorg.
# Let the descending winds be as a wall. 
# Let the gates in all directions be shut, rising above the crown, and let the three-forked roads to the Kingdom revolve.
# Shut. Shut. Shut. Shut. Shut.
# Five perfections for each repetition.
# And now, let the filled sigils be annihilated in my stead!
# Set.
# Let thy body rest under my dominion, let my fate rest in thy blade.
# If thou submitteth to the call of the Holy Grail, and if thou wilt obey this mind, this reason, then thou shalt respond.
# I make my oath here.
# I am that person who is become the virtue of all Heaven.
# I am that person who is covered with the evil of all Hades.
# Thou seven heavens, clad in a trinity of words,
# come past thy restraining rings, and be thou the hands that protect the balance-!

("smn_rit_opening_lines","Let silver and steel be the essence.^ Let stone and the archduke of contracts be the foundation."),
("smn_rit_opening_lines_01","Ye first, O silver, O iron.^ O stone of the foundation, O Archduke of the Contract."),

# If user comes from a magus family, allow this line to give extra summoning power
# s3 being their ancestor
("smn_rit_family_declaration", " Let my great master {s3} be the ancestor"),
("smn_rit_family_declaration_01", " Hear me in the name of our great teacher, the {s3}"),

# {s9} being the team of the user, either black or red
("smn_rit_great_war_team_declaration", "Let {s9} be the color I pay tribute to."),


("smn_rit_first_paragraph", "Let rise a wall against the wind that shall fall.^ Let the four cardinal gates close.^ Let the three-forked road from the crown reaching unto the Kingdom rotate."),
("smn_rit_first_paragraph_01", "Let the descending winds be as a wall.^ Let the gates in all directions be shut, rising above the crown, and let the three-forked roads to the Kingdom revolve."),

("smn_rit_five_times_01", "Let it be filled. Again. Again. Again. Again.^Let it be filled fivefold for every turn, simply breaking asunder with every filling."),
("smn_rit_five_times_02", "Shut. Shut. Shut. Shut. Shut.^Five perfections for each repetition.\ And now, let the filled sigils be annihilated in my stead!\ Set."),
("smn_rit_five_times_03", "Fill. Fill. Fill. Fill. Fill.^Let each be turned over five times, simply breaking asunder the fulfilled time."),


("smn_rit_second_paragraph", "Let it be declared now;\ your flesh shall serve under me, and my fate shall be with your sword.\ Submit to the beckoning of the Holy Grail.\ Answer, if you would submit to this will and this truth."),

("smn_rit_third_paragraph", "An oath shall be sworn here.\ I shall attain all virtues of all of Heaven; I shall have dominion over all evils of all of Hell."),

("smn_rit_mad_enhancement","Yet you shall serve with your eyes clouded by chaos.\ For you would be one caged in madness.\ I shall wield your chains."),
("smn_rit_assassin","Your Holy Order fallen, your Wish unheard, join me. Masked and Cloaked, Your Dagger from the shadows performs His Will."),

("smn_rit_final_paragraph", "From the Seventh Heaven, attended to by three great words of power,\ come forth from the ring of restraint, protector of the holy balance!"),

# ##### Material Registers
("material_note", "Anyone translating this! DO NOT translate materials_***. They are declarations inside the BRFs and need to be exactly the name in the BRF! Thank you for your hard work!"),
("material_camo", "camo"),
("material_petrified", "petrified"),
("material_gilded", "gilded"),
("material_mercury", "mercury"),
("material_shadow", "shadow"),
("material_frozen", "frozen"),

]
