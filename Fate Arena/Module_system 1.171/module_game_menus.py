from header_game_menus import *
from header_parties import *
from header_items import *
from header_mission_templates import *
from header_music import *
from header_terrain_types import *
from module_constants import *

# #######################################################################
#  (menu-id, menu-flags, menu_text, mesh-name, [<operations>], [<options>]),
#
#   Each game menu is a tuple that contains the following fields:
#  
#  1) Game-menu id (string): used for referencing game-menus in other files.
#     The prefix menu_ is automatically added before each game-menu-id
#
#  2) Game-menu flags (int). See header_game_menus.py for a list of available flags.
#     You can also specify menu text color here, with the menu_text_color macro
#  3) Game-menu text (string).
#  4) mesh-name (string). Not currently used. Must be the string "none"
#  5) Operations block (list). A list of operations. See header_operations.py for reference.
#     The operations block is executed when the game menu is activated.
#  6) List of Menu options (List).
#     Each menu-option record is a tuple containing the following fields:
#   6.1) Menu-option-id (string) used for referencing game-menus in other files.
#        The prefix mno_ is automatically added before each menu-option.
#   6.2) Conditions block (list). This must be a valid operation block. See header_operations.py for reference. 
#        The conditions are executed for each menu option to decide whether the option will be shown to the player or not.
#   6.3) Menu-option text (string).
#   6.4) Consequences block (list). This must be a valid operation block. See header_operations.py for reference. 
#        The consequences are executed for the menu option that has been selected by the player.
#
#
# Note: The first Menu is the initial character creation menu.
# #######################################################################

# #######################################################################
#	I also found no reference to which menu items were hardcoded 
#	in the Native file, so I can't tell you if these are ~truly~ hardcoded 
#	or if they are just referenced somewhere else that got overlooked.
#
#
#		TODO:
#			Syntax Cleanup
#			True requirement testing.
#				
#
#		If I were to gamble, I think the only things required will 
#		be things that have a gamekey attached to them. 
# #######################################################################



game_menus = [

    # #######################################################################
	#	These are required to stay in this order
	# #######################################################################
    ("start_game_0",menu_text_color(0xFF000000)|mnf_disable_all_keys,
    "Hello, Magus. Welcome to Chaldea. You're needed to sort out a pseduo-Singularity in a dimension known as Calradia. Unlike what we're used to, this doesn't seem to be caused by the Beasts, but by an actual Holy Grail (or imitation ritual). The world itself is fairly mundane, so do not expect much in the way of a mage society. The only mages around appear to have been brought in by the Grail itself to participate in the war. Along side your War, the Kingdoms and Lords will be waging their own, so you might want to avoid local politics and focus on the Grail War. Or join up with a Kingdom and use their forces to overwhelm your opponents. - - Note: This story will change, it's meant for the FGO campaign, but adds context. - -",
    "none",
    [],
    [
     # ("continue",[],"Continue...",
       # [(jump_to_menu, "mnu_start_game_1"),
        # ]
       # ),
	   ("start_one_page",[],"Begin the Ray-Shift ",
		[(start_presentation, "prsnt_character_creation"),],
	   ),
	   
      ("go_back",[],"Go back",
       [
         (change_screen_quit),
       ]),
    ]
  ),
	
	("start_phase_2",mnf_disable_all_keys,
    "You hear about Calradia, a land torn between rival kingdoms battling each other for supremacy,\
 a haven for knights and mercenaries,  cutthroats and adventurers, all willing to risk their lives in pursuit of fortune, power, or glory...\
 In this land which holds great dangers and even greater opportunities, you believe you may leave your past behind and start a new life.\
 You feel that finally, you hold the key of your destiny in your hands, free to choose as you will,\
 and that whatever course you take, great adventures will await you. Drawn by the stories you hear about Calradia and its kingdoms, you...",
    "none",
    [],
    [
	   ###FATE Cheat GAME START SIONFEL
	  ("fatetester",[
	  #(eq, "$current_startup_quest_phase", 0),
	  ],"Fuyuki War!",
       [
         (assign, "$current_town", "p_town_fuyuki"),
         (assign, "$g_starting_town", "$current_town"),
         # (assign, "$g_journey_string", "str_journey_to_sargoth"),
		  (jump_to_menu, "mnu_fuyuki"),
		  
		  (party_add_members, "p_main_party", "trp_mage_enforcer", 45),
		 # (jump_to_menu, "mnu_start_phase_fuyuki"),
		 # (party_add_members, "p_main_party", "trp_saber", 1),
		 # (party_add_members, "p_main_party", "trp_lancer", 1),
		 # (party_add_members, "p_main_party", "trp_rider", 1),
		 # (party_add_members, "p_main_party", "trp_caster", 1),
		 # (party_add_members, "p_main_party", "trp_archer", 1),
		 # (party_add_members, "p_main_party", "trp_assassin", 1),
		 # (party_add_members, "p_main_party", "trp_berserker", 1),
		 
		 # (party_add_members, "p_main_party", "trp_saber_01", 1),
		 # (troop_set_slot, "trp_saber_01", slot_fate_master, "trp_player"), 
		 # (assign, "$g_saber", "trp_saber_01"),
		 # (party_add_members, "p_main_party", "trp_lancer_01", 1),
		 # (troop_set_slot, "trp_lancer_01", slot_fate_master, "trp_player"),
		 # (assign, "$g_saber", "trp_lancer_01"),
		 # (party_add_members, "p_main_party", "trp_rider_01", 1),
		 # (troop_set_slot, "trp_rider_01", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_caster_01", 1),
		 # (troop_set_slot, "trp_caster_01", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_archer_01", 1),
		 # (troop_set_slot, "trp_archer_01", slot_fate_master, "trp_player"),
		 # (assign, "$g_archer", "trp_archer_01"),
		 # (party_add_members, "p_main_party", "trp_assassin_01", 1),
		 # (troop_set_slot, "trp_assassin_01", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_berserker_01", 1),
		 # (troop_set_slot, "trp_berserker_01", slot_fate_master, "trp_player"),
		 
		 # (party_add_members, "p_main_party", "trp_saber_02", 1),
		 # (troop_set_slot, "trp_saber_02", slot_fate_master, "trp_player"), 
		 # (party_add_members, "p_main_party", "trp_lancer_02", 1),
		 # (troop_set_slot, "trp_saber_02", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_rider_02", 1),
		 # (troop_set_slot, "trp_saber_02", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_caster_02", 1),
		 # (troop_set_slot, "trp_saber_02", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_archer_02", 1),
		 # (troop_set_slot, "trp_archer_02", slot_fate_master, "trp_player"),
		 # (assign, "$g_archer", "trp_archer_02"),
		 # (party_add_members, "p_main_party", "trp_assassin_02", 1),
		 # (troop_set_slot, "trp_saber_02", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_berserker_02", 1),
		 # (troop_set_slot, "trp_saber_02", slot_fate_master, "trp_player"),
		 
		 # (party_add_members, "p_main_party", "trp_saber_03", 1),
		 # (troop_set_slot, "trp_saber_03", slot_fate_master, "trp_player"), 
		 # (party_add_members, "p_main_party", "trp_lancer_03", 1),
		 # (troop_set_slot, "trp_saber_03", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_rider_03", 1),
		 # (troop_set_slot, "trp_saber_03", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_caster_03", 1),
		 # (troop_set_slot, "trp_saber_03", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_archer_03", 1),
		 # (troop_set_slot, "trp_saber_03", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_assassin_03", 1),
		 # (troop_set_slot, "trp_saber_03", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_berserker_03", 1),
		 # (troop_set_slot, "trp_saber_03", slot_fate_master, "trp_player"),
		 
		 # (party_add_members, "p_main_party", "trp_saber_04", 1),
		 # (troop_set_slot, "trp_saber_04", slot_fate_master, "trp_player"), 
		 # (party_add_members, "p_main_party", "trp_lancer_04", 1),
		 # (troop_set_slot, "trp_lancer_04", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_rider_04", 1),
		 # (troop_set_slot, "trp_rider_04", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_caster_04", 1),
		 # (troop_set_slot, "trp_caster_04", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_archer_04", 1),
		 # (troop_set_slot, "trp_archer_04", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_assassin_04", 1),
		 # (troop_set_slot, "trp_assassin_04", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_berserker_04", 1),
		 # (troop_set_slot, "trp_berserker_04", slot_fate_master, "trp_player"),
		 
		 # (party_add_members, "p_main_party", "trp_saber_05", 1),
		 # (troop_set_slot, "trp_saber_05", slot_fate_master, "trp_player"), 
		 # (party_add_members, "p_main_party", "trp_lancer_05", 1),
		 # (troop_set_slot, "trp_lancer_05", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_rider_05", 1),
		 # (troop_set_slot, "trp_rider_05", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_caster_05", 1),
		 # (troop_set_slot, "trp_caster_05", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_archer_05", 1),
		 # (troop_set_slot, "trp_archer_05", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_assassin_05", 1),
		 # (troop_set_slot, "trp_assassin_05", slot_fate_master, "trp_player"),
		 # (party_add_members, "p_main_party", "trp_berserker_05", 1),
		 # (troop_set_slot, "trp_berserker_05", slot_fate_master, "trp_player"),
		 		 
				 # Prototype Heroes, not actually servants
		 # (troop_add_item, "trp_player", "itm_relic_saber",0),
		 # (troop_add_item, "trp_player", "itm_relic_lancer",0),
		 # (troop_add_item, "trp_player", "itm_relic_rider",0),
		 # (troop_add_item, "trp_player", "itm_relic_archer",0),
		 # (troop_add_item, "trp_player", "itm_relic_caster",0),
		 # (troop_add_item, "trp_player", "itm_relic_assassin",0),
		 # (troop_add_item, "trp_player", "itm_relic_berserker",0),
		 
				# Fate/ Stay Night
		 # (troop_add_item, "trp_player", "itm_relic_saber01",0),
		 # (troop_add_item, "trp_player", "itm_relic_lancer01",0),
		 # (troop_add_item, "trp_player", "itm_relic_rider01",0),
		 # (troop_add_item, "trp_player", "itm_relic_archer01",0),
		 # (troop_add_item, "trp_player", "itm_relic_caster01",0),
		 # (troop_add_item, "trp_player", "itm_relic_assassin01",0),
		 # (troop_add_item, "trp_player", "itm_relic_berserker01",0),
		 
				# Fate/ Zero, mostly
		 # (troop_add_item, "trp_player", "itm_relic_saber02",0),
		 # (troop_add_item, "trp_player", "itm_relic_lancer02",0),
		 # (troop_add_item, "trp_player", "itm_relic_rider02",0),
		 # (troop_add_item, "trp_player", "itm_relic_archer02",0),
		 # (troop_add_item, "trp_player", "itm_relic_caster02",0),
		 # (troop_add_item, "trp_player", "itm_relic_assassin02",0),
		 # (troop_add_item, "trp_player", "itm_relic_berserker02",0),
		 
				#Fate/ Apocrypha? Maybe
		 # (troop_add_item, "trp_player", "itm_relic_saber03",0),
		 # (troop_add_item, "trp_player", "itm_relic_lancer03",0),
		 # (troop_add_item, "trp_player", "itm_relic_rider03",0),
		 # (troop_add_item, "trp_player", "itm_relic_archer03",0),
		 # (troop_add_item, "trp_player", "itm_relic_caster03",0),
		 # (troop_add_item, "trp_player", "itm_relic_assassin03",0),
		 # (troop_add_item, "trp_player", "itm_relic_berserker03",0),
		 
		 # (troop_add_item, "trp_player", "itm_relic_saber04",0),
		 # (troop_add_item, "trp_player", "itm_relic_lancer04",0),
		 # (troop_add_item, "trp_player", "itm_relic_rider04",0),
		 # (troop_add_item, "trp_player", "itm_relic_archer04",0),
		 # (troop_add_item, "trp_player", "itm_relic_caster04",0),
		 # (troop_add_item, "trp_player", "itm_relic_assassin04",0),
		 # (troop_add_item, "trp_player", "itm_relic_berserker04",0),
		 
		 # (troop_add_item, "trp_player", "itm_relic_saber05",0),
		 # (troop_add_item, "trp_player", "itm_relic_lancer05",0),
		 # (troop_add_item, "trp_player", "itm_relic_rider05",0),
		 # (troop_add_item, "trp_player", "itm_relic_archer05",0),
		 # (troop_add_item, "trp_player", "itm_relic_caster05",0),
		 # (troop_add_item, "trp_player", "itm_relic_assassin05",0),
		 # (troop_add_item, "trp_player", "itm_relic_berserker05",0),
		 
		 # (troop_add_item, "trp_player", "itm_excalibur",0),
		 #  (troop_add_item, "trp_player", "itm_excalibur_invis",0),
		 (troop_add_item, "trp_player", "itm_excalibur_gold",0),
		 # (troop_add_item, "trp_player", "itm_throwing_daggers_assassin",0),
		 (troop_add_item, "trp_player", "itm_kanshou",0),
		 (troop_add_item, "trp_player", "itm_bakuya",0),
		 # (troop_add_item, "trp_player", "itm_durindana",0),
		  (troop_add_item, "trp_player", "itm_yew_bow", 0),
		  (troop_add_item, "trp_player", "itm_yew_bolts", 0),
		 # (troop_add_item, "trp_player", "itm_aestus_estus",0),
		  (troop_add_item, "trp_player", "itm_caladbolg",0),
		  (troop_add_item, "trp_player", "itm_gae_bolg",0),
		  (troop_add_item, "trp_player", "itm_gae_bolg_thrown",0),		  
		 # (troop_add_item, "trp_player", "itm_gae_dearg",0),	
		 # (troop_add_item, "trp_player", "itm_gae_buidhe",0),
		 # (troop_add_item, "trp_player", "itm_hrunting",0),	
		 # (troop_add_item, "trp_player", "itm_emiya_bow",0),
		 # (troop_add_item, "trp_player", "itm_skull_mask_with_hood",0),
		  (troop_add_item, "trp_player", "itm_gate_baby",0),
		  (troop_add_item, "trp_player", "itm_atalanta_arrows",0),
		  (troop_add_item, "trp_player", "itm_tauropolos",0),
		  (troop_add_item, "trp_player", "itm_stella_arrow",0),
		 # (troop_add_item, "trp_player", "itm_bridal_chest",0),
		 # (troop_add_item, "trp_player", "itm_axe_sword", 0),
		 
		 
		 
		 # (troop_add_item, "trp_player", "itm_nagimaki",0),
		 
		 
		 (troop_raise_proficiency_linear, "trp_player", wpt_one_handed_weapon, 4800),
         (troop_raise_proficiency_linear, "trp_player", wpt_two_handed_weapon, 4800),
         (troop_raise_proficiency_linear, "trp_player", wpt_polearm, 4800),
         (troop_raise_proficiency_linear, "trp_player", wpt_crossbow, 4800),
         (troop_raise_proficiency_linear, "trp_player", wpt_archery, 4800), 
		 (troop_raise_proficiency_linear, "trp_player", wpt_throwing, 4800),
		 
		 (troop_add_gold, "trp_player", 100000),
		 (troop_raise_skill, "trp_player", skl_athletics, 10),
		 (troop_raise_skill, "trp_player", skl_engineer, 10),
		 (troop_raise_skill, "trp_player", skl_first_aid, 10),
		 (troop_raise_skill, "trp_player", skl_horse_archery, 10),
		 (troop_raise_skill, "trp_player", skl_ironflesh, 10),
		 (troop_raise_skill, "trp_player", skl_looting, 10),
		 (troop_raise_skill, "trp_player", skl_pathfinding, 10),
		 (troop_raise_skill, "trp_player", skl_persuasion, 10),
		 (troop_raise_skill, "trp_player", skl_power_draw, 10),
		 (troop_raise_skill, "trp_player", skl_power_strike, 10),
		 (troop_raise_skill, "trp_player", skl_power_throw, 10),
		 (troop_raise_skill, "trp_player", skl_riding, 10),
         (troop_raise_skill, "trp_player", skl_leadership, 10),
         (troop_raise_skill, "trp_player", skl_prisoner_management, 10),
         (troop_raise_skill, "trp_player", skl_inventory_management, 10),
		 (troop_raise_skill, "trp_player", skl_trade, 10),
		 (troop_raise_skill, "trp_player", skl_surgery, 10),
		 (troop_raise_skill, "trp_player", skl_wound_treatment, 10),
		 (troop_raise_skill, "trp_player", skl_spotting, 10),
		 (troop_raise_skill, "trp_player", skl_tactics, 10),
		 (troop_raise_skill, "trp_player", skl_tracking, 10),
         (troop_raise_skill, "trp_player", skl_trainer, 10),
         (troop_raise_skill, "trp_player", skl_shield, 10),
         (troop_raise_skill, "trp_player", skl_weapon_master, 10),

		 
		 (troop_raise_attribute, "trp_player", ca_strength, 1000),
         (troop_raise_attribute, "trp_player", ca_agility, 1000),
         (troop_raise_attribute, "trp_player", ca_charisma, 1000),
		 (troop_raise_attribute, "trp_player", ca_intelligence, 1000),
		 
		 (troop_set_slot, "trp_player", fate_magic_projection_level, 10),
		 (troop_set_slot, "trp_player", fate_magic_reinforcement_level, 10),
		 (troop_set_slot, "trp_player", fate_magic_familiar_level, 10),
		 (troop_set_slot, "trp_player", fate_magic_heal_level, 10),
		 (troop_set_slot, "trp_player", fate_magic_sacrament_level, 10),
		 (troop_set_slot, "trp_player", fate_magic_rune_level, 10),
		 (troop_set_slot, "trp_player", fate_magic_necro_level, 10),
		 (troop_set_slot, "trp_player", fate_magic_fire_level, 10),
		 (troop_set_slot, "trp_player", fate_magic_earth_level, 10),
		 (troop_set_slot, "trp_player", fate_magic_wind_level, 10),
		 (troop_set_slot, "trp_player", fate_magic_special_level, 10),
		 
		# (call_script, "script_get_player_party_morale_values"),
        # (party_set_morale, "p_main_party", reg0),
		# (party_relocate_near_party, "p_main_party", "$g_starting_town", 2),
         # (change_screen_return),
       ]),
    ]
  ),
 
   ("start_game_1",menu_text_color(0xFF000000)|mnf_disable_all_keys,
    "Select your character's gender.",
    "none",
    [],
    [
	   # ###Fate Test SIONFEL
	   ("start_one_page",[],"Enter the Nasu-verse",
	   [(start_presentation, "prsnt_character_creation"),],
	   ),
	  ("go_back",[],"Go back",
       [
	     (jump_to_menu,"mnu_start_game_0"),
       ]),
    ]
  ),

 
  (
    "start_game_3",mnf_disable_all_keys,
    "Choose your scenario:",
    "none",
    [],
    [
      ("go_back",[],"Go back",
      [(change_screen_quit),]
	  ),
    ]
  ),
	
	("tutorial", mnf_disable_all_keys,
	"Tutorial",
	"none",
	[],
	[
	    ("return", [], "Return",
		[
		    (change_screen_quit),
		]),
	]),

    ("reports", 0,
    "Reports",
    "none",
    [],
    [
        ("resume_travelling", [], "Resume travelling.",
        [
		    (change_screen_return),
        ]),
    ]),

    ("camp", mnf_disable_all_keys,
    "Camp",
    "none",
    [],
    [
        ("camp_wait_here", [], "Rest.",
        [
            (rest_for_hours_interactive, 24 * 365, 5, 1),
            (change_screen_return),
        ]),

	   # ################### SIONFEL FOR FATE FIND THIS LATER
	   ("camp_fate_player_stats",[],"{!}Fate Player Stats",
       [
			(troop_get_slot, ":player_alignment", "$g_player_troop", slot_fate_alignment),
			(str_store_string, s10, ":player_alignment"),
			 (troop_get_slot, ":player_personality", "$g_player_troop", fate_personality_type),
			 (str_store_string, s11, ":player_personality"),
			 (troop_get_slot, ":player_parents", "$g_player_troop", fate_parents_bg),
			 (str_store_string, s12, ":player_parents"),
			 (troop_get_slot, ":player_background", "$g_player_troop", fate_plyr_background),
			 (str_store_string, s13, ":player_background"),
			 (troop_get_slot, ":player_education", "$g_player_troop", fate_education),
			 (str_store_string, s14, ":player_education"),
			 (troop_get_slot, ":player_training", "$g_player_troop", fate_training),
			 (str_store_string, s15, ":player_training"),
			 (troop_get_slot, ":player_mana", "$g_player_troop", slot_fate_max_mana),
			 (assign, reg10, ":player_mana"),
			 (troop_get_slot, ":player_classification", "$g_player_troop", slot_fate_classification),
			 (str_store_string, s16, ":player_classification"),
			 (display_log_message, "@DEBUG: player has {s10} alignment"),
			 (display_log_message, "@DEBUG: player has {s11} personality"),
			 (display_log_message, "@DEBUG: player has {s12} parents"),
			 (display_log_message, "@DEBUG: player has {s13} background"),
			 (display_log_message, "@DEBUG: player has {s14} education"),
			 (display_log_message, "@DEBUG: player has {s15} training"),
			 (display_log_message, "@DEBUG: player has {reg10} mana"),
			 (display_log_message, "@DEBUG: player is of {s16} classification"),
         (jump_to_menu, "mnu_camp"),
        ]
       ),
	   ("camp_fate_spawn_masters",[],"{!}Fate Spawn Enemy Masters!",
       [
			(call_script, "script_cf_fate_spawn_enemy_master_parties"),
			(jump_to_menu, "mnu_camp"),
        ]
       ),
	   ("camp_fate_gimme",[],"{!}Gimme just like all the doods!",
       [
		 (party_add_members, "p_main_party", "trp_saber_01", 1),
		 (troop_set_slot, "trp_saber_01", slot_fate_master, "trp_player"), 
		 (assign, "$g_saber", "trp_saber_01"),
		 (party_add_members, "p_main_party", "trp_lancer_01", 1),
		 (troop_set_slot, "trp_lancer_01", slot_fate_master, "trp_player"),
		 (assign, "$g_saber", "trp_lancer_01"),
		 (party_add_members, "p_main_party", "trp_rider_01", 1),
		 (troop_set_slot, "trp_rider_01", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_caster_01", 1),
		 (troop_set_slot, "trp_caster_01", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_archer_01", 1),
		 (troop_set_slot, "trp_archer_01", slot_fate_master, "trp_player"),
		 (assign, "$g_archer", "trp_archer_01"),
		 (party_add_members, "p_main_party", "trp_assassin_01", 1),
		 (troop_set_slot, "trp_assassin_01", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_berserker_01", 1),
		 (troop_set_slot, "trp_berserker_01", slot_fate_master, "trp_player"),
		 
		 (party_add_members, "p_main_party", "trp_saber_02", 1),
		 (troop_set_slot, "trp_saber_02", slot_fate_master, "trp_player"), 
		 (party_add_members, "p_main_party", "trp_lancer_02", 1),
		 (troop_set_slot, "trp_saber_02", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_rider_02", 1),
		 (troop_set_slot, "trp_saber_02", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_caster_02", 1),
		 (troop_set_slot, "trp_saber_02", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_archer_02", 1),
		 (troop_set_slot, "trp_archer_02", slot_fate_master, "trp_player"),
		 (assign, "$g_archer", "trp_archer_02"),
		 (party_add_members, "p_main_party", "trp_assassin_02", 1),
		 (troop_set_slot, "trp_saber_02", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_berserker_02", 1),
		 (troop_set_slot, "trp_saber_02", slot_fate_master, "trp_player"),
		 
		 (party_add_members, "p_main_party", "trp_saber_03", 1),
		 (troop_set_slot, "trp_saber_03", slot_fate_master, "trp_player"), 
		 (party_add_members, "p_main_party", "trp_lancer_03", 1),
		 (troop_set_slot, "trp_saber_03", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_rider_03", 1),
		 (troop_set_slot, "trp_saber_03", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_caster_03", 1),
		 (troop_set_slot, "trp_saber_03", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_archer_03", 1),
		 (troop_set_slot, "trp_saber_03", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_assassin_03", 1),
		 (troop_set_slot, "trp_saber_03", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_berserker_03", 1),
		 (troop_set_slot, "trp_saber_03", slot_fate_master, "trp_player"),
		 
		 (party_add_members, "p_main_party", "trp_saber_04", 1),
		 (troop_set_slot, "trp_saber_04", slot_fate_master, "trp_player"), 
		 (party_add_members, "p_main_party", "trp_lancer_04", 1),
		 (troop_set_slot, "trp_lancer_04", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_rider_04", 1),
		 (troop_set_slot, "trp_rider_04", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_caster_04", 1),
		 (troop_set_slot, "trp_caster_04", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_archer_04", 1),
		 (troop_set_slot, "trp_archer_04", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_assassin_04", 1),
		 (troop_set_slot, "trp_assassin_04", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_berserker_04", 1),
		 (troop_set_slot, "trp_berserker_04", slot_fate_master, "trp_player"),
		 
		 (party_add_members, "p_main_party", "trp_saber_05", 1),
		 (troop_set_slot, "trp_saber_05", slot_fate_master, "trp_player"), 
		 (party_add_members, "p_main_party", "trp_lancer_05", 1),
		 (troop_set_slot, "trp_lancer_05", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_rider_05", 1),
		 (troop_set_slot, "trp_rider_05", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_caster_05", 1),
		 (troop_set_slot, "trp_caster_05", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_archer_05", 1),
		 (troop_set_slot, "trp_archer_05", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_assassin_05", 1),
		 (troop_set_slot, "trp_assassin_05", slot_fate_master, "trp_player"),
		 (party_add_members, "p_main_party", "trp_berserker_05", 1),
		 (troop_set_slot, "trp_berserker_05", slot_fate_master, "trp_player"),
			(jump_to_menu, "mnu_camp"),
        ]
       ),
	   ("camp_fate_item_list",[],"{!}Fate: Summoning Ritual",
			[(start_presentation, "prsnt_fate_summoning"),],
	   ),
	   ("camp_fate_servant_diary",[],"{!}Fate: Servant Diary",
			[(start_presentation, "prsnt_servant_diary"),],
	   ),
	   ("camp_fate_cooking_menu",[],"{!}Fate: Cooking",
			[(start_presentation, "prsnt_fate_cooking"),],
	   ),
		("camp_fate_custom_battle_menu",[],"{!}Fate: Quick Battle",
			[(start_presentation, "prsnt_replacement_custom_battle"),],
	   ),	   
	   # ("camp_fate_servant_diary",[],"{!}Fate: Choose Spell Slots",
			# [(jump_to_menu, "mnu_spellbook"),],
	   # ),
	   
	   #####################################################
		
		
        ("resume_travelling", [], "Dismantle camp.",
        [
            (change_screen_return),
        ]),
    ]),
	
	# #######################################################################
	#	Now is your time to shine!
	# #######################################################################
	
# #####################################################################
# ### FATE SABER RIDER MENUS
# #####################################################################
#Fate Debug Menu 
  
#Player Creation
#Parents
#Backgrounds
#Education
#Training
#Personality
# IF THE PLAYER MAKES CHOICES THAT LINE UP 100% WITH A REAL CANON MASTERS
# THEY WILL PLAY THEM INSTEAD OF THE CUSTOM CHARACTERS
  
  
#TODO Summoning Ritual
#Check players MANA LEVEL, INT, PERSONALITY, CATALYSTS, OTHER SUMMONED SERVANTS,

#TODO Seven Servant List Thingy
#Base the servant pages on the VN. Basically encyclopedia pages that unlock inform
#as you play. Check for things such as seeing the servant, have you fought the servant
#who the master is, what the NP is, stats, class, and maybe previous dialog I'll figure it out 

# ########################################
# #### Scene menus for transversal #######
# ########################################

(
    "fuyuki",mnf_auto_enter,
    "You enter the town of Fuyuki.",
    "none", [],
    [
      ("fuyuki_1",[],"Residential Area - Buke Quarter",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_1"),
	  ],"Residential Area - Buke Quarter"),
	  
	  ("fuyuki_2",[],"Residential Area - Foreign Quarter",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_2"),
	  ],"Residential Area - Foreign Quarter"),
	  
	  ("fuyuki_3",[],"Residential Area - Foreign Quarter",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_3"),
	  ],"Residential Area - Foreign Quarter"),
	  
	  ("fuyuki_4",[],"Homurahara Private Academy",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_4"),
	  ],"Homurahara Private Academy"),
	  
	  ("fuyuki_5",[],"Miyama Crossing - Crossroads",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_5"),
	  ],"Miyama Crossing - Crossroads"),
	  
	  ("fuyuki_6",[],"Miyama Downtown - Mount Miyama Shopping District",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_6"),
	  ],"Miyama Downtown - Mount Miyama Shopping District"),
	  
	  ("fuyuki_7",[],"Ryuudou Temple",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_7"),
	  ],"Ryuudou Temple"),
	  
	  ("fuyuki_8",[],"Mount Enzou",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_8"),
	  ],"Mount Enzou"),
	  
	  ("fuyuki_9",[],"Mion River - Fuyuki Bridge",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_9"),
	  ],"Mion River - Fuyuki Bridge"),
	  
	  ("fuyuki_10",[],"Mion River - Oceanside Park",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_10"),
	  ],"Mion River - Oceanside Park"),
	  
	  ("fuyuki_11",[],"Shinto Financial District - Fuyuki Station-Front Park",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_11"),
	  ],"Shinto Financial District - Fuyuki Station-Front Park"),
	  
	  ("fuyuki_12",[],"Shinto Financial District - Fuyuki Center Building",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_12"),
	  ],"Shinto Financial District - Fuyuki Center Building"),
	  
	  ("fuyuki_13",[],"Fuyuki Central Park",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_13"),
	  ],"Fuyuki Central Park"),
	  
	  ("fuyuki_14",[],"Fuyuki Church - Foreigner's Cemetary",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_14"),
	  ],"Fuyuki Church - Foreigner's Cemetary"),
	  
	  ("fuyuki_15",[],"Fuyuki Church - Chapel & Rectory",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_15"),
	  ],"Fuyuki Church - Chapel & Rectory"),
	  
	  ("fuyuki_16",[],"Industrial Park - Dockyard",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_16"),
	  ],"Industrial Park - Dockyard"),
	  
	  ("fuyuki_17",[],"Industrial Park - Factory District",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_17"),
	  ],"Industrial Park - Factory District"),
	  
	  ("fuyuki_18",[],"Miyama Suburb - Einzbern Forest",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_18"),
	  ],"Miyama Suburb - Einzbern Forest"),
	  
	  ("fuyuki_19",[],"Miyama Suburb - Einzbern Castle",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_19"),
	  ],"Miyama Suburb - Einzbern Castle"),
	  
	  ("fuyuki_20",[],"Not Sure Yet",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_20"),
	  ],"Not Sure Yet"),
	# ##### Interior Scenes
	  ("fuyuki_int_emiya_house",[],"Enter: Emiya Residence",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fate_emiya_house"),
	  ],"Enter: Emiya Residence"),
	  
	  ("fuyuki_int_tohsaka_house",[],"Enter: Tohsaka Residence",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fate_tohsaka_house"),
	  ],"Enter: Tohsaka Residence"),
	  
	  ("fuyuki_int_matou_house",[],"Enter: Matou Residence",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fate_matou_house"),
	  ],"Enter: Matou Residence"),
	  
	  ("fuyuki_int_mackenzie_house",[],"Enter: Mackenzie Residence",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fate_mackenzie_house"),
	  ],"Enter: Mackenzie Residence"),
	  
	  ("fuyuki_int_school",[],"Enter: Homurahara Academy",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fate_homurahara_academy"),
	  ],"Enter: Homurahara Academy"),
	  
	  ("fuyuki_int_church",[],"Enter: Fuyuki Church",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fate_fuyuki_church"),
	  ],"Enter: Fuyuki Church"),
	  
	  ("fuyuki_int_chinese_restaurant",[],"Enter: Koushuuensaikan Taizan",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fate_chinese_restaurant"),
	  ],"Enter: Koushuuensaikan Taizan"),
	  
	  ("fuyuki_int_antique_store",[],"Enter: Singing Birds Retreat",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fate_singing_birds"),
	  ],"Enter: Singing Birds Retreat"),
	  
	  ("fuyuki_int_temple",[],"Enter: Ryuudou Temple",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fate_ryuudou_temple"),
	  ],"Enter: Ryuudou Temple"),
	  
	  ("fuyuki_int_grail_cave",[],"Enter: Ten no Sakazuki",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fate_grail_cavern"),
	  ],"Enter: Ten no Sakazuki"),
	  
	  ("fuyuki_int_liquor_store",[],"Enter: Copenhagen",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fate_copenhagen"),
	  ],"Enter: Copenhagen"),
	  
	  ("fuyuki_int_mall",[],"Enter: Verde Mall",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fate_verde"),
	  ],"Enter: Verde Mall"),
	  
	  ("fuyuki_int_library",[],"Enter: Fuyuki Central Library",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fate_fuyuki_library"),
	  ],"Enter: Fuyuki Central Library"),
	  
	  ("fuyuki_int_water_park",[],"Enter: Exciting Splash Water Park",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fate_exciting_splash"),
	  ],"Enter: Exciting Splash Water Park"),
	  
	  ("fuyuki_int_coffee_shop",[],"Enter: Ahnenerbe Coffee Shop",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fate_ahnenerbe"),
	  ],"Enter: Ahnenerbe Coffee Shop"),
	  
	  ("fuyuki_int_einzbern_castle",[],"Enter: Einzbern Castle",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fate_einzbern_castle"),
	  ],"Enter: Einzbern Castle"),
	  
    ]
  ),
  
(
    "church", mnf_auto_enter,
    "You have stumbled upon a Church",
    "none", [],
    [
	   
	  ("church_1",[],"Fuyuki Church - Chapel & Rectory",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_15"),
	  ],"Fuyuki Church - Chapel & Rectory"),
	  
	  ("church_2",[],"Fuyuki Church - Foreigner's Cemetary",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fuyuki_14"),
	  ],"Fuyuki Church - Foreigner's Cemetary"),
	  
	  ("church_interior",[],"Enter: Fuyuki Church",
	  [
	  (call_script, "script_fate_fuyuki_town_scripts", "scn_fate_fuyuki_church"),
	  ],"Enter: Fuyuki Church"),	  
    ]
  ),

# # Character Creation # #
(
    "banner_selection_for_noble",mnf_disable_all_keys,
    "You join the world of the NASUverse", 
    "none", [
   # (assign,"$current_string_reg",10),
	 # (assign, ":difficulty", 0),
	 
	 # (try_begin),
		# (eq, "$character_gender", tf_female),
		# (str_store_string, s14, "str_woman"),
		# (val_add, ":difficulty", 1),
	 # (else_try),	
		# (str_store_string, s14, "str_man"),
	 # (try_end),
	
	 # (try_begin),
        # (eq,"$background_type",cb_noble),
		# (str_store_string, s15, "str_noble"),
		# (val_sub, ":difficulty", 1),
	 # (else_try),
		# (str_store_string, s15, "str_common"),
	 # (try_end),
	 
	 # # (try_begin),
		# # (eq, ":difficulty", -1),
		# # (str_store_string, s16, "str_may_find_that_you_are_able_to_take_your_place_among_calradias_great_lords_relatively_quickly"),
	 # # (else_try),
		# # (eq, ":difficulty", 0),
		# (str_store_string, s16, "@Let's Get Magic Nonsense a-goin'"),
	 # # (else_try),
		# # (eq, ":difficulty", 1),
		# # (str_store_string, s16, "str_may_face_great_difficulties_establishing_yourself_as_an_equal_among_calradias_great_lords"),
	 # # (try_end),
    ],
    [
      ("continue",[],"Continue...",
        [
           (call_script, "script_fate_start_assign_skills"), ## CC
           # (try_begin),
             # (eq, "$background_type", cb_noble),
             (jump_to_menu, "mnu_auto_return"),
# #normal_banner_begin
             # (start_presentation, "prsnt_banner_selection"),
           # (else_try),
             (change_screen_return, 0),
          # (try_end),
        ]),
    ]
  ),
# CC 
 # (menu-id, menu-flags, menu_text, mesh-name, [<operations>], [<options>]),
 ("spellbook", mnf_disable_all_keys, 
 "Assign your combat spell slots.", 
 "none", [ ], 
 [
	("continue",[],"Continue...", []),
 ]),
 
 ("fate_battle_prepare",0,
   "Prepare to duel!",
   "none",
   [],
   [
     ("continue",[],"Continue...",
      [
        (assign, "$g_leave_encounter", 0),
        
			(call_script, "script_fate_quick_battle_setup", "$g_fate_battle_map_selected"),
			
			# (modify_visitors_at_site, "scn_town_1_arena"),
			# (reset_visitors),
			# (set_visitor, 0, "$g_fate_battle_master_1"),
			# (set_visitor, 1, "$g_fate_battle_servant_1"),
			# (set_visitor, 2, "$g_fate_battle_master_2"),
			# (set_visitor, 3, "$g_fate_battle_servant_2"),
			# (set_jump_mission, "mt_fate_quick_battle"),
			# (jump_to_scene, "scn_town_1_arena"),
			# (jump_to_menu, "mnu_arena_duel_conclusion"),
			# (change_screen_mission), 
			# (presentation_set_duration, 0),      
      ]),
    ]
  ),
  
  (
    "fate_battle_end", menu_text_color(0xFF000000)|mnf_disable_all_keys,
    "Thank you for playing!",
    "none",
    [
		(music_set_situation, 0),
    ],
    [
		
	  ("rematch",[],"Rematch.",
       [(jump_to_menu, "mnu_fate_battle_prepare"),
        ]
       ),
	   
	   ("selection", [], "Character Selection",
	   [
	   (assign, "$g_fate_exit", 0),
	   (start_presentation, "prsnt_replacement_custom_battle"),]
	   ),
	
      ("continue",[],"Exit to Main Screen.",
       [(change_screen_quit),
        ]
       ),
    ]
  ),
  
  (
    "auto_return",0,
    "{!}This menu automatically returns to caller.",
    "none",
    [(change_screen_return, 0)],
    [
    ]
  ),
	
 ]
