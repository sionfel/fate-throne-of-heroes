from header_particle_systems import *

# #######################################################################
#   Each particle system contains the following fields:
#  
#  1) Particle system id (string): used for referencing particle systems in other files.
#     The prefix psys_ is automatically added before each particle system id.
#  2) Particle system flags (int). See header_particle_systems.py for a list of available flags
#  3) mesh-name.
# ###
#  4) Num particles per second:    Number of particles emitted per second.
#  5) Particle Life:    Each particle lives this long (in seconds).
#  6) Damping:          How much particle's speed is lost due to friction.
#  7) Gravity strength: Effect of gravity. (Negative values make the particles float upwards.)
#  8) Turbulance size:  Size of random turbulance (in meters)
#  9) Turbulance strength: How much a particle is affected by turbulance.
# ###
# 10,11) Alpha keys :    Each attribute is controlled by two keys and 
# 12,13) Red keys   :    each key has two fields: (time, magnitude)
# 14,15) Green keys :    For example scale key (0.3,0.6) means 
# 16,17) Blue keys  :    scale of each particle will be 0.6 at the
# 18,19) Scale keys :    time 0.3 (where time=0 means creation and time=1 means end of the particle)
#
# The magnitudes are interpolated in between the two keys and remain constant beyond the keys.
# Except the alpha always starts from 0 at time 0.
# ###
# 20) Emit Box Size :   The dimension of the box particles are emitted from.
# 21) Emit velocity :   Particles are initially shot with this velocity.
# 22) Emit dir randomness
# 23) Particle rotation speed: Particles start to rotate with this (angular) speed (degrees per second).
# 24) Particle rotation damping: How quickly particles stop their rotation
# #######################################################################

particle_systems = [
  
    # #######################################################################
    #	These particles are all brought into existance by engine features
	#		and thus should be here.
	# #######################################################################
    ("game_rain", psf_billboard_2d|psf_global_emit_dir|psf_always_emit, "prtcl_rain",
     500, 0.5, 0.33, 1.0, 10.0, 0.0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (1.0, 0.3), (1, 0.3),        #alpha keys
     (1.0, 1.0), (1, 1.0),      #red keys
     (1.0, 1.0), (1, 1.0),      #green keys
     (1.0, 1.0), (1, 1.0),      #blue keys
     (1.0, 1.0),   (1.0, 1.0),   #scale keys
     (8.2, 8.2, 0.2),           #emit box size
     (0, 0, -10.0),               #emit velocity
     0.0,                       #emit dir randomness
     0,                       #rotation speed
     0.5                        #rotation damping
    ),
	
    ("game_snow", psf_billboard_3d|psf_global_emit_dir|psf_always_emit|psf_randomize_size|psf_randomize_rotation, "prt_mesh_snow_fall_1",
     150, 2, 0.2, 0.1, 30, 20,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.2, 1), (1, 1),        #alpha keys
     (1.0, 1.0), (1, 1.0),      #red keys
     (1.0, 1.0), (1, 1.0),      #green keys
     (1.0, 1.0), (1, 1.0),      #blue keys
     (1.0, 1.0),   (1.0, 1.0),   #scale keys
     (10, 10, 0.5),           #emit box size
     (0, 0, -5.0),               #emit velocity
     1,                       #emit dir randomness
     200,                       #rotation speed
     0.5                        #rotation damping
    ),
	
    ("game_blood", psf_billboard_3d |psf_randomize_size|psf_randomize_rotation,  "prt_mesh_blood_1",
     500, 0.65, 3, 0.5, 0, 0,        #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, 0.7), (0.7, 0.7),          #alpha keys
     (0.1, 0.7), (1, 0.7),      #red keys
     (0.1, 0.7), (1, 0.7),       #green keys
     (0.1, 0.7), (1, 0.7),      #blue keys
     (0.0, 0.015),   (1, 0.018),  #scale keys
     (0, 0.05, 0),               #emit box size
     (0, 1.0, 0.3),                #emit velocity
     0.9,                       #emit dir randomness
     0,                         #rotation speed
     0,                         #rotation damping
    ),
    ("game_blood_2", psf_billboard_3d | psf_randomize_size|psf_randomize_rotation ,  "prt_mesh_blood_3",
     2000, 0.6, 3, 0.3, 0, 0,        #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, 0.25), (0.7, 0.1),        #alpha keys
     (0.1, 0.7), (1, 0.7),      #red keys
     (0.1, 0.7), (1, 0.7),       #green keys
     (0.1, 0.7), (1, 0.7),      #blue keys
     (0.0, 0.15),   (1, 0.35),    #scale keys
     (0.01, 0.2, 0.01),             #emit box size
     (0.2, 0.3, 0),                 #emit velocity
     0.3,                         #emit dir randomness
     150,                       #rotation speed
     0,                       #rotation damping
     ),
    
     ("game_hoof_dust", psf_billboard_3d|psf_randomize_size|psf_randomize_rotation|psf_2d_turbulance, "prt_mesh_dust_1",#prt_mesh_dust_1
     5, 2.0,  10, 0.05, 10.0, 39.0, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.2, 0.5), (1, 0.0),        #alpha keys
     (0, 1), (1, 1),        #red keys
     (0, 0.9),(1, 0.9),         #green keys
     (0, 0.78),(1, 0.78),         #blue keys
     (0.0, 2.0),   (1.0, 3.5),   #scale keys
     (0.2, 0.3, 0.2),           #emit box size
     (0, 0, 3.9),                 #emit velocity
     0.5,                         #emit dir randomness
     130,                       #rotation speed
     0.5                        #rotation damping
    ),

    ("game_hoof_dust_snow", psf_billboard_3d|psf_randomize_size, "prt_mesh_snow_dust_1",#prt_mesh_dust_1
     6, 2, 3.5, 1, 10.0, 0.0, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.2, 1), (1, 1),        #alpha keys
     (0, 1), (1, 1),        #red keys
     (0, 1),(1, 1),         #green keys
     (0, 1),(1, 1),         #blue keys
     (0.5, 4),   (1.0, 5.7),   #scale keys
     (0.2, 1, 0.1),           #emit box size
     (0, 0, 1),                 #emit velocity
     2,                         #emit dir randomness
     0,                       #rotation speed
     0                        #rotation damping
    ),
     ("game_hoof_dust_mud", psf_billboard_2d|psf_randomize_size|psf_randomize_rotation|psf_2d_turbulance, "prt_mesh_mud_1",#prt_mesh_dust_1
     5, .7,  10, 3, 0, 0, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0, 1), (1, 1),        #alpha keys
     (0, .7), (1, .7),        #red keys
     (0, 0.6),(1, 0.6),         #green keys
     (0, 0.4),(1, 0.4),         #blue keys
     (0.0, 0.2),   (1.0, 0.22),   #scale keys
     (0.15, 0.5, 0.1),           #emit box size
     (0, 0, 15),                 #emit velocity
     6,                         #emit dir randomness
     200,                       #rotation speed
     0.5                        #rotation damping
    ),

    ("game_water_splash_1", psf_billboard_3d|psf_randomize_size|psf_randomize_rotation|psf_emit_at_water_level, "prtcl_drop",
     20, 0.85, 0.25, 0.9, 10.0, 0.0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.3, 0.5), (1, 0.0),        #alpha keys
     (1.0, 1.0), (1, 1.0),      #red keys
     (1.0, 1.0), (1, 1.0),      #green keys
     (1.0, 1.0), (1, 1.0),      #blue keys
     (0.0, 0.3),   (1.0, 0.18),   #scale keys
     (0.3, 0.2, 0.1),           #emit box size
     (0, 1.2, 2.3),               #emit velocity
     0.3,                       #emit dir randomness
     50,                       #rotation speed
     0.5                        #rotation damping
    ),
    
    ("game_water_splash_2", psf_billboard_3d|psf_randomize_size|psf_randomize_rotation|psf_emit_at_water_level, "prtcl_splash_b",
     30, 0.4, 0.7, 0.5, 10.0, 0.0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.3, 1.0), (1, 0.3),        #alpha keys
     (1.0, 1.0), (1, 1.0),      #red keys
     (1.0, 1.0), (1, 1.0),      #green keys
     (1.0, 1.0), (1, 1.0),      #blue keys
     (0.0, 0.25),   (1.0, 0.7),   #scale keys
     (0.4, 0.3, 0.1),           #emit box size
     (0, 1.3, 1.1),               #emit velocity
     0.1,                       #emit dir randomness
     50,                       #rotation speed
     0.5                        #rotation damping
    ),

    ("game_water_splash_3", psf_emit_at_water_level , "prt_mesh_water_wave_1",
     5, 2.0, 0, 0.0, 10.0, 0.0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.03, 0.2), (1, 0.0),        #alpha keys
     (1.0, 1.0), (1, 1.0),      #red keys
     (1.0, 1.0), (1, 1.0),      #green keys
     (1.0, 1.0), (1, 1.0),      #blue keys
     (0.0, 3),   (1.0, 10),   #scale keys
     (0.0, 0.0, 0.0),           #emit box size
     (0, 0, 0),                 #emit velocity
     0.0,                       #emit dir randomness
     0,                       #rotation speed
     0.5                        #rotation damping
    ),

	# #######################################################################
	#		Here be where you might add on to, matey
	# #######################################################################
	
	# #######################################################################
	# ### Fate Particles, Yes, Really
	# #######################################################################
	("lancelot_obscurement_smoke", psf_billboard_3d|psf_global_emit_dir|psf_always_emit, "prt_mesh_smoke_1",
     10, 3.0, 2.0, 0.25, 2.0, 1.0,          #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0, 0.8), (1, 1.0),       #alpha keys
     (0, 0.05), (1, 0.01),      #red keys
     (0, 0.05),(1, 0.01),       #green keys
     (0, 0.05), (1, 0.01),      #blue keys
     (0, 1.0),   (1, 0.5),      #scale keys
     (0.45, 0.45, 0.45),        #emit box size
     (0.0, 0.0, 0.25),           #emit velocity
     0.5,                      #emit dir randomness
     0.15,                      #rotation speed
     1.0                        #rotation damping
    ), 
	("invisible_air", psf_billboard_3d|psf_global_emit_dir|psf_always_emit|psf_randomize_rotation|psf_randomize_size, "prt_mesh_steam_1",
     2, 0.5, 1, -0.25, 8.0, 1,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.5, 0.1), (1, 0),        #alpha keys
     (0.0, 1), (1, 0.1),        #red keys
     (0.0, 1),(1, 0.1),         #green keys
     (0.0, 1), (1, 0.1),        #blue keys
     (0, 0.5),   (0.9, 0.75),   #scale keys
     (0.05, 0.05, 0),           #emit box size
     (0, 0, 0.1),               #emit velocity
     0,                         #emit dir randomness
     100,                       #rotation speed
     0.5,                       #rotation damping
    ),
	("yew_poison_smoke", psf_billboard_3d | psf_global_emit_dir | psf_always_emit, "prt_mesh_smoke_1",
     25, 3, 1, -0.1, 4, 4,      #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0, 0.55), (1, 0.35),            #alpha keys
     (0, 0.2), (1, 0.2),        #red keys
     (0, 0.3), (1, 0.3),        #green keys
     (0, 0.2), (1, 0.2),        #blue keys
     (0, 2.5),   (1, 4.0),    	#scale keys
     (5, 5, 0.1),               #emit box size
     (0, 0.01, -0.01),          #emit velocity
	  0.02,                     #emit dir randomness
      15,                       #rotation speed
      0,                        #rotation damping
    ),
	
	#psf_turn_to_velocity
	("trail_effect", psf_billboard_3d|psf_global_emit_dir|psf_always_emit, "trail",
     1, 0.0, 0.0, -0.0, 0.0, 0.0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, 1.0), (1.0, 1.0),        #alpha keys
     (0.0, 1.0), (1.0, 1.0),      #red keys
     (0.0, 0.0), (1.0, 0.0),      #green keys
     (0.0, 0.0), (1.0, 0.0),      #blue keys
     (0.0, 0.5), (1.0, 0.5),    #scale keys
     (0.01, 0.01, 0.0),         #emit box size
     (0, 0, 0.0),               #emit velocity
     0.00,                      #emit dir randomness
     0,                       #rotation speed
	 1,                       #rotation damping
    ),
	
	("larger_explosion", psf_billboard_3d|psf_global_emit_dir|psf_always_emit|psf_randomize_size|psf_randomize_rotation, "prt_mesh_fire_1",
     100, 2.6, 0.2, 0.5, 10.0, 0.5,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.7, 0.5), (1, 0.0),        #alpha keys
     (0.5, 1.0), (1, 0.0),      #red keys
     (0.5, 0.7), (1, 0.0),       #green keys
     (0.5, 0.2), (1, 0.0),      #blue keys
     (0.7, 1.0), (1, 0.0),   #scale keys
     (0.4, 0.2, 1.1),            #emit box size
     (0, 0, 5.0),               #emit velocity
     0.25,                       #emit dir randomness
     100,                       #rotation speed
     0.5                        #rotation damping
    ),
	("lava", psf_billboard_3d|psf_global_emit_dir|psf_always_emit|psf_randomize_size|psf_randomize_rotation, "prt_mesh_fire_1",
     100, 2.6, 0.2, 1.0, 10.0, 0.5,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.5, 0.5), (1, 1.0),        #alpha keys
     (0.5, 1.0), (1, 0.0),      #red keys
     (0.5, 0.7),(1, 0.0),       #green keys
     (0.5, 0.2), (1, 0.0),      #blue keys
     (0, 0.4),   (1, 1),   #scale keys
     (0.4, 0.2, 0.2),            #emit box size
     (0, 0, 5.0),               #emit velocity
     0.25,                       #emit dir randomness
     100,                       #rotation speed
     0.5                        #rotation damping
    ),  
	
	# Placeholder Particles for conversion
	("gourd_smoke", psf_billboard_3d|psf_randomize_size,  "prt_mesh_dust_1",
     500, 3, 15, -0.05, 10.0, 0.2,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.1, 0.5), (1, 0),        #alpha keys
     (0.1, 0.8), (1, 0.8),      #red keys
     (0.1, 0.7),(1, 0.7),       #green keys
     (0.1, 0.6), (1, 0.7),      #blue keys
     (0.0, 0.5),   (1, 1),    #scale keys
     (0.2, 0.2, 0.5),           #emit box size
     (0, 0, 0.05),               #emit velocity
     2,                         #emit dir randomness
     10,                        #rotation speed
     0.1,                       #rotation damping
    ),
	
	("fireplace_fire_small", psf_billboard_3d|psf_global_emit_dir|psf_always_emit|psf_randomize_size|psf_randomize_rotation, "prt_mesh_fire_1",
     25, 0.8, 0.2, -0.1, 10.0, 0.0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.5, 0.5), (1, 0),        #alpha keys
     (0.5, 1.0), (1, 0.9),      #red keys
     (0.5, 0.7),(1, 0.3),       #green keys
     (0.5, 0.2), (1, 0.0),      #blue keys
     (0, 0.2),   (1, 0.7),   #scale keys
     (0.2, 0.1, 0.01),      #emit box size
     (0, 0, 0.2),               #emit velocity
     0.1,                       #emit dir randomness
     100,                       #rotation speed
     0.5                        #rotation damping
    ),
    ("fireplace_fire_big", psf_billboard_3d|psf_global_emit_dir|psf_always_emit|psf_randomize_size|psf_randomize_rotation, "prt_mesh_fire_1",
     35, 0.6, 0.2, -0.2, 10.0, 0.0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.5, 0.5), (1, 0),        #alpha keys
     (0.5, 1.0), (1, 0.9),      #red keys
     (0.5, 0.7),(1, 0.3),       #green keys
     (0.5, 0.2), (1, 0.0),      #blue keys
     (0, 1),   (1, 1.5),   #scale keys
     (0.4, 0.2, 0.01),            #emit box size
     (0, 0, 0.4),               #emit velocity
     0.1,                       #emit dir randomness
     100,                       #rotation speed
     0.5                        #rotation damping
    ),
	("village_fire_big", psf_billboard_3d|psf_global_emit_dir|psf_always_emit|psf_randomize_size|psf_randomize_rotation, "prt_mesh_fire_1",
     50, 1.0, 0, -1.2, 25.0, 10.0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.2, 0.7), (1, 0),        #alpha keys
     (0.2, 1.0), (1, 0.9),      #red keys
     (0.2, 0.7),(1, 0.3),       #green keys
     (0.2, 0.2), (1, 0.0),      #blue keys
     (0, 2),   (1, 6),          #scale keys
     (2.2, 2.2, 0.2),           #emit box size
     (0, 0, 0.0),               #emit velocity
     0.0,                       #emit dir randomness
     250,                       #rotation speed
     0.3                        #rotation damping
    ),
	("dummy_smoke_big", psf_billboard_3d|psf_randomize_size,  "prt_mesh_dust_1",
     500, 9, 15, -0.05, 10.0, 0.2,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.1, 0.9), (1, 0),        #alpha keys
     (0.1, 0.8), (1, 0.8),      #red keys
     (0.1, 0.7),(1, 0.7),       #green keys
     (0.1, 0.6), (1, 0.7),      #blue keys
     (0.0, 5),   (1, 15.0),    #scale keys
     (3, 3, 5),           #emit box size
     (0, 0, 0.05),               #emit velocity
     2,                         #emit dir randomness
     10,                        #rotation speed
     0.1,                       #rotation damping
    ),
	("charge_spark", psf_billboard_3d|psf_randomize_size,  "prt_sparks_mesh_1",
     5, 3, 15, -0.05, 10.0, 0.2,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.7, 0.9), (1, 0.1),        #alpha keys
     (0.1, 1), (1, 0.8),      #red keys
     (0.1, 0.9),(1, 0.7),       #green keys
     (0.1, 0.8), (1, 0.6),      #blue keys
     (0.0, 0.25),   (1, 0.4),    #scale keys
     (3, 3, 0.25),           #emit box size
     (0, 0, -0.01),               #emit velocity
     2,                         #emit dir randomness
     10,                        #rotation speed
     0.1,                       #rotation damping
    ),
	("charge_spark_field", psf_billboard_3d|psf_randomize_size,  "prt_sparks_mesh_1",
     1, 3, 15, -0.05, 10.0, 0.2,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.7, 0.9), (1, 0.1),        #alpha keys
     (0.1, 1), (1, 0.8),      #red keys
     (0.1, 0.9),(1, 0.7),       #green keys
     (0.1, 0.8), (1, 0.6),      #blue keys
     (0.0, 0.5),   (1, 2),    #scale keys
     (15, 15, 0.25),           #emit box size
     (0, 0, -0.01),               #emit velocity
     2,                         #emit dir randomness
     10,                        #rotation speed
     0.1,                       #rotation damping
    ),
	("charge_spark_full", psf_billboard_3d|psf_randomize_size,  "prt_sparks_mesh_1",
     3, 3, 15, -0.1, 10.0, 0.2,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.7, 0.9), (1, 0.1),        #alpha keys
     (0.1, 1), (1, 0.8),      #red keys
     (0.1, 0.9),(1, 0.7),       #green keys
     (0.1, 0.8), (1, 0.6),      #blue keys
     (0.0, 0.25),   (1, 0.4),    #scale keys
     (1, 1, 0.7),           #emit box size
     (0, 0, -0.1),               #emit velocity
     2,                         #emit dir randomness
     10,                        #rotation speed
     0.1,                       #rotation damping
    ),
	("charge_beams_full", psf_billboard_3d|psf_randomize_size,  "prt_mesh_moon_beam",
     1, 1, 15, -0.5, 10.0, 0.2,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.7, 0.9), (1, 0.1),        #alpha keys
     (0.1, 1), (1, 0.8),      #red keys
     (0.1, 0.9),(1, 0.7),       #green keys
     (0.1, 0.8), (1, 0.6),      #blue keys
     (0.0, 0.5),   (1, 1),    #scale keys
     (0.25, 0.25, 0.2),           #emit box size
     (0, 0, -2),               #emit velocity
     1,                         #emit dir randomness
     10,                        #rotation speed
     0.1,                       #rotation damping
    ),
	("charge_beams_2_full", psf_billboard_3d|psf_randomize_size,  "prt_mesh_moon_beam",
     1, 3, 15, -0.75, 0.0, 0.0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.7, 0.9), (1, 0.1),        #alpha keys
     (0.1, 1), (1, 0.8),      #red keys
     (0.1, 0.9),(1, 0.7),       #green keys
     (0.1, 0.8), (1, 0.6),      #blue keys
     (0.0, 0.7),   (1, 2.5),    #scale keys
     (5, 5, 0.2),           #emit box size
     (0, 0, -3),               #emit velocity
     1,                         #emit dir randomness
     0,                        #rotation speed
     1.0,                       #rotation damping
    ),
	
	
	("pistol_smoke", psf_billboard_3d, "prtcl_dust_a",
     90, 2.5, 0.6, -0.2, 60.0, 1.5,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, 0.75), (1, 0),       #alpha keys
     (0.0, 0.7), (1, 0.4),      #red keys
     (0.0, 0.7),(1, 0.4),       #green keys
     (0.0, 0.7), (1, 0.4),      #blue keys
     (0, 1.5),   (0.5, 11.0),   #scale keys
     (0.1, 0.1, 0.1),           #emit box size
     (2, 2, 0),                 #emit velocity
     0.1                        #emit dir randomness
    ),
	
	# ("blur", psf_billboard_3d, "heat",
     # 90, 2.5, 0.6, -0.2, 60.0, 1.5,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     # (0.0, 0.75), (1, 0),       #alpha keys
     # (0.0, 0.7), (1, 0.4),      #red keys
     # (0.0, 0.7),(1, 0.4),       #green keys
     # (0.0, 0.7), (1, 0.4),      #blue keys
     # (0, 1.5),   (0.5, 11.0),   #scale keys
     # (0.1, 0.1, 0.1),           #emit box size
     # (2, 2, 0),                 #emit velocity
     # 0.1                        #emit dir randomness
    # ),
	("laser", psf_billboard_3d,  "prt_sparks_mesh_1",
     50, 0.5, 0, 0, 0, 0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, 1), (1, 1),        #alpha keys
     (0.0, 1), (1, 1),      #red keys
     (0.0, 0.0),(1, 0.0),       #green keys
     (0.0, 0.0), (1, 0.0),      #blue keys
     (0.0, 0.5),   (1, 0.5),    #scale keys
     (0, 0, 0.0),           #emit box size
     (0, 0, 0.0),               #emit velocity
     0,                         #emit dir randomness
     0,                        #rotation speed
     0,                       #rotation damping
    ),
	("impact", psf_billboard_3d,  "impact_part",
     1, 0.25, 0, 0, 0, 0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.1, 1.0), (1, 1.0),        #alpha keys
     (0.1, 0.5), (0.7, 1),      #red keys
     (0.1, 0.5), (0.7, 0.0),       #green keys
     (0.1, 0.0), (1.0, 0.0),      #blue keys
     (0.0, 1.5), (1, 1.5),    #scale keys
     (0, 0, 0.0),           #emit box size
     (0.0, 0.0, 0.0),               #emit velocity
     0,                         #emit dir randomness
     0,                        #rotation speed
     0,                       #rotation damping
    ),
	("yellowvine", psf_turn_to_velocity|psf_randomize_rotation,  "yellowvine",
     100, 0.25, 25, -0.05, 0, 0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.1, 1.0), (1, 1.0),        #alpha keys
     (0.1, 0.8), (0.7, 0.5),      #red keys
     (0.1, 0.8), (0.7, 0.5),       #green keys
     (0.1, 0.8), (0.7, 0.5),      #blue keys
     (0.0, 1.5), (1, 1.5),    #scale keys
     (0, 0, 0.0),           #emit box size
     (0.01, 0.01, 0.01),               #emit velocity
     15,                         #emit dir randomness
     100,                        #rotation speed
     10,                       #rotation damping
    ),
	("impact_burst", psf_billboard_3d,  "impact_part",
     100, 0.5, 20, 0.75, 2, 10,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, 1.0), (1, 1),        #alpha keys
     (0.0, 0.5), (0.7, 1),      #red keys
     (0.0, 0.0), (0.7, 0.0),       #green keys
     (0.0, 0.0), (0.5, 0.0),      #blue keys
     (0.0, 0.25), (1, 0.15),    #scale keys
     (0, 0, 0.0),           #emit box size
     (0.1, 0.1, 0.1),               #emit velocity
     10,                         #emit dir randomness
     100,                        #rotation speed
     15,                       #rotation damping
    ),
	("aestus_rose", psf_billboard_2d | psf_always_emit, "prt_mesh_rose_a",
     100, 8, 0.02, 0.025, 1, 5,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0, 1), (1, 1),        #alpha keys
     (0, 0.5), (1, 0.5),      #red keys
     (0, 0.5), (1, 0.5),      #green keys
     (0, 0.5), (1, 0.5),      #blue keys
     (0, 0.25),   (1, 0.25),    #scale keys
     (0.25, 0.25, 0.1),             #emit box size
     (0, 0, 0.9),              #emit velocity
      0.5,                        #emit dir randomness
      10,                        #rotation speed
      0,                         #rotation damping
    ),
	("aestus_area", psf_billboard_2d | psf_always_emit, "prt_mesh_rose_a",
     10, 8, 0.02, 0.025, 50, 15,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0, 1), (1, 1),        #alpha keys
     (0, 0.5), (1, 0.5),      #red keys
     (0, 0.5), (1, 0.5),      #green keys
     (0, 0.5), (1, 0.5),      #blue keys
     (0, 0.25),   (1, 0.25),    #scale keys
     (24.0, 24.0, 2.0),             #emit box size
     (0.2, 0.1, 0.15),              #emit velocity
      15.0,                        #emit dir randomness
      75,                        #rotation speed
      5,                         #rotation damping
    ),
	("aestus_slash", psf_billboard_2d | psf_always_emit, "prt_mesh_rose_a",
     10, 4, 0.02, 0.025, 1, 5,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0, 1), (1, 1),        #alpha keys
     (0, 0.5), (1, 0.5),      #red keys
     (0, 0.5), (1, 0.5),      #green keys
     (0, 0.5), (1, 0.5),      #blue keys
     (0, 0.25),   (1, 0.25),    #scale keys
     (0.0, 0.0, 0.0),             #emit box size
     (0.1, 0.1, 0.1),              #emit velocity
      0.5,                        #emit dir randomness
      10,                        #rotation speed
      0,                         #rotation damping
    ),
	("cast", 0,  "cast",
     1, 1, 0, 0, 0, 0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, 1.0), (1, 1.0),        #alpha keys
     (0.0, 1), (0.7, 1),      #red keys
     (0.0, 1), (0.7, 1),       #green keys
     (0.0, 1), (1.0, 1),      #blue keys
     (0.0, 5.0), (0.3, 7),    #scale keys
     (0, 0, 0.0),           #emit box size
     (0.0, 1.0, 0.0),               #emit velocity
     0,                         #emit dir randomness
     0,                        #rotation speed
     12,                       #rotation damping
    ),
	("burst", 0,  "burst",
     1, 0.25, 0, 0, 0, 0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, 1.0), (1, 1.0),        #alpha keys
     (0.01, 1), (1.0, 1),      #red keys
     (0.01, 1), (1.0, 1),       #green keys
     (0.01, 1), (1.0, 1),      #blue keys
     (0.0, 0.1), (1.0, 1),    #scale keys
     (0, 0, 0.0),           #emit box size
     (0.0, 1.0, 0.0),               #emit velocity
     0,                         #emit dir randomness
     0,                        #rotation speed
     12,                       #rotation damping
    ),
	("beam", 0,  "alignment_test",
     50, 0.5, 0, 0, 0, 0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, 1), (1, 1),        #alpha keys
     (0.0, 1), (1, 1),      #red keys
     (0.0, 1),(1, 1),       #green keys
     (0.0, 1), (1, 1),      #blue keys
     (0.0, 1),   (1, 1),    #scale keys
     (0, 0, 0.0),           #emit box size
     (0, 0, 0.0),               #emit velocity
     0,                         #emit dir randomness
     0,                        #rotation speed
     0,                       #rotation damping
    ),
]
