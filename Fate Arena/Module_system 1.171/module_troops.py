import random
from header_common import *
from header_items import *
from header_troops import *
from header_skills import *
from ID_factions import *
from ID_items import *
from ID_scenes import *

# ["troop_id","Troop_Name","Troop_Name_Plural", flags, scene, 0, faction, [inv_list], attributes, weapon_proficencies, skills, facecode, 0],
# #######################################################################
#  Each troop contains the following fields:
#  1) Troop id (string): used for referencing troops in other files. The prefix trp_ is automatically added before each troop-id .
#  2) Troop name (string).
#  3) Plural troop name (string).
#  4) Troop flags (int). See header_troops.py for a list of available flags
#  5) Scene (int) (only applicable to heroes) For example: scn_reyvadin_castle|entry(1) puts troop in reyvadin castle's first entry point
#  6) Reserved (int). Put constant "reserved" or 0.
#  7) Faction (int)
#  8) Inventory (list): Must be a list of items
#  9) Attributes (int): Example usage:
#           str_6|agi_6|int_4|cha_5|level(5)
# 10) Weapon proficiencies (int): Example usage:
#           wp_one_handed(55)|wp_two_handed(90)|wp_polearm(36)|wp_archery(80)|wp_crossbow(24)|wp_throwing(45)
#     The function wp(x) will create random weapon proficiencies close to value x.
#     To make an expert archer with other weapon proficiencies close to 60 you can use something like:
#           wp_archery(160) | wp(60)
# 11) Skills (int): See header_skills.py to see a list of skills. Example:
#           knows_ironflesh_3|knows_power_strike_2|knows_athletics_2|knows_riding_2
# 12) Face code (int): You can obtain the face code by pressing ctrl+E in face generator screen
# 13) Face code (int)(2) (only applicable to regular troops, can be omitted for heroes):
#     The game will create random faces between Face code 1 and face code 2 for generated troops
# 14) Troop image (string): If this variable is set, the troop will use an image rather than its 3D visual during the conversations
# #######################################################################

# #######################################################################
#		Function Definitions
# #######################################################################

def wp(x):
  n = 0
  r = 10 + int(x / 10)
#  n |= wp_one_handed(x + random.randrange(r))
#  n |= wp_two_handed(x + random.randrange(r))
#  n |= wp_polearm(x + random.randrange(r))
#  n |= wp_archery(x + random.randrange(r))
#  n |= wp_crossbow(x + random.randrange(r))
#  n |= wp_throwing(x + random.randrange(r))
  n |= wp_one_handed(x)
  n |= wp_two_handed(x)
  n |= wp_polearm(x)
  n |= wp_archery(x)
  n |= wp_crossbow(x)
  n |= wp_throwing(x)
  return n

def wpe(m,a,c,t):
   n = 0
   n |= wp_one_handed(m)
   n |= wp_two_handed(m)
   n |= wp_polearm(m)
   n |= wp_archery(a)
   n |= wp_crossbow(c)
   n |= wp_throwing(t)
   return n

def wpex(o,w,p,a,c,t):
   n = 0
   n |= wp_one_handed(o)
   n |= wp_two_handed(w)
   n |= wp_polearm(p)
   n |= wp_archery(a)
   n |= wp_crossbow(c)
   n |= wp_throwing(t)
   return n
   
def wp_melee(x):
  n = 0
  r = 10 + int(x / 10)
#  n |= wp_one_handed(x + random.randrange(r))
#  n |= wp_two_handed(x + random.randrange(r))
#  n |= wp_polearm(x + random.randrange(r))
  n |= wp_one_handed(x + 20)
  n |= wp_two_handed(x)
  n |= wp_polearm(x + 10)
  return n

# #######################################################################
#		Definitions
#			NOTE: This is /not/ hardcoded, but I left it for ease of reading the code.
# #######################################################################
start_attrib = str_4|agi_4|int_4|cha_4

knows_common = knows_riding_1|knows_trade_2|knows_inventory_management_2|knows_prisoner_management_1|knows_leadership_1
knows_common_multiplayer = knows_trade_10|knows_inventory_management_10|knows_prisoner_management_10|knows_leadership_10|knows_spotting_10|knows_pathfinding_10|knows_tracking_10|knows_engineer_10|knows_first_aid_10|knows_surgery_10|knows_wound_treatment_10|knows_tactics_10|knows_trainer_10|knows_looting_10
def_attrib = str_7 | agi_5 | int_4 | cha_4
def_attrib_multiplayer = int_30 | cha_30



knows_lord_1 = knows_riding_3|knows_trade_2|knows_inventory_management_2|knows_tactics_4|knows_prisoner_management_4|knows_leadership_7

knows_warrior_npc = knows_weapon_master_2|knows_ironflesh_1|knows_athletics_1|knows_power_strike_2|knows_riding_2|knows_shield_1|knows_inventory_management_2
knows_merchant_npc = knows_riding_2|knows_trade_3|knows_inventory_management_3 #knows persuasion
knows_tracker_npc = knows_weapon_master_1|knows_athletics_2|knows_spotting_2|knows_pathfinding_2|knows_tracking_2|knows_ironflesh_1|knows_inventory_management_2

lord_attrib = str_20|agi_20|int_20|cha_20|level(38)

knight_attrib_1 = str_15|agi_14|int_8|cha_16|level(22)
knight_attrib_2 = str_16|agi_16|int_10|cha_18|level(26)
knight_attrib_3 = str_18|agi_17|int_12|cha_20|level(30)
knight_attrib_4 = str_19|agi_19|int_13|cha_22|level(35)
knight_attrib_5 = str_20|agi_20|int_15|cha_25|level(41)
knight_skills_1 = knows_riding_3|knows_ironflesh_2|knows_power_strike_3|knows_athletics_1|knows_tactics_2|knows_prisoner_management_1|knows_leadership_3
knight_skills_2 = knows_riding_4|knows_ironflesh_3|knows_power_strike_4|knows_athletics_2|knows_tactics_3|knows_prisoner_management_2|knows_leadership_5
knight_skills_3 = knows_riding_5|knows_ironflesh_4|knows_power_strike_5|knows_athletics_3|knows_tactics_4|knows_prisoner_management_2|knows_leadership_6
knight_skills_4 = knows_riding_6|knows_ironflesh_5|knows_power_strike_6|knows_athletics_4|knows_tactics_5|knows_prisoner_management_3|knows_leadership_7
knight_skills_5 = knows_riding_7|knows_ironflesh_6|knows_power_strike_7|knows_athletics_5|knows_tactics_6|knows_prisoner_management_3|knows_leadership_9

reserved = 0
no_scene = 0

##################################################################################################################
#### FATE RIDER SABER FACECODES, ATTRIBUTES AND SKILLS
##################################################################################################################

saber_attrib = str_25|agi_25|int_15|cha_25|level(45)
lancer_attrib = str_20|agi_30|int_15|cha_15|level(38)
rider_attrib = str_20|agi_25|int_15|cha_15|level(38)
caster_attrib = str_10|agi_10|int_30|cha_25|level(30)
archer_attrib = str_15|agi_30|int_15|cha_25|level(30)
assassin_attrib = str_18|agi_25|int_15|cha_15|level(30)
berserker_attrib = str_25|agi_20|int_3|cha_3|level(15)
mage_attrib = str_7|agi_11|int_20|cha_25|level(15)

saber_rank_d = knows_riding_2|knows_ironflesh_2|knows_power_strike_4|knows_athletics_1|knows_tactics_2|knows_prisoner_management_1|knows_leadership_3
lancer_rank_d = knows_ironflesh_2|knows_power_strike_3|knows_athletics_5
rider_rank_d = knows_riding_4|knows_ironflesh_2|knows_power_strike_3|knows_athletics_1|knows_tactics_2|knows_leadership_3
caster_rank_d = knows_ironflesh_2|knows_athletics_1|knows_tactics_2|knows_prisoner_management_1|knows_leadership_3
archer_rank_d = knows_ironflesh_2|knows_power_draw_3|knows_athletics_4
assassin_rank_d = knows_ironflesh_2|knows_power_throw_4|knows_athletics_6
berserker_rank_d = knows_ironflesh_5|knows_power_strike_5
mage_rank_d = knows_common
church_member = knows_common

swadian_face_younger_1 = 0x0000000000000001124000000020000000000000001c00800000000000000000
swadian_face_young_1   = 0x0000000400000001124000000020000000000000001c00800000000000000000
swadian_face_middle_1  = 0x0000000800000001124000000020000000000000001c00800000000000000000
swadian_face_old_1     = 0x0000000d00000001124000000020000000000000001c00800000000000000000
swadian_face_older_1   = 0x0000000fc0000001124000000020000000000000001c00800000000000000000

swadian_face_younger_2 = 0x00000000000062c76ddcdf7feefbffff00000000001efdbc0000000000000000
swadian_face_young_2   = 0x00000003c00062c76ddcdf7feefbffff00000000001efdbc0000000000000000
swadian_face_middle_2  = 0x00000007c00062c76ddcdf7feefbffff00000000001efdbc0000000000000000
swadian_face_old_2     = 0x0000000bc00062c76ddcdf7feefbffff00000000001efdbc0000000000000000
swadian_face_older_2   = 0x0000000fc00062c76ddcdf7feefbffff00000000001efdbc0000000000000000

vaegir_face_younger_1 = 0x0000000000000001124000000020000000000000001c00800000000000000000
vaegir_face_young_1   = 0x0000000400000001124000000020000000000000001c00800000000000000000
vaegir_face_middle_1  = 0x0000000800000001124000000020000000000000001c00800000000000000000
vaegir_face_old_1     = 0x0000000d00000001124000000020000000000000001c00800000000000000000
vaegir_face_older_1   = 0x0000000fc0000001124000000020000000000000001c00800000000000000000

vaegir_face_younger_2 = 0x000000003f00230c4deeffffffffffff00000000001efff90000000000000000
vaegir_face_young_2   = 0x00000003bf00230c4deeffffffffffff00000000001efff90000000000000000
vaegir_face_middle_2  = 0x00000007bf00230c4deeffffffffffff00000000001efff90000000000000000
vaegir_face_old_2     = 0x0000000cbf00230c4deeffffffffffff00000000001efff90000000000000000
vaegir_face_older_2   = 0x0000000ff100230c4deeffffffffffff00000000001efff90000000000000000

khergit_face_younger_1 = 0x0000000009003109207000000000000000000000001c80470000000000000000
khergit_face_young_1   = 0x00000003c9003109207000000000000000000000001c80470000000000000000
khergit_face_middle_1  = 0x00000007c9003109207000000000000000000000001c80470000000000000000
khergit_face_old_1     = 0x0000000b89003109207000000000000000000000001c80470000000000000000
khergit_face_older_1   = 0x0000000fc9003109207000000000000000000000001c80470000000000000000

khergit_face_younger_2 = 0x000000003f0061cd6d7ffbdf9df6ebee00000000001ffb7f0000000000000000
khergit_face_young_2   = 0x00000003bf0061cd6d7ffbdf9df6ebee00000000001ffb7f0000000000000000
khergit_face_middle_2  = 0x000000077f0061cd6d7ffbdf9df6ebee00000000001ffb7f0000000000000000
khergit_face_old_2     = 0x0000000b3f0061cd6d7ffbdf9df6ebee00000000001ffb7f0000000000000000
khergit_face_older_2   = 0x0000000fff0061cd6d7ffbdf9df6ebee00000000001ffb7f0000000000000000

nord_face_younger_1 = 0x0000000000000001124000000020000000000000001c00800000000000000000
nord_face_young_1   = 0x0000000400000001124000000020000000000000001c00800000000000000000
nord_face_middle_1  = 0x0000000800000001124000000020000000000000001c00800000000000000000
nord_face_old_1     = 0x0000000d00000001124000000020000000000000001c00800000000000000000
nord_face_older_1   = 0x0000000fc0000001124000000020000000000000001c00800000000000000000

nord_face_younger_2 = 0x00000000310023084deeffffffffffff00000000001efff90000000000000000
nord_face_young_2   = 0x00000003b10023084deeffffffffffff00000000001efff90000000000000000
nord_face_middle_2  = 0x00000008310023084deeffffffffffff00000000001efff90000000000000000
nord_face_old_2     = 0x0000000c710023084deeffffffffffff00000000001efff90000000000000000
nord_face_older_2   = 0x0000000ff10023084deeffffffffffff00000000001efff90000000000000000

rhodok_face_younger_1 = 0x0000000009002003140000000000000000000000001c80400000000000000000
rhodok_face_young_1   = 0x0000000449002003140000000000000000000000001c80400000000000000000
rhodok_face_middle_1  = 0x0000000849002003140000000000000000000000001c80400000000000000000
rhodok_face_old_1     = 0x0000000cc9002003140000000000000000000000001c80400000000000000000
rhodok_face_older_1   = 0x0000000fc9002003140000000000000000000000001c80400000000000000000

rhodok_face_younger_2 = 0x00000000000062c76ddcdf7feefbffff00000000001efdbc0000000000000000
rhodok_face_young_2   = 0x00000003c00062c76ddcdf7feefbffff00000000001efdbc0000000000000000
rhodok_face_middle_2  = 0x00000007c00062c76ddcdf7feefbffff00000000001efdbc0000000000000000
rhodok_face_old_2     = 0x0000000bc00062c76ddcdf7feefbffff00000000001efdbc0000000000000000
rhodok_face_older_2   = 0x0000000fc00062c76ddcdf7feefbffff00000000001efdbc0000000000000000

man_face_younger_1 = 0x0000000000000001124000000020000000000000001c00800000000000000000
man_face_young_1   = 0x0000000400000001124000000020000000000000001c00800000000000000000
man_face_middle_1  = 0x0000000800000001124000000020000000000000001c00800000000000000000
man_face_old_1     = 0x0000000d00000001124000000020000000000000001c00800000000000000000
man_face_older_1   = 0x0000000fc0000001124000000020000000000000001c00800000000000000000

man_face_younger_2 = 0x000000003f0052064deeffffffffffff00000000001efff90000000000000000
man_face_young_2   = 0x00000003bf0052064deeffffffffffff00000000001efff90000000000000000
man_face_middle_2  = 0x00000007bf0052064deeffffffffffff00000000001efff90000000000000000
man_face_old_2     = 0x0000000bff0052064deeffffffffffff00000000001efff90000000000000000
man_face_older_2   = 0x0000000fff0052064deeffffffffffff00000000001efff90000000000000000

merchant_face_1    = man_face_young_1
merchant_face_2    = man_face_older_2

woman_face_1    = 0x0000000000000001000000000000000000000000001c00000000000000000000
woman_face_2    = 0x00000003bf0030067ff7fbffefff6dff00000000001f6dbf0000000000000000

swadian_woman_face_1 = 0x0000000180102006124925124928924900000000001c92890000000000000000
swadian_woman_face_2 = 0x00000001bf1000061db6d75db6b6dbad00000000001c92890000000000000000

khergit_woman_face_1 = 0x0000000180103006124925124928924900000000001c92890000000000000000
khergit_woman_face_2 = 0x00000001af1030025b6eb6dd6db6dd6d00000000001eedae0000000000000000

refugee_face1 = woman_face_1
refugee_face2 = woman_face_2
girl_face1    = woman_face_1
girl_face2    = woman_face_2

mercenary_face_1 = 0x0000000000000000000000000000000000000000001c00000000000000000000
mercenary_face_2 = 0x0000000cff00730b6db6db6db7fbffff00000000001efffe0000000000000000

vaegir_face1  = vaegir_face_young_1
vaegir_face2  = vaegir_face_older_2

bandit_face1  = man_face_young_1
bandit_face2  = man_face_older_2

undead_face1  = 0x00000000002000000000000000000000
undead_face2  = 0x000000000020010000001fffffffffff

arthur_face = 0x0000000008000004188c7ac6dd463ca400000000001e17a40000000000000000
emiya_face = 0x000000002c000003188c7ac6dd463ca400000000001e17a40000000000000000
saber_face1 = 0x0000000008000004188c7ac6dd463ca400000000001e17a40000000000000000
saber_face2 = 0x0000000008000004188c7ac6dd463ca400000000001e17a40000000000000000
saber_face3 = 0x0000000fc0001009195d75cd8f58a50500000000001d57690000000000000000
saber_face4 = 0x000000003f0000113604afb51b585af600000000001cd33b0000000000000000
saber_face5 = 0x0000000008000004188c7ac6dd463ca400000000001e17a40000000000000000
lancer_face1 = 0x000000003b103014211bd9469a8a392400000000001ce7340000000000000000
lancer_face2 = 0x000000003b103014211bd9469a8a392400000000001ce7340000000000000000
lancer_face3 = 0x000000000010100f211bd9469a8a392400000000001ce7340000000000000000
lancer_face4 = 0x0000000026101010211bd9469a8a392400000000001ce7340000000000000000
lancer_face5 = 0x0000000004100003211bd9469a8a392400000000001ce7340000000000000000
rider_face1 = 0x000000000a00000236db6db6db6db6db00000000001db6db0000000000000000
rider_face2 = 0x000000000000218717f1c765f174e8ed00000000001ef4ed0000000000000000
rider_face3 = 0x000000000000100436db6db6db6db6db00000000001db6db0000000000000000
rider_face4 = 0x000000000700200636db6db6db6db6db00000000001db6db0000000000000000
rider_face5 = 0x00000000000410055dd46cdae22e46a500000000001f35120000000000000000
caster_face1 = 0x0000000023000004374b06b6ebcdbadb00000000001d36e30000000000000000
caster_face2 = 0x000000001e1020020c9a65144242208200000000001c90420000000000000000
caster_face3 = 0x000000000100550f36db6db6db6db6db00000000001db6db0000000000000000
caster_face4 = 0x000000000900000836db6db6db6db6db00000000001db6db0000000000000000
caster_face5 = 0x000000001f001150174b8d2aca6e885400000000001dd84c0000000000000000
archer_face1 = 0x000000000a000011542473d4db6db6db00000000001cb8ed0000000000000000
archer_face2 = 0x000000000a000011542473d4db6db6db00000000001cb8ed0000000000000000
archer_face3 = 0x000000000a000006542473d4db6db6db00000000001cb8ed0000000000000000
archer_face4 = 0x000000002600000136db6db6db6db6db00000000001db6db0000000000000000
archer_face5 = 0x000000001f00600536db6db6db6db6db00000000001db6db0000000000000000
assassin_face1 = 0x00000006b7004000258b6d1ce3489adb00000000001f35560000000000000000
assassin_face2 = 0x000000003f102002471a65f2accd932900000000001ead610000000000000000
assassin_face3 = 0x000000065f0c41893764693124569cd500000000001ce36d0000000000000000
assassin_face4 = 0x000000003610200322849a14f38a450e00000000001eb32d0000000000000000
assassin_face5 = 0x0000000fff0042cb4ba331a873d7c6e300000000001c92820000000000000000
berserker_face1 = 0x00000005400c3004273cd9b4894924cb00000000001ca9a20000000000000000
berserker_face2 = 0x000000057f006009201d43e370555d7300000000001efff80000000000000000
berserker_face3 = 0x0000000026002014371c6db3715cbb2300000000001db9730000000000000000
berserker_face4 = 0x000000000d0020013b6579430998c5ad000000000011b6560000000000000000
berserker_face5 = 0x000000012f10021044e4515860549cc400000000001de4630000000000000000
mage_face1 = 0x00000009a10040853a9a6654a4fac6e900000000001d35210000000000000000

tf_guarantee_all = tf_guarantee_boots|tf_guarantee_armor|tf_guarantee_gloves|tf_guarantee_helmet|tf_guarantee_horse|tf_guarantee_shield|tf_guarantee_ranged
tf_guarantee_all_wo_ranged = tf_guarantee_boots|tf_guarantee_armor|tf_guarantee_gloves|tf_guarantee_helmet|tf_guarantee_horse|tf_guarantee_shield

	# #######################################################################
	#		Troops
	# #######################################################################
troops = [

    # #######################################################################
    #	Hardcoded
	# #######################################################################

	["player","Player","Player", tf_hero|tf_unmoveable_in_party_window, 0, 0, fac_commoners, [], start_attrib, 0, 0, 0, 0],
	["multiplayer_profile_troop_male","multiplayer_profile_troop_male","multiplayer_profile_troop_male", tf_hero|tf_guarantee_all, 0, 0, fac_commoners, [], 0, 0, 0, 0, 0],
	["multiplayer_profile_troop_female","multiplayer_profile_troop_female","multiplayer_profile_troop_female", tf_hero|tf_female|tf_guarantee_all, 0, 0, fac_commoners, [], 0, 0, 0, 0, 0],
	["temp_troop","Temp Troop","Temp Troop", 0, 0, 0, fac_commoners, [], 0, 0, 0, 0, 0],
    
	# #######################################################################
	#	Troops before this point are hardwired into the game
	# #######################################################################
###########################################################################  
#### Fate: Rider/Saber Characters
###########################################################################
   #Saber Class Servants
	#TODO: Stats Distribution, Facial Design, Items, Balance
   ["saber","Saber","Saber",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_mounted, 0, reserved, fac_commoners,
    [itm_excalibur],
    saber_attrib,wp(920),saber_rank_d,
    arthur_face],
	#Artoria Pendragon
   ["saber_01","Saber","Saber",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_mounted, 0, reserved, fac_commoners,
    [itm_artoria_gauntlets,itm_artoria_boots,itm_artoria_plate,itm_excalibur_invis],
    saber_attrib,wp(920),saber_rank_d,
    arthur_face],
	#Nero Claudius
   ["saber_02","Saber","Saber",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_mounted, 0, reserved, fac_commoners,
    [itm_nero_boots,itm_nero_dress,itm_aestus_estus],
    saber_attrib,wp(920),saber_rank_d,
    arthur_face],
	#Siegfried
   ["saber_03","Saber","Saber",tf_hero|tf_unmoveable_in_party_window|tf_mounted, 0, reserved, fac_commoners,
    [itm_sieg_boots,itm_sieg_gauntlets,itm_sieg_plate,itm_balmung],
    saber_attrib,wp(920),saber_rank_d,
    saber_face3],
	#Fergus mac Roich
   ["saber_04","Saber","Saber",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_fergus_gauntlets,itm_fergus_boots,itm_fergus_vest,itm_caladbolg],
    saber_attrib,wp(920),saber_rank_d,
    saber_face4],
	#Richard the Lionheart
   ["saber_05","Saber","Saber",tf_hero|tf_unmoveable_in_party_window|tf_mounted, 0, reserved, fac_commoners,
    [itm_lion_gauntlets,itm_lion_boots,itm_lion_plate,itm_excalibur_lion],
    saber_attrib,wp(920),saber_rank_d,
    saber_face5],
	
   #Lancer Class Servants
	#TODO: create Cu Chulainn, Diarmuid Ua Duibhne, Fionn mac Cumhaill, Hector, Brynhildr
   ["lancer","Lancer","Lancer",tf_hero|tf_unmoveable_in_party_window, 0, reserved, fac_commoners,
    [itm_gae_bolg],
    lancer_attrib,wp(900),lancer_rank_d,
    lancer_face1],
	#Cu Chulainn
   ["lancer_01","Lancer","Lancer",tf_hero|tf_unmoveable_in_party_window|tf_mounted, 0, reserved, fac_commoners,
    [itm_cu_gauntlets,itm_cu_boots,itm_cu_suit,itm_gae_bolg],
    lancer_attrib,wp(920),lancer_rank_d,
    lancer_face1],
	#Diarmuid Ua Duibhne
   ["lancer_02","Lancer","Lancer",tf_hero|tf_unmoveable_in_party_window|tf_mounted, 0, reserved, fac_commoners,
    [itm_diarmund_gloves,itm_diarmund_boots,itm_diarmund_suit,itm_gae_buidhe,itm_gae_dearg],
    lancer_attrib,wp(920),lancer_rank_d,
    lancer_face2],
	#Fionn mac Cumhaill
   ["lancer_03","Lancer","Lancer",tf_hero|tf_unmoveable_in_party_window|tf_mounted, 0, reserved, fac_commoners,
    [itm_fionn_gloves,itm_fionn_boots,itm_fionn_armor,itm_mac_an_luin],
    lancer_attrib,wp(920),lancer_rank_d,
    lancer_face3],
	#Hector
   ["lancer_04","Lancer","Lancer",tf_hero|tf_unmoveable_in_party_window|tf_mounted, 0, reserved, fac_commoners,
    [itm_hector_arm,itm_hector_boots,itm_hector_gear,itm_durindana],
    lancer_attrib,wp(920),lancer_rank_d,
    lancer_face4],
	#Brynhildr
   ["lancer_05","Lancer","Lancer",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_mounted, 0, reserved, fac_commoners,
    [itm_bryn_boots,itm_bryn_clothes,itm_brynhildr],
    lancer_attrib,wp(920),lancer_rank_d,
    lancer_face5],

   #Archer Class Servants
	#TODO:
   ["archer","Archer","Archer",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [],
    archer_attrib,wp(800),archer_rank_d,
    archer_face1],
	#EMIYA
   ["archer_01","Archer","Archer",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all|tf_guarantee_ranged, 0, reserved, fac_commoners,
    [itm_kanshou,itm_shroud_of_martin,itm_emiya_boots,],#itm_emiya_gloves],
    archer_attrib,wp(800),archer_rank_d,
    emiya_face],
	#Gilgamesh
   ["archer_02","Archer","Archer",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_gilgamesh_ea,itm_gil_armor,itm_gil_boots],
    archer_attrib,wp(800),archer_rank_d,
    archer_face1],
	#Atalanta
   ["archer_03","Archer","Archer",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_tauropolos,itm_atalanta_arrows,itm_atalanta_arrows,itm_atalanta_dress,itm_atalanta_boots],
    archer_attrib,wp(800),archer_rank_d,
    archer_face3],
	#Robin Hood
   ["archer_04","Archer","Archer",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_robin_knife,itm_yew_bow,itm_yew_bolts_actual,itm_robin_clothes,itm_robin_boots,itm_robin_gloves,itm_robin_hood],
    archer_attrib,wp(800),archer_rank_d,
    archer_face4],
	#Arash
   ["archer_05","Archer","Archer",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_arash_bow,itm_created_arrows,itm_arash_boots,itm_arash_plate,itm_arash_gloves,itm_arash_hood],
    archer_attrib,wp(800),archer_rank_d,
    archer_face5],
      
   #Rider Class Servants
	#TODO:
   ["rider","Rider","Rider",tf_hero|tf_unmoveable_in_party_window|tf_mounted, 0, reserved, fac_commoners,
    [],
    rider_attrib,wp(830),rider_rank_d,
    rider_face1],
	#Medusa
   ["rider_01","Rider","Rider",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_mounted|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_medusa_mask,itm_medusa_boots,itm_medusa_dress,itm_medusa_dagger],
    rider_attrib,wp(920),rider_rank_d,
    rider_face1],
	#Iskander - Alexander the Great
   ["rider_02","Rider","Rider",tf_hero|tf_large_servants|tf_unmoveable_in_party_window|tf_mounted|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_iskandar_boots,itm_iskandar_plate,itm_kupriotes],
    rider_attrib, wp(920), rider_rank_d,
    rider_face2],
	#Achilles
   ["rider_03","Rider","Rider",tf_hero|tf_unmoveable_in_party_window|tf_mounted|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_achilles_gauntlets,itm_achilles_boots,itm_achilles_plate,itm_kosmos_shield,itm_diatrekhon],
    rider_attrib,wp(920),rider_rank_d,
    rider_face3],
	#Astolfo
   ["rider_04","Rider","Rider",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_mounted|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_astolfo_gauntlets,itm_astolfo_boots,itm_astolfo_plate,itm_argalia,itm_astolfo_sidearm],
    rider_attrib,wp(920),rider_rank_d,
    rider_face4],
	#Saint George
   ["rider_05","Rider","Rider",tf_hero|tf_unmoveable_in_party_window|tf_mounted|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_george_gauntlets,itm_george_boots,itm_george_armor,itm_ascalon],
    rider_attrib,wp(920),rider_rank_d,
    rider_face5],
   
   #Caster Class Servants
    #TODO:
	["caster","Caster","Caster",tf_hero|tf_unmoveable_in_party_window, 0, reserved, fac_commoners,
    [],
    caster_attrib,wp(500),caster_rank_d,
    caster_face1],
	#Medea
	["caster_01","Caster","Caster",tf_hero|tf_female|tf_unmoveable_in_party_window, 0, reserved, fac_commoners,
    [itm_medea_boots,itm_medea_robes,itm_medea_staff,itm_rulebreaker],
    caster_attrib,wp(500),caster_rank_d,
    caster_face2],
	#Bluebeard - Gilles de Rais
	["caster_02","Caster","Caster",tf_hero|tf_unmoveable_in_party_window, 0, reserved, fac_commoners,
    [itm_gilles_boots,itm_gilles_cloak],
    caster_attrib,wp(500),caster_rank_d,
    caster_face1],
	#William Shakespeare
	["caster_03","Caster","Caster",tf_hero|tf_unmoveable_in_party_window, 0, reserved, fac_commoners,
    [itm_shakespeare_gloves,itm_shakespeare_boots,itm_shakespeare_garb],
    caster_attrib,wp(500),caster_rank_d,
    caster_face3],
	#Avicebron
	["caster_04","Caster","Caster",tf_hero|tf_unmoveable_in_party_window, 0, reserved, fac_commoners,
    [itm_avicebron_gloves,itm_avicebron_boots,itm_avicebron_clothes],
    caster_attrib,wp(500),caster_rank_d,
    caster_face4],
	#Hohenheim
	["caster_05","Caster","Caster",tf_hero|tf_unmoveable_in_party_window, 0, reserved, fac_commoners,
    [itm_hohenheim_gloves,itm_hohenheim_boots,itm_hohenheim_coat,itm_paracelsus_sword],
    caster_attrib,wp(500),caster_rank_d,
    caster_face5],
   
   #Assassin Class Servants
	["assassin","Assassin","Assassin",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_throwing_daggers_assassin,itm_skull_mask_with_hood,itm_assassin_robes],
    assassin_attrib,wp(780),assassin_rank_d,
    assassin_face1],
	#Hassan of the Cursed Arm
	["assassin_01","Assassin","Assassin",tf_hero|tf_large_servants|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_throwing_daggers_assassin,itm_skull_mask_with_hood,itm_cursedarm_wrapped,itm_cursed_arm_wrapped_body],
    assassin_attrib,wp(780),assassin_rank_d,
    assassin_face1],
	#Hassan of the Hundred Faces
	["assassin_02","Assassin","Assassin",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_throwing_daggers_assassin,itm_serenity_dagger,itm_serenity_mask,itm_assassin_robes],
    assassin_attrib,wp(780),assassin_rank_d,
    assassin_face2],
	#Hassan of Shadow Peeling
	["assassin_03","Assassin","Assassin",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_throwing_daggers_assassin,itm_shadow_dagger,itm_shadow_mask,itm_assassin_robes_a],
    assassin_attrib,wp(780),assassin_rank_d,
    assassin_face3],
	#Hassan of Serenity
	["assassin_04","Assassin","Assassin",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_throwing_daggers_assassin,itm_hundred_dagger,itm_hundred_face_mask,itm_assassin_robes],
    assassin_attrib,wp(780),assassin_rank_d,
    assassin_face4],
	#King Hassan-i-Sabbah, Old Man of The Mountain
	["assassin_05","Assassin","Assassin",tf_hero|tf_large_servants|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_throwing_daggers_assassin,itm_hassan_sword,itm_skull_helmet,itm_king_hassan_armor],
    assassin_attrib,wp(1000),assassin_rank_d,
    assassin_face5],
   
   #Berserker Class Servants
    #TODO: 
   ["berserker","Berserker","Berserker",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_berserkelot_armor,itm_berserkelot_greaves,itm_berserkelot_gloves,itm_zerkalot_sword],
    berserker_attrib,wp(700),berserker_rank_d,
    berserker_face1],
	#Heracles 
   ["berserker_01","Berserker","Berserker",tf_hero|tf_large_servants|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_heracles_wear,itm_heracles_armcuffs,itm_heracles_footcuffs,itm_axe_sword],
    berserker_attrib,wp(700),berserker_rank_d,
    berserker_face2],
	#Lancelot du Lac
   ["berserker_02","Berserker","Berserker",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_berserkelot_armor,itm_berserkelot_greaves,itm_berserkelot_helmet,itm_berserkelot_gloves,itm_zerkalot_sword],
    berserker_attrib,wp(700),berserker_rank_d,
    berserker_face1],
	#Spartacus
   ["berserker_03","Berserker","Berserker",tf_hero|tf_large_servants|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_spart_mask,itm_spart_restraints,itm_spart_boots,itm_spart_gauntlets,itm_spart_sword],
    berserker_attrib,wp(700),berserker_rank_d,
    berserker_face3],
	#Frankenstein's Monster
   ["berserker_04","Berserker","Berserker",tf_hero|tf_female|tf_unmoveable_in_party_window, 0, reserved, fac_commoners,
    [itm_frankie_dress,itm_frankie_boots,itm_fran_gloves,itm_fran_veil,itm_bridal_chest],
    berserker_attrib,wp(700),berserker_rank_d,
    berserker_face4],
	#Asterios
   ["berserker_05","Berserker","Berserker",tf_hero|tf_large_servants|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_minotaur_mask,itm_asterios_belt,itm_asterios_bindings,itm_asterios_boots,itm_labrys_left,itm_labrys_right],
    berserker_attrib,wp(700),berserker_rank_d,
    berserker_face5],

   
   #Special Class Servants *Shielders, Rulers, Fakes, Alters, Foreigners, Etc
    ["avenger","Avenger","Avenger",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [],
    saber_attrib,wp(1000),assassin_rank_d,
    0x0000000fc000001112344966f47362e200000000001cacde0000000000000000],
	# Angra Maynu
	["avenger_01","Avenger","Avenger",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [],
    saber_attrib,wp(1000),assassin_rank_d,
    0x0000000fc000001112344966f47362e200000000001cacde0000000000000000],
   
    ["ruler","Ruler","Ruler",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [],
    saber_attrib,wp(1000),assassin_rank_d,
    0x0000000fc000001112344966f47362e200000000001cacde0000000000000000],
	# Jeanne
	["ruler_01","Ruler","Ruler",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [],
    saber_attrib,wp(1000),assassin_rank_d,
    0x0000000fc000001112344966f47362e200000000001cacde0000000000000000],
	# Shirou
	["ruler_02","Ruler","Ruler",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_black_key],
    saber_attrib,wp(1000),assassin_rank_d,
    0x0000000fc000001112344966f47362e200000000001cacde0000000000000000],
	
	
	["shielder","Shielder","Shielder",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [],
    saber_attrib,wp(1000),assassin_rank_d,
    0x0000000fc000001112344966f47362e200000000001cacde0000000000000000],
	# Mash - Galahad
	["shielder_01","Shielder","Shielder",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [itm_mash_armor,itm_mash_guants,itm_mash_boots,itm_mash_shield,itm_mash_shield_bash],
    saber_attrib,wp(1000),assassin_rank_d|knows_shield_10,
    0x0000000fc000001112344966f47362e200000000001cacde0000000000000000],
	
	
	#Generic Assassins for 100 Face Summons 
	#TODO: Giant, Midget
	["assassin_100_face", "Weak Assassin", "Weak Assassin", tf_randomize_face|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [],
    assassin_attrib,wp(100),assassin_rank_d,
    man_face_younger_1, man_face_older_2],
	["assassin_100_face_giant", "Weak Assassin", "Weak Assassin", tf_large_servants|tf_randomize_face|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [],
    assassin_attrib,wp(100),assassin_rank_d,
    man_face_younger_1, man_face_older_2],
	["assassin_100_face_dwarf", "Weak Assassin", "Weak Assassin", tf_small_servants|tf_randomize_face|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [],
    assassin_attrib,wp(100),assassin_rank_d,
    man_face_younger_1, man_face_older_2],
	["assassin_100_face_lady", "Weak Assassin", "Weak Assassin", tf_female|tf_randomize_face|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners,
    [],
    assassin_attrib,wp(100),assassin_rank_d,
    woman_face_1, woman_face_2],
	
	#Ionioi Hetairoi Troops
	["ionioi_hetairoi_footman","Ionioi Hetairoi Footman","Ionioi Hetairoi Footmen",tf_guarantee_boots|tf_guarantee_armor|tf_guarantee_shield,0,0,fac_commoners,
    [],
    def_attrib|level(9),wp(75),knows_common|knows_athletics_2,swadian_face_young_1, swadian_face_old_2],
	["ionioi_hetairoi_horseman","Ionioi Hetairoi Horseman","Ionioi Hetairoi Horsemen",tf_mounted|tf_guarantee_boots|tf_guarantee_armor|tf_guarantee_helmet|tf_guarantee_horse|tf_guarantee_shield,0,0,fac_commoners,
    [],
    def_attrib|level(21),wp(100),knows_riding_3|knows_ironflesh_3|knows_power_strike_3,vaegir_face_young_1, vaegir_face_older_2],
	["ionioi_hetairoi_archer","Ionioi Hetairoi Archer","Ionioi Hetairoi Archers",tf_guarantee_ranged|tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners,
    [],
    str_10 | agi_5 | int_4 | cha_4|level(14),wp(60),knows_ironflesh_1|knows_power_draw_1|knows_power_throw_1,vaegir_face_young_1, vaegir_face_old_2],
	["sakura_shadow","sakura_shadow","sakura_shadow",tf_large_servants|tf_guarantee_all|tf_guarantee_all, 0, reserved, fac_commoners, [itm_sakura_shadow, itm_sakura_shadow_h, itm_sakura_shadow_l], mage_attrib,wp(70),knows_merchant_npc, emiya_face],
	
	["starfish_a","sakura_shadow","sakura_shadow", tf_guarantee_all|tf_guarantee_all, 0, reserved, fac_commoners, [itm_eldrich_eyes, itm_sakura_shadow_h, itm_sakura_shadow_l], mage_attrib,wp(70),knows_merchant_npc, emiya_face],
	["starfish_b","sakura_shadow","sakura_shadow", tf_guarantee_all|tf_guarantee_all, 0, reserved, fac_commoners, [itm_eldrich_tendrils, itm_sakura_shadow_h, itm_sakura_shadow_l], mage_attrib,wp(70),knows_merchant_npc, emiya_face],
	["starfish_c","sakura_shadow","sakura_shadow", tf_guarantee_all|tf_guarantee_all, 0, reserved, fac_commoners, [itm_eldrich_eyes, itm_sakura_shadow_h, itm_sakura_shadow_l], mage_attrib,wp(70),knows_merchant_npc, emiya_face],
	["giant_starfish","sakura_shadow","sakura_shadow", tf_large_servants|tf_guarantee_all|tf_guarantee_all, 0, reserved, fac_commoners, [itm_eldrich_tendrils, itm_sakura_shadow_h, itm_sakura_shadow_l], mage_attrib,wp(70),knows_merchant_npc, emiya_face],
	   
	["golem_defense","sakura_shadow","sakura_shadow", tf_large_servants|tf_guarantee_all|tf_guarantee_all, 0, reserved, fac_commoners, [itm_sakura_shadow, itm_sakura_shadow_h, itm_sakura_shadow_l], mage_attrib,wp(70),knows_merchant_npc, emiya_face],
	["golem_ranged","sakura_shadow","sakura_shadow", tf_guarantee_all|tf_guarantee_all, 0, reserved, fac_commoners, [itm_sakura_shadow, itm_sakura_shadow_h, itm_sakura_shadow_l], mage_attrib,wp(70),knows_merchant_npc, emiya_face],
	["golem_power","sakura_shadow","sakura_shadow", tf_large_servants|tf_guarantee_all|tf_guarantee_all, 0, reserved, fac_commoners, [itm_sakura_shadow, itm_sakura_shadow_h, itm_sakura_shadow_l], mage_attrib,wp(70),knows_merchant_npc, emiya_face],
	["golem_magic","sakura_shadow","sakura_shadow", tf_small_servants|tf_guarantee_all|tf_guarantee_all, 0, reserved, fac_commoners, [itm_sakura_shadow, itm_sakura_shadow_h, itm_sakura_shadow_l], mage_attrib,wp(70),knows_merchant_npc, emiya_face],
	
	["actor","Actor","Actors", tf_actors|tf_guarantee_boots|tf_guarantee_armor, no_scene, reserved, fac_commoners, [], str_6|agi_6|level(5), wp(80), knows_common, 0],
	   
   
   #Masters
	#TODO:  strange fake, Prototype,
		#Caren Kotomine, Einzbern Master, Ayaka Sajyou
		#TODO: HAVE THE GAME CHOOSE RANDOM MASTERS TO PIT YOU AGAINST IN THE FREEPLAY GAME MODE
		#TODO: MAKE GAME MODES THAT  SUMMON FIXED MASTERS WITH FIXED SERVANTS
	########Fate/ SN - Hollow Ataraxia Masters
	["master_staynight_01","Shirou Emiya","Shirou Emiya",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved, fac_commoners, [itm_shirou_shirt,itm_shirou_sneakers,itm_kendo_sword], mage_attrib,wp(70),knows_merchant_npc,
	   emiya_face],
	["master_staynight_02","Rin Tohsaka","Rin Tohsaka",tf_female|tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[itm_azoth_sword,itm_rin_outfit,itm_rin_heels,itm_rin_ribbon], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	["master_staynight_03","Illyasviel von Einzbern","Illyasviel von Einzbern",tf_female|tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[itm_illya_cap,itm_illya_peacoat,itm_illya_mittens], mage_attrib,wp(70),knows_merchant_npc,
	   0x00000000000400055915aa226b4d975200000000001ea49e0000000000000000],
	["master_staynight_04","Sakura Matou","Sakura Matou",tf_female|tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[itm_female_fuyuki_uniform, itm_school_shoes, itm_hunting_knife], mage_attrib, wp(70),knows_merchant_npc,
	   0x00000000100010034712c91d0b46245a00000000001d4cd30000000000000000],
	["master_staynight_05","Shinji Matou","Shinji Matou",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[itm_hunting_knife, itm_fuyuki_uniform, itm_sneaker_hitop],
	   mage_attrib,wp(70),knows_merchant_npc,
	   0x00000003290410055712aa22aa4d975200000000001db49e0000000000000000],
	["master_staynight_06","Souichirou Kuzuki","Souichirou Kuzuki",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners, [itm_dress_oxford, itm_casual_suit_navy], mage_attrib, wp(150), knows_merchant_npc,
	   0x000000053f002011494c6944e3cad31b00000000001cd3650000000000000000],
	["master_staynight_07","Kirei Kotomine","Kirei Kotomine",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners, [itm_dress_cheap, itm_priest_raiment_black, itm_black_key, itm_black_key], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000bbf002001494c6948e24ad31b00000000001ca3650000000000000000],
	["master_staynight_08","Zouken Matou","Zouken Matou",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[itm_kimono_traditional, itm_sandal_geta],
	   mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000fe7001001200004000000000000000000001d22010000000000000000],
	["master_staynight_09","Bazett Fraga McRemitz","Bazett Fraga McRemitz",tf_female|tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[itm_black_leather_gloves, itm_black_suit, itm_dress_oxford, itm_test_lightsaber], mage_attrib,wp(70),knows_merchant_npc,
	   0x000000001b040002591568a2db499aea00000000001ca49c0000000000000000],
	   
	########Fate/ Zero Masters
	["master_zero_01","Kiritsugu Emiya","Kiritsugu Emiya",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[itm_contender, itm_origin_rounds, itm_black_suit, itm_black_leather_gloves, itm_dress_oxford],
	   mage_attrib,wp(70),knows_merchant_npc,
	   0x00000003b50420034915aa286b4d975200000000001da49e0000000000000000],
	["master_zero_02","Kirei Kotomine","Kirei Kotomine",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[itm_dress_cheap, itm_priest_raiment_black, itm_black_key, itm_black_key],
	   mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000bbf002001494c6948e24ad31b00000000001ca3650000000000000000],
	["master_zero_03","Tokiomi Tohsaka","Tokiomi Tohsaka",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[],
	   mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	["master_zero_04","Waver Velvet","Waver Velvet",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[],
	   mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	["master_zero_05","Kariya Matou","Kariya Matou",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[],
	   mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	["master_zero_06","Ryuunosuke Uryuu","Ryuunosuke Uryuu",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[itm_hunting_knife],
	   mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	["master_zero_07","Kayneth El-Melloi Archibald","Kayneth El-Melloi Archibald",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[],
	   mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	   
	########Fate/ Apocrypha Masters
		#########Black Faction -Yggdmillennia's-
	["master_apocrypha_01","Darnic Prestone Yggdmillennia","Darnic Prestone Yggdmillennia",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[],
	   mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	["master_apocrypha_02","Gordes Musik Yggdmillennia","Gordes Musik Yggdmillennia",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[],
	   mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	["master_apocrypha_03","Fiore Forvedge Yggdmillennia","Fiore Forvedge Yggdmillennia",tf_female|tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[],
	   mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	["master_apocrypha_04","Celenike Icecolle Yggdmillennia","Celenike Icecolle Yggdmillennia",tf_female|tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[],
	   mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	["master_apocrypha_05","Roche Frain Yggdmillennia","Roche Frain Yggdmillennia",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[],  mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	["master_apocrypha_06","Caules Forvedge Yggdmillennia","Caules Forvedge Yggdmillennia",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[],  mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	["master_apocrypha_07","Reika Rikudou","Reika Rikudou",tf_female|tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[],  mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
		########## Red Faction
	["master_apocrypha_08","Kairi Sisigou","Kairi Sisigou",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[itm_sawedoff,itm_necro_grenade,itm_gandr_shell],  mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	
	##############Fate/ strange fake
	
	##############Fate/ Ex-
	#1a Hakuno Kishinami -male NERO or NAMELESS EMIYA
	["master_extra_01a","Hakuno Kishinami","Hakuno Kishinami",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[itm_fn_fal, itm_rifle_ammo], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	#1b Hakuno Kishinami -female NERO or NAMELESS EMIYA
	["master_extra_01b","Hakuno Kishinami","Hakuno Kishinami",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	#2 Rin Tohsaka -extra (fancy boy Cu Chulainn)
	["master_extra_02","Rin Tohsaka","Rin Tohsaka",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	#3 Rani VIII (Lu Bu, so a Berserker)
	["master_extra_03","Rani VIII","Rani VIII",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	#4 Shinji Matou (Francis Drake, RIDER)
	["master_extra_04","Shinji Matou","Shinji Matou",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	#5 Leonard B. Harway (Gawain, SABER)
	["master_extra_05","Leonard B. Harway","Leonard B. Harway",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	#6 Dan Blackmore (Robin Hood)
	["master_extra_06","Dan Blackmore","Dan Blackmore",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	#7 Alice (Storybook? Caster)
	["master_extra_07","Alice","Alice",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	#8 Julius B. Harway (Li Shuwen, Assassin)
	["master_extra_08","Julius B. Harway","Julius B. Harway",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	#9 Run Ru (Lancer, Vlad III)
	["master_extra_09","Run Ru","Run Ru",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	#10 Monji Gatou (Berserker, Arcueid Brunestud - Tskikiheme-)
	["master_extra_10","Monji Gatou","Monji Gatou",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	#11 Twice H. Pieceman (Saver, "Savior", Buddha)
	["master_extra_11","Twice H. Pieceman","Twice H. Pieceman",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	
	##############Fate/ Grand Order
	["master_fgo_male","Ritsuka Fujimaru","Ritsuka Fujimaru",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[],
	   mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	["master_fgo_female","Ritsuka Fujimaru","Ritsuka Fujimaru",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
	   
	##############Fate/ Prototype
		#Ayaka Sajyou The Seventh
	["master_proto_01","Ayaka Sajyou","Ayaka Sajyou",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
		#The Sixth, Powers
	["master_proto_02","The Sixth, Powers","The Sixth, Powers",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
		#The Fifth, Virtues
	["master_proto_03","The Fifth, Virtues","The Fifth, Virtues",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
		#The Forth, Dominons
	["master_proto_04","The Forth, Dominons","The Forth, Dominons",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
		#Aro Isemi The Third, Thrones
	["master_proto_05","Aro Isemi","Aro Isemi",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
		#Misaya Reiroukan - The Second, Cherubim
	["master_proto_06","Misaya Reiroukan","Misaya Reiroukan",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
		#Sancraid Phahn - VOID, Absences
	["master_proto_07","Sancraid Phahn","Sancraid Phahn",tf_hero|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
		#Manaka Sajyou - The First, Seraphim
	["master_proto_08","Manaka Sajyou","Manaka Sajyou",tf_hero|tf_female|tf_unmoveable_in_party_window|tf_guarantee_all, 0, reserved,  fac_commoners,[], mage_attrib,wp(70),knows_merchant_npc,
	   0x0000000d7f0400035915aa226b4d975200000000001ea49e0000000000000000],
		
	##############Fate/ Prototype - Silver Sky Fragments or whatever
		#Ayaka Sajyou The Seventh
		#The Sixth, Powers
		#The Fifth, Virtues
		#The Forth, Dominons
		#The Third, Thrones
		#The Second, Cherubim
		#The First, Seraphim
		#VOID, Absences

	
	["end_masters","I should never be seen","not a thing",tf_hero|tf_guarantee_shield|tf_guarantee_armor|tf_guarantee_boots,  0, reserved, fac_commoners,[],def_attrib|level(5),wp(20),knows_common,mage_face1], 
	

   #Non-Master Magi NPCs
		#Non-Factional
    ["summoner","I summon servants","Summoner",tf_hero|tf_guarantee_shield|tf_guarantee_armor|tf_guarantee_boots,  scn_fate_ahnenerbe|entry(1), reserved, fac_commoners,[],def_attrib|level(5),wp(20),knows_common,mage_face1],
		#Mages Association Doods
	["mage_enforcer", "Seal Designation Enforcer", "Seal Designation Enforcer", tf_guarantee_shield|tf_guarantee_armor|tf_guarantee_boots,  0, reserved, fac_commoners, [itm_azoth_sword],def_attrib|level(10),wp(120), knows_common, mage_face1],
	
		# Shopkeeper NPCs
		
		# Copenhagen
		# ## Master of Copenhagen
		# ## Otoko Hotaruzuka - Master's Daughter
	
		# Ahnenerbe
		# ## George - Owner, Collects Relics, Italian Food Master
		# ## Chikagi Katsuragi - Part Time Clerk, woman
		# ## Hibiki Hibino - Part Time clerk, woman
		
		# Mount Miyama
		# Koushuuensaikan Taizan
		# ## Batsu - Chef for the Spicy Mapo Tofu joint
		# Edomaeya - Sells little chocolate fish
		# Singing Birds Retreat
		# Flower Shop
		
		#Church
	["church_overseer","Grail War Overseer","Grail War Overseer",tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners,
		[],
		def_attrib|str_20|agi_16|level(20),wp(180),knows_common|knows_power_strike_5|knows_ironflesh_3,    bandit_face1, bandit_face2],
	
   #Troops
	#Church Troops
	["executor","Executor","Executors",tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners, #Essentially an Inquisitor
		[itm_black_key],
		def_attrib|str_20|agi_16|level(20), wp(180), church_member|knows_power_strike_5|knows_ironflesh_3, bandit_face1, bandit_face2],
	["high_executor","High Executor","High Executors",tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners, #Essentially an Inquisitor
		[itm_black_key],
		def_attrib|str_20|agi_16|level(35), wp(230), church_member|knows_power_strike_5|knows_ironflesh_3, bandit_face1, bandit_face2],
	["inquistitor","Inquisitor","Inquisitor",tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners,
		[itm_black_key],
		def_attrib|str_20|agi_16|level(35), wp(230), church_member|knows_power_strike_5|knows_ironflesh_3, bandit_face1, bandit_face2],
	["priest","Priest","Priest",tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners,
		[itm_black_key],
		def_attrib|str_20|agi_16|level(20), wp(80), church_member|knows_power_strike_5|knows_ironflesh_3, bandit_face1, bandit_face2],
	["bishop","Bishop","Bishop",tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners,
		[itm_black_key],
		def_attrib|str_20|agi_16|level(20), wp(100), church_member|knows_power_strike_5|knows_ironflesh_3, bandit_face1, bandit_face2],
	["archbishop","Archbishop","Archbishop",tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners,
		[itm_black_key],
		def_attrib|str_20|agi_16|level(20), wp(140), church_member|knows_power_strike_5|knows_ironflesh_3, bandit_face1, bandit_face2],
	["cardinal","Cardinal","Cardinal",tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners,
		[itm_black_key],
		def_attrib|str_20|agi_16|level(20), wp(180), church_member|knows_power_strike_5|knows_ironflesh_3, bandit_face1, bandit_face2],
	["exorcist","Exorcists","Exorcists",tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners,
		[itm_black_key],
		def_attrib|str_20|agi_16|level(20), wp(230), church_member|knows_power_strike_5|knows_ironflesh_3, bandit_face1, bandit_face2],
	["mage_executor","Mage-Hunter Executor","Mage-Hunter Executor",tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners,
		[itm_black_key],
		def_attrib|str_20|agi_16|level(20), wp(275), church_member|knows_power_strike_5|knows_ironflesh_3, bandit_face1, bandit_face2],
		
	# Vampires and the like
	["ghoul","Ghoul","Ghouls",tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners,
		[itm_black_key],
		def_attrib|str_20|agi_16|level(20), wp(70), knows_common|knows_power_strike_5|knows_ironflesh_3, bandit_face1, bandit_face2],
	["dead_apostle","Dead Apostle","Dead Apostles",tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners,
		[itm_black_key],
		def_attrib|str_20|agi_16|level(20), wp(180), knows_common|knows_power_strike_5|knows_ironflesh_3, bandit_face1, bandit_face2],
		
	# Familiar Summons
	 # Dragon Tooth Warrior
	["dtooth_warrior","Dragon Tooth Warrior","Dragon Tooth Warriors",tf_guarantee_all,no_scene,reserved,fac_commoners,
		[],
		str_6|agi_6|level(11), wp(80), knows_common, mercenary_face_1, mercenary_face_2],
	 # Dragon Tooth Archer
	["dtooth_archer","Dragon Tooth Archer","Dragon Tooth Archers",tf_guarantee_all,no_scene,reserved,fac_commoners,
	   [],
	   str_8|agi_6|level(14), wp(100), knows_common|knows_power_draw_4, mercenary_face_1, mercenary_face_2],
	 # Stone Golem
	["stone_golem","Stone Golem","Stone Golems",tf_large_servants|tf_guarantee_all|tf_unmoveable_in_party_window, 0, reserved, fac_commoners,
		[itm_heracles_wear,itm_heracles_armcuffs,itm_heracles_footcuffs,itm_axe_sword],
		berserker_attrib, wp(100), knows_common, berserker_face2],
	 # Puppet
	["puppet","Puppet","Puppet",tf_guarantee_boots|tf_guarantee_armor,no_scene,reserved,fac_commoners,
		[],
		str_6|agi_6|level(5), wp(80), knows_common, mercenary_face_1, mercenary_face_2],
	
	# ##### Fate Townspeople ###################################################################
	# Fuyuki - Casual, more suburban style clothing. Few Suits
	["fuyuki_walker_m","Townsman","Townsmen",tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners,
		[],
		def_attrib|level(4),wp(60),knows_common,man_face_young_1, man_face_old_2],
	["fuyuki_walker_f","Townswoman","Townswomen",tf_female|tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners,
		[],
		def_attrib|level(2),wp(40),knows_common,woman_face_1,woman_face_2],
		
	["fuyuki_student_m","Student","Student",tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners,
		[],
		def_attrib|level(4),wp(60),knows_common,man_face_young_1, man_face_old_2],
	["fuyuki_student_f","Student","Student",tf_female|tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners,
		[],
		def_attrib|level(2),wp(40),knows_common,woman_face_1,woman_face_2],
	
	# Tokyo - More eclectic clothing with a focus on fancier clothes
	["tokyo_walker_m","Townsman","Townsmen",tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners,
		[],
		def_attrib|level(4),wp(60),knows_common,man_face_young_1, man_face_old_2],
	["tokyo_walker_f","Townswoman","Townswomen",tf_female|tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners,
		[],
		def_attrib|level(2),wp(40),knows_common,woman_face_1,woman_face_2],
	
	# Snowfield - American style clothing. Maybe Snowfield doesn't have people? Check.
	["snowfield_walker_m","Townsman","Townsmen",tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners,
		[],
		def_attrib|level(4),wp(60),knows_common,man_face_young_1, man_face_old_2],
	["snowfield_walker_f","Townswoman","Townswomen",tf_female|tf_guarantee_boots|tf_guarantee_armor,0,0,fac_commoners,
		[],
		def_attrib|level(2),wp(40),knows_common,woman_face_1,woman_face_2],
	
	# # Police People #####
	["fuyuki_policeman", "Fuyuki City Policeman", "Fuyuki City Police", tf_guarantee_boots|tf_guarantee_armor, 0, 0, fac_commoners,
		[],
		def_attrib|level(14), wp(120), knows_common, man_face_young_1, man_face_old_2],
		
	["tokyo_policeman", "Tokyo Metropolitan Policeman", "Tokyo Metropolitan Police", tf_guarantee_boots|tf_guarantee_armor, 0, 0, fac_commoners,
		[],
		def_attrib|level(14), wp(120), knows_common, man_face_young_1, man_face_old_2],
	
	["snowfield_policeman", "Snowfield Sheriff", "Snowfield Sheriff", tf_guarantee_boots|tf_guarantee_armor, 0, 0, fac_commoners,
		[],
		def_attrib|level(14), wp(120), knows_common, man_face_young_1, man_face_old_2],
	
	["end_troops","Temp Troop","Temp Troop", 0, 0, 0, fac_commoners, [], 0, 0, 0, 0, 0],
]

# #######################################################################
#	Troop Tree Definitions
#		Pretty easy, for a troop that can only go one way:
#			upgrade(base_troop_id, upgraded_troop_id),
#		For a branching path:
#			upgrade(base_troop_id, upgraded_troop_id_1. upgraded_troop_id_2),
# #######################################################################