from header_common import *
from header_scene_props import *
from header_operations import *
from header_triggers import *
from header_sounds import *
from module_constants import *
import string


# #######################################################################
# ("prop-id", prop-flags, "mesh-name", "collider-name", [triggers]),
#
#  Each scene prop record contains the following fields:
#  1) Scene prop id: used for referencing scene props in other files. The prefix spr_ is automatically added before each scene prop id.
#  2) Scene prop flags. See header_scene_props.py for a list of available flags
#  3) Mesh name: Name of the mesh.
#  4) Physics object name:
#  5) Triggers: Simple triggers that are associated with the scene prop
# #######################################################################


scene_props = [

    # #######################################################################
	#	This is a list Theo left in, in its completion, I didn't remove items
	#	as I have no scenes of which to test this, but, I can bet there are
	#	more than a few that are not entirely necessary. Maybe v1.0.1
	#
	#				B A R E B O N E S - I E R
    # #######################################################################
	
  ("invalid_object", 0, "question_mark", "0", []),
  ("inventory", sokf_type_container|sokf_place_at_origin, "package", "bobaggage", []),
  ("empty", 0, "0", "0", []),
  ("chest_a", sokf_type_container, "chest_gothic", "bochest_gothic", []),
  ("container_small_chest", sokf_type_container,"package","bobaggage", []),
  ("container_chest_b", sokf_type_container, "chest_b","bo_chest_b", []),
  ("container_chest_c", sokf_type_container, "chest_c", "bo_chest_c", []),
  ("player_chest", sokf_type_container, "player_chest", "bo_player_chest", []),
  ("locked_player_chest", 0, "player_chest", "bo_player_chest", []),

  ("light_sun",sokf_invisible,"light_sphere","0",  [
     (ti_on_init_scene_prop,
      [
          (neg|is_currently_night),
          (store_trigger_param_1, ":prop_instance_no"),
          (set_fixed_point_multiplier, 100),
          (prop_instance_get_scale, pos5, ":prop_instance_no"),
          (position_get_scale_x, ":scale", pos5),
          (store_time_of_day,reg(12)),
          (try_begin),
            (is_between,reg(12),5,20),
            (store_mul, ":red", 5 * 200, ":scale"),
            (store_mul, ":green", 5 * 193, ":scale"),
            (store_mul, ":blue", 5 * 180, ":scale"),
          (else_try),
            (store_mul, ":red", 5 * 90, ":scale"),
            (store_mul, ":green", 5 * 115, ":scale"),
            (store_mul, ":blue", 5 * 150, ":scale"),
          (try_end),
          (val_div, ":red", 100),
          (val_div, ":green", 100),
          (val_div, ":blue", 100),
          (set_current_color,":red", ":green", ":blue"),
          (set_position_delta,0,0,0),
          (add_point_light_to_entity, 0, 0),
      ]),
    ]),
  ("light",sokf_invisible,"light_sphere","0",  [
     (ti_on_init_scene_prop,
      [
          (store_trigger_param_1, ":prop_instance_no"),
          (set_fixed_point_multiplier, 100),
          (prop_instance_get_scale, pos5, ":prop_instance_no"),
          (position_get_scale_x, ":scale", pos5),
          (store_mul, ":red", 3 * 200, ":scale"),
          (store_mul, ":green", 3 * 145, ":scale"),
          (store_mul, ":blue", 3 * 45, ":scale"),
          (val_div, ":red", 100),
          (val_div, ":green", 100),
          (val_div, ":blue", 100),
          (set_current_color,":red", ":green", ":blue"),
          (set_position_delta,0,0,0),
          (add_point_light_to_entity, 10, 30),
      ]),
    ]),
  ("light_red",sokf_invisible,"light_sphere","0",  [
     (ti_on_init_scene_prop,
      [
          (store_trigger_param_1, ":prop_instance_no"),
          (set_fixed_point_multiplier, 100),
          (prop_instance_get_scale, pos5, ":prop_instance_no"),
          (position_get_scale_x, ":scale", pos5),
          (store_mul, ":red", 2 * 170, ":scale"),
          (store_mul, ":green", 2 * 100, ":scale"),
          (store_mul, ":blue", 2 * 30, ":scale"),
          (val_div, ":red", 100),
          (val_div, ":green", 100),
          (val_div, ":blue", 100),
          (set_current_color,":red", ":green", ":blue"),
          (set_position_delta,0,0,0),
          (add_point_light_to_entity, 20, 30),
      ]),
    ]),
  ("light_night",sokf_invisible,"light_sphere","0",  [
     (ti_on_init_scene_prop,
      [
          (is_currently_night, 0),
          (store_trigger_param_1, ":prop_instance_no"),
          (set_fixed_point_multiplier, 100),
          (prop_instance_get_scale, pos5, ":prop_instance_no"),
          (position_get_scale_x, ":scale", pos5),
          (store_mul, ":red", 3 * 160, ":scale"),
          (store_mul, ":green", 3 * 145, ":scale"),
          (store_mul, ":blue", 3 * 100, ":scale"),
          (val_div, ":red", 100),
          (val_div, ":green", 100),
          (val_div, ":blue", 100),
          (set_current_color,":red", ":green", ":blue"),
          (set_position_delta,0,0,0),
          (add_point_light_to_entity, 10, 30),
      ]),
    ]),
  ("torch",0,"torch_a","0",[]),
  ("torch_night",0,"torch_a","0",[]),
  ("barrier_20m",sokf_invisible|sokf_type_barrier,"barrier_20m","bo_barrier_20m", []),
  ("barrier_16m",sokf_invisible|sokf_type_barrier,"barrier_16m","bo_barrier_16m", []),
  ("barrier_8m" ,sokf_invisible|sokf_type_barrier,"barrier_8m" ,"bo_barrier_8m" , []),
  ("barrier_4m" ,sokf_invisible|sokf_type_barrier,"barrier_4m" ,"bo_barrier_4m" , []),
  ("barrier_2m" ,sokf_invisible|sokf_type_barrier,"barrier_2m" ,"bo_barrier_2m" , []),
  
  ("exit_4m" ,sokf_invisible|sokf_type_barrier_leave,"barrier_4m" ,"bo_barrier_4m" , []),
  ("exit_8m" ,sokf_invisible|sokf_type_barrier_leave,"barrier_8m" ,"bo_barrier_8m" , []),
  ("exit_16m" ,sokf_invisible|sokf_type_barrier_leave,"barrier_16m" ,"bo_barrier_16m" , []),

  ("ai_limiter_2m" ,sokf_invisible|sokf_type_ai_limiter,"barrier_2m" ,"bo_barrier_2m" , []),
  ("ai_limiter_4m" ,sokf_invisible|sokf_type_ai_limiter,"barrier_4m" ,"bo_barrier_4m" , []),
  ("ai_limiter_8m" ,sokf_invisible|sokf_type_ai_limiter,"barrier_8m" ,"bo_barrier_8m" , []),
  ("ai_limiter_16m",sokf_invisible|sokf_type_ai_limiter,"barrier_16m","bo_barrier_16m", []),

    # #######################################################################
    # Other 
	# #######################################################################
	
#########################################################################################
############ Fate/ Scene Props i.e. Yew Tree etc ########################################
#########################################################################################

 ("shader_sphere", sokf_enforce_shadows, "Sphere", "0", []),
 ("shader_mesh", sokf_enforce_shadows, "WeirdSphere", "0", []),
 ("shader_cylinder", sokf_enforce_shadows, "Cylinder", "0", []),
 ("shader_monkey", sokf_enforce_shadows, "Suzanne", "0", []),
 
 ("aoe_marker", 0, "aoe_marker", "0", []),
 ("beam_marker", 0, "beam_marker", "0", []),
 ("arc_marker", 0, "arc_marker", "0", []),
 ("semi_marker", 0, "semi_marker", "0", []),
 ("cone_marker", 0, "cone_marker", "0", []),
 ("sphere_marker", 0, "sphere_marker", "0", []),
 
 ("alignment_test", 0, "beam", "0", []),
 
 
 ("bodytest", sokf_dynamic, "psSuzanne", "0", []),

("fate_yew_tree",sokf_show_hit_point_bar|sokf_destructible,"tree_a01","bo_tree_a01", 
	[
	
	(ti_on_init_scene_prop,
		[
		  (store_trigger_param_1, ":instance_no"),
		  (scene_prop_set_hit_points, ":instance_no", 250),
		  (particle_system_add_new, "psys_yew_poison_smoke"),
		]),
	
	(ti_on_scene_prop_destroy,
      [
        (store_trigger_param_1, ":instance_no"),
        (prop_instance_get_position, pos1, ":instance_no"),
        (copy_position, pos2, pos1),
        (position_set_z, pos2, -100000),
        (prop_instance_animate_to_position, ":instance_no", pos2, 1),
        (play_sound, "snd_hit_wood_wood"),
        ]),
    
     (ti_on_scene_prop_hit,
		[
		  (store_trigger_param_1, ":instance_no"),       
		  (store_trigger_param_2, ":damage"),
		  
		  (try_begin),
			(scene_prop_get_hit_points, ":hit_points", ":instance_no"),
			(val_sub, ":hit_points", ":damage"),
			(gt, ":hit_points", 0),
			(assign, reg10, ":hit_points"),
			(assign, reg11, ":damage"),
			(play_sound, "snd_hit_wood_wood"),
		  (else_try),
			(neg|multiplayer_is_server),
			(play_sound, "snd_hit_wood_wood"),
		  (try_end),

		  (try_begin),
			(this_or_next|multiplayer_is_server),
			(neg|game_in_multiplayer_mode),

			(particle_system_burst, "psys_game_hoof_dust", pos1, 3),
			(particle_system_burst, "psys_game_hoof_dust", pos1, 10),
		  (try_end),      
		]),  
	]),
 
 ("fate_scene_prefab", sokf_invisible, "edit_cursor", "0", 
	[
    (ti_on_init_scene_prop,
     [
		(store_trigger_param_1, ":instance_no"),
		(prop_instance_get_variation_id, ":variance_id", ":instance_no"),
		(prop_instance_get_variation_id_2, ":variance_id2", ":instance_no"),
        (prop_instance_get_position, pos1, ":instance_no"),
		
        
        
		(assign, reg15, ":variance_id"),
		# (display_message, "@{reg15}"),
		
		# (neg|is_edit_mode_enabled),
		(try_begin),
			(eq, ":variance_id", -1),
			#(spawn_agent, "trp_"),
		(else_try),
			(eq, ":variance_id", 0),
			# (is_edit_mode_enabled),
			# (set_fixed_point_multiplier, 100),
			# (init_position, pos15),
			# (try_for_range, ":rows", 0, 100),
			
			# (position_move_z, pos15, 1000),
			# (store_mul, ":grid_x", 100, ":rows"),
			# (position_move_x, pos15, ":grid_x"),
				# (try_for_range, ":columns", 0, 100),
					# (store_mul, ":grid_y", 100, ":columns"),
					# (position_move_y, pos15, ":grid_y"),
					# (position_set_z_to_ground_level, pos15),
					# (set_spawn_position, pos15),
					# (spawn_scene_prop, "spr_gizmo"),
				# (try_end),
			# (try_end),
		(else_try),
			(eq, ":variance_id", 1),
			(spawn_agent, "trp_ghoul"),
		(else_try),
			(eq, ":variance_id", 2),
			(spawn_agent, "trp_dead_apostle"),
		(else_try),
			(eq, ":variance_id", 3),
			(spawn_agent, "trp_fuyuki_walker_m"),
		(else_try),
			(eq, ":variance_id", 4),
			(spawn_agent, "trp_fuyuki_policeman"),
		(else_try),
			(eq, ":variance_id", 5),
			(spawn_agent, "trp_tokyo_policeman"),
		(else_try),
			(eq, ":variance_id", 6),
			(try_begin),
			(ge, ":variance_id2", 1),
				(try_for_range, ":difference", 0, ":variance_id2"),
					(position_set_z_to_ground_level, pos1),
					(set_spawn_position, pos1),
					(spawn_scene_prop, "spr_emiya_house_wall"),
					#(val_add, ":difference", 495),
					(position_move_x, pos1, 200),
				(try_end),
			(else_try),
				(spawn_scene_prop, "spr_emiya_house_wall"),
			(try_end),
		(else_try),
			(spawn_agent, "trp_bishop"),
		(try_end),
	 ]
	)]),
	
  ("emiya_house_wall",0,"wall","bo_wall", []),
  ("emiya_house",0,"house","bo_house", []),
  ("emiya_house_shed",0,"shed","bo_shed", []),
  ("emiya_house_backhouse",0,"backhouse","bo_backhouse", []),
  ("emiya_house_gatehouse",0,"gatehouse","bo_gatehouse", []),
  
  ("maze",0,"maze","bo_maze", []),
  
  ("aestus", 0, "aestus", "bo_aestus", []),
  
  ("marble_cloudy", 0, "marble_cloudy", "0", [
	(ti_on_init_scene_prop,
		[
		(store_trigger_param_1, ":instance_no"),
		(scene_prop_set_visibility, ":instance_no", 0),
	
		]),
  ]),
  
  
  ("saber_background", 0, "SaberBG", "bo_billboard", []),
  
  ("news", 0, "0", "0", [
  ]),
  
  ("trigger", sokf_invisible, "barrier_2m", "bo_barrier_2m", [
	# (ti_on_init_scene_prop,
		# [
		  # (store_trigger_param_1, ":instance_no"),
		  
		  # (scene_prop_set_visibility, ":instance_no", 0),
		# ]),
  ]),
  
  
  ("reality_marble", 0, "marble", "bo_marble", [
  (ti_on_init_scene_prop,
		[
		(set_fixed_point_multiplier, 1000),
		(store_trigger_param_1, ":instance_no"),
		#(prop_instance_enable_physics, ":instance_no", 0),
		#(scene_prop_set_visibility, ":instance_no", 0),
		
		(prop_instance_get_position, pos1, ":instance_no"),
		
		(try_for_range, ":i", 0, 4),
			(copy_position, pos2, pos1),
			(store_mul, ":rotation", 90, ":i"),
			(position_rotate_z, pos2, ":rotation", 1),
			(position_move_y, pos2, 3000),
			(position_move_z, pos2, 900),
			(set_spawn_position, pos2),
			(spawn_scene_prop, "spr_barrier_20m"),
			(prop_instance_set_scale, reg0, 3000, 100, 700),
			(scene_prop_set_visibility, reg0, 0),
			#(val_add, ":difference", 495),
			#(position_move_x, pos1, 500),
		(try_end),
		
		(set_spawn_position, pos1),
		(spawn_scene_prop, "spr_marble_cloudy"),
		]),
  
  ]),
  ("gizmo",sokf_invisible,"edit_cursor","0", []),
]
