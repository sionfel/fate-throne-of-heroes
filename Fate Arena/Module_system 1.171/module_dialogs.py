from header_common import *
from header_dialogs import *
from header_operations import *
from header_parties import *
from header_item_modifiers import *
from header_skills import *
from header_triggers import *
from ID_troops import *
from ID_party_templates import *
from module_constants import *

# #######################################################################
# [speaker_troop_id, "dialog_state", [(conditions_block)], "dialog_string", "ending_dialog_state", [consequences_block], "voice_acting_string"],
#
# During a dialog, the dialog lines are scanned from top to bottom.
# If the dialog-line is spoken by the player, all the matching lines are displayed for the player to pick from.
# If the dialog-line is spoken by another, the first (top-most) matching line is selected.
#
#  Each dialog line contains the following fields:
# 1) Dialogue partner: This should match the person player is talking to.
#    Usually this is a troop-id.
#    You can also use a party-template-id by appending '|party_tpl' to this field.
#    Use the constant 'anyone' if you'd like the line to match anybody.
#    Appending '|plyr' to this field means that the actual line is spoken by the player
#    Appending '|other(troop_id)' means that this line is spoken by a third person on the scene.
#       (You must make sure that this third person is present on the scene)
#
# 2) Starting dialog-state:
#    During a dialog there's always an active Dialog-state.
#    A dialog-line's starting dialog state must be the same as the active dialog state, for the line to be a possible candidate.
#    If the dialog is started by meeting a party on the map, initially, the active dialog state is "start"
#    If the dialog is started by speaking to an NPC in a town, initially, the active dialog state is "start"
#    If the dialog is started by helping a party defeat another party, initially, the active dialog state is "party_relieved"
#    If the dialog is started by liberating a prisoner, initially, the active dialog state is "prisoner_liberated"
#    If the dialog is started by defeating a party led by a hero, initially, the active dialog state is "enemy_defeated"
#    If the dialog is started by a trigger, initially, the active dialog state is "event_triggered"
#	 If the dialog is started by speaking to a member of your party the dialog state is "member_chat"
# "party_encounter" : 
# "close_window" : If chosen as the ending dialogue state, it will well, close the dialog window
# "trade" : 
# "exchange_members" : 
# "trade_prisoners" : 
# "buy_mercenaries" : 
# "view_char" : 
# "training" : 
# "prisoner_chat" : 

# 3) Conditions block (list): This must be a valid operation block. See header_operations.py for reference.  
# 4) Dialog Text (string):
# 5) Ending dialog-state:
#    If a dialog line is picked, the active dialog-state will become the picked line's ending dialog-state.
# 6) Consequences block (list): This must be a valid operation block. See header_operations.py for reference.
# 7) Voice-over (string): sound filename for the voice over. Leave here empty for no voice over
# #######################################################################

# #######################################################################
#		NOTE: I couldn't easily find a use in Native of the 
#		voice acting string so everything after [consequences_block] 
#		in the added schema is entirely speculation.
# #######################################################################




dialogs = [

    [anyone|plyr, "start", [], "Dialog Error. No dialog found.", "close_window", []],
	
#############################################################################
####FATE/RIDER SABER DIALOGUE
#############################################################################

 [trp_summoner, "start", [
	(troop_slot_eq, "$g_talk_troop", slot_troop_met_previously, 0),
	], "Good day to you, {young man/lassie}.", "summoner_intro_1",[]
	],
	
 [trp_summoner, "start", [
	], "Good day to you, {playername}. Got any new summons for me?", "summoner_convo_1",[]
	],
	
 [trp_summoner|plyr, "summoner_intro_1", [], 
	"Forgive me, rumor is that you can help me obtain a servant since Sionfel doesn't know how this game works anymore.",
	"summoner_intro_2",
	[(troop_set_slot, "$g_talk_troop", slot_troop_met_previously, 1),]
	],
	
[trp_summoner, "summoner_intro_2", [
	], "You know, I think I can, assuming you have the right items.", "summoner_convo_1",[]
	],
	
   
[trp_summoner|plyr|repeat_for_100,"summoner_convo_1", [
	(store_repeat_object,":catalysts"),
      (val_add,":catalysts",servant_catalysts_begin),
      (is_between,":catalysts",servant_catalysts_begin,servant_catalysts_end),
      (player_has_item,":catalysts"),
      (str_store_item_name,s5,":catalysts"),
	(player_has_item, ":catalysts"),
   ],
   "I have the {s5}.", "summon_servant",[
   (store_repeat_object,":catalysts"),
	  (val_add,":catalysts",servant_catalysts_begin),
      (str_store_item_name,s6,":catalysts"),
	  (item_get_slot, ":summoned_servant", ":catalysts", slot_fate_catalyst_servant),
	  (troop_remove_item, "trp_player", ":catalysts"),
	  (party_add_members, "p_main_party", ":summoned_servant", 1),
	  (str_store_troop_name, s10, ":summoned_servant"),
	  (troop_set_slot, ":summoned_servant", slot_fate_master, "trp_player"),
   ]], 
   
[trp_summoner,"summon_servant", [],
   "You gave away a {s6} summoning the servant called {s10}", "close_window",[
   (display_message, "@{!}{s10} Joins the Party"),
   ]],    
   
[trp_summoner|plyr, "summoner_convo_1", [], 
	"I will be seeing you, dear friend.", "close_window",[]
	],
	
##############################################################################
#### FATE SABER RIDER Servant Conversation Threads
##############################################################################   
  [anyone, "start", [
	 (is_between, "$g_talk_troop", servants_begin, servants_end),
	 ], 
	 "Hey, Master {playername}, I understand the need to conversate but can't it wait? We do not know if an enemy servant might attack at any moment and I would prefer to not be caught off guard.", "close_window",
	 [(troop_set_slot, "$g_talk_troop", slot_troop_met_previously, 1),]
	 ],	

############member_chat is talking from the party window
  [anyone, "member_chat", [
	  (is_between, "$g_talk_troop", servants_begin, servants_end),
	  (troop_slot_eq, "$g_talk_troop", slot_troop_met_previously, 1),
	  (str_clear, s12),
	  (str_clear, s13),
	  (str_store_troop_name, s13, "$g_talk_troop"),
	  (troop_get_slot, ":greet", "$g_talk_troop", slot_fate_greet),
	  (str_store_string, s12, ":greet"),
	 ], 
	 "{s12}", "servant_dialog",
	 []
	 ],
	 
 [anyone, "member_chat", [
	 (is_between, "$g_talk_troop", servants_begin, servants_end),
	 (troop_slot_eq, "$g_talk_troop", slot_troop_met_previously, 0),
	 (str_clear, s12),
	 (str_clear, s13),
	 (str_store_troop_name, s13, "$g_talk_troop"),
	 (troop_get_slot, ":greet", "$g_talk_troop", slot_fate_initial_greet),
	 (str_store_string, s12, ":greet"),
	 ], 
	 "{s12}", "servant_intro",
	 [(troop_set_slot, "$g_talk_troop", slot_troop_met_previously, 1),]
	 ],
  [anyone|plyr, "servant_intro", [
	 ], 
	 "Hello, {s13}. I am {playername}, lets do this vow bs.", "servant_dialog",
	 []
	 ],
	 
  
 [anyone, "servant_dialog", [],
	 "This is generic servant dialog.", "servant_dialog2",
	 []
	],	
	
 [anyone|plyr, "servant_dialog2", [
	(troop_slot_eq, "$g_talk_troop", slot_fate_true_name_revealed, 0),
	],
	 "Tell me your name, loser", "servant_name_reveal",
	 [
	 (str_clear, s12),
	 (str_clear, s13),
	 (str_clear, s14),
	 (troop_get_slot, ":truename", "$g_talk_troop", slot_fate_true_name),
	 (troop_get_slot, ":greet", "$g_talk_troop", slot_fate_reveal_greet),
	 (troop_get_slot, ":speech", "$g_talk_troop", slot_fate_summon_speech),
	 (troop_set_name, "$g_talk_troop", ":truename"),
	 (str_store_string, s12, ":greet"),
	 (str_store_troop_name, s13, "$g_talk_troop"),
	 (str_store_string, s14, ":speech"),]
	 ],
	 
  [anyone|plyr, "servant_dialog2", [],
	 "Tell me your slots", "servant_debug",
	 [
	 (str_clear, s10),
	 (str_clear, s11),
	 (str_clear, s12),
	 
	 (troop_get_slot, ":truename", "$g_talk_troop", slot_fate_true_name),
	 (troop_get_slot, ":alignment", "$g_talk_troop", slot_fate_alignment),
	 (troop_get_slot, ":class", "$g_talk_troop", slot_fate_servant_class),
	 (troop_get_slot, ":height", "$g_talk_troop", slot_fate_height),
	 (troop_get_slot, ":weight", "$g_talk_troop", slot_fate_weight),
	 (troop_get_slot, ":mana", "$g_talk_troop", slot_fate_max_mana),
	 
	 (troop_get_slot, ":truenamerevealed", "$g_talk_troop", slot_fate_true_name_revealed),
	 (troop_get_slot, ":infotier", "$g_talk_troop", slot_fate_info_tier),
	 (troop_get_slot, ":nobleused", "$g_talk_troop", slot_fate_noble_phantasm_used),
	 (troop_get_slot, ":classification", "$g_talk_troop", slot_fate_classification),
	 
	 (str_store_string, s10, ":truename"),
	 (str_store_string, s11, ":alignment"),
	 (str_store_string, s12, ":class"),
	 (str_store_string, s13, ":classification"),
	 (assign, reg10, ":height"),
	 (assign, reg11, ":weight"),
	 (assign, reg12, ":mana"),
	 (assign, reg13, ":truenamerevealed"),
	 (assign, reg14, ":infotier"),
	 (assign, reg15, ":nobleused"),
	 ]
	],
 [anyone|plyr, "servant_dialog2", [],
	 "Tell me your servant skills", "servant_debug2",
	 [
	 	 
	 (troop_get_slot, ":skill1", "$g_talk_troop", fate_skill_slot_01),
	 (troop_get_slot, ":skill2", "$g_talk_troop", fate_skill_slot_02),
	 (troop_get_slot, ":skill3", "$g_talk_troop", fate_skill_slot_03),
	 (troop_get_slot, ":skill4", "$g_talk_troop", fate_skill_slot_04),
	 (troop_get_slot, ":skill5", "$g_talk_troop", fate_skill_slot_05),
	 (troop_get_slot, ":skill6", "$g_talk_troop", fate_skill_slot_06),
	 (troop_get_slot, ":skill7", "$g_talk_troop", fate_skill_slot_07),
	 (troop_get_slot, ":skill8", "$g_talk_troop", fate_skill_slot_08),
	 (troop_get_slot, ":skill1lvl", "$g_talk_troop", fate_skill_slot_01_level),
	 (troop_get_slot, ":skill2lvl", "$g_talk_troop", fate_skill_slot_02_level),
	 (troop_get_slot, ":skill3lvl", "$g_talk_troop", fate_skill_slot_03_level),
	 (troop_get_slot, ":skill4lvl", "$g_talk_troop", fate_skill_slot_04_level),
	 (troop_get_slot, ":skill5lvl", "$g_talk_troop", fate_skill_slot_05_level),
	 (troop_get_slot, ":skill6lvl", "$g_talk_troop", fate_skill_slot_06_level),
	 (troop_get_slot, ":skill7lvl", "$g_talk_troop", fate_skill_slot_07_level),
	 (troop_get_slot, ":skill8lvl", "$g_talk_troop", fate_skill_slot_08_level),
	 
	 (str_store_string, s10, ":skill1"),
	 (str_store_string, s11, ":skill2"),
	 (str_store_string, s12, ":skill3"),
	 (str_store_string, s13, ":skill4"),
	 (str_store_string, s14, ":skill5"),
	 (str_store_string, s15, ":skill6"),
	 (str_store_string, s16, ":skill7"),
	 (str_store_string, s17, ":skill8"),
	 
	 (assign, reg10, ":skill1lvl"),
	 (assign, reg11, ":skill2lvl"),
	 (assign, reg12, ":skill3lvl"),
	 (assign, reg13, ":skill4lvl"),
	 (assign, reg14, ":skill5lvl"),
	 (assign, reg15, ":skill6lvl"),
	 (assign, reg16, ":skill7lvl"),
	 (assign, reg17, ":skill8lvl"),

	 ]
	],
	
 [anyone|plyr, "servant_dialog2", [],
	 "Tell me your servant parameters", "servant_debug3",
	 [
	 	 
	 (troop_get_slot, ":str", "$g_talk_troop", fate_param_str),
	 (troop_get_slot, ":end", "$g_talk_troop", fate_param_end),
	 (troop_get_slot, ":agi", "$g_talk_troop", fate_param_agi),
	 (troop_get_slot, ":mgc", "$g_talk_troop", fate_param_mgc),
	 (troop_get_slot, ":lck", "$g_talk_troop", fate_param_lck),
	 (troop_get_slot, ":npm", "$g_talk_troop", fate_param_npm),
	 
	 (assign, reg10, ":str"),
	 (assign, reg11, ":end"),
	 (assign, reg12, ":agi"),
	 (assign, reg13, ":mgc"),
	 (assign, reg14, ":lck"),
	 (assign, reg15, ":npm"),

	 ]
	],
	
 [anyone|plyr, "servant_dialog2", [(troop_slot_eq, "$g_talk_troop", slot_fate_info_tier, 0),],
	 "I'm going to trick you to think you know me more", "servant_dialog5",
	 [
	 (troop_set_slot, "$g_talk_troop", slot_fate_info_tier, 1),
	 ]
	],
	
 [anyone|plyr, "servant_dialog2", [(troop_slot_eq, "$g_talk_troop", slot_fate_info_tier, 1),],
	 "I'm going to trick you to think you know me even more", "servant_dialog5",
	 [
	 (troop_set_slot, "$g_talk_troop", slot_fate_info_tier, 2),
	 ]
	],
 [anyone|plyr, "servant_dialog2", [(troop_slot_eq, "$g_talk_troop", slot_fate_info_tier, 2),],
	 "I'm going to trick you to think you know me even, even more", "servant_dialog5",
	 [
	 (troop_set_slot, "$g_talk_troop", slot_fate_info_tier, 3),
	 ]
	],
 [anyone|plyr, "servant_dialog2", [(troop_slot_eq, "$g_talk_troop", slot_fate_info_tier, 3),],
	 "I'm going to trick you to think you know the most", "servant_dialog5",
	 [
	 (troop_set_slot, "$g_talk_troop", slot_fate_info_tier, 4),
	 ]
	],
 [anyone|plyr, "servant_dialog2", [(troop_slot_eq, "$g_talk_troop", slot_fate_info_tier, 4),],
	 "I'm going to reset how much I know you", "servant_dialog5",
	 [
	 (troop_set_slot, "$g_talk_troop", slot_fate_info_tier, 0),
	 ]
	],
  [anyone, "servant_dialog5", [],
	 "okay", "servant_dialog2",
	 []],
	 
  # [anyone|plyr,"servant_dialog2", [], #I Added this for debuging EMiya being a punk and not equipping shit

   # "Let me see your equipment.", "member_trade",[]],
	
 [anyone|plyr, "servant_dialog2", [],
	 "BYE!", "close_window",
	 []
	],
 [anyone, "servant_name_reveal", [],
	   "{s12} {s13}, {s14}.", "servant_dialog2",
	   [
	   (troop_set_slot, "$g_talk_troop", slot_fate_true_name_revealed, 1),
	   ]
	  ],
  [anyone, "servant_debug", [],
	   "Hey, {playername}^ I am {s10}^ I am a {s13}^ I have {s11} alignment^ I am a {s12} class servant^ I am {reg10}cm tall^ I weigh {reg11}kg^ I have {reg12} units of mana^ I {reg13?have:have not} revealed my name^ I {reg15?have:have not} used my Noble Phantasm^ You are at the {reg14} info tier with me.", 
	   "servant_dialog2",
	   []
	  ],
  [anyone, "servant_debug2", [],
	   "Hey, {playername}^ My Skills are as follows^ {s10} : Rank {reg10}^ {s11} : Rank {reg11}^ {s12} : Rank {reg12}^ {s13} : Rank {reg13}^ {s14} : Rank {reg14}^ {s15} : Rank {reg15}^ {s16} : Rank {reg16}^ {s17} : Rank {reg17}", 
	   "servant_dialog2",
	   []
	  ],
  [anyone, "servant_debug3", [],
	   "Hey, {playername}^ My Parameters are as follows^ Strength: {reg10}^ Endurance: {reg11}^ Agility: {reg12}^ Magic: {reg13}^ Luck: {reg14}^ Noble Phantasm: {reg15}", 
	   "servant_dialog2",
	   []
	  ],
	
]