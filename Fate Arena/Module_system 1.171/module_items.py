﻿from module_constants import *
from ID_factions import *
from header_items import  *
from header_operations import *
from header_triggers import *

# #######################################################################
#	["item_id", "Item_name_string", [("mesh_name",modifier)], flags, capabilities, value, stats, modifiers, [triggers], [faction_id]],
#
#  Each item record contains the following fields:
#  1) Item id: used for referencing items in other files.
#     The prefix itm_ is automatically added before each item id.
#  2) Item name. Name of item as it'll appear in inventory window
#  3) List of meshes.  Each mesh record is a tuple containing the following fields:
#    3.1) Mesh name.
#    3.2) Modifier bits that this mesh matches.
#     Note that the first mesh record is the default.
#  4) Item flags. See header_items.py for a list of available flags.
#  5) Item capabilities. Used for which animations this item is used with. 
#						 See header_items.py for a list of available flags.
#  6) Item value.
#  7) Item stats: Bitwise-or of various stats about the item such as:
#      weight, abundance, difficulty, head_armor, body_armor,leg_armor, etc...
#  8) Modifier bits: Modifiers that can be applied to this item.
#  9) [Optional] Triggers: List of simple triggers to be associated with the item.
#  10) [Optional] Factions: List of factions that item can be found as merchandise.
# #######################################################################


# #######################################################################
# 	imodbits/constants declarations
#
#	You can use use this as a way of declaring multiple imodbits for 
#	convenience/ ease of use. Native MS uses this as a way to combine
#	the hardcoded imodbits from header_items for ease of use.
# #######################################################################


imodbits_none = 0
imodbits_horse_basic = imodbit_swaybacked|imodbit_lame|imodbit_spirited|imodbit_heavy|imodbit_stubborn
imodbits_cloth  = imodbit_tattered | imodbit_ragged | imodbit_sturdy | imodbit_thick | imodbit_hardened
imodbits_armor  = imodbit_rusty | imodbit_battered | imodbit_crude | imodbit_thick | imodbit_reinforced |imodbit_lordly
imodbits_plate  = imodbit_cracked | imodbit_rusty | imodbit_battered | imodbit_crude | imodbit_thick | imodbit_reinforced |imodbit_lordly
imodbits_polearm = imodbit_cracked | imodbit_bent | imodbit_balanced
imodbits_shield  = imodbit_cracked | imodbit_battered |imodbit_thick | imodbit_reinforced
imodbits_sword   = imodbit_rusty | imodbit_chipped | imodbit_balanced |imodbit_tempered
imodbits_sword_high   = imodbit_rusty | imodbit_chipped | imodbit_balanced |imodbit_tempered|imodbit_masterwork
imodbits_axe   = imodbit_rusty | imodbit_chipped | imodbit_heavy
imodbits_mace   = imodbit_rusty | imodbit_chipped | imodbit_heavy
imodbits_pick   = imodbit_rusty | imodbit_chipped | imodbit_balanced | imodbit_heavy
imodbits_bow = imodbit_cracked | imodbit_bent | imodbit_strong |imodbit_masterwork
imodbits_crossbow = imodbit_cracked | imodbit_bent | imodbit_masterwork
imodbits_missile   = imodbit_bent | imodbit_large_bag
imodbits_thrown   = imodbit_bent | imodbit_heavy| imodbit_balanced| imodbit_large_bag
imodbits_thrown_minus_heavy = imodbit_bent | imodbit_balanced| imodbit_large_bag

imodbits_horse_good = imodbit_spirited|imodbit_heavy
imodbits_good   = imodbit_sturdy | imodbit_thick | imodbit_hardened | imodbit_reinforced
imodbits_bad    = imodbit_rusty | imodbit_chipped | imodbit_tattered | imodbit_ragged | imodbit_cracked | imodbit_bent

items = [

# #######################################################################
# 	Hardcoded items, according to Native MS
# #######################################################################
	
	["no_item", "INVALID ITEM", [("invalid_item",0)], 0, 0, 1, 0, 0],
	["tutorial_spear", "INVALID ITEM", [("invalid_item",0)], 0, 0, 1, 0, 0],
	["tutorial_club", "INVALID ITEM", [("invalid_item",0)], 0, 0, 1, 0, 0],
	["tutorial_battle_axe", "INVALID ITEM", [("invalid_item",0)], 0, 0, 1, 0, 0],
	["tutorial_arrows", "INVALID ITEM", [("invalid_item",0)], 0, 0, 1, 0, 0],
	["tutorial_bolts", "INVALID ITEM", [("invalid_item",0)], 0, 0, 1, 0, 0],
	["tutorial_short_bow", "INVALID ITEM", [("invalid_item",0)], 0, 0, 1, 0, 0],
	["tutorial_crossbow", "INVALID ITEM", [("invalid_item",0)], 0, 0, 1, 0, 0],
	["tutorial_throwing_daggers", "INVALID ITEM", [("invalid_item",0)], 0, 0, 1, 0, 0],
	["tutorial_saddle_horse", "INVALID ITEM", [("invalid_item",0)], 0, 0, 1, 0, 0],
	["tutorial_shield", "INVALID ITEM", [("invalid_item",0)], 0, 0, 1, 0, 0],
	["tutorial_staff_no_attack", "INVALID ITEM", [("invalid_item",0)], 0, 0, 1, 0, 0],
	["tutorial_sword", "INVALID ITEM", [("invalid_item",0)], 0, 0, 1, 0, 0],
	["tutorial_axe", "INVALID ITEM", [("invalid_item",0)], 0, 0, 1, 0, 0],
	["tutorial_dagger", "INVALID ITEM", [("invalid_item",0)], 0, 0, 1, 0, 0],
	["horse_meat", "INVALID ITEM", [("invalid_item",0)], 0, 0, 1, 0, 0],

# #######################################################################
#	This is as far as the engine requires, so feel free to go wild!!!
# #######################################################################

# ##################################################################
# ## Fate/rider saber Items
# ##################################################################

#Servants Items
 #Generalized Gear
 #Saber Gear
	#Artoria Pendragon
	
	# # For so-called laser sabers we'll use a script that allows you to ####
	# ## enter a width of beam and length of beam. this will allow us to ####
	# ## use agent_get_look_position to draw rectangle to check if agents are
	# ## inside and deal damage in those cases then take that info for ######
	# ## particle_system_burst to show off the laser. Look for a better #####
	# ## solutions ##########################################################
	
		#Excalibur, basic
	["excalibur", "Excalibur", [("excalibur",0)], itp_no_blur|itp_type_two_handed_wpn|itp_can_knock_down|itp_two_handed|itp_unique|itp_primary, itc_bastardsword|itcf_carry_sword_back, 526, weight(2.25)|difficulty(9)|spd_rtng(80) | weapon_length(115)|swing_damage(32, cut) | thrust_damage(27, pierce),imodbits_sword_high ],
	
		#Excalibur - Invisible Wind
	["excalibur_invis", "Excalibur", [("excalibur_invis",0)], itp_no_blur|itp_type_two_handed_wpn|itp_can_knock_down|itp_two_handed|itp_unique|itp_primary, itc_bastardsword|itcf_carry_sword_back, 526, weight(2.25)|difficulty(9)|spd_rtng(80) | weapon_length(115)|swing_damage(40, cut) | thrust_damage(37, pierce),imodbits_sword_high,
		[(ti_on_init_item, [
			(store_trigger_param_1, ":agent"),
			(neq, ":agent", -1),
            (agent_get_position, pos0, ":agent"),
			
			(try_for_range, reg4, 10, 120),
				(set_position_delta, pos0, reg4, 0, 0), #Position, X, Y, Z.
				(particle_system_add_new, "psys_invisible_air", pos0),
				(val_add, reg4, 10),
			(try_end),])]],
			
		# Excalibur - Activated
	["excalibur_gold", "Excalibur (Activated)", [("excalibur",0)], itp_no_blur|itp_type_two_handed_wpn|itp_can_knock_down|itp_two_handed|itp_unique|itp_primary, itc_bastardsword|itcf_carry_sword_back, 526 , weight(2.25)|difficulty(9)|spd_rtng(80) | weapon_length(115)|swing_damage(52, cut) | thrust_damage(42, pierce),imodbits_sword_high ],
	
		#Caliburn
	["caliburn", "Caliburn", [("caliburn",0),], itp_no_blur|itp_type_two_handed_wpn|itp_can_knock_down|itp_two_handed|itp_unique|itp_primary, itc_bastardsword|itcf_carry_sword_back,
	526 , weight(2.25)|difficulty(0)|spd_rtng(80) | weapon_length(110)|swing_damage(31, cut) | thrust_damage(27, pierce),imodbits_sword_high ],
	
		#Avalon
	["avalon", "Avalon", [("norman_shield_3",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
	
		#armor
	["artoria_plate", "Plate Armor", [("artoriaarmor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],
	
		#dope gauntlets
	["artoria_gauntlets","Gauntlets", [("artoria_gaunt_L",0),("artoria_gaunt_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],
	
		#shoes
	["artoria_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ], 
	
	#Nero
		# Aestus Estus
	["aestus_estus",  "Aestus Estus", [("aestus_estus_updated",0),], itp_no_blur|itp_type_two_handed_wpn|itp_two_handed|itp_unique|itp_primary, itc_nodachi|itcf_carry_sword_back, 920, weight(3.5)|difficulty(0)|spd_rtng(75) | weapon_length(152)|swing_damage(43, cut) | thrust_damage(0, pierce),imodbits_axe ],
	
		#red dress
	["nero_dress", "Red Dress", [("red_dress",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],
	
		#shoes
	["nero_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],
	
	#Siegfried
		#D-slayer sword
	["balmung", "Balmung", [("balmung",0),("scab_bastardsw_b", ixmesh_carry)], itp_no_blur|itp_type_two_handed_wpn|itp_unique|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn,
	524, weight(1)|difficulty(0)|spd_rtng(94) | weapon_length(130)|swing_damage(57, cut) | thrust_damage(41,  pierce),imodbits_sword_high],
	
		#slutty Armor 
	["sieg_plate", "Plate Armor", [("full_plate_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],
	
		#gauntlets
	["sieg_gauntlets","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],
	
		#boots
	["sieg_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],
	
	#Fergus mac Roich
		#rainbow sword
	["caladbolg", "Caladbolg", [("caladbolg",0)], itp_no_blur|itp_type_two_handed_wpn|itp_unique|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back, 524, weight(1)|difficulty(0)|spd_rtng(94) | weapon_length(165)|swing_damage(57, cut) | thrust_damage(41, pierce), imodbits_sword_high ],
	
		#clothes
	["fergus_vest", "Fergus' Digs", [("archers_vest",0)], itp_unique|itp_type_body_armor|itp_covers_legs, 0, 260, weight(6)|abundance(0)|head_armor(0)|body_armor(72)|leg_armor(20)|difficulty(0) ,imodbits_cloth],
	
		#gauntlets
	["fergus_gauntlets","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],
	
		#boots
	["fergus_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],
	
	#Richard Lionheart
		#Not Excalibur but yes
	["excalibur_lion", "Excalibur", [("excalibur",0),], itp_no_blur|itp_type_two_handed_wpn|itp_can_knock_down|itp_two_handed|itp_unique|itp_primary, itc_bastardsword|itcf_carry_sword_back, 526 , weight(2.25)|difficulty(9)|spd_rtng(80) | weapon_length(115)|swing_damage(48, cut) | thrust_damage(37, pierce),imodbits_sword_high ],
	
		#Armor
	["lion_plate", "Plate Armor", [("tourn_armor_a",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],
	
		#boots
	["lion_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],
		# Gauntlets
	["lion_gauntlets","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],
	
 #Lancer Gear
	#Cu Chulainn
		# gae bolg
	["gae_bolg", "Gae Bolg", [("gae_bolg",0)], itp_no_blur|itp_type_polearm|itp_unique|itp_primary|itp_can_penetrate_shield|itp_wooden_parry, itc_staff, 76, weight(2.5)|difficulty(0)|spd_rtng(95)|weapon_length(201)|swing_damage(35, cut)|thrust_damage(45, pierce), imodbits_polearm, 
		[
		# (ti_on_init_item, [
			# (store_trigger_param_1, ":agent"),
			# (neq, ":agent", -1),
            # (agent_get_position, pos0, ":agent"),
				# (try_for_range, reg4, 170, 201),
					# #(agent_get_wielded_item, ":equipped", ":agent", 0),
					# #(eq, ":equipped", "itm_gae_bolg"),
					# (set_position_delta, pos0, reg4, 0, 0), #Position, X, Y, Z.
					# (particle_system_add_new, "psys_trail_effect", pos0),
					# (val_add, reg4, 10),
				# (try_end),
				# ])
				]],
	
		# Gae Bolg - Thrown, Gotta find a way to make it change path and murder someone
	 ["gae_bolg_thrown", "Gae Bolg (Thrown)", [("gae_bolg",0)], itp_no_blur|itp_type_thrown |itp_unique|itp_extra_penetration|itp_remove_item_on_use|itp_crush_through|itp_no_pick_up_from_ground|itp_primary ,itcf_throw_javelin, 525 , weight(3)|difficulty(2)|spd_rtng(87)|shoot_speed(100)|thrust_damage(45,  pierce)|max_ammo(1)|weapon_length(201), imodbits_thrown,
			[(ti_on_missile_hit, [
				(set_fixed_point_multiplier, 100), 							# Will keep math consistant
				(store_trigger_param_1, ":agent"), 							# Store the shooting Agent
				# (store_trigger_param_2, ":hit"),
				# (eq, ":hit", 1),
			
				# (copy_position, pos10, pos1),								# Store the hit position
				# (agent_get_position, pos4, ":agent"),						# Store where the shooter is
				# (position_set_z_to_ground_level, pos10),
				# (position_set_z_to_ground_level, pos4),						# Make sure their feet are on the ground
				# (assign, ":chosen_angle", 30),
				
				# #(position_move_z, pos4, 100, 1), # around chest height

				# #(position_get_z, ":agent_z", pos4),							#
				# #(position_get_z, ":target_z", pos10),						#
				
				# #(store_sub, ":z_length", ":agent_z", ":target_z"),			# Difference in height between shooter and target
						
			    # (get_distance_between_positions, ":distance", pos10, pos4),		# How Far am I from there?
				
				# # (store_mul, ":2theta", ":chosen_angle", 2),
				# # (convert_to_fixed_point, ":2theta"),
				# # (store_tan, ":tangent", ":2theta"),
				
				# # (val_add, ":z_length", ":tangent"), #Testing my idea of how the compensation works
				
				# #(val_add, ":distance", ":z_length"),			# This compensates for differences in terrain
				
				# # ### The Next Batch is what works. ########
				
				# # (store_mul, ":speed_prime", ":distance", 981),				# Distance = (2speed^2 / gravity)
				# # ####(val_div, ":speed_prime", 2),
				# # (store_sqrt, ":speed", ":speed_prime"),					# so Speed = sqrt((distance*gravity)/2)
				
				# # ####### Based on #########################
				# # ### d = (v^2 sin(2 a))/g #################
				# # ##########################################
				
				# # ### Before this is what works ############
				
				# # ##########################################
				# # ## For Modular Speed Function ############
				# # # v = sqrt(((d)*(g))/(sin(2 a))) ###
				# # ### d = distance, g = gravity, a = angle #
				# # ##########################################
				
				# # ##########################################
				# # ## For Modular Angle Function ############
				# # a = (sin^-1((d*g)/v^2))/2
				# # ### d = distance, g = gravity, a = angle #
				# # ##########################################
			 
				
				 # (store_mul, ":numerator", ":distance", 981),
				 # (assign, reg15, ":numerator"),
				 # (store_mul, ":2theta", ":chosen_angle", 2),
				 # # (convert_to_fixed_point, ":2theta"),
				 # (store_sin, ":denominator", ":2theta"),
				 # (assign, reg16, ":denominator"),
				
				 # (store_div, ":speed_prime", ":numerator", ":denominator"),
				 # (assign, reg17, ":speed_prime"),
				 
				
				 # (store_sqrt, ":speed", ":speed_prime"),
				 # # (assign, reg18, ":speed"),
				 
				# # (display_log_message, "@{reg15}/{reg16} = {reg17}"),
				# # (display_log_message, "@sqrt{reg17} = {reg18}"),
				
				# # (assign, ":speed", 5250),
				
				# # (store_mul, ":numerator", ":distance", 981),
				# # (store_mul, ":denominator", ":speed", ":speed"),
				# # (store_div, ":prime", ":numerator", ":denominator"),
				# # (store_asin, ":intim", ":prime"),
				# # # (convert_from_fixed_point, ":intim"),
				# # (val_div, ":intim", 2),
				# # (assign, ":chosen_angle", ":intim"),
				
				# # (assign, reg17, ":intim"),
				# # (assign, reg10, ":speed"),
				# # (assign, reg11, ":distance"),
				# # (assign, reg12, 981),
				# # (display_log_message, "@asin({reg11}*981/sq{reg10})/2"),
				# # (display_log_message, "@Angle of release {reg17}"),
				
				
				
				# (position_rotate_y, pos10, 180), # Reflect the Projectile
				# (position_get_rotation_around_x, reg7, pos10), # Get new X after Y rotation
				# (val_mul, reg7, -1), 							# Get the inverse of X to cancel
				# (position_rotate_x, pos10, reg7),				# Cancel previous X
				# #(position_rotate_x, pos10, 45), 				# The ~*~Perfect~*~ Theta
				# (position_rotate_x, pos10, ":chosen_angle"),
				
				# (val_div, ":speed", 9),
								

				# #(position_move_z, pos10, 100, 1), # So it doesn't spawn inside the ground
				# (val_sub, ":agent", 1), # maybe use so that the agent "catches" the weapon
				# (add_missile, ":agent", pos10, ":speed", "itm_gae_bolg_return", 0, "itm_gae_bolg_return", 0),
				(item_get_slot, ":active", "itm_gae_bolg_thrown", fate_weapon_activated),
				
				(try_begin),
					(gt, ":active", 0),
					
					(try_for_agents, ":agent_no", pos1, 500),
					(neq, ":agent", ":agent_no"),
					
					(agent_deliver_damage_to_agent, ":agent", ":agent_no", 2500),	
					(try_end),
					
				(else_try),
					
				(try_end),
				
				]),
			],
			
			],
			
		# Gae Bolg - return,
	["gae_bolg_return", "Gae Bolg (Return)", [("gae_bolg",0)], itp_no_blur|itp_type_thrown|itp_unique|itp_ignore_friction|itp_remove_item_on_use|itp_crush_through|itp_no_pick_up_from_ground|itp_primary, itcf_throw_axe, 525 , weight(3)|difficulty(2)|spd_rtng(87)|shoot_speed(100)|thrust_damage(0,  pierce)|max_ammo(1)|weapon_length(50),imodbits_thrown,
			[(ti_on_missile_hit, [
				(set_fixed_point_multiplier, 100), 							# Will keep math consistant
		
				
				(try_for_agents, ":agent_no"),
				  (agent_is_alive, ":agent_no"),
				  (agent_is_human, ":agent_no"),
				  
				  (agent_is_alive, ":agent_no"),
				  (agent_get_position, pos4, ":agent_no"),
			      (get_distance_between_positions_in_meters,":distance", pos1, pos4), 
				  
				  (le, ":distance", 1),
				  (agent_equip_item, ":agent_no", "itm_gae_bolg_thrown"),
				  (agent_set_wielded_item, ":agent_no", "itm_gae_bolg_thrown"),
				  #(display_log_message, "@something in range."),
				(try_end),
				]),
			],
			
			],

		# blue spandex suit
	["cu_suit", "Cu Chulainn", [("mail_and_plate",0)], itp_unique|itp_type_body_armor  |itp_covers_legs, 0, 260, weight(6)|abundance(0)|head_armor(0)|body_armor(72)|leg_armor(20)|difficulty(0) ,imodbits_cloth],
	
		# Plate Toed Boots
	["cu_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
	
		# Steel Gloves
	["cu_gauntlets","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],
		
		# Runes - Throwing Weapons with random effects
	["cu_runes", "Runes", [("throwing_stone",0)], itp_no_blur|itp_type_thrown|itp_unique|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30) | thrust_damage(11, blunt)|max_ammo(18)|weapon_length(8),imodbit_large_bag ],
		
	#Diarmuid Ua Duibhne
		#gae dearg
	["gae_dearg", "Gae Dearg", [("gae_dearg",0)], itp_no_blur|itp_type_polearm|itp_unique|itp_primary|itp_can_penetrate_shield|itp_wooden_parry|itp_extra_penetration, itc_guandao|itc_dagger, 76, weight(2.5)|difficulty(0)|spd_rtng(85)|weapon_length(190)|swing_damage(42, cut)|thrust_damage(37, pierce),imodbits_polearm],
	
	["gae_dearg_offhand", "Gae Dearg", [("gae_dearg_offhand",0)], itp_no_blur|itp_unique|itp_type_shield, 0, 697, weight(2.5)|hit_points(700)|body_armor(17)|spd_rtng(61)|shield_width(10),imodbits_shield ],
	
		#gae buidhe
	["gae_buidhe", "Gae Buidhe", [("gae_buidhe",0)], itp_no_blur|itp_type_polearm|itp_unique|itp_primary|itp_wooden_parry, itc_dagger, 76, weight(2.5)|difficulty(0)|spd_rtng(95)|weapon_length(135)|swing_damage(35, cut)|thrust_damage(45, pierce),imodbits_polearm],
	
	["gae_buidhe_offhand", "Gae Buidhe", [("gae_buidhe_offhand",0)], itp_no_blur|itp_unique|itp_type_shield, 0, 697, weight(2.5)|hit_points(700)|body_armor(17)|spd_rtng(61)|shield_width(10),imodbits_shield ],
	
		#green spandex suit
	["diarmund_suit", "Diarmund Clothes", [("light_mail_and_plate",0)], itp_unique| itp_type_body_armor  |itp_covers_legs, 0, 260, weight(6)|abundance(0)|head_armor(0)|body_armor(72)|leg_armor(20)|difficulty(0) ,imodbits_cloth],
	
		#shoes
	["diarmund_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
	
		#long leather gloves
	["diarmund_gloves","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],
	
	#Fionn mac Cumhaill
		#Mac an Luin
	["mac_an_luin", "Mac an Luin", [("fionn",0)], itp_no_blur|itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear|itcf_carry_spear, 345, weight(2.25)|difficulty(0)|spd_rtng(92) | weapon_length(204)|swing_damage(20, blunt) | thrust_damage(45,  pierce),imodbits_polearm ],	
		
		#armor
	["fionn_armor", "Plate Armor", [("full_plate_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#gloves
	["fionn_gloves","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#boots
	["fionn_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],
		
	#Hector
		# Durindana - Spear, He liked to throw is so thrown variation is coded just in case
	["durindana_thrown", "Durindana Pilum", [("durindana",0),], itp_no_blur|itp_type_thrown |itp_unique|itp_primary|itp_remove_item_on_use|itp_extra_penetration|itp_next_item_as_melee, itcf_throw_javelin, 525, weight(3.5)|difficulty(0)|spd_rtng(87)|shoot_speed(120)|thrust_damage(95, pierce)|max_ammo(1)|weapon_length(270),imodbits_thrown],
	
		# Durindana - Spear
	["durindana", "Durindana", [("durindana",0)], itp_no_blur|itp_type_polearm|itp_unique|itp_primary|itp_can_penetrate_shield|itp_wooden_parry, itc_spear, 76, weight(3.5)|difficulty(0)|spd_rtng(85)|weapon_length(270)|thrust_damage(45, pierce),imodbits_polearm],
	
		# Durindana - sword
	["durindana_spada", "Durindana Spada", [("durindana_spada",0)], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_can_penetrate_shield, itc_longsword, 76, weight(3.5)|difficulty(0)|spd_rtng(150)|weapon_length(148)|swing_damage(45, cut)|thrust_damage(35, pierce), imodbits_polearm],
	
		#iron arm
	["hector_arm","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#combat gear
	["hector_gear", "Plate Armor", [("full_plate_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#knee high boots
	["hector_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
	#Brynhildr
		# Brynhildr Romantia
	["brynhildr", "Brynhildr Romantia", [("spear_d_2-8m",0)], itp_no_blur|itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear|itcf_carry_spear, 345, weight(2.25)|difficulty(0)|spd_rtng(92)| weapon_length(165)|swing_damage(20, blunt) | thrust_damage(57, pierce),imodbits_polearm],	
		
		# Dress
	["bryn_clothes", "Plate Armor", [("full_plate_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#incredibly long hair
	["bryn_head", "Brynhildr Visage", [("full_plate_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],

		#boots
	["bryn_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],
		
 #Archer Gear
	#EMIYA
		#Bow
	["emiya_bow", "Archer's Bow", [("new_emiya_bow",0)], itp_no_blur|itp_type_bow|itp_unique|itp_primary|itp_two_handed, itcf_shoot_bow, 145, weight(1.75)|difficulty(2)|spd_rtng(99)|shoot_speed(76) | thrust_damage(40, pierce), imodbits_bow, 
	[(ti_on_weapon_attack, [
			(set_fixed_point_multiplier, 100), 							# Will keep math consistant
			# (store_trigger_param_1, ":agent"), 							# Store the shooting Agent
			(store_trigger_param_1, ":agent"),
			(gt, ":agent", 0),
		
			(agent_get_ammo, ":ammo", ":agent", 1),
			
			(lt, ":ammo", 10),
			
			#Clear his various available arrows and reequip the basic arrows
			(agent_unequip_item, ":agent", "itm_emiya_arrows"),
			(agent_unequip_item, ":agent", "itm_caladbolg_ii"),
			(agent_unequip_item, ":agent", "itm_hrunting"),
			
			
			
			(agent_equip_item, ":agent", "itm_emiya_arrows"),
			(agent_set_wielded_item, ":agent", "itm_emiya_arrows"),		
		]),	
	]],
	
	["emiya_arrows", "Projected Arrows", [("piercing_arrow", 0), ("flying_missile", ixmesh_flying_ammo)], itp_no_blur|itp_type_arrows|itp_no_pick_up_from_ground|itp_unique, 0, 350,weight(3)|abundance(0)|weapon_length(91)|thrust_damage(2, pierce)|max_ammo(20), imodbits_missile],
	
		#Shroud of Martin - Red Jacket
	["shroud_of_martin", "Red Overcoat", [("emiya_coat",0)], itp_unique|itp_type_body_armor|itp_covers_legs, 0, 260, weight(6)|abundance(0)|head_armor(0)|body_armor(72)|leg_armor(20)|difficulty(0) ,imodbits_cloth],
		
		#Boots
	["emiya_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],

	["emiya_gloves", "emiya gloves", [("gauntlets_L",0),], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],
		
		#Kanshou - White Falchion (Thrown)
	["kanshou", "Kanshou", [("kanshou",0)], itp_no_blur|itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground|itp_next_item_as_melee, itcf_throw_axe,
	360, weight(2.5)|difficulty(0)|spd_rtng(99) | shoot_speed(35) | thrust_damage(45,cut)|max_ammo(56)|weapon_length(40),imodbits_thrown_minus_heavy, 
	 [(ti_on_weapon_attack, [
			(call_script, "script_cf_fate_double_throw", "itm_bakuya", "itm_bakuya_thrown"),
            ]),]],
			
		#Kanshou - White Falchion
	["kanshou_melee", "Kanshou", [("kanshou",0)], itp_no_blur|itp_type_one_handed_wpn|itp_no_pick_up_from_ground|itp_unique|itp_primary, itc_scimitar, 105, weight(2.5)|difficulty(8)|spd_rtng(98) | weapon_length(65)|swing_damage(45, cut) | thrust_damage(0, pierce), imodbits_sword ],
		
		#Bakuya - Black Falchion (Shield)
	["bakuya", "Bakuya", [("bakuya_offhand",0), ("bakuya", ixmesh_carry)], itp_unique|itp_type_shield|itp_no_pick_up_from_ground, 0,  697 , weight(2.5)|hit_points(700)|body_armor(17)|spd_rtng(61)|shield_width(10), imodbits_shield ],
	
		#Bakuya - Black Falchion (Thrown)
	["bakuya_thrown", "Bakuya", [("bakuya",0)], itp_no_blur|itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground, itcf_throw_axe,
	360, weight(2.5)|difficulty(0)|spd_rtng(99) | shoot_speed(35) | thrust_damage(45,cut)|max_ammo(100)|weapon_length(40),imodbits_thrown_minus_heavy,],
	
		#Kanshou - White Falchion
	["kanshou_oe", "Kanshou Overedge", [("kanshou",0)], itp_no_blur|itp_type_one_handed_wpn|itp_no_pick_up_from_ground|itp_unique|itp_primary, itc_scimitar, 105, weight(4.5)|difficulty(8)|spd_rtng(88) | weapon_length(85)|swing_damage(55, cut) | thrust_damage(40, pierce), imodbits_sword ],
		
		#Bakuya - Black Falchion (Shield)
	["bakuya_oe", "Bakuya Overedge", [("bakuya",0), ("bakuya", ixmesh_carry)], itp_unique|itp_type_shield|itp_no_pick_up_from_ground, 0,  697 , weight(2.5)|hit_points(1200)|body_armor(50)|spd_rtng(50)|shield_width(20), imodbits_shield],
		
		#unlimited blade works generic swords for ammo
		
		#Broken Phantasm - Caladbolg II
	
	["caladbolg_ii","Caladbolg II", [("arrow_b",0),("flying_missile",ixmesh_flying_ammo)], itp_no_blur|itp_type_arrows|itp_no_pick_up_from_ground|itp_unique, 0, 410, weight(3.5)|abundance(0)|weapon_length(95)|thrust_damage(35,pierce)|max_ammo(1), imodbits_missile, 
	[
	
	(ti_on_missile_hit, [
			 # (particle_system_burst, "psys_gourd_smoke", pos1, 100),
			 # (particle_system_burst, "psys_fireplace_fire_big", pos1, 100),
			 
			(store_trigger_param_1, ":agent"),
			
			(call_script, "script_cf_fate_explosion", pos1, ":agent", 1250, 4500, 7000),

            ]),			
	]],
			
		#Broken Phantasm - Hrunting	
	["hrunting", "Hrunting", [("hrunting",0), ("hrunting",ixmesh_flying_ammo)], itp_type_arrows|itp_no_pick_up_from_ground|itp_unique, 0, 410,weight(3.5)|abundance(0)|weapon_length(95)|thrust_damage(35,pierce)|max_ammo(1), imodbits_missile, 
	[
	
	(ti_on_missile_hit, [
			 # (particle_system_burst, "psys_gourd_smoke", pos1, 100),
			 # (particle_system_burst, "psys_fireplace_fire_big", pos1, 100),
			 
			 (store_trigger_param_1, ":agent"),
			
			 (call_script, "script_cf_fate_explosion", pos1, ":agent", 1000, 3000, 5000),
            ]),			
	]],
			
		#Rho Aias
		
		
	#Gilgamesh
		#Ea
	["gilgamesh_ea", "EA - Sword of Rapture", [("ea",0)], itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_wooden_parry|itp_unbalanced, itc_longsword, 305, weight(4.5)|difficulty(13)|spd_rtng(95) | weapon_length(85)|swing_damage(75, pierce)|thrust_damage(55, pierce),imodbits_mace ],
		
		#Gate of Babylon -empty hands that trigger crossed arm animation and summon near infinite weapons as long as he has mana
	["gate_baby", "gate open", [("ea",0)], itp_type_one_handed_wpn|itp_primary, itc_greatlance, 0 , weight(1.25)|difficulty(0)|spd_rtng(103) | weapon_length(1)|swing_damage(20, cut) | thrust_damage(13,  pierce),imodbits_sword_high, 
	 [(ti_on_weapon_attack, [
			(set_fixed_point_multiplier, 100),
			(store_trigger_param_1, ":agent"),
			(agent_set_animation, ":agent", "anim_pose_3"),
			(agent_get_look_position, pos1, ":agent"),
			
			(try_for_range, ":spawn", 0, 5),
				#(store_random_in_range, ":rand_y", 200, 2300),
				(store_random_in_range, ":rand_x", -200, 200),
				(store_random_in_range, ":rand_z", 200, 1500),
				(store_random_in_range, ":wobble", 10, -10),
				#(position_move_y, pos1, ":rand_y", 0),
				(position_move_x, pos1, ":rand_x", 0),
				(position_move_z, pos1, ":rand_z", 0),
				(position_move_y, pos1, -150, 0),
				(store_div, ":adjust_rotation", ":rand_z", -50),
				(position_rotate_x, pos1, ":adjust_rotation"),
				(position_rotate_y, pos1, ":wobble"),
				(add_missile, ":agent", pos1, 6000, "itm_babylon_sword", 0, "itm_babylon_sword", 0),
			(try_end),
            ]),]],	
		
		#Gate Stored Weapons for ammo
	["babylon_sword","Golden Treasure, Sword", [("gil_sword_shoot",0),("gil_sword_shoot",ixmesh_flying_ammo)], itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground,itcf_throw_javelin|itcf_carry_sword_left_hip,
	360, weight(2.5)|difficulty(0)|spd_rtng(99) | shoot_speed(600) | thrust_damage(45,pierce)|max_ammo(1)|weapon_length(65),imodbits_thrown_minus_heavy, 
	[
	
	(ti_on_missile_hit, [
			
			 (store_trigger_param_1, ":agent"),
			
			 (call_script, "script_cf_fate_explosion", pos1, ":agent", 1500, 2500, 3000),
            ]),			
	]],
		
		#Golden Armor
	["gil_armor", "Plate Armor", [("lamellar_armor_c",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Golden Boots
	["gil_boots", "Plate Boots", [("lamellar_boots_a",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
	#Atalanta
	
		#Tauropolos - Atalanta Bow
	["tauropolos", "Tauropolos", [("long_bow",0),("long_bow_carry",ixmesh_carry)], itp_no_blur|itp_type_bow |itp_unique|itp_primary|itp_extra_penetration|itp_two_handed ,itcf_shoot_bow|itcf_carry_bow_back, 145 , weight(1.75)|difficulty(2)|spd_rtng(99) | shoot_speed(50) | thrust_damage(40, pierce),imodbits_bow,
		[(ti_on_weapon_attack, [
			(store_trigger_param_1, ":agent"),
			
			(agent_unequip_item, ":agent", "itm_calamity_arrows"),
			(agent_equip_item, ":agent", "itm_atalanta_arrows"),
			(agent_set_wielded_item, ":agent", "itm_atalanta_arrows"),
			]),]],
	
	
		#Atalanta's Arrows
	
	["atalanta_arrows","Grecean Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_no_blur|itp_type_arrows|itp_unique|itp_default_ammo, itcf_carry_quiver_back, 72, weight(3)|abundance(160)|weapon_length(95)|thrust_damage(15,pierce)|max_ammo(30),imodbits_missile],
	
	["calamity_arrows","Calamity Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_no_blur|itp_type_arrows|itp_unique|itp_default_ammo, itcf_carry_quiver_back, 72, weight(3)|abundance(160)|weapon_length(95)|thrust_damage(15,pierce)|max_ammo(1),imodbits_missile, 
			[(ti_on_missile_hit, [
							
				(set_fixed_point_multiplier, 100), 							# Will keep math consistant
				(store_trigger_param_1, ":agent"), 							# Store the shooting Agent
				
				(call_script, "script_target_missiles", ":agent", pos1, 300, 1000),
				
				# (agent_get_look_position, pos9, ":agent"), 					# Get Agent's look for math
				# (init_position, pos10), 									# Initial the Position that will be used for math 
				
				# (position_copy_origin, pos10, pos1),						# duplicate the info from the trigger Pos to math Pos
				
				# (position_get_rotation_around_z, ":look_z", pos9),			# Rip the Z-axis from the look pos
				# (val_add, ":look_z", 180),									# Rotate 180 to insure the missiles are coming towards the player
				# (position_rotate_z, pos10, ":look_z", 1),					# apply the new Z-axis to math pos
				
				# (position_move_z, pos10, 6000, 1),							# Move the missiles very high, they're from space after all
				# #(position_move_y, pos10, -5000, 1),
				# (position_move_y, pos10, -5000, 0), 						# I decided this distance through trial-and-error for perfect arcs when the next line
				# (position_rotate_x, pos10, -45, 1),							# This insures arrows fall towards the Earth, sort of a sharp angle. But if modified move_y needs to change too.
				
				# (copy_position, pos9, pos10),								# I use this so that the randomization doesn't mess with the original math
				# # (position_move_z, pos10, 200, 1),
				# # (position_get_rotation_around_y, ":orig_y", pos1),
				# # (position_get_rotation_around_z, ":orig_z", pos1),
				
				# # Get agent look position, take the y rotation ######
				# # add 180, reset all positional data, add the new ###
				# # rotational data, the original -45 and use that for#
				# # the rest of the positional math. this should insure
				# # that the arrows are spawned in coming TOWARDS you
				# # every time and not at some glbal rotation
				
				
				# (try_for_range, ":spawn", 0, 301),							# number of arrows
					 # (copy_position, pos10, pos9),							# reset the pos every new arrow so randomized pos below doesn't mess with the center
					 # (store_random_in_range, ":rand_y", -500, 500), 		# the rest creates a 10m sq that arrows fall between
					 # (store_random_in_range, ":rand_x", -500, 500),
					 # (store_random_in_range, ":rand_z", -500, 500),
					
					 # (position_move_y, pos10, ":rand_y", 1),
					 # (position_move_x, pos10, ":rand_x", 1),
					 # (position_move_z, pos10, ":rand_z", 1),										
																			# # finally add the arrows.
					# (add_missile, ":agent", pos10, 5000, "itm_tauropolos", 0, "itm_astral_arrows", 0), 
				# (try_end),
				]),
			],
	
	],
		
	
		# Astral Arrows for Calamity
	 ["astral_arrows","Barbed Arrows", [("none",0),("mystic_missile",ixmesh_flying_ammo),("quiver_d", ixmesh_carry)], itp_no_blur|itp_type_arrows|itp_unique|itp_no_pick_up_from_ground, itcf_carry_quiver_back_right, 124,weight(3)|abundance(70)|weapon_length(95)|thrust_damage(5,pierce)|max_ammo(30),imodbits_missile #,
		# [(ti_on_missile_hit, [
			 # (particle_system_burst, "psys_gourd_smoke", pos1, 1),
			 # (particle_system_burst, "psys_fireplace_fire_big", pos1, 10),
			 # (call_script, "script_cf_fate_explosion", 100, 2000, 5000),
            # ]),
		# ],
	],
		
		#dress
	["atalanta_dress", "Plate Armor", [("green_dress",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#cat ears
	["atalanta_head", "Cat Ears", [("breaker",0)], itp_type_head_armor|itp_unique, 0, 193, weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0), imodbits_cloth],
		
		#long boots
	["atalanta_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
	
		#berserk transformation - body
	["atalanta_boar_body", "Agius Body", [("tribal_warrior_outfit_a",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#berserk transformation - cat ears
	["atalanta_boar_head", "Agius Ears", [("helmet_fur_a",0)], itp_type_head_armor|itp_unique, 0, 193, weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0), imodbits_cloth],
		
		#berserk transformation - legs
	["atalanta_boar_legs", "Agius Legs", [("hide_boots_a",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
	
		# berserk transformation - right claw (build left claw into body mesh)
	["atalanta_boar_claw", "Agius Claw", [("peasant_knife_new",0)], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_left, 18, weight(0.5)|difficulty(0)|spd_rtng(110)|weapon_length(40)|swing_damage(51, cut)|thrust_damage(25, pierce),imodbits_sword],
		
	#Robin Hood
	
		#Yew Bow
	["yew_bow","Yew Bow", [("crossbow_a", 0)], itp_type_crossbow|itp_unique|itp_primary, itp_no_blur|itcf_shoot_pistol|itcf_reload_pistol|itcf_carry_pistol_front_left,525, weight(1)|abundance(100)|spd_rtng(85)|shoot_speed(90)|max_ammo(1)|thrust_damage(55, pierce), imodbits_crossbow, [
	(ti_on_init_item, [
		(store_trigger_param_1, ":agent"),
		(neq, ":agent", -1),
		
		(try_begin),
			(agent_slot_eq, ":agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_04_01"),
			(agent_slot_ge, ":agent", slot_agent_active_np_timer, 1),
			
			(cur_item_set_material, "str_material_camo", 0),
		(else_try),
			(agent_slot_ge, ":agent", fate_agent_petrified, 1),
			
			(cur_item_set_material, "str_material_petrified", 0),
		(try_end),
		]),
		
	(ti_on_weapon_attack, [
		(store_trigger_param_1, ":agent"),
		
		(agent_get_ammo, ":ammo", ":agent", 1),
		
		(lt, ":ammo", 10),
		
		(agent_unequip_item, ":agent", "itm_yew_bolts"),
		(agent_equip_item, ":agent", "itm_yew_bolts_actual"),
		(agent_set_wielded_item, ":agent", "itm_yew_bolts_actual"),
	])]],
	 
		#Yew Tree Arrow
	["yew_bolts","Yew Tree Summoner", [("piercing_arrow",0),("flying_missile",ixmesh_flying_ammo),("none", ixmesh_carry)], itp_type_bolts|itp_no_pick_up_from_ground|itp_unique, itcf_carry_quiver_back_right, 350,weight(3)|abundance(50)|weapon_length(91)|thrust_damage(5, pierce)|max_ammo(1),imodbits_missile, 
	[	
	(ti_on_init_item, [
		(store_trigger_param_1, ":agent"),
		(neq, ":agent", -1),
        (agent_slot_eq, ":agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_04_01"),
		(agent_slot_ge, ":agent", slot_agent_active_np_timer, 1),
		
		(cur_item_set_material, "str_material_camo", 1),
		]),
	
	(ti_on_missile_hit, [
			(set_fixed_point_multiplier, 100),
			
			(store_trigger_param_1, ":agent"),
			(particle_system_burst, "psys_gourd_smoke", pos1, 100),
			(particle_system_burst, "psys_fireplace_fire_big", pos1, 100),
			 
			 
			(copy_position, pos2, pos1), # Make a duplicate of landing location
			(position_move_z, pos2, 50, 1), # Move it up
			(call_script, "script_lookat", pos2, pos1), # Look at that position
			
			(position_rotate_x, pos2, -90), # There seems to be an issue inside the ti_on_missile_hit where the axis are mislabelled somehow, requiring us to do this.
			
			(cast_ray, reg1, pos3, pos2), # Cast a ray, this should make a duplicate of pos1, but with the normals of the surface stored in the rotational values
			
			# (position_copy_rotation, pos1, pos3),
			(position_rotate_x, pos3, 90),
			
			(set_spawn_position, pos3),
			 
			(spawn_scene_prop, "spr_fate_yew_tree"),
			 
			(scene_prop_set_slot, reg0, fate_prop_owner, ":agent"),
            ]),]],
			
	["yew_bolts_actual", "Yew Bolts", [("piercing_arrow", 0), ("flying_missile", ixmesh_flying_ammo), ("none", ixmesh_carry)], itp_no_blur|itp_type_bolts|itp_no_pick_up_from_ground|itp_unique, itcf_carry_quiver_back_right, 350, weight(3)|abundance(50)|weapon_length(91)|thrust_damage(15, pierce)|max_ammo(50), imodbits_missile, [
	(ti_on_init_item, [
		(store_trigger_param_1, ":agent"),
		(neq, ":agent", -1),
        (agent_slot_eq, ":agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_04_01"),
		(agent_slot_ge, ":agent", slot_agent_active_np_timer, 1),
		
		(cur_item_set_material, "str_material_camo", 0),
		])
	]],
			
		#Dagger
	["robin_knife", "Knife", [("peasant_knife_new",0)], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_left, 18, weight(0.5)|difficulty(0)|spd_rtng(110)|weapon_length(40)|swing_damage(51, cut)|thrust_damage(25, pierce),imodbits_sword, [
	(ti_on_init_item, [
		(store_trigger_param_1, ":agent"),
		(neq, ":agent", -1),
        (agent_slot_eq, ":agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_04_01"),
		(agent_slot_ge, ":agent", slot_agent_active_np_timer, 1),
		
		(cur_item_set_material, "str_material_camo", 0),
		])
	]],
		
		#Green Hood
	["robin_hood", "Hood", [("hood_new",0)],itp_unique|itp_type_head_armor|itp_civilian,0,9, weight(1)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth, [
		(ti_on_init_item, [
		(store_trigger_param_1, ":agent"),
		(neq, ":agent", -1),
        (agent_slot_eq, ":agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_04_01"),
		(agent_slot_ge, ":agent", slot_agent_active_np_timer, 1),
		
		(cur_item_set_material, "str_material_camo", 0),
		])
	]],	
		
		#Archer Gear
	["robin_clothes", "Plate Armor", [("peasant_man_a",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate, [
		(ti_on_init_item, [
		(store_trigger_param_1, ":agent"),
		(neq, ":agent", -1),
        (agent_slot_eq, ":agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_04_01"),
		(agent_slot_ge, ":agent", slot_agent_active_np_timer, 1),
		
		(cur_item_set_material, "str_material_camo", 0),
		])
	]],	
		
	["robin_gloves", "emiya gloves", [("gauntlets_L",0),], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor, [
	(ti_on_init_item, [
		(store_trigger_param_1, ":agent"),
		(neq, ":agent", -1),
        (agent_slot_eq, ":agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_04_01"),
		(agent_slot_ge, ":agent", slot_agent_active_np_timer, 1),
		
		(cur_item_set_material, "str_material_camo", 0),
		(cur_item_set_material, "str_material_camo", 1),
		(cur_item_set_material, "str_material_camo", 2),
		]),
	
	]],
	
		#Brown Leather Boots
	["robin_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate, [
		(ti_on_init_item, [
		(store_trigger_param_1, ":agent"),
		(neq, ":agent", -1),
        (agent_slot_eq, ":agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_04_01"),
		(agent_slot_ge, ":agent", slot_agent_active_np_timer, 1),
		
		(cur_item_set_material, "str_material_camo", 0),
		]),
	] ],
	
	#Arash
		#Bow and Arrow Creation - Crimson Bow
	["arash_bow", "Arash's Bow", [("ArashBow",0)], itp_no_blur|itp_type_bow|itp_unique|itp_primary|itp_two_handed ,itcf_shoot_bow, 145 , weight(1.75)|difficulty(2)|spd_rtng(99) | shoot_speed(76) | thrust_damage(40, pierce),imodbits_bow,
		[(ti_on_weapon_attack, [
			(set_fixed_point_multiplier, 100), 							# Will keep math consistant
			# (store_trigger_param_1, ":agent"), 							# Store the shooting Agent
			(store_trigger_param_1, ":agent"),
		
			(agent_get_ammo, ":ammo", ":agent", 1),
			
			(lt, ":ammo", 10),
			
			(agent_unequip_item, ":agent", "itm_stella_arrow"),
			(agent_equip_item, ":agent", "itm_created_arrows"),
			(agent_set_wielded_item, ":agent", "itm_created_arrows"),
			# (agent_get_look_position, pos9, ":agent"), 					# Get Agent's look for math
			# (agent_get_bone_position, pos10, ":agent", 13, 1),
			# (position_copy_rotation, pos10, pos9),
			
			# (copy_position, pos11, pos10),

			
			# (agent_get_slot, ":charge_time", ":agent", slot_agent_charge_time),
			# (convert_from_fixed_point, ":charge_time"),	
		
			# (display_message, "@I will fire {reg50} extra shots!"),
			# (gt, ":charge_time", 1),
			# (try_for_range, ":shots", 0, ":charge_time"),
				# (store_random_in_range, ":rand_x", -10, 10),
				# (store_random_in_range, ":rand_y", -10, 10), 
				# (store_random_in_range, ":rand_z", -10, 10),
							
				# (position_rotate_x, pos10, ":rand_x"),
				# (position_rotate_y, pos10, ":rand_y"),
				# (position_rotate_z, pos10, ":rand_z"),
							
				# (agent_set_animation, ":agent", "anim_release_bow", 1),
				# (add_missile, ":agent", pos10, 7600, "itm_arash_bow", 0, "itm_created_arrows", 0),
				# (copy_position, pos10, pos11),
			# (try_end),
		
		]),]
	],
	
		#Bow and Arrow Creation - Crimson Arrows
	["created_arrows", "Created Arrows", [("arash_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver_c", ixmesh_carry)], itp_no_blur|itp_type_arrows|itp_no_pick_up_from_ground|itp_unique, 0, 350,weight(3)|abundance(0)|weapon_length(91)|thrust_damage(2,pierce)|max_ammo(255),imodbits_missile,],
	
		#Stella, Summons meteor! HUGE AOE stone_ball
	["stella_arrow", "Stella", [("arash_arrow",0),("flying_missile",ixmesh_flying_ammo)], itp_no_blur|itp_type_arrows|itp_no_pick_up_from_ground|itp_unique, 0, 350,weight(3)|abundance(0)|weapon_length(91)|thrust_damage(2,pierce)|max_ammo(1),imodbits_missile,
		[(ti_on_missile_hit, [
				(set_fixed_point_multiplier, 100), 							# Will keep math consistant
				(store_trigger_param_1, ":agent"), 							# Store the shooting Agent
				(agent_get_look_position, pos9, ":agent"), 					# Get Agent's look for math
				(init_position, pos10), 									# Initial the Position that will be used for math 
				
				(position_copy_origin, pos10, pos1),						# duplicate the info from the trigger Pos to math Pos
				
				(position_get_rotation_around_z, ":look_z", pos9),			# Rip the Z-axis from the look pos
				#(val_add, ":look_z", 90),									# Rotate 180 to insure the missiles are coming towards the player
				(position_rotate_z, pos10, ":look_z", 1),					# apply the new Z-axis to math pos
				
				(position_move_z, pos10, 6000, 1),							# Move the missiles very high, they're from space after all
				(position_move_y, pos10, -5000, 0), 						# I decided this distance through trial-and-error for perfect arcs when the next line
				(position_rotate_x, pos10, -45, 1),							# This insures arrows fall towards the Earth, sort of a sharp angle. But if modified move_y needs to change too.
				
				(add_missile, ":agent", pos10, 9000, "itm_stella_meteor", 0, "itm_stella_meteor", 0), 
					 
				]),]
	],
		# Stella - The Lone Meteor
	["stella_meteor", "Stella - Lone Meteor", [("rock_a",0)], itp_no_blur|itp_type_thrown|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(30) | thrust_damage(11,  blunt)|max_ammo(18)|weapon_length(8), imodbit_large_bag,
		[(ti_on_missile_hit, [
		
			(store_trigger_param_1, ":agent"),
			
			(call_script, "script_cf_fate_explosion", pos1, ":agent", 7000, 7500, 10000),
		]),]
	],
	
		#Blue Archer Gear
	["arash_plate", "Plate Armor", [("full_plate_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Red Boots
	["arash_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
		#Yellow Hood
	["arash_hood", "Hood", [("hood_new",0)], itp_unique|itp_type_head_armor|itp_civilian,0,9, weight(1)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],	
		
		#Red MMA Style Gloves
	["arash_gloves","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0), imodbits_armor],	
		
 #Rider Gear
	#Medusa
		#Fucking Pegasus
	["bellerophon","Bellerophon", [("courser",0)], itp_unique|itp_type_horse, 0, 600,abundance(0)|body_armor(50)|hit_points(110)|difficulty(2)|horse_speed(65)|horse_maneuver(60)|horse_charge(25)|horse_scale(112),imodbits_horse_basic|imodbit_champion],
		
		#Nameless Dagger, maybe find a way to do a chain mechanic? Allow throw and return?
	["medusa_dagger", "Medusa Dagger", [("medusa_dagger",0),], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary, itc_longsword, 37, weight(0.75)|difficulty(0)|spd_rtng(125) | weapon_length(47)|swing_damage(22, cut)|thrust_damage(49, pierce), imodbits_sword_high ],
		
		#Eye Cover
	["medusa_mask", "Breaker Gorgon", [("breaker",0)], itp_type_head_armor|itp_doesnt_cover_hair|itp_unique, 0, 193, weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0), imodbits_cloth],
		
		#Dress
	["medusa_dress", "Black Minidress", [("full_plate_armor",0)], itp_unique|itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Long Boots
	["medusa_boots", "Plated Heeled Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
	#Iskandar
		#Divine Bull
	["divine_bull","Divine Bull", [("courser",0)], itp_unique|itp_type_horse, 0, 600,abundance(0)|body_armor(50)|hit_points(110)|difficulty(2)|horse_speed(65)|horse_maneuver(60)|horse_charge(75)|horse_scale(175),imodbits_horse_basic|imodbit_champion],	
		
		#Gordius Wheel
		
		
		#Bucephalus - Big ass Horse
	["bucephalus","Bucephalus", [("charger_plate_1",0)], itp_unique|itp_type_horse, 0, 600,abundance(0)|body_armor(75)|hit_points(250)|difficulty(2)|horse_speed(65)|horse_maneuver(60)|horse_charge(50)|horse_scale(145),imodbits_horse_basic|imodbit_champion],
		
		#Mantle
		
		
		#Greek Styled Armor
	["iskandar_plate", "Plate Armor", [("sarranid_elite_cavalary",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Sandles with Shinguards
	["iskandar_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
		#Sword of the Kupriotes
	["kupriotes", "Sword of the Kupriotes", [("b_bastard_sword",0),("scab_bastardsw_b", ixmesh_carry)], itp_no_blur|itp_type_one_handed_wpn|itp_unique| itp_two_handed|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 524, weight(3)|difficulty(11)|spd_rtng(94) | weapon_length(130)|swing_damage(40, cut) | thrust_damage(31, pierce), imodbits_sword_high],
 
		# #Temp Measure to Summon Ioioan Heteroi
	# ["ionioi_hetairoi_summon", "Ionioi Hetairoi", [("throwing_stone",0)], itp_type_thrown |itp_unique|itp_primary ,itcf_throw_stone, 1 , weight(4)|difficulty(0)|spd_rtng(99) | shoot_speed(99) | thrust_damage(11 ,  blunt)|max_ammo(1)|weapon_length(8),imodbit_large_bag,
	# [(ti_on_missile_hit, [
			  # (store_trigger_param_1, ":agent"),
			 # # (agent_get_team ,":team", ":agent"),
			 # (agent_get_party_id, ":party", ":agent"),
			  # (agent_get_entry_no, ":entry", ":agent"),
			 # # (entry_point_get_position, pos10, ":entry"),
			 # (party_add_template, ":party", "pt_fate_ionioi_hetairoi"),
			# # (party_quick_attach_to_current_battle, "pt_fate_ionioi_hetairoi", 150),
			 # (add_reinforcements_to_entry, ":entry", 150),
			
			# (party_remove_members, ":party", "trp_ionioi_hetairoi_horseman", 25),
			# (party_remove_members, ":party", "trp_ionioi_hetairoi_archer", 35),
			# (party_remove_members, ":party", "trp_ionioi_hetairoi_footman", 40),
				# # (try_for_range, ":spawn", 0, 120),
				
				# # #(store_mul, ":variant", 10, ":spawn"),
				# # (position_move_x, pos10, ":spawn", 1),
				# # (set_spawn_position, pos10),
				
				
				# # (store_random_in_range, ":rand", 0, 30),
				
				# # (le, ":rand", 10),
				# # (spawn_agent, "trp_ionioi_hetairoi_footman"),
				# # (agent_set_team,reg0,":team"),
					# # (else_try),
				# # (le, ":rand", 20),
				# # (spawn_agent, "trp_ionioi_hetairoi_horseman"),
				# # (agent_set_team,reg0,":team"),
					# # (else_try),
				# # (spawn_agent, "trp_ionioi_hetairoi_archer"),
				# # (agent_set_team,reg0,":team"),
				# # (try_end),
            # ]),]
	# ],
		# Jeans and Admirable Tactics T-Shirt
	["admirable_tactics_shirt", "Fucking Nerd's Clothing", [("leather_vest_a",0)], itp_unique|itp_type_body_armor|itp_covers_legs|itp_civilian ,0, 3 , weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
		
	#Achilles
		#Diatrekhon Aster Lonkhe - Hero Killing Spear
	["diatrekhon", "Diatrekhon Aster Lonkhe", [("awl_pike_b",0)], itp_no_blur|itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear, 345, weight(2.25)|difficulty(0)|spd_rtng(92) | weapon_length(165)|swing_damage(20, blunt) | thrust_damage(57, pierce) ,imodbits_polearm],	
		
		#Akhilleus Kosmos - Shield
	["kosmos_shield", "Akhilleus Kosmos", [("tableau_shield_round_4",0)], itp_unique|itp_type_shield, 0, 430, weight(4.5)|hit_points(1200)|body_armor(32)|spd_rtng(81)|shield_width(50),imodbits_shield],
		
		#Side arming sword
	
		
		#flying chariot - Trois Tragoidia
	
		
		#Black and silver armor
	["achilles_plate", "Grecian Plate", [("full_plate_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#silver boots
	["achilles_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
		#matching gloves
	["achilles_gauntlets","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
	#Astolfo
		#La Black Luna, Horn that causes Rout
	
		
		#Trap of Argalia, Blunt Lance
	["argalia", "Trap of Argalia", [("joust_of_peace",0)], itp_no_blur|itp_couchable|itp_type_polearm|itp_unique|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_greatlance, 158, weight(5)|difficulty(0)|spd_rtng(78)|weapon_length(240)|swing_damage(0, cut)|thrust_damage(71, blunt), imodbits_polearm],	
		
		#Thin Sidearming Sword
	["astolfo_sidearm", "Arming Sword", [("sword_medieval_c",0),("sword_medieval_c_scabbard", ixmesh_carry)], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 410 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(29, cut) | thrust_damage(24, pierce),imodbits_sword_high ],
		
		#Hippogriff
	["hippogriff","Hippogriff", [("warhorse_steppe",0)], itp_unique|itp_type_horse, 0, 600,abundance(0)|body_armor(35)|hit_points(100)|difficulty(2)|horse_speed(70)|horse_maneuver(75)|horse_charge(20)|horse_scale(135),imodbits_horse_basic|imodbit_champion],
		
		#Casseur de Logistille - Grants rank A magic resistance
	
		
		#Black Armor
	["astolfo_plate", "Plate Armor", [("full_plate_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#White Cape
	
		
		#White Gauntlets
	["astolfo_gauntlets","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#White Boots with Spurs
	["astolfo_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
	#Saint George
		#Ascalon - Dragon Slaying Sword
	["ascalon", "Ascalon", [("military_cleaver_c",0)], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary, itc_scimitar|itcf_carry_sword_left_hip, 263, weight(1.5)|difficulty(0)|spd_rtng(96) | weapon_length(95)|swing_damage(35, cut)|thrust_damage(0,  pierce),imodbits_sword_high],
		
		#Bayard - Horse Negates first attack
	["bayard","Bayard", [("courser",0)], itp_unique|itp_type_horse, 0, 600,abundance(0)|body_armor(255)|hit_points(255)|difficulty(2)|horse_speed(70)|horse_maneuver(45)|horse_charge(20)|horse_scale(107),imodbits_horse_basic|imodbit_champion],
		
		#Copper Armor with White Tabard
	["george_armor", "Plate Armor", [("mail_long_surcoat_new",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Copper Gauntlets
	["george_gauntlets","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#Dragon Themed Copper Boots
	["george_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
 #Caster Gear
	#Medea
		#rule breaker
	["rulebreaker", "Rule Breaker", [("rulebreaker",0),], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_left, 37, weight(0.75)|difficulty(0)|spd_rtng(135) | weapon_length(30)|swing_damage(37, cut)|thrust_damage(35, pierce),imodbits_sword_high	],
		
		#Medea's Staff
	["medea_staff",  "Medea's Staff", [("medea_staff",0)], itp_no_blur|itp_type_polearm|itp_offset_lance|itp_unique| itp_primary, itc_staff|itcf_carry_sword_back, 202, weight(2)|difficulty(0)|spd_rtng(97) | weapon_length(140)|swing_damage(25, blunt) | thrust_damage(26,  blunt),imodbits_polearm ],
		
		#Weird Black Headgarb
	["medea_cap", "Skull Mask", [("turret_hat_r",0)], itp_type_head_armor|itp_unique, 0, 193, weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0),imodbits_cloth],
		
		#Purple Cape, Dress
	["medea_robes", "Plate Armor", [("woolen_dress",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Slippers
	["medea_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
	#Gilles De Rais
		#Prelati's Spellbook
		
		
		#Cloak
	["gilles_cloak", "Plate Armor", [("pilgrim_outfit",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Mantle
		
		
		#Slippers
	["gilles_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
	#William Shakespeare
		#First Folio
		
		
		#Victorian Garb
	["shakespeare_garb", "Plate Armor", [("blue_gambeson",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Leather Gloves
	["shakespeare_gloves","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#Classic Boots
	["shakespeare_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
	#Avicebron
		#focus mask
	["avicebron_mask", "Focus Mask", [("skull_mask",0)], itp_type_head_armor|itp_unique, 0, 193, weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0),imodbits_cloth],
		
		#Jester Outfit with cape
	["avicebron_clothes", "Plate Armor", [("red_gambeson",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Strange Pen Shoes
	["avicebron_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],

		# Black Gloves
	["avicebron_gloves","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
	#Hohenheim
		#Azoth Sword - Sword of Paracelsus
	["paracelsus_sword", "Azoth Sword - Sword of Paracelsus", [("sword_viking_b_small",0),], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary, itc_longsword, 162, weight(1.25)|difficulty(0)|spd_rtng(103)|weapon_length(85)|swing_damage(55, cut)|thrust_damage(32, pierce),imodbits_sword_high ],
		
		#Some red book
	
		
		#Elemental Crystals
	
		
		#Lab Coat looking Cloak
	["hohenheim_coat", "Plate Armor", [("sar_robe_b",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Silver Greaves
	["hohenheim_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
		#Black Weird Gloves
	["hohenheim_gloves","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#Very long hair
		
		
 #Assassin Gear
		#Generalized Assassin Gear
		
	["assassin_robes", "Plate Armor", [("pilgrim_outfit",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],
	["assassin_robes_a", "Plate Armor", [("robe",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],
		
	["throwing_daggers_assassin", "The Assassin's Throwing Daggers", [("throwing_dagger",0)], itp_no_blur|itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground ,itcf_throw_knife, 193 , weight(2.5)|difficulty(0)|spd_rtng(130) | shoot_speed(100) | thrust_damage(34, cut)|max_ammo(30)|weapon_length(0),imodbits_thrown],
		
	#Cursed Arm
	
	["cursed_arm_wrapped_body", "Plate Armor", [("pilgrim_outfit",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],
	
	["cursed_arm_unwrapped_body", "Plate Armor", [("cursedarmtest",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
	
		#Skull Mask
	["skull_mask_with_hood", "Skull Mask", [("skull_mask_with_hood",0)], itp_type_head_armor|itp_unique, 0, 193, weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0),imodbits_cloth],
	
		#Arm, wrapped
	["cursedarm_wrapped","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#Arm, unwrapped
	["cursedarm_unwrapped","Gauntlets", [("cursedarmhandL",0)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#Cursed Dagger
	["cursed_dagger_thrown", "Thrown Cursed Arm Daggers", [("throwing_dagger",0)], itp_no_blur|itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground|itp_next_item_as_melee ,itcf_throw_knife, 193 , weight(2.5)|difficulty(0)|spd_rtng(130) | shoot_speed(100) | thrust_damage(34, cut)|max_ammo(30)|weapon_length(0),imodbits_thrown],
		
	["cursed_dagger", "Cursed Arm Dagger", [("dagger_b",0),], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary, itc_dagger, 37, weight(0.75)|difficulty(0)|spd_rtng(135) | weapon_length(30)|swing_damage(37, cut)|thrust_damage(35, pierce),imodbits_sword_high	],

	["cursedarm", "Cursed Arm Dagger", [("cursedarmweapon",0),], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary, itc_longsword, 37, weight(0.75)|difficulty(0)|spd_rtng(135) | weapon_length(30)|swing_damage(25, blunt)|thrust_damage(99999, pierce),imodbits_sword_high],
		#Cursed Dagger - Thrown
		
	#Serenity
		# Mask
	["serenity_mask", "Skull Mask", [("skull_mask",0)], itp_type_head_armor|itp_unique|itp_doesnt_cover_hair, 0, 193, weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0),imodbits_cloth],
		# Poison Dagger
		
	["serenity_dagger_thrown", "Thrown Cursed Arm Daggers", [("throwing_dagger",0)], itp_no_blur|itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground|itp_next_item_as_melee ,itcf_throw_knife, 193 , weight(2.5)|difficulty(0)|spd_rtng(130) | shoot_speed(100) | thrust_damage(34, cut)|max_ammo(30)|weapon_length(0),imodbits_thrown],
		
	["serenity_dagger", "Serenity Dagger", [("dagger_b",0),], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary, itc_dagger, 37, weight(0.75)|difficulty(0)|spd_rtng(135) | weapon_length(30)|swing_damage(37, cut)|thrust_damage(35, pierce),imodbits_sword_high	],
	
	
	#Shadow Peeling
		# Mask
	["shadow_mask", "Skull Mask", [("skull_mask",0)], itp_type_head_armor|itp_unique   ,0, 193 , weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0) ,imodbits_cloth ],
		# Dagger
		
	["shadow_dagger_thrown", "Thrown Cursed Arm Daggers", [("throwing_dagger",0)], itp_no_blur|itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground|itp_next_item_as_melee ,itcf_throw_knife, 193 , weight(2.5)|difficulty(0)|spd_rtng(130) | shoot_speed(100) | thrust_damage(34, cut)|max_ammo(30)|weapon_length(0),imodbits_thrown],
		
	["shadow_dagger", "Shadow-Peeling Dagger", [("dagger_b",0),], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary, itc_dagger|itcf_carry_dagger_front_left, 37, weight(0.75)|difficulty(0)|spd_rtng(135) | weapon_length(30)|swing_damage(37, cut)|thrust_damage(35, pierce),imodbits_sword_high	],
	
	#100 Face
	
		#100 Face Dagger
	
	["hundred_dagger_thrown", "Thrown Cursed Arm Daggers", [("throwing_dagger",0)], itp_no_blur|itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground|itp_next_item_as_melee ,itcf_throw_knife, 193 , weight(2.5)|difficulty(0)|spd_rtng(130) | shoot_speed(100) | thrust_damage(34, cut)|max_ammo(30)|weapon_length(0),imodbits_thrown],
		
	["hundred_dagger", "Hundred Face Dagger", [("dagger_b",0),], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary, itc_dagger|itcf_carry_dagger_front_left, 37, weight(0.75)|difficulty(0)|spd_rtng(135) | weapon_length(30)|swing_damage(37, cut)|thrust_damage(35, pierce),imodbits_sword_high	],
		#100 Face Dagger - Thrown
	
		#100 Face Mask
	["hundred_face_mask", "Skull Mask", [("skull_mask",0)], itp_type_head_armor|itp_doesnt_cover_hair|itp_unique   ,0, 193 , weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0) ,imodbits_cloth ],
		
		
	#King Hassan
		#Armor
	["king_hassan_armor", "Plate Armor", [("robe",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Skull Helmet
	["skull_helmet", "Skull Mask", [("skull_mask",0)], itp_type_head_armor|itp_unique, 0, 193, weight(2)|abundance(100)|head_armor(54)|body_armor(5)|leg_armor(0), imodbits_cloth],
		
		#Giant Sword 
	["hassan_sword", "The Sword of the Old Man of the Mountain", [("azrael",0)], itp_no_blur|itp_type_two_handed_wpn|itp_unique|itp_primary, itc_bastardsword|itcf_carry_sword_back, 263, weight(1.5)|difficulty(0)|spd_rtng(96) | weapon_length(95)|swing_damage(47, cut)|thrust_damage(0, pierce), imodbits_sword_high],
		
		#Shield
	["coffin_shield", "Heavy Board Shield", [("tableau_shield_pavise_1" ,0)], itp_unique|itp_type_shield|itp_cant_use_on_horseback|itp_wooden_parry, itcf_carry_board_shield, 370, weight(5)|hit_points(550)|body_armor(14)|spd_rtng(78)|shield_width(43)|shield_height(100), imodbits_shield],
		
 #Berserker Gear
	#Heracles
		#Nameless Axe-Sword
	["axe_sword", "Stone Axe-Sword", [("axesword",0)], itp_no_blur|itp_unique|itp_type_two_handed_wpn|itp_primary, itc_greatsword|itcf_carry_sword_back, 1123, weight(2.75)|difficulty(1)|spd_rtng(87)|weapon_length(150)|swing_damage(53, cut)|thrust_damage(40,  cut), imodbits_none],
		
		#Armored Skirt thing
	["heracles_wear", "Plate Armor", [("full_plate_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Arm Cuffs
	["heracles_armcuffs","Gauntlets", [("gauntlets_L",0)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#Foot Cuffs
	["heracles_footcuffs", "Black Greaves", [("black_greaves",0)], itp_type_foot_armor|itp_attach_armature, 0, 2361 , weight(3.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(45)|difficulty(0) ,imodbits_none],
		
	#Lancelot
		#Arondight
	["zerkalot_sword", "Arondight", [("sword_two_handed_a",0)], itp_no_blur|itp_type_two_handed_wpn|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back, 1123, weight(2.75)|difficulty(10)|spd_rtng(87)|weapon_length(120)|swing_damage(53, cut) | thrust_damage(40 ,  pierce),imodbits_none],
	
		#Dark Armor
	["berserkelot_armor", "Black Armor", [("black_armor",0)], itp_type_body_armor|itp_covers_legs, 0, 9496, weight(28)|abundance(0)|head_armor(0)|body_armor(75)|leg_armor(25)|difficulty(10), imodbits_none],
	
		#Dark Helmet
	["berserkelot_helmet", "Full Helm", [("great_helmet_new_b",0)], itp_type_head_armor |itp_covers_head , 0, 811, weight(2.5)|abundance(0)|head_armor(75)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_none, 
		[(ti_on_init_item, [
		
			 (store_trigger_param_1, ":agent"),
			 (gt, ":agent", 0),
             (agent_get_position, pos0, ":agent"),
			 
			 (try_for_range, reg4, 0, 5),
				(val_sub, reg10, 120),
                (set_position_delta, pos0, reg10, 0, 0), #Position, X, Y, Z.
			    (particle_system_add_new, "psys_lancelot_obscurement_smoke", pos1),
				(store_mul, reg10, reg4, 25),
             (try_end),
			 
			]
			)
		]],
			 
		#Dark Greaves
	["berserkelot_greaves", "Black Greaves", [("black_greaves",0)], itp_type_foot_armor  | itp_attach_armature,0, 2361 , weight(3.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(45)|difficulty(0) ,imodbits_none],
	
		#Dark Gauntlets
	["berserkelot_gloves","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_type_hand_armor,0, 1040, weight(1.0)|abundance(0)|body_armor(20)|difficulty(0),imodbits_none],
	
	#Sparticus
		#Gladiator Sword
	["spart_sword", "Sword", [("spartacus_sword",0)], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary, itc_scimitar|itcf_carry_sword_left_hip, 263, weight(1.5)|difficulty(0)|spd_rtng(96) | weapon_length(95)|swing_damage(35, cut)| thrust_damage(0, pierce), imodbits_sword_high],
	
	["spart_club", "Club", [("spartacus_club",0)], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary, itc_scimitar|itcf_carry_sword_left_hip, 263, weight(1.5)|difficulty(0)|spd_rtng(96) | weapon_length(95)|swing_damage(35, pierce)| thrust_damage(0, blunt), imodbits_sword_high],
		
		#Face Gear
	["spart_mask", "Bondage Mask", [("spartacus_mask",0)], itp_type_head_armor |itp_covers_hair ,0, 811 , weight(2.5)|abundance(0)|head_armor(75)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_none],
		
		#Slave restraints
	["spart_restraints", "Plate Armor", [("man_body",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Shinguards
	["spart_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
		#Arm Guards
	["spart_gauntlets","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
	#Frankenstein
		#Weird Club - Bridal Chest
	["bridal_chest",  "Bridal Chest", [("bridal_chest",0)], itp_no_blur|itp_type_polearm|itp_offset_lance| itp_primary|itp_two_handed|itp_unique, itc_staff, 169, weight(7)|difficulty(18)|spd_rtng(78)|weapon_length(130)|swing_damage(50, blunt) | thrust_damage(35, blunt), imodbits_polearm],
		
		#Bridal Chest - Opened for Blasted Tree
	["bridal_chest_opened",  "Bridal Chest", [("bridal_chest",0)], itp_no_blur|itp_type_polearm|itp_offset_lance| itp_primary|itp_two_handed|itp_unique, itc_staff, 169 , weight(7)|difficulty(18)|spd_rtng(78) | weapon_length(130)|swing_damage(50 , blunt) | thrust_damage(35, blunt), imodbits_polearm ],
		
		#Wedding Dress
	["frankie_dress", "Plate Armor", [("bride_dress",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Heels
	["frankie_boots", "Plate Boots", [("bride_shoes",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
		#fran's gloves
	["fran_gloves","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#Horn and veil
	["fran_veil", "Crown of Frankenstein", [("bride_crown",0)],  itp_type_head_armor|itp_doesnt_cover_hair|itp_unique|itp_attach_armature, 0, 1, weight(0.5)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
		
	#Asterios
		#Left Poleaxe - Labrys
	["labrys_left", "Labrys", [("asterios_l",0)], itp_unique|itp_type_shield|itp_no_pick_up_from_ground, 0,  697 , weight(2.5)|hit_points(1700)|body_armor(0)|spd_rtng(61)|shield_width(10), imodbits_shield ],
		
		#Right Polexe - Labrys
	["labrys_right", "Labrys", [("asterios",0)], itp_no_blur|itp_type_polearm|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced|itp_unique, itc_bastardsword, 660, weight(5.5)|difficulty(10)|spd_rtng(91)|weapon_length(170)|swing_damage(54, cut)|thrust_damage(19, blunt),imodbits_axe],
		
		#Bullhead Mask
	["minotaur_mask", "Minotaur Mask", [("asterios_mask",0)], itp_unique|itp_type_head_armor|itp_doesnt_cover_hair,0, 1240 , weight(2.75)|abundance(100)|head_armor(55)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],	
		
		#Stomach-guard and Dress?
	["asterios_belt", "Plate Armor", [("man_body",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Arm Bindings
	["asterios_bindings","Gauntlets", [("gauntlets_L",0),("gauntlets_L", imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#Leg Bindings
	["asterios_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
	
	# #### Fate Noble Phantasms ############
	# ######## and Servant Weapons #########
	
	# Dainsleif
	# Durandal
	# Gram
	# Harpe
	["harpe", "Harpe", [("scythe",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_staff|itcf_carry_spear, 43 , weight(2)|difficulty(0)|spd_rtng(97) | weapon_length(182)|swing_damage(30 , cut) | thrust_damage(14, pierce), imodbits_polearm ],
	
	# Houtengeki
	# Vajra
	
		
		#Stomach-guard and Dress?
	["mash_armor", "Plate Armor", [("black_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Arm Bindings
	["mash_guants","Gauntlets", [("gauntlets_L",0),("gauntlets_L", imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#Leg Bindings
	["mash_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
	
	["mash_shield", "Mash Shield",   [("mash",0)], itp_no_blur|itp_unique|itp_type_shield|itp_cant_use_on_horseback|itp_wooden_parry, itcf_carry_board_shield, 370, weight(75)|hit_points(2000)|body_armor(45)|spd_rtng(88)|shield_width(600)|shield_height(300),imodbits_shield],
	
	["mash_shield_bash", "invisible wpn", [("none",0)], itp_unique|itp_type_one_handed_wpn|itp_can_knock_down|itp_primary|itp_wooden_parry|itp_unbalanced, itc_cleaver|itcf_carry_axe_back, 510, weight(5.0)|difficulty(10)|spd_rtng(35)|weapon_length(125)|swing_damage(50, cut)|thrust_damage(0, pierce),imodbits_axe,
		[(ti_on_weapon_attack, [
			(store_trigger_param_1, ":agent_id"),
			(set_fixed_point_multiplier, 100),
			
			(agent_get_action_dir, ":attack_dir", ":agent_id"),
			
			(try_begin),
				(eq, ":attack_dir", 0),
				(agent_set_animation, ":agent_id", "anim_release_overswing_staff", 1),
			(else_try),
				(eq, ":attack_dir", 1),
				(agent_set_animation, ":agent_id", "anim_release_slashright_staff", 1),
			(else_try),
				(eq, ":attack_dir", 2),
				(agent_set_animation, ":agent_id", "anim_release_slashleft_staff", 1),
			(else_try),
				(eq, ":attack_dir", 3),
				(agent_set_animation, ":agent_id", "anim_release_overswing_staff", 1),
			(try_end),
		
			(call_script, "script_fate_mash_shieldbash", ":agent_id"),	
		])]],
#Master Items
 #Summoning Gear
  #Saber Catalysts
   #Tester Saber
   ["relic_saber","Saber Relic", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Artoria
   ["relic_saber01","Avalon", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Nero Claudius
   ["relic_saber02","Golden Cithara", [("lyre",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Siegfried
   ["relic_saber03","Perserved Fig Leaf", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Fergus mac Roich
   ["relic_saber04","Humming Stone Shard", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Richard Lionheart
   ["relic_saber05","Decorative Lead Sword", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
  #Lancer Catalysts
   #Tester Lancer
   ["relic_lancer","Lancer Relic", [("honey_pot",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Cu Chulainn
   ["relic_lancer01","Strange Earring", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Diarmuid Ua Duibhne
   ["relic_lancer02","Ancient Cup", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Fionn mac Cumhaill
   ["relic_lancer03","Boar-shaped Horn", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Hector
   ["relic_lancer04","Hector", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Brynhildr
   ["relic_lancer05","Brynhildr", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
  #Archer Catalysts
   #Tester Archer
   ["relic_archer","Archer Relic", [("leatherwork_inventory",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Heroic Spirit EMIYA
   ["relic_archer01","Jeweled Pendant", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Gilgamesh
   ["relic_archer02","Fossilized Snake Skin", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Atalanta
   ["relic_archer03","Longship Mast", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Robin Hood
   ["relic_archer04","Yew Tree Stump", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Arash
   ["relic_archer05","Fossilized Walnut", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
  #Rider Catalysts
   #Tester Rider
   ["relic_rider","Rider Relic", [("steppe_helmetY",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Medusa
   ["relic_rider01","Statuesque Head", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Iskandar
   ["relic_rider02","Torn Fabric", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Achilles
   ["relic_rider03","Jar of Oil", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Astolfo
   ["relic_rider04","Strange Liquid in Glass Bottle", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Saint George
   ["relic_rider05","Alleged Dragon Scales", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
  #Caster Catalysts
   #Tester Caster
   ["relic_caster","Caster Relic", [("book_a",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Medea
   ["relic_caster01","Shining Fleece", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Gilles de Rais
   ["relic_caster02","Le Mistere du Siege d'Orleans", [("book_d",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #William Shakespeare
   ["relic_caster03","Broken Pen", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Avicebron
   ["relic_caster04","Strange Stone Shards", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Hohenheim
   ["relic_caster05","Gilded Flask", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
  #Assassin Catalysts TODO: Do these get Catalysts?
   ["relic_assassin","Generic Assassin Relic", [("throwing_dagger",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_assassin01","Cursed Arm Relic", [("throwing_dagger",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_assassin02","Hundred Faces Relic", [("throwing_dagger",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_assassin03","Shadow Peeling Relic", [("throwing_dagger",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_assassin04","Serenity Relic", [("throwing_dagger",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_assassin05","King Hassan Relic", [("throwing_dagger",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
  #Berserker Catalysts
   #Tester Berserker
   ["relic_berserker","Berserker Relic", [("vaeg_helmet9",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Heracles
   ["relic_berserker01","Immense Stone Slab", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(570)|abundance(0),imodbits_none],
   #Lancelot
   ["relic_berserker02","Wrapped Elm Branch", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Sparticus
   ["relic_berserker03","Roman Manacles", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Frankenstein's Monster
   ["relic_berserker04","Bizarre Blueprints", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Asterios
   ["relic_berserker05","Polished Thread", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_avenger","Avenger Relic", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_avenger01","Book of Evil", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_ruler","Ruler Relic", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_ruler01","Cross", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_ruler02","Japan Cross", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_shielder","Shielder Relic", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_shielder01","Galahad", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   
   ["relics_end","Not a thing", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   
   ["gob_sword","Tresure Sword", [("scimitar_b",0),("scimitar_b",ixmesh_flying_ammo)], itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground, itcf_throw_javelin|itcf_carry_sword_left_hip,
	360, weight(2.5)|difficulty(0)|spd_rtng(99) | shoot_speed(600) | thrust_damage(20,pierce)|max_ammo(1)|weapon_length(55),imodbits_thrown_minus_heavy, 
	[
	
	(ti_on_missile_hit, [
			 # (particle_system_burst, "psys_gourd_smoke", pos1, 100),
			 # (particle_system_burst, "psys_fireplace_fire_big", pos1, 100),
			 # (call_script, "script_cf_fate_explosion", 750, 1250, 2500),
            ]),			
	]],	
	
	["gob_sword2","Tresure Sword", [("bastard_sword_b",0),("bastard_sword_b",ixmesh_flying_ammo)], itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground,itcf_throw_javelin|itcf_carry_sword_left_hip,
	360, weight(2.5)|difficulty(0)|spd_rtng(99) | shoot_speed(600) | thrust_damage(20,pierce)|max_ammo(1)|weapon_length(75),imodbits_thrown_minus_heavy, 
	[
	
	(ti_on_missile_hit, [
			 # (particle_system_burst, "psys_gourd_smoke", pos1, 100),
			 # (particle_system_burst, "psys_fireplace_fire_big", pos1, 100),
			 # (call_script, "script_cf_fate_explosion", 750, 1250, 2500),
            ]),			
	]],	
	
	["gob_spear1","Tresure Sword", [("military_sickle_a",0),("military_sickle_a",ixmesh_flying_ammo)], itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground,itcf_throw_javelin|itcf_carry_sword_left_hip,
	360, weight(2.5)|difficulty(0)|spd_rtng(99) | shoot_speed(600) | thrust_damage(20,pierce)|max_ammo(1)|weapon_length(70),imodbits_thrown_minus_heavy, 
	[
	
	(ti_on_missile_hit, [
			 # (particle_system_burst, "psys_gourd_smoke", pos1, 100),
			 # (particle_system_burst, "psys_fireplace_fire_big", pos1, 100),
			 # (call_script, "script_cf_fate_explosion", 750, 1250, 2500),
            ]),			
	]],
	
	["gob_spear2","Tresure Sword", [("khergit_pike_b",0),("khergit_pike_b",ixmesh_flying_ammo)], itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground,itcf_throw_javelin|itcf_carry_sword_left_hip,
	360, weight(2.5)|difficulty(0)|spd_rtng(99) | shoot_speed(600) | thrust_damage(20,pierce)|max_ammo(1)|weapon_length(120),imodbits_thrown_minus_heavy, 
	[
	
	(ti_on_missile_hit, [
			 # (particle_system_burst, "psys_gourd_smoke", pos1, 100),
			 # (particle_system_burst, "psys_fireplace_fire_big", pos1, 100),
			 # (call_script, "script_cf_fate_explosion", 750, 1250, 2500),
            ]),			
	]],
	
	["gob_axe","Tresure Sword", [("two_handed_battle_long_axe_c",0),("two_handed_battle_long_axe_c",ixmesh_flying_ammo)], itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground,itcf_throw_javelin|itcf_carry_sword_left_hip,
	360, weight(2.5)|difficulty(0)|spd_rtng(99) | shoot_speed(600) | thrust_damage(45,pierce)|max_ammo(1)|weapon_length(65),imodbits_thrown_minus_heavy, 
	[
	
	(ti_on_missile_hit, [
			 # (particle_system_burst, "psys_gourd_smoke", pos1, 100),
			 # (particle_system_burst, "psys_fireplace_fire_big", pos1, 100),
			 # (call_script, "script_cf_fate_explosion", 750, 1250, 2500),
            ]),			
	]],

 # Specific NPC Gear
	# Taiga
 # Specific Master Gear # Use Generalized Gear Whenever Possible
	# #######Fate/ SN - Hollow Ataraxia Masters
		# Shirou Emiya
			# Baseball Tee Shirt and Jeans
			["shirou_shirt", "Shirou Casual", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
			# Converse Style Sneakers
			["shirou_sneakers", "Hi-top Sneakers", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
			# Rolled Poster
			# Shinai
			# Right Arm Shroud (HF Route)
			["shirou_shirt_hf", "Baseball Shirt with Shroud", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
		# Rin Tohsaka
			# Red Top with Black Skirt
			["rin_outfit", "Red Top and Skirt", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
			# Knee High Socks with Shoes
			["rin_heels", "Tohsaka Dress Shoes", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
			# Bows in Hair
			["rin_ribbon", "Rin Hair Ribbons", [("felt_hat_a_new",0)],  itp_type_head_armor |itp_civilian,0, 4 , weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
			# Single Use Gem Spells
				# Freeze
				["gemstone_saph", "Charged Sapphire", [("gandr",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
				# Explosion
				["gemstone_red", "Charged Ruby", [("gandr",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
				# Stun
				["gemstone_yellow", "Charged Yellow", [("gandr",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
				# Trap
				["gemstone_lapis", "Charged Lapis", [("gandr",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
			
		# Illyasviel von Einzbern
			# Winter Cap
			["illya_cap", "Woolen Cap", [("felt_hat_a_new",0)],  itp_type_head_armor |itp_civilian,0, 4 , weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
			# Peacoat
			["illya_peacoat", "Peacoat", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
			# Mittens
			["illya_mittens","Wool Mittens", [("leather_gloves_L",0)], itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],
			# Wire Bird
			["wire_bird", "Bird Familiar", [("gandr",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
		# Sakura Matou
			# Ribbons in Hair
			# Cherry Blossom Apron
			# School Uniform
			# Casual Sweater
			# HF Shadow Outfit
		# Shinji Matou
			# Command Seal Book
			# School Uniform
			# Knife
		# Souichirou Kuzuki
			# Three Piece Suit
			# Glasses
		# Kirei Kotomine
			# Priest Garb
			# Sleeveless Combat Shirt
		# Zouken Matou
			# Kimono
			# Cane
			# BUGS
		# Bazett Fraga McRemitz
			# Fragarach - Essentially Punch Dagger
			# Seal Designation Enforcer Suit
			# Leather Gloves
	# #######Fate/ Zero Masters
		# Kiritsugu Emiya
			# Cigarettes
			# Bone Bullets
		# Kirei Kotomine
			# Shawl
			# Enhanced Black Keys
		# Tokiomi Tohsaka
			# Red Suit
			# Crystals
			# Gem Magic
			# Gemmed Cane
		# Waver Velvet
			# Sweater and Collared Shirt, Khakis
			# Brown Leather Shoes
			# Sacrificial Knife
		# Kariya Matou
			# Bugs
			# Hoodie and Dirty Jeans
			# Torn Sneakers
		# Ryuunosuke Uryuu
			# Knife
			# Casual Clothes
			# Pistol
		# Kayneth El-Melloi Archibald
			# Coat
			# Metallic ~ Autonomous Ball ~ Thingie
			# Gloves
	   
	# #######Fate/ Apocrypha Masters
		# ########Black Faction -Yggdmillennia's-
			#Yggdmillennia Suit
		# Darnic Prestone Yggdmillennia (Vlad III, Lancer)
		# Gordes Musik Yggdmillennia (Siegfried, Saber)
		# Fiore Forvedge Yggdmillennia (Chiron, Archer)
			# Iron Spider Legs
		# Celenike Icecolle Yggdmillennia (Astolfo, Rider)
			# Knives
		# Roche Frain Yggdmillennia (Avicebron, Caster)
		# Caules Forvedge Yggdmillennia (Frankenstein, Berserker)
		# Reika Rikudou (Jack the Ripper, Assassin)
			# Short Green Dress
			# Knife
			# Revolver
		# ######### Red Faction
		# Kairi Sisigou (Mordred, Saber)
			# Finger Homing Shotgun Shells
			# 
		# Rottweil Berzinsky‎ (Atalanta, Archer)
		# Feend vor Sembren ‎(Karna, Lancer)
		# Cabik Pentel‎‎ (Achilles, Rider)
		# Jean Rum‎ (Shakespeare, Caster)
		# Shirou Kotomine (Semiramis, Assassin)
			# Holy Katana
			# Priest Garb
			# Samurai Garb
			# Black Keys
		# Deimlet Pentel‎‎ (Spartacus, Berserker)
	# #############Fate/ strange fake
	
	# #############Fate/ Ex-
		# Hakuno Kishinami -male NERO, NAMELESS, Fox
		# Hakuno Kishinami -female NERO or NAMELESS, Fox
		# Rin Tohsaka -extra (fancy boy Cu Chulainn)
			# Rin Extra Outfit
			# Rin Extra Ribbon
		# Rani VIII (Lu Bu, so a Berserker)
			# Atlas Outfit
			
		# Shinji Matou (Francis Drake, RIDER)
			# Knife
		# Leonard B. Harway (Gawain, SABER)
		# Dan Blackmore (Robin Hood, Archer)
			# Old Style Sniper Rifle
		# Alice (Fairytale Caster)
		# Julius B. Harway (Li Shuwen, Assassin)
		# Run Ru (Lancer, Vlad III)
		# Monji Gatou (Berserker, Arcueid Brunestud - Tskikiheme-)
		# Twice H. Pieceman (Saver, "Savior", Buddha)
	
	# #############Fate/ Grand Order
		# Ritsuka Fujimaru, both genders
			# Various Available Outfits
	# #############Fate/ Prototype
		# Ayaka Sajyou: The Seventh, Princes (Arthur (Proto), Saber)
		# The Sixth, Powers (Servant Unknown)
		# Anonymous Master: The Fifth, Virtues (Gilgamesh (Proto), Archer)
		# The Forth, Dominons (Servant Unknown)
		# Aro Isemi: The Third, Thrones (Perseus, Rider)
		# Misaya Reiroukan: The Second, Cherubim (Cú Chulainn (Proto), Lancer)
		# Sancraid Phahn: VOID, Absences (Heracles (Proto), Berserker)
		# Manaka Sajyou: The First, Seraphim (Beast VI, Beast)


   #Generic Mage Gear
	#Azoth Sword
	["azoth_sword", "Azoth Sword", [("azoth",0),], itp_no_blur|itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip, 162, weight(1.25)|difficulty(0)|spd_rtng(103)|weapon_length(85)|swing_damage(28, cut)|thrust_damage(21, pierce),imodbits_sword_high],
	
	["test_lightsaber", "Lightsaber", [("lightsaber",0),], itp_no_blur|itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip, 162, weight(1.25)|difficulty(0)|spd_rtng(103)|weapon_length(85)|swing_damage(28, cut)|thrust_damage(21, pierce),imodbits_sword_high],
	
	["imod_test_helmet", "Agius Ears", [("helmet_fur_a",imod_plain), ("azoth",imod_tattered), ("flintlock_pistol",imod_ragged), ("apples",imod_sturdy), ("linen",imod_thick), ("avalon", imod_hardened)], itp_type_head_armor|itp_unique, 0, 193, weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0), imodbits_cloth],
 #Modern Weapons
	#Multiple Modern Firearms IMFDB as Source
		# Ammunition
		["cartridges", "Cartridges", [("cartridge_a",0)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_default_ammo, 0, 41, weight(2.25)| abundance(90)| weapon_length(3)|thrust_damage(1, pierce)| max_ammo (50), imodbits_missile],
		
		["origin_rounds", "Origin Rounds", [("cartridge_a",0)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_default_ammo, 0, 41, weight(2.25)| abundance(90)| weapon_length(3)|thrust_damage(15, pierce)| max_ammo (18), imodbits_missile],
		
		["pistol_ammo", "Cartridges", [("cartridge_a",0)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_default_ammo, 0, 41, weight(2.25)| abundance(90)| weapon_length(3)|thrust_damage(3, pierce)| max_ammo (25), imodbits_missile],
		
		["pistol_ammo_piercing", "Cartridges", [("cartridge_a",0)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_default_ammo, 0, 41, weight(2.25)| abundance(90)| weapon_length(3)|thrust_damage(5, pierce)| max_ammo (35), imodbits_missile],
		
		["rifle_ammo", "Cartridges", [("cartridge_a",0)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_default_ammo, 0, 41, weight(2.25)| abundance(90)| weapon_length(3)|thrust_damage(7, pierce)| max_ammo (30), imodbits_missile],
		
		["rifle_ammo_piercing", "Cartridges", [("cartridge_a",0)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_default_ammo, 0, 41, weight(2.25)| abundance(90)| weapon_length(3)|thrust_damage(10, pierce)| max_ammo (20), imodbits_missile],
		
		["heavy_cartridge", "Cartridges", [("cartridge_a",0)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_default_ammo, 0, 41, weight(75)| abundance(90)| weapon_length(3)|thrust_damage(35, pierce)| max_ammo (200), imodbits_missile],
		
		["rocket_ammo", "Cartridges", [("cartridge_a",0)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_default_ammo, 0, 41, weight(75)| abundance(90)| weapon_length(3)|thrust_damage(85, pierce)| max_ammo (6), imodbits_missile],
		
		["gandr_shell", "Cursed Shell", [("cartridge_a",0), ("gandr", ixmesh_flying_ammo)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_default_ammo, 0, 41, weight(75)| abundance(90)| weapon_length(3)|thrust_damage(35, pierce)| max_ammo (6), imodbits_missile],
			#pistols
			
		#Beretta 92F
		["beretta", "Beretta 92F", [("beretta92fs",0)], itp_type_pistol|itp_primary ,itcf_shoot_pistol|itcf_reload_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(55) | shoot_speed(160) | thrust_damage(45 ,pierce)|max_ammo(12)|accuracy(85),imodbits_none,
			[]
			],
		#Colt M1911A1
		["colt", "Colt M1911A1", [("1911",0)], itp_type_pistol|itp_primary ,itcf_shoot_pistol|itcf_reload_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(45) | shoot_speed(160) | thrust_damage(55 ,pierce)|max_ammo(7)|accuracy(75),imodbits_none,
			[]
			],
		#Glock 17
		["glock", "Glock 17", [("glock17",0)], itp_type_pistol|itp_primary ,itcf_shoot_pistol|itcf_reload_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(75) | shoot_speed(160) | thrust_damage(35 ,pierce)|max_ammo(17)|accuracy(90),imodbits_none,
			[]
			],
		#Contender
		["contender", "Contender", [("contender",0)], itp_type_pistol|itp_primary ,itcf_shoot_pistol|itcf_reload_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(55) | shoot_speed(250) | thrust_damage(75 ,pierce)|max_ammo(1)|accuracy(150),imodbits_none,
			[]
			],
		#Tokarev TT-33
		["tokarev", "Tokarev TT-33", [("TT33",0)], itp_type_pistol|itp_primary ,itcf_shoot_pistol|itcf_reload_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(65) | shoot_speed(160) | thrust_damage(35 ,pierce)|max_ammo(8)|accuracy(85),imodbits_none,
			[]
			],
		#Walther PP
		["walther", "Walther PP", [("waltherPPK",0)], itp_type_pistol|itp_primary ,itcf_shoot_pistol|itcf_reload_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(70) | shoot_speed(160) | thrust_damage(30 ,pierce)|max_ammo(10)|accuracy(85),imodbits_none,
			[]
			],
		#Webley Mk. IV
		["webley", "Webley Mk. IV", [("webley",0)], itp_type_pistol|itp_primary ,itcf_shoot_pistol|itcf_reload_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(45) | shoot_speed(160) | thrust_damage(35 ,pierce)|max_ammo(6)|accuracy(90),imodbits_none,
			[]
			],
		
			#smgs
		#Calico M950
		["calico", "Calico M950", [("flintlock_pistol",0)], itp_type_pistol|itp_primary, itcf_shoot_pistol|itcf_reload_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(55) | shoot_speed(160) | thrust_damage(35 ,pierce)|max_ammo(50)|accuracy(45),imodbits_none,
			[]
			],
		#Heckler & Koch MP5A3
		["mp5", "HK MP5A3", [("mp5",0)], itp_type_musket|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 230, weight(1.5)|difficulty(0)|spd_rtng(55)|shoot_speed(160)|thrust_damage(45, pierce)|max_ammo(30)|accuracy(55),imodbits_none,
			[]
			],
		#Madsen M50
		["madsen", "Madsen M50", [("flintlock_pistol",0)], itp_type_musket|itp_primary|itp_two_handed ,itcf_shoot_pistol|itcf_reload_pistol, 230, weight(1.5)|difficulty(0)|spd_rtng(55)| shoot_speed(160)|thrust_damage(35, pierce)|max_ammo(32)|accuracy(50),imodbits_none,
			[]
			],
		#AKMS
		["akms", "AKMS", [("akm",0)], itp_type_musket|itp_primary|itp_two_handed ,itcf_shoot_pistol|itcf_reload_pistol, 230, weight(1.5)|difficulty(0)|spd_rtng(55)|shoot_speed(160)|thrust_damage(50, pierce)|max_ammo(35)|accuracy(30),imodbits_none,
			[]
			],
		
			#assault rifles
		#Steyr AUG A1
		["aug", "Steyr AUG A1", [("AugA1",0)], itp_type_musket|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 230, weight(1.5)|difficulty(0)|spd_rtng(55)| shoot_speed(160)|thrust_damage(60, pierce)|max_ammo(42)|accuracy(65),imodbits_none,
			[]
			],
		#AK-47
		["ak47", "AK47", [("Ak47",0)], itp_type_musket|itp_primary|itp_two_handed|itp_next_item_as_melee ,itcf_shoot_musket|itcf_reload_pistol, 230, weight(1.5)|difficulty(0)|spd_rtng(55)| shoot_speed(160)|thrust_damage(45, pierce)|max_ammo(30)|accuracy(50),imodbits_none,
			[]
			],
		["ak47_melee", "AK47", [("Ak47",0)], itp_no_blur|itp_type_polearm|itp_primary|itp_can_penetrate_shield|itp_wooden_parry, itc_spear, 76, weight(3.5)|difficulty(0)|spd_rtng(85)|weapon_length(65)|thrust_damage(35, pierce),imodbits_polearm],
		
		#FN FAL
		["fn_fal", "FN FAL", [("fnfal",0)], itp_type_musket|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 230, weight(1.5)|difficulty(0)|spd_rtng(55)| shoot_speed(160)|thrust_damage(45, pierce)|max_ammo(20)|accuracy(65),imodbits_none,
			[]
			],
		
			#Shotgun
		#Remington 870
		["remington_shotgun", "Remington 870", [("remington870",0)], itp_type_musket|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(55) | shoot_speed(160) | thrust_damage(15 ,pierce)|max_ammo(8)|accuracy(50),imodbits_none,
			[]
			],
			
		["remington_shotgun_so", "Remington 870 (Sawed-off)", [("remington870so",0)], itp_type_musket|itp_primary|itp_two_handed ,itcf_shoot_pistol|itcf_reload_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(55) | shoot_speed(160) | thrust_damage(15 ,pierce)|max_ammo(8)|accuracy(50),imodbits_none,
			[]
			],
			
		#Sawed-off Savage/Stevens 311A
		["sawedoff", "Sawed-off Savage/Stevens 311A", [("sawedoff",0)], itp_type_pistol|itp_primary ,itcf_shoot_pistol|itcf_reload_pistol, 230 , weapon_length(10)|weight(1.5)|difficulty(0)|spd_rtng(55) | shoot_speed(100) | thrust_damage(45 ,pierce)|max_ammo(2)|accuracy(55),imodbits_none,
			[]
			],
		
			#Sniper/Hunting Rifles
		#Barrett M82A1M
		["m82", "Beretta M82A1M", [("Barrett_M82A1",0)], itp_type_musket|itp_primary|itp_two_handed,itcf_shoot_musket|itcf_reload_pistol, 230, weight(1.5)|difficulty(0)|spd_rtng(35)| shoot_speed(160)|thrust_damage(135, pierce)|max_ammo(5)|accuracy(135),imodbits_none,
			[]
			],
		#Remington 700
		["remington_rifle", "Remington 700", [("flintlock_pistol",0)], itp_type_musket|itp_primary|itp_two_handed,itcf_shoot_musket|itcf_reload_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(75)| shoot_speed(160)|thrust_damage(45, pierce)|max_ammo(10)|accuracy(115),imodbits_none,
			[]
			],
		#Walther WA 2000
		["wa2000", "Walther WA 2000", [("WA2000",0)], itp_type_musket|itp_primary|itp_two_handed ,itcf_shoot_musket|itcf_reload_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(55)| shoot_speed(160)|thrust_damage(65, pierce)|max_ammo(6)|accuracy(150),imodbits_none,
			[]
			],
		
			#Heavy Weapons
		#M61 Vulcan
		["vulcan", "M61 Vulcan", [("vulcan",0)], itp_type_musket|itp_primary|itp_two_handed ,itcf_shoot_pistol|itcf_reload_pistol, 230 , weapon_length(150)|weight(65.5)|difficulty(0)|spd_rtng(15) | shoot_speed(160) | thrust_damage(85 ,pierce)|max_ammo(250)|accuracy(15),imodbits_none,
			[]
			],
		#Type 91 MANPADS
		["rocket", "Type 91 MANPADS", [("flintlock_pistol",0)], itp_type_pistol|itp_primary|itp_two_handed ,itcf_shoot_pistol|itcf_reload_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(15) | shoot_speed(160) | thrust_damage(85 ,pierce)|max_ammo(1)|accuracy(85),imodbits_none,
			[]
			],
		
	#Swords, Daggers, Etc
		["nagimaki", "Nagimaki", [("snm_nagimaki",0),("snm_nagimaki_scabbard",ixmesh_carry)], itp_no_blur|itp_type_two_handed_wpn| itp_primary, itc_poleaxe|itcf_carry_spear|itcf_show_holster_when_drawn, 679 , weight(2.0)|difficulty(9)|spd_rtng(108) | weapon_length(1803)|swing_damage(32 , cut) | thrust_damage(18 ,  pierce),imodbits_sword ],
		#Kendo Training Sword
		["kendo_sword", "Azoth Sword", [("shinai",0),], itp_no_blur|itp_type_one_handed_wpn|itp_primary, itc_bastardsword|itcf_carry_sword_left_hip, 162, weight(1.25)|difficulty(0)|spd_rtng(103)|weapon_length(113)|swing_damage(28, blunt)|thrust_damage(21, blunt),imodbits_sword_high],
		#Taiga's Training Sword
		["taiga_sword", "Taiga's Shinai", [("shinai_taiga",0)], itp_no_blur|itp_type_one_handed_wpn|itp_primary, itc_bastardsword|itcf_carry_sword_left_hip, 162, weight(1.25)|difficulty(0)|spd_rtng(103)|weapon_length(115)|swing_damage(45, blunt)|thrust_damage(25, blunt),imodbits_sword_high],
		#Bokken
		["wooden_katana", "Bokken", [("snm_nagimaki",0),("snm_nagimaki_scabbard",ixmesh_carry)], itp_no_blur|itp_type_two_handed_wpn| itp_primary, itc_poleaxe|itcf_carry_spear|itcf_show_holster_when_drawn, 679 , weight(2.0)|difficulty(9)|spd_rtng(108) | weapon_length(115)|swing_damage(32 , cut) | thrust_damage(18 ,  pierce),imodbits_sword ],
		#Hunting Knife
		["hunting_knife", "Hunting Knife", [("peasant_knife_new",0)], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_left, 18, weight(0.5)|difficulty(0)|spd_rtng(110)|weapon_length(40)|swing_damage(51, cut)|thrust_damage(25, pierce),imodbits_sword],
	# Grenades, High Explosives
		#M67 Grenade
		["frag_grenade", "M67 Grenade", [("throwing_stone",0)], itp_type_thrown|itp_primary ,itcf_throw_stone, 1 , weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(30) | thrust_damage(11 ,  blunt)|max_ammo(18)|weapon_length(8),imodbit_large_bag ],
		#M18 Smoke Grenade
		["smoke_grenade", "M18 Smoke Grenade", [("throwing_stone",0)], itp_type_thrown|itp_primary ,itcf_throw_stone, 1 , weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(30) | thrust_damage(11 ,  blunt)|max_ammo(18)|weapon_length(8),imodbit_large_bag ],
		#Model 7290 Flashbang
		["stun_grenade", "Model 7290 Flashbang", [("throwing_stone",0)], itp_type_thrown|itp_primary ,itcf_throw_stone, 1 , weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(30) | thrust_damage(11 ,  blunt)|max_ammo(18)|weapon_length(8),imodbit_large_bag ],
		#C4
		["placed_explosive", "C4 Plastic Explosive", [("throwing_stone",0)], itp_type_thrown|itp_primary ,itcf_throw_stone, 1 , weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(30) | thrust_damage(11 ,  blunt)|max_ammo(18)|weapon_length(8),imodbit_large_bag ],
		["placed_explosive_trigger", "C4 Plastic Explosive - Remote", [("throwing_stone",0)], itp_type_thrown|itp_primary ,itcf_throw_stone, 1 , weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(30) | thrust_damage(11 ,  blunt)|max_ammo(18)|weapon_length(8),imodbit_large_bag ],
		#Mk 2 hand grenade
		["frag_grenade_01", "Mk2 Fragmentation Grenade", [("throwing_stone",0)], itp_type_thrown|itp_primary ,itcf_throw_stone, 1 , weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(30) | thrust_damage(11 ,  blunt)|max_ammo(18)|weapon_length(8),imodbit_large_bag ],
		#Mk 2 hand grenade - Heart Variant
		["necro_grenade", "Demonic Heart Grenade", [("throwing_stone",0)], itp_type_thrown|itp_primary ,itcf_throw_stone, 1 , weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(30) | thrust_damage(11 ,  blunt)|max_ammo(18)|weapon_length(8),imodbit_large_bag ],
	# Yumi
		["yumi", "Yumi", [("long_bow",0), ("long_bow_carry",ixmesh_carry)], itp_no_blur|itp_type_bow |itp_primary|itp_two_handed, itcf_shoot_bow|itcf_carry_bow_back, 145, weight(1.75)|difficulty(3)|spd_rtng(79)|shoot_speed(56)| thrust_damage(22, pierce), imodbits_bow],
	# Training Yumi
		["school_yumi", "Yumi", [("long_bow",0), ("long_bow_carry",ixmesh_carry)], itp_no_blur|itp_type_bow |itp_primary|itp_two_handed, itcf_shoot_bow|itcf_carry_bow_back, 145, weight(1.75)|difficulty(3)|spd_rtng(79)|shoot_speed(56)| thrust_damage(22, pierce), imodbits_bow],
		
 # Church Centric Weapons
	["black_key", "Black Keys", [("throwing_dagger",0)], itp_no_blur|itp_type_thrown|itp_primary|itp_next_item_as_melee ,itcf_throw_knife, 193 , weight(2.5)|difficulty(0)|spd_rtng(110) | shoot_speed(24) | thrust_damage(25 ,  cut)|max_ammo(13)|weapon_length(0),imodbits_thrown ],
	
	["black_key_melee", "Black Keys", [("throwing_dagger",0)], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary, itc_dagger|itcf_carry_dagger_front_left, 18, weight(0.5)|difficulty(0)|spd_rtng(110)|weapon_length(40)|swing_damage(51, cut)|thrust_damage(25, pierce),imodbits_sword],
 
 # Combat Spells
	["gandr", "Gandr", [("gandr",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
	
	["firebolt", "Fire Bolt", [("fire_bolt",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
	
	["windbolt", "Wind Bolt", [("wind_bolt",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
	
	["waterbolt", "Water Bolt", [("water_bolt",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
	
	["earthbolt", "Earth Bolt", [("earth_bolt",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
	
	["fireaoe", "Fire aoe", [("fire_bolt",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
	
	["windaoe", "Wind aoe", [("wind_bolt",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
	
	["wateraoe", "Water aoe", [("water_bolt",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
	
	["earthaoe", "Earth aoe", [("earth_bolt",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(0, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag,
			[(ti_on_missile_hit, [
				(set_fixed_point_multiplier, 100), 							# Will keep math consistant

				(try_for_agents, ":agents", pos1, 150),
					(agent_is_human, ":agents"),
					(agent_is_alive, ":agents"),
					
					(agent_get_slot, ":petrified", ":agents", fate_agent_petrified),
					(val_add, ":petrified", 2),
					(agent_set_slot, ":agents", fate_agent_petrified, ":petrified"),
					#(agent_set_animation, ":agents", "anim_petrify_1"),
				(try_end),
				]),
			],
			],
	["voidbolt", "Void Bolt", [("void_bolt",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(85, blunt)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
	
	# When hit with this, check against a seperate poison resistance
	["poison", "Poison Damage", [("snm_nagimaki",0),("snm_nagimaki_scabbard",ixmesh_carry)], itp_type_two_handed_wpn| itp_primary, itc_poleaxe|itcf_carry_spear|itcf_show_holster_when_drawn, 679 , weight(2.0)|difficulty(9)|spd_rtng(108) | weapon_length(115)|swing_damage(100, blunt) | thrust_damage(100,  blunt),imodbits_sword ],
	
	# When hit with this, check the attackers NP level
	["np_damage", "Noble Phantasm Damage", [("snm_nagimaki",0),("snm_nagimaki_scabbard",ixmesh_carry)], itp_type_two_handed_wpn| itp_primary, itc_poleaxe|itcf_carry_spear|itcf_show_holster_when_drawn, 679 , weight(2.0)|difficulty(9)|spd_rtng(108) | weapon_length(115)|swing_damage(100, blunt) | thrust_damage(100,  blunt),imodbits_sword ],
		
	#Mystic Codes
	
	#Heal Cast, Minor
	#Heal Cast, Major
	#Heal Cast, Perfect
	
	#Projection Magic: Sword
	#Projection Magic: Spear
	#Projection Magic: Shield
	
	#Reinforcement Magic: Legs
	#Reinforcement Magic: Weapon
	#Reinforcement Magic: Skin
	#Reinforcement Magic: Eyes
	
	#Time Alter Magic, Slow Time
	#Time Alter Magic, Double Slow Time
	#Time Alter Magic, Triple Slow Time
	
	#Mana Increase
	#Major Mana Increase
	
 # #####################################################################
 # ####### Servant Legend Books ########################################
 # #####################################################################
 
	["book_celtic_legends","Ulster Cycle", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Fergus, Cu, 
	
	["book_arthurian_legends","Historia Regum Britanniae", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Arthur, Richard, Lancelot
	
	["book_greek_history","Metamorphoses", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Medea, Achilles, Iskandar, Hector 
	
	["book_greek_legends","Mythology of Ancient Greece", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Heracles, Medusa, Atalanta, Asterios, 
	
	["book_roman_history","De vita Caesarum", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Nero, Spartacus
	
	["book_swedish_legends","Niflunga Saga", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Siegfried, Brynhildr
	
	["book_irish_legends","Fenian Cycle", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Fionn Mac Cool, Diarmund
	
	["book_french_legends","Chanson de Geste", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Astolfo, Gilles de Rais
	
	["book_biblical_legends","Acta Sanctorum", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Saint George, 
	
	["book_assassin_legends","Hashashin: Legends and Tales", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Assassins
	
	["book_modern_legends","Modern Myth: Folklore and Heroes", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Frankenstein, William Shakespeare, Hohenheim
	
	["book_ancient_legends","Ancient Myth, from Oral History to Legend", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Gilgamesh
	
	["book_philosophy_history", "Philosophy, Ethics and the Thinkers throughout History", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Avicebron
	  
	#Clothes
		#Casual Clothes
			#Shiro Style Baseball Tee
	["baseball_shirt", "Baseball Shirt", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
			#Tee and Jeans
	["tee_shirt", "Tee Shirt", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
			#Button-up and Jeans
	["casual_shirt", "Button-up Shirt", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
			#Office Worker Clothes
	["office_shirt", "Work Shirt", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
			#School Uniforms
				# Fuyuki High School
	["fuyuki_uniform", "Fuyuki High Uniform", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	["female_fuyuki_uniform", "Fuyuki High Uniform", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
				# Moon Cell School
	["moon_cell_uniform", "Tsukumihara Academy Uniform", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	["female_moon_cell_uniform", "Tsukumihara Academy Uniform", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
				# Teacher Suit
	["casual_suit_grey", "Cheap Suit", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["casual_suit_brown", "Cheap Suit", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["casual_suit_navy", "Cheap Suit", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
			# Assassin Clothing
				# Black Suits
	["black_suit", "Black Dress Suit", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
				# Black Leather Gloves
	["black_leather_gloves","Leather Gloves", [("leather_gloves_L",0)], itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],
	
	#Shoes, Boots, Footwear
		#Sandals, Geta
	["sandal_geta", "Geta Sandals", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_civilian|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Sandals, Clogs
	["sandal_birks", "Clog Suede Sandals", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_civilian|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Sandals, Slides
	["sandal_athletic", "Athletic Sandals", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_civilian|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Sandals, Thongs
	["sandal_thongs", "Flip-Flops", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_civilian|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
	
		#School, Inside Shoes
	["school_shoes", "School Shoes", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_civilian|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Sneakers, Converse
	["sneaker_hitop", "Hi-top Sneakers", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Sneakers, Runners
	["sneaker_running", "Running Shoes", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Sneakers, Skate
	["sneaker_skate", "Skate Shoes", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Sneakers, Knit
	["sneaker_knit", "Knit Sneakers", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		
		#Boots, Docs
	["boot_combat", "Combat Boots", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Boots, Timbs
	["boot_urban", "Wheat Work Boots", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Boots, Dress
	["boot_dress", "Dress Boots", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Boots, Dress
	["boot_dress_monkstrap", "Dress Boots", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Boots, Rain
	["boot_rain", "Rain Boots", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		
		#Dress, Cheap
	["dress_cheap", "Cheap Dress Shoes", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Dress, Oxford
	["dress_oxford", "Dress Shoes", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Dress, Monkstrap
	["dress_monk", "Monkstraps", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Dress, Wingtip
	["dress_wingtip", "Wingtips", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
	
	#Hats, Beanies
		#Beanie
	["beanie", "Beanie", [("felt_hat_a_new",0)],  itp_type_head_armor |itp_civilian,0, 4 , weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
		#Beanie with Pouf
	["ski_hat", "Ski Beanie", [("felt_hat_a_new",0)],  itp_type_head_armor |itp_civilian,0, 4 , weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
		#Baseball Cap
	["baseball_hat", "Baseball Cap", [("felt_hat_a_new",0)],  itp_type_head_armor |itp_civilian,0, 4 , weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
		#Large Floppy dumb Hipster Shit
	["widebrimmed_hat", "Wide-Brimmed Hat", [("felt_hat_a_new",0)], itp_type_head_armor |itp_civilian,0, 4 , weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
	
	# Gloves
	
	["driving_gloves_black","Leather Gloves", [("leather_gloves_L",0)], itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],
	
	["driving_gloves_brown","Leather Gloves", [("leather_gloves_L",0)], itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],
	
	["white_gloves","Leather Gloves", [("leather_gloves_L",0)], itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],
	
	["mittens","Leather Gloves", [("leather_gloves_L",0)], itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],
	
	["nordic_mittens","Leather Gloves", [("leather_gloves_L",0)], itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],
 #Holy Church Gear
	#Raiments
	["priest_raiment_black", "Priest Raiment", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["priest_raiment_red", "Priest Raiment", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["priest_raiment_white", "Priest Raiment", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["executor_raiment_black", "Executor Raiment", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["executor_raiment_red", "Executor Raiment", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
 #Clock Tower Gear
	# Mage's Gear, Old Style
	["mage_outfit_old", "Mage Garments", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["mage_outfit_eastern", "Mage Garments", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["mage_outfit_ragged", "Mage Garments", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["mage_outfit_noble", "Mage Garments", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["mage_outfit_wealthy_red", "Mage Garments", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["mage_outfit_wealthy_black", "Mage Garments", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["mage_outfit_modern", "Mage Garments", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["mage_outfit_highfashion", "Mage Garments", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],

	# Wire Horse Familiar
	["wire_familiar_horse","Wire Construct - Mount", [("courser",0)], itp_unique|itp_type_horse, 0, 600,abundance(0)|body_armor(255)|hit_points(255)|difficulty(2)|horse_speed(65)|horse_maneuver(47)|horse_charge(10)|horse_scale(102),imodbits_horse_basic|imodbit_champion],
	
	["kimono_modern", "Kimono", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["kimono_traditional", "Kimono", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["kimono_dress", "Kimono", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
 #	Summons Items
	["sakura_shadow", "Kimono", [("shadow",0)], itp_type_body_armor|itp_covers_legs|itp_covers_head, 0, 3, weight(1)|abundance(100)|head_armor(10)|body_armor(10)|leg_armor(10)|difficulty(0), imodbits_cloth ],
	["sakura_shadow_h", "Kimono", [("none",0)], itp_type_hand_armor, 0, 3, weight(1)|abundance(100)|head_armor(1)|body_armor(1)|leg_armor(1)|difficulty(0), imodbits_cloth ],
	["sakura_shadow_l", "Kimono", [("none",0)], itp_type_foot_armor, 0, 3, weight(1)|abundance(100)|head_armor(1)|body_armor(1)|leg_armor(1)|difficulty(0), imodbits_cloth ],
	
	["eldrich_eyes", "Kimono", [("eye_horror",0)], itp_type_body_armor|itp_covers_legs|itp_covers_head, 0, 3, weight(1)|abundance(100)|head_armor(10)|body_armor(10)|leg_armor(10)|difficulty(0), imodbits_cloth ],
	["eldrich_tendrils", "Kimono", [("tendril_horror",0)], itp_type_body_armor|itp_covers_legs|itp_covers_head, 0, 3, weight(1)|abundance(100)|head_armor(10)|body_armor(10)|leg_armor(10)|difficulty(0), imodbits_cloth ],
 # Fate Food Items
	## Ingredients
	### Proteins	
	# Salmon Filet
	["protein_salmon","Salmon", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(4),imodbits_none],
	# Mackerel Pike
	["protein_pike","Mackerel Pike", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(4),imodbits_none],
	# Yellow-fin Tuna
	["protein_yellowfin","Yellowfin Tuna", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(4),imodbits_none],
	# Bluefin Tuna
	["protein_bluefin","Bluefin Tuna", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(3),imodbits_none],
	# King Crab Legs
	["protein_crab","King Crab Legs", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Lobster
	["protein_lobster","Lobster", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(3),imodbits_none],
	# Shrimp
	["protein_shrimp","Raw Shrimp", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(8),imodbits_none],
	# Squid
	["protein_squid","Raw Squid", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(2),imodbits_none],
	# Chicken
	["protein_chicken","Chicken", [("chicken",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Beef
	["protein_beef","Beef", [("raw_meat",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# High Quality Beef
	["protein_quality_beef","Wagyu Beef", [("raw_meat",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(5)|max_ammo(50),imodbits_none],
	# Pork
	["protein_pork","Pork", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Tofu
	["protein_tofu","Tofu", [("smoked_fish",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Clams
	["protein_clam","Clams", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(12),imodbits_none],
	### Vegetables
	# Carrots
	["veggie_carrot","Carrots", [("cabbage",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Taro
	["veggie_taro","Raw Taro", [("cabbage",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Spinach
	["veggie_spinach","Spinach", [("cabbage",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Mixed Greens
	["veggie_mixed_green","Mixed Greens", [("cabbage",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Mustard Greens
	["veggie_mustard_green","Mustard Greens", [("cabbage",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Potatoes
	["veggie_potato","Potatoes", [("cabbage",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Leeks
	["veggie_leek","Leeks", [("cabbage",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Napa Cabbage
	["veggie_cabbage","Napa Cabbage", [("cabbage",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Bamboo Shoots
	["veggie_bamboo","Bamboo Shoots", [("cabbage",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(4),imodbits_none],
	# Onions
	["veggie_onion","Onions", [("cabbage",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(4),imodbits_none],
	### Starches and Carbs
	# Noodles
	["carb_noodles","Ramen Noodles", [("bread_a",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Cheap Instant Noodles
	["carb_cheap_noodles","Cheap Instant Noodles", [("bread_a",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# High-Quality Noodles
	["carb_quality_noodles","Quality Ramen Noodles", [("bread_a",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Rice
	["carb_rice","Rice", [("bread_a",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Bread
	["carb_bread_store","Bread", [("bread_a",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Breadcrumbs - Made by Player (either bread or homemade bread)
	["carb_breadcrumbs","Breadcrumbs", [("bread_a",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Tempura
	["carb_tempura","Tempura", [("bread_a",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Batter - Made by player (Flour, Eggs, Beer sometimes)
	["carb_batter","Batter", [("bread_a",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	### Spices and Condiments
	# Butter
	["ingredient_butter","Butter", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# Flour
	["ingredient_flour","Flour", [("salt_sack",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# Eggs
	["ingredient_eggs","Eggs", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(12),imodbits_none],
	# Salt, Pepper, Spices
	["ingredient_spicerack","Spice Rack", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(100),imodbits_none],
	# Cooking Sake
	["ingredient_sake","Cooking Sake", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(25),imodbits_none],
	# Ginger
	["ingredient_ginger","Ginger", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Mayonnaise - Can also be cooked, eggwhites and vinegar
	["ingredient_mayo","Mayonnaise", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# Soy Sauce
	["ingredient_soy","Soy Sauce", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(30),imodbits_none],
	# Olive Oil
	["ingredient_olive_oil", "Olive Oil", [("oil",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# vinegar
	["ingredient_vinegar", "Vinegar", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# milk
	["ingredient_milk", "Milk", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(30),imodbits_none],
	### Instant Items, still require cooking, but are the only ingredient
	# Instant Coffee
	["instant_coffee","Instant Coffee", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# CUP NOODLES, MOTHERFUCKER
	["instant_noodles","Cup Noodles (R)", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Instant Ramen
	["instant_ramen","Ramen Block", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Instant Ramen, Fancier
	["instant_quality_ramen","High-Quality Ramen Block", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Breakfast Cereal
	["instant_cereal","Box of Cereal", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# Oatmeal
	["instant_oatmeal","Oatmeal", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(20),imodbits_none],
	## Pre-Made, Either Restaurant or Convenient Stores
	# Spicy Mapo Tofu
	["mapo_tofu","Spicy Mapo Tofu", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Bento Box - Beef
	["bento_beef","Beef Bento Box", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Bento Box - Pork
	["bento_pork","Pork Bento Box", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Box Sushi
	["store_sushi","Sushi Bento", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Bento Box - Pork
	["steak","Beef Filete", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Bento Box - Pork
	["fried_chicken","Buttermilk Chicken", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Bento Box - Pork
	["veggie_medley","Vegetable Medley", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Sandwich - Eggs
	["egg_sandwich","Wrapped Egg Sandwich", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Sandwich - Turkey
	["turkey_sandwich","Wrapped Turkey Sandwich", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Ramune Soda
	["soda_ramune","Ramune Soda", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Coca Cola
	["soda_coke","Coca Cola", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Pepsi
	["soda_pepsi","Pepsi", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Canned coffee
	["canned_coffee","Canned Coffee", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Beer
	["alcohol_beer","Canned Beer", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Sake
	["alcohol_sake","Sake", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Whiskey, Japanese
	["alcohol_jap_whiskey","Japanese Whiskey", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# Whiskey, American
	["alcohol_usa_whiskey","American Whiskey", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# Whiskey, Scotch
	["alocohol_scotch","10 Year Scotch", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# Whiskey, Bourbon
	["alcohol_bourbon","Kentucky Bourbon", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# Latte
	["coffee_latte","Latte", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Cappuccino
	["coffee_cappuccino","Cappuccino", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Espresso
	["coffee_espresso","Espresso", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Drip Coffee
	["coffee_drip","Drip Coffee", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	## Cooked Instant Items
	# Instant Coffee
	["cooked_instant_coffee","Bitter Cup of Coffee", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# CUP NOODLES, MOTHERFUCKER
	["cooked_instant_noodles","Steaming Cup Noodles (R)", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Instant Ramen
	["cooked_instant_ramen","Instant Ramen", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Instant Ramen, Fancier
	["cooked_instant_quality_ramen","High-Quality Instant Ramen", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Breakfast Cereal
	["cooked_instant_cereal","Bowl of Breakfast Cereal", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Oatmeal
	["cooked_instant_oatmeal","Bowl of Instant Oatmeal", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	## Cooking System Items
	# Fancy Ramen - High Quality Beef, Shrimp, Pork, Chicken, Egg, Noodle
	["cooked_homemade_ramen","Homemade Ramen", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Salad with Protein - Chicken, Beef, Shrimp or Tuna, Green Leafy Veggie, Vinegar
	["cooked_salad","Fresh Salad", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Sushi - Salmon, Crab, Shrimp, Yellowfin or Blue Fin, Rice
	["cooked_sushi","Handmade Sushi", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Fried Rice - Rice, oil, carrots
	["cooked_fried_rice","Steaming Vegetable Fried Rice", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Fried Rice with Protein - rice and oil and carrots and Chicken, Beef, Pork, or Shrimp
	["cooked_meat_fried_rice","Steaming Fried Rice", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Steamed Rice - Rice and Butter
	["cooked_steamed_rice","Steamed Rice", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Steamed Rice - Rice and Butter and Chicken, Beef, Pork, or Shrimp and a veggie
	["cooked_meat_steamed_rice","Steamed Rice with Meat", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Homemade Bread - Flour, Eggs, Milk
	["cooked_bread","Homemade Bread", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Sandwich - Any protein except Squid, bread, and a green leaf veggie
	["cooked_sandwich","Sliced Sandwich", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Meatloaf - Beef, Breadcrumbs, Eggs
	["cooked_meatloaf","Meatloaf with Gravy", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Pancakes - Flour, Eggs, Milk
	["cooked_pancakes","Fluffy Pancakes", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Scrambled Eggs - Eggs
	["cooked_eggs","Scrambled Eggs", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Tempura Fried Proteins - Shrimp, Squid, Chicken
	["cooked_tempura","Tempura Fried Proteins", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Toshikoshi Soba - Episode 1
	["cooked_soba","Toshikoshi Soba", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Salmon and Mushrooms Baked In Foil - ep2
	["cooked_salmon_mushroom","Roasted Salmon with Mushrooms", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Spring Chirashizushi - ep 3
	["cooked_chirashizushi","Chirashizushi", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Spring Greens and Bacon Sandwich - ep 4
	["cooked_blt","Pressed Spring Green and Bacon Sandwich", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Bamboo Shoot Gratin -5 
	["cooked_bamboo_shoot","Tinder Bamboo Shoot Gratin", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Hamburg Steak- 6
	["cooked_humburger","German-style Hamburg Steak", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Refreshing and Easy to Eat Chilled Ochazuke - 7 
	["cooked_ochazuke","Crisp, Refreshing Ochazuke", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Tohsaka's Gomoku Fried Rice - 8
	["cooked_chinese_fried_rice","Chinese Style Fried Rice", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Taro Simmered in Broth - 9
	["cooked_taro","Taro Simmered in Broth", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Karaage, Fried Chicken - 10 # Basically Chicken Nuggeys
	["cooked_fried_chicken","Karaage Fried Chicken", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# The Specially Fluffy Omurice - 11
	["cooked_omurice","Fluffy, Omurice with Tomato Sauce", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Roast Beef Made Only With Frying Pan - 12
	["cooked_roat_beef","Single-pan Roast Beef", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Hot, Hot, Hot Pot - 13
	["cooked_hot_pot","Familiar Hot Pot", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	
	#["test_head", "Skull Mask", [("horrifying",0)], itp_type_head_armor|itp_unique|itp_covers_head|itp_doesnt_cover_hair, 0, 193, weight(2)|abundance(100)|head_armor(54)|body_armor(5)|leg_armor(0), imodbits_cloth],
	
	["items_end", "Items End", [("shield_round_a",0)], 0, 0, 1, 0, 0],
]
