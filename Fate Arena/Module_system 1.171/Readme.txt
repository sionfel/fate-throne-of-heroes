
   _____                     _      _                _  
  / ____|                   | |    | |              | | 
 | (___    ___  _ __  _   _ | |__  | |__    ___   __| | 
  \___ \  / __|| '__|| | | || '_ \ | '_ \  / _ \ / _` | 
  ____) || (__ | |   | |_| || |_) || |_) ||  __/| (_| | 
 |_____/  \___||_|    \__,_||_.__/ |_.__/  \___| \__,_| 
  __  __             _         _       
 |  \/  |           | |       | |      
 | \  / |  ___    __| | _   _ | |  ___ 
 | |\/| | / _ \  / _` || | | || | / _ \
 | |  | || (_) || (_| || |_| || ||  __/
 |_|  |_| \___/  \__,_| \__,_||_| \___|
   _____              _                     __    __  ______  __                              
  / ____|            | |                   /_ |  /_ ||____  |/_ |                             
 | (___   _   _  ___ | |_  ___  _ __ ___    | |   | |    / /  | |                             
  \___ \ | | | |/ __|| __|/ _ \| '_ ` _ \   | |   | |   / /   | |                             
  ____) || |_| |\__ \| |_|  __/| | | | | |  | | _ | |  / /    | |                             
 |_____/  \__, ||___/ \__|\___||_| |_| |_|  |_|(_)|_| /_/     |_|                             
           __/ |                                                                              
          |___/                                                                               


Originally created for 1.134 by the incredibly patient and legendary Theoris.

Also included is the cleaned, reorganized and heavily commented header_operations made by the man, the myth, the legend Lav!

So what did you do, dstn/Sionfel/SupaNinjaMan? Well I:
	Included a scrubbed blank Native module so that your load times are lickity-split
	Re added the bits left from TW to tell you how to use the MS
	Did this Readme. . . . 
	. . .
	I guess I went through and added proper spacing to the Native declarations since the inconsistency murders my eyes.
	. . .
	. . .
	I probably did something else. Don't look at me.
	
# ##### Original Readme.txt included in 1.171 ################################### #

Mount&Blade Module System

For getting documentation and the latest version of the module system check out:

www.taleworlds.com/mb_module_system.html

# ################################################################################ #
ASCII Art was generated on http://patorjk.com/software/taag/ using
Big by Glenn Chappell 4/93 -- based on Standard
Includes ISO Latin-1
Greek characters by Bruce Jakeway <pbjakeway@neumann.uwaterloo.ca>
figlet release 2.2 -- November 1996
Permission is hereby given to modify this font, as long as the
modifier's name is placed on a comment line.

Modified by Paul Burton  12/96 to include new parameter
supported by FIGlet and FIGWin.  May also be slightly modified for better use
of new full-width/kern/smush alternatives, but default output is NOT changed.