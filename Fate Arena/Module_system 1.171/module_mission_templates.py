from header_common import *
from header_operations import *
from header_mission_templates import *
from header_animations import *
from header_sounds import *
from header_music import *
from header_items import *
from module_constants import *
from ID_scenes import *

# #######################################################################
#   Each mission-template is a tuple that contains the following fields:
#  1) Mission-template id (string): used for referencing mission-templates in other files.
#     The prefix mt_ is automatically added before each mission-template id
#
#  2) Mission-template flags (int): See header_mission-templates.py for a list of available flags
#  3) Mission-type(int): Which mission types this mission template matches.
#     For mission-types to be used with the default party-meeting system,
#     this should be 'charge' or 'charge_with_ally' otherwise must be -1.
#     
#  4) Mission description text (string).
#  5) List of spawn records (list): Each spawn record is a tuple that contains the following fields:
#    5.1) entry-no: Troops spawned from this spawn record will use this entry
#    5.2) spawn flags.
#    5.3) alter flags. which equipment will be overriden
#    5.4) ai flags.
#    5.5) Number of troops to spawn.
#    5.6) list of equipment to add to troops spawned from here (maximum 8).
#  6) List of triggers (list).
#     See module_triggers.py for infomation about triggers.
# #######################################################################


common_inventory_not_available = (
  ti_inventory_key_pressed, 0, 0,
  [
    (display_message, "str_cant_use_inventory_now"),
    ], [])

############################################################################
###### Fate/ Rider Saber Battle Scripts ####################################
############################################################################
# #common_fate_triggers = (
	# 10, 0, 0, [],
			 # [
			 #(try_for_prop_instances, ":props", [spr_fate_yew_tree]),
			 
				# # #I'mma go through and add a check for damaged avoidance skills
				# # #and then changes incoming damage based on that
				 # (store_trigger_param_1, ":agent"),
				 # (str_store_agent_name, s10, ":agent"),
				 # (store_trigger_param_2, ":attacker"),
				 # (str_store_agent_name, s11, ":attacker"),
				 # (store_trigger_param_3, ":damage"),
				 # (assign, reg13, ":damage"),
				 # (agent_get_troop_id, ":troop_type", ":agent"),
				 # (troop_get_slot, s13, ":troop_type", slot_fate_classification),
				# # (item_get_type, ":item", reg0),
				# # (str_store_string, s15, ":item"),
				 
				 # (try_begin),
					# (troop_slot_eq, ":troop_type", slot_fate_classification, "str_classification_servant"),
					# #(eg, s13, "str_classification_servant"),
					# (display_log_message, "@DEBUG: Servant {s10} was hit for {reg13} damage by {s11}!"),
				# #	(display_log_message, "@DEBUG: and was hit by a {s15}"),
					# (try_begin),
						# # fate_skill_slot_05
						# (troop_slot_eq, ":troop_type", fate_skill_slot_05, "str_skill_protection_from_arrows"),
						# (display_log_message, "@DEBUG: {s10} has PROTECTION FROM ARROWS"),
						# #(try_begin),
							# #test what incoming damage is from a ranged weapon and go from teere
						# #(try_end),
				 # (try_end),
				 # (else_try),
					# (display_log_message, "@DEBUG: {s10} was hit for {reg13} damage by {s11}!"),
				# #	(display_log_message, "@DEBUG: and was hit by a {s15}"),
				 # (try_end),
				 
				 
			 # ]) 
double_jump = (
     0, 0, 0, [(neg|is_presentation_active, "prsnt_fate_rts_camera")],
    [
	(set_fixed_point_multiplier, 100),
	
	(get_player_agent_no, ":agent"), 		# Who is the player
	(gt, ":agent", 0),
	
	(agent_is_alive, ":agent"),
	(agent_get_position, pos0, ":agent"), 	# Where is the player
	(agent_get_animation, ":animation", ":agent", 0), # What are their legs doing?
	(agent_get_slot, ":double_jump", ":agent", slot_agent_is_double_jumping), # Are they double-jump?
	
	(agent_get_troop_id, ":troop", ":agent"), # Get your troop id
	(troop_get_slot, ":class", ":troop", slot_fate_classification), # Get their class
	(agent_get_slot, ":enhanced_legs", ":agent", slot_agent_has_enhanced_legs), # can they double-jump?
	
	(agent_get_speed, pos2, ":agent"),		# Get the agent's speed
	(position_get_y, ":forward_momentum", pos2), # What is their speed in decimeters/sec on their local Y (-Y = backwards, +Y = forwards)
	(val_div, ":forward_momentum", 100),			# To shrink the momentum to not launch the agent super quickly
	(position_get_x, ":sideway_momentum", pos2), # What is their speed in decimeters/sec on their local Y (-Y = backwards, +Y = forwards)
	(val_div, ":sideway_momentum", 100),			# To shrink the momentum to not launch the agent super quickly
	# (val_add, ":forward_momentum", 2),	# Set this to one half the z movement to add a longer jump


	(try_begin), 
		(key_clicked, key_space), 			# If the player pressed space this frame
		(this_or_next|eq, ":animation", "anim_jump"), # And they were already jumping
		(eq, ":animation", "anim_jump_loop"), # or in middle of a jump
		
		(this_or_next|eq, ":class", "str_classification_servant"),	# Servants can double jump
		(ge, ":enhanced_legs", 2),									# Can double jump
		
		(val_add, ":double_jump", 50), 		  # storing the amount of time, in centiseconds, you want to add motion to.
		(agent_set_animation, ":agent", "anim_double_jump", 0), # And change their animation to the double_jump
	(else_try),
		(key_clicked, key_space),
		(neq, ":animation", "anim_jump"), # And they were already jumping
		(neq, ":animation", "anim_jump_loop"), # or in middle of a jump
		(neq, ":animation", "anim_double_jump_loop"), # or in middle of a jump
		(neq, ":animation", "anim_double_jump"), # or in middle of a jump
		
		(this_or_next|eq, ":class", "str_classification_servant"),	# Servants can double jump
		(this_or_next|eq, ":enhanced_legs", 1),						# Can enhanced or double jump (3 is just a double jump)
		(eq, ":enhanced_legs", 3),
		
		(val_add, ":double_jump", 10),
	(try_end),


	(try_begin), 
		(ge, ":double_jump", 1),
		(agent_set_no_dynamics, ":agent", 1),  # Turning dynamics off allows us to basically no_clip
		(try_begin),
			(gt, ":double_jump", 10),
			(position_move_y, pos0, ":forward_momentum"), # Activating this will make a longer jump arc, I recommend 1/2 the Z
			(position_move_x, pos0, ":sideway_momentum"),
		(try_end),
		(position_move_z, pos0, 10),			# Setting this to 2 makes an IDENTICAL jumping arc to the original! 
		(agent_set_position, ":agent", pos0),	# Move them to the new position
		(agent_set_no_dynamics, ":agent", 0), #Dynamics back on
		(val_sub, ":double_jump", 1),		# subtract a centisecond from remaining upwards movement time
	(try_end),
    (agent_set_slot, ":agent", slot_agent_is_double_jumping, ":double_jump"), # Store the double_jump value

    ])
	
common_fate_damage_system = (
 ti_on_agent_hit, 0, 0, [],
[
	(set_fixed_point_multiplier, 100),
	(store_trigger_param_1, ":agent"),
	(store_trigger_param_2, ":attacker"),	
	(store_trigger_param_3, ":damage"),
	# (assign, ":original_damage", ":damage"),
	(store_trigger_param, ":bodypart", 4),
	(store_trigger_param, ":missile", 5),
	# (assign, reg11, ":damage"),
	(agent_get_troop_id, ":troop", ":agent"),
	(agent_get_troop_id, ":attacker_troop", ":attacker"),
	# (agent_get_wielded_item, ":attacker_weapon", ":attacker", 0),
	(assign, ":attacker_weapon", reg0),
	# (item_get_type, ":attacker_weapon_type", ":attacker_weapon"),
	 (str_store_troop_name, s10, ":troop"),
	 (str_store_troop_name, s11, ":attacker_troop"),
	# (store_agent_hit_points, ":health_remaining_percent", ":agent", 0),
	(agent_get_slot, ":armorlevel", ":agent", slot_agent_armor_level),
	(agent_get_slot, ":magicres", ":agent", slot_agent_magic_resist),
	
	(agent_is_alive, ":agent"),
	(agent_is_human, ":agent"),
	
	(store_agent_hit_points, ":health_remaining_actual", ":agent", 1),
	
	(store_troop_health, ":current_health", ":troop"),
	(troop_set_health, ":troop", 100),
	(store_troop_health, ":max_health_actual", ":troop", 1),
	(troop_set_health, ":troop", ":current_health"),
	
	(agent_get_slot, ":i_frames", ":agent", slot_agent_invulnerability_timer),
	
	(try_begin),
		(gt, ":attacker_weapon", 0),
		#(val_div, ":damage", 2),
	(else_try),
		(val_add, ":damage", 10),
	(try_end),
	
	(try_begin),
		(eq, ":attacker_troop", "trp_saber_02"),
		
		(scene_prop_get_num_instances, ":i", "spr_aestus"),
		(val_sub, ":i", 1),
		(scene_prop_get_instance, ":instance", "spr_aestus", ":i"),
		
		(prop_instance_get_position, pos20, ":instance"),
		(get_distance_between_positions_in_meters, ":dist", pos20, pos0),
		
		(lt, ":dist", 20),
		
		#(store_div, ":burst_strength", ":damage", 2),
		#(particle_system_burst, "psys_aestus_rose", pos0, ":burst_strength"),
		(particle_system_burst, "psys_aestus_rose", pos0, ":damage"),
	(try_end),
	
	
	# # New Damage Formula:
	# # Magic Damage:
	# # If Mage's Magecraft skill is < Magic Resistance, nullify all damage
	# # else (Spell Damage + Enhanced) - (Magic Resistance + Magical Defense)
	# #
	# # Magic Resistance is Magic + 0.25 * Endurance
	# #
	# #
	# # Weapon Damage:
	# # If Weapon or Skill Rating < Physical, nullify damage
	# # else (Weapon Rating + Skill Rating) / 2 + 0.5 * Strength + 0.25 * Agility
	# # - (Armor Rating + 0.5 * Endurance + 0.25 * Agility) and 5 * Luck % Chance to Negate
	# # 
	# #
	# # Locational Damage (Over Threshold)
	# # Head: Stun
	# # Chest: Fall Over
	# # Left Arm:
	# # Right Arm: Slower Attack Speed
	# # Foot: Lower Agent Speed by 10% of damage, Stagger
	
	# additional charge damage
	(try_begin),
		(eq, ":missile", -1),
		(gt, ":attacker_weapon", 0),
		(agent_get_slot, ":charge_time", ":attacker", slot_agent_active_charge),
		(convert_from_fixed_point, ":charge_time"),
		(val_mul, ":charge_time", 10),
		(val_min, ":charge_time", 100),
		(assign, reg50, ":charge_time"),
		(gt, ":charge_time", 1),
		(val_add, ":damage", ":charge_time"),
		(agent_set_slot, ":attacker", slot_agent_active_charge, 0),
	(try_end),
	
	(try_begin),
		(eq, ":attacker", ":agent"),
		(is_between, ":bodypart", 1, 4),
		(assign, ":damage", 0),
	(try_end),
	
	(try_begin),
		(is_between, ":troop", servants_begin, servants_end), # Is the unit being hit a Servant?
		(try_begin),
			(neg|is_between, ":attacker_troop", servants_begin, servants_end), # and is agent hitting not a servant?		
			(val_div, ":damage", 10), # puts damage at 10%
			# (assign, reg10, ":damage"),
			# (display_message, "@{s10} was hit by {s11} for {reg11} but has instead taken {reg10} damage instead!"),
			# (assign, reg15, ":health_remaining_actual"),
			# (val_sub, ":health_remaining_actual", ":damage"),
			# (assign, reg16, ":health_remaining_actual"),
			# (display_message, "@{s10} health before hit: {reg15}, after {reg16}"),
			# (eq, ":attacker_troop", "trp_player"),
		(else_try),
			(is_between, ":attacker_troop", servants_begin, servants_end), # and is agent hitting a servant?
		(try_end),
	(try_end),
	
	
	# THIS IS THE ARROW BLOCKING SCRIPT
	(try_begin),
		(neq, ":missile", -1),
		
		(try_begin),
			(position_get_z, ":hit_height", pos0),
			(position_get_x, ":hit_horizontal", pos0),
			 
			(agent_get_position, pos1, ":agent"),
			(position_get_z, ":hit_agent_vertical", pos1),
			(position_get_x, ":hit_agent_horizontal", pos1),
			 
			(store_sub, ":local_hit_vertical", ":hit_height", ":hit_agent_vertical"),
			(store_sub, ":local_hit_horizontal", ":hit_horizontal", ":hit_agent_horizontal"),
			 
			(assign, reg15, ":local_hit_horizontal"),
			(assign, reg16, ":local_hit_vertical"),
			  
			(agent_get_defend_action, ":is_blocking", ":agent"), # These were off during initial testing 
			(eq, ":is_blocking", 2), # These were off during initial testing 
			(agent_get_action_dir, ":block_dir", ":agent"), #down = 0, right = 1, left = 2, up = 3
					
			(try_begin),
					(eq, ":block_dir", 3),
					(ge, ":local_hit_vertical", 100),
					(agent_set_animation, ":agent", "anim_defend_up_twohanded_parry_1", 1),
					(display_message, "@You blocked a missile!", 0x339933),
					(play_sound_at_position, "snd_shield_hit_metal_metal", pos1, sf_vol_12),
					(assign, ":damage", 0),
				(else_try),
					(eq, ":block_dir", 0),
					(is_between, ":local_hit_vertical", 40, 110),
					(agent_set_animation, ":agent", "anim_defend_left_twohanded_parry_2", 1),
					(display_message, "@You blocked a missile!"),
					(play_sound_at_position, "snd_shield_hit_metal_metal", pos1, sf_vol_12),
					(assign, ":damage", 0),
			(try_end),
		(try_end),
		
		(eq, ":troop", "trp_lancer_01"),
		(try_begin),
			(is_between, ":missile", "itm_gandr", "itm_book_celtic_legends"),
		(else_try),
			(neq, ":damage", 0),
			(assign, ":damage", 0),
		(try_end),
	(try_end),
	
	# Limb Damage System
	
	(try_begin),
		(gt, ":attacker_weapon", 0),
		(item_get_swing_damage_type, ":damage_type", ":attacker_weapon"),
	(else_try),
		(assign, ":damage_type", 0),
	(try_end),
	
	(try_begin),
		(eq, ":damage_type", 0),	# Cut
		(assign, ":dampen", 15),
	(else_try),
		(eq, ":damage_type", 1),	# Pierce
		(assign, ":dampen", 10),
	(else_try),
		(eq, ":damage_type", 2),	# Blunt
		(assign, ":dampen", 5),
	(try_end),
	
	(store_div, ":injury", ":damage", ":dampen"),

	(try_begin),
		(gt, ":attacker_weapon", 0),
		(item_slot_ge, ":attacker_weapon", fate_weapon_activated, 1),
		(eq, ":attacker_weapon", "itm_gae_buidhe"),
		(val_mul, ":injury", 5),
	(try_end),
	
	(try_begin),
		(gt, ":attacker_weapon", 0),
		#(item_slot_ge, ":attacker_weapon", fate_weapon_activated, 1),
		(eq, ":attacker_weapon", "itm_gae_bolg_thrown"),
		(assign, ":injury", 100),
	(try_end),
	
	(agent_get_slot, ":torso_injury", ":agent", fate_agent_wounds_chest),
	(agent_get_slot, ":head_injury", ":agent", fate_agent_wounds_head),
	(agent_get_slot, ":lleg_injury", ":agent", fate_agent_wounds_lleg),
	(agent_get_slot, ":rleg_injury", ":agent", fate_agent_wounds_rleg),
	(agent_get_slot, ":larm_injury", ":agent", fate_agent_wounds_larm),
	(agent_get_slot, ":rarm_injury", ":agent", fate_agent_wounds_rarm),
	
	(agent_get_wielded_item, ":left_item", ":agent", 1),
	(agent_get_wielded_item, ":right_item", ":agent", 0),
	
	(position_set_z_to_ground_level, pos31),
	(set_spawn_position, pos31),
	
	(try_begin),
		(gt, ":right_item", 0),
		(item_get_type, ":right_item_type", ":right_item"),
	(try_end),

	(try_begin),
		(this_or_next|eq, ":bodypart", 0), # Guts
		(is_between, ":bodypart", 7, 9), # Spine and Thorax
		(val_add, ":injury", ":torso_injury"),
		
		(val_clamp, ":injury", 0, 100),
		(agent_set_slot, ":agent", fate_agent_wounds_chest, ":injury"),
	(else_try),
		(is_between, ":bodypart", 1, 4), # Left Leg
		(val_add, ":injury", ":lleg_injury"),
		
		(val_clamp, ":injury", 0, 100),
		(agent_set_slot, ":agent", fate_agent_wounds_lleg, ":injury"),
	(else_try),	
		(is_between, ":bodypart", 4, 7), # Right Leg
		(val_add, ":injury", ":rleg_injury"),
		
		(val_clamp, ":injury", 0, 100),
		(agent_set_slot, ":agent", fate_agent_wounds_rleg, ":injury"),
	(else_try),
		(eq, ":bodypart", 9), # head
		(val_add, ":injury", ":head_injury"),
		
		(val_clamp, ":injury", 0, 100),
		(agent_set_slot, ":agent", fate_agent_wounds_head, ":injury"),
	(else_try),
		(is_between, ":bodypart", 10, 14), # Left arm
		(val_add, ":injury", ":larm_injury"),
		
		(val_clamp, ":injury", 0, 100),
		(agent_set_slot, ":agent", fate_agent_wounds_larm, ":injury"),
		
		(try_begin),
			(gt, ":injury", 95),
			
			(try_begin),
				(gt, ":left_item", 0),
				(agent_unequip_item, ":agent", ":left_item"),
				(spawn_item, ":left_item", 0, 1000),
			(try_end),
			
			(try_begin),
				(gt, ":right_item", 0),
				(eq, ":left_item", -1),
				(this_or_next|eq, ":right_item_type", itp_type_two_handed_wpn),
				(this_or_next|eq, ":right_item_type", itp_type_bow),
				(eq, ":right_item_type", itp_type_polearm),
				(agent_unequip_item, ":agent", ":right_item"),
				(spawn_item, ":right_item", 0, 1000),
			(try_end),
		(try_end),
	(else_try),
		(is_between, ":bodypart", 15, 19), #Right arm
		(val_add, ":injury", ":rarm_injury"),
		
		(val_clamp, ":injury", 0, 100),
		(agent_set_slot, ":agent", fate_agent_wounds_rarm, ":injury"),
		
		(try_begin),
			(gt, ":injury", 95),
			(gt, ":right_item", 0),
			(agent_unequip_item, ":agent", ":right_item"),
			(spawn_item, ":right_item", 0, 1000),
		(try_end),
	(try_end),
	
	# # Apply Damage Resistance, Is it Magic?
	(try_begin),
		(is_between, ":missile", "itm_gandr", "itm_poison"),
		(val_div, ":damage", ":magicres"),
	(else_try),
		(eq, ":attacker_weapon", "itm_np_damage"),
		# I haven't decided how this one will work
	(else_try),
		(val_div, ":damage", ":armorlevel"),
	(try_end),
	
	(try_begin),
		(gt, ":damage", ":health_remaining_actual"),
		(troop_get_slot, ":battle_continuation", ":troop", slot_fate_cur_battle_continuations),
		(gt, ":battle_continuation", 1),									# Agent has some BC charge left
		
		(val_sub, ":damage", ":health_remaining_actual"),					# Check Rollover Damage
		(val_sub, ":battle_continuation", ":damage"),						# Subtract it from the pool of total BC Charge
		
		(assign, ":heal", ":battle_continuation"),
		
		(gt, ":heal", 1),
		
		(agent_set_slot, ":agent", slot_fate_can_ressurect, 1),				# Mark that this boy can come back
		
		(val_min, ":heal", ":max_health_actual"),							# Whichever is less, use that to heal the guy
		
		(agent_set_hit_points, ":agent", ":heal", 1),						# Set hitpoints to w/e was lesd
		(val_sub, ":battle_continuation", ":heal"), 						# Subtract however much was used to heal him from the total pool
		
		(troop_set_slot, ":troop", slot_fate_cur_battle_continuations, ":battle_continuation"), # Store the BC - (Damage + total HP used in resserection)
		
		(store_random_in_range, ":respawn", 3, 10),
		(val_mul, ":respawn", 100),
		
		(agent_set_slot, ":agent", slot_fate_death_time, ":respawn"),		# Set respawn time to 5 seconds
		(agent_set_animation, ":agent", "anim_fall_chest_front"),			# Feign death
		(agent_set_no_dynamics, ":agent", 1),								# disable animation
		(agent_set_is_alarmed, ":agent", 0),
		# (assign, ":spell_cost", 200),
		(agent_get_team, ":team", ":agent"),								# check this agent's team
		
		(try_for_agents, ":enemy_agent"),							# run through everyone in scene
			(agent_is_alive, ":enemy_agent"),							
			(agent_get_team, ":enemy_team", ":enemy_agent"),				# what's their team
			(teams_are_enemies, ":enemy_team", ":team"),					# are they your enemy?
			(agent_add_relation_with_agent, ":enemy_agent", ":agent", 0),	# not anymore!
			(agent_add_relation_with_agent, ":agent", ":enemy_agent", 0),
		(try_end),
		
		(assign, reg30, ":battle_continuation"),
		(assign, reg60, ":damage"),
		(get_player_agent_no, ":player"),
		
		(try_begin),
			(eq, ":attacker", ":player"),
			(display_message, "str_delivered_damage"),
			(display_message, "@{s10} killed by {s11}", 0x44ddaa),
			(set_show_messages, 0),
			(assign, "$g_fate_messages_disabled", 1),
		(else_try),
			# (display_message, "@I have {reg30} BC Charge remaining!", 0x666633),
		(try_end),
		
		(val_mul, ":damage", 0),											# do not actually damage the agent. THEY ARE DED!
	(try_end),
	
	(try_begin),
		#(get_player_agent_no, ":player"),
		#(eq, ":agent", ":player"),
		(this_or_next|gt, "$g_fate_battle_god_mode", 0),
		(gt, ":i_frames", 0),
		(assign, ":damage", 0),
	(try_end),
	
	(try_begin),
		(eq, ":attacker_weapon", "itm_cursedarm"),
		(assign, ":damage", 10000),
	(try_end),
	
	(set_trigger_result, ":damage"),
])

fate_magic_casts = (
	 0.0, 0, 0, [
	 (key_clicked, key_middle_mouse_button),
	 (neg|is_presentation_active, "prsnt_fate_rts_camera"),
	 ],
	 [		
			(set_fixed_point_multiplier, 100),
			(get_player_agent_no, ":player_agent"),
			(gt, ":player_agent", 0),
			(agent_is_alive, ":player_agent"),
			
			(get_player_agent_no, ":player_agent"),
			
			(agent_get_troop_id, ":troop", ":player_agent"),
			
			(assign, ":can_use_magic", 0),
			
			(troop_get_slot, ":class", ":troop", slot_fate_classification), # Get their classification
			
			(try_begin),
				(eq, ":class", "str_classification_magus"),
				(assign, ":can_use_magic", 1),
			(else_try),
				(eq, ":class", "str_classification_master"),
				(assign, ":can_use_magic", 1),
			(else_try),
				(eq, ":class", "str_classification_servant"),
				(troop_get_slot, ":servant_class", ":troop", slot_fate_servant_class), # Get their servant class
				(try_begin),
					(eq, ":servant_class", "str_class_caster"),
					(assign, ":can_use_magic", 1),
				(else_try),
					(try_for_range, ":skill_slots", fate_skill_slot, fate_np_slot_01),
						(troop_get_slot, ":contents", ":troop", ":skill_slots"),
						(this_or_next|eq, ":contents", "str_skill_magecraft"),
						(this_or_next|eq, ":contents", "str_skill_rune_magic"),
						(this_or_next|eq, ":contents", "str_skill_primordial_rune"),
						(eq, ":contents", "str_skill_numerology"),
						(assign, ":can_use_magic", 1),
						
					(try_end),
				(try_end),
			(try_end),
			
			(try_begin),
				(neq, ":can_use_magic", 1),
				(display_message, "@This troop cannot cast magic!", 0xFF2222),
			(try_end),
			
			(eq, ":can_use_magic", 1),
			
			(agent_set_animation, ":player_agent", "anim_ready_pistol", 1),
			(call_script, "script_magic_cast", ":player_agent"),
	 ]
 )
 
fate_servant_skill_select = (
	 0.0, 0, 0, [
	 (key_clicked, key_b),
	 # ],
	 # [
	 
	# (get_player_agent_no, ":agent"),
	# (agent_is_alive, ":agent"),
	# (agent_is_human, ":agent"),
	# (agent_get_position, pos30, ":agent"),
	
	# # (try_for_agents, ":enemy_agent", pos30, 10000),
		# # (neq, ":agent", ":enemy_agent"),
		# # (agent_is_alive, ":enemy_agent"),
		# # (agent_is_human, ":enemy_agent"),
		# # (agent_is_non_player, ":enemy_agent"),
			
		# # (call_script, "script_offscreen_volley", ":agent", ":enemy_agent", 500),
	# # (try_end),
	
	# # ]
		
	 (neg|is_presentation_active, "prsnt_noble_phantasm_selection"),
	 (neg|is_presentation_active, "prsnt_fate_rts_camera"),
	 (neg|is_presentation_active, "prsnt_fate_magic_select"),
	 (neg|is_presentation_active, "prsnt_servant_skill_selection"),
	 (try_begin),
		(neq, "$g_fate_battle_servants_on", 1),
		(display_message, "@Buddy, there aren't servants to use their skills", 0xdddd00),
	 (try_end),
	 (eq, "$g_fate_battle_servants_on", 1),
	 ],
	 [	

	  (try_for_range, ":key", key_1, key_a),
		(omit_key_once, ":key"),
	  (try_end),
	  
	  (try_for_range, ":key", key_numpad_0, key_num_lock),
		(omit_key_once, ":key"),
	  (try_end),
	  
		(start_presentation, "prsnt_servant_skill_selection"),
	 ]
 )
 
fate_magic_select = (
	 0.0, 0, 0, [
	 (key_clicked, key_m),
	 (neg|is_presentation_active, "prsnt_noble_phantasm_selection"),
	 (neg|is_presentation_active, "prsnt_fate_magic_select"),
	 (neg|is_presentation_active, "prsnt_servant_skill_selection"),
	 ],
	 [
		(get_player_agent_no, ":player_agent"),
		(gt, ":player_agent", 0),
		(agent_is_alive, ":player_agent"),
		
		(store_agent_hit_points, ":hp", ":player_agent"),
		
		(agent_get_troop_id, ":troop", ":player_agent"),
		
		(assign, ":can_use_magic", 0),
		
		(troop_get_slot, ":class", ":troop", slot_fate_classification), # Get their classification
		
		(try_begin),
			(eq, ":class", "str_classification_magus"),
			(assign, ":can_use_magic", 1),
		(else_try),
			(eq, ":class", "str_classification_master"),
			(assign, ":can_use_magic", 1),
		(else_try),
			(eq, ":class", "str_classification_servant"),
			(troop_get_slot, ":servant_class", ":troop", slot_fate_servant_class), # Get their servant class
			(try_begin),
				(eq, ":servant_class", "str_class_caster"),
				(assign, ":can_use_magic", 1),
			(else_try),
				(try_for_range, ":skill_slots", fate_skill_slot, fate_np_slot_01),
					(troop_get_slot, ":contents", ":troop", ":skill_slots"),
					(this_or_next|eq, ":contents", "str_skill_magecraft"),
					(this_or_next|eq, ":contents", "str_skill_rune_magic"),
					(this_or_next|eq, ":contents", "str_skill_primordial_rune"),
					(eq, ":contents", "str_skill_numerology"),
					(assign, ":can_use_magic", 1),
					
				(try_end),
			(try_end),
		(try_end),
		
		(try_begin),
			(neq, ":can_use_magic", 1),
			(display_message, "@This troop cannot cast magic!", 0xFF2222),
		(try_end),
		
		(eq, ":can_use_magic", 1),
		(agent_set_animation, ":player_agent", "anim_ready_pistol", 1),
		
		(try_begin),
			(eq, ":troop", "trp_archer_01"),
			
			(agent_get_wielded_item, ":right", ":player_agent", 0),
			(try_begin),
				(this_or_next|eq, ":right", "itm_kanshou_melee"),
				(this_or_next|neq, ":right", "itm_emiya_bow"),
				(eq, ":right", "itm_kanshou"),
				(troop_set_slot, ":troop", slot_fate_spell_slot_1, "str_magecraft_projection_trace"),
				(troop_set_slot, ":troop", slot_fate_spell_slot_2, "str_magecraft_reinforcement_strengthen"),
				(troop_set_slot, ":troop", slot_fate_spell_slot_2_target_modifier, target_weapon),
				(troop_set_slot, ":troop", slot_fate_spell_slot_3, "str_special_emiya_projection_rho_aias"),

				(try_begin),
					(gt, ":hp", 25),
					(troop_set_slot, ":troop", slot_fate_spell_slot_4, "str_magecraft_reinforcement_enhance"),
					(troop_set_slot, ":troop", slot_fate_spell_slot_4_target_modifier, target_eyes),
				(else_try),
					(troop_set_slot, ":troop", slot_fate_spell_slot_4, "str_special_emiya_projection_caliburn"),
					(troop_set_slot, ":troop", slot_fate_spell_slot_4_target_modifier, 0),
				(try_end),
				
				(troop_set_slot, ":troop", fate_magic_projection_item, "itm_emiya_bow"),
			(else_try),
				(eq, ":right", "itm_emiya_bow"),
				(troop_set_slot, ":troop", slot_fate_spell_slot_1, "str_magecraft_projection_trace"),
				(troop_set_slot, ":troop", slot_fate_spell_slot_2, "str_special_emiya_projection_hrunting"),
				(troop_set_slot, ":troop", slot_fate_spell_slot_2_target_modifier, 0),
				(troop_set_slot, ":troop", slot_fate_spell_slot_3, "str_special_emiya_projection_caladbolg"),
				(troop_set_slot, ":troop", slot_fate_spell_slot_3_target_modifier, 0),
				(troop_set_slot, ":troop", slot_fate_spell_slot_4, "str_magecraft_reinforcement_enhance"),
				(troop_set_slot, ":troop", slot_fate_spell_slot_4_target_modifier, target_eyes),
				(troop_set_slot, ":troop", fate_magic_projection_item, "itm_kanshou"),
			(try_end),
		(try_end),
		
	  (try_for_range, ":key", key_1, key_a),
		(omit_key_once, ":key"),
	  (try_end),
	  
	  (try_for_range, ":key", key_numpad_0, key_num_lock),
		(omit_key_once, ":key"),
	  (try_end),
		
		(start_presentation, "prsnt_fate_magic_select"),
	 ]
 )
 
fate_nested_menus = (
	 0.0, 0, 0, [
	 (key_clicked, key_q),
	 (neg|is_presentation_active, "prsnt_noble_phantasm_selection"),
	 (neg|is_presentation_active, "prsnt_fate_magic_select"),
	 (neg|is_presentation_active, "prsnt_servant_skill_selection"),
	 (neg|is_presentation_active, "prsnt_nested_menus"),
	 ],
	 [
	  (get_player_agent_no, ":player_agent"),
	  (gt, ":player_agent", 0),
	  (agent_is_alive, ":player_agent"),
		
	  (try_for_range, ":key", key_1, key_a),
		(omit_key_once, ":key"),
	  (try_end),
	  
	  (try_for_range, ":key", key_numpad_0, key_num_lock),
		(omit_key_once, ":key"),
	  (try_end),
		
		(start_presentation, "prsnt_nested_menus"),
	 ]
 )
 
fate_elemental_visualization = (
	 0, 0, 0, [],
	 [
		# Basically, every second fire a burst the length of the weapon in the color of their damage.
		# Seems simple enough.
		(set_fixed_point_multiplier, 100),
		(try_for_agents, ":agent"),
			(ge, ":agent", 0),
			(agent_is_alive, ":agent"),
			(agent_is_active, ":agent"),
			(agent_is_human, ":agent"),
			# (neg|agent_has_item_equipped, ":agent", -1),
			#(agent_get_position, pos0, ":agent"),
			(agent_get_troop_id, ":agent_troop", ":agent"),
						
			(this_or_next|is_between, ":agent_troop", fate_troops_begin, fate_troops_end),
			(eq, ":agent_troop", "trp_player"),
			
			(agent_get_bone_position, pos0, ":agent", 19, 1),
			(agent_get_wielded_item, ":equipped_item", ":agent", 0),
			
			
			(neq, ":equipped_item", -1),
			(item_get_type, ":item_type", ":equipped_item"),
			
			(item_get_weapon_length, ":item_length", ":equipped_item"),
			(position_move_y, pos0, ":item_length"),
			
			
			(neq, ":item_type", itp_type_bow),
			(neq, ":item_type", itp_type_crossbow),
			# (neq, ":item_type", itp_type_thrown),
			# (neq, ":item_type", itp_type_arrows),
			(neq, ":item_type", itp_type_bolts),
			
			(item_get_slot, ":enhancement", ":equipped_item", fate_weapon_enhancement),
			(item_get_slot, ":activated", ":equipped_item", fate_weapon_activated),
			(item_get_slot, ":element", ":equipped_item", fate_weapon_element),
			
			(try_begin),
				(gt, ":enhancement", 0),
				(assign, ":element", ":enhancement"),
			(try_end),
			
			(try_begin),
				(gt, ":activated", 0),
				
				(try_begin),
					(eq, ":equipped_item", "itm_mac_an_luin"),
					(add_missile, ":agent", pos0, 10000, "itm_astral_arrows", 0, "itm_astral_arrows", 0),
				(else_try),
					(eq, ":equipped_item", "itm_gae_buidhe"),
					 # (particle_system_burst, "psys_yellowvine", pos0, 1),
					 # (particle_system_burst, "psys_fireplace_fire_small", pos0, 10),
				(else_try),
					(eq, ":equipped_item", "itm_aestus_estus"),
					(val_div, ":item_length", 15),
					(agent_get_bone_position, pos0, ":agent", 19, 1),
					(try_for_range, ":spawn", 0, ":item_length"),
						(val_mul, ":spawn", 5),
						(position_move_y, pos0, ":spawn"),
						(particle_system_burst, "psys_aestus_slash", pos0, 1),
					(try_end),
				(try_end),
			(try_end),
			
			# reuse this later to assign different particles to the burst
			# (try_begin),
				# (eq, ":element", "str_dmg_type_divine"),
				# (set_current_color, 150, 150, 150),
			# (else_try),
				# (eq, ":element", "str_dmg_type_demonic"),
				# (set_current_color, 75, 10, 10),
			# (else_try),
				# (eq, ":element", "str_dmg_type_dragon"),
				# (set_current_color, 95, 20, 20),
			# (else_try),
				# (eq, ":element", "str_dmg_type_fire"),
				# (set_current_color, 150, 0, 0),
			# (else_try),
				# (eq, ":element", "str_dmg_type_wind"),
				# (set_current_color, 200, 200, 200),
			# (else_try),
				# (eq, ":element", "str_dmg_type_earth"),
				# (set_current_color, 20, 150, 20),
			# (else_try),
				# (eq, ":element", "str_dmg_type_holy"),
				# (set_current_color, 255, 255, 255),
			# (else_try),
				# (eq, ":element", "str_dmg_type_undead"),
				# (set_current_color, 15, 15, 15),
			# (try_end),
			
			(gt, ":element", 0),
			(neq, ":element", "str_dmg_type_normal"),
			
			(particle_system_burst, "psys_invisible_air", pos0, 1),
			
		(try_end),
	 ]
 )
 
fate_noble_phantasm_activate = (
	 0.0, 0, 0, [
	 (key_clicked, key_n),
	 (neg|is_presentation_active, "prsnt_noble_phantasm_selection"),
	 (neg|is_presentation_active, "prsnt_fate_rts_camera"),
	 (neg|is_presentation_active, "prsnt_fate_magic_select"),
	 (neg|is_presentation_active, "prsnt_servant_skill_selection"),
	 (try_begin),
		(neq, "$g_fate_battle_servants_on", 1),
		(display_message, "@Buddy, there aren't servants to do NPs", 0xdddd00),
	 (try_end),
	 (eq, "$g_fate_battle_servants_on", 1),
	 ],
	 [
		# (try_for_agents, ":agent"),
			# (agent_is_alive, ":agent"),
			# (agent_is_human, ":agent"),
			# (agent_is_non_player, ":agent"),
			# (agent_get_troop_id, ":troop", ":agent"),
		
			# (eq, ":troop", "trp_archer_02"),
		# #(display_message, "@NOBLE PHANTASM CALLED."),
		# # Get your servant
		# # Get chosen Noble Phantasm
		# # Have Servant activate NP
			# (agent_get_attack_action, ":action", ":agent"),
			# (eq, ":action", 0),
			# (call_script, "script_gate_of_babylon", ":agent"),
		# (try_end),
		
		# (key_is_down, key_n),
		# (get_player_agent_no, ":agent"),
		# (agent_is_alive, ":agent"),
		# (agent_is_human, ":agent"),
		# (call_script, "script_gate_of_babylon", ":agent"),
	  (try_for_range, ":key", key_1, key_a),
		(omit_key_once, ":key"),
	  (try_end),
	  
	  (try_for_range, ":key", key_numpad_0, key_num_lock),
		(omit_key_once, ":key"),
	  (try_end),
		
		(start_presentation, "prsnt_noble_phantasm_selection"),
		
	 ]
 )
	
fate_hud_elements = (
	0.0, 0, 0, [],
	[
		(assign, ":hud_was_started", 0),
	
		(try_begin),
			(neg|is_presentation_active, "prsnt_fate_hud"),
			(assign, ":hud_was_started", 1),
			(start_presentation, "prsnt_fate_hud"),
		(try_end),
		
		(try_begin),
			(neg|is_presentation_active, "prsnt_fate_overhead_healthbars"),
			(neg|is_presentation_active, "prsnt_fate_magic_select"),
			(neq, ":hud_was_started", 1),
			(start_presentation, "prsnt_fate_overhead_healthbars"),
		(try_end),
	]
)

fate_gae_bolg_test = (
	0.0, 0, 0, [(key_clicked, key_j),],
	[
		(set_fixed_point_multiplier, 100),
		(get_player_agent_no, ":player"),
		(agent_is_alive, ":player"),
		(agent_get_look_position, pos2, ":player"),
		
		(agent_get_item_slot, ":item", ":player", 4),
		(try_begin),
			(neq, ":item", -1),
			(agent_unequip_item, ":player", ":item"),
		(try_end),
		
		(store_random_in_range, ":imod", 0, 6),
		
		(try_begin),
			(ge, ":imod", 1),
			(val_add, ":imod", 22),
		(try_end),
	
		(agent_equip_item, ":player", "itm_imod_test_helmet", ":imod", ":imod", ":imod", ":imod", ":imod", ":imod"),
		(assign, reg10, ":imod"),
		(display_message, "@{reg10}"),
		
		
		# (agent_get_slot, ":flying", ":player", fate_agent_is_flying),
		
		# (try_begin),
			# (eq, ":flying", 0),
			# (agent_set_no_dynamics, ":player", 1),
			
			# (position_move_z, 100, pos2),
			
			# (agent_set_position, ":player", pos2),
			# (agent_set_animation, ":player", "anim_flying_pose"),
			# (agent_set_slot, ":player", fate_agent_is_flying, 1),
		# (else_try),
			# (agent_set_no_dynamics, ":player", 0),
			# (agent_set_slot, ":player", fate_agent_is_flying, 0),
			
			# (position_set_z_to_ground_level, pos2),
			# (agent_set_position, ":player", pos2),
			
			# (agent_set_animation, ":player", "anim_flying_break"),
		# (try_end),
		
		# (try_for_agents, ":agent"),
			# (neq, ":agent", ":player"),
			# (agent_get_position, pos3, ":agent"),
			# (str_store_agent_name, s10, ":agent"),
			
			# (call_script, "script_get_angle_between_points_360", pos2, pos3),
			# (get_angle_between_positions, reg30, pos2, pos3),
			
			# (display_message, "@Angle from Player to {s10}: {reg26}, {reg27}, {reg30}"),
		# (try_end),
		
		# (display_message, "@Basic"),
		# (display_log_message, "@Log"),
		# (display_debug_message, "@Debug"),
		
		# (agent_get_wielded_item, ":equipped_item", ":agent", 0),
		# (agent_unequip_item, ":player", ":equipped_item"),
		# (agent_equip_item, ":player", "itm_caliburn", 3),
		# (agent_set_wielded_item, ":player", "itm_caliburn"),
		
		# (agent_get_look_position, pos2, ":player"),
		# (position_move_z, pos2, 150),
		# (position_move_y, pos2, 100, 0),
		# (add_missile, ":player", pos2, 5000, "itm_earthaoe", 0, "itm_earthaoe", 0),
		#(neg|is_presentation_active, "prsnt_fate_rts_camera"),
		
		#(start_presentation, "prsnt_fate_rts_camera"),
		# (try_for_agents, ":agent"),
			# (agent_is_human, ":agent"),
			# (call_script, "script_reset_armor", ":agent"),
		# (try_end),
		
		# (mission_cam_get_position, pos10),
		
		# (cast_ray, reg1, pos11, pos10),
		# (position_move_y, pos11, -100),
		
		# (try_for_range, ":i", 0, 100),
			# (copy_position, pos12, pos11),
			
			# (store_random_in_range, ":weapon", "itm_gob_sword", "itm_shirou_shirt"),
			
			# (store_random_in_range, ":randx", -55, 55),
			# (store_random_in_range, ":randy", -45, 45),
			
			# (position_rotate_z, pos12, ":randy"),
			# (position_rotate_x, pos12, ":randx"),
		
			# (position_move_y, pos12, -1500),
			
			# (call_script, "script_lookat", pos12, pos11),
			# #(position_rotate_x, pos12, -90),
			# (particle_system_burst, "psys_pistol_smoke", pos12, 1),
			# (add_missile, ":player", pos12, 3500, ":weapon", 0, ":weapon", 0),
		# (try_end),
		
		# (try_for_agents, ":agent"),
		
			# (agent_get_troop_id, ":troop", ":agent"),
			
			# (str_store_agent_name, s1, ":agent"),
			
			# (agent_get_item_slot, ":helmet", ":agent", 5),
			
			# (str_store_troop_face_keys, s0, ":troop"),

			# (try_for_range, ":key", 0, 28),
				# (store_mul, ":face_key", ":key", 10),
				# (store_random_in_range, ":value", -100, 100),
				# (face_keys_set_morph_key, s0, ":face_key", ":value"),
			# (try_end),
			
			# (troop_set_face_keys, ":troop", s0),
			
			
			# (try_begin),
				# (eq, ":helmet", -1),
				# (agent_equip_item, ":agent", "itm_minotaur_mask"),
				# (agent_unequip_item, ":agent", "itm_minotaur_mask"),
			# (else_try),
				# (agent_unequip_item, ":agent", ":helmet"),
				# (agent_equip_item, ":agent", ":helmet"),
			# (try_end),
			
			# (display_message, "@{s1} Face Keys changed"),
		# (try_end),
	]
)

fate_servant_heal_system = (
	 1, 0, 0, [],
	 [
		# (store_trigger_param_1, ":agent"),
		# (store_trigger_param_2, ":cur_health"),	
		# (store_trigger_param_3, ":max_health"),
		# (store_trigger_param_3, ":orig_damage"),
		# (agent_get_troop_id, ":troop", ":agent"),
		# (troop_get_slot, ":master",  ":troop", slot_fate_master),
		# (troop_get_slot, ":master_max_mana", ":master", slot_max_mana),
		# (troop_get_slot, ":master_cur_mana", ":master", slot_cur_mana),
		# (store_div, ":mana_diff", ":max_mana", ":cur_mana"),
		
		# (agent_is_alive, ":agent"),
		
		(assign, ":master_agent", -1),
		
		# # check if servant
		(try_for_agents, ":agent"),
			# (agent_is_non_player, ":agent"),
			(agent_is_human, ":agent"),
			(agent_is_alive, ":agent"),
			(agent_get_troop_id, ":troop", ":agent"),
			(troop_get_slot, ":max_mana", ":troop", slot_fate_max_mana),
			
			(try_begin),
				(troop_is_hero, ":troop"),
				(try_begin),
					(is_between, ":troop", servants_begin, servants_end), # Is the agent a Servant?
				(else_try),
					(troop_get_slot, ":cur_mana", ":troop", slot_fate_cur_mana),
					(lt, ":cur_mana", ":max_mana"),
					(store_div, ":fivepercentmax", ":max_mana", 50),
					(val_add, ":cur_mana", ":fivepercentmax"),
					(val_min, ":cur_mana", ":max_mana"),
					(troop_set_slot, ":troop", slot_fate_cur_mana, ":cur_mana"),
					# (assign, reg30, ":cur_mana"),
					# (display_message, "@I have recovered {reg30} mana."),
				(try_end),
			(else_try),
				(agent_get_slot, ":cur_mana", ":agent", fate_agnt_cur_mana),
				(lt, ":cur_mana", ":max_mana"),
				(store_div, ":fivepercentmax", ":max_mana", 50),
				(val_add, ":cur_mana", ":fivepercentmax"),
				(val_min, ":cur_mana", ":max_mana"),
				(agent_set_slot, ":agent", fate_agnt_cur_mana, ":cur_mana"),
				# (assign, reg30, ":cur_mana"),
				# (display_message, "@I have recovered {reg30} mana."),
			(try_end),
			
			(is_between, ":troop", servants_begin, servants_end), # Is the agent a Servant?
				(troop_get_slot, ":master",  ":troop", slot_fate_master),
				(try_for_agents, ":master_a"),
					(agent_get_troop_id, ":mtroop", ":master_a"),
					(eq, ":mtroop", ":master"),
					(assign, ":master_agent", ":master_a"),
				(try_end),
				
				(assign, ":continue", 0),
				
				(try_begin),
					(eq, "$g_fate_battle_masters_on", 0),
					
					(assign, ":continue", 1),
				(else_try),
					(neq, ":master_agent", -1),
					(agent_is_alive, ":master_agent"),
					(neg|troop_is_wounded, ":master"),
					
					(assign, ":continue", 1),
				(try_end),
				
				(eq, ":continue", 1),
				
				(troop_get_slot, ":cur_mana", ":troop", slot_fate_cur_mana),
				(try_begin),
					(lt, ":cur_mana", ":max_mana"),
					(store_div, ":fivepercentmax", ":max_mana", 50),
					(val_add, ":cur_mana", ":fivepercentmax"),
					(val_min, ":cur_mana", ":max_mana"),
					(troop_set_slot, ":troop", slot_fate_cur_mana, ":cur_mana"),
				(try_end),
				
				(neq, ":master_agent", -1),
				(agent_is_alive, ":master_agent"),
				(neg|troop_is_wounded, ":master"),
				
				(troop_get_slot, reg5, ":master", slot_fate_max_mana),
				(troop_get_slot, reg6, ":master", slot_fate_cur_mana),
				(store_div, ":mana_diff", reg6, reg5),
				(str_store_agent_name, s11, ":agent"),
				(str_store_troop_name, s12, ":master"),
				# #if servant, and their master alive
				(store_agent_hit_points, ":cur_health" , ":agent", 0),				
				(lt, ":cur_health", 100),
						#if master alive, heal 10% per 2 second at full mana, 2% at empty
						(try_begin),
							(le, ":mana_diff", 2),
							(assign, ":heal", 10),
						(else_try),
							(le, ":mana_diff", 4),
							(assign, ":heal", 8),
						(else_try),
							(le, ":mana_diff", 8),
							(assign, ":heal", 6),
						(else_try),
							(le, ":mana_diff", 16),
							(assign, ":heal", 4),
						(else_try),
							(assign, ":heal", 2),
						#(else_try),
						(try_end),
					(val_add, ":cur_health", ":heal"),
					(agent_set_hit_points, ":agent", ":cur_health", 0),
					(assign, reg10, ":heal"),
					# (display_message, "@Servant {s11} healed for {reg10}% by Master {s12}!"),
			 (end_try),
	 ]
 )
 
fate_aoe_poison = (
		0.1, 0, 0, [],
		[	
			(set_fixed_point_multiplier, 100),
			
			(scene_prop_get_num_instances, ":num_instances", "spr_fate_yew_tree"), # How many Yew Trees are there?
			
			(gt, ":num_instances", 0), # More than zero?
			
			(try_for_range, ":cur_i", 0, ":num_instances"),
			
				(scene_prop_get_instance, ":instance", "spr_fate_yew_tree", ":cur_i"),
				
				(scene_prop_get_slot, ":spawner", ":instance", fate_prop_owner),
				(prop_instance_get_position, pos1, ":instance"),
				
				(try_for_agents, ":agent_no", pos1, 750),
					(neq, ":agent_no", ":spawner"),
					(agent_is_alive, ":agent_no"),
					
					
					# (get_distance_between_positions, ":dist", pos1, pos2),
					
					#(le, ":dist", 750),	
					(agent_get_slot, ":build_up", ":agent_no", fate_agent_poison_build_up),
					(val_add, ":build_up", 20),
					(agent_set_slot, ":agent_no", fate_agent_poison_build_up, ":build_up"),
				(try_end),
				
			(try_end),
		]
)

fate_servant_agent_effects = (
  ti_on_agent_spawn, 0, 0, [], [
	
	(store_trigger_param_1, ":agent"),
	(agent_get_troop_id, ":cur_troop", ":agent"),
	(get_battle_advantage, ":adv"),
	
	(agent_set_slot, ":agent", slot_agent_charge_time, 0),
	(agent_set_slot, ":agent", slot_agent_active_charge, 0),
	
	(call_script, "script_fate_battle_continuation", ":agent", -1), # reset the bc
	
	(try_begin),
		(troop_get_slot, ":classification", ":cur_troop", slot_fate_classification),
		(eq, ":classification", "str_classification_magus"),
		(agent_get_slot, ":current_mana", ":agent", fate_agnt_max_mana),
		(agent_set_slot, ":agent", fate_agnt_cur_mana, ":current_mana"),
	(try_end),
	
	(try_begin),
	(is_between, ":cur_troop", servants_begin, servants_end),
	
		# (troop_get_slot, ":class", ":troop", slot_fate_servant_class),
		(try_begin),
			
			#(agent_set_damage_modifier, ":agent", 150),
			# Saber
			(troop_slot_eq, ":cur_troop", slot_fate_servant_class, "str_class_saber"),
				#(agent_set_damage_modifier, ":agent", 300),
				(agent_set_horse_speed_factor, ":agent", 200),
				(agent_set_speed_limit, ":agent", 30),
				(agent_set_speed_modifier, ":agent", 175),
			(else_try),
			# Lancer
			(troop_slot_eq, ":cur_troop", slot_fate_servant_class, "str_class_lancer"),
				#(agent_set_damage_modifier, ":agent", 250),
				(agent_set_speed_limit, ":agent", 40),
				(agent_set_speed_modifier, ":agent", 200),
			(else_try),
			# Archer
			(troop_slot_eq, ":cur_troop", slot_fate_servant_class, "str_class_archer"),
				(agent_set_accuracy_modifier, ":agent", 500),
				(agent_set_ranged_damage_modifier, ":agent", 250),
				(agent_set_reload_speed_modifier, ":agent", 200),
				(agent_set_speed_limit, ":agent", 35),
				(agent_set_speed_modifier, ":agent", 225),
			(else_try),
			# Rider
			(troop_slot_eq, ":cur_troop", slot_fate_servant_class, "str_class_rider"),
				(agent_set_horse_speed_factor, ":agent", 350),
				(agent_set_speed_modifier, ":agent", 150),
				(agent_set_speed_limit, ":agent", 55),
			(else_try),
			# Caster
			(troop_slot_eq, ":cur_troop", slot_fate_servant_class, "str_class_caster"),
				(agent_set_accuracy_modifier, ":agent", 250),
				(agent_set_speed_modifier, ":agent", 125),
			(else_try),
			# Assassin
			(troop_slot_eq, ":cur_troop", slot_fate_servant_class, "str_class_assassin"),
				(agent_set_accuracy_modifier, ":agent", 300),
				(agent_set_ranged_damage_modifier, ":agent", 150),
				(agent_set_speed_modifier, ":agent", 200),
				(agent_set_speed_limit, ":agent", 30),
			(else_try),
			# Berserker
			(troop_slot_eq, ":cur_troop", slot_fate_servant_class, "str_class_berserker"),
				#(agent_set_damage_modifier, ":agent", 400),
				(agent_ai_set_always_attack_in_melee, ":agent", 1),
				(agent_set_speed_modifier, ":agent", 150),
				(agent_set_speed_limit, ":agent", 30),
				(agent_ai_set_aggressiveness, ":agent", 100),
			(else_try),
			# Ruler
			(troop_slot_eq, ":cur_troop", slot_fate_servant_class, "str_class_ruler"),
				#(agent_set_damage_modifier, ":agent", 200),
				(agent_set_speed_modifier, ":agent", 150),
				(agent_set_speed_limit, ":agent", 30),
			(else_try),
			# Shielder
			(troop_slot_eq, ":cur_troop", slot_fate_servant_class, "str_class_shielder"),
				#(agent_set_damage_modifier, ":agent", 125),
				(agent_set_speed_modifier, ":agent", 125),
				(agent_set_speed_limit, ":agent", 25),
			(else_try),
		
		
		(try_end),
		
		# Check for individual Servants and adjust variables, and assign bonuses
		# based on skills.
		
		
		(try_begin),
			(eq, ":cur_troop", "trp_archer_03"), # Atalanta
				(call_script, "script_p_skill_crossing_arcadia", ":agent"),
				
			(lt, ":adv", 1), 
				(call_script, "script_p_skill_aesthetics_of_the_last_spurt", ":agent"),
		(try_end),
		 
		# Artoria Pendragon
			# N/A
		# Nero Claudius
			# N/A
		# Siegfried
			# N/A
		# Fergus mac Roich
			# N/A
		# Richard Lionheart
			# N/A
		
		# Cu Chulainn
			#
		# Diarmuid Ua Duibhne
			#
		# Fionn mac Cumhaill
			#
		# Hector
			#
		# Brynhildr
			#

		# EMIYA
			#
		# Gilgamesh
			#
		# Robin Hood
			#
		# Arash
			#

		# Medusa
			#
		# Iskandar
			#
		# Achilles
			#
		# Astolfo
			#
		# Saint George
			#

		# Medea
		# Gilles de Rais
		# William Shakespeare
		# Avicebron
		# Hohenheim

		# Hassan of the Cursed Arm
		# Hassan of the Hundred Faces
		# Hassan of the Shadow Peeling
		# Hassan of Serenity
		# King Hassan-i-Sabbah

		# Heracles
		# Lancelot
		# Spartacus
		# Frankenstein
		# Asterios
	(try_end),
	
	(try_begin),
		(eq, ":cur_troop", "trp_actor"),
		
		(store_random_in_range, ":random_class", 0, 7400),
		(val_div, ":random_class", 1000),
		
		(store_random_in_range, ":random_servant", 0, 5400),
		(val_div, ":random_servant", 1000),
			
		(try_begin),
			(eq, ":random_class", 0),
			(val_add, ":random_servant", saber_begin),
		(else_try),
			(eq, ":random_class", 1),
			(val_add, ":random_servant", archer_begin),
		(else_try),
			(eq, ":random_class", 2),
			(val_add, ":random_servant", lancer_begin),
		(else_try),
			(eq, ":random_class", 3),
			(val_add, ":random_servant", rider_begin),
		(else_try),
			(eq, ":random_class", 4),
			(val_add, ":random_servant", assassin_begin),
		(else_try),
			(eq, ":random_class", 5),
			(val_add, ":random_servant", caster_begin),
		(else_try),
			(val_add, ":random_servant", berserker_begin),
		(try_end),
		
		(try_for_range, ":slot", 0, 8),
			(agent_get_item_slot, ":item", ":agent", ":slot"),
			
			(try_begin),
				(gt, ":item", 0),
				(agent_unequip_item, ":agent", ":item"),
			(try_end),
			
			(troop_get_inventory_slot, ":newitem", ":random_servant", ":slot"),
			
			(try_begin),
				(gt, ":newitem", 0),
				(agent_equip_item, ":agent", ":newitem", ":slot"),
			(try_end),
			
		(try_end),
		
	(try_end),
	
	(call_script, "script_determine_per_agent_stats", ":agent"),
    ])
	
fate_servant_death_handling = (
  ti_on_agent_killed_or_wounded, 0, 0, [], [
	
	(set_fixed_point_multiplier, 100),
	(store_trigger_param_1, ":agent"),
	(agent_get_troop_id, ":cur_troop", ":agent"),

	(store_trigger_param_2, ":killer_agent"),
	(agent_get_troop_id, ":killer_troop", ":killer_agent"),
	
	(str_store_agent_name, s10, ":agent"),
	(str_store_agent_name, s11, ":killer_agent"),
	
	# (display_message, "@{s10} killed by {s11}", 0x44ddaa),
	
	(is_between, ":killer_troop", servants_begin, servants_end),
		(try_begin),
			(neq, ":cur_troop", "trp_player"),
			(neg|troop_slot_ge, ":cur_troop", slot_fate_cur_battle_continuations, 1),
			(set_trigger_result, 1),
			# (display_log_message, "@There is no surviving the attack of a servant"),
		(else_try),
			(is_between, ":cur_troop", servants_begin, servants_end),
			(set_trigger_result, 1),
			# (display_log_message, "@Only a servant can kill another servant."),
		(try_end),
		
		
  ])
  
charged_shot = (
	0.05, 0, 0, [], [
	(set_fixed_point_multiplier, 100),
	
	(try_for_agents, ":agent"),
		(agent_slot_ge, ":agent", slot_agent_active_charge, 1),
		
		(agent_get_ammo, ":cur_ammo", ":agent", 1),
		(gt, ":cur_ammo", 0),
		(agent_is_active, ":agent"),
		(agent_is_human, ":agent"),
		
		(agent_get_slot, ":charged_shots", ":agent", slot_agent_active_charge),
		(convert_from_fixed_point, ":charged_shots"),
		
		(ge, ":charged_shots", 1),
		(agent_get_wielded_item, ":equipped_item", ":agent", 0),
		(gt, ":equipped_item", 0),
		
		(agent_get_item_slot, ":slot1", ":agent", 0),
		(try_begin),
			(gt, ":slot1", 0),
			(item_get_type, ":slot1_type", ":slot1"),
		(try_end),
			
		(agent_get_item_slot, ":slot2", ":agent", 1),
		(try_begin),
			(gt, ":slot2", 0),
			(item_get_type, ":slot2_type", ":slot2"),
		(try_end),
		
		(agent_get_item_slot, ":slot3", ":agent", 2),
		(try_begin),
			(gt, ":slot3", 0),
			(item_get_type, ":slot3_type", ":slot3"),
		(try_end),
		
		(agent_get_item_slot, ":slot4", ":agent", 3),
		(try_begin),
			(gt, ":slot4", 0),
			(item_get_type, ":slot4_type", ":slot4"),
		(try_end),
		
		(item_get_type, ":item_type", ":equipped_item"),
		
		(try_begin),
			(gt, ":slot1", 0),
			(eq, ":slot1_type", itp_type_arrows),
			(assign, ":ammo", ":slot1"),
		(else_try),
			(gt, ":slot2", 0),
			(eq, ":slot2_type", itp_type_arrows),
			(assign, ":ammo", ":slot2"),
		(else_try),
			(gt, ":slot3", 0),
			(eq, ":slot3_type", itp_type_arrows),
			(assign, ":ammo", ":slot3"),
		(else_try),
			(gt, ":slot4", 0),
			(eq, ":slot4_type", itp_type_arrows),
			(assign, ":ammo", ":slot4"),
		(try_end),
		
		(agent_get_look_position, pos9, ":agent"), 		# Get Agent's look for math
		(agent_get_bone_position, pos10, ":agent", 13, 1), # left hand
		(agent_get_bone_position, pos11, ":agent", 19, 1), # right hand
		(position_copy_rotation, pos10, pos9),
		(position_copy_rotation, pos11, pos9),
		
		(is_between, ":item_type", itp_type_bow, itp_type_goods),
		
		(try_begin),
			(eq, ":item_type", itp_type_bow),
			(assign, ":spawn_pos", pos10),
			(try_begin),
				(gt, ":slot1", 0),
				(eq, ":slot1_type", itp_type_arrows),
				(assign, ":ammo", ":slot1"),
			(else_try),
				(gt, ":slot2", 0),
				(eq, ":slot2_type", itp_type_arrows),
				(assign, ":ammo", ":slot2"),
			(else_try),
				(gt, ":slot3", 0),
				(eq, ":slot3_type", itp_type_arrows),
				(assign, ":ammo", ":slot3"),
			(else_try),
				(gt, ":slot4", 0),
				(eq, ":slot4_type", itp_type_arrows),
				(assign, ":ammo", ":slot4"),
			(try_end),
			
			(store_mod, ":mod_charge", ":charged_shots", 2),
			(val_min, ":charged_shots", 20),
			
			(try_begin),
				(eq, ":mod_charge", 0),
				(agent_set_animation, ":agent", "anim_ready_bow", 1),
			(else_try),
				(agent_set_animation, ":agent", "anim_release_bow", 1),
			(try_end),
		(else_try),
			(eq, ":item_type", itp_type_crossbow),
			(assign, ":spawn_pos", pos11),
			(try_begin),
				(gt, ":slot1", 0),
				(eq, ":slot1_type", itp_type_bolts),
				(assign, ":ammo", ":slot1"),
			(else_try),
				(gt, ":slot2", 0),
				(eq, ":slot2_type", itp_type_bolts),
				(assign, ":ammo", ":slot2"),
			(else_try),
				(gt, ":slot3", 0),
				(eq, ":slot3_type", itp_type_bolts),
				(assign, ":ammo", ":slot3"),
			(else_try),
				(gt, ":slot4", 0),
				(eq, ":slot4_type", itp_type_bolts),
				(assign, ":ammo", ":slot4"),
			(try_end),
			
			(store_mod, ":mod_charge", ":charged_shots", 2),
			
			(try_begin),
				(eq, ":mod_charge", 0),
				(agent_set_animation, ":agent", "anim_ready_pistol", 1),
			(else_try),
				(agent_set_animation, ":agent", "anim_release_pistol", 1),
			(try_end),
		(else_try),
			(eq, ":item_type", itp_type_thrown),
			
			(assign, ":spawn_pos", pos11),
			
			(assign, ":ammo", ":equipped_item"),
			
			(store_mod, ":mod_charge", ":charged_shots", 2),
			
			(try_begin),
				(eq, ":mod_charge", 0),
				(agent_set_animation, ":agent", "anim_ready_throwing_knife", 1),
			(else_try),
				(agent_set_animation, ":agent", "anim_release_throwing_knife", 1),
			(try_end),
		(try_end),	
			
			(add_missile, ":agent", ":spawn_pos", 7600, ":equipped_item", 0, ":ammo", 0),
			
			(agent_get_ammo, ":total_ammo", ":agent", 1),
			(val_sub, ":total_ammo", 1),
			(agent_set_ammo, ":agent", ":ammo", ":total_ammo"),
			
			(val_sub, ":charged_shots", 1),
			(convert_to_fixed_point, ":charged_shots"),
			(agent_set_slot, ":agent", slot_agent_active_charge, ":charged_shots"),
	(try_end),
	
	]
)
  
fate_charge_attack = (
	0, 0, 0, [], [
	
	(set_fixed_point_multiplier, 100),
	
	(try_for_agents, ":agent"),
	
		(agent_get_slot, ":charge_time", ":agent", slot_agent_charge_time),
		(agent_get_wielded_item, ":equipped_item", ":agent", 0),
		
		
		
		# (assign, ":charge_seconds", ":charge_time"),
		# (convert_from_fixed_point, ":charge_seconds"),
		# (assign, reg50, ":charge_seconds"),
		
		(neq, ":equipped_item", -1),
		(item_get_type, ":item_type", ":equipped_item"),

		(try_begin),
			(agent_get_attack_action, ":action", ":agent"),
			(try_begin),
				(eq, ":action", 1),
				(val_add, ":charge_time", 1),
				(agent_set_slot, ":agent", slot_agent_charge_time, ":charge_time"),
			(else_try),
				(gt, ":charge_time", 0),
				(try_begin),
					(eq, ":item_type", itp_type_bow),
					(val_sub, ":charge_time", 100),
					(agent_set_slot, ":agent", slot_agent_active_charge, ":charge_time"),
				(else_try),
					(agent_set_slot, ":agent", slot_agent_active_charge, ":charge_time"),
				(try_end),
				# (display_message, "@Charge time of {reg50} seconds!"),
				(assign, ":charge_time", 0),
				(agent_set_slot, ":agent", slot_agent_charge_time, ":charge_time"),
			(try_end),
		(try_end),
	(try_end),
	]
)

fate_charge_attack_effects = (
	0.0, 0, 0, [], [
	
	(set_fixed_point_multiplier, 100),
	(store_mission_timer_a_msec, ":mission_timer"),
	(val_mod, ":mission_timer", 20),
	
	(try_for_agents, ":agent"),
		(agent_is_alive, ":agent"),
		(agent_is_human, ":agent"),
		
		(agent_get_slot, ":charge_time", ":agent", slot_agent_charge_time),
		(assign, ":charge_time_msec", ":charge_time"),
		(convert_from_fixed_point, ":charge_time"),
		
		(gt, ":charge_time", 0),
		
		(agent_get_bone_position, pos0, ":agent", 19, 1),
		(agent_get_bone_position, pos1, ":agent", 13, 1),
		(agent_get_position, pos2, ":agent"),
		(agent_get_wielded_item, ":equipped_item", ":agent", 0),
		
		(gt, ":equipped_item", 0),
		(item_get_type, ":item_type", ":equipped_item"),

		(try_begin),
			(eq, ":item_type", itp_type_bow),
			(assign, ":hand", pos1),
		(else_try),
			(assign, ":hand", pos0),
		(try_end),
		
		(gt, ":charge_time", 1),
		
		(try_begin),
			(eq, ":charge_time_msec", 100),
			(particle_system_burst, "psys_invisible_air", ":hand", 1),
		(else_try),
			(eq, ":charge_time_msec", 400),
			(particle_system_burst, "psys_fireplace_fire_small", ":hand", 1),
		(else_try),
			(eq, ":charge_time_msec", 700),
			(particle_system_burst, "psys_fireplace_fire_big", ":hand", 1),
		(try_end),
		
		
		(try_begin),
			(is_between, ":charge_time", 3, 7),
			(particle_system_burst, "psys_charge_spark", pos2, 1),
			(eq, ":mission_timer", 0),
			(particle_system_burst, "psys_charge_spark_field", pos2, 1),
		(else_try),
			(ge, ":charge_time", 7),
			(particle_system_burst, "psys_charge_spark", pos2, 3),
			#(particle_system_burst, "psys_charge_spark_field", pos2, 10),
			(particle_system_burst, "psys_charge_spark_full", pos2, 1),
			(particle_system_burst, "psys_charge_beams_full", pos2, 1),	
			(eq, ":mission_timer", 0),
			(particle_system_burst, "psys_charge_spark_field", pos2, 5),
			(particle_system_burst, "psys_charge_beams_2_full", pos2, 1),		
		(try_end),
		
		# (else_try),
			# (try_begin),
				# (lt, ":charge_time", 2),
				# (particle_system_burst, "psys_invisible_air", pos0, 1),
			# (else_try),
				# (is_between, ":charge_time", 2, 5),
				# (particle_system_burst, "psys_fireplace_fire_small", pos0, 1),
				# (particle_system_burst, "psys_charge_spark", pos2, 1),
			# (else_try),
				# (ge, ":charge_time", 5),
				# (particle_system_burst, "psys_fireplace_fire_big", pos0, 1),
				# #(particle_system_burst, "psys_lava", pos2, 1),
				# (particle_system_burst, "psys_charge_spark", pos2, 10),
			# (try_end),
		# (try_end),
		
	(try_end),
	]
)

fate_servant_combat_system = (
    0.1, 0, 0, [],
  [
(set_fixed_point_multiplier, 100),

(try_for_agents, ":potential_servant"),	# Run a loop through all agents in a mission:
		(agent_is_non_player, ":potential_servant"),
        (agent_is_human, ":potential_servant"),	# are they human?
        (agent_is_alive, ":potential_servant"),	# are they alive?
		(agent_get_troop_id, ":potential_servant_troop", ":potential_servant"),	# Who are they?
		(is_between, ":potential_servant_troop", servants_begin, servants_end),	# Is that a Servant?
		
		(assign, ":servant", ":potential_servant"),				# Then store them									
		(agent_get_team, ":servant_team", ":servant"),			# What team are they on?
		(agent_get_position, pos11, ":servant"),
		
		# (assign, ":shortest_dist", 1000),
		# (assign, ":nearest_enemy", 0),
		
		# (try_for_agents, ":enemy_agent", pos11, 2500),
			# (agent_is_human, ":enemy_agent"),
			# (agent_is_alive, ":enemy_agent"),
			# (agent_get_team, ":enemy_agent_team", ":enemy_agent"),
			# (teams_are_enemies, ":enemy_agent_team", ":servant_team"),
			
			# (neq, ":enemy_agent", ":servant"),
			#(agent_get_position, pos10, ":servant"),
			# (agent_get_position, pos11, ":enemy_agent"),
			# (get_distance_between_positions, ":agent_dist", pos10, pos11),
			# (gt, ":agent_dist", 0),
			# (lt, ":agent_dist", ":shortest_dist"),
				# (assign, ":shortest_dist", ":agent_dist"),
				# (assign, ":nearest_enemy", ":enemy_agent"),
				
		# (try_end),
		
		(agent_ai_get_look_target, ":enemy_agent", ":servant"),
		
		(try_begin),
			(ge, ":enemy_agent", 0),
			
			(agent_get_position, pos10, ":enemy_agent"),
			
			# (agent_get_troop_id, ":e_troop", ":enemy_agent"),
			# (str_store_troop_name, s35, ":e_troop"),
			# (display_message, "@Target acquired, {s35}"),
			
			(get_distance_between_positions, ":agent_dist", pos10, pos11),
			(lt, ":agent_dist", 7500),
			(agent_is_human, ":enemy_agent"),
			(agent_is_alive, ":enemy_agent"),
			(agent_get_team, ":enemy_agent_team", ":enemy_agent"),
			(teams_are_enemies, ":enemy_agent_team", ":servant_team"),
			# (gt, ":nearest_enemy", 0),
			(call_script, "script_fate_servant_combat", ":servant", ":enemy_agent"),
			(agent_force_rethink, ":servant"),
		(else_try),
			(agent_clear_scripted_mode, ":servant"),
		(try_end),
(try_end),
 ])
 
fate_combat_magic_system = (
    0.5, 0, 0, [],
[
	(set_fixed_point_multiplier, 100),
	# (assign, ":distance", 10000),
	# (assign, ":nearest_enemy", 0),

	(try_for_agents, ":magic_user"),	# Run a loop through all agents in a mission:
		(agent_is_non_player, ":magic_user"), # are they. . . .you?!
		(agent_is_human, ":magic_user"),	# are they human?
		(agent_is_alive, ":magic_user"),	# are they still alive?
		(agent_is_alarmed, ":magic_user"),	# Is in a fight
		# (agent_slot_ge, ":magic_user", fate_agnt_cur_mana, 1),
		
		(agent_get_troop_id, ":magic_user_troop", ":magic_user"),	# Who are they?
		# (neq, ":magic_user_troop", "trp_summoner"),
		
		(troop_get_slot, ":class", ":magic_user_troop", slot_fate_classification),
		# (eq, ":class", "str_classification_magus"),
		(this_or_next|eq, ":class", "str_classification_magus"),	# Magic can be used by magus
		(this_or_next|eq, ":class", "str_classification_servant"),	# Servants
		(eq, ":class", "str_classification_master"),				# and Masters
		
			(agent_get_team, ":magic_user_team", ":magic_user"),						# What team are they on?
		#	(str_store_troop_name, s10, ":magic_user_troop"),			# Give me their name.
		#	(display_message, "@{s10} has passed the magus check and will continue!"),	#debug Check
			
			# (store_random_in_range, ":spell_choice", 0, 4),
			
			(troop_get_slot, ":spellone", ":magic_user_troop", slot_fate_spell_slot_1),
			# (call_script, "script_determine_spell_tags", ":spellone"),
			# (assign, ":spellone_element", s10),
			# (assign, ":spellone_effect", reg9),
			# (assign, ":spellone_target", reg10),
			# (assign, ":spellone_cost", reg11),
			(troop_get_slot, ":spelltwo", ":magic_user_troop", slot_fate_spell_slot_2),
			# (call_script, "script_determine_spell_tags", ":spelltwo"),
			# (assign, ":spelltwo_element", s10),
			# (assign, ":spelltwo_effect", reg9),
			# (assign, ":spelltwo_target", reg10),
			# (assign, ":spelltwo_cost", reg11),
			# (troop_get_slot, ":spellthr", ":magic_user_troop", slot_fate_spell_slot_3),
			# (call_script, "script_determine_spell_tags", ":spellthr"),
			# (assign, ":spellthr_element", s10),
			# (assign, ":spellthr_effect", reg9),
			# (assign, ":spellthr_target", reg10),
			# (assign, ":spellthr_cost", reg11),
			# (troop_get_slot, ":spellfou", ":magic_user_troop", slot_fate_spell_slot_4),
			# (call_script, "script_determine_spell_tags", ":spellfou"),
			# (assign, ":spellfou_element", s10),
			# (assign, ":spellfou_effect", reg9),
			# (assign, ":spellfou_target", reg10),
			# (assign, ":spellfour_cost", reg11),
			
			# Pull the agents assigned spells. 
			# Check to see their utility.
			# Check the situation the agent is in currently.
			# Decide which of the selected spells best fits this situation
			# Use it.
			
			# (try_begin),
				# (eq, ":spell_choice", 0),
				# (agent_set_slot, ":magic_user", fate_agnt_chosen_spell, ":spellone"),
			# (else_try),
				# (eq, ":spell_choice", 1),
				# (agent_set_slot, ":magic_user", fate_agnt_chosen_spell, ":spelltwo"),
			# (else_try),
				# (eq, ":spell_choice", 2),
				# (agent_set_slot, ":magic_user", fate_agnt_chosen_spell, ":spellthr"),
			# (else_try),
				# (agent_set_slot, ":magic_user", fate_agnt_chosen_spell, ":spellfou"),
			# (try_end),
			
			
			(gt, ":spellone", 0),
			(gt, ":spelltwo", 0),
			
			# (try_for_agents, ":enemy_agent"),
				# (agent_is_human, ":enemy_agent"),
				# (agent_is_alive, ":enemy_agent"),
				# (neq, ":enemy_agent", ":magic_user"),
				# (agent_get_team, ":enemy_agent_team", ":enemy_agent"),
				# (teams_are_enemies, ":enemy_agent_team", ":magic_user_team"),
				# (agent_get_position, pos12, ":enemy_agent"),
				(agent_get_position, pos11, ":magic_user"),
				# (get_distance_between_positions, ":delta", pos11, pos12),	# How far apart are they
				# (le, ":delta", ":distance"),							# Is this agent closer or equidistant than the previous closest one (closer than 10m)?
					# (assign, ":distance", ":delta"),					# assign this as the new shortest distance
					# (assign, ":nearest_enemy", ":enemy_agent"),					
			# (try_end),
			
			# # (agent_ai_get_cached_enemy, ":nearest_enemy", ":magic_user", 0),
			# # (agent_is_alive, ":nearest_enemy"),
			# # (agent_get_position, pos11, ":magic_user"),
			# # (agent_get_position, pos12, ":nearest_enemy"),
			# # (get_distance_between_positions, ":distance", pos11, pos12),
			
			# (try_begin),
				# # (le, ":shortest_dist", 500),
				# (le, ":distance", 2000),
				# (gt, ":nearest_enemy", 0),
				
				# (agent_get_team, ":enemy_agent_team", ":nearest_enemy"),
				# (teams_are_enemies, ":enemy_agent_team", ":magic_user_team"),
				
				# (agent_set_slot, ":magic_user", fate_agnt_chosen_spell, ":spelltwo"),
				# (call_script, "script_magic_cast", ":magic_user"),
				# # (display_message, "@I see an agent, is within twenty meters"),
			# (else_try),
				# (agent_set_slot, ":magic_user", fate_agnt_chosen_spell, ":spellone"),
				# (call_script, "script_magic_cast", ":magic_user"),
				# # (display_message, "@I see an agent, but is too far, so I will enhance"),
			# (try_end),
			
			(try_for_agents, ":enemy_agent", pos11, 5000),
				(agent_is_alive, ":enemy_agent"),
				(agent_get_team, ":enemy_agent_team", ":enemy_agent"),
				(teams_are_enemies, ":enemy_agent_team", ":magic_user_team"),
				(agent_get_position, pos10, ":enemy_agent"),
				(get_distance_between_positions, ":distance", pos10, pos11),
				
				(try_begin),
					(troop_is_hero, ":magic_user_troop"), # Is this agent a mob or someone important
					(try_begin),
						(lt, ":distance", 1000),
						(agent_is_in_line_of_sight, ":magic_user", pos10),
						(troop_set_slot, ":magic_user_troop", slot_fate_chosen_spell, 2),
					(else_try),
						# (agent_set_slot, ":magic_user", fate_agnt_chosen_spell, ":spellone"),
						(troop_set_slot, ":magic_user_troop", slot_fate_chosen_spell, 1),
					(try_end),
				(else_try),
					(try_begin),
						(lt, ":distance", 1000),
						(agent_is_in_line_of_sight, ":magic_user", pos10),
						(agent_set_slot, ":magic_user", fate_agnt_chosen_spell, 2),
					(else_try),
						(agent_set_slot, ":magic_user", fate_agnt_chosen_spell, 1),
					(try_end),
				(try_end),
				
				(call_script, "script_magic_cast", ":magic_user"),
			(try_end),
	(try_end),
 ])
  
fate_battle_continuation = (
  0, 0, 0, [], [
	(set_fixed_point_multiplier, 100),
	
	(try_for_agents, ":agent"),
		# (agent_get_troop_id, ":troop", ":agent"),
		# (troop_slot_ge, ":troop", slot_fate_max_battle_continuations, 1),
		(agent_slot_ge, ":agent", slot_fate_can_ressurect, 1),
		
		(agent_get_team, ":team", ":agent"),
		
		(agent_get_slot, ":death_time", ":agent", slot_fate_death_time),
		
		(try_begin),
			(gt, ":death_time", 0),
			#(convert_from_fixed_point, ":death_time"),
			(val_sub, ":death_time", 1),
			#(convert_to_fixed_point, ":death_time"),		# Subtract Death Time
			(agent_set_slot, ":agent", slot_fate_death_time, ":death_time"),
		(else_try),
			(agent_set_no_dynamics, ":agent", 0),
			(agent_set_slot, ":agent", slot_fate_can_ressurect, 0),		# Agent is marked as no longer waiting to respawn
			(agent_set_animation, ":agent", "anim_ressurection"),		# Play ressurection animation
			(agent_set_is_alarmed, ":agent", 1),
			(try_for_agents, ":enemy_agent"),
				(agent_get_team, ":a_team", ":enemy_agent"),
				(teams_are_enemies, ":a_team", ":team"),						# are we enemies?
				(agent_add_relation_with_agent, ":enemy_agent", ":agent", -1),	# we definately are now
				(agent_add_relation_with_agent, ":agent", ":enemy_agent", -1),
			(try_end),
		(try_end),
	(try_end),		
  ])
fate_triggers = [
		fate_aoe_poison,
		common_fate_damage_system,
		fate_servant_agent_effects,
		fate_servant_death_handling, 
		fate_servant_combat_system, 
		fate_combat_magic_system,
		fate_battle_continuation,
		fate_servant_heal_system,
		fate_hud_elements,
		fate_gae_bolg_test,
		fate_magic_casts,
		fate_magic_select,
		fate_elemental_visualization,
		fate_charge_attack,
		fate_charge_attack_effects,
		charged_shot,
		fate_noble_phantasm_activate,
		double_jump,
		fate_servant_skill_select,
    ]
  
  
mission_templates = [
    # #######################################################################
    #		town_default and conversation_encounter are Hardcoded! 
	#	Absolutely DO NOT move. You can modify them, but be cool about it.
	# #######################################################################
    ("town_default", 0, -1,
    "Town Default",
    [
	    (0, mtef_scene_source|mtef_team_0, af_override_horse, 0, 1, []),
    ],     
    []),

    ("conversation_encounter", 0, -1,
    "Conversation Encounter",
    [
	    (0, mtef_visitor_source, af_override_fullhelm, 0, 1, []), 
		(1, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
        (2, mtef_visitor_source, af_override_fullhelm, 0, 1, []), 
		(3, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(4, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(5, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(6, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
        (7, mtef_visitor_source, af_override_fullhelm, 0, 1, []), 
		(8, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(9, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(10, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(11, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		
        # prisoners now...
        (12, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(13, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(14, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(15, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(16, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
    
	    # Other party
        (17, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(18, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(19, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(20, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(21, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(22, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(23, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(24, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(25, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(26, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(27, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(28, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(29, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(30, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
		(31, mtef_visitor_source, af_override_fullhelm, 0, 1, []),
    ],
    [],
    ),
  
    # #######################################################################
	#	I left lead_charge in because it is so commonly used, but it's not
	#						~ R E Q U I R E D ~ 
	#										y'know.
	# #######################################################################

    ("lead_charge", mtf_battle_mode, charge,
    "Lead Charge",
    [
        (1, mtef_defenders|mtef_team_0, 0, aif_start_alarmed, 12, []),
        (0, mtef_defenders|mtef_team_0, 0, aif_start_alarmed, 0, []),
        (4, mtef_attackers|mtef_team_1, 0, aif_start_alarmed, 12, []),
        (4, mtef_attackers|mtef_team_1, 0, aif_start_alarmed, 0, []),
    ],
    [
    ]),
	
	# #######################################################################
	#	Feel free to plug away here.
	# #######################################################################
 
	(
    "fate_quick_battle",mtf_arena_fight|mtf_commit_casualties,-1,
    "Prepare yourselves!",
    [    
	  (0, mtef_visitor_source|mtef_team_0, 0, aif_start_alarmed, 1,[]),
	  (1, mtef_visitor_source|mtef_team_0, 0, aif_start_alarmed, 1,[]),
	  # (42, mtef_visitor_source|mtef_team_0, 0, aif_start_alarmed, 1,[]),
	  
	  (16, mtef_visitor_source|mtef_team_1, 0, aif_start_alarmed, 1,[]),
	  (17, mtef_visitor_source|mtef_team_1, 0, aif_start_alarmed, 1,[]),    
	 #  (43, mtef_visitor_source|mtef_team_0, 0, aif_start_alarmed, 1,[]),
    ],
    [
		common_inventory_not_available,
		fate_aoe_poison,
		common_fate_damage_system,
		fate_servant_agent_effects,
		fate_servant_death_handling, 
		fate_servant_combat_system, 
		fate_combat_magic_system,
		fate_battle_continuation,
		fate_servant_heal_system,
		fate_hud_elements,  
		fate_gae_bolg_test,
		fate_magic_casts,
		fate_magic_select,
		fate_elemental_visualization,
		fate_charge_attack,
		fate_charge_attack_effects,
		charged_shot,
		fate_noble_phantasm_activate,
		double_jump,
		fate_servant_skill_select,
		fate_nested_menus,
		 
      
	  (ti_tab_pressed, 0, 0, [(display_message, "str_cannot_leave_now")], []),
	  
	  # (0, 0, 0, [(key_clicked, key_k)], 
	  # [
		# (start_presentation, "prsnt_fate_marble_test"),
	  # ]),
	
		(ti_before_mission_start, 0, 0, [],
       [
		(set_fixed_point_multiplier, 100),
		(store_current_scene, ":cur_scene"),
		(try_begin),
			(eq, ":cur_scene", "scn_fate_ryuudou_temple_2v2"),
			(store_random_in_range, ":time", 20, 24),
			(set_startup_sun_light, 5, 6, 8),
			(set_startup_ambient_light, 10, 12, 16),
			#(set_startup_sky_light, 1, 1, 1),
			(set_startup_ground_ambient_light, 0, 0, 0),
		(else_try),
			(eq, ":cur_scene", "scn_fate_final_destination_2v2"),
			(assign, ":time", 12),
			(set_startup_sun_light, 15, 12, 10),
			(set_startup_ambient_light, 40, 40, 40),
		(else_try),
			(store_random_in_range, ":time", 12, 19),
			(set_startup_ambient_light, 40, 40, 40),
		(try_end),
		
		(scene_set_day_time, ":time"),
		
       ]),
	   
	   # (ti_before_mission_start, 0, 0, [], 
	   # [
	   # (set_fixed_point_multiplier, 100),
		# # (store_random_in_range, ":precip", 0, 4),
		# (assign, ":precip", 1),
		 # (store_random_in_range, ":precip_strength", 0, 101),
		 # (set_rain, ":precip", ":precip_strength"),
		 # #(store_div, ":fog", ":precip_strength", 10),
		 # #(set_fog_distance, ":fog"),
		 # (eq, ":precip", 1),
		 # (convert_to_fixed_point, ":precip_strength"),
		 # (val_div, ":precip_strength", 100),
		 # (convert_from_fixed_point, ":precip_strength"),
		 # (set_shader_param_float, "@fRainSpecular", ":precip_strength"),
		 ## (set_shader_param_int, "@fCursorVisible", 0),
	   # ]),
	   
	   (ti_on_agent_spawn, 0, 0, [],
       [
		(store_trigger_param, ":agent", 1),
		
        #(try_for_agents, ":agent"),
			(agent_get_troop_id, ":troop", ":agent"),
			
			(try_begin),
				(eq, ":troop", "$g_fate_battle_master_1"),
				(assign, "$g_fate_battle_master_1_agent_id", ":agent"),
			(try_end),
			
			(try_begin),
				(eq, ":troop", "$g_fate_battle_master_2"),
				(assign, "$g_fate_battle_master_2_agent_id", ":agent"),
			(try_end),
			
			(try_begin),
				(eq, ":troop", "$g_fate_battle_servant_1"),
				(assign, "$g_fate_battle_servant_1_agent_id", ":agent"),
			(try_end),
			
			(try_begin),
				(eq, ":troop", "$g_fate_battle_servant_2"),
				(assign, "$g_fate_battle_servant_2_agent_id", ":agent"),
			(try_end),
		#(try_end),
		
			(troop_get_slot, ":max_mana", ":troop", slot_fate_max_mana),
			
			(agent_set_slot, ":agent", fate_agnt_max_mana, ":max_mana"),
			(agent_set_slot, ":agent", fate_agnt_cur_mana, ":max_mana"),
	
       ]),
	   
	   (ti_after_mission_start, 0, 0, [],
       [
	   (reset_mission_timer_a),
	   
		(try_for_prop_instances, ":marble", "spr_reality_marble"),
			(prop_instance_enable_physics, ":marble", 0),
			(scene_prop_set_visibility, ":marble", 0),
		(try_end),
		
		(rebuild_shadow_map),
		#(assign, "$g_fate_player_flying", 0),
       ]),
	   
	   (ti_before_mission_start, 0, 0, [],
	   [
	   (call_script, "script_game_quick_start"),
	   ]),

		(0.5, 0, 0, [], 
		[
			# (set_fixed_point_multiplier, 1000),
			(try_for_range, ":type", "spr_shader_sphere", "spr_fate_yew_tree"),
				(neg|is_edit_mode_enabled),
				(scene_prop_get_instance, ":props", ":type", 0),
				(prop_instance_get_position, pos10, ":props"),
				#(store_div, ":tenth", 1, 10),
				(position_rotate_z, pos10, 90),
				#(prop_instance_set_position, ":props", pos10),
				(prop_instance_animate_to_position, ":props", pos10, 500),
			(try_end),
			
			(try_begin),
				(scene_prop_get_instance, ":props", "spr_marble_cloudy", 0),
				(prop_instance_get_position, pos10, ":props"),
				#(store_div, ":tenth", 1, 10),
				(position_rotate_z, pos10, 90),
				#(prop_instance_set_position, ":props", pos10),
				(prop_instance_animate_to_position, ":props", pos10, 500),
			(try_end),
			
			(try_for_agents, ":agent"),
			(agent_is_alive, ":agent"), 
			
			
				(try_begin),
					(agent_slot_ge, ":agent", fate_agent_poison_build_up, 100),
					# (agent_get_slot, ":build_up", ":agent", fate_agent_poison_build_up),
					(agent_get_slot, ":poisoned", ":agent", fate_agent_poison_damage),
					(val_add, ":poisoned", 1),
					(agent_set_slot, ":agent", fate_agent_poison_build_up, 0),
					(agent_set_slot, ":agent", fate_agent_poison_damage, ":poisoned"),
				(try_end),
				
				(try_begin),
					(agent_slot_ge, ":agent", fate_agent_poison_damage, 1),
					
					(agent_ai_get_cached_enemy, ":target", ":agent", 0),
					(gt, ":target", 0),
					
					(agent_get_slot, ":poisoned", ":agent", fate_agent_poison_damage),
					(val_mul, ":poisoned", 10),
					(agent_deliver_damage_to_agent, ":target", ":agent", ":poisoned", "itm_poison"),
				(try_end),
			(try_end),
		]),

		(ti_on_item_wielded, 0, 0, [], 
		[
		(set_fixed_point_multiplier, 100),
		(store_trigger_param_1, ":agent"),
		(store_trigger_param_2, ":item"),
		
		(gt, ":agent", 0),
		(gt, ":item", 0),
		
		(agent_get_troop_id, ":troop", ":agent"),
		
		
		(try_begin),
			(eq, ":troop", "trp_lancer_02"),
			(try_begin),
				(eq, ":item", "itm_gae_dearg"),
				(agent_unequip_item, ":agent", "itm_gae_dearg_offhand"),
				(agent_equip_item, ":agent", "itm_gae_buidhe_offhand"),
				(agent_set_wielded_item, ":agent", "itm_gae_buidhe_offhand"),
			(else_try),
				(eq, ":item", "itm_gae_buidhe"),
				(agent_unequip_item, ":agent", "itm_gae_buidhe_offhand"),
				(agent_equip_item, ":agent", "itm_gae_dearg_offhand"),
				(agent_set_wielded_item, ":agent", "itm_gae_dearg_offhand"),
			(try_end),
		(else_try),
			(eq, ":troop", "trp_archer_01"),
			
			(try_begin),
				(eq, ":item", "itm_emiya_bow"),
				(agent_unequip_item, ":agent", "itm_kanshou"),
				(agent_unequip_item, ":agent", "itm_kanshou_melee"),
				(agent_unequip_item, ":agent", "itm_bakuya"),
				(agent_equip_item, ":agent", "itm_emiya_arrows"),
				(agent_set_wielded_item, ":agent", "itm_emiya_arrows"),
				# (agent_get_wielded_item, ":right", ":agent", 0),
				
				
				
				(troop_set_slot, ":troop", fate_magic_projection_item, "itm_kanshou"),
			(else_try),
				(eq, ":item", "itm_caliburn"),
				
				(agent_unequip_item, ":agent", "itm_kanshou"),
				(agent_unequip_item, ":agent", "itm_kanshou_melee"),
				(agent_unequip_item, ":agent", "itm_bakuya"),
			(else_try),
				(this_or_next|eq, ":item", "itm_kanshou"),
				(eq, ":item", "itm_kanshou_melee"),
				
				(agent_unequip_item, ":agent", "itm_emiya_arrows"),
				
				(troop_set_slot, ":troop", fate_magic_projection_item, "itm_emiya_bow"),

				(agent_get_ammo, ":ammo", ":agent", 1),
				(try_begin),
					(gt, ":ammo", 2),
					(agent_equip_item, ":agent", "itm_bakuya"),
					(agent_set_wielded_item, ":agent", "itm_bakuya"),
				(else_try),
					(agent_unequip_item, ":agent", "itm_bakuya"),
				(try_end),
			# (else_try),
				# (eq, ":item", "itm_caliburn"),
				# (agent_unequip_item, ":agent", "itm_bakuya"),
				# (agent_unequip_item, ":agent", "itm_kanshou_melee"),
				# (agent_unequip_item, ":agent", "itm_kanshou"),
			(try_end),
		(try_end),
		]),
		
		(1, 0, 0, [	(neg|num_active_teams_le, 1),
		], 
		[
			(try_for_range, ":item", weapons_begin, servant_catalysts_begin),
				(item_slot_ge, ":item", fate_weapon_activated, 1),
				(item_get_slot, ":activation_time", ":item", fate_weapon_activated),
				(val_sub, ":activation_time", 1),
				(item_set_slot, ":item", fate_weapon_activated, ":activation_time"),
			(try_end),
			
			(try_for_agents, ":agent"),
				
				(agent_is_human, ":agent"),
				(agent_is_alive, ":agent"),
				(agent_get_position, pos31, ":agent"),
				(agent_get_troop_id, ":troop", ":agent"),
				
				# Animation Block
				
				(agent_get_animation, ":animation", ":agent", 0),
				(store_agent_hit_points, ":hp", ":agent", 0),
				
				(troop_get_slot, ":max_mana", ":troop", slot_fate_max_mana),
				(troop_get_slot, ":cur_mana", ":troop", slot_fate_cur_mana),
				
				(store_div, ":tenth_mana", ":max_mana", 10),
				
				# This allows them to die if their master has died first
				# I do want to change it such that their mana is drained, and if
				# they are withing the bottom 10% of their mana, and their
				# master is dead, then the damage occurs.
				(assign, ":fadeaway", 25),
				(assign, ":ia_level", 10),
				
				(try_begin),
					(eq, ":agent", "$g_fate_battle_servant_1_agent_id"),
					(neq, "$g_fate_battle_masters_on", 0),
					
					(neg|agent_is_alive, "$g_fate_battle_master_1_agent_id"),
					
					(try_for_range, ":bone", 0, 19),
						(agent_get_bone_position, pos15, ":agent", ":bone", 1),
						(particle_system_burst, "psys_invisible_air", pos15, 5),
					(try_end),					
					
					(try_for_range, ":slots", fate_skill_slot, fate_skill_slot_08_level),
						(troop_slot_eq, ":troop", ":slots", "str_skill_independent_action"),
						(val_add, ":slots", 1),
						(troop_get_slot, ":ia_level", ":troop", ":slots"),	
					(try_end),
					
					# (val_sub, ":hp", ":ia_level"),
					# (val_max, ":hp", 0),
					# (agent_set_hit_points, ":agent", ":hp", 0),
					# (eq, ":hp", 0),
					# (str_store_agent_name, s10, ":agent"),
					# (display_message, "@{s10} has faded without their Master"),
					# (set_show_messages, 0),
					# (assign, "$g_fate_messages_disabled", 1),
					# (agent_deliver_damage_to_agent, ":agent", ":agent", 100),
					(store_mul, ":sub", ":fadeaway", ":ia_level"),
					
					(try_begin),
						(gt, ":cur_mana", ":tenth_mana"),
						(val_sub, ":cur_mana", ":sub"),
						(val_max, ":cur_mana", ":tenth_mana"),
					(try_end),
					
					(troop_set_slot, ":troop", slot_fate_cur_mana, ":cur_mana"),
					
					(le, ":cur_mana", ":tenth_mana"),
					
					(val_sub, ":hp", ":ia_level"),
					(val_max, ":hp", 0),
					(agent_set_hit_points, ":agent", ":hp", 0),
					(eq, ":hp", 0),
					(troop_set_slot, ":troop", slot_fate_cur_battle_continuations, 0), 
					(str_store_agent_name, s10, ":agent"),
					(display_message, "@{s10} has faded without their Master", 0xcc9911),
					#(agent_fade_out, ":agent"),
					(set_show_messages, 0),
					(assign, "$g_fate_messages_disabled", 1),
					(agent_deliver_damage_to_agent, ":agent", ":agent", 100),
					
				(else_try),
					(eq, ":agent", "$g_fate_battle_servant_2_agent_id"),
					(neq, "$g_fate_battle_masters_on", 0),
					
					(neg|agent_is_alive, "$g_fate_battle_master_2_agent_id"),
					
					(try_for_range, ":slots", fate_skill_slot, fate_skill_slot_08_level),
						(troop_slot_eq, ":troop", ":slots", "str_skill_independent_action"),
						(val_add, ":slots", 1),
						(troop_get_slot, ":ia_level", ":troop", ":slots"),	
					(try_end),
					
					(store_mul, ":sub", ":fadeaway", ":ia_level"),
					
					(try_begin),
						(gt, ":cur_mana", ":tenth_mana"),
						(val_sub, ":cur_mana", ":sub"),
						(val_max, ":cur_mana", ":tenth_mana"),
					(try_end),
					
					(troop_set_slot, ":troop", slot_fate_cur_mana, ":cur_mana"),
					
					(le, ":cur_mana", ":tenth_mana"),
					
					(val_sub, ":hp", ":ia_level"),
					(val_max, ":hp", 0),
					(agent_set_hit_points, ":agent", ":hp", 0),
					(eq, ":hp", 0),
					(troop_set_slot, ":troop", slot_fate_cur_battle_continuations, 0), 
					(str_store_agent_name, s10, ":agent"),
					(display_message, "@{s10} has faded without their Master", 0xcc9911),
					#(agent_fade_out, ":agent"),
					(set_show_messages, 0),
					(assign, "$g_fate_messages_disabled", 1),
					(agent_deliver_damage_to_agent, ":agent", ":agent", 100),
				(try_end),
								
				(try_begin),
					(agent_slot_ge, ":agent", fate_agent_petrified, 1),
					(neg|is_between, ":animation", "anim_petrify_1", "anim_petrify_break"),
					(agent_set_animation, ":agent", "anim_petrify_1"),
				(else_try),
					(agent_slot_eq, ":agent", fate_agent_petrified, 0),
					(is_between, ":animation", "anim_petrify_1", "anim_petrify_break"),
					(agent_set_animation, ":agent", "anim_petrify_break"),
				(try_end),
				
				(try_begin),
					(agent_slot_ge, ":agent", fate_agent_frozen, 1),
					(neg|is_between, ":animation", "anim_frozen_1", "anim_frozen_break"),
					(agent_set_animation, ":agent", "anim_frozen_1"),
				(else_try),
					(agent_slot_eq, ":agent", fate_agent_frozen, 0),
					(is_between, ":animation", "anim_frozen_1", "anim_frozen_break"),
					(agent_set_animation, ":agent", "anim_frozen_break"),
				(try_end),
				
				(try_begin),
					(agent_slot_ge, ":agent", fate_agent_confused, 1),
					(store_random_in_range, ":chance", 1, 11),
					(gt, ":chance", 5),
					(agent_set_animation, ":agent", "anim_confused"),
				(try_end),
				
				(try_begin),
					(agent_slot_ge, ":agent", fate_agent_enraged, 1),
					(store_random_in_range, ":chance", 0, 11),
					(gt, ":chance", 5),
					(agent_set_animation, ":agent", "anim_enraged"),
				(try_end),
				
				(try_begin),
					(agent_slot_ge, ":agent", fate_agent_ensnared, 1),
					(neg|is_between, ":animation", "anim_ensnared_1", "anim_ensnared_break"),
					(agent_set_animation, ":agent", "anim_ensnared_1"),
				(else_try),
					(agent_slot_eq, ":agent", fate_agent_ensnared, 0),
					(is_between, ":animation", "anim_ensnared_1", "anim_ensnared_break"),
					(agent_set_animation, ":agent", "anim_ensnared_break"),
				(try_end),
				
				(try_begin),
					(agent_slot_ge, ":agent", fate_agent_burned, 1),
					(try_for_range, ":bone", 0, 19),
						(agent_get_bone_position, pos15, ":agent", ":bone", 1),
						(particle_system_burst, "psys_fireplace_fire_small", pos15, 5),
					(try_end),
					
					(agent_ai_get_cached_enemy, ":target", ":agent", 0),
					(gt, ":target", 0),
					
					(agent_deliver_damage_to_agent, ":target", ":agent", 15, "itm_firebolt"),
				(try_end),
				
				# Injury Effects and Recovery
				(assign, ":lslowdown", 0),
				(assign, ":rslowdown", 0),
				
				(agent_get_slot, ":orig_speed", ":agent", slot_agent_original_speed),
				(agent_get_speed_modifier, ":speed", ":agent"),
				(val_max, ":speed", ":orig_speed"),
				
				(agent_get_slot, ":orig_accuracy", ":agent", slot_agent_original_accuracy),
				(agent_get_accuracy_modifier, ":accuracy", ":agent"),
				(val_max, ":accuracy", ":orig_accuracy"),
				
				(try_for_range, ":injury_slot", fate_agent_wounds_larm, fate_agent_dodges),
					(agent_slot_ge, ":agent", ":injury_slot", 1),
					(agent_get_slot, ":injury_level", ":agent", ":injury_slot"),
					
					(agent_get_wielded_item, ":left_item", ":agent", 1),
					(agent_get_wielded_item, ":right_item", ":agent", 0),
					
					(try_begin),
						(gt, ":right_item", 0),
						(item_get_type, ":right_item_type", ":right_item"),
					(try_end),
					
					(try_begin),
						(gt, ":injury_level", 10),
					
						
						
						(try_begin),
							(eq, ":injury_slot", fate_agent_wounds_larm),
							(agent_get_bone_position, pos20, ":agent", 13, 1),
						(else_try),
							(eq, ":injury_slot", fate_agent_wounds_rarm),
							(agent_get_bone_position, pos20, ":agent", 17, 1),
						(else_try),
							(eq, ":injury_slot", fate_agent_wounds_lleg),
							(agent_get_bone_position, pos20, ":agent", 2, 1),
							(store_div, ":lslowdown", ":injury_level", 2),
							(ge, ":injury_level", 85),
							(store_div, ":lslowdown", ":speed", 2),
						(else_try),
							(eq, ":injury_slot", fate_agent_wounds_rleg),
							(agent_get_bone_position, pos20, ":agent", 5, 1),
							(store_div, ":rslowdown", ":injury_level", 2),
							(ge, ":injury_level", 85),
							(store_div, ":rslowdown", ":speed", 2),
						(else_try),
							(eq, ":injury_slot", fate_agent_wounds_chest),
							(agent_get_bone_position, pos20, ":agent", 8, 1),
						(else_try),
							(eq, ":injury_slot", fate_agent_wounds_head),
							(agent_get_bone_position, pos20, ":agent", 9, 1),
							(assign, ":accuracyloss", ":injury_level"),
						(try_end),
						
						(position_set_z_to_ground_level, pos31),
						# (position_move_z, pos31, 5, 1),
						(set_spawn_position, pos31),
						
						(try_begin),
							(ge, ":injury_level", 99),
							(try_begin),
								(eq, ":injury_slot", fate_agent_wounds_larm),
								(try_begin),
									(gt, ":left_item", 0),
									(agent_unequip_item, ":agent", ":left_item"),
									(spawn_item, ":left_item", 0, 1000),
								(try_end),
								
								(try_begin),
									(gt, ":right_item", 0),
									(eq, ":left_item", -1),
									(this_or_next|eq, ":right_item_type", itp_type_two_handed_wpn),
									(this_or_next|eq, ":right_item_type", itp_type_bow),
									(eq, ":right_item_type", itp_type_polearm),
									(agent_unequip_item, ":agent", ":right_item"),
									(spawn_item, ":right_item", 0, 1000),
								(try_end),
							(else_try),
								(eq, ":injury_slot", fate_agent_wounds_rarm),
								(gt, ":right_item", 0),
								(agent_unequip_item, ":agent", ":right_item"),
								(spawn_item, ":right_item", 0, 1000),
							(else_try),
								(eq, ":injury_slot", fate_agent_wounds_lleg),
								
							(else_try),
								(eq, ":injury_slot", fate_agent_wounds_rleg),
								
							(else_try),
								(eq, ":injury_slot", fate_agent_wounds_chest),
								
							(else_try),
								(eq, ":injury_slot", fate_agent_wounds_head),
							(try_end),
						(try_end),
						
						(particle_system_burst, "psys_impact_burst", pos20, ":injury_level"),
					(try_end),
					
					
					(this_or_next|lt, ":injury_level", 41),	# If 40% or Below, Heal
					(is_between, ":injury_level", 42, 61), # If between 42 and 60 Heal
					(val_sub, ":injury_level", 1),
					(val_clamp, ":injury_level", 0, 100),
					(agent_set_slot, ":agent", ":injury_slot", ":injury_level"),
				(try_end),
				
				(store_add, ":slowdown", ":lslowdown", ":rslowdown"),
				(val_sub, ":speed", ":slowdown"),
				(val_max, ":speed", 0),
				(agent_set_speed_modifier, ":agent", ":speed"),
				
				(val_sub, ":accuracy", ":accuracyloss"),
				(val_max, ":accuracy", 0),
				(agent_set_accuracy_modifier, ":agent", ":accuracy"),
				
				# Debuff Recovery
				
				(try_for_range, ":debuff_slot", fate_agent_petrified, fate_agent_armor_debuff + 1),
					(agent_slot_ge, ":agent", ":debuff_slot", 1),
					(agent_get_slot, ":debuff_time", ":agent", ":debuff_slot"),
					(val_sub, ":debuff_time", 1),
					(val_max, ":debuff_time", 0),
					(agent_set_slot, ":agent", ":debuff_slot", ":debuff_time"),
				(try_end),
				
				# Armor and Magic Res Boost
								
				(try_begin),
					(agent_get_slot, ":armor_time", ":agent", slot_agent_armor_boost_timer),
					(agent_get_slot, ":magic_res_time", ":agent", slot_agent_magic_res_boost_timer),
					(agent_get_slot, ":np_time", ":agent", slot_agent_active_np_timer),
					(agent_get_slot, ":invul_time", ":agent", slot_agent_invulnerability_timer),
					(agent_get_slot, ":equip_time", ":agent", slot_agent_reset_equip_timer),
					
					(val_sub, ":armor_time", 1),
					(val_sub, ":magic_res_time", 1),
					(val_sub, ":np_time", 1),
					(val_sub, ":invul_time", 1),
					(val_sub, ":equip_time", 1),
					
					(val_max, ":armor_time", 0),
					(val_max, ":magic_res_time", 0),
					(val_max, ":np_time", 0),
					(val_max, ":invul_time", 0),
					(val_max, ":equip_time", 0),
					
					(try_begin),
						(eq, ":np_time", 0),
						(agent_slot_ge, ":agent", slot_agent_active_noble_phantasm, 0),
						(agent_set_slot, ":agent", slot_agent_active_noble_phantasm, 0),
					(try_end),
					
					(agent_set_slot, ":agent", slot_agent_armor_boost_timer, ":armor_time"),
					(agent_set_slot, ":agent", slot_agent_magic_res_boost_timer, ":magic_res_time"),
					(agent_set_slot, ":agent", slot_agent_active_np_timer, ":np_time"),
					(agent_set_slot, ":agent", slot_agent_invulnerability_timer, ":invul_time"),
					(agent_set_slot, ":agent", slot_agent_reset_equip_timer, ":equip_time"),
				(try_end),
				
				(try_begin),
					(agent_slot_eq, ":agent", slot_agent_reset_equip_timer, 1),
					(try_for_range, ":slot", 0, 8),
						(agent_get_item_slot, ":item", ":agent", ":slot"),
						
						(troop_get_inventory_slot, ":newitem", ":troop", ":slot"),
						
						(try_begin),
							(gt, ":newitem", 0),
							(neq, ":newitem", ":item"),
							
							(try_begin),
								(gt, ":item", 0),
								(agent_unequip_item, ":agent", ":item"),
							(try_end),
							
							(agent_equip_item, ":agent", ":newitem"),
						(try_end),
					(try_end),
				(try_end),
			(try_end),
			
			# (try_for_prop_instances, ":p_instance"),
				
			# (try_end),
		]),
		
		# (ti_on_scene_prop_init, 0, 0, [],
		# [
			# (store_trigger_param_1, ":instance"),

			# (store_mission_timer_a, ":time"), # Time in seconds

			# (scene_prop_set_slot, ":instance", fate_prop_spawn_time, ":time"),
			# (assign, reg15, ":time"),
			# (display_message, "@{reg15}"),
		# ]),
		
		(0.1, 0, 0, [], 
		  [
			(call_script, "script_fate_battle_spectate"),
			(call_script, "script_update_fate_hud"),
			
			(set_fixed_point_multiplier, 100),
			(store_mission_timer_a, ":cur_time"),
						
			(try_for_prop_instances, ":instance", "spr_aestus"),
				
				
				(prop_instance_is_valid, ":instance"),
				(scene_prop_get_slot, ":spawntime", ":instance", fate_prop_spawn_time),
				
				(gt, ":spawntime", 0),
								
				(val_add, ":spawntime", 15),
				
				(eq, ":cur_time", ":spawntime"),
				
				(prop_instance_get_position, pos10, ":instance"),
				(position_move_z, pos10, -2500, 1),
								
				(scene_prop_set_prune_time, ":instance", 0),
				(prop_instance_animate_to_position, ":instance", pos10, 500),
				
				# (neg|prop_instance_is_animating, ":instance"),
						
				# (try_for_prop_instances, ":prop"),
					 # (prop_instance_get_position, pos12, ":prop"),
					 # (get_distance_between_positions, ":dist", pos12, pos10),
					 # (le, ":dist", 7000),
					 # (position_move_z, pos12, 1500),
					 # (prop_instance_animate_to_position, ":prop", pos12, 500),
					 # # (scene_prop_set_visibility, ":prop", 0),
					 # # (scene_prop_fade_out, ":prop", 1),
				# (try_end),
				
				(try_for_prop_instances, ":prop"),
					(neq, ":prop", ":instance"),
					
					(prop_instance_is_animating, ":is_animating", ":prop"),
					
					
					(eq, ":is_animating", 0),
					
					#(scene_prop_get_slot, ":o_pos", ":prop", fate_prop_original_pos),
					
					# (scene_prop_get_slot, ":o_pos_x", ":prop", fate_prop_original_pos_x),
					# (scene_prop_get_slot, ":o_pos_y", ":prop", fate_prop_original_pos_y),
					# (scene_prop_get_slot, ":o_pos_z", ":prop", fate_prop_original_pos_z),
					# (scene_prop_get_slot, ":o_pos_rot", ":prop", fate_prop_original_pos_rot),
					
					(scene_prop_get_slot, ":o_pos_x", ":prop", fate_prop_original_pos_x),
					(scene_prop_get_slot, ":o_pos_y", ":prop", fate_prop_original_pos_y),
					(scene_prop_get_slot, ":o_pos_z", ":prop", fate_prop_original_pos_z),
					(scene_prop_get_slot, ":o_pos_rot", ":prop", fate_prop_original_pos_rot),
					
					(position_set_x, pos11, ":o_pos_x"),
					(position_set_y, pos11, ":o_pos_y"),
					(position_set_z, pos11, ":o_pos_z"),
					(position_rotate_z, pos11, ":o_pos_rot", 1),
					
					(prop_instance_get_position, pos12, ":prop"),
					
					(get_distance_between_positions, ":dist", pos12, pos10),
					(get_distance_between_positions, ":same", pos12, pos11),
					
					(le, ":dist", 2300), # Keep it within a certain radius.
					
					(ge, ":same", 100), # Not Within a meter of original position
					
					# (neq, ":o_pos", pos12),
					
					(prop_instance_enable_physics, ":prop", 1),
					(prop_instance_animate_to_position, ":prop", pos11, 500),
				(try_end),
			(try_end),
		  ]),
		  
		# (0.25, 0, 0, [], 
		# [
			# (set_fixed_point_multiplier, 100),
			# (get_player_agent_no, ":p_agent"),
			
			# (agent_get_speed, ":p_agent", pos2),
			
			# (position_get_x, reg1, pos2),
			# (position_get_y, reg2, pos2),
			
			# (display_message, "@Player Speed: ({reg1}, {reg2})")
			
		# ]),
		
		# TO DO !
		# Set a ti_once trigger that launches at the start of the match to store every prop's original pos!
		# When a Reality Marble is in play, the props move down under ground and are replaced, after 15 seconds
		# They are animated back to their original pos.
		
		(ti_after_mission_start, 0, ti_once, [],
		[
			#(display_message, "@ Scene Prop Initialization Complete", 0xdd00dd),
			(set_fixed_point_multiplier, 100),
			
			(assign, reg30, 0),
			
			(try_for_prop_instances, ":instance"),
				# (prop_instance_is_valid, ":instance"),
				(prop_instance_get_position, pos10, ":instance"),
				
				(position_get_rotation_around_z, ":o_pos_rot", pos10),
				
				(position_get_x, ":o_pos_x", pos10),
				(position_get_y, ":o_pos_y", pos10),
				(position_get_z, ":o_pos_z", pos10),
				
				(scene_prop_set_slot, ":instance", fate_prop_original_pos_x, ":o_pos_x"),
				(scene_prop_set_slot, ":instance", fate_prop_original_pos_y, ":o_pos_y"),
				(scene_prop_set_slot, ":instance", fate_prop_original_pos_z, ":o_pos_z"),
				(scene_prop_set_slot, ":instance", fate_prop_original_pos_rot, ":o_pos_rot"),
				
				(val_add, reg30, 1),
			(try_end),
			
			# (display_message, "@ Scene Prop Initialization Complete {reg30} props registered", 0xdd00dd),
		]),
		
		(0.0, 0, 0, [], 
		[
			(set_fixed_point_multiplier, 100),
			
			#(try_for_prop_instances, ":instance", "spr_aestus"),
				(scene_prop_get_instance, ":instance", "spr_aestus", 0),
				# (prop_instance_get_position, pos10, ":props"),
			
				(gt, ":instance", -1),
				#(display_message, "@Aestus is detected!"),
				(prop_instance_get_position, pos10, ":instance"),
				(scene_prop_get_slot, ":owner_agent", ":instance", fate_prop_owner),
				
				# fate_prop_original_pos_x = 7
				# fate_prop_original_pos_y = 8
				# fate_prop_original_pos_z = 9

				# fate_prop_original_pos_rot = 10
				
				(scene_prop_get_slot, ":o_pos_x", ":instance", fate_prop_original_pos_x),
				(scene_prop_get_slot, ":o_pos_y", ":instance", fate_prop_original_pos_y),
				(scene_prop_get_slot, ":o_pos_z", ":instance", fate_prop_original_pos_z),
				(scene_prop_get_slot, ":o_pos_rot", ":instance", fate_prop_original_pos_rot),
				
				(init_position, pos11),
				
				(position_set_x, pos11, ":o_pos_x"),
				(position_set_y, pos11, ":o_pos_y"),
				(position_set_z, pos11, ":o_pos_z"),
				(position_rotate_z, pos11, ":o_pos_rot"),
				# (eq, ":o_pos", pos10),
				# (scene_prop_get_slot, ":spawntime", ":instance", fate_prop_spawn_time),
				
				# (store_mission_timer_a, ":cur_time"),
				
				(position_get_z, ":z", pos10),
							
				(try_begin),
					(ge, ":z", 0),
					(particle_system_burst, "psys_aestus_area", pos10, 5),
				(try_end),
				
				
				(try_for_agents, ":agent", pos10, 2400),
					(agent_is_alive, ":agent"),
					(agent_is_human, ":agent"),
					# (agent_get_position, pos7, ":agent"),
				
					(try_begin),
						(eq, ":agent", ":owner_agent"),
						
						(agent_get_slot, ":cur_mana", ":agent", fate_agnt_cur_mana),
						(agent_get_slot, ":max_mana", ":agent", fate_agnt_max_mana),
						(store_agent_hit_points, ":cur_health", ":agent"),
						
						
						# (try_begin),
							# (agent_get_wielded_item, ":equipped_item", ":agent", 0),
							# (gt, ":equipped_item", 0),
							# (item_get_weapon_length, ":length", ":equipped_item"),
							# (val_div, ":length", 10),
							# (val_add, ":length", 1),
							# (agent_get_bone_position, pos25, ":agent", 19, 1),
							# (try_for_range, ":offset", 0, ":length"),
								# (position_move_y, pos25, ":offset"),
								# (particle_system_burst, "psys_fireplace_fire_small", pos25, 1),
							# (try_end),
						# (try_end),
						
						(val_add, ":cur_health", 1),
						(val_add, ":cur_mana", 200),
						
						(val_min, ":cur_mana", ":max_mana"),
						(val_min, ":cur_health", 100),
						
						(agent_set_hit_points, ":agent", ":cur_health"),
						(agent_set_slot, ":agent", fate_agnt_cur_mana, ":cur_mana"),
						# (particle_system_burst, "psys_aestus_rose", pos7, 1),
						# (display_message, "@ Am I firing?"),
						
						
					(else_try),
						(agent_set_slot, ":agent", fate_agent_weakened, 1),
						(agent_set_slot, ":agent", fate_agent_silenced, 1),
					(try_end),
				(try_end),
			# (try_end),
		]),

      (1, 4, ti_once, [
		(this_or_next|eq, "$g_fate_battle_servants_on", 1),
		(eq, "$g_fate_battle_masters_on", 1),
		
		(num_active_teams_le, 1)],
       [
           (finish_mission, 1),
       ]),
	   
	  (ti_on_item_wielded, 0, 0, [], [
		(store_trigger_param_1, ":agent"),
		(store_trigger_param_2, ":item"),
		
		#(store_add, ":next_item", ":item", 1),
		(store_sub, ":previous_item", ":item", 1),

		#(agent_get_animation, ":anim", ":servant", 1),
		(gt, ":agent", 0),
		(gt, ":item", 0),

		(this_or_next|item_has_property, ":item", itp_next_item_as_melee),
		(item_has_property, ":previous_item", itp_next_item_as_melee),
		
		(agent_set_animation_progress, ":agent", 1000000),
		
	   ] ),
	   
	   (ti_on_item_unwielded, 0, 0, [], [
		(store_trigger_param_1, ":agent"),
		(store_trigger_param_2, ":item"),
		(agent_get_wielded_item, ":equipped_item", ":agent", 0),
		
		#(agent_get_animation, ":anim", ":servant", 1),
		(gt, ":agent", 0),
		(gt, ":item", 0),

		(this_or_next|eq, ":item", "itm_kanshou"),
		(eq, ":item", "itm_kanshou_melee"),
		
		(this_or_next|neq, ":equipped_item", "itm_kanshou"),
		(neq, ":equipped_item", "itm_kanshou_melee"),
		
		(agent_unequip_item, ":agent", "itm_bakuya"),
		
		
		(agent_set_animation_progress, ":agent", 1000000),
		
	   ] ),
	   
	   (ti_on_item_dropped, 0, 0, [], [
		(store_trigger_param_1, ":agent"),
		(store_trigger_param_2, ":item"),
		(agent_get_wielded_item, ":equipped_item", ":agent", 0),
		
		#(agent_get_animation, ":anim", ":servant", 1),
		(gt, ":agent", 0),
		(gt, ":item", 0),

		(this_or_next|eq, ":item", "itm_kanshou"),
		(eq, ":item", "itm_kanshou_melee"),
		
		(this_or_next|neq, ":equipped_item", "itm_kanshou"),
		(neq, ":equipped_item", "itm_kanshou_melee"),
		
		(agent_unequip_item, ":agent", "itm_bakuya"),
		
		
		(agent_set_animation_progress, ":agent", 1000000),
		
	   ] ),
	   
	   (0,0,0,[(eq, "$g_fate_messages_disabled", 1),],[(set_show_messages, 1),]),
	   (1,0,0,[],
	   [
		(set_fixed_point_multiplier, 100),
		#(get_player_agent_no, ":agent"),
		
		(try_for_prop_instances, ":instance", "spr_trigger"),
			#(prop_instance_is_valid, ":instance"),
			#(prop_instance_get_position, pos1, ":instance"),
			(try_for_agents, ":agent"),
				(str_store_agent_name, s10, ":agent"),
				(scene_prop_has_agent_on_it, ":instance", ":agent"),
				(prop_instance_get_variation_id, reg1, ":instance"),
				(prop_instance_get_variation_id, reg2, ":instance"),
				(display_message, "@{s10} Detected {reg1} {reg2}"),
			(try_end),
		(try_end),
	   ]),
	   
	   (1, 0, 0, [(store_current_scene, ":scn"),
					(eq, ":scn", "scn_fate_final_destination_2v2")], 
	   [
			(set_fixed_point_multiplier, 100),
			(try_for_agents, ":agent"),
				(agent_is_alive, ":agent"),
				(agent_get_position, pos15, ":agent"),
				(position_get_z, ":altitude", pos15),
				(lt, ":altitude", -1000),
				(agent_set_hit_points, ":agent", 0),
				(str_store_agent_name, s10, ":agent"),
				(display_message, "@{s10} has fallen to their death.", 0xcc9911),
				#(agent_fade_out, ":agent"),
				(set_show_messages, 0),
				(assign, "$g_fate_messages_disabled", 1),
				(agent_deliver_damage_to_agent, ":agent", ":agent", 1000),
			(try_end),
	   ]),
	   
	   (0, 0, 0, [
	    (get_player_agent_no, ":player"),
		(neq, ":player", -1),
		(agent_is_alive, ":player"),
		(agent_slot_eq, ":player", fate_agent_is_flying, 1),
	   ],
	   [
		(set_fixed_point_multiplier, 100),
		(get_player_agent_no, ":agent"),
		(agent_get_position, pos2, ":agent"),
		(copy_position, pos3, pos2),
		
		(try_begin),
			(neg|key_is_down, key_right_mouse_button),
			(try_begin),
				(key_is_down, key_w),
				(position_move_y, pos2, 5),
			(else_try),
				(key_is_down, key_s),
				(position_move_y, pos2, -5),
			(try_end),
		(else_try),
			(try_begin),
				(key_is_down, key_w),
				(position_move_z, pos2, 10),
			(else_try),
				(key_is_down, key_s),
				(position_move_z, pos2, -10),
			(try_end),
		(try_end),
		
		(try_begin),
			(key_is_down, key_a),
			(position_move_x, pos2, -5),
		(else_try),
			(key_is_down, key_d),
			(position_move_x, pos2, 5),
		(try_end),
		
		(position_get_distance_to_ground_level, ":altitude", pos2),
		(try_begin),
			(lt, ":altitude", 150),
			(store_sub, ":change", 150, ":altitude"),
			(position_move_z, pos2, ":change"),
		(else_try),
			(gt, ":altitude", 1500),
			(store_sub, ":change", 1500, ":altitude"),
			(position_move_z, pos2, ":change"),
		(try_end),
	   
		(agent_set_position, ":agent", pos2),
	   
	   ]),
	   
	   (0, 0, 0, [ ], 
	   [
		(try_begin),
			(neg|is_presentation_active, "prsnt_noble_phantasm_selection"),
			(neg|is_presentation_active, "prsnt_fate_magic_select"),
			(neg|is_presentation_active, "prsnt_servant_skill_selection"),
			(neg|is_presentation_active, "prsnt_nested_menus"),
			# (set_shader_param_int, "@fCursorVisible", 1),
		(else_try),
			# (set_shader_param_int, "@fCursorVisible", 0),
		(try_end),
	   ]),
	   
	   (0, 0, ti_once, [(key_clicked, key_k)],
	   [
		(try_for_range, ":item", "itm_excalibur", "itm_items_end"),
		(scene_item_get_num_instances, ":num", ":item"),
		(try_for_range, ":i_num", 0, ":num"),
			(scene_item_get_instance, ":instance", ":item", ":i_num"),
			(prop_instance_get_position, pos1, ":instance"),
			(prop_instance_set_scale, ":instance", 0, 0, 0),
			(set_spawn_position, pos1),
			(spawn_item, ":item", 0),
		(try_end),
		(try_end),
	   ]),
    ]# + fate_triggers,
  ), 
]
