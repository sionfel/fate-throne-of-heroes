from header_common import *
from header_operations import *
from header_parties import *
from header_items import *
from header_skills import *
from header_triggers import *
from header_troops import *
from module_constants import *

# #######################################################################
#	(check_interval, delay_interval, rearm_interval, [conditions], [consequences]),
#
#  Each trigger contains the following fields:
# 1) Check interval: How frequently this trigger will be checked
# 2) Delay interval: Time to wait before applying the consequences of the trigger
#    After its conditions have been evaluated as true.
# 3) Re-arm interval. How much time must pass after applying the consequences of the trigger for the trigger to become active again.
#    You can put the constant ti_once here to make sure that the trigger never becomes active again after it fires once.
# 4) Conditions block (list). This must be a valid operation block. See header_operations.py for reference.
#    Every time the trigger is checked, the conditions block will be executed.
#    If the conditions block returns true, the consequences block will be executed.
#    If the conditions block is empty, it is assumed that it always evaluates to true.
# 5) Consequences block (list). This must be a valid operation block. See header_operations.py for reference. 
# #######################################################################

# #######################################################################
#	Similar to simple_triggers, but capable of a whole lot more.
#	At least I think. I use these, I don't think I've touched simple_triggers in years
# #######################################################################

triggers = [
###################################################################################
####### FATE/ rider saber Triggers ################################################
################### This is where passive noncombat skills are active #############
###################################################################################
#Check skill slots, pull their level, assign bonuses 
############## Golden Rule ######################################################## 
 #Every Two days check if servants in party have Golden Rule and give player gold based
 #on both the perk level as well as their luck!!
 # (1, 6, 48, [],
      # [			
           # (try_for_range, ":servant", servants_begin, servants_end),
			 # (main_party_has_troop, ":servant"),
			 # (str_store_troop_name, s10, ":servant"),
				# (try_for_range, ":perkslot", fate_skill_slot_begin, fate_skill_slot_end),
					# (troop_slot_eq, ":servant", ":perkslot", "str_skill_golden_rule"),
					# (val_add, ":perkslot", 1),
					# (troop_get_slot, reg10, ":servant", ":perkslot"),
					# (troop_get_slot, reg13, ":servant", fate_param_lck),
					# (store_random_in_range, reg11, 900/reg13, 1000),
					# (ge, reg11, 500),
						# (store_div, reg12, reg11, reg10),
						# (call_script, "script_troop_add_gold", "trp_player", reg12),
						# (display_log_message, "@DEBUG: {s10} has rank {reg10} Golden Rule giving you {reg12} gold."),
						# (ge, reg11, 800),
							# (troop_add_items, "trp_player", "itm_velvet", 1),
							# (display_log_message, "@DEBUG: {s10}'s Golden Rule also gave you treasure!"),
				# (try_end),
			# (try_end),
      # ]),
# ################## Battle Continuation Refresh ######################################
 # # once a day check if servants are lacking battle continutaion slot, if so, trigger,
 # # immediately, then wait three days
 # (1, 0, 72, [],
      # [			
		# (try_for_range, ":servant", servants_begin, servants_end),
			# (str_store_troop_name, s10, ":servant"),
			# (troop_get_slot, reg10, ":servant", slot_fate_cur_battle_continuations),
			# (troop_get_slot, reg11, ":servant", slot_fate_max_battle_continuations),
			# (lt, reg10, reg11),
				# (val_add, reg10, 1),
				# (troop_set_slot, ":servant",  slot_fate_max_battle_continuations, reg10),
				# (display_log_message, "@DEBUG: {s10}'s Battle Continuation stock has recharged by one!"),
		# (try_end),
      # ]),
# ##################################################################################
# ########################## Daily Check for War Status ############################
# ############# The average grail war last two weeks, to help enforce this #########
# ############ I'll perform a daily check to see remaining servants and ############
# ########### masters and if over a certain number force an interaction ############
# ############ between two at random. This will appear as a conversation ###########
# ################# for the player party. Obviously won't be called ################
# ########################## during a Moon Cell Grail War ##########################
# ##################################################################################
# one a day, check remaining servants and force interactions
# (1, 0, 0, [
#			(ge, "$g_grail_war_first_day", 0),
#			],
#      [	
		# (assign, ":cur_masters", 0),
		# (store_current_day, ":day"),
		# (assign, ":war_start", "$g_grail_war_first_day"),
		# (store_sub, ":war_length", ":day", "war_start"),
		# (set_fixed_point_multiplier, 100),
		#
		#
		# (try_begin),
		#	(this_or_next|eq, "$g_grail_war", "str_war_type_fuyuki"),
		#	(this_or_next|eq, "$g_grail_war", "str_war_type_tokyo"),
		# 	(assign, ":total_masters", 7),
		# (else_try),
		#	(assign, ":total_masters", 14),
		# (try_end),
		#
		# (try_for_parties, ":party"),
		#	(party_is_active, ":party"),
		#	(party_get_slot, ":classification", ":party", slot_fate_classification),
		#		(eq, ":classification", "str_fate_master"),
		#		(val_add, ":cur_masters", 1),
		# (try_end),
		#
		# (store_div, ":master_ratio", ":cur_masters", ":total_masters"),
		#
		# At the half-way point, if over 50% force an interaction each day
		#
		# (try_begin),
		#	(lt, ":master_ratio", 0.5),
		#	(gt, ":war_length", 7),
		#	(call_script, "script_force_battle"),
		# (try_end),
#      ]),
# ##################################################################################
# ########### This trigger fills remaining Master Slots if the #####################
# ############ Grail War has not yet begun. ########################################
# ##################################################################################
# ##################################################################################
# ##################################################################################
# ##################################################################################
# check if all masters have been assigned, if they have not, assign one.
# use a separate check with ti_once to declare the war started once this
# (1, 0, 0, [(this_or_next|neq, "$g_grail_war", "str_war_type_moon"),
#			(this_or_next|neq, "$g_grail_war", "str_war_type_grand"),
#			(neq, "$g_grail_war_first_day", 0),
#			],
#      [	
		# (assign, ":cur_masters", 0),
		# (set_fixed_point_multiplier, 100),
		#
		#
		# (try_begin),
		#	(this_or_next|eq, "$g_grail_war", "str_war_type_fuyuki"),
		#	(this_or_next|eq, "$g_grail_war", "str_war_type_tokyo"),
		# 	(assign, ":total_masters", 7),
		# (else_try),
		#	(assign, ":total_masters", 14),
		# (try_end),
		#
		#
		#
		# (try_begin),
		#	(lt, ":masters", 0.5),
		#	(call_script, "script_grail_war_start"),
		# (try_end),
#      ]),



#Below should be a script somewhere else
#Servant chance of listening, based on personality match, master skill, relationship, various servant skills etc

    (0, 0, 0, [],
    [
    ]),	
]
