from ID_items import *
from ID_quests import *
from ID_factions import *

# #######################################################################
#	slot = slot_id_number
#	constant = what_the_constant_represents
#
#		These constants are used in various files.
#		If you need to define a value that will be used in those files,
#		just define it here rather than copying it across each file, so
#		that it will be easy to change it if you need to.
# #######################################################################

# #######################################################################
#		Ideally you'll use a different number per slot so as 
#		not to accidentally overwrite previously assigned slots, but 
#		things like items and scenes won't be sharing slot information,
#		so feel free to reset your numbering scheme per category.
#
#		The slots left in are purely for segmentation and categorization
#		efforts are are in no way REQUIRED. They're just to be helpful.
# #######################################################################

# #######################################################################
#	A useful way to think of slots it like an address system for storage.
#
#
#
#
# #######################################################################

# Shader constants

################################################################################
## FATE/ SABER RIDER Constants
################################################################################

servants_begin = "trp_saber"
servants_end = "trp_assassin_100_face"

fate_troops_begin = "trp_saber"
fate_troops_end = "trp_end_troops"

skills_begin = "str_skill_aesthetics_of_the_last_spurt"
skills_end = "str_skill_aotls_desc"

saber_begin = "trp_saber_01"
lancer_begin = "trp_lancer_01"
archer_begin = "trp_archer_01"
rider_begin = "trp_rider_01"
caster_begin = "trp_caster_01"
assassin_begin = "trp_assassin_01"
berserker_begin = "trp_berserker_01"
extra_class_begin = "trp_avenger"
subservants_begin = "trp_assassin_100_face"
master_begin = "trp_master_staynight_01"
magus_begin = master_begin
nonmaster_magus_begin = "trp_summoner"
church_begin = "trp_church_overseer"

saber_end = "trp_lancer"
lancer_end = "trp_archer"
archer_end = "trp_rider"
rider_end = "trp_caster"
caster_end = "trp_assassin"
assassin_end = "trp_berserker"
berserker_end = extra_class_begin
extra_class_end = "trp_assassin_100_face"
subservants_end = master_begin
master_end = "trp_end_masters"
magus_end = church_begin
#church_end =

weapons_begin = "itm_excalibur"

servant_catalysts_begin = "itm_relic_saber"
servant_catalysts_end = "itm_relics_end"

servant_true_names_begin = "str_servant_saber_generic"
servant_summon_speech_begin = "str_saber_generic_speech"

spells_begin = "str_spell_projection"
spells_end = "str_dmg_type_normal"

fate_master_weapons_begin = "itm_azoth_sword"
fate_legend_books_begin = "itm_book_celtic_legends"
fate_master_clothing_begin = "itm_baseball_shirt"

fate_cooking_protein_begin = "itm_protein_salmon"
fate_cooking_veggies_begin = "itm_veggie_carrot"
fate_cooking_carb_begin = "itm_carb_noodles"
fate_cooking_ingredient_begin = "itm_ingredient_butter"
fate_cooking_instant_begin = "itm_instant_coffee"
fate_cooking_premade_begin = "itm_mapo_tofu"

fate_master_weapons_end = fate_legend_books_begin
fate_legend_books_end = fate_master_clothing_begin
fate_master_clothing_end = fate_cooking_protein_begin
fate_cooking_protein_end = fate_cooking_veggies_begin
fate_cooking_veggies_end = fate_cooking_carb_begin
fate_cooking_carb_end = fate_cooking_ingredient_begin
fate_cooking_ingredient_end = fate_cooking_instant_begin
fate_cooking_instant_end = fate_cooking_premade_begin
fate_cooking_premade_end = "itm_items_end"

projection_magic_begin = "str_spell_projection_sword"
projection_magic_end = "str_spell_reinforcement_weapon"

#slot_fate_culture =
#slot_fate_ =

# Elemental Magecraft Begin
fire_magecraft_begin = "str_magecraft_fire_bolt"
water_magecraft_begin = "str_magecraft_water_bolt"
wind_magecraft_begin = "str_magecraft_wind_bolt"
earth_magecraft_begin = "str_magecraft_earth_bolt"

# Specialty Magecraft Begin
alchemy_magecraft_begin = "str_magecraft_alchemy_flashair"
astromancy_magecraft_begin = "str_magecraft_astromancy_horoscope"
sacrament_magecraft_begin = "str_magecraft_sacrament_baptism"
countermagic_magecraft_begin = "str_magecraft_countermagic_rebound"
curse_magecraft_begin = "str_magecraft_curse_geis"
transfer_magecraft_begin = "str_magecraft_transfer_mana"
healing_magecraft_begin = "str_magecraft_healing"
jewel_magecraft_begin = "str_magecraft_jewel_storage"
reincarnation_magecraft_begin = "str_magecraft_reincarnation_familiar"
perception_magecraft_begin = "str_magecraft_perception_other"
consciousness_magecraft_begin = "str_magecraft_consciousness_remote"
necromancy_magecraft_begin = "str_magecraft_necromancy_raise"
golemancy_magecraft_begin = "str_magecraft_golemancy_create"
numerology_magecraft_begin = "str_magecraft_numerology_starbow"
projection_magecraft_begin = "str_magecraft_projection_gradation"
reinforcement_magecraft_begin = "str_magecraft_reinforcement_enhance"
alteration_magecraft_begin = "str_magecraft_alteration_element"
memory_magecraft_begin = "str_magecraft_memory_relationship"
command_magecraft_begin = "str_magecraft_command_dictate"
rune_magecraft_begin = "str_magecraft_runes_ansuz"
spatial_magecraft_begin = "str_magecraft_spatial_teleport"
invocation_magecraft_begin = "str_magecraft_invocation_possess"
evocation_magecraft_begin = "str_magecraft_evocation_commune"
time_magecraft_begin = "str_magecraft_time_stagnate"
puppet_magecraft_begin = "str_magecraft_puppet_build"
formalcraft_magecraft_begin = "str_magecraft_formal_barrier"

# Elemental Magecraft End
fire_magecraft_end = water_magecraft_begin
water_magecraft_end = wind_magecraft_begin
wind_magecraft_end = earth_magecraft_begin
earth_magecraft_end = alchemy_magecraft_begin

# Specialty Magecraft End
alchemy_magecraft_end	 		= astromancy_magecraft_begin
astromancy_magecraft_end 		= sacrament_magecraft_begin
sacrament_magecraft_end 		= countermagic_magecraft_begin
countermagic_magecraft_end	 	= curse_magecraft_begin
curse_magecraft_end 			= transfer_magecraft_begin
transfer_magecraft_end	 		= healing_magecraft_begin
healing_magecraft_end	 		= jewel_magecraft_begin
jewel_magecraft_end 			= reincarnation_magecraft_begin
reincarnation_magecraft_end 	= perception_magecraft_begin
perception_magecraft_end 		= consciousness_magecraft_begin
consciousness_magecraft_end 	= necromancy_magecraft_begin
necromancy_magecraft_end 		= golemancy_magecraft_begin
golemancy_magecraft_end 		= numerology_magecraft_begin
numerology_magecraft_end 		= projection_magecraft_begin
projection_magecraft_end 		= reinforcement_magecraft_begin
reinforcement_magecraft_end 	= alteration_magecraft_begin
alteration_magecraft_end 		= memory_magecraft_begin
memory_magecraft_end 			= command_magecraft_begin
command_magecraft_end 			= rune_magecraft_begin
rune_magecraft_end 				= spatial_magecraft_begin
spatial_magecraft_end 			= invocation_magecraft_begin
invocation_magecraft_end 		= evocation_magecraft_begin
evocation_magecraft_end 		= time_magecraft_begin
time_magecraft_end 				= puppet_magecraft_begin
puppet_magecraft_end 			= formalcraft_magecraft_begin
formalcraft_magecraft_end 		= "str_dmg_type_normal"

####### Servant Names #############

fate_saber_name = "str_servant_saber_generic"
fate_lancer_name = "str_servant_lancer_generic"
fate_rider_name = "str_servant_rider_generic"
fate_archer_name = "str_servant_archer_generic"
fate_caster_name = "str_servant_caster_generic"
fate_assassin_name = "str_servant_assassin_generic"
fate_berserker_name = "str_servant_berserker_generic"


######### Fate Master Slots ####################

# fate_master_kwldge_

######### Fate Party Slots #####################

fate_party_slot_party_type = 300 # Master, Church, Mage Association, Nonmagi 
# fate_party_slot_
# fate_party_slot_
# fate_party_slot_
# fate_party_slot_
# fate_party_slot_

# #######################################################################
# 		Item Slots
# #######################################################################

item_slots = 0

######### Fate Weapon Slots ####################

fate_weapon_element 	= 1		# Stores Elemental String
fate_weapon_enhancement = 2		# Stores INT of enhanced value, goes unused
fate_weapon_piercing 	= 3		# Stores flat damage values

# For Servant Info Pages
fate_weapon_name		= 4		#
fate_weapon_rank 		= 5		#
fate_weapon_description = 6		#

fate_mystic_code_spell_01 = 7	# Stores additional combat spells from equipped mystic codes
fate_mystic_code_spell_02 = 8	#
fate_mystic_code_spell_03 = 9	#
fate_mystic_code_spell_04 = 10	# 

fate_weapon_additional_attack = 11		# Bonus attack stat granted by magic weapon
fate_weapon_additional_defense = 12		# Bonus defence granted by magic items
fate_weapon_additional_mana = 13		# Bonus mana stored inside of weapon
fate_weapon_additional_damage = 14		# Bonus damage enhancement from magic weapon

fate_weapon_activated = 15				# Is this weapon considered active? If so, store time in seconds it will be 
										# This is usually watched and will be swapped to an unpowered version if 0

fate_weapon_charge_type = 16			# Stores the charge attacck type for either melee or ranged weapons
fate_weapon_thrust_charge_type = 17		# Same as above, but for thrusts only

fate_weapon_attack_type = 18			# Mostly for ranged weapons to determine single or multi shot

# Firearm Slots
max_effective_range 	= 19	# Tells AI how close an enemy needs to be to shoot
time_to_max_accuracy	= 20	# Determines AI aiming time, but also how quickly accuracy is regained
max_accuracy_mod		= 21	# How accurate something can get, snipers get 5, shotguns get like 40
min_accuracy_mod		= 22	# Upper limit of inaccuracy, 50 for boltaction, 10 for autos
clip_size				= 23	# How many shots a weapon can hold
rounds_per_second		= 24	# How many shots are fired per second if the trigger is held (1 is semiauto) 
accuracy_lost_per		= 25	# How much accuracy is lost by each shot.

# #######################################################################
# 		Troop Slots
# #######################################################################

troop_slots = 0

slot_troop_met_previously = 1

######### Fate Parameter Slots ###########

fate_param_str = 2
fate_param_end = 3
fate_param_agi = 4
fate_param_mgc = 5
fate_param_lck = 6
fate_param_npm = 7

######## Servant Stat Slots ###############

slot_fate_true_name = 8
slot_fate_servant_class = 9
slot_fate_alignment = 10
slot_fate_master = 11
slot_fate_servant = 11 # Servants will store their master here, masters will store their servant
slot_fate_height = 12
slot_fate_weight = 13
slot_fate_true_name_revealed = 14
slot_fate_info_tier = 15
slot_fate_catalyst_used = 16
slot_fate_catalyst_servant = 17
slot_fate_holy_grail_war = 18
slot_fate_noble_phantasm_used = 19
slot_fate_corrupted_grail = 20
slot_fate_anti_heroes_active = slot_fate_corrupted_grail
slot_fate_max_mana = 21
slot_fate_cur_mana = 22
slot_fate_classification = 23
slot_fate_permanent_injury = 24
slot_fate_has_magic_armor = 25
slot_fate_cur_battle_continuations = 26
slot_fate_max_battle_continuations = 27
slot_fate_can_ressurect = 28
slot_fate_death_time = 29
slot_fate_death_pos = 30
slot_fate_death_team = 31

######### Fate Magic System Slots #############

fate_magic_projection_level = 32
fate_magic_projection_item = 33

fate_magic_reinforcement_level = 34

fate_magic_familiar_level = 35
fate_magic_familiar_type = 36

fate_magic_heal_level = 37
fate_magic_sacrament_level = 38
fate_magic_rune_level = 39
fate_magic_necro_level = 40
fate_magic_fire_level = 50
fate_magic_earth_level = 60
fate_magic_wind_level = 61
fate_magic_special_level = 62

fate_magic_water_align 	= 60
fate_magic_fire_align	= 61
fate_magic_wind_align	= 62
fate_magic_earth_align	= 63
fate_magic_ether_align	= 64

fate_magic_origin		= 65
fate_magic_attribute	= 66


######### Servant Skills Slots ###########

# fate_class_skill_slot = 
# fate_class_skill_slot_level = 


fate_skill_slot = 80
fate_skill_slot_level = 81
fate_skill_slot_01 = 82
fate_skill_slot_01_level = 83
fate_skill_slot_02 = 84
fate_skill_slot_02_level = 85
fate_skill_slot_03 = 86
fate_skill_slot_03_level = 87
fate_skill_slot_04 = 88
fate_skill_slot_04_level = 89
fate_skill_slot_05 = 90
fate_skill_slot_05_level = 91
fate_skill_slot_06 = 92
fate_skill_slot_06_level = 93
fate_skill_slot_07 = 94
fate_skill_slot_07_level = 95
fate_skill_slot_08 = 96
fate_skill_slot_08_level = 97

fate_np_slot_01 = 98
fate_np_slot_01_level = 99
fate_np_slot_01_cost = 100

fate_np_slot_02 = 101
fate_np_slot_02_level = 102
fate_np_slot_02_cost = 103

fate_np_slot_03 = 104
fate_np_slot_03_level = 105
fate_np_slot_03_cost = 106

fate_np_slot_04 = 107
fate_np_slot_04_level = 108
fate_np_slot_04_cost = 109

fate_np_slot_05 = 107
fate_np_slot_05_level = 108
fate_np_slot_05_cost = 109

fate_skill_slot_begin = fate_skill_slot
fate_skill_slot_end = fate_skill_slot_08_level

####### Player Variables #################

fate_personality_type = 110
fate_parents_bg = 111
fate_plyr_background = 112
fate_education = 113
fate_training = 114
fate_family_magus_history = 115

fate_kwldge_celtic = 116
fate_kwldge_english = 117
fate_kwldge_greek_hist = 118
fate_kwldge_greek_myth = 119
fate_kwldge_roman = 120
fate_kwldge_swedish = 121
fate_kwldge_french = 122
fate_kwldge_bible = 123
fate_kwldge_assassin = 124
fate_kwldge_modern = 125
fate_kwldge_ancient = 126
fate_kwldge_philosophy = 127

###### Servant Dialogue Slots ############

slot_fate_initial_greet = 128
slot_fate_reveal_greet = 129
slot_fate_greet = 130
slot_fate_summon_speech = 131
slot_fate_dialogue1 = 132
slot_fate_dialogue2 = 133
slot_fate_dialogue3 = 134
slot_fate_motivation = 135
slot_fate_history = 136

slot_fate_summoning_chance = 140 # This one is probably only called just the once.

slot_fate_chosen_spell = 150

slot_fate_spell_slot_1 						= 151
slot_fate_spell_slot_1_strength_modifier 	= 152
slot_fate_spell_slot_1_target_modifier 		= 153

slot_fate_spell_slot_2 						= 154
slot_fate_spell_slot_2_strength_modifier 	= 155
slot_fate_spell_slot_2_target_modifier 		= 156

slot_fate_spell_slot_3 						= 157
slot_fate_spell_slot_3_strength_modifier 	= 158
slot_fate_spell_slot_3_target_modifier 		= 159

slot_fate_spell_slot_4 						= 160
slot_fate_spell_slot_4_strength_modifier 	= 161
slot_fate_spell_slot_4_target_modifier 		= 162

# ####### Troop Personality Trait Matrix ################################

# #######################################################################
# 	Hopefully by combining these we can discribe all personality types
# Like Shirou should be a = 3, h = 7, l = 4, d = 10, e = 8, p = 2, s = 8
# So he should not attack often, honorable in a fight, often driven by 
# emotion very driven towards goals, caring, very much an idealist, 
# and driven to help others.
#
# 	The idea is instead of defining a fixed personality like pragmatist
# that may leave out some intricacies of the person like how much they
# care about honorable combat, I will assign each character a 0-10 rating
# and use that value in decision making. Somehow. I will also start the 
# player at a 5 for all seven values and then based on character creation
# and in game decisions have each value tick up or down to determine the
# player personality. Hopefully this will allow an organic way for each
# player to express themselves in gameplay and encourage playthroughs
# with this in mind, since personality is one of the driving factors
# behind Servant summoning and the diplomacy system.
#
# 	Like a pragmatist may join forces despite obvious personality clashes
# due to the fact it's, well, pragmatic, but an honorable person would
# never allow a union with someone who is highly selfish and lacking
# empathy.
# #######################################################################

slot_personality_aggression = 180 # 0 is complete pacifist, 10 is aggressive to a fault
slot_personality_honor		= 181 # 0 is debauched, 10 is a chivalric ideal
slot_personality_logic		= 182 # 0 acts solely on instinct, 10 is a computer
slot_personality_drive		= 183 # 0 is lazy, 10 is driven to a fauly
slot_personality_empathy	= 184 # 0 is sociopathic, 10 is a extremely converning
slot_personality_pragmatism	= 185 # 0 is total realist, 10 is total pragmatist
slot_personality_selfless	= 186 # 0 is completely selfish, 10 is totally selfless

s_pa = slot_personality_aggression # Just shortening them to less descriptive names
s_ph = slot_personality_honor
s_pl = slot_personality_logic
s_pd = slot_personality_drive
s_pe = slot_personality_empathy
s_pp = slot_personality_pragmatism
s_ps = slot_personality_selfless

# #######################################################################
# 		Agent Slots
# #######################################################################

agent_slots = 0

slot_agent_is_running_away = 1		# 0 - Staying, 1 - Fleeing

slot_agent_charge_time = 2			# Current charge time, in msec
slot_agent_active_charge = 3		# Is currently excecuting a charge manuveur
slot_agent_charged_shots = 4		# Number of shots left
slot_agent_charge_melee_type = 5	# 1 - Charge Damage, 2 - Elemental, 3 - Status Inflict
slot_agent_charge_range_type = 6	# 1 - Rapid-fire, 2 - Charge Damage, 3 - Elemental, 4 - Status Inflict
slot_agent_range_attack_type = 7	# 1 - Accurate, 2 - Rapid, 
slot_agent_is_double_jumping = 8	# Is the agent currently in a double jump
slot_agent_has_enhanced_legs = 9	# 0 - No, 1 - Enhanced, 2 - Doublejump , 3 - Both

slot_agent_elemental_alignment = 10

# Agent stats, determined on spawn via stats and equipment
slot_agent_armor_level 	= 20		# (Endurance + 0.5 * Agility + Armor Rating)
slot_agent_magic_resist	= 21		# (Magic Attribute + 0.25 * Endurance)

slot_agent_original_damage = 22
slot_agent_original_accuracy = 23
slot_agent_original_speed = 24
slot_agent_original_rspeed = 25

slot_agent_armor_boost = 26				# Adds directly to armor level
slot_agent_armor_boost_timer = 27		# When reaching zero, armor boost is set to zero

slot_agent_magic_res_boost = 28			# Adds directly to magic res
slot_agent_magic_res_boost_timer = 29	# When reaching zero, magic res boost is set to zero

slot_agent_reset_equip_timer = 30 		# When reaching zero, the agent resets their items to their default

slot_agent_invulnerability_timer = 31	# If gt 0, cannot take damage

slot_agent_active_noble_phantasm = 32
slot_agent_active_np_timer		 = 33	# If gt 0, keep above active, if 0, clear above

######### Fate Agent Caster Slots ##############

fate_agnt_max_mana = 49
fate_agnt_cur_mana = 50
fate_agnt_chosen_spell = 51
fate_agnt_chosen_spell_mod = 52
# fate_agnt_magic_armor = 53

# #### Fate Limb Injury and Poison System ####

# Wounds Thresholds
#	10 - 40: 10% Slowed Usage of limb, natural recovery
#	41 - 60: 25% Slowed Usage of Limb, Left arm cannot hold 2h weapons or shields, Can only heal to 41% without magic
#	61 - 90: 75% Slowed Usage of Limb, Cannot used ranged attacks (arms), lose skills that require those limbs 10% lowered max health, can only heal to 61% naturally
#	100: Complete Loss of Limb, 20% lowered Max Health, requires magic in order to heal at all
fate_agent_wounds_larm = 60
fate_agent_wounds_rarm = 61
fate_agent_wounds_lleg = 62
fate_agent_wounds_rleg = 63
fate_agent_wounds_chest = 64
fate_agent_wounds_head = 65

fate_agent_dodges = 67 # Store value of guarantee dodges. Instead of taking damage character will phase through the attacks

fate_agent_poison_build_up 			= 68
fate_agent_poison_damage 			= 69

fate_agent_petrify_build_up 		= 70	# Out of 100, When Passing the
fate_agent_stun_build_up			= 71	# threshold the effect takes place
fate_agent_slow_build_up			= 72	# the effect will take place and
fate_agent_burn_build_up			= 73	# We'll pass the time in seconds to
fate_agent_frost_build_up			= 74	# a different slot
fate_agent_confusion_build_up		= 75
fate_agent_rage_build_up			= 76	# Certain servants are immune to certain debuffs
fate_agent_blind_build_up			= 77	# Certain spells can also inflict immunity
fate_agent_silence_build_up			= 78
fate_agent_snare_build_up			= 79
fate_agent_weaken_build_up			= 80
fate_agent_armor_debuff_build_up	= 81

# These are timers, stored in seconds

fate_agent_petrified			= 90	# Cannot move while there is value in the timer	
fate_agent_stunned				= 91	# Similar to Petrified
fate_agent_slowed				= 92	# Slows agent by a lot
fate_agent_burned				= 93	# Causes extra damage per second and lowers damage
fate_agent_frozen				= 94	# Similar to Petrified, extra melee damage on frozen targets
fate_agent_confused				= 95	# Features a per second chance to forget goals and stand idle
fate_agent_enraged				= 96	# Bonus damage, but loses spellcasting, orders, etc
fate_agent_blinded				= 97	# Severly harms accuracy and enemy tracking
fate_agent_silenced				= 98	# Cannot cast anything requiring words, NPs, Skills, Non-Crest Spells
fate_agent_ensnared				= 99	# Cannot move, but it cheap
fate_agent_weakened				= 100	# Lowers all physical stats and speed
fate_agent_armor_debuff			= 101	# Loses some armor

fate_agent_current_accuracy		= 109
fate_agent_ammo_in_clip			= 110

fate_agent_is_flying			= 115
fate_agent_current_minions		= 116

# #######################################################################
# 		Faction Slots
# #######################################################################

faction_slots = 0

# #######################################################################
# 		Party Slots
# #######################################################################

party_slots = 0

slot_party_type = 1

# fate_party_slot_party_type = 300 # Master, Church, Mage Association, Nonmagi 
# fate_party_slot_
# fate_party_slot_
# fate_party_slot_
# fate_party_slot_
# fate_party_slot_

# #######################################################################
# 		Scene Slots
# #######################################################################

scene_slots = 0

# #######################################################################
# 		Player Slots
# #######################################################################

player_slots = 0


# #######################################################################
# 		Team Slots
# #######################################################################

team_slots = 0

# #######################################################################
# 		Quest Slots
# #######################################################################

quest_slots = 0

# #######################################################################
# 		Party Template Slots
# #######################################################################

party_template_slots = 0

# #######################################################################
# 		Scene Prop Slots
# #######################################################################

scene_prop_slots = 0

fate_prop_spawn_time = 1
fate_prop_owner = 2

fate_prop_target_pos_x = 3
fate_prop_target_pos_y = 4
fate_prop_target_pos_z = 5

fate_prop_target_pos_rot = 6

fate_prop_original_pos_x = 7
fate_prop_original_pos_y = 8
fate_prop_original_pos_z = 9

fate_prop_original_pos_rot = 10

# #######################################################################
#		Miscellaneous
# #######################################################################
miscellaneous = 0

# #######################################################################
# 				Achievements!
#		Achievements might be able to be removed, but with the compiled 
#		module being as empty as you possibly could go, I can't really 
#		test if popping one would cause an error without the constant.
#
#		By this logic, they're still here.
# #######################################################################

# NORMAL ACHIEVEMENTS
ACHIEVEMENT_NONE_SHALL_PASS = 1,
ACHIEVEMENT_MAN_EATER = 2,
ACHIEVEMENT_THE_HOLY_HAND_GRENADE = 3,
ACHIEVEMENT_LOOK_AT_THE_BONES = 4,
ACHIEVEMENT_KHAAAN = 5,
ACHIEVEMENT_GET_UP_STAND_UP = 6,
ACHIEVEMENT_BARON_GOT_BACK = 7,
ACHIEVEMENT_BEST_SERVED_COLD = 8,
ACHIEVEMENT_TRICK_SHOT = 9,
ACHIEVEMENT_GAMBIT = 10,
ACHIEVEMENT_OLD_SCHOOL_SNIPER = 11,
ACHIEVEMENT_CALRADIAN_ARMY_KNIFE = 12,
ACHIEVEMENT_MOUNTAIN_BLADE = 13,
ACHIEVEMENT_HOLY_DIVER = 14,
ACHIEVEMENT_FORCE_OF_NATURE = 15,

# SKILL RELATED ACHIEVEMENTS:
ACHIEVEMENT_BRING_OUT_YOUR_DEAD = 16,
ACHIEVEMENT_MIGHT_MAKES_RIGHT = 17,
ACHIEVEMENT_COMMUNITY_SERVICE = 18,
ACHIEVEMENT_AGILE_WARRIOR = 19,
ACHIEVEMENT_MELEE_MASTER = 20,
ACHIEVEMENT_DEXTEROUS_DASTARD = 21,
ACHIEVEMENT_MIND_ON_THE_MONEY = 22,
ACHIEVEMENT_ART_OF_WAR = 23,
ACHIEVEMENT_THE_RANGER = 24,
ACHIEVEMENT_TROJAN_BUNNY_MAKER = 25,

# MAP RELATED ACHIEVEMENTS:
ACHIEVEMENT_MIGRATING_COCONUTS = 26,
ACHIEVEMENT_HELP_HELP_IM_BEING_REPRESSED = 27,
ACHIEVEMENT_SARRANIDIAN_NIGHTS = 28,
ACHIEVEMENT_OLD_DIRTY_SCOUNDREL = 29,
ACHIEVEMENT_THE_BANDIT = 30,
ACHIEVEMENT_GOT_MILK = 31,
ACHIEVEMENT_SOLD_INTO_SLAVERY = 32,
ACHIEVEMENT_MEDIEVAL_TIMES = 33,
ACHIEVEMENT_GOOD_SAMARITAN = 34,
ACHIEVEMENT_MORALE_LEADER = 35,
ACHIEVEMENT_ABUNDANT_FEAST = 36,
ACHIEVEMENT_BOOK_WORM = 37,
ACHIEVEMENT_ROMANTIC_WARRIOR = 38,

# POLITICALLY ORIENTED ACHIEVEMENTS:
ACHIEVEMENT_HAPPILY_EVER_AFTER = 39,
ACHIEVEMENT_HEART_BREAKER = 40,
ACHIEVEMENT_AUTONOMOUS_COLLECTIVE = 41,
ACHIEVEMENT_I_DUB_THEE = 42,
ACHIEVEMENT_SASSY = 43,
ACHIEVEMENT_THE_GOLDEN_THRONE = 44,
ACHIEVEMENT_KNIGHTS_OF_THE_ROUND = 45,
ACHIEVEMENT_TALKING_HELPS = 46,
ACHIEVEMENT_KINGMAKER = 47,
ACHIEVEMENT_PUGNACIOUS_D = 48,
ACHIEVEMENT_GOLD_FARMER = 49,
ACHIEVEMENT_ROYALITY_PAYMENT = 50,
ACHIEVEMENT_MEDIEVAL_EMLAK = 51,
ACHIEVEMENT_CALRADIAN_TEA_PARTY = 52,
ACHIEVEMENT_MANIFEST_DESTINY = 53,
ACHIEVEMENT_CONCILIO_CALRADI = 54,
ACHIEVEMENT_VICTUM_SEQUENS = 55,

# MULTIPLAYER ACHIEVEMENTS:
ACHIEVEMENT_THIS_IS_OUR_LAND = 56,
ACHIEVEMENT_SPOIL_THE_CHARGE = 57,
ACHIEVEMENT_HARASSING_HORSEMAN = 58,
ACHIEVEMENT_THROWING_STAR = 59,
ACHIEVEMENT_SHISH_KEBAB = 60,
ACHIEVEMENT_RUIN_THE_RAID = 61,
ACHIEVEMENT_LAST_MAN_STANDING = 62,
ACHIEVEMENT_EVERY_BREATH_YOU_TAKE = 63,
ACHIEVEMENT_CHOPPY_CHOP_CHOP = 64,
ACHIEVEMENT_MACE_IN_YER_FACE = 65,
ACHIEVEMENT_THE_HUSCARL = 66,
ACHIEVEMENT_GLORIOUS_MOTHER_FACTION = 67,
ACHIEVEMENT_ELITE_WARRIOR = 68,

# COMBINED ACHIEVEMENTS
ACHIEVEMENT_SON_OF_ODIN = 69,
ACHIEVEMENT_KING_ARTHUR = 70,
ACHIEVEMENT_KASSAI_MASTER = 71,
ACHIEVEMENT_IRON_BEAR = 72,
ACHIEVEMENT_LEGENDARY_RASTAM = 73,
ACHIEVEMENT_SVAROG_THE_MIGHTY = 74,

# GENDER_ACHIEVEMENTS
ACHIEVEMENT_MAN_HANDLER = 75,
ACHIEVEMENT_GIRL_POWER = 76,
ACHIEVEMENT_QUEEN = 77,
ACHIEVEMENT_EMPRESS = 78,
ACHIEVEMENT_TALK_OF_THE_TOWN = 79,
ACHIEVEMENT_LADY_OF_THE_LAKE = 80,