from header_common import *
from header_operations import *
from header_triggers import *
from header_scenes import *
from module_constants import *

# #######################################################################
#  Each scene record contains the following fields:
#  1) Scene id {string}: used for referencing scenes in other files. The prefix scn_ is automatically added before each scene-id.
#  2) Scene flags {int}. See header_scenes.py for a list of available flags
#  3) Mesh name {string}: This is used for indoor scenes only. Use the keyword "none" for outdoor scenes.
#  4) Body name {string}: This is used for indoor scenes only. Use the keyword "none" for outdoor scenes.
#  5) Min-pos {(float,float)}: minimum (x,y) coordinate. Player can't move beyond this limit.
#  6) Max-pos {(float,float)}: maximum (x,y) coordinate. Player can't move beyond this limit.
#  7) Water-level {float}. 
#  8) Terrain code {string}: You can obtain the terrain code by copying it from the terrain generator screen
#  9) List of other scenes accessible from this scene {list of strings}.
#     (deprecated. This will probably be removed in future versions of the module system)
#     (In the new system passages are used to travel between scenes and
#     the passage's variation-no is used to select the game menu item that the passage leads to.)
# 10) List of chest-troops used in this scene {list of strings}. You can access chests by placing them in edit mode.
#     The chest's variation-no is used with this list for selecting which troop's inventory it will access.
# #######################################################################

scenes = [

    # #######################################################################
	# 		These are summoned by the engine in specific cases.
	# #######################################################################
	
  ("random_scene", sf_generate|sf_randomize|sf_auto_entry_points, "none", "none", (0,0), (240,240), -0.5,"0x300028000003e8fa0000034e00004b34000059be",
    [],[]),
  ("conversation_scene",0,"encounter_spot", "bo_encounter_spot", (-40,-40),(40,40),-100,"0",
    [],[]),
  ("water",0,"none", "none", (-1000,-1000),(1000,1000),-0.5,"0",
    [],[]),
  ("random_scene_steppe",sf_generate|sf_randomize|sf_auto_entry_points,"none", "none", (0,0),(240,240),-0.5,"0x0000000229602800000691a400003efe00004b34000059be",
    [],[], "outer_terrain_steppe"),
  ("random_scene_plain",sf_generate|sf_randomize|sf_auto_entry_points,"none", "none", (0,0),(240,240),-0.5,"0x0000000229602800000691a400003efe00004b34000059be",
    [],[], "outer_terrain_plain"),
  ("random_scene_snow",sf_generate|sf_randomize|sf_auto_entry_points,"none", "none", (0,0),(240,240),-0.5,"0x0000000229602800000691a400003efe00004b34000059be",
    [],[], "outer_terrain_snow"),
  ("random_scene_desert",sf_generate|sf_randomize|sf_auto_entry_points,"none", "none", (0,0),(240,240),-0.5,"0x0000000229602800000691a400003efe00004b34000059be",
    [],[], "outer_terrain_desert_b"),
  ("random_scene_steppe_forest",sf_generate|sf_randomize|sf_auto_entry_points,"none", "none", (0,0),(240,240),-0.5,"0x300028000003e8fa0000034e00004b34000059be",
    [],[], "outer_terrain_plain"),
  ("random_scene_plain_forest",sf_generate|sf_randomize|sf_auto_entry_points,"none", "none", (0,0),(240,240),-0.5,"0x300028000003e8fa0000034e00004b34000059be",
    [],[], "outer_terrain_plain"),
  ("random_scene_snow_forest",sf_generate|sf_randomize|sf_auto_entry_points,"none", "none", (0,0),(240,240),-0.5,"0x300028000003e8fa0000034e00004b34000059be",
    [],[], "outer_terrain_snow"),
  ("random_scene_desert_forest",sf_generate|sf_randomize|sf_auto_entry_points,"none", "none", (0,0),(240,240),-0.5,"0x300028000003e8fa0000034e00004b34000059be",
    [],[], "outer_terrain_desert"),
	
  # #######################################################################
  # 	Here's where your stuff belongs
  # #######################################################################

	# #########################
	# ### Fate Scenes Begin ###
	# ## Outside Fuyuki First #
	# ### Gotta figure out how I wanna do these, tbh 
	# #########################

	# Buke Quarter - Emiya Residence
  ("fuyuki_1",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	# Foreign Quarter - Tohsaka Residence
  ("fuyuki_2",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	# Foreign Quarter - Matou Residence
  ("fuyuki_3",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	# Homurahara Private Academy
  ("fuyuki_4",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	# Miyama Crossing / Intersection
  ("fuyuki_5",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	# Miyama Downtown - Mount Miyama Shopping District
  ("fuyuki_6",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	# Ryuudou Temple
  ("fuyuki_7",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	# Mount Enzou
  ("fuyuki_8",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	# Mion River - Fuyuki Bridge
  ("fuyuki_9",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	# Mion River - Oceanside Park
  ("fuyuki_10",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	# Shinto Financial District - Fuyuki Station-Front Park
  ("fuyuki_11",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	# Shinto Financial District - Fuyuki Center Building
  ("fuyuki_12",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	# Fuyuki Central Park
  ("fuyuki_13",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[], "outer_terrain_plain"),
	# Fuyuki Church - Foreigner's Cemetary
  ("fuyuki_14",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[], "outer_terrain_plain"),
	# Fuyuki Church - Chapel & Rectory
  ("fuyuki_15",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	# Industrial Park - Dockyard
  ("fuyuki_16",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	# Industrial Park - Factory District
  ("fuyuki_17",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	# Miyama Suburb - Einzbern Forest - Abandoned House 18, 20 on big map
  ("fuyuki_18",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	# Miyama Suburb - Einzbern Castle # 19 on Big Map
  ("fuyuki_19",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_snow"),
	# Shinto Municipality Limits - "Haunted Mansion", Foreign Quarter - "Clockwork Mansion"
	# 21, 22 on Big map. Opposite sides of town tho, necessary?
  ("fuyuki_20",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
# ############################
# ## Indoor Scenes ###########
# ############################
# ### Miyama Town ############
  ("fate_emiya_house",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
  ("fate_tohsaka_house",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
  ("fate_matou_house",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
  ("fate_mackenzie_house",sf_indoors,"interior_house_b", "bo_interior_house_b", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
  ("fate_homurahara_academy",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
  ("fate_fuyuki_church",sf_indoors,"interior_house_b", "bo_interior_house_b", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
  ("fate_chinese_restaurant",sf_indoors,"interior_house_b", "bo_interior_house_b", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
  ("fate_singing_birds",sf_indoors,"interior_house_b", "bo_interior_house_b", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
# ### Mount Enzou ############
  ("fate_ryuudou_temple",sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
  ("fate_grail_cavern",sf_indoors,"interior_house_b", "bo_interior_house_b", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
# ### Mount Miyama ###########
  ("fate_copenhagen",sf_indoors,"interior_house_b", "bo_interior_house_b", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
  ("fate_verde",sf_indoors,"interior_house_b", "bo_interior_house_b", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
  ("fate_fuyuki_library",sf_indoors,"interior_house_b", "bo_interior_house_b", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
  ("fate_exciting_splash", sf_indoors,"interior_house_b", "bo_interior_house_b", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
  ("fate_ahnenerbe", sf_indoors,"interior_house_b", "bo_interior_house_b", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
# #### Suburbs ###############
  ("fate_einzbern_castle", sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	

 # ### Quick Battle Arenas ###
 ("fate_emiya_house_2v2", sf_generate, "none", "none", (0,0), (100,100), -0.5,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[], "outer_terrain_plain"),
	
 ("fate_ryuudou_temple_2v2", sf_generate, "none", "none", (0,0),(100,100),-100,"0x00000001300389800003a4ea000058340000637a0000399b",
    [],[],"outer_terrain_plain"),
	
 ("fate_homurahara_academy_2v2", sf_generate,"none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	
 ("fate_sewers_2v2", sf_indoors|sf_force_skybox,"dungeon_a", "bo_dungeon_a", (-500,-500),(500,500), -10,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	
 ("fate_extra_labrynth_2v2", sf_indoors|sf_force_skybox,"dungeon_a", "bo_dungeon_a", (-500,-500),(500,500), -10,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	
 ("fate_final_destination_2v2", sf_indoors|sf_force_skybox, "utapau_terrain", "bo_utapau_terrain", (-200,-200),(200,200),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	
 ("fate_quick_end", sf_generate, "none", "none", (0,0),(100,100),-100,"0x00000002300005000007b5eb000041d000001e6a000038a1",
    [],[],"outer_terrain_plain"),
	
  # #######################################################################
  # 				Multiplayer Scenes
  # #######################################################################
]
