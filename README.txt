

                                   AW                                                                                                                    
`7MM"""YMM        mm              ,M'                                                                                                                    
  MM    `7        MM              MV                                                                                                                     
  MM   d  ,6"Yb.mmMMmm .gP"Ya    AW                                                                                                                      
  MM""MM 8)   MM  MM  ,M'   Yb  ,M'                                                                                                                      
  MM   Y  ,pm9MM  MM  8M""""""  MV                                                                                                                       
  MM     8M   MM  MM  YM.    , AW                                                                                                                        
.JMML.   `Moo9^Yo.`Mbmo`Mbmmd',M'                                                                                                                        
                              MV                                                                                                                         
                             AW                                                                                                                          
                                                                                                                                                         
                           ,,                                                            ,...                                                            
            MMP""MM""YMM `7MM                                                          .d' ""    `7MMF'  `7MMF'                                          
            P'   MM   `7   MM                                                          dM`         MM      MM                                            
                 MM        MMpMMMb.  `7Mb,od8 ,pW"Wq.`7MMpMMMb.  .gP"Ya       ,pW"Wq. mMMmm        MM      MM  .gP"Ya `7Mb,od8 ,pW"Wq.   .gP"Ya  ,pP"Ybd 
                 MM        MM    MM    MM' "'6W'   `Wb MM    MM ,M'   Yb     6W'   `Wb MM          MMmmmmmmMM ,M'   Yb  MM' "'6W'   `Wb ,M'   Yb 8I   `" 
                 MM        MM    MM    MM    8M     M8 MM    MM 8M""""""     8M     M8 MM          MM      MM 8M""""""  MM    8M     M8 8M"""""" `YMMMa. 
                 MM        MM    MM    MM    YA.   ,A9 MM    MM YM.    ,     YA.   ,A9 MM          MM      MM YM.    ,  MM    YA.   ,A9 YM.    , L.   I8 
               .JMML.    .JMML  JMML..JMML.   `Ybmd9'.JMML  JMML.`Mbmmd'      `Ybmd9'.JMML.      .JMML.  .JMML.`Mbmmd'.JMML.   `Ybmd9'   `Mbmmd' M9mmmP' 
                                                                                                                                                         
        Hello! Welcome to the Fate/ Throne of Heroes git repository!
		
		All code within is property of dstn / Sionfel / SupaNinjaMan (I am a man of many names), Taleworlds Entertainment, and those brave souls of the
		Taleworlds Modding Community credited below.
		
		It is being licensed to the community at large with the intent that it is used inside non-commercial, free, projects built within Mount&Blade or its derivatives.
		This license covers only the code in this repository, with art and scenes being under several other licenses found inside the released module's Credits.txt.
		
		I have chosen to make this source open as I continue working to help those looking to start.
		
		Community Credits are as Follows:
		
		Tools:
		W.R.E.C.K. (with incomplete integration), from Lav (https://forums.taleworlds.com/index.php?threads/warband-refined-enhanced-compiler-kit-v1-0-0-mar-01-2015.325102/)
		
		Presentations:
		One-Sheet Character Creator Presentation (apparently from our Chinese friends, not where I sourced it) https://bbs.mountblade.com.cn/thread-530734-1-1.html
			This one is heavily, modified, and will be more so by the time I'm done with it.
		Particle Preview Presentation, from Dalion. Used for development reasons https://forums.taleworlds.com/index.php?threads/particle-preview-presentation.434330/
		
		Scripts:
		Crude fragmentation grenade, by Urist https://forums.taleworlds.com/index.php?threads/crude-fragmentation-grenade.231097/#post-5529542
		
		Notable Changes to Community Code Included:
		My compile.bat (W.R.E.C.K.) stays open after compiling and will recompile upon being interacted with!
		
		module_sounds being organized! Sort of!
		
		