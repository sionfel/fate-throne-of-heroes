///////////////////////////////////////////////////////////////////////////////////
//
// Mount&Blade Warband Shaders
// You can add edit main shaders and lighting system with this file.
// You cannot change fx_configuration.h file since it holds application dependent 
// configration parameters. Sorry its not well documented. 
// Please send your feedbacks to our forums.
//
// All rights reserved.
// www.taleworlds.com
//
//
///////////////////////////////////////////////////////////////////////////////////
// compile_fx.bat:
// ------------------------------
// @echo off
// fxc /D PS_2_X=ps_2_a /T fx_2_0 /Fo mb_2a.fxo mb.fx
// fxc /D PS_2_X=ps_2_b /T fx_2_0 /Fo mb_2b.fxo mb.fx
// pause>nul
///////////////////////////////////////////////////////////////////////////////////

#include "../headers/fx_configuration.h"		// source code dependent configration definitions..
#include "../headers/common_constants.h"		// Moves all Functions to that header file, which also links to constants.h
#include "../headers/sionfel_constants.h"		// Moves all Functions to that header file, which also links to constants.h
#include "../headers/common_functions.h"		// Moves all Functions to that header file, which also links to constants.h
#include "../headers/sionfel_functions.h"		// Moves all Functions to that header file, which also links to constants.h
#include "../headers/common_techniques.h"		// Moves all Functions to that header file, which also links to constants.h
#include "../headers/sionfel_techniques.h"		// Moves all Functions to that header file, which also links to constants.h
#include "native_shaders.fx"	// 

/* 	float4 Pos					: POSITION;
	float  Fog					: FOG;
	
	float4 VertexColor			: COLOR0;
	#ifdef INCLUDE_VERTEX_LIGHTING 
	float3 VertexLighting		: COLOR1;
	#endif
	
	float2 Tex0					: TEXCOORD0;
	float3 SunLightDir			: TEXCOORD1;
	float3 SkyLightDir			: TEXCOORD2;
	#ifndef USE_LIGHTING_PASS 
	float4 PointLightDir		: TEXCOORD3;
	#endif
	float4 ShadowTexCoord		: TEXCOORD4;
	float2 ShadowTexelPos		: TEXCOORD5;
	float3 ViewDir				: TEXCOORD6; */

// struct VS_OUTPUT_FATE_ANIME_SHADER
// {
// 	float4 Pos           : POSITION;
// 	//float2 Tex0        	 : TEXCOORD0;
// 	float4 Color         : COLOR0;
// 	//float3 vBinormal : BINORMAL;
// 	//float4 vBlendWeights : BLENDWEIGHT;
// 	//float4 vBlendIndices : BLENDINDICES;
// 	
// 	float  Fog           : FOG;
// };

struct VS_OUTPUT_OUTLINE
{
	float4 Pos					: POSITION;
	float  Fog				    : FOG;
	
	float4 Color				: COLOR0;
	float3 wNorm				: TEXCOORD0;
	float3 Camera				: TEXCOORD1;
};

VS_OUTPUT_OUTLINE vs_fate_anime_shader_outline(uniform const bool use_skinning, float4 vPosition : POSITION, float4 vNormal : NORMAL, float4 vTangent : TANGENT, float4 vColor : COLOR, float2 tc : TEXCOORD0, float4 vBlendWeights : BLENDWEIGHT, float4 vBlendIndices : BLENDINDICES)
{
	VS_OUTPUT_OUTLINE Out;

	//Out.Tex0 = tc;
	float3 P = mul(matWorldView, vPosition); //position in view space
	//apply fog
	float d = length(P);
	
	float4 vWorldPos = (float4)mul(matWorld,vPosition);
	
	vPosition.xyz += vNormal * 0.01;
	
	float4 vObjectPos = vPosition;
	float4 vObjectN = vNormal;
	
	if(use_skinning)
	{
		vObjectPos = skinning_deform(vPosition, vBlendWeights, vBlendIndices);
		vObjectN = normalize(skinning_deform(vNormal, vBlendWeights, vBlendIndices));
	}
	
	Out.wNorm = normalize(mul((float3x3)matWorld, vObjectN));
	Out.Camera = normalize(vCameraPos-vWorldPos);
		
	vPosition = mul(matWorldViewProj, vObjectPos);

	Out.Pos = vPosition;
	
	Out.Color = float4(0, 0, 0, 0);
	//Out.Tex0 = tc;
	Out.Fog = get_fog_amount_new(d, vWorldPos.z);

	return Out;
}

PS_OUTPUT ps_fate_anime_outline(VS_OUTPUT_OUTLINE In)
{
	
	PS_OUTPUT Output;
	//Output.RGBColor =  float4(1, 1, 1, 1);
	//Output.RGBColor *= tex2D(DiffuseTextureSamplerNoWrap, In.Tex0);
	Output.RGBColor = step( 0.98, pow(1 - dot(In.wNorm, In.Camera), 100));
//	Output.RGBColor = float4(1,0,0,1);
	return Output;
}

technique fate_anime_shader_outline_skinned
{
	pass P0
	{
		VertexShader = compile vs_2_0 vs_fate_anime_shader_outline(true);
		PixelShader = compile ps_2_0 ps_fate_anime_outline();
	}
}
technique fate_anime_shader_outline
{
	pass P0
	{
		
		VertexShader = compile vs_2_0 vs_fate_anime_shader_outline(false);
		PixelShader = compile ps_2_0 ps_fate_anime_outline();
	}
}

struct VS_OUTPUT_FATE_ANIME_SHADER2
{
	float4 Pos           : POSITION;
	float2 Tex0        	 : TEXCOORD0;
	float4 Color         : COLOR0;
	
	float4 LocalPosition	: TEXCOORD2;
	float4 ScreenSpace		: TEXCOORD3;
	float4 ShadowTexCoord		: TEXCOORD4;
	float4 WorldPosition		: TEXCOORD5;	
	float3 ViewDir				: TEXCOORD6;
	float3 WorldNormal			: TEXCOORD7;
	// float4 PointLight			: TEXCOORD1;
	
	
	float  Fog           : FOG;
};

VS_OUTPUT_FATE_ANIME_SHADER2 vs_fate_anime_shader_color(float4 vPosition : POSITION, float4 vNormal : NORMAL, float4 vColor : COLOR, float2 tc : TEXCOORD0, float4 vBlendWeights : BLENDWEIGHT, float4 vBlendIndices : BLENDINDICES, float3 vTangent : TANGENT)
{
	VS_OUTPUT_FATE_ANIME_SHADER2 Out;
	
	//------------- This code belongs to MTarini from TLD shader vs_specular_alpha_skin
	float4 vObjectPos = mul(matWorldArray[vBlendIndices.x], vPosition) * vBlendWeights.x
                      + mul(matWorldArray[vBlendIndices.y], vPosition) * vBlendWeights.y
                      + mul(matWorldArray[vBlendIndices.z], vPosition) * vBlendWeights.z
                      + mul(matWorldArray[vBlendIndices.w], vPosition) * vBlendWeights.w;
	float3 vObjectN = normalize(mul((float3x3)matWorldArray[vBlendIndices.x], vNormal) * vBlendWeights.x
							  + mul((float3x3)matWorldArray[vBlendIndices.y], vNormal) * vBlendWeights.y
							  + mul((float3x3)matWorldArray[vBlendIndices.z], vNormal) * vBlendWeights.z
							  + mul((float3x3)matWorldArray[vBlendIndices.w], vNormal) * vBlendWeights.w);
	
	float4 vWorldPos = mul(matWorld,vObjectPos);
	Out.Pos = mul(matViewProj, vWorldPos);
	float3 vWorldN = normalize(mul((float3x3)matWorld, vObjectN));
	//-------------
	
	Out.Tex0 = tc;
	
	Out.WorldPosition = vWorldPos;
	Out.LocalPosition = vObjectPos;
	
	Out.ScreenSpace.xy = (float2(Out.Pos.x, -Out.Pos.y) + Out.Pos.w)/2;
	Out.ScreenSpace.xy += (vDepthRT_HalfPixel_ViewportSizeInv.xy * Out.Pos.w);
	Out.ScreenSpace.zw = Out.Pos.zw;
	
	Out.ScreenSpace.xyz = (mul(matWorldView, normalize(vObjectN)));
	//Out.ScreenSpace.xyz = normalize(mul((float3x3)transpose(matWorld), vObjectN));
	
	float4 diffuse_light;

	//directional lights, compute diffuse color
	//diffuse_light += (dot(vWorldN, -vSkyLightDir) * vSkyLightColor);
	//diffuse_light += saturate((dot(vWorldN, -vSunDir*0.5) * vSunColor));

	//point lights
	diffuse_light = calculate_point_lights_diffuse(vWorldPos, vWorldN, true, false);
	// Out.PointLight = diffuse_light;
	//diffuse_light = smoothstep(0.7,0.85,diffuse_light);
	
	
	//apply material color
	// Out.Color = (vMaterialColor * vColor ) * (diffuse_light);
	// Out.Color = vColor + saturate(diffuse_light); // > 0.5 ? float4(1,1,1,1):float4(0,0,0,1);
	
	//float4 ambient =  + ((vSkyLightColor) * isLit);
	//float4 ambient = vAmbientColor + vGroundAmbientColor + ((vSkyLightColor) * isLit);
	
	
	//Out.Color *= vMaterialColor;
	// Out.Color.rgb = 1.0;
	// * sin(tc.y * 20 *  time_var); // Cool time fade effect
	
	float3 P = mul(matWorldView, vPosition); //position in view space
	//apply fog
	float d = length(P);
	
	float4 ShadowPos = mul(matSunViewProj, vWorldPos);
	Out.ShadowTexCoord = ShadowPos;
	Out.ShadowTexCoord.z /= ShadowPos.w;
	Out.ShadowTexCoord.w = 1.0f;
	//Out.ShadowTexCoord =
	Out.Fog = get_fog_amount_new(d, vWorldPos.z);
	Out.WorldNormal = vWorldN;
	Out.ViewDir = (vCameraPos - vWorldPos);
	
	//uniform const int PcfMode, 
	// Out.ViewDir = normalize(vCameraPos.xyz - vWorldPos.xyz);
	
	diffuse_light += vAmbientColor + vSkyLightColor;
	Out.Color = vMaterialColor * vColor;
	Out.Color.a = 1.0f;
	
	//vColor.rgb = darkenBlood(vColor.rgb);
	
	//Out.Color = vColor;
	return Out;
}

VertexShader vs_fate_anime_shader_color_compiled = compile vs_2_a vs_fate_anime_shader_color();

PS_OUTPUT ps_fate_anime_shader_color(VS_OUTPUT_FATE_ANIME_SHADER2 In, uniform const bool use_specularmap, uniform const bool use_bumpmap, uniform const bool use_emissionmap, uniform const bool use_rreflection = false) 
{ 
	PS_OUTPUT Output;
   
    // Register Texture Maps and Uniforms
    float4 color = tex2D(MeshTextureSampler, In.Tex0);		// Register Diffuse Texture
	float4 reflection = tex2D(ReflectionTextureSampler, In.Tex0);
	float sun_amount = tex2Dproj(ShadowmapTextureSampler, In.ShadowTexCoord).r;    // Suniness
	float3 viewDir = normalize(In.ViewDir);
	
	float3 normal;
	if(use_bumpmap)
		normal = normalize(lerp((2.0f * tex2D(NormalTextureSampler, In.Tex0) - 1.0f), In.WorldNormal, 0.80));
	else
		normal = normalize(In.WorldNormal);
	
	float fresnel = 1-(saturate(dot(viewDir, normal)));

    float NdotL = dot(-vSunDir, normal); // Normal dot Lighting Direction
	float NdotS = dot(float3(0,0,1), normal); // Normal dot Sky Direction
	float NdotG = dot(float3(0,0,-1), normal); // Normal dot Ground Direction
    float3 halfVector = normalize(-vSunDir + viewDir);
	float3 skyHalfVector = normalize(float3(0,0,1) + viewDir);
    float NdotH = dot(normal, halfVector); // Normal dot Half Vector
	float SdotH = dot(float3(0,0,1), skyHalfVector);
	float distToCamera = length(mul(matView, In.WorldPosition));
	
	/*
	From Native envmap shader
	float3 relative_cam_pos = normalize(vCameraPos - vWorldPos);
	float2 envpos;
	float3 tempvec = relative_cam_pos - vWorldN;

	envpos.x = (tempvec.y);// + tempvec.x);
	envpos.y = tempvec.z;
	envpos += 1.0f;
	//   envpos *= 0.5f;

	Out.Tex0.zw = envpos;
	*/
	/*
	float2 reflectionUV;	// below kind of works, stretches along the depth though
	reflectionUV.x = (viewDir.x - normal.x) / 2 + 0.5;
	reflectionUV.y = (viewDir.z - normal.z) / 2 + 0.5;
	*/

	float3 reflectionUV	= (reflect(viewDir, normal) + 1) / 2;
	
	float4 env = tex2D(ReflectionTextureSampler, reflectionUV.xz);
	
	NdotS = (NdotS > 0) ? NdotS : 0; 
	SdotH = (SdotH > 0) ? SdotH : 0; 
	
    float4 emission = 0;										// Register Emission Value
	if(use_emissionmap)
		{
		emission = tex2D(Diffuse2Sampler, In.Tex0);			// If using Emission map, load Diffuse2 as that map
		// emission *=  abs(sin(-2 * In.LocalPosition.y + time_var));	// This varies the power of emission based on the y-axis of the model (local)
		}
   
    float4 specular;										// Register Specular Value
	float specStrength = lerp(fMaterialPower, fMaterialPower / 10, fRainSpecular);

    if(use_specularmap)
        specular = tex2D(SpecularTextureSampler, In.Tex0);	// If this shader uses Specular Maps, load it
    else
	{
        specular = float4(0.1,0.1,0.1,1);				// else set up a basic light specular
		specStrength *= 3;								// But tighten the reflections
	}
	
	/*
	These are loaded inside OpenBRF
	vMaterialColor
	vMaterialColor2
	fMaterialPower		// Used to determine the strength of the specular fSpecular = specColor * pow( saturate(dot(vHalf, normal)), fMaterialPower) * sun_amount;
	vSpecularColor 		// Since Native uses b/w speculars, this allows tinting
	*/
   
    //float4 shadow = smoothstep(0.75,0.76, dot(vSunDir, normal)) * vAmbientColor; // Basically makes a antisun to make a shadow
    float4 isLit = NdotL * sun_amount;
	//isLit = anime_smoothstep(isLit);

    isLit = isLit.r > 0.5 ? float4(1,1,1,1): float4(0,0,0,1); // If under 50% shade, make it completely shaded
	float4 sunLight = vSunColor * isLit;
	float4 skyLight = vSkyLightColor * NdotS;
	
	fresnel = smoothstep(0.85, 0.87, fresnel); 
	
	//specular *= (smoothstep(0.90, 0.92, pow((NdotH * NdotL), specStrength)) * sunLight + (pow(SdotH * NdotS, specStrength) * vSkyLightColor)); //	
	specular *= smoothstep(0.5, 0.75, pow((NdotH * NdotL), specStrength)) * sunLight + smoothstep(0.75, 0.85, (pow(SdotH * NdotS, specStrength)) * skyLight); //	
   
     

    //float sunIntensity = smoothstep(0.02, 0.05, NdotL); //Actual
	// float sunIntensity = smoothstep(0.5, 0.55, NdotL); // Test

	float4 ambient = (vAmbientColor + ((vSkyLightColor) * isLit));
    float4 light = saturate(/*(vSkyLightColor * isLit) +*/ (/*sunIntensity * */sunLight + skyLight));
	
	light.rgb += bNightVision * flashlight(distToCamera, In.ViewDir, normal);
	
	// light.rgb += calculate_spot_lights_diffuse(In.WorldPosition, normal);
	
	
	// if(bNightVision == 1)	// Night Vision Test
	// {
	// light.rgb += lerp(float3(0,0,0), float3(0.25, 0.25, 0.25), smoothstep(0.55, 0.89, (saturate(dot(In.ViewDir.xyz, normal.xyz))))); // Take the dot from camera view to the normal of the object being rendered. The more flat on, the more illumination
	// }
	
	
	Output.RGBColor = color * In.Color;
    Output.RGBColor.rgb *= ambient + light + (fresnel * sunLight);
	Output.RGBColor.rgb += (specular * env) + (emission * emission.a);
	
	//Output.RGBColor = ambient + light;
	
	Output.RGBColor = saturate(Output.RGBColor);	
	
	if(use_rreflection){
		
		if(vSunColor.r > 0)
			Output.RGBColor.rgb += vSunColor * (1 / length(vSunColor)) * retroReflection(tex2D(Diffuse2Sampler, In.Tex0), viewDir, -vSunDir, normal) * NdotL * sun_amount;
		
		float3 fl = flashlight(pow(distToCamera, 0.1), In.ViewDir, normal);
		Output.RGBColor.rgb += fl * bNightVision * retroReflection(tex2D(Diffuse2Sampler, In.Tex0), viewDir, viewDir, normal);
	}
	
	//Output.RGBColor.rgb = env;
	
    return Output;
}

PS_OUTPUT ps_fate_anime_matcap(VS_OUTPUT_FATE_ANIME_SHADER2 In, uniform const bool use_bumpmap) 
{ 
	PS_OUTPUT Output;
   
    // Register Texture Maps and Uniforms
    
	/*
	float sun_amount = tex2Dproj(ShadowmapTextureSampler, In.ShadowTexCoord).r;    // Suniness
	float3 viewDir = normalize(In.ViewDir);
	
	float3 normal;
	if(use_bumpmap)
		normal = normalize(lerp((2.0f * tex2D(NormalTextureSampler, In.Tex0) - 1.0f), In.WorldNormal, 0.80));
	else
		normal = normalize(In.WorldNormal);
	
	//normal = In.ScreenSpace;
	
	
	//float3 reflectionUV	= (reflect(viewDir, normal));
	//float3 reflectionUV	= In.ScreenSpace;
	float3 reflectionUV	= (In.ScreenSpace + 1) / 2;
	reflectionUV.y = 1 - reflectionUV.y;
	//float3 reflectionUV	= (reflect(viewDir, In.ScreenSpace) + 1) / 2;
	
	*/
	
	//float3 reflectionUV	= (reflect(normalize(In.ViewDir), normalize(In.WorldNormal)) + 1) / 2;
	//float2 reflectionUV	= mul(matView, normalize(In.WorldNormal)).xy * 0.5 + 0.5;
	//reflectionUV.y *= -1;
	
	float3 viewPos = mul(matView, In.WorldPosition);
	float3 viewDir = normalize(viewPos);
	
	float3 viewCross = cross(viewDir, mul(matView, normalize(In.WorldNormal)));
	
	float2 reflectionUV = float2(-viewCross.y, viewCross.x);
	reflectionUV = reflectionUV * 0.5 + 0.5;
	reflectionUV.y *= -1;
	
	float4 color = tex2D(MeshTextureSampler, reflectionUV);		// Register Diffuse Texture
	//float4 color = tex2D(MeshTextureSampler, reflectionUV.xz);		// Register Diffuse Texture
	
	//
	//float fresnel = 1-(saturate(dot(viewDir, normal)));
	//
    //float NdotL = dot(-vSunDir, normal); // Normal dot Lighting Direction
	//
    //float4 isLit = smoothstep(0.15, 0.50, NdotL * sun_amount);
	//
    ////isLit = isLit.r > 0.5 ? float4(1,1,1,1): float4(0,0,0,1); // If under 50% shade, make it completely shaded
	//float4 sunLight = vSunColor * isLit;
	//
	//fresnel = smoothstep(0.85, 0.87, fresnel); 
	//
	//float4 ambient = (vAmbientColor + ((vSkyLightColor) * isLit));
    //float4 light = saturate(/*(vSkyLightColor * isLit) +*/ (/*sunIntensity * */sunLight));
	

	//Output.RGBColor = color * In.Color;
	Output.RGBColor = color;
    //Output.RGBColor.rgb *= ambient + light + (fresnel * sunLight);
	
	//Output.RGBColor.rgb = reflectionUV;
	
    return Output;
}

PS_OUTPUT ps_fate_anime_shader_irrid(VS_OUTPUT_FATE_ANIME_SHADER2 In) 
{ 
	PS_OUTPUT Output;
   
    // Register Texture Maps and Uniforms
    
	
	float sun_amount = tex2Dproj(ShadowmapTextureSampler, In.ShadowTexCoord).r;    // Suniness
	float3 viewDir = normalize(In.ViewDir);
	
	float4 color = tex2D(MeshTextureSampler, In.Tex0);		// Register Diffuse Texture
	float4 color2 = tex2D(Diffuse2Sampler, In.Tex0);		// Register Diffuse Texture

	float iri = pow(dot(normalize(In.WorldNormal), viewDir), 3);
	// float iri = pow(1 - (saturate(dot(viewDir, In.WorldNormal))), 25);
	
	
	Output.RGBColor = lerp(color, color2, iri);
	// Output.RGBColor.rgb = iri;
	Output.RGBColor.a = 1;
    return Output;
}

PS_OUTPUT ps_fate_anime_enviroment_preview(VS_OUTPUT_FATE_ANIME_SHADER2 In) 
{ 
	PS_OUTPUT Output;
	
	//float3 viewCross = cross(In.ViewDir, In.WorldNormal);
	//float3 viewNorm = float3(-viewCross.y, viewCross.x, 0.0);
   
	// float3 reflectionUV	= (reflect(normalize(In.ViewDir), normalize(In.WorldNormal)) + 1) / 2;
	//float3 reflectionUV	= reflect(normalize(In.ViewDir), normalize(In.WorldNormal)) * 0.5 + 0.5;
	float3 reflectionUV	= reflect(normalize(In.ViewDir), normalize(In.WorldNormal)) * 0.5 + 0.5;
	//float3 reflectionUV	= (reflect(normalize(viewNorm), normalize(In.WorldNormal)) + 1) / 2;
	
	float4 color = tex2D(ReflectionTextureSampler, reflectionUV.xz);
	//float4 color = texCUBE(ReflectionTextureSampler, reflectionUV);
	//float4 color = tex2D(ReflectionTextureSampler, reflectionUV);
	
	Output.RGBColor = color;
	
    return Output;
}

PS_OUTPUT ps_fate_shader_dots(VS_OUTPUT_FATE_ANIME_SHADER2 In) 
{ 
	PS_OUTPUT Output;
   
    // Register Texture Maps and Uniforms
    float4 color = tex2D(DiffuseTextureSamplerNoWrap, In.Tex0);
	float4 shading = tex2D(Diffuse2Sampler, In.ScreenSpace.xy/* * 2.0f*/);
    float4 specular = tex2D(SpecularTextureSampler, In.Tex0);
    float sun_amount = tex2Dproj(ShadowmapTextureSampler, In.ShadowTexCoord).r;
	
    
	float3 normal = normalize(In.WorldNormal);
	
    float3 viewDir = In.ViewDir;
    
    float NdotL = dot(-vSunDir, normal); // Normal dot Lighting Direction
    float3 halfVector = normalize(-vSunDir + viewDir);
    float NdotH = dot(normal, halfVector); // Normal dot Half Vector    
   
    float4 shadow = smoothstep(0.75,0.76, dot(vSunDir, normal)) * vAmbientColor; // Basically makes a antisun to make a shadow
    float4 isLit = NdotL * sun_amount;

    isLit = isLit.r > 0.5 ? float4(1,1,1,1): float4(0,0,0,1); // If under 50% shade, make it completely shaded
	float4 sunLight = vSunColor * isLit;

	specular = specular * pow((NdotH * NdotL), fMaterialPower);

    //float sunIntensity = smoothstep(0.02, 0.05, NdotL);

	float4 ambient = (vAmbientColor + vGroundAmbientColor + ((vSkyLightColor) * isLit));
    float4 light = saturate((vSkyLightColor * isLit) + (NdotL * sunLight));
   
    light += ambient;
   
    light.rgb -= shadow;
	
	float4 black = float4(0,0,0,1);

    Output.RGBColor = color;
    Output.RGBColor *= ambient + light;
	Output.RGBColor += (specular * sunLight * tex2D(SpecularTextureSampler, In.Tex0));	
	
	float luminosity = (Output.RGBColor.r * 0.299f + Output.RGBColor.g * 0.587f + Output.RGBColor.b * 0.114f);
	
	Output.RGBColor = color;
	
	if (luminosity < 0.15)
		Output.RGBColor.rgb *= (shading.b * shading.g);
	
	if (luminosity < 0.2 && luminosity > 0.15)
		Output.RGBColor.rgb *= shading.r;
	
	if (luminosity < 0.5 && luminosity > 0.2)
		Output.RGBColor.rgb *= shading.g;
	
	Output.RGBColor += (specular * sunLight * tex2D(SpecularTextureSampler, In.Tex0));	
	
    return Output;
}

PS_OUTPUT ps_fate_shader_camo(VS_OUTPUT_FATE_ANIME_SHADER2 In) 
{ 
	PS_OUTPUT Output;
   
	In.ScreenSpace.xy /= In.ScreenSpace.w;
    // Register Texture Maps and Uniforms
    float4 shading = tex2D(ScreenTextureSampler, In.ScreenSpace.xy);
	float3 normal = normalize(In.WorldNormal);
	
    float3 viewDir = In.ViewDir;
	float fresnel = 1-(saturate(dot(viewDir, normal)));
	
    Output.RGBColor = shading;
	Output.RGBColor.a = clamp(fresnel - abs(sin(In.WorldPosition.z - time_var * 2)), 0.05, 1);
	
    return Output;
}

PS_OUTPUT ps_fate_anime_shader_skin(VS_OUTPUT_FATE_ANIME_SHADER2 In) 
{ 
	PS_OUTPUT Output;
   
    // Register Texture Maps and Uniforms
    float4 main = tex2D(DiffuseTextureSamplerNoWrap, In.Tex0);
	float4 main_aged = tex2D(Diffuse2Sampler, In.Tex0);
	float4 specular = tex2D(SpecularTextureSampler, In.Tex0);
	float3 normal = normalize(lerp((2.0f * tex2D(NormalTextureSampler, In.Tex0) - 1.0f), In.WorldNormal, 0.55));
	//float3 normal = In.WorldNormal;
	
	float4 color;
	
	color.rgb = lerp(main.rgb, main_aged.rgb, In.Color.a);
	color.a = 1.0f;
	
	
    float sun_amount = tex2Dproj(ShadowmapTextureSampler, In.ShadowTexCoord).r;
	
    float3 viewDir = In.ViewDir;
    float fresnel = 1-(saturate(dot(viewDir, In.WorldNormal)));

    float NdotL = dot(-vSunDir, normal); // Normal dot Lighting Direction
    float3 halfVector = normalize(-vSunDir + viewDir);
    float NdotH = dot(normal, halfVector); // Normal dot Half Vector    
   
    float4 shadow = smoothstep(0.75,0.76, dot(vSunDir, normal)) * vAmbientColor; // Basically makes a antisun to make a shadow
    float4 isLit = NdotL * sun_amount;

    isLit = isLit.r > 0.5 ? float4(1,1,1,1): float4(0,0,0,1); // If under 50% shade, make it completely shaded
	float4 sunLight = vSunColor * isLit;

    //specular = specular * pow(abs(NdotH) * smoothstep(0.75, 0.78, abs(NdotL)), fMaterialPower);
	specular = specular * pow((NdotH * NdotL), fMaterialPower);
	
    float specularIntensitySmooth;
    specularIntensitySmooth = smoothstep(0.85, 0.86, specular);
    specular *= specularIntensitySmooth;
	   
    fresnel = smoothstep(0.85, 0.87, fresnel);  

    float sunIntensity = smoothstep(0.02, 0.05, NdotL);

	float4 ambient = vAmbientColor + vGroundAmbientColor + ((vSkyLightColor) * isLit);
    float4 light = saturate(ambient + (sunIntensity * sunLight));
	
	light.rgb += bNightVision * flashlight(length(mul(matView, In.WorldPosition)), In.ViewDir, normal);
   
    light.rgb -= shadow * 0.25;

    Output.RGBColor = color;
    Output.RGBColor *= light + (fresnel * sunLight * 0.5f);
	Output.RGBColor += (specular * sunLight * tex2D(SpecularTextureSampler, In.Tex0));
	
    return Output;
}

technique fate_anime_shader
{
	pass P0
	{
		VertexShader = vs_fate_anime_shader_color_compiled;
		PixelShader = compile ps_2_a ps_fate_anime_shader_color(false,false,false);
	}
}

technique fate_anime_shader_emission
{
	pass P0
	{
		VertexShader = vs_fate_anime_shader_color_compiled;
		PixelShader = compile ps_2_a ps_fate_anime_shader_color(false,false,true);
	}
}

technique fate_dot_shader
{
	pass P0
	{
		VertexShader = vs_fate_anime_shader_color_compiled;
		PixelShader = compile ps_2_a ps_fate_shader_dots();
	}
}

technique fate_camo_shader
{
	pass P0
	{
		VertexShader = vs_fate_anime_shader_color_compiled;
		PixelShader = compile ps_2_a ps_fate_shader_camo();
	}
}

technique fate_matcap
{
	pass P0
	{
		VertexShader = vs_fate_anime_shader_color_compiled;
		PixelShader = compile ps_2_a ps_fate_anime_matcap(false);
	}
}

technique fate_envmap_preview
{
	pass P0
	{
		VertexShader = vs_fate_anime_shader_color_compiled;
		PixelShader = compile ps_2_a ps_fate_anime_enviroment_preview();
	}
}

technique fate_matcap_bump
{
	pass P0
	{
		VertexShader = vs_fate_anime_shader_color_compiled;
		PixelShader = compile ps_2_a ps_fate_anime_matcap(true);
	}
}


technique fate_anime_shader_skin
{
	pass P0
	{
		VertexShader = vs_fate_anime_shader_color_compiled;
		PixelShader = compile ps_2_a ps_fate_anime_shader_skin();
		//PixelShader = compile ps_2_a ps_fate_anime_shader_color(false,true,false);
	}
}

technique fate_anime_shader_spec_nobump
{
	pass P0
	{
		VertexShader = vs_fate_anime_shader_color_compiled;
		PixelShader = compile ps_2_a ps_fate_anime_shader_color(true,false,false);
		
	}
}
technique fate_anime_shader_spec_bump
{
	pass P0
	{
		VertexShader = vs_fate_anime_shader_color_compiled;
		PixelShader = compile ps_2_a ps_fate_anime_shader_color(true,true,false);
		
	}
}
technique fate_anime_shader_retro
{
	pass P0
	{
		VertexShader = vs_fate_anime_shader_color_compiled;
		PixelShader = compile ps_2_a ps_fate_anime_shader_color(true,true,false,true);
		
	}
}
technique fate_anime_shader_nospec_bump
{
	pass P0
	{
		VertexShader = vs_fate_anime_shader_color_compiled;
		PixelShader = compile ps_2_a ps_fate_anime_shader_color(false,true,false);
		
	}
}

technique fate_anime_shader_irrid
{
	pass P0
	{
		VertexShader = vs_fate_anime_shader_color_compiled;
		PixelShader = compile ps_2_a ps_fate_anime_shader_irrid();
		
	}
}

struct VS_OUTPUT_TICKER
{
	float4 Pos           : POSITION;	// Vertex Position, Local to Mesh
	float3 Tex0        	 : TEXCOORD0;	// Texture Coordinates form Mesh
	float4 Color         : COLOR0;		// Vertex Coloring
	
	
	float3 ViewDir		 : TEXCOORD6;
	float  Fog           : FOG;			// Pixel Shader Fog
};

VS_OUTPUT_TICKER vs_news_ticker(float4 vPosition : POSITION, float4 vColor : COLOR, float2 tc : TEXCOORD0, float3 vNormal : NORMAL, float3 vBinormal : BINORMAL, float3 vTangent : TANGENT)
{
	VS_OUTPUT_TICKER Out;							// Out is just naming the Output Varaibles
	
	float4 vWorldPos = mul(matWorld, vPosition);	// vPosition in World Space, Used for Fog.
	
	Out.Tex0.xy = tc;									// Texture Coords passed straight through
	
	
	Out.Pos = mul(matWorldViewProj, vPosition);		// vPosition in Projected World View (World Space in Camera)
	
	Out.ViewDir = normalize(vCameraPos-vWorldPos);
	
	// Native Fog Calculation //
	float3 P = mul(matWorldView, vPosition); 		// vPosition in view space
	float d = length(P);							// Distance from Camera
	Out.Tex0.z = P.z;									// Store this inside the texture coords
	Out.Fog = get_fog_amount_new(d, vWorldPos.z);	// Pass to a function to determine fog amount

	Out.Color = vColor;								// Vertex Color passed straight through
	
	
	
	float3 vWorldN = normalize(mul((float3x3)matWorld, vNormal)); //normal in world space
	float3 vWorld_binormal = normalize(mul((float3x3)matWorld, vBinormal)); //normal in world space
	float3 vWorld_tangent  = normalize(mul((float3x3)matWorld, vTangent)); //normal in world space

	float3x3 TBNMatrix = float3x3(vWorld_tangent, vWorld_binormal, vWorldN); 

	Out.ViewDir = mul(TBNMatrix, Out.ViewDir);
	
	
	return Out;										// Output the VS Structure
}

PS_OUTPUT ps_news_ticker(VS_OUTPUT_TICKER In) 
{ 
	PS_OUTPUT Output;		// Name outputted variables Output.yaddayadda

    // Register Texture Maps and Uniforms
    float4 color = tex2D(MeshTextureSampler, In.Tex0);	// Using MeshTextureSampler is absolutely necessary to allow scrolling. The more common DiffuseTextureSamplerNoWrap, limits the texture within 0 - 1.0 UV space.
	// We load a static texture here mostly to get the blue channels offset for the scroll.

	// Our scroll offsets
	//float vertical_scroll = 0.05f * time_var;	
	//float horizontal_scroll = 0.1f * time_var;
	
	float vertical_scroll = 0.05f * time_var + color.b * 0.01f;	
	float horizontal_scroll = 0.1f * time_var + color.b * 0.01f;
	
	// The use of color.b will add a very small jump in Texture Coordinates based on the Blue Channel of the texture
	// Adds a cool glitched look. Really, it's unnecessary, but I wanted to show how we can use channels to convey nontexture data like dead or misaligned pixels in a display.
	// Without color.b you can move this to the VS and save marginal computational time (Per Vertex vs Per Pixel).

	float4 y_displaced = tex2D(MeshTextureSampler, float2(In.Tex0.x, In.Tex0.y + vertical_scroll));	
	float4 x_displaced = tex2D(MeshTextureSampler, float2(In.Tex0.x + horizontal_scroll, In.Tex0.y + (y_displaced.b * 0.01f)));
	// By scrolling the textures seperately we can have vertical scrolling effects that don't scroll horizontally
	// As well as the inverse. Basically keeps the displayed texture from scrolling at a angle

	x_displaced.r = max(0.05f, x_displaced.r); 
	// the max() function makes sure the screen at leasts has a very small backing.
	// So the screen is still displayed even if the red channel doesn't have data.
	// You can actually do a reverse affect with throw out data under a certain value
	// with something like
	// x_displaced.r = x_displaced.r > 0.5 ? x_displaced.r : 0;
	// Read this as if red > half, keep red, if not, make = 0;
	
	float4 diffuse_color = (x_displaced.r + y_displaced.g) * In.Color;
	// We're adding the horizontal red (Text) and the vertical green (Scanlines), then multiplying them by vColor
	
	diffuse_color.a = x_displaced.r + y_displaced.g;
	// Make sure the channels used in the diffuse are visible
	diffuse_color.a -= smoothstep( 0.96, 0.99, sin(25 * In.Tex0.y - (10 * time_var)));
	// Subtract a downward scrolling narrow band of scanlines
	// smoothstep makes anything less than the first value 0, and thing more than the last value 1
	// and everything between a smooth gradient between 0 and 1.
	// Multiplying the y-coordinate from the UV will make more scan lines appear per UV sheet, 25 here
	// Multiplying time makes it move much faster
	diffuse_color.a -= sin(2 * In.Tex0.y + (3 * time_var)) * 0.25f;
	// Now we subtract 2 larger, slower gradients moving upward to add a slower strobe style effect
	diffuse_color.a = max(0.15f, diffuse_color.a);
	// And make sure everything is always AT LEAST 15% opaque
	diffuse_color.a = min(0.80f, diffuse_color.a);
	// But never more than 80%

	float4 darkened = diffuse_color * 0.15f;
	float4 brightened = diffuse_color * (1.0f + max(2 * sin(In.Tex0.y + time_var) * 0.25f, 0));

    Output.RGBColor = brightened;	// Output the PS Structure
	
	INPUT_OUTPUT_GAMMA(brightened.rgb);
	OUTPUT_GAMMA(diffuse_color.rgb);
    return Output;
}

PS_OUTPUT ps_jumbotron(VS_OUTPUT_TICKER In) 
{ 
	PS_OUTPUT Output;		// Name outputted variables Output.yaddayadda

    // Register Texture Maps and Uniforms
    float4 color = tex2D(ScreenTextureSampler, In.Tex0);
	
	Output.RGBColor = color;
    return Output;
}

technique news_ticker	// Name the shader technique
{
	pass P0	// In the first pass (only one available in our engine :cry:)
	{
		VertexShader = compile vs_2_0 vs_news_ticker();	// Our Vertex Shader, compiled using vs_2_0 with no constants
		PixelShader = compile ps_2_a ps_news_ticker(); // Our Pixel Shader, compiled using ps_2_a with no constants
		
	}
}

technique jumbotron	// Name the shader technique
{
	pass P0	// In the first pass (only one available in our engine :cry:)
	{
		VertexShader = compile vs_2_0 vs_news_ticker();	// Our Vertex Shader, compiled using vs_2_0 with no constants
		PixelShader = compile ps_2_a ps_jumbotron(); // Our Pixel Shader, compiled using ps_2_a with no constants
		
	}
}

struct VS_OUTPUT_PLAYSTATION
{
	float4 Pos           : POSITION;	// Vertex Position, Local to Mesh
	float2 Tex0        	 : TEXCOORD0;	// Texture Coordinates from Mesh
	float4 Color         : COLOR0;		// Vertex Coloring
	
	float  Fog           : FOG;			// Pixel Shader Fog
	
	float4 ShadowTexCoord		: TEXCOORD2;
	//float2 ShadowTexelPos		: TEXCOORD3;
	
	//float3 SunLightDir			: TEXCOORD6;
	//float3 SkyLightDir			: TEXCOORD5;
	// Add Sunlight Color
	
	float3 ViewDir				: TEXCOORD6;
	float3 WorldNormal			: TEXCOORD7;
};

VS_OUTPUT_PLAYSTATION vs_playstation(float4 vPosition : POSITION, float4 vColor : COLOR, float2 tc : TEXCOORD0, float4 vNormal : NORMAL)
{
	VS_OUTPUT_PLAYSTATION Out;							// Out is just naming the Output Varaibles
	
	float4 vWorldPos = mul(matWorld, vPosition);	// vPosition in World Space, Used for Fog.
	
	Out.Pos = playstationify(vPosition);			// Magic
	
	Out.Tex0 = tc;									// Texture Coords passed straight through
	
	//Out.Pos = mul(matWorldViewProj, vPosition);		// vPosition in Projected World View (World Space in Camera)
	
	float3 vObjectN = vNormal;
	float3 vWorldN = normalize(mul((float3x3)matWorld, vObjectN));
	
	float NdotL = dot(-vSunDir, vWorldN);
	
	//diffuse_light += (dot(vWorldN, -vSkyLightDir) * vSkyLightColor);
	//diffuse_light += saturate((dot(vWorldN, -vSunDir*0.5) * vSunColor));

	//point lights
	float4 diffuse_light = calculate_point_lights_diffuse(vWorldPos, vWorldN, true, false);
	diffuse_light += (dot(vWorldN, -vSkyLightDir) * vSkyLightColor);
	//diffuse_light += saturate((dot(vWorldN, -vSunDir*0.5) * vSunColor));
	
	float4 Lighting = diffuse_light + vAmbientColor + vSunColor * NdotL;
	
	
	// Native Fog Calculation //
	float3 P = mul(matWorldView, vPosition); 		// vPosition in view space
	float d = length(P);							// Distance from Camera
	Out.Fog = get_fog_amount_new(d, vWorldPos.z);	// Pass to a function to determine fog amount
	
	float4 ShadowPos = mul(matSunViewProj, vWorldPos);
	Out.ShadowTexCoord = ShadowPos;
	Out.ShadowTexCoord.z /= ShadowPos.w;
	Out.ShadowTexCoord.w = 1.0f;
	//Out.ShadowTexelPos = Out.ShadowTexCoord * fShadowMapSize;
	Out.WorldNormal = vWorldN;
	Out.ViewDir = normalize(vCameraPos-vWorldPos);

	Lighting.rgb += bNightVision * flashlight(d, Out.ViewDir, vWorldN);

	Out.Color = vColor * Lighting;								// Vertex Color * Vertex Lighting
	//Out.Color.rgb = floor(Out.Color.rgb * 5)/5;
	return Out;										// Output the VS Structure
}

PS_OUTPUT ps_playstation(VS_OUTPUT_PLAYSTATION In) 
{ 
	PS_OUTPUT Output;		// Name outputted variables Output.yaddayadda

    // Register Texture Maps and Uniforms
    float4 diffuse_color = tex2D(MeshTextureSampler, In.Tex0);	// Using MeshTextureSampler is absolutely necessary to allow scrolling. The more common DiffuseTextureSamplerNoWrap, limits the texture within 0 - 1.0 UV space.
	
	float3 normal = In.WorldNormal;
	float sun_amount = tex2Dproj(ShadowmapTextureSampler, In.ShadowTexCoord).r;

	Output.RGBColor = diffuse_color * ((In.Color * sun_amount) + vAmbientColor);
    //Output.RGBColor *= light + (fresnel * sunLight * 0.5f);
    return Output;
}

technique playstation	// Name the shader technique
{
	pass P0	// In the first pass (only one available in our engine :cry:)
	{
		VertexShader = compile vs_2_0 vs_playstation();	// Our Vertex Shader, compiled using vs_2_0 with no constants
		PixelShader = compile ps_2_a ps_playstation(); // Our Pixel Shader, compiled using ps_2_a with no constants
		
	}
}

struct VS_INPUT_FATE_HAIR
{
	float4 vPosition : POSITION;
	float3 vNormal : NORMAL;
	float3 vTangent : BINORMAL;
	
	float2 tc : TEXCOORD0;
	float4 vColor : COLOR0;
};
struct VS_OUTPUT_FATE_HAIR
{
	float4 Pos					: POSITION;
	float2 Tex0					: TEXCOORD0;
	
	float4 VertexLighting		: TEXCOORD1;
	
	float3 viewVec				: TEXCOORD2;
	float3 normal				: TEXCOORD3;
	float3 tangent				: TEXCOORD4;
	float4 VertexColor			: COLOR0;
	
	
	float4 ShadowTexCoord		: TEXCOORD6;
	float2 ShadowTexelPos		: TEXCOORD7;
	float  Fog				    : FOG;
};

VS_OUTPUT_FATE_HAIR vs_fate_hair_aniso (VS_INPUT_FATE_HAIR In)
{
	INITIALIZE_OUTPUT(VS_OUTPUT_HAIR, Out);

	Out.Pos = mul(matWorldViewProj, In.vPosition);

	float4 vWorldPos = (float4)mul(matWorld,In.vPosition);
	float3 vWorldN = normalize(mul((float3x3)matWorld, In.vNormal)); //normal in world space

	float3 P = mul(matWorldView, In.vPosition); //position in view space

	Out.Tex0 = In.tc;

	float4 diffuse_light = vAmbientColor;
	//   diffuse_light.rgb *= gradient_factor * (gradient_offset + vWorldN.z);

	//directional lights, compute diffuse color
	diffuse_light += saturate(dot(vWorldN, -vSkyLightDir)) * vSkyLightColor;

	//point lights
	#ifndef USE_LIGHTING_PASS
	diffuse_light += calculate_point_lights_diffuse(vWorldPos, vWorldN, true, false);
	#endif
	
	//apply material color
	
	Out.VertexLighting = saturate(In.vColor * diffuse_light);
	
	Out.VertexColor = In.vColor;
	

		float3 Pview = vCameraPos - vWorldPos;
		Out.normal = normalize( mul( matWorld, In.vNormal ) );
		Out.tangent = normalize( mul( matWorld, In.vTangent ) );
		Out.viewVec = normalize( Pview );


		float4 ShadowPos = mul(matSunViewProj, vWorldPos);
		Out.ShadowTexCoord = ShadowPos;
		Out.ShadowTexCoord.z /= ShadowPos.w;
		Out.ShadowTexCoord.w = 1.0f;
		Out.ShadowTexelPos = Out.ShadowTexCoord * fShadowMapSize;
		//shadow mapping variables end
	//apply fog
	float d = length(P);

	Out.Fog = get_fog_amount_new(d, vWorldPos.z);
	
	
	return Out;
}
PS_OUTPUT ps_fate_hair_aniso(VS_OUTPUT_FATE_HAIR In)
{
	PS_OUTPUT Output;

	//vMaterialColor2.a -> age slider 0..1
	//vMaterialColor -> hair color
	
	float3 lightDir = -vSunDir;
	float3 hairBaseColor = vMaterialColor.rgb;


	// diffuse term
	float3 diffuse = hairBaseColor * vSunColor.rgb * In.VertexColor.rgb * HairDiffuseTerm(In.normal, lightDir);
			

	float4 tex1_col = tex2D(MeshTextureSampler, In.Tex0);
	INPUT_TEX_GAMMA(tex1_col.rgb);
	float4 tex2_col = tex2D(Diffuse2Sampler, In.Tex0);
	float alpha = saturate(((2.0f * vMaterialColor2.a ) + tex2_col.a) - 1.9f);
	
	float4 final_col = tex1_col;
	final_col.rgb *= hairBaseColor;
	final_col.rgb *= (1.0f - alpha);
	final_col.rgb += tex2_col.rgb * alpha;
		
	float sun_amount = tex2Dproj(ShadowmapTextureSampler, In.ShadowTexCoord).r;

	
	float3 specular = calculate_hair_specular(In.normal, In.tangent, lightDir, In.viewVec, In.Tex0);
	
	float4 total_light = vAmbientColor;
	total_light.rgb += (((diffuse + specular) * sun_amount));
	
	//float4 total_light = vAmbientColor;
	//total_light.rgb += diffuse+ * sun_amount;
	total_light.rgb += In.VertexLighting.rgb;
	
	Output.RGBColor.rgb = total_light * final_col.rgb;
	OUTPUT_GAMMA(Output.RGBColor.rgb);
	
	Output.RGBColor.a = tex1_col.a * vMaterialColor.a;
	
	Output.RGBColor = saturate(Output.RGBColor);	//do not bloom!	
	
	return Output;
}

PS_OUTPUT ps_fate_hair_aniso_oai(VS_OUTPUT_FATE_HAIR In)
{
	PS_OUTPUT Output;

	//vMaterialColor2.a -> age slider 0..1
	//vMaterialColor -> hair color

	// Calculate the light direction and hair base color.
	float3 lightDir = -vSunDir;
	float3 hairBaseColor = vMaterialColor.rgb;

	// Calculate the diffuse lighting.
	float3 diffuse = hairBaseColor * vSunColor.rgb * In.VertexColor.rgb * HairDiffuseTerm(In.normal, lightDir);

	// Calculate the texture colors.
	float4 tex1_col = tex2D(MeshTextureSampler, In.Tex0);
	INPUT_TEX_GAMMA(tex1_col.rgb);
	float4 tex2_col = tex2D(Diffuse2Sampler, In.Tex0);
	float alpha = saturate(((2.0f * vMaterialColor2.a ) + tex2_col.a) - 1.9f);

	// Calculate the final color.
	float4 final_col = tex1_col;
	final_col.rgb *= hairBaseColor;
	final_col.rgb *= (1.0f - alpha);
	final_col.rgb += tex2_col.rgb * alpha;

	// Calculate the shadow amount.
	float sun_amount = tex2Dproj(ShadowmapTextureSampler, In.ShadowTexCoord).r;
	
	// Calculate the specular lighting.
	float3 specular = calculate_hair_specular(In.normal, In.tangent, lightDir, In.viewVec, In.Tex0);

	// Calculate the total lighting.
	float4 total_light = vAmbientColor;
	total_light.rgb += (((diffuse + specular) * sun_amount));
	total_light.rgb += In.VertexLighting.rgb;

	// Calculate the final color of the pixel.
	Output.RGBColor.rgb = total_light * final_col.rgb;
	OUTPUT_GAMMA(Output.RGBColor.rgb);

	// Calculate the final alpha of the pixel.
	Output.RGBColor.a = tex1_col.a * vMaterialColor.a;

	// Clamp the final color to ensure it is within a valid range.
	Output.RGBColor = saturate(Output.RGBColor);

	// Return the final color.
	return Output;
}

technique fate_hair_shader_aniso
{
	pass P0
	{
		VertexShader = compile vs_2_0 vs_fate_hair_aniso();
		PixelShader = compile ps_2_a ps_fate_hair_aniso();
	}
}

VS_OUTPUT_FONT_X vs_beam_align(float4 vPosition : POSITION, float4 vColor : COLOR, float2 tc : TEXCOORD0, float3 vNormal: NORMAL)
{
	VS_OUTPUT_FONT_X Out;
	
	float4 vWorldPos = mul(matWorld, vPosition);
	
	/*
	float3 ViewDir = normalize(vCameraPos - vWorldPos);
	
	float3x3 matAlign = AxisBillboard(float3(0,1,0), ViewDir);
	float3 alignLocalPos = mul(matAlign, vPosition);
	
	Out.Pos = mul(matWorldViewProj, float4(alignLocalPos, vPosition.w));
	*/
	
	Out.Pos = mul(matWorldViewProj, vPosition);
	//float4 vWorldPos = (float4)mul(matWorld,vPosition);
	
	Out.Tex0 = tc;
	Out.Color = vColor * vMaterialColor;

	//apply fog
	float3 P = mul(matWorldView, vPosition); //position in view space
	//float4 vWorldPos = (float4)mul(matWorld,vPosition);
	float d = length(P);
	Out.Fog = get_fog_amount_map(d, vWorldPos.z);

	return Out;
}

PS_OUTPUT ps_beam_align(VS_OUTPUT_FONT_X In) 
{ 
	PS_OUTPUT Output;
	Output.RGBColor =  In.Color;
	Output.RGBColor *= tex2D(MeshTextureSampler, In.Tex0);

		/* OUTPUT_GAMMA(Output.RGBColor.rgb);
	
		float depth = tex2Dproj(DepthTextureSampler, In.projCoord).r;
		
		float alpha_factor = saturate((depth-In.Depth) * 4096);
		
		Output.RGBColor *= alpha_factor; */
	
	return Output;
}

technique beam_add_shader
{
	pass P0
	{
		VertexShader = compile vs_2_0 vs_beam_align();
		PixelShader = compile ps_2_a ps_beam_align();
	}
}

// Eldrich Shader
// Use vertexColor to alter the vertex positions of vertices before moving them inside the skin deformation matrix.
// 2 Horror Types, Eyes and Tendrils
// Eyes: Red Channel, untouched. Green if < 1, use it to determine the Amount of Shrinking and Growing the spheres do, Blue if < 1 use it as an offset of which the grow/shrink occurs so it doesn't just pulsate logicallu.
// Tendrils: Red: untouched. Green, amount to deform tendril along x/y (Vertical), Blue amount to deform tendril along x/z (depth from Forward). 

VS_OUTPUT_FATE_ANIME_SHADER2 vs_fate_eldrich(float4 vPosition : POSITION, 	float4 vNormal : NORMAL, 			float4 vColor : COLOR, 
											 float2 tc : TEXCOORD0, 		float4 vBlendWeights : BLENDWEIGHT, float4 vBlendIndices : BLENDINDICES, 
											 float3 vTangent : TANGENT,		uniform const bool is_eyes = false, uniform const bool is_tendrils = false)
{
	VS_OUTPUT_FATE_ANIME_SHADER2 Out;
	
	// So, idea will be to alter the vertex positions.
	
	if(is_tendrils)
	{
		if(vColor.g < 1.0){
		vPosition.x += 0.25 * vColor.g * sin(time_var + 100 * vPosition.y);
		}
		
		if(vColor.b < 1.0){
		vPosition.z += 0.25 * vColor.b * sin(time_var + 100 * vPosition.z);
		}
	}
	
	// Eyes will add or subtract the vPosition along their normals
	if(is_eyes && vColor.g < 1.0f)
	{
		float seed = vColor.b * 255;
		vPosition.xyz += 0.1 * vNormal * vColor.g * sin(seed + time_var * vColor.b * 2);
		tc.x += 0.5 * sin(time_var) - 0.1 * sin(12 * time_var + seed);
		tc.y += 0.15 * sin(0.15 * time_var);
	}
	
	//------------- This code belongs to MTarini from TLD shader vs_specular_alpha_skin
	float4 vObjectPos = mul(matWorldArray[vBlendIndices.x], vPosition) * vBlendWeights.x
                      + mul(matWorldArray[vBlendIndices.y], vPosition) * vBlendWeights.y
                      + mul(matWorldArray[vBlendIndices.z], vPosition) * vBlendWeights.z
                      + mul(matWorldArray[vBlendIndices.w], vPosition) * vBlendWeights.w;
	float3 vObjectN = normalize(mul((float3x3)matWorldArray[vBlendIndices.x], vNormal) * vBlendWeights.x
							  + mul((float3x3)matWorldArray[vBlendIndices.y], vNormal) * vBlendWeights.y
							  + mul((float3x3)matWorldArray[vBlendIndices.z], vNormal) * vBlendWeights.z
							  + mul((float3x3)matWorldArray[vBlendIndices.w], vNormal) * vBlendWeights.w);
	
	float4 vWorldPos = mul(matWorld,vObjectPos);
	Out.Pos = mul(matViewProj, vWorldPos);
	float3 vWorldN = normalize(mul((float3x3)matWorld, vObjectN));
	//-------------
	
	Out.Tex0 = tc;
	
	Out.WorldPosition = vWorldPos;
	Out.LocalPosition = vObjectPos;
	
	Out.ScreenSpace.xy = (float2(Out.Pos.x, -Out.Pos.y) + Out.Pos.w)/2;
	Out.ScreenSpace.xy += (vDepthRT_HalfPixel_ViewportSizeInv.xy * Out.Pos.w);
	Out.ScreenSpace.zw = Out.Pos.zw;
	
	float4 diffuse_light;

	//directional lights, compute diffuse color
	//diffuse_light += (dot(vWorldN, -vSkyLightDir) * vSkyLightColor);
	//diffuse_light += saturate((dot(vWorldN, -vSunDir*0.5) * vSunColor));

	//point lights
	diffuse_light = calculate_point_lights_diffuse(vWorldPos, vWorldN, true, false);
	diffuse_light = smoothstep(0.8,0.81,diffuse_light);
	
	
	//apply material color
	// Out.Color = (vMaterialColor * vColor ) * (diffuse_light);
	// Out.Color = vColor + saturate(diffuse_light); // > 0.5 ? float4(1,1,1,1):float4(0,0,0,1);
	
	//float4 ambient =  + ((vSkyLightColor) * isLit);
	//float4 ambient = vAmbientColor + vGroundAmbientColor + ((vSkyLightColor) * isLit);
	
	
	//Out.Color *= vMaterialColor;
	// Out.Color.rgb = 1.0;
	// * sin(tc.y * 20 *  time_var); // Cool time fade effect
	
	float3 P = mul(matWorldView, vPosition); //position in view space
	//apply fog
	float d = length(P);
	
	float4 ShadowPos = mul(matSunViewProj, vWorldPos);
	Out.ShadowTexCoord = ShadowPos;
	Out.ShadowTexCoord.z /= ShadowPos.w;
	Out.ShadowTexCoord.w = 1.0f;
	//Out.ShadowTexCoord =
	Out.Fog = get_fog_amount_new(d, vWorldPos.z);
	Out.WorldNormal = vWorldN;
	Out.ViewDir = normalize(vCameraPos-vWorldPos);
	//uniform const int PcfMode, 
	// Out.ViewDir = normalize(vCameraPos.xyz - vWorldPos.xyz);
	
	/*diffuse_light += vAmbientColor + vSkyLightColor;
	Out.Color = vMaterialColor * vColor * diffuse_light;
	Out.Color.a = 1.0f;*/
	Out.Color = float4(vColor.r, 1, 1, 1);
	return Out;
}
technique eldrich_eyes
{
	pass P0
	{
		VertexShader = compile vs_2_0 vs_fate_eldrich(true, false);
		PixelShader = compile ps_2_a ps_fate_anime_shader_color(false, false, false);
	}
}
technique eldrich_tendrils
{
	pass P0
	{
		VertexShader = compile vs_2_0 vs_fate_eldrich(false, true);
		PixelShader = compile ps_2_a ps_fate_anime_shader_color(false, false, false);
	}
}

float fScrollVelocity = 0.006f;

// My biggest change was to fold them together into a single VertexShader instead of two when it's a really minor change.
// and also making the scroll velocity float a global so you can modify it on the fly inside the engine.
VS_OUTPUT_FONT vs_menu_move(float4 vPosition : POSITION, float4 vColor : COLOR, float2 tc : TEXCOORD0, uniform const bool move_right = false)
{
    VS_OUTPUT_FONT Out;

	float vel = fScrollVelocity; // velocity
	if(move_right == true){
		tc.x += vel * time_var;
	}
	else{
		tc.x -= vel * time_var;
	}

    Out.Pos = mul(matWorldViewProj, vPosition);

    float3 P = mul(matWorldView, vPosition).xyz; //position in view space

    Out.Tex0 = tc;
    Out.Color = vColor * vMaterialColor;

    //apply fog
    float d = length(P);
    float4 vWorldPos = (float4)mul(matWorld,vPosition);
    Out.Fog = get_fog_amount_new(d, vWorldPos.z);

    return Out;
}

// This bit is interesting, I don't know the name of this technique, so I'm going to call it precompiling
// Normally you do the compilation at the technique definition level, but, since you may incur compiling
// at multiple techniques, for commonly used shaders, it's more performant to precompile them. In this case
// it is less than necessary since it is only used once, but, I have never done it, so it was fun to use.

VertexShader vs_menu_move_compiled_2_0 = compile vs_2_0 vs_menu_move(false);
VertexShader vs_menu_dust_compiled_2_0 = compile vs_2_0 vs_menu_move(true);

technique main_menu_move
{
    pass P0
    {
        VertexShader = vs_menu_move_compiled_2_0;
        PixelShader = compile ps_2_0 ps_no_shading();
    }
}

technique main_menu_dust
{
    pass P0
    {
        VertexShader = vs_menu_dust_compiled_2_0;
        PixelShader = compile ps_2_0 ps_no_shading();
    }
}


PS_OUTPUT ps_gen_cloud(VS_OUTPUT_TICKER In) 
{ 
	PS_OUTPUT Output;

    // Register Texture Maps and Uniforms
	float vertical_scroll = 0.05 * time_var;	
	float horizontal_scroll = 0.05 * time_var;
	
	float stati = tex2D(MeshTextureSampler, float2(In.Tex0.x /* In.Tex0.z*/, In.Tex0.y /* In.Tex0.z*/)).b;
    float lowres = tex2D(MeshTextureSampler, float2(In.Tex0.x + horizontal_scroll, In.Tex0.y + vertical_scroll)).r;
	float midres = tex2D(MeshTextureSampler, float2(In.Tex0.x - horizontal_scroll, In.Tex0.y)).g;
	float highres = tex2D(MeshTextureSampler, float2(In.Tex0.x + horizontal_scroll, In.Tex0.y - vertical_scroll)).b;
	float color = stati * lowres * midres * highres;
	//color = saturate(clamp(color - 0.1, 0, 1) * 5);
	//color = smoothstep(0.01, 0.35, color);
	
    Output.RGBColor.rgb = saturate(color * In.Tex0.z * -0.25);	// Output the PS Structure
	Output.RGBColor.a = smoothstep(0.15, 0.5, color);
    return Output;
}

technique volumetric_clouds
{
    pass P0
    {
        VertexShader = compile vs_2_0 vs_news_ticker();
        PixelShader = compile ps_2_a ps_gen_cloud();
    }
}

// BSDF Attempt in DX9

struct VS_OUTPUT_VERTEX_MANIP
{
	float4 Pos           : POSITION;
	float2 Tex0        	 : TEXCOORD0;
	float4 Color         : COLOR0;
	
	float  Fog           : FOG;
};

VS_OUTPUT_VERTEX_MANIP vs_fate_missile_animate(float4 vPosition : POSITION, float4 vNormal : NORMAL, float4 vColor : COLOR, float2 tc : TEXCOORD0, uniform const bool swarm = false)
{
	VS_OUTPUT_VERTEX_MANIP Out;
	
	float4 vWorldPos = mul(matWorld, vPosition);
	
	if(swarm)	// If using the searm shader
	{
		float angle = (length(vColor) * 5 * time_var) % 360;
		float speed = length(vColor) * 0.25 * 2 * sin(time_var * vColor.b /*+ length(vWorldPos)*/);
		
		if(vColor.x > 0.5)
		{
			speed *= -1;
			angle *= -1;
		}
		
		float4x4 matRot = rotationMatrix(vColor, angle);
		vPosition.x += speed;
		vPosition = mul(matRot, vPosition);
		vNormal = mul(matRot, vNormal);
		
		if(tc.x < 0.55 && (time_var * 10) % 2 < 1)
			tc.y += 0.5;
	}
	else	// Is missile ripple
	{
		float growth = max(sin(-3 * time_var + (vPosition.y * 10 * vColor.b)) * vColor.r, 0);
		float saveY = vPosition.y;
		vPosition.xyz += vNormal * growth * 0.1;
		vPosition.y = saveY;
	}
	
	
	//vWorldPos = mul(matWorld, vPosition);
	float3 vWorldN = normalize(mul((float3x3)matWorld, vNormal));	
	
	
	Out.Pos = mul(matWorldViewProj, vPosition);
	//Out.Pos = vWorldPos;
	
	
	Out.Color.rgb = lerp(vSunColor, vAmbientColor, dot(vWorldN, vSunDir));
	Out.Color.a = 1;
	
	Out.Tex0 = tc;
	float3 P = mul(matWorldView, vPosition); //position in view space
	//apply fog
	float d = length(P);
	
	Out.Fog = get_fog_amount_new(d, vWorldPos.z);

	return Out;
}

PS_OUTPUT ps_fate_missile_animate(VS_OUTPUT_VERTEX_MANIP In) 
{ 
	PS_OUTPUT Output;
	Output.RGBColor =  In.Color;
	Output.RGBColor *= tex2D(MeshTextureSampler, In.Tex0);
	
	return Output;
}

technique fate_missile_animate
{
    pass P0
    {
        VertexShader = compile vs_2_0 vs_fate_missile_animate();
        PixelShader = compile ps_2_a ps_fate_missile_animate();
    }
}

technique fate_swarm
{
    pass P0
    {
        VertexShader = compile vs_2_0 vs_fate_missile_animate(true);
        PixelShader = compile ps_2_a ps_fate_missile_animate();
    }
}

VS_OUTPUT_VERTEX_MANIP vs_fate_title_animate(float4 vPosition : POSITION, float4 vNormal : NORMAL, float4 vColor : COLOR, float2 tc : TEXCOORD0, uniform const bool bubbles = false)
{
	VS_OUTPUT_VERTEX_MANIP Out;
	
	float4 vWorldPos = mul(matWorld, vPosition);
	float3 vWorldN = normalize(mul((float3x3)matWorld, vNormal));	
	
	if(bubbles)
	{
		float3 offsetPos = float3(0.005 * (sin(vColor.b * time_var) + cos(vColor.r * 0.1 * time_var)), 0.005 * sin(vColor.r * time_var + (255 * vColor.b)), length(vColor) * - 0.5 ); 
		float2 offsetTC = float2(0.2 * time_var * vColor.b, sin(0.25 * time_var * vColor.g));
		vPosition.xyz += offsetPos;
		
		tc += offsetTC;
	}
	
	//vWorldPos = mul(matWorld, vPosition);
	
	
	
	Out.Pos = mul(matWorldViewProj, vPosition);
	//Out.Pos = vWorldPos;
	
	
	// Out.Color.rgb = lerp(vSunColor, vAmbientColor, dot(vWorldN, lampDir));
	Out.Color.rgb = 1;
	Out.Color.a = 1;
	
	Out.Tex0 = tc;
	float3 P = mul(matWorldView, vPosition); //position in view space
	//apply fog
	float d = length(P);
	
	Out.Fog = get_fog_amount_new(d, vWorldPos.z);

	return Out;
}

VS_OUTPUT_BUMP vs_fate_title(float4 vPosition : POSITION, float3 vNormal : NORMAL, float2 tc : TEXCOORD0,  float3 vTangent : TANGENT, float3 vBinormal : BINORMAL, float4 vVertexColor : COLOR0, float4 vPointLightDir : COLOR1)
{
	VS_OUTPUT_BUMP Out;
	
	float4 vWorldPos = mul(matWorld, vPosition);
	float3 vWorldN = normalize(mul((float3x3)matWorld, vNormal));	
	
	float3 vWorld_binormal = normalize(mul((float3x3)matWorld, vBinormal)); //normal in world space
	float3 vWorld_tangent  = normalize(mul((float3x3)matWorld, vTangent)); //normal in world spac
	float3x3 TBNMatrix = float3x3(vWorld_tangent, vWorld_binormal, vWorldN); 
	
	
	//float3 lampDir1 = float3(0, 0, 0);
	//float3 lampDir2 = float3(0.5, 0.5, 1);
	
	float4x4 lampRot = rotationMatrix(float3(0.0f, 0.0f, 1.0f), (time_var * 0.5) % 360);
	
	float3 lampDir  = mul(float3(0.0f, 0.0f, 1.0f), rotationMatrix(float3(1.0f, 0.0f, 0.0f), 45));
	lampDir  = mul(lampDir, lampRot);
	float3 lamp2Dir  = mul(float3(0.0f, 0.0f, 1.0f), rotationMatrix(float3(1.0f, 0.0f, 0.0f), 45));
	
	float4x4 matRot = rotationMatrix(float3(0.0f, 0.0f, 1.0f), 45);
	lamp2Dir = mul(matRot, lamp2Dir);
	
	float3 P = mul(matWorldView, vPosition); //position in view space
	float d = length(P);
	
	Out.Pos				= mul(matWorldViewProj, vPosition);
	Out.VertexColor		= vVertexColor;
	Out.Tex0			= tc;
	Out.SunLightDir		= mul(TBNMatrix, -lampDir);
	Out.SkyLightDir		= mul(TBNMatrix, -lamp2Dir);
	Out.PointLightDir	= 0;
	Out.ShadowTexCoord	= 0;
	Out.ShadowTexelPos	= 0;
	Out.Fog				= get_fog_amount_new(d, vWorldPos.z);
	Out.ViewDir			= normalize(vCameraPos - vWorldPos);
	Out.WorldNormal		= vWorldN;

	return Out;
}

PS_OUTPUT ps_fate_title_animate(VS_OUTPUT_BUMP In) 
{ 
	PS_OUTPUT Output;
		
	Output.RGBColor = In.VertexColor;
	float4 diffuse = tex2D(MeshTextureSampler, In.Tex0);
	float4 specular = tex2D(SpecularTextureSampler, In.Tex0);
	float4 normal = tex2D(NormalTextureSampler, In.Tex0);
	
	Output.RGBColor *= diffuse;
	
	float3 viewDir = In.ViewDir;
	float3 lampColor = float3(1.0, 1.0, 1.0);
	float3 secondLamp = float3(0.2, 0.35, 0.2);
	float3 ambientColor = float3(0.15, 0.15, 0.35);
	
	float3 total_light;

    float NdotL = smoothstep(0.47, 0.55, dot(-In.SunLightDir, normal)); // Normal dot Lighting Direction
    float NdotS = smoothstep(0.47, 0.55, dot(-In.SkyLightDir, normal)); // Normal dot Lighting Direction
    // float3 halfVector = normalize(-In.SunLightDir + viewDir);
    //float NdotH = dot(normal, halfVector); // Normal dot Half Vector
	
	// ambientColor = lerp(ambientColor, secondLamp, NdotS);
	total_light = lerp(ambientColor, lampColor, NdotL);
	
	//float3 spec_level = pow(NdotH, 15) * specular * lampColor;
	
	Output.RGBColor.rgb *= total_light;
	// Output.RGBColor.rgb = ambientColor;
	//Output.RGBColor.rgb += spec_level;
	
	
	
	return Output;
}

technique fate_title
{
    pass P0
    {
        VertexShader = compile vs_2_0 vs_fate_title();
        PixelShader = compile ps_2_a ps_fate_title_animate();
    }
}

technique fate_title_bubbles
{
    pass P0
    {
        VertexShader = compile vs_2_0 vs_fate_title_animate(true);
        PixelShader = compile ps_2_a ps_no_shading();
    }
}

VS_OUTPUT_BUMP vs_light_beam(float4 vPosition : POSITION, float3 vNormal : NORMAL, float2 tc : TEXCOORD0, float4 vVertexColor : COLOR0, float4 vPointLightDir : COLOR1)
{
	VS_OUTPUT_BUMP Out;
	
	float4 vWorldPos = mul(matWorld, vPosition);
	float3 vWorldN = normalize(mul((float3x3)matWorld, vNormal));	
		
	
	//float3 lampDir1 = float3(0, 0, 0);
	//float3 lampDir2 = float3(0.5, 0.5, 1);
	
	float4x4 lampRot = rotationMatrix(float3(0.0f, 0.0f, 1.0f), (time_var * 0.5) % 360);
	
	float3 lampDir  = mul(float3(0.0f, 0.0f, 1.0f), rotationMatrix(float3(1.0f, 0.0f, 0.0f), 45));
	lampDir  = mul(lampDir, lampRot);
	float3 lamp2Dir  = mul(float3(0.0f, 0.0f, 1.0f), rotationMatrix(float3(1.0f, 0.0f, 0.0f), 45));
	
	float4x4 matRot = rotationMatrix(float3(0.0f, 0.0f, 1.0f), 45);
	lamp2Dir = mul(matRot, lamp2Dir);
	
	float3 P = mul(matWorldView, vPosition); //position in view space
	float d = length(P);
	
	Out.Pos				= mul(matWorldViewProj, vPosition);
	Out.VertexColor		= vVertexColor;
	Out.Tex0			= tc;
	Out.SunLightDir		= get_fog_amount_new(d, vWorldPos.z);
	Out.SkyLightDir		= 0;
	Out.PointLightDir	= 0;
	Out.ShadowTexCoord	= 0;
	Out.ShadowTexelPos	= d;
	Out.Fog				= get_fog_amount_new(d, vWorldPos.z);
	Out.ViewDir			= normalize(vCameraPos - vWorldPos);
	Out.WorldNormal		= vWorldN;

	return Out;
}

PS_OUTPUT ps_light_beam(VS_OUTPUT_BUMP In) 
{ 
	PS_OUTPUT Output;
	float4 tex_col = tex2D(MeshTextureSampler, In.Tex0);
	INPUT_TEX_GAMMA(tex_col.rgb);
	//In.VertexColor.a *= pow(smoothstep(0.25, 0.75, dot(In.WorldNormal, In.ViewDir)), 7);
	In.VertexColor.a *= saturate(pow(dot(In.WorldNormal, In.ViewDir), 7) + 0.1);
	In.VertexColor.a *= smoothstep(1, 15, In.ShadowTexelPos.x);
	Output.RGBColor =  In.VertexColor /* tex_col*/;
	OUTPUT_GAMMA(Output.RGBColor.rgb);
	return Output;
}

technique fate_light_beam
{
    pass P0
    {
        VertexShader = compile vs_2_0 vs_light_beam();
        PixelShader = compile ps_2_a ps_light_beam();
    }
}

// openai chatbot's shaders
/*
PS_OUTPUT ps_ai_general_shader(VS_OUTPUT_FATE_ANIME_SHADER2 In) 
{
    // Sample the diffuse map
    float4 diffuseColor = tex2D(diffuseSampler, input.tex);

    // Sample the specular map
    float4 specularColor = tex2D(specularSampler, input.tex);

    // Sample the anisotropic direction map
    float3 anisotropicDir = tex2D(anisotropicSampler, input.tex).rgb;
    anisotropicDir = normalize(anisotropicDir * 2 - 1);

    // Sample the normal map
    float3 normal = tex2D(normalSampler, input.tex).rgb;
    normal = normalize(normal * 2 - 1);
	
	// Calculate the tangent-space anisotropic direction
	float3 tangentAnisotropicDir = mul(anisotropicDir, input.tangent);
	tangentAnisotropicDir = mul(tangentAnisotropicDir, input.binormal);
	tangentAnisotropicDir = mul(tangentAnisotropicDir, input.normal);

	// Calculate the tangent-space light direction for point lights
	float3 lightDir = pointLightPosition - input.pos.xyz;
	lightDir = normalize(lightDir);
	float3 tangentLightDir = mul(lightDir, input.tangent);
	tangentLightDir = mul(tangentLightDir, input.binormal);
	tangentLightDir = mul(tangentLightDir, input.normal);

	// Calculate the point light attenuation
	float pointLightAttenuation = saturate(1 - length(lightDir) / pointLightRadius);

	// Calculate the point light contribution
	float3 pointLight = pointLightColor * pointLightAttenuation * saturate(dot(tangentLightDir, tangentAnisotropicDir));

	// Calculate the tangent-space light direction for the spotlight
	lightDir = spotlightPosition - input.pos.xyz;
	lightDir = normalize(lightDir);
	tangentLightDir = mul(lightDir, input.tangent);
	tangentLightDir = mul(tangentLightDir, input.binormal);
	tangentLightDir = mul(tangentLightDir, input.normal);

	// Calculate the spotlight attenuation
	float spotlightAttenuation = saturate((dot(lightDir, -spotlightDirection) - spotlightOuterAngle) / (spotlightInnerAngle - spotlightOuterAngle));
	// Calculate the spotlight contribution
	float3 spotlight = spotlightColor * spotlightAttenuation * saturate(dot(tangentLightDir, tangentAnisotropicDir));

	// Sample the environment map
	float3 envMapSample = texCUBE(envMapSampler, tangentLightDir).rgb;

	// Calculate the ambient lighting
	float3 ambientLight = envMapSample;

	// Calculate the final lighting
	float3 lighting = ambientLight + pointLight + spotlight;

	// Apply the lighting to the diffuse and specular colors
	diffuseColor *= lighting;
	specularColor *= lighting;

	// Calculate the final color
	float4 finalColor = diffuseColor + specularColor;

	return finalColor;
	
	sampler2D diffuseSampler;
	sampler2D specularSampler;
	sampler2D normalSampler;
	sampler2D envMapSampler;

	struct VertexInput
	{
		float4 pos : POSITION;
		float2 tex : TEXCOORD0;
		float3 tangent : TANGENT;
		float3 binormal : BINORMAL;
		float3 normal : NORMAL;
	};

	struct PixelInput
	{
		float4 pos : SV_POSITION;
		float2 tex : TEXCOORD0;
		float3 tangent : TANGENT;
		float3 binormal : BINORMAL;
		float3 normal : NORMAL;
	};

	// Ambient light parameters
	float3 ambientLightColor = float3(1, 1, 1);

	// Point light parameters
	float3 pointLightColor = float3(1, 1, 1);
	float3 pointLightPosition = float3(0, 5, 0);
	float pointLightRadius = 5;

	// Spotlight parameters
	float3 spotlightColor = float3(1, 1, 1);
	float3 spotlightPosition = float3(0, 5, 0);
	float3 spotlightDirection = normalize(float3(-1, -1, -1));
	float spotlightInnerAngle = 30;
	float spotlightOuterAngle = 45;

	float4 main(PixelInput input) : SV_TARGET
	{
	// Sample the diffuse map
	float4 diffuseColor = tex2D(diffuseSampler, input.tex);

	// Sample the specular map
	float4 specularMapSample = tex2D(specularSampler, input.tex);
	float3 specularColor = specularMapSample.rgb;

	// Sample the normal map
	float3 normalMapSample = tex2D(normalSampler, input.tex).rgb;

	// Calculate the tangent-space normal from the normal map sample
	float3 tangentNormal = normalize(2 * normalMapSample - 1);
	tangentNormal = mul(tangentNormal, input.tangent);
	tangentNormal = mul(tangentNormal, input.binormal);
	tangentNormal = mul(tangentNormal, input.normal);

	// Calculate the tangent-space light direction for point lights
	float3 lightDir = pointLightPosition - input.pos.xyz;
	lightDir = normalize(lightDir);
	float3 tangentLightDir = mul(lightDir, input.tangent);
	tangentLightDir = mul(tangentLightDir, input.binormal);
	tangentLightDir = mul(tangentLightDir, input.normal);

	// Calculate the point light attenuation
	float pointLightAttenuation = saturate(1 - length(lightDir) / pointLightRadius);

	// Calculate the point light contribution
	float3 pointLight = pointLightColor * pointLightAttenuation * saturate(dot(tangentLightDir, tangentNormal));

	// Calculate the tangent-space light direction for the spotlight
	lightDir = spotlightPosition - input.pos.xyz;
	lightDir = normalize(lightDir);
	tangentLightDir = mul(lightDir, input.tangent);
	tangentLightDir = mul(tangentLightDir, input.binormal);
	tangentLightDir = mul(tangentLightDir, input.normal);
		// Calculate the spotlight attenuation
	float spotlightAttenuation = saturate((dot(lightDir, -spotlightDirection) - spotlightOuterAngle) / (spotlightInnerAngle - spotlightOuterAngle));

	// Calculate the spotlight contribution
	float3 spotlight = spotlightColor * spotlightAttenuation * saturate(dot(tangentLightDir, tangentNormal));

	// Sample the environment map
	float3 envMapSample = texCUBE(envMapSampler, tangentLightDir).rgb;

	// Calculate the ambient lighting
	float3 ambientLight = ambientLightColor * envMapSample;

	// Calculate the retroreflection
	float retroReflection = specularMapSample.b;

	// Calculate the self-illumination
	float selfIllumination = specularMapSample.g;

	// Calculate the final lighting
	float3 lighting = ambientLight + pointLight + spotlight + selfIllumination;

	// Apply the lighting to the diffuse and specular colors
	diffuseColor *= lighting;
	specularColor *= lighting;

	// Calculate the final color
	float4 finalColor = diffuseColor + specularColor + retroReflection;

	return finalColor;
	}
	
	*/
