reserved_variables = []

# "_h" = float,
# "_frame_thickness" = float,
# "_width" = float,
# "_text_flags" = hex_value,
# "_text_position" = (posx, posy),
# "_text_size" = (scale_x, scale_y),
# "_size" = (scale_x, scale_y),
# "_position" = (posx, posy),
# "_highlight_text_color" = hex_color 0xAARRGGBB Alpha, Red, Green, Blue
# "_text_color" = hex_color 0xAARRGGBB Alpha, Red, Green, Blue
# module_varables = [
# 
# 
# # Initial Buffer_sizes
# # Modders may adjust these large enough so that the game will not need to restore
# # buffers during the game. However keeping them excessively large will reduce performance in most systems.
# #Buffer_sizes (if smaller than 65535, this also represents the maximum vertex count for a corresponding mesh)
# # Buffer Size Varables, 
# ("new_buffer_size_dx7_regular_ffp", 	("_static", 262144), 
# 										("_dynamic", 262144),
# 		),
# ("new_buffer_size_regular_ffp", 		("_static", 32768), 
# 										("_dynamic", 32768),
# 		),
# ("new_buffer_size_regular", 			("_static", 131072), 
# 										("_dynamic", 65536),
# 		),
# ("new_buffer_size_skinning", 			("_static", 131072), 
# 										("_dynamic", 65536),
# 		),
# ("new_buffer_size_normal_map", 			("_static", 131072), 
# 										("_dynamic", 65536),
# 		),
# ("new_buffer_size_normal_map_skinning", ("_static", 131072), 
# 										("_dynamic", 65536),
# 		),
# 
# #Sounds variables
# 
# # sound priority of 0-256 (0 highest 256 lowest) calculated as such:
# # float pf = (base_dist / (base_dist + dist)) * ((1.0f - priority_effect) + priority_effect * (priority_squared ? get_priority() * get_priority() : get_priority()));
# # sounds prio lower then 225 override other sounds in the list with lowest priority.
# ("sound_base_dist", 3.5),
# ("sound_priority_effect", 0.45),
# ("sound_priority_squared", 1),
# ("log_sound_priority", 0),
# 
# #Global_values
# ("musket_left_hand", ("_dif", (0.0, -0.37, -0.04))),
# 
# ("arrow_button_height", 0.1),
# ("arrow_button_width_ratio", 1.51),
# ("combobox_child_button", 	("_position", (0.001, 0.0)), 
# 							("_size", (0.215, 0.029)), 
# 							("_text_size", (0.018, 0.018)),
# 		),
# ("combobox_button", 		("_size", (0.217, 0.038)),
# 		),
# ("combobox_text", 			("_position", (0.0, 0.013),
# 							("_size", 0.02),
# 		),
# ("combobox_child_button_text", 
# 							("_position", (0.0, 0.01)),
# 		),
# 
# #Yes_no_window
# yes_no_generic_combo
# yes_no_text_title
# yes_no_text_question_with_text_box
# yes_no_text_generic_label2
# yes_no_text_question
# yes_no_text_generic_combo
# yes_no_generic_text_box
# yes_no_generic_text_box_simple_1
# yes_no_generic_text_box_simple_2
# yes_no_button
# yes_no_button1
# yes_no_button2
# yes_no_button1_longer
# yes_no_button2_longer
# 
# #Load_save_window
# save_b_done
# save_b_cancel
# save_b_delete
# 
# #Load_save_window_variables
# save_b_done_h = 0.05
# save_b_cancel_h = 0.05
# save_b_delete_h = 0.04
# save_b_save_x = 0.015
# save_b_save_y = 0.51
# save_b_save_x_a = 0.32
# save_b_save_y_a = 0.21
# save_b_save_size_y = 0.2
# save_b_save_t_s = 0.03
# save_b_save_button_h = 0.37
# save_b_save_t_c = 0xFFAAAAAA
# save_b_save_t_h_c = 0xFFFFFFFF
# save_t_name_x = 0.05
# save_t_name_y = 0.12
# save_t_info_x = 0.05
# save_t_info_y = 0.07
# save_t_error_x = 0.02
# save_t_error_y = 0.01
# 
# #Character_window
# char_b_reset
# char_b_done
# char_b_stats
# char_b_next
# char_b_prev
# char_bo_skills
# char_b_attribs_base
# char_b_skills_base
# char_b_weapon_proficiencies_base
# char_s_skills
# char_sc_skills_outer
# char_t_weapon_proficiencies_title
# char_t_attributes_title
# char_t_skills_title
# char_bo_attribs
# char_bo_general_info
# char_tb_character_name
# char_bo_skill_desc
# char_t_explanation
# char_i_character
# char_i_character_shadow
# character_text_skill_points
# character_text_attribute_points
# character_text_weapon_prop_points
# 
# 
# #Character_window_variables
# char_b_reset_h = 0.038
# char_b_done_h = 0.038
# char_b_stats_h = 0.03
# char_b_prev_h = 0.025
# char_b_next_h = 0.025
# char_b_skills_w = 0.26
# char_b_skills_y_a = 0.0175
# char_b_skills_x_a = 0.025
# char_b_weapon_proficiencies_y_a = 0.03
# char_b_attribs_y_a = 0.03
# char_b_skill_levels_x = 0.24
# char_b_skill_levels_w = 0.02
# char_b_skill_increment_x = 0.2625
# char_b_skill_increment_y_a = 0.005
# char_b_skill_increment_h_a = 0.002
# char_b_skill_increment_t_s = 0.018
# char_b_weapon_proficiency_increment_x = 0.935
# char_b_weapon_proficiency_increment_y = 0.375
# char_b_weapon_proficiency_increment_y_a = 0.03
# char_b_weapon_proficiency_increment_h = 0.024
# char_b_weapon_proficiency_increment_t_s = 0.024
# char_b_attrib_increment_x = 0.17
# char_b_attrib_increment_y_a = 0.0000
# char_b_attrib_increment_h = 0.025
# char_b_attrib_increment_t_s = 0.025
# char_tb_character_name_t_c = 0xFFFF0000
# char_t_current_level_x = 0
# char_t_current_level_y = 0.045
# char_t_base_attrib_x = 0
# char_t_base_attrib_y = 0.020
# char_t_level_x = 0
# char_t_level_y = 0.12
# char_t_xp_x = 0
# char_t_xp_y = 0.07
# char_t_next_level_at_x = 0
# char_t_next_level_at_y = 0.045
# char_t_health_x = 0
# char_t_health_y = 0.095
# char_num_skills = 24
# 
# #Inventory_window
# inv_s_inventory1
# inv_s_inventory2
# inv_sc_inventory1
# inv_sc_inventory2
# inv_t_gold1
# inv_t_gold2
# inv_t_name1
# inv_t_name2
# inv_b_leave
# inv_t_total_cost
# inv_t_armor_base
# inv_t_armor_base2
# inv_i_character
# inv_i_character2
# inv_i_character_shadow
# inv_i_character_shadow2
# inv_im_equipment_menu
# inv_hinge1
# inv_hinge2
# inv_hinge3
# inv_hinge4
# 
# inv_i_equipment_pangel_bg
# 
# 
# #Inventory_window_variables
# inv_b_leave_h = 0.04
# inv_t_armor_y_a = 0.024
# inv_visible_inventory_area = 0.598
# inv_equipment_x = 0.2125
# inv_equipment_y = 0.2953
# inv_armor_x = 0.111
# inv_horse_y = 0.007
# inv_equipment_menu_x_a = 0.333333
# 
# #Face_gen_window
# face_bo_deform
# face_bo_button
# face_sl_deform_key_base
# face_t_deform_key_base
# face_s_deform
# face_sc_deform
# face_b_next_face
# face_b_next_beard
# face_b_next_hair
# face_b_prev_face
# face_b_prev_beard
# face_b_prev_hair
# face_t_change_beard
# face_t_change_face
# face_t_change_hair
# face_b_randomize
# face_b_reset
# face_b_done
# face_sl_age
# face_t_age
# face_sl_hair_color
# face_t_hair_color
# 
# #Face_gen_window_variables
# face_deform_key_h = 0.033
# face_button_h = 0.035
# face_button_arrow_h = 0.05
# 
# #Party_window
# party_b_done
# party_b_reset
# party_b_talk
# party_b_release
# party_b_move_up
# party_b_move_down
# party_t_upgrade
# party_b_upgrade1
# party_b_upgrade2
# party_bo_info
# party_t_troop_cost
# party_bo_members_base
# party_s_members1
# party_s_members2
# party_sc_members1
# party_sc_members2
# party_bo_party1
# party_bo_party2
# party_t_prisoner_management
# party_t_morale
# party_t_gold
# party_t_weekly_cost
# party_t_max_companions
# party_t_max_prisoners
# party_t_party_skills
# party_t_choose_companions
# party_t_choose_prisoners
# party_bo_skills
# party_t_skills
# party_s_skills
# party_sc_skills
# party_i_character
# 
# party_t_selected_troop_name
# 
# #Party_window_variables
# party_b_done_h = 0.04
# party_b_reset_h = 0.04
# party_b_talk_h = 0.04
# party_b_release_h = 0.035
# party_b_move_up_h = 0.03
# party_b_move_down_h = 0.03
# party_b_upgrade1_h = 0.029
# party_b_upgrade2_h = 0.029
# party_b_member_h = 0.0262
# party_b_members_h = 0.0262
# party_b_members_width_ratio = 7.8
# party_b_members_button_h = 0.06
# party_b_members_x = 0.03
# party_bo_skills_t_x = 0.045
# party_bo_skills_t_x_deep = 0.06
# party_bo_skills_t_y = -0.035
# party_bo_skills_t_y_a = 0.03
# party_bo_skills_t_y_a_short = 0.025
# party_b_member_t_c = 0xFF000000
# party_b_member_t_c_wounded = 0xFFFF0000
# party_b_upgrade_t_c_passive = 0xFFCC0000
# party_bo_member1_h = 0.391
# party_bo_member2_h = 0.135
# party_bo_member1_y = 0.296
# party_bo_member2_y = 0.106
# party_info_warn_x = 0.05
# party_info_warn_y = 0.18
# 
# party_b_group
# party_b_group_h = 0.03
# 
# party_b_rename
# party_b_rename_h = 0.03
# 
# party_b_close
# party_b_close_h = 0.03
# 
# 
# 
# party_b_morale_text
# party_b_group_header
# party_b_group_information
# 
# #Notes_window
# note_i_picture
# note_t_keys
# note_t_keys_title
# note_t_data
# note_t_title
# note_b_back
# note_b_forward
# note_b_map_follow
# note_bo_keys
# note_bo_data
# note_s_keys
# note_sc_keys
# note_s_data
# note_sc_data
# 
# #Notes_window_variables
# note_button_sm_h = 0.024
# note_image_border = 0.03
# note_button_x_a = 0.123
# note_button_x = 0.012
# note_button_y = 0.01
# note_button_h = 0.038
# 
# #Conversation_window
# conv_bo_board
# conv_t_partner
# conv_t_utterer_name
# conv_bo_choices
# 
# conv_s_choices
# 
# conv_sc_choices
# 
# conv_t_press_mouse
# 
# conversation_conversation_mesh
# 
# 
# #Conversation_window_variables
# conv_sentence_x = 0.03
# conv_sentence_y_a = 0.041
# conv_sentence_text_x = 0.03
# conv_sentence_size_y = 0.0397
# conv_sentence_width_ratio = 22.8
# conv_sentence_button_height = 0.045
# conv_choices_min_y = 0.27
# 
# #Profile_window
# profile_text_choose_profile
# profile_text_multiplayer
# profile_profiles_combo
# profile_exit_button
# profile_host_button
# profile_join_button
# profile_delete_button
# profile_create_button
# profile_edit_button
# 
# profile_character_image
# 
# #Multiplayer_client_window
# multiplayer_client_connect_button
# multiplayer_client_add_to_fav_button
# multiplayer_client_exit_button
# multiplayer_client_table_render_height_adder = 0.5
# multiplayer_client_host_button
# multiplayer_client_change_profile_button
# multiplayer_client_servers_list
# multiplayer_client_list_type_combo
# multiplayer_client_info_text
# multiplayer_client_filter_info_text
# multiplayer_client_list_type_text
# multiplayer_client_password_text
# multiplayer_client_password_textbox
# multiplayer_client_refresh_button
# multiplayer_client_search_button
# multiplayer_client_cancel_button
# multiplayer_client_remove_from_fav_button
# multiplayer_client_remove_from_fav_button
# 
# multiplayer_client_server_filter_checkbox
# 
# server_filter_vertex_color = 0xAAFFFFBA
# table_sort_icon_position_y_displacement_offset = -0.06
# 
# 
# multiplayer_client_filter_area_height = 0.112
# multiplayer_client_password_area_height = 0.05
# multiplayer_client_filter_area_height2 = 0.0
# multiplayer_client_password_area_height2 = -0.05
# mp_client_table_test1 = 0.0
# mp_client_table_test2 = 0.0
# 
# server_filter_panel
# 
# mp_server_table_icon_widget_pos_x_multiplier = -2.9
# mp_server_table_icon_widget_pos_y_multiplier = -4.75
# 
# table_sort_color
# 
# server_filter_checkbox_background
# server_filter_panel_has_players_checkbox
# server_filter_panel_is_not_full_checkbox
# server_filter_panel_is_version_compatible_checkbox
# server_filter_panel_is_password_free_checkbox
# server_filter_panel_native_only_checkbox
# server_filter_panel_ping_limit_combo_label
# server_filter_panel_game_type_combo_label
# server_filter_panel_ping_limit_label
# server_filter_panel_game_type_label
# server_filter_panel_filter_info_text
# 
# table_line_color
# 
# game_ui_container
# game_ui_player
# game_ui_horse
# game_ui_player_health
# game_ui_horse_health
# game_ui_ammo_status
# game_ui_shield_status
# game_ui_gold
# game_ui_ammo
# game_ui_orders_container
# tactical_game_ui_gold_mesh
# tactical_game_ui_notification
# tactical_game_ui_watching
# 
# # main menu (initial)
# initial_tutorial_button
# initial_start_new_game_button
# initial_resume_game_button
# initial_multiplayer_button
# initial_custom_battle_button
# initial_options_button
# initial_credits_button
# initial_quit_button
# initial_game_logo
# initial_version_info
# initial_module_version_info
# 
# 
# #game loading 
# loading_game_logo
# 
# 
# options_game_logo
# 
# options_right_control_offset_x = -0.07
# 
# 
# tab_item
# tab_item_mouse_event
# 
# tab_panel_posx = -0.00
# tab_panel_posy = -0.617
# tab_panel_sizex = 0.9375
# tab_panel_sizey = 0.64
# 
# tab_posx = 0.03
# tab_posy = 0.645
# tab_sizex = 0.7
# tab_sizey = 0.07
# 
# tab_item_mouse_offset_y1 = 0.668
# 
# control_options_scrollable_posx = 0.05
# control_options_scrollable_sizex = 0.711
# 
# options_window_text_color = 0xff000000
# 
# options_done_button_posx = 0.80
# options_done_button_posy = 0.04
# 
# graphics_options_overall_label_posx = 0.775
# graphics_options_overall_combo_posx = 0.7945
# graphics_options_overall_combo_text_size = 0.014
# 
# graphics_options_performance_posx = 0.775
# graphics_options_performance_posy = 0.42
# graphics_options_performance_textsize = 0.025
# 
# options_difficulty
# #starting (first screen)
# starting_game_logo
# #escape window
# escape_game_logo
# escape_return_button
# escape_options_button
# escape_save_exit_button
# escape_save_button
# escape_save_as_button
# escape_exit_button
# profile_character_shadow
# 
# # Loading Screen
# starting_window_progress = 0xFFF8EACB
# 
# game_menu_width = 0.75
# game_menu_top_width = 0.75
# ]

true = 1
false = 0

jpg = 0
png = 1
bmp = 2
tga = 3

module_ini = [
("module_name", "Fate Throne of Heroes"),

"Module Versioning",

("module_version", 0001),
("compatible_module_version", 0001),
("compatible_multiplayer_version_no", 0001),
("compatible_savegame_module_version", 0001),
("compatible_with_warband", 1),
("# operation_set_version", 1174),

"Loading Variables",

("scan_module_sounds", 1),
("scan_module_textures", 1),
("use_case_insensitive_mesh_searches", 0),

"Content Variables", 
("has", ("_custom_battle", true),
		("_multiplayer", false),
		("_single_player", true),
		("_tutorial", false),
	),

"World Map Parameters",
"	Map Constants",

("map", ("_max_distance", 175.0),

		("_min_x" , -180),
		("_min_y" , -145),
		("_max_x" , 180),
		("_max_y" , 145),
		
		("_sea_direction" , -40),
		("_sea_wave_rotation", 300),
		("_sea_speed_x" , 0.02),
		("_sea_speed_y" , -0.02),
		("_min_elevation" , 0.2),
		("_max_elevation" , 0.5),
		("_river_direction" , 140),
		("_river_speed_x" , 0.01),
		("_river_speed_y" , -0.01),
		("_tree_types", 17),
		("_snow_tree_types", 3),
		("_steppe_tree_types", 5),
		("_desert_tree_types", 4),
	),

"	Map Time and Date Parameters",

("time_multiplier", 0.25),

"Leveling Parameters",

("player_xp_multiplier", 2.0),
("hero_xp_multiplier", 2.0),
("regulars_xp_multiplier", 3.0),

("level_boundary_multiplier", 2.0),

("attribute_required_per_skill_level", 2),

("attribute_points_per_level", 0.195),
("skill_points_per_level", 2),
("weapon_points_per_level", 5),

("can_run_faster_with_skills", 0),

("player_wounded_treshold", 5),
("hero_wounded_treshold", 15),
("base_companion_limit", 20),
("skill_leadership_bonus", 3),
("skill_prisoner_management_bonus", 5),
("track_spotting_multiplier", 0.8),

"	Weapon Proficiencies",
	
("display", ("_wp_archery", true),
			("_wp_crossbows", true),
			("_wp_firearms", true),
			("_wp_one_handed" , true),
			("_wp_polearms", true),
			("_wp_throwing", true),
			("_wp_two_handed", true),
	),

"Item Parameters",

("timid_modifier_speed_bonus", 0),
("meek_modifier_speed_bonus", 0),

("use_crossbow_as_firearm", false),

"Party Parameters",

("auto_compute_party_radius", true),
("seeing_range", 6.5),
("show_party_ids_instead_of_names", false),
("use_strict_pathfinding_for_ships", true),

"Game Menu Parameter",

("show_troop_upgrades_button", false),

"Miscellaneous World Things",

("show_faction_color", true),
("show_quest_notes", true),
("disable_disband_on_terrain_type", false),
("auto_create_note_indices", false),
("disable_food_slot", true),
("num_hints", true),

"Character Creation Parameters",

("has_accessories_for_female", false),
("limit_hair_colors", true),

"Scene Variables",

("disable_zoom", false),
("can_adjust_camera_distance", true),
("disable_force_leaving_conversations", true),
("far_plane_distance", 5000),

"Combat Parameters",

("shield_penetration",	("_offset", 30.0), 
						("_factor", 3.0),
		),
("crush_through_treshold", 2.4),

("armor_soak_factor",	("_against_cut", 0.8),
						("_against_pierce", 0.65),
						("_against_blunt", 0.5),
		),

("armor_reduction_factor",	("_against_cut", 1.0),
							("_against_pierce", 0.5),
							("_against_blunt", 0.75),
		),

("extra_penetration_factor",	("_soak", 1.0),
								("_reduction", 1.0),
		),

("damage_interrupt_attack_threshold", 3.0),
("damage_interrupt_attack_threshold_mp", 1.0),

("ai_decide_direction_according_to_damage", true),
("consider_weapon_length_for_weapon_quality", true),

("apply_all_ammo_damage_modifiers", true),

("brace_rotation_limit", 0.012),

("couched_lance_damage_multiplier", 0.65),
("horse_charge_damage_multiplier", 1.0),
("fall_damage_multiplier", 1.0),

("disable_attack_while_jumping", false),

("lance_pike_effect_speed", 3.0),

("missile_damage_speed_power", 1.9),
("melee_damage_speed_power", 2.0),

("no_friendly_fire_for_bots", false),

("battle_size",		("_min", 150),
					("_max", 750), 
		),

"Physics Parameters",

("air_friction",	("_arrow", 0.002),
					("_bullet", 0.001),
		),

"In Mission Features (Enable/Disable)",

("can_crouch", false),
("can_objects_make_sound", true),
("can_reload_while_moving", true),
("can_use_scene_props_in_single_player", true),
("has_forced_particles", true),

("horses_rear_with_attack", 1),
("horses_try_running_away", 1),
("multiplayer_walk_enabled", 0),
("shorter_pistol_aiming", 1),
("use_advanced_formation",  0),

("use_phased_reload", 0),

"In Scene Optimizations",

("disable_moveable_flag_optimization", true),
("mission_object_prune_time", 180),

"Graphical Parameters",

("add_set_neighbors_to_tangent_flag_to_shader", true),
("blood_multiplier", 30.0),
("disable_high_hdr", false),
("fix_gamma_on_dx7_operation_colors", true),
("use_bordered_shadow_sampler", true),
("screenshot_format", png),

"Multiplayer Parameters",

("restrict_attacks_more_in_multiplayer", false),
("show_multiplayer_gold", false),
("sync_block_directions", false),
("sync_ragdoll_effects", false),

"Performance and Logs",

("dont_supress_initial_warnings", true),
("give_performance_warnings", true),
("maximum_number_of_notification_messages", 15),
("reduce_texture_loader_memory_usage", 0),
("supports_directx_7", 0),
("use_scene_unloading", 0),
("use_texture_degration_cache", 0),

("dont_load_regular_troop_inventories", 1),

"Resource Loading",
("load_mod_resource", ["fate_shaders", "fate_textures"]),

("load_resource", ["test", "textures_face_gen", "shaders", "textures", "ccoop_extra_ui_textures", "materials", "materials_face_gen", "ccoop_extra_ui_materials",]),

("load_mod_resource", "fate_materials"),

("load_resource", 	[	"uimeshes",
						"meshes_face_gen",
						"helpers",
						"map_tree_meshes",
						"map_icon_meshes",
						"particle_meshes",
						"skeletons",
						"tree_meshes",
						"xtree_meshes",
						"grass_meshes",
						"plant_meshes",
						"body_meshes",
						"object_meshes",
						"object_bodies",
						"goods_meshes",
						"item_meshes1",
						"horse_a",
						"arabian_horses",
						"arabian_castle",
						"food",
						"beards",
						"armors_b",
						"armors_c",
						"armors_d",
						"armors_e",
						"armors_f",
						"armors_g",
						"armors_h",
						"armors_i",
						"boots_b",
						"boots_c",
						"helmets",
						"helmets_b",
						"village_houses",
						"village_houses_a",
						"village_houses_b",
						"hair",
						"deneme",
						"interiors_a",
						"interiors_b",
						"interiors_c",
						"arena",
						"map_icons_b",
						"castle_a",
						"dungeon",
						"stone_houses",
						"snowy_houses",
						"snowy_castle",
						"helmets_d",
						"helmets_e",
						"helmets_f",
						"castle_b",
						"square_keep",
						"anim_b",
						"shields",
						"shields_b",
						"weapon_meshes_c",
						"xtree_meshes_b",
						"map_icons_c",
						"pictures",
						"user_interface_b",
						"user_interface_c",
						"scene_encounter_spot",
						"interior_thirsty_lion",
						"scene_small_tavern",
						"weapon_meshes1",
						"weapon_meshes_b",
						"houses1",
						"wall_meshes1",
						"town_houses",
						"doors",
						"churches",
						"town_houses_b",
						"castle_c",
						"castle_d",
						"castle_e",
						"castle_f",
						"castle_g",
						"castle_h",
						"castle_i",
						"gatehouse",
						"viking_houses",
						"fake_houses",
						"town_houses_c",
						"banners",
						"map_flags",
						"map_flags_b",
						"map_flags_c",
						"map_flags_d",
						"particles_2",
						"prisons",
						"prisons_b",
						"interiors_d",
						"costumes_b",
						"costumes_c",
						"arena_costumes",
						"boots_a",
						"terrain_borders",
						"terrain_borders_b",
						"terrain_borders_c",
						"skyboxes",
						"object_b",
						"tree_e_meshes",
						"destroy",
						"destroy_snowy",
						"xtree_meshes_c",
						"grass_meshes_b",
						"interiors_steppe",
						"grooming_horse",
						"town_houses_d",
						"horses_b",
						"ani_horse_mounted",
						"deneme2",
						"horse_skeleton",
						"steppe_fake_houses",
						"weapon_meshes_d",
						"tableau_shields",
						"heraldic_armors",
						"spear",
						"weapons_e",
						"weapons_f",
						"instruments",
						"sarranid_armors",
						"custom_banner",
						"simple_primitives",
						"ani_man_walk",
						"ani_twohanded",
						"ani_onehanded",
						"ani_death",
						"ani_stand_guardsman",
						"ani_human_mounted",
						"ani_lady_stand",
						"ani_poses",
						"ani_stand_shopkeeper",
						"ani_man_cheer",
						"ani_stand_onhorse",
						"ani_throw_stone",
						"ani_strikes",
						"ani_equip_arms",
						"ani_run_p",
						"ani_run_forward_left_right",
						"uni_strikes3",
						"ani_walk_sideways",
						"ani_run_sideways",
						"ani_stand",
						"ani_crouch_down",
						"ani_low_walk",
						"ani_turn_man",
						"ani_attacks_single",
						"ani_lancer",
						"ani_attacks",
						"ani_kicks",
						"ani_parry_attack",
						"ani_walk_backward",
						"ani_run_lookingsides",
						"ani_defends",
						"ani_walk_lookingsides",
						"ani_jump",
						"ani_wedding",
						"arabian_props",
						"uni_jump",
						"uni_stances",
						"uni_equip",
						"uni_strike",
						"uni_throws",
						"uni_fistswing",
						"uni_lord_stand",
						"uni_defence",
						"uni_sideways",
						"dart",
						"armors_new_a",
						"armors_new_b",
						"armors_new_heraldic",
						"armors_new_arena",
						"crossbows",
						"arabian_armors",
						"rock",
						"costumes_d",
						"nordic_helmets",
						"sarranid_helmets",
						"sarranid_armor",
						"raw_materials",
						"khergit_lady_dress",
						"vaegir_helmets",
						"gauntlets_new",
						"sarranid_lady_dress",
						"sarranid_boots",
						"bride_dress",
						"full_plate_armor",
						"weapon_meshes_e",
						"fur_armors_a",
						"ui_server_filter",
						"warhorse_new",
						"ship",
						"arabian_houses",
						"object_c",
						"tree_f",
						"interiors_arabian",
						"arabian_village",
						"valleyProps",
						"workshops",
						"barrier_primitives",
						"town_houses_e",
						"wb_mp_objects_a",
						"prisonCart",
						"ccoop_extra_ui_meshes",
						]
		),

"Mod Resources",

("load_mod_resource",	["fate_ui", 
						"fate_sceneprops", 
						"fate_particles", 
						"basemeshes", 
						"sabers", 
						"lancers", 
						"archers", 
						"casters", 
						"riders", 
						"assassins", 
						"berserkers", 
						"extras", 
						"masters", 
						"spells", 
						"mageweapons", 
						"utapau", 
						"streetprops", 
						"goldpile", 
						"envmap_textures", 
						"envmap_materials",
						]
		),



]