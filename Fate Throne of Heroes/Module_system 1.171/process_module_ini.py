from module_info import *
#from process_common import *
#from process_operations import *

from module_variables import *

file = open(export_dir + "module_new.ini","w")

file.write("# Hey, This is a new generated Module.ini")

for i in range(len(module_ini)):
	
	if type(module_ini[i]) is str:
		file.write("\n# " + (str(module_ini[i])))
	# Works!
	elif type(module_ini[i][1]) is list:
		for list_items in range(len(module_ini[i][1])):
			file.write("\n" + (str(module_ini[i][0])) + ' = ' + str(module_ini[i][1][list_items]))
	# Works!
	elif type(module_ini[i][1]) is not tuple:
		file.write("\n" + (str(module_ini[i][0])) + ' = ' + str(module_ini[i][1]))
	
	# First item is printed twice. :\
	elif type(module_ini[i][1]) is tuple:
		for nested in module_ini[i]:
			if type(nested) is str:
				continue
			file.write("\n" + str(module_ini[i][0]) + str(nested[0]) + ' = ' +  str(nested[1]))

file.close()