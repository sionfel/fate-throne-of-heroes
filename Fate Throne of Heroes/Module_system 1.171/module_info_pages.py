####################################################################################################################
#  Each quest record contains the following fields:
#  1) Info page id: used for referencing info pages in other files. The prefix ip_ is automatically added before each info page id.
#  2) Info page name: Name displayed in the info page screen.
#
####################################################################################################################

info_pages = [
 ("church", "Holy Church", "    A branch of the Roman Catholic Church specializing in hiding and destroying the proof of existance of magic, demons, vampires, and anything else otherworldly."),
 ("mages_assoc", "Mage's Association", "    A global organization of Magi."),
 
 ("wandering_sea", "The Wandering Sea", "    A branch of magi that while not under, and often in direct conflict with, Mage's Association are in an situation in which they seem allied."),
 
 ("atlas_assoc", "Atlas Association", "    Alchemists essentially, specializing in brain fragmentation. Magi from the Atlas Association often specialize in Numerology, and creating a certain brand of "),
 
 ("master", "Master (Concept)", "An extension of the Grail War, Master's are magi (or occasionally, non-Magi) that are bound by contract to a Servant and as part of that contract, provide their Servant with Mana in order to keep them bound to the Earth."), 
 ("personality", "Personality", "Personality is large part of a being's self. For our purposes it is divided into 7 quantifiable traits that range from 0 to 10 with these traits being: aggression, honor, logic, drive, empathy, pragmatism, selflessness. ^ The sorts of company a person attracts is often dependant on both parties' level of expression of these various traits. ^Should you make a decision that is seem as particularly extreme in on of these categories, you may find yourself gently altering your own personality to one extreme, with decisions counter to your current expression of the trait, altering you more greatly than one's that align with your current self. ^Of course, there are more unnatural ways to alter your personality, if one know's the right people or incantations."),
 
 ("lore", "Knowledge, Myths and Legends", "A personal knowledge of certain branches of myth and legend may affect the speed at which you come to understand specific enemies, and maybe, more interestingly, unveil their True Name."),
 
 ("elements", "Elemental Affinity", "    A Magi's personal affinity towards one of the 5 elements."),
 
 ("origin", "Origin", "A Magi's piece of the root. Every being is formed around an idea or concept formally known as their origin. Commonly origins will affect one's affenety with certain branches of Magecraft, with particularly lucky (or unlucky) people finding their Origins granting them immense, otherwise unaccessible, powers. ^For our sake, one might see it as a sort of perk that affects their growth in various categories. ^^Generally an Origin is seen as a fixed part of one's being, but, certain men have found ways of encouraging the changing of one's Origin to something they may find more advantagious."),
 
 ("mana", "Magic Energy", "Mana, or Prana, is the purified energy of the Earth, filtered through the body, and used in Thalmaturgy. Magi naturally take in energy in a number of different ways, either passively through the enviroment, or actively through consumption of specific materials. One's Mana stores depend on their magic circuits, both the amount and quality. You can temporarily boost your stores of mana by forming a temporary contract with a different magus to draw from their stores."),
 
 ("magic_crest", "Magic Crest", "Magic crests are the crystalized knowledge of a mage family, passed on from family head to family head for generations. Receiving a Crest is considered an incredible honor, and is the goal of any magus. Having a Crest both increases your Mana stores, as well as allows you to use spells that you may not know how they work, but can be fired as a single action by feeding mana through that branch of the crest. ^^One may receive a Branch Crest from another family, basically implating part of the Main Family's Crest in order to allow them to crystalize their own knowledge, and have access to the main Crest holder's mana stores for a brief time as their body acclimates to their new Crest. This is a way more prominent families can take newer or weaker families into their own and increase their overall power."),
 
 ("spells", "Thalmaturgy, Spells and Magecraft", "Basically spellcasting. There are many ways to cast spells, such as pushing magical energy through one's own circuits, chants, formalcraft or mystic codes."),
 
 ("heroic_spirit", "Servant (Heroic Spirit)", "    Servants are spirits summoned by the Holy Grail for the purpose of competing under Masters in the Holy Grail War.^    While not the true existance of the Heroic Spirit, it is an emination of the Spirit into one of the prepared seven vessels by the Holy Grail, using the World's systems in orfer to conjure impossibly strong spirits that no one mage could ever hope to summon independently."),
 
 ("abilities", "Servant Abilities", "    Each Servant is equiped with Abilities, viewable on the Status Menu."),
 
 ("skills", "Servant Skills", "    Each Servant is equiped with Skills, viewable on the Status Menu. These Skills come in a variety of use cases: some being useful in combat, while others are used exclusively outside of physical contests. ^Skills come in two flavors, Personal and Class. Class skills are guaranteed skills as part of the Servant's class container, with power varying on the Heroic Spirit's deeds in life. Personal Skills are skill and attributes the Heroic Spirit carried in life, that manifest themselves in their Servant form."),
 
 ("classes", "Servant Classes", "    Within a Grail War, Heroic Spirits are summoned inside limited scope Class Containers as they would be too powerful otherwise to summon as Familiars. These Classes are seperated into two primary groups.^ Knight Classes: Saber, Lancer and Archer.^ Cavalry Classes: Rider, Assassin, Caster and Berserker.^ Of course, these are not the only classes, and there may exist sub classes, this is the most general line up for any Grail War."),
]
