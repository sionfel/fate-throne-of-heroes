﻿from module_constants import *
from ID_factions import *
from header_items import  *
from header_operations import *
from header_triggers import *

# #######################################################################
#	["item_id", "Item_name_string", [("mesh_name",modifier)], flags, capabilities, value, stats, modifiers, [triggers], [faction_id]],
#
#  Each item record contains the following fields:
#  1) Item id: used for referencing items in other files.
#     The prefix itm_ is automatically added before each item id.
#  2) Item name. Name of item as it'll appear in inventory window
#  3) List of meshes.  Each mesh record is a tuple containing the following fields:
#    3.1) Mesh name.
#    3.2) Modifier bits that this mesh matches.
#     Note that the first mesh record is the default.
#  4) Item flags. See header_items.py for a list of available flags.
#  5) Item capabilities. Used for which animations this item is used with. 
#						 See header_items.py for a list of available flags.
#  6) Item value.
#  7) Item stats: Bitwise-or of various stats about the item such as:
#      weight, abundance, difficulty, head_armor, body_armor,leg_armor, etc...
#  8) Modifier bits: Modifiers that can be applied to this item.
#  9) [Optional] Triggers: List of simple triggers to be associated with the item.
#  10) [Optional] Factions: List of factions that item can be found as merchandise.
# #######################################################################


# #######################################################################
# 	imodbits/constants declarations
#
#	You can use use this as a way of declaring multiple imodbits for 
#	convenience/ ease of use. Native MS uses this as a way to combine
#	the hardcoded imodbits from header_items for ease of use.
# #######################################################################


imodbits_none = 0
imodbits_horse_basic = imodbit_swaybacked|imodbit_lame|imodbit_spirited|imodbit_heavy|imodbit_stubborn
imodbits_cloth  = imodbit_tattered | imodbit_ragged | imodbit_sturdy | imodbit_thick | imodbit_hardened
imodbits_armor  = imodbit_rusty | imodbit_battered | imodbit_crude | imodbit_thick | imodbit_reinforced |imodbit_lordly
imodbits_plate  = imodbit_cracked | imodbit_rusty | imodbit_battered | imodbit_crude | imodbit_thick | imodbit_reinforced |imodbit_lordly
imodbits_polearm = imodbit_cracked | imodbit_bent | imodbit_balanced
imodbits_shield  = imodbit_cracked | imodbit_battered |imodbit_thick | imodbit_reinforced
imodbits_sword   = imodbit_rusty | imodbit_chipped | imodbit_balanced |imodbit_tempered
imodbits_sword_high   = imodbit_rusty | imodbit_chipped | imodbit_balanced |imodbit_tempered|imodbit_masterwork
imodbits_axe   = imodbit_rusty | imodbit_chipped | imodbit_heavy
imodbits_mace   = imodbit_rusty | imodbit_chipped | imodbit_heavy
imodbits_pick   = imodbit_rusty | imodbit_chipped | imodbit_balanced | imodbit_heavy
imodbits_bow = imodbit_cracked | imodbit_bent | imodbit_strong |imodbit_masterwork
imodbits_crossbow = imodbit_cracked | imodbit_bent | imodbit_masterwork
imodbits_missile   = imodbit_bent | imodbit_large_bag
imodbits_thrown   = imodbit_bent | imodbit_heavy| imodbit_balanced| imodbit_large_bag
imodbits_thrown_minus_heavy = imodbit_bent | imodbit_balanced| imodbit_large_bag

imodbits_horse_good = imodbit_spirited|imodbit_heavy
imodbits_good   = imodbit_sturdy | imodbit_thick | imodbit_hardened | imodbit_reinforced
imodbits_bad    = imodbit_rusty | imodbit_chipped | imodbit_tattered | imodbit_ragged | imodbit_cracked | imodbit_bent
# Replace winged mace/spiked mace with: Flanged mace / Knobbed mace?
# Fauchard (majowski glaive) 

bullet_impact = (ti_on_missile_hit, [

	(set_fixed_point_multiplier, 100),
	
	(store_trigger_param_1, ":shooter"),
	(store_trigger_param_2, ":struck"),	
	
	(agent_get_position, pos10, ":shooter"),
	(get_distance_between_positions_in_meters, ":distance", pos1, pos10),
	(val_min, ":distance", 15),
	(call_script, "script_bullet_collision_particle", ":struck", ":distance"),
	# (set_spawn_position, pos1),
	# (spawn_scene_prop, "spr_GildedSphere"),
])
			
smaller_bullet_impact = (ti_on_missile_hit, [
	(store_trigger_param_1, ":shooter"),
	(store_trigger_param_2, ":struck"),
	(agent_get_position, pos10, ":shooter"),
	(get_distance_between_positions_in_meters, ":distance", pos1, pos10),
	(val_min, ":distance", 15),
	(val_div, ":distance", 2),
	(call_script, "script_bullet_collision_particle", ":struck", ":distance"),
			])
			
material_swap = (ti_on_init_item, [
	(store_trigger_param_1, ":agent"),
	(store_trigger_param_2, ":troop"),
	(neq, ":agent", -1),
	
	(assign, ":swap_needed", 0),
	
	(try_begin),
		(agent_slot_ge, ":agent", fate_agent_petrified, 1),
		
		(assign, ":material", "str_material_petrified"),
		(assign, ":swap_needed", 1),
	(else_try),
		(agent_slot_ge, ":agent", fate_agent_frozen, 1),
	
		(assign, ":material", "str_material_petrified"),
		(assign, ":swap_needed", 1),
	(else_try),
		(neq, ":troop", -1),
		(try_begin),
			(eq, ":troop", "trp_trimmau"),
			
			(assign, ":material", "str_material_mercury"),
			(assign, ":swap_needed", 1),
		(else_try),
			(eq, "$gilded", 1),
			
			(assign, ":material", "str_material_gilded"),
			(assign, ":swap_needed", 1),
		(else_try),
			(troop_slot_eq, ":troop", slot_fate_is_shadow, 1),
			
			(assign, ":material", "str_material_shadow"),
			(assign, ":swap_needed", 1),
		(try_end),
	
	(else_try),
		(agent_slot_eq, ":agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_04_01"),
		(agent_slot_ge, ":agent", slot_agent_active_np_timer, 1),
		
		(assign, ":material", "str_material_camo"),
		(assign, ":swap_needed", 1),
	(try_end),
	
	
	(neq, ":swap_needed", 0),
	(cur_item_set_material, ":material", -1),
		]),
		
attack_effects = (ti_on_weapon_attack, [
	(store_trigger_param_1, ":agent"),
	
	(neq, ":agent", -1),
	
	(agent_get_action_dir, ":dir", ":agent"),
	
	(try_begin),
		(neq, ":dir", 0),
		(particle_system_burst, "psys_slash", pos1, 1),
	(else_try),
		(particle_system_burst, "psys_stab", pos1, 1),
	(try_end),
		]),
items = [
# item_name, mesh_name, item_properties, item_capabilities, slot_no, cost, bonus_flags, weapon_flags, scale, view_dir, pos_offset
 ["no_item","INVALID ITEM", [("invalid_item",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary, itc_longsword, 3,weight(1.5)|spd_rtng(103)|weapon_length(90)|swing_damage(16,blunt)|thrust_damage(10,blunt),imodbits_none],

 ["tutorial_spear", "Spear", [("spear",0)], itp_type_polearm| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_spear, 0 , weight(4.5)|difficulty(0)|spd_rtng(80) | weapon_length(158)|swing_damage(0 , cut) | thrust_damage(19 ,  pierce),imodbits_polearm ],
 ["tutorial_club", "Club", [("club",0)], itp_type_one_handed_wpn| itp_primary|itp_wooden_parry|itp_wooden_attack, itc_scimitar, 0 , weight(2.5)|difficulty(0)|spd_rtng(95) | weapon_length(95)|swing_damage(11 , blunt) | thrust_damage(0 ,  pierce),imodbits_none ],
 ["tutorial_battle_axe", "Battle Axe", [("battle_ax",0)], itp_type_two_handed_wpn| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 0 , weight(5)|difficulty(0)|spd_rtng(88) | weapon_length(108)|swing_damage(27 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
 ["tutorial_arrows","Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(3)|abundance(160)|weapon_length(95)|thrust_damage(0,pierce)|max_ammo(20),imodbits_missile],
 ["tutorial_bolts","Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag", ixmesh_carry),("bolt_bag_b", ixmesh_carry|imodbit_large_bag)], itp_type_bolts, itcf_carry_quiver_right_vertical, 0,weight(2.25)|abundance(90)|weapon_length(55)|thrust_damage(0,pierce)|max_ammo(18),imodbits_missile],
 ["tutorial_short_bow", "Short Bow", [("short_bow",0),("short_bow_carry",ixmesh_carry)], itp_type_bow |itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bow_back, 0 , weight(1)|difficulty(0)|spd_rtng(98) | shoot_speed(49) | thrust_damage(12 ,  pierce  ),imodbits_bow ],
 ["tutorial_crossbow", "Crossbow", [("crossbow",0)], itp_type_crossbow |itp_primary|itp_two_handed|itp_cant_reload_on_horseback , itcf_shoot_crossbow|itcf_carry_crossbow_back, 0 , weight(3)|difficulty(0)|spd_rtng(42)|  shoot_speed(68) | thrust_damage(32,pierce)|max_ammo(1),imodbits_crossbow ],
 ["tutorial_throwing_daggers", "Throwing Daggers", [("throwing_dagger",0)], itp_type_thrown |itp_primary ,itcf_throw_knife, 0 , weight(3.5)|difficulty(0)|spd_rtng(102) | shoot_speed(25) | thrust_damage(16 ,  cut)|max_ammo(14)|weapon_length(0),imodbits_missile ],
 ["tutorial_saddle_horse", "Saddle Horse", [("saddle_horse",0)], itp_type_horse, 0, 0,abundance(90)|body_armor(3)|difficulty(0)|horse_speed(40)|horse_maneuver(38)|horse_charge(8),imodbits_horse_basic],
 ["tutorial_shield", "Kite Shield", [("shield_kite_a",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|weapon_length(150),imodbits_shield ],
 ["tutorial_staff_no_attack","Staff", [("wooden_staff",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack,itc_parry_polearm|itcf_carry_sword_back,9, weight(3.5)|spd_rtng(120) | weapon_length(115)|swing_damage(0,blunt) | thrust_damage(0,blunt),imodbits_none],
 ["tutorial_staff","Staff", [("wooden_staff",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack,itc_staff|itcf_carry_sword_back,9, weight(3.5)|spd_rtng(120) | weapon_length(115)|swing_damage(16,blunt) | thrust_damage(16,blunt),imodbits_none],
 ["tutorial_sword", "Sword", [("long_sword",0),("scab_longsw_a", ixmesh_carry)], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0 , weight(1.5)|difficulty(0)|spd_rtng(100) | weapon_length(102)|swing_damage(18 , cut) | thrust_damage(15 ,  pierce),imodbits_sword ],
 ["tutorial_axe", "Axe", [("iron_ax",0)], itp_type_two_handed_wpn| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 0 , weight(4)|difficulty(0)|spd_rtng(91) | weapon_length(108)|swing_damage(19 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],

 ["tutorial_dagger","Dagger", [("practice_dagger",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary, itc_longsword, 3,weight(1.5)|spd_rtng(103)|weapon_length(40)|swing_damage(16,blunt)|thrust_damage(10,blunt),imodbits_none],


 ["horse_meat","Horse Meat", [("raw_meat",0)], itp_type_goods|itp_consumable|itp_food, 0, 12,weight(40)|food_quality(30)|max_ammo(40),imodbits_none],
# Items before this point are hardwired and their order should not be changed!
 ["practice_sword","Practice Sword", [("practice_sword",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary|itp_wooden_parry|itp_wooden_attack, itc_longsword, 3,weight(1.5)|spd_rtng(103)|weapon_length(90)|swing_damage(22,blunt)|thrust_damage(20,blunt),imodbits_none],
 ["heavy_practice_sword","Heavy Practice Sword", [("heavy_practicesword",0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_wooden_parry|itp_wooden_attack, itc_greatsword,
    21, weight(6.25)|spd_rtng(94)|weapon_length(128)|swing_damage(30,blunt)|thrust_damage(24,blunt),imodbits_none],
 ["practice_dagger","Practice Dagger", [("practice_dagger",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary|itp_no_parry|itp_wooden_attack, itc_dagger|itcf_carry_dagger_front_left, 2,weight(0.5)|spd_rtng(110)|weapon_length(47)|swing_damage(16,blunt)|thrust_damage(14,blunt),imodbits_none],
 ["practice_axe", "Practice Axe", [("hatchet",0)], itp_type_one_handed_wpn| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 24 , weight(2) | spd_rtng(95) | weapon_length(75) | swing_damage(24, blunt) | thrust_damage(0, pierce), imodbits_axe],
 ["arena_axe", "Axe", [("arena_axe",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,
 137 , weight(1.5)|spd_rtng(100) | weapon_length(69)|swing_damage(24 , blunt) | thrust_damage(0 ,  pierce),imodbits_axe ],
 ["arena_sword", "Sword", [("arena_sword_one_handed",0),("sword_medieval_b_scabbard", ixmesh_carry),], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 243 , weight(1.5)|spd_rtng(99) | weapon_length(95)|swing_damage(22 , blunt) | thrust_damage(20 ,  blunt),imodbits_sword_high ],
 ["arena_sword_two_handed",  "Two Handed Sword", [("arena_sword_two_handed",0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back,
 670 , weight(2.75)|spd_rtng(93) | weapon_length(110)|swing_damage(30 , blunt) | thrust_damage(24 ,  blunt),imodbits_sword_high ],
 ["arena_lance",         "Lance", [("arena_lance",0)], itp_couchable|itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_staff|itcf_carry_spear,
 90 , weight(2.5)|spd_rtng(96) | weapon_length(150)|swing_damage(20 , blunt) | thrust_damage(25 ,  blunt),imodbits_polearm ],
 ["practice_staff","Practice Staff", [("wooden_staff",0)],itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack,itc_staff|itcf_carry_sword_back,9, weight(2.5)|spd_rtng(103) | weapon_length(118)|swing_damage(18,blunt) | thrust_damage(18,blunt),imodbits_none],
 ["practice_lance","Practice Lance", [("joust_of_peace",0)], itp_couchable|itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack, itc_greatlance, 18,weight(4.25)|spd_rtng(58)|weapon_length(240)|swing_damage(0,blunt)|thrust_damage(15,blunt),imodbits_none],
 ["practice_shield","Practice Shield", [("shield_round_a",0)], itp_type_shield|itp_wooden_parry, itcf_carry_round_shield, 20,weight(3.5)|body_armor(1)|hit_points(200)|spd_rtng(100)|shield_width(50),imodbits_none],
 ["practice_bow","Practice Bow", [("hunting_bow",0), ("hunting_bow_carry",ixmesh_carry)], itp_type_bow |itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bow_back, 0, weight(1.5)|spd_rtng(90) | shoot_speed(40) | thrust_damage(21, blunt),imodbits_bow ],
##                                                     ("hunting_bow",0)],                  itp_type_bow|itp_two_handed|itp_primary|itp_attach_left_hand, itcf_shoot_bow, 4,weight(1.5)|spd_rtng(90)|shoot_speed(40)|thrust_damage(19,blunt),imodbits_none],
 ["practice_crossbow", "Practice Crossbow", [("crossbow_a",0)], itp_type_crossbow |itp_primary|itp_two_handed ,itcf_shoot_crossbow|itcf_carry_crossbow_back, 0, weight(3)|spd_rtng(42)| shoot_speed(68) | thrust_damage(32,blunt)|max_ammo(1),imodbits_crossbow],
 ["practice_javelin", "Practice Javelins", [("javelin",0),("javelins_quiver_new", ixmesh_carry)], itp_type_thrown |itp_primary|itp_next_item_as_melee,itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 0, weight(5) | spd_rtng(91) | shoot_speed(28) | thrust_damage(27, blunt) | max_ammo(50) | weapon_length(75), imodbits_thrown],
 ["practice_javelin_melee", "practice_javelin_melee", [("javelin",0)], itp_type_polearm|itp_primary|itp_penalty_with_shield|itp_wooden_parry , itc_staff, 0, weight(1)|difficulty(0)|spd_rtng(91) |swing_damage(12, blunt)| thrust_damage(14,  blunt)|weapon_length(75),imodbits_polearm ],
 ["practice_throwing_daggers", "Throwing Daggers", [("throwing_dagger",0)], itp_type_thrown |itp_primary ,itcf_throw_knife, 0 , weight(3.5)|spd_rtng(102) | shoot_speed(25) | thrust_damage(16, blunt)|max_ammo(10)|weapon_length(0),imodbits_thrown ],
 ["practice_throwing_daggers_100_amount", "Throwing Daggers", [("throwing_dagger",0)], itp_type_thrown |itp_primary ,itcf_throw_knife, 0 , weight(3.5)|spd_rtng(102) | shoot_speed(25) | thrust_damage(16, blunt)|max_ammo(100)|weapon_length(0),imodbits_thrown ],
# ["cheap_shirt","Cheap Shirt", [("shirt",0)], itp_type_body_armor|itp_covers_legs, 0, 4,weight(1.25)|body_armor(3),imodbits_none],
 ["practice_horse","Practice Horse", [("saddle_horse",0)], itp_type_horse, 0, 37,body_armor(10)|horse_speed(40)|horse_maneuver(37)|horse_charge(14),imodbits_none],
 ["practice_arrows","Practice Arrows", [("arena_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(1.5)|weapon_length(95)|max_ammo(80),imodbits_missile],
## ["practice_arrows","Practice Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo)], itp_type_arrows, 0, 31,weight(1.5)|weapon_length(95)|max_ammo(80),imodbits_none],
 ["practice_bolts","Practice Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag", ixmesh_carry),("bolt_bag_b", ixmesh_carry|imodbit_large_bag)], itp_type_bolts, itcf_carry_quiver_right_vertical, 0,weight(2.25)|weapon_length(55)|max_ammo(49),imodbits_missile],
 ["practice_arrows_10_amount","Practice Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(1.5)|weapon_length(95)|max_ammo(10),imodbits_missile],
 ["practice_arrows_100_amount","Practice Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(1.5)|weapon_length(95)|max_ammo(100),imodbits_missile],
 ["practice_bolts_9_amount","Practice Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag", ixmesh_carry),("bolt_bag_b", ixmesh_carry|imodbit_large_bag)], itp_type_bolts, itcf_carry_quiver_right_vertical, 0,weight(2.25)|weapon_length(55)|max_ammo(9),imodbits_missile],
 ["practice_boots", "Practice Boots", [("boot_nomad_a",0)], itp_type_foot_armor |itp_civilian  | itp_attach_armature,0, 11 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(10), imodbits_cloth ],
 ["red_tourney_armor","Red Tourney Armor", [("tourn_armor_a",0)], itp_type_body_armor|itp_covers_legs, 0, 152,weight(15.0)|body_armor(20)|leg_armor(6),imodbits_none],
 ["blue_tourney_armor","Blue Tourney Armor", [("mail_shirt",0)], itp_type_body_armor|itp_covers_legs, 0, 152,weight(15.0)|body_armor(20)|leg_armor(6),imodbits_none],
 ["green_tourney_armor","Green Tourney Armor", [("leather_vest",0)], itp_type_body_armor|itp_covers_legs, 0, 152,weight(15.0)|body_armor(20)|leg_armor(6),imodbits_none],
 ["gold_tourney_armor","Gold Tourney Armor", [("padded_armor",0)], itp_type_body_armor|itp_covers_legs, 0, 152,weight(15.0)|body_armor(20)|leg_armor(6),imodbits_none],
 ["red_tourney_helmet","Red Tourney Helmet",[("flattop_helmet",0)],itp_type_head_armor,0,126, weight(2)|head_armor(16),imodbits_none],
 ["blue_tourney_helmet","Blue Tourney Helmet",[("segmented_helm",0)],itp_type_head_armor,0,126, weight(2)|head_armor(16),imodbits_none],
 ["green_tourney_helmet","Green Tourney Helmet",[("hood_c",0)],itp_type_head_armor,0,126, weight(2)|head_armor(16),imodbits_none],
 ["gold_tourney_helmet","Gold Tourney Helmet",[("hood_a",0)],itp_type_head_armor,0,126, weight(2)|head_armor(16),imodbits_none],

["arena_shield_red", "Shield", [("arena_shield_red",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|weapon_length(60),imodbits_shield ],
["arena_shield_blue", "Shield", [("arena_shield_blue",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|weapon_length(60),imodbits_shield ],
["arena_shield_green", "Shield", [("arena_shield_green",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|weapon_length(60),imodbits_shield ],
["arena_shield_yellow", "Shield", [("arena_shield_yellow",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|weapon_length(60),imodbits_shield ],

["arena_armor_white", "Arena Armor White", [("arena_armorW_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_armor_red", "Arena Armor Red", [("arena_armorR_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_armor_blue", "Arena Armor Blue", [("arena_armorB_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_armor_green", "Arena Armor Green", [("arena_armorG_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_armor_yellow", "Arena Armor Yellow", [("arena_armorY_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(29)|leg_armor(13), imodbits_armor ],
["arena_tunic_white", "Arena Tunic White ", [("arena_tunicW_new",0)], itp_type_body_armor |itp_covers_legs ,0, 47 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ],
["arena_tunic_red", "Arena Tunic Red", [("arena_tunicR_new",0)], itp_type_body_armor |itp_covers_legs ,0, 27 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ], 
["arena_tunic_blue", "Arena Tunic Blue", [("arena_tunicB_new",0)], itp_type_body_armor |itp_covers_legs ,0, 27 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ], 
["arena_tunic_green", "Arena Tunic Green", [("arena_tunicG_new",0)], itp_type_body_armor |itp_covers_legs ,0, 27 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ],
["arena_tunic_yellow", "Arena Tunic Yellow", [("arena_tunicY_new",0)], itp_type_body_armor |itp_covers_legs ,0, 27 , weight(2)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(6), imodbits_cloth ],
#headwear
["arena_helmet_red", "Arena Helmet Red", [("arena_helmetR",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_helmet_blue", "Arena Helmet Blue", [("arena_helmetB",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_helmet_green", "Arena Helmet Green", [("arena_helmetG",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_helmet_yellow", "Arena Helmet Yellow", [("arena_helmetY",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["steppe_helmet_white", "Steppe Helmet White", [("steppe_helmetW",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ], 
["steppe_helmet_red", "Steppe Helmet Red", [("steppe_helmetR",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ], 
["steppe_helmet_blue", "Steppe Helmet Blue", [("steppe_helmetB",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ], 
["steppe_helmet_green", "Steppe Helmet Green", [("steppe_helmetG",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ], 
["steppe_helmet_yellow", "Steppe Helmet Yellow", [("steppe_helmetY",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0), imodbits_plate ], 
["tourney_helm_white", "Tourney Helm White", [("tourney_helmR",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["tourney_helm_red", "Tourney Helm Red", [("tourney_helmR",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["tourney_helm_blue", "Tourney Helm Blue", [("tourney_helmB",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["tourney_helm_green", "Tourney Helm Green", [("tourney_helmG",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["tourney_helm_yellow", "Tourney Helm Yellow", [("tourney_helmY",0)], itp_type_head_armor|itp_covers_head,0, 760 , weight(2.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_turban_red", "Arena Turban", [("tuareg_open",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_turban_blue", "Arena Turban", [("tuareg_open",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_turban_green", "Arena Turban", [("tuareg_open",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],
["arena_turban_yellow", "Arena Turban", [("tuareg_open",0)], itp_type_head_armor|itp_fit_to_head ,0, 187 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0), imodbits_plate ],

# A treatise on The Method of Mechanical Theorems Archimedes
 
#This book must be at the beginning of readable books
 ["book_tactics","De Re Militari", [("book_a",0)], itp_type_book, 0, 4000,weight(2)|abundance(100),imodbits_none],
 ["book_persuasion","Rhetorica ad Herennium", [("book_b",0)], itp_type_book, 0, 5000,weight(2)|abundance(100),imodbits_none],
 ["book_leadership","The Life of Alixenus the Great", [("book_d",0)], itp_type_book, 0, 4200,weight(2)|abundance(100),imodbits_none],
 ["book_intelligence","Essays on Logic", [("book_e",0)], itp_type_book, 0, 2900,weight(2)|abundance(100),imodbits_none],
 ["book_trade","A Treatise on the Value of Things", [("book_f",0)], itp_type_book, 0, 3100,weight(2)|abundance(100),imodbits_none],
 ["book_weapon_mastery", "On the Art of Fighting with Swords", [("book_d",0)], itp_type_book, 0, 4200,weight(2)|abundance(100),imodbits_none],
 ["book_engineering","Method of Mechanical Theorems", [("book_open",0)], itp_type_book, 0, 4000,weight(2)|abundance(100),imodbits_none],

#Reference books
#This book must be at the beginning of reference books
 ["book_wound_treatment_reference","The Book of Healing", [("book_c",0)], itp_type_book, 0, 3500,weight(2)|abundance(100),imodbits_none],
 ["book_training_reference","Manual of Arms", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(100),imodbits_none],
 ["book_surgery_reference","The Great Book of Surgery", [("book_c",0)], itp_type_book, 0, 3500,weight(2)|abundance(100),imodbits_none],

 #other trade goods (first one is spice)
 ["spice","Spice", [("spice_sack",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 880,weight(40)|abundance(25)|max_ammo(50),imodbits_none],
 ["salt","Salt", [("salt_sack",0)], itp_merchandise|itp_type_goods, 0, 255,weight(50)|abundance(120),imodbits_none],


 #["flour","Flour", [("salt_sack",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 40,weight(50)|abundance(100)|food_quality(45)|max_ammo(50),imodbits_none],

 ["oil","Oil", [("oil",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 450,weight(50)|abundance(60)|max_ammo(50),imodbits_none],

 ["pottery","Pottery", [("jug",0)], itp_merchandise|itp_type_goods, 0, 100,weight(50)|abundance(90),imodbits_none],

 ["raw_flax","Flax Bundle", [("raw_flax",0)], itp_merchandise|itp_type_goods, 0, 150,weight(40)|abundance(90),imodbits_none],
 ["linen","Linen", [("linen",0)], itp_merchandise|itp_type_goods, 0, 250,weight(40)|abundance(90),imodbits_none],

 ["wool","Wool", [("wool_sack",0)], itp_merchandise|itp_type_goods, 0, 130,weight(40)|abundance(90),imodbits_none],
 ["wool_cloth","Wool Cloth", [("wool_cloth",0)], itp_merchandise|itp_type_goods, 0, 250,weight(40)|abundance(90),imodbits_none],

 ["raw_silk","Raw Silk", [("raw_silk_bundle",0)], itp_merchandise|itp_type_goods, 0, 600,weight(30)|abundance(90),imodbits_none],
 ["raw_dyes","Dyes", [("dyes",0)], itp_merchandise|itp_type_goods, 0, 200,weight(10)|abundance(90),imodbits_none],
 ["velvet","Velvet", [("velvet",0)], itp_merchandise|itp_type_goods, 0, 1025,weight(40)|abundance(30),imodbits_none],

 ["iron","Iron", [("iron",0)], itp_merchandise|itp_type_goods, 0,264,weight(60)|abundance(60),imodbits_none],
 ["tools","Tools", [("iron_hammer",0)], itp_merchandise|itp_type_goods, 0, 410,weight(50)|abundance(90),imodbits_none],

 ["raw_leather","Hides", [("leatherwork_inventory",0)], itp_merchandise|itp_type_goods, 0, 120,weight(40)|abundance(90),imodbits_none],
 ["leatherwork","Leatherwork", [("leatherwork_frame",0)], itp_merchandise|itp_type_goods, 0, 220,weight(40)|abundance(90),imodbits_none],
 
 ["raw_date_fruit","Date Fruit", [("date_inventory",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 120,weight(40)|food_quality(10)|max_ammo(10),imodbits_none],
 ["furs","Furs", [("fur_pack",0)], itp_merchandise|itp_type_goods, 0, 391,weight(40)|abundance(90),imodbits_none],

 ["wine","Wine", [("amphora_slim",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 220,weight(30)|abundance(60)|max_ammo(50),imodbits_none],
 ["ale","Ale", [("ale_barrel",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 120,weight(30)|abundance(70)|max_ammo(50),imodbits_none],

# ["dry_bread", "wheat_sack", itp_type_goods|itp_consumable, 0, slt_none,view_goods,95,weight(2),max_ammo(50),imodbits_none],
#foods (first one is smoked_fish)
 ["smoked_fish","Smoked Fish", [("smoked_fish",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(110)|food_quality(50)|max_ammo(50),imodbits_none],
 ["cheese","Cheese", [("cheese_b",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 75,weight(6)|abundance(110)|food_quality(40)|max_ammo(30),imodbits_none],
 ["honey","Honey", [("honey_pot",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 220,weight(5)|abundance(110)|food_quality(40)|max_ammo(30),imodbits_none],
 ["sausages","Sausages", [("sausages",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 85,weight(10)|abundance(110)|food_quality(40)|max_ammo(40),imodbits_none],
 ["cabbages","Cabbages", [("cabbage",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 30,weight(15)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],
 ["dried_meat","Dried Meat", [("smoked_meat",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 85,weight(15)|abundance(100)|food_quality(70)|max_ammo(50),imodbits_none],
 ["apples","Fruit", [("apple_basket",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 44,weight(20)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],
 ["raw_grapes","Grapes", [("grapes_inventory",0)], itp_merchandise|itp_consumable|itp_type_goods, 0, 75,weight(40)|abundance(90)|food_quality(10)|max_ammo(10),imodbits_none], #x2 for wine
 ["raw_olives","Olives", [("olive_inventory",0)], itp_merchandise|itp_consumable|itp_type_goods, 0, 100,weight(40)|abundance(90)|food_quality(10)|max_ammo(10),imodbits_none], #x3 for oil
 ["grain","Grain", [("wheat_sack",0)], itp_merchandise|itp_type_goods|itp_consumable, 0, 30,weight(30)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],

 ["cattle_meat","Beef", [("raw_meat",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 80,weight(20)|abundance(100)|food_quality(80)|max_ammo(50),imodbits_none],
 ["bread","Bread", [("bread_a",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 50,weight(30)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],
 ["chicken","Chicken", [("chicken",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 95,weight(10)|abundance(110)|food_quality(40)|max_ammo(50),imodbits_none],
 ["pork","Pork", [("pork",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 75,weight(15)|abundance(100)|food_quality(70)|max_ammo(50),imodbits_none],
 ["butter","Butter", [("butter_pot",0)], itp_merchandise|itp_type_goods|itp_consumable|itp_food, 0, 150,weight(6)|abundance(110)|food_quality(40)|max_ammo(30),imodbits_none],
 

 #Would like to remove flour altogether and reduce chicken, pork and butter (perishables) to non-trade items. Apples could perhaps become a generic "fruit", also representing dried fruit and grapes
 # Armagan: changed order so that it'll be easier to remove them from trade goods if necessary.
#************************************************************************************************
# ITEMS before this point are hardcoded into item_codes.h and their order should not be changed!
#************************************************************************************************

# Quest Items

 ["siege_supply","Supplies", [("ale_barrel",0)], itp_type_goods, 0, 96,weight(40)|abundance(70),imodbits_none],
 ["quest_wine","Wine", [("amphora_slim",0)], itp_type_goods, 0, 46,weight(40)|abundance(60)|max_ammo(50),imodbits_none],
 ["quest_ale","Ale", [("ale_barrel",0)], itp_type_goods, 0, 31,weight(40)|abundance(70)|max_ammo(50),imodbits_none],

# Horses: sumpter horse/ pack horse, saddle horse, steppe horse, warm blood, geldling, stallion,   war mount, charger, 
# Carthorse, hunter, heavy hunter, hackney, palfrey, courser, destrier.
 ["sumpter_horse","Sumpter Horse", [("sumpter_horse",0)], itp_merchandise|itp_type_horse, 0, 134,abundance(90)|hit_points(100)|body_armor(14)|difficulty(1)|horse_speed(37)|horse_maneuver(39)|horse_charge(9)|horse_scale(100),imodbits_horse_basic],
 ["saddle_horse","Saddle Horse", [("saddle_horse",0),("horse_c",imodbits_horse_good)], itp_merchandise|itp_type_horse, 0, 240,abundance(90)|hit_points(100)|body_armor(8)|difficulty(1)|horse_speed(45)|horse_maneuver(44)|horse_charge(10)|horse_scale(104),imodbits_horse_basic],
 ["steppe_horse","Steppe Horse", [("steppe_horse",0)], itp_merchandise|itp_type_horse, 0, 192,abundance(80)|hit_points(120)|body_armor(10)|difficulty(2)|horse_speed(40)|horse_maneuver(51)|horse_charge(8)|horse_scale(98),imodbits_horse_basic, [], [fac_kingdom_2, fac_kingdom_3]],
 ["arabian_horse_a","Desert Horse", [("arabian_horse_a",0)], itp_merchandise|itp_type_horse, 0, 550,abundance(80)|hit_points(110)|body_armor(10)|difficulty(2)|horse_speed(42)|horse_maneuver(50)|horse_charge(12)|horse_scale(100),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_3, fac_kingdom_6]],
 ["courser","Courser", [("courser",0)], itp_merchandise|itp_type_horse, 0, 600,abundance(70)|body_armor(12)|hit_points(110)|difficulty(2)|horse_speed(50)|horse_maneuver(44)|horse_charge(12)|horse_scale(106),imodbits_horse_basic|imodbit_champion],
 ["arabian_horse_b","Sarranid Horse", [("arabian_horse_b",0)], itp_merchandise|itp_type_horse, 0, 700,abundance(80)|hit_points(120)|body_armor(10)|difficulty(3)|horse_speed(43)|horse_maneuver(54)|horse_charge(16)|horse_scale(100),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_6]],
 ["hunter","Hunter", [("hunting_horse",0),("hunting_horse",imodbits_horse_good)], itp_merchandise|itp_type_horse, 0, 810,abundance(60)|hit_points(160)|body_armor(18)|difficulty(3)|horse_speed(43)|horse_maneuver(44)|horse_charge(24)|horse_scale(108),imodbits_horse_basic|imodbit_champion],
 ["warhorse","War Horse", [("warhorse_chain",0)], itp_merchandise|itp_type_horse, 0, 1224,abundance(50)|hit_points(165)|body_armor(40)|difficulty(4)|horse_speed(40)|horse_maneuver(41)|horse_charge(28)|horse_scale(110),imodbits_horse_basic|imodbit_champion],
 ["charger","Charger", [("charger_new",0)], itp_merchandise|itp_type_horse, 0, 1811,abundance(40)|hit_points(165)|body_armor(58)|difficulty(4)|horse_speed(40)|horse_maneuver(44)|horse_charge(32)|horse_scale(112),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_1, fac_kingdom_5]],



#whalebone crossbow, yew bow, war bow, arming sword 
 ["arrows","Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows|itp_merchandise|itp_default_ammo, itcf_carry_quiver_back, 
 72,weight(3)|abundance(160)|weapon_length(95)|thrust_damage(1,pierce)|max_ammo(30),imodbits_missile, [bullet_impact]],
 ["khergit_arrows","Khergit Arrows", [("arrow_b",0),("flying_missile",ixmesh_flying_ammo),("quiver_b", ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back_right, 
 410,weight(3.5)|abundance(30)|weapon_length(95)|thrust_damage(3,pierce)|max_ammo(30),imodbits_missile, [bullet_impact]],
 ["barbed_arrows","Barbed Arrows", [("barbed_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver_d", ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back_right, 
 124,weight(3)|abundance(70)|weapon_length(95)|thrust_damage(2,pierce)|max_ammo(30),imodbits_missile, [bullet_impact]],
 ["bodkin_arrows","Bodkin Arrows", [("piercing_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver_c", ixmesh_carry)], itp_type_arrows|itp_merchandise, itcf_carry_quiver_back_right, 
 350,weight(3)|abundance(50)|weapon_length(91)|thrust_damage(3,pierce)|max_ammo(28),imodbits_missile, [bullet_impact]],
 ["bolts","Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag", ixmesh_carry),("bolt_bag_b", ixmesh_carry|imodbit_large_bag)], itp_type_bolts|itp_merchandise|itp_default_ammo|itp_can_penetrate_shield, itcf_carry_quiver_right_vertical, 
 64,weight(2.25)|abundance(90)|weapon_length(63)|thrust_damage(1,pierce)|max_ammo(29),imodbits_missile, [bullet_impact]],
 ["steel_bolts","Steel Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag_c", ixmesh_carry)], itp_type_bolts|itp_merchandise|itp_can_penetrate_shield, itcf_carry_quiver_right_vertical, 
 210,weight(2.5)|abundance(20)|weapon_length(63)|thrust_damage(2,pierce)|max_ammo(29),imodbits_missile, [bullet_impact]],
 ["cartridges","Cartridges", [("cartridge_a",0)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield, 0, 
 41,weight(2.25)|abundance(90)|weapon_length(3)|thrust_damage(1,pierce)|max_ammo(50),imodbits_missile, [bullet_impact]],

["pilgrim_disguise", "Pilgrim Disguise", [("pilgrim_outfit",0)], 0| itp_type_body_armor |itp_covers_legs |itp_civilian ,0, 25 , weight(2)|abundance(100)|head_armor(0)|body_armor(19)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["pilgrim_hood", "Pilgrim Hood", [("pilgrim_hood",0)], 0| itp_type_head_armor |itp_civilian  ,0, 35 , weight(1.25)|abundance(100)|head_armor(14)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],

# ARMOR
#handwear
["leather_gloves","Leather Gloves", [("leather_gloves_L",0)], itp_merchandise|itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],
["mail_mittens","Mail Mittens", [("mail_mittens_L",0)], itp_merchandise|itp_type_hand_armor,0, 350, weight(0.5)|abundance(100)|body_armor(4)|difficulty(0),imodbits_armor],
["scale_gauntlets","Scale Gauntlets", [("scale_gauntlets_b_L",0)], itp_merchandise|itp_type_hand_armor,0, 710, weight(0.75)|abundance(100)|body_armor(5)|difficulty(0),imodbits_armor],
["lamellar_gauntlets","Lamellar Gauntlets", [("scale_gauntlets_a_L",0)], itp_merchandise|itp_type_hand_armor,0, 910, weight(0.9)|abundance(100)|body_armor(6)|difficulty(0),imodbits_armor],
["gauntlets","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_merchandise|itp_type_hand_armor,0, 1040, weight(1.0)|abundance(100)|body_armor(7)|difficulty(0),imodbits_armor],

#footwear
["wrapping_boots", "Wrapping Boots", [("wrapping_boots_a",0)], itp_merchandise| itp_type_foot_armor |itp_civilian | itp_attach_armature ,0,
 3 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0) ,imodbits_cloth ],
["woolen_hose", "Woolen Hose", [("woolen_hose_a",0)], itp_merchandise| itp_type_foot_armor |itp_civilian | itp_attach_armature ,0,
 6 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(4)|difficulty(0) ,imodbits_cloth ],
["blue_hose", "Blue Hose", [("blue_hose_a",0)], itp_merchandise| itp_type_foot_armor |itp_civilian | itp_attach_armature ,0,
 11 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(5)|difficulty(0) ,imodbits_cloth ],
["hunter_boots", "Hunter Boots", [("hunter_boots_a",0)], itp_merchandise| itp_type_foot_armor |itp_civilian | itp_attach_armature,0,
 19 , weight(1.25)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(9)|difficulty(0) ,imodbits_cloth ],
["hide_boots", "Hide Boots", [("hide_boots_a",0)], itp_merchandise| itp_type_foot_armor |itp_civilian  | itp_attach_armature,0,
 34 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["ankle_boots", "Ankle Boots", [("ankle_boots_a_new",0)], itp_merchandise| itp_type_foot_armor |itp_civilian  | itp_attach_armature,0,
 75 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["nomad_boots", "Nomad Boots", [("nomad_boots_a",0)], itp_merchandise| itp_type_foot_armor  |itp_civilian | itp_attach_armature,0,
 90 , weight(1.25)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(14)|difficulty(0) ,imodbits_cloth ],
["leather_boots", "Leather Boots", [("leather_boots_a",0)], itp_merchandise| itp_type_foot_armor  |itp_civilian | itp_attach_armature,0,
 174 , weight(1.25)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(16)|difficulty(0) ,imodbits_cloth ],
["splinted_leather_greaves", "Splinted Leather Greaves", [("leather_greaves_a",0)], itp_merchandise| itp_type_foot_armor | itp_attach_armature,0,
 310 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(21)|difficulty(0) ,imodbits_armor ],
["mail_chausses", "Mail Chausses", [("mail_chausses_a",0)], itp_merchandise| itp_type_foot_armor | itp_attach_armature  ,0,
 530 , weight(3)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(24)|difficulty(0) ,imodbits_armor ],
["splinted_greaves", "Splinted Greaves", [("splinted_greaves_a",0)], itp_merchandise| itp_type_foot_armor | itp_attach_armature,0,
 853 , weight(2.75)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(28)|difficulty(7) ,imodbits_armor ],
["mail_boots", "Mail Boots", [("mail_boots_a",0)], itp_merchandise| itp_type_foot_armor | itp_attach_armature  ,0,
 1250 , weight(3)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(31)|difficulty(8) ,imodbits_armor ],
["iron_greaves", "Iron Greaves", [("iron_greaves_a",0)], itp_merchandise| itp_type_foot_armor | itp_attach_armature,0,
 1770 , weight(3.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(9) ,imodbits_armor ],
["black_greaves", "Black Greaves", [("black_greaves",0)], itp_type_foot_armor  | itp_attach_armature,0,
 2361 , weight(3.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(35)|difficulty(0) ,imodbits_armor ],
["khergit_leather_boots", "Khergit Leather Boots", [("khergit_leather_boots",0)], itp_merchandise| itp_type_foot_armor |itp_civilian | itp_attach_armature ,0,
 120 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(18)|difficulty(0) ,imodbits_cloth ],
["sarranid_boots_a", "Sarranid Shoes", [("sarranid_shoes",0)], itp_type_foot_armor |itp_civilian | itp_attach_armature ,0,
 30 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["sarranid_boots_b", "Sarranid Leather Boots", [("sarranid_boots",0)], itp_merchandise| itp_type_foot_armor |itp_civilian | itp_attach_armature ,0,
 120 , weight(2)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(16)|difficulty(0) ,imodbits_cloth ],
["sarranid_boots_c", "Plated Boots", [("sarranid_camel_boots",0)], itp_merchandise| itp_type_foot_armor |itp_civilian | itp_attach_armature ,0,
 280 , weight(3)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(20)|difficulty(0) ,imodbits_plate ],
["sarranid_boots_d", "Sarranid Mail Boots", [("sarranid_mail_chausses",0)], itp_merchandise| itp_type_foot_armor |itp_civilian | itp_attach_armature ,0,
 920 , weight(3)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(30)|difficulty(0) ,imodbits_armor ],

["sarranid_head_cloth", "Lady Head Cloth", [("tulbent",0)],  itp_type_head_armor | itp_doesnt_cover_hair |itp_civilian |itp_attach_armature,0, 1 , weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["sarranid_head_cloth_b", "Lady Head Cloth", [("tulbent_b",0)],  itp_type_head_armor | itp_doesnt_cover_hair |itp_civilian |itp_attach_armature,0, 1 , weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["sarranid_felt_head_cloth", "Head Cloth", [("common_tulbent",0)],  itp_type_head_armor  |itp_civilian |itp_attach_armature,0, 1 , weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["sarranid_felt_head_cloth_b", "Head Cloth", [("common_tulbent_b",0)],  itp_type_head_armor  |itp_civilian |itp_attach_armature,0, 1 , weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],


#bodywear
["lady_dress_ruby", "Lady Dress", [("lady_dress_r",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 500 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["lady_dress_green", "Lady Dress", [("lady_dress_g",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 500 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["lady_dress_blue", "Lady Dress", [("lady_dress_b",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 500 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["red_dress", "Red Dress", [("red_dress",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 500 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["brown_dress", "Brown Dress", [("brown_dress",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 500 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["green_dress", "Green Dress", [("green_dress",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 500 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["khergit_lady_dress", "Khergit Lady Dress", [("khergit_lady_dress",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 500 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["khergit_lady_dress_b", "Khergit Leather Lady Dress", [("khergit_lady_dress_b",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 500 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["sarranid_lady_dress", "Sarranid Lady Dress", [("sarranid_lady_dress",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 500 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["sarranid_lady_dress_b", "Sarranid Lady Dress", [("sarranid_lady_dress_b",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 500 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["sarranid_common_dress", "Sarranid Dress", [("sarranid_common_dress",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 500 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["sarranid_common_dress_b", "Sarranid Dress", [("sarranid_common_dress_b",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 500 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["courtly_outfit", "Courtly Outfit", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian   ,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["nobleman_outfit", "Nobleman Outfit", [("nobleman_outfit_b_new",0)], itp_type_body_armor|itp_covers_legs|itp_civilian   ,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(15)|leg_armor(12)|difficulty(0) ,imodbits_cloth ], 
["nomad_armor", "Nomad Armor", [("nomad_armor_new",0)], itp_merchandise| itp_type_body_armor |itp_covers_legs   ,0, 25 , weight(2)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["khergit_armor", "Khergit Armor", [("khergit_armor_new",0)], itp_merchandise| itp_type_body_armor | itp_covers_legs ,0, 38 , weight(2)|abundance(100)|head_armor(0)|body_armor(24)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["leather_jacket", "Leather Jacket", [("leather_jacket_new",0)], itp_merchandise| itp_type_body_armor | itp_covers_legs  |itp_civilian ,0, 50 , weight(3)|abundance(100)|head_armor(0)|body_armor(20)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],

#NEW:
["rawhide_coat", "Rawhide Coat", [("coat_of_plates_b",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0, 12 , weight(5)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
#NEW: was lthr_armor_a
["leather_armor", "Leather Armor", [("tattered_leather_armor_a",0)], itp_merchandise| itp_type_body_armor |itp_covers_legs  ,0, 65 , weight(7)|abundance(100)|head_armor(0)|body_armor(18)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["fur_coat", "Fur Coat", [("fur_coat",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0, 117 , weight(6)|abundance(100)|head_armor(0)|body_armor(13)|leg_armor(6)|difficulty(0) ,imodbits_armor ],



#for future:
["coat", "Coat", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["leather_coat", "Leather Coat", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["mail_coat", "Coat of Mail", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["long_mail_coat", "Long Coat of Mail", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["mail_with_tunic_red", "Mail with Tunic", [("arena_armorR_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(39)|leg_armor(8), imodbits_armor ],
["mail_with_tunic_green", "Mail with Tunic", [("arena_armorG_new",0)], itp_type_body_armor  |itp_covers_legs ,0, 650 , weight(16)|abundance(100)|head_armor(0)|body_armor(39)|leg_armor(8), imodbits_armor ],
["hide_coat", "Hide Coat", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["merchant_outfit", "Merchant Outfit", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["homespun_dress", "Homespun Dress", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["thick_coat", "Thick Coat", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["coat_with_cape", "Coat with Cape", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["steppe_outfit", "Steppe Outfit", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["nordic_outfit", "Nordic Outfit", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["nordic_armor", "Nordic Armor", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["hide_armor", "Hide Armor", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["cloaked_tunic", "Cloaked Tunic", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["sleeveless_tunic", "Sleeveless Tunic", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["sleeveless_leather_tunic", "Sleeveless Leather Tunic", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["linen_shirt", "Linen Shirt", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["wool_coat", "Wool Coat", [("nobleman_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
#end

["dress", "Dress", [("dress",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 6 , weight(1)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(2)|difficulty(0) ,imodbits_cloth ],
["blue_dress", "Blue Dress", [("blue_dress_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 6 , weight(1)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(2)|difficulty(0) ,imodbits_cloth ],
["peasant_dress", "Peasant Dress", [("peasant_dress_b_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 6 , weight(1)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(2)|difficulty(0) ,imodbits_cloth ], 
["woolen_dress", "Woolen Dress", [("woolen_dress",0)], itp_merchandise| itp_type_body_armor|itp_civilian  |itp_covers_legs ,0,
 10 , weight(1.75)|abundance(100)|head_armor(0)|body_armor(8)|leg_armor(2)|difficulty(0) ,imodbits_cloth ],
["shirt", "Shirt", [("shirt",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0,
 3 , weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["linen_tunic", "Linen Tunic", [("shirt_a",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 6 , weight(1)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(1)|difficulty(0) ,imodbits_cloth ],
["short_tunic", "Red Tunic", [("rich_tunic_a",0)], itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 10 , weight(1)|abundance(100)|head_armor(0)|body_armor(7)|leg_armor(1)|difficulty(0) ,imodbits_cloth ],
 ["red_shirt", "Red Shirt", [("rich_tunic_a",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 10 , weight(1)|abundance(100)|head_armor(0)|body_armor(7)|leg_armor(1)|difficulty(0) ,imodbits_cloth ],
 ["red_tunic", "Red Tunic", [("arena_tunicR_new",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 10 , weight(1)|abundance(100)|head_armor(0)|body_armor(7)|leg_armor(1)|difficulty(0) ,imodbits_cloth ],

 ["green_tunic", "Green Tunic", [("arena_tunicG_new",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 10 , weight(1)|abundance(100)|head_armor(0)|body_armor(7)|leg_armor(1)|difficulty(0) ,imodbits_cloth ],
 ["blue_tunic", "Blue Tunic", [("arena_tunicB_new",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 10 , weight(1)|abundance(100)|head_armor(0)|body_armor(7)|leg_armor(1)|difficulty(0) ,imodbits_cloth ],
["robe", "Robe", [("robe",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 31 , weight(1.5)|abundance(100)|head_armor(0)|body_armor(8)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
#NEW: was coarse_tunic
["coarse_tunic", "Tunic with vest", [("coarse_tunic_a",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 47 , weight(2)|abundance(100)|head_armor(0)|body_armor(11)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
["leather_apron", "Leather Apron", [("leather_apron",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 61 , weight(3)|abundance(100)|head_armor(0)|body_armor(12)|leg_armor(7)|difficulty(0) ,imodbits_cloth ],
#NEW: was tabard_a
["tabard", "Tabard", [("tabard_b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 107 , weight(3)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
#NEW: was leather_vest
["leather_vest", "Leather Vest", [("leather_vest_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0,
 146 , weight(4)|abundance(100)|head_armor(0)|body_armor(15)|leg_armor(7)|difficulty(0) ,imodbits_cloth ],
["steppe_armor", "Steppe Armor", [("lamellar_leather",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 195 , weight(5)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["gambeson", "Gambeson", [("white_gambeson",0)], itp_merchandise| itp_type_body_armor|itp_covers_legs|itp_civilian,0,
 260 , weight(5)|abundance(100)|head_armor(0)|body_armor(20)|leg_armor(5)|difficulty(0) ,imodbits_cloth ],
["blue_gambeson", "Blue Gambeson", [("blue_gambeson",0)], itp_merchandise| itp_type_body_armor|itp_covers_legs|itp_civilian,0,
 270 , weight(5)|abundance(100)|head_armor(0)|body_armor(21)|leg_armor(5)|difficulty(0) ,imodbits_cloth ],
#NEW: was red_gambeson
["red_gambeson", "Red Gambeson", [("red_gambeson_a",0)], itp_merchandise| itp_type_body_armor|itp_covers_legs|itp_civilian,0,
 275 , weight(5)|abundance(100)|head_armor(0)|body_armor(21)|leg_armor(5)|difficulty(0) ,imodbits_cloth ],
#NEW: was aketon_a
["padded_cloth", "Aketon", [("padded_cloth_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 297 , weight(11)|abundance(100)|head_armor(0)|body_armor(22)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
#NEW:
 ["aketon_green", "Padded Cloth", [("padded_cloth_b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 297 , weight(11)|abundance(100)|head_armor(0)|body_armor(22)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
 #NEW: was "leather_jerkin"
["leather_jerkin", "Leather Jerkin", [("ragged_leather_jerkin",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 321 , weight(6)|abundance(100)|head_armor(0)|body_armor(23)|leg_armor(6)|difficulty(0) ,imodbits_cloth ],
["nomad_vest", "Nomad Vest", [("nomad_vest_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0,
 360 , weight(7)|abundance(50)|head_armor(0)|body_armor(22)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],
["ragged_outfit", "Ragged Outfit", [("ragged_outfit_a_new",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 390 , weight(7)|abundance(100)|head_armor(0)|body_armor(23)|leg_armor(9)|difficulty(0) ,imodbits_cloth ],
 #NEW: was padded_leather
["padded_leather", "Padded Leather", [("leather_armor_b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian,0,
 454 , weight(12)|abundance(100)|head_armor(0)|body_armor(27)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["tribal_warrior_outfit", "Tribal Warrior Outfit", [("tribal_warrior_outfit_a_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0,
 520 , weight(14)|abundance(100)|head_armor(0)|body_armor(30)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
["nomad_robe", "Nomad Robe", [("nomad_robe_a",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs |itp_civilian,0,
 610 , weight(15)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(10)|difficulty(0) ,imodbits_cloth ],
#["heraldric_armor", "Heraldric Armor", [("tourn_armor_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 442 , weight(17)|abundance(100)|head_armor(0)|body_armor(35)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
#NEW: was "std_lthr_coat"
["studded_leather_coat", "Studded Leather Coat", [("leather_armor_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 690 , weight(14)|abundance(100)|head_armor(0)|body_armor(36)|leg_armor(10)|difficulty(7) ,imodbits_armor ],

["byrnie", "Byrnie", [("byrnie_a_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 795 , weight(17)|abundance(100)|head_armor(0)|body_armor(39)|leg_armor(6)|difficulty(7) ,imodbits_armor ],
#["blackwhite_surcoat", "Black and White Surcoat", [("surcoat_blackwhite",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 348 , weight(16)|abundance(100)|head_armor(0)|body_armor(33)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
#["green_surcoat", "Green Surcoat", [("surcoat_green",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 348 , weight(16)|abundance(100)|head_armor(0)|body_armor(33)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
#["blue_surcoat", "Blue Surcoat", [("surcoat_blue",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 350 , weight(16)|abundance(100)|head_armor(0)|body_armor(33)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
#["red_surcoat", "Red Surcoat", [("surcoat_red",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0, 350 , weight(16)|abundance(100)|head_armor(0)|body_armor(33)|leg_armor(8)|difficulty(7) ,imodbits_armor ],
#NEW: was "haubergeon_a"
["haubergeon", "Haubergeon", [("haubergeon_c",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 863 , weight(18)|abundance(100)|head_armor(0)|body_armor(41)|leg_armor(6)|difficulty(6) ,imodbits_armor ],

["lamellar_vest", "Lamellar Vest", [("lamellar_vest_a",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 970 , weight(18)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(8)|difficulty(7) ,imodbits_cloth ],

["lamellar_vest_khergit", "Khergit Lamellar Vest", [("lamellar_vest_b",0)], itp_merchandise| itp_type_body_armor |itp_civilian |itp_covers_legs ,0,
 970 , weight(18)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(8)|difficulty(7) ,imodbits_cloth ],

 #NEW: was mail_shirt
["mail_shirt", "Mail Shirt", [("mail_shirt_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 1040 , weight(19)|abundance(100)|head_armor(0)|body_armor(37)|leg_armor(12)|difficulty(7) ,imodbits_armor ],

["mail_hauberk", "Mail Hauberk", [("hauberk_a_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 1320 , weight(19)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(12)|difficulty(7) ,imodbits_armor ],

["mail_with_surcoat", "Mail with Surcoat", [("mail_long_surcoat_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 1544 , weight(22)|abundance(100)|head_armor(0)|body_armor(42)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["surcoat_over_mail", "Surcoat over Mail", [("surcoat_over_mail_new",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 1720 , weight(22)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
#["lamellar_cuirass", "Lamellar Cuirass", [("lamellar_armor",0)], itp_type_body_armor  |itp_covers_legs,0, 1020 , weight(25)|abundance(100)|head_armor(0)|body_armor(43)|leg_armor(15)|difficulty(9) ,imodbits_armor ],
#NEW: was "brigandine_a"
["brigandine_red", "Brigandine", [("brigandine_b",0)], itp_merchandise| itp_type_body_armor|itp_covers_legs,0,
 1830 , weight(19)|abundance(100)|head_armor(0)|body_armor(46)|leg_armor(12)|difficulty(0) ,imodbits_armor ],
["lamellar_armor", "Lamellar Armor", [("lamellar_armor_b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 2410 , weight(25)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(13)|difficulty(0) ,imodbits_armor ],
["scale_armor", "Scale Armor", [("lamellar_armor_e",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 2558 , weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(13)|difficulty(8) ,imodbits_armor ],
 #NEW: was "reinf_jerkin"
["banded_armor", "Banded Armor", [("banded_armor_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 2710 , weight(23)|abundance(100)|head_armor(0)|body_armor(49)|leg_armor(14)|difficulty(8) ,imodbits_armor ],
#NEW: was hard_lthr_a
["cuir_bouilli", "Cuir Bouilli", [("cuir_bouilli_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 3100 , weight(24)|abundance(100)|head_armor(0)|body_armor(50)|leg_armor(15)|difficulty(8) ,imodbits_armor ],
["coat_of_plates", "Coat of Plates", [("coat_of_plates_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 3828 , weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(16)|difficulty(8) ,imodbits_armor ],
["coat_of_plates_red", "Coat of Plates", [("coat_of_plates_red",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 3828 , weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(16)|difficulty(8) ,imodbits_armor ],
["plate_armor", "Plate Armor", [("full_plate_armor",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 6553 , weight(27)|abundance(100)|head_armor(0)|body_armor(55)|leg_armor(17)|difficulty(9) ,imodbits_plate, material_swap ],
["black_armor", "Black Armor", [("black_armor",0)], itp_type_body_armor  |itp_covers_legs ,0,
 9496 , weight(28)|abundance(100)|head_armor(0)|body_armor(57)|leg_armor(18)|difficulty(10) ,imodbits_plate ],

##armors_d
["pelt_coat", "Pelt Coat", [("thick_coat_a",0)],  itp_merchandise|itp_type_body_armor  |itp_covers_legs ,0,
 14, weight(2)|abundance(100)|head_armor(0)|body_armor(9)|leg_armor(1)|difficulty(0) ,imodbits_cloth ],
##armors_e
["khergit_elite_armor", "Khergit Elite Armor", [("lamellar_armor_d",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 3828 , weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(16)|difficulty(8) ,imodbits_armor ],
["vaegir_elite_armor", "Vaegir Elite Armor", [("lamellar_armor_c",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 3828 , weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(16)|difficulty(8) ,imodbits_armor ],
["sarranid_elite_armor", "Sarranid Elite Armor", [("tunic_armor_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian ,0,
 3828 , weight(25)|abundance(100)|head_armor(0)|body_armor(52)|leg_armor(16)|difficulty(8) ,imodbits_armor ],


 ["sarranid_dress_a", "Dress", [("woolen_dress",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 33 , weight(1)|abundance(100)|head_armor(0)|body_armor(9)|leg_armor(9)|difficulty(0) ,imodbits_cloth ],
 ["sarranid_dress_b", "Dress", [("woolen_dress",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 33 , weight(1)|abundance(100)|head_armor(0)|body_armor(9)|leg_armor(9)|difficulty(0) ,imodbits_cloth ],
 ["sarranid_cloth_robe", "Worn Robe", [("sar_robe",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 33 , weight(1)|abundance(100)|head_armor(0)|body_armor(9)|leg_armor(9)|difficulty(0) ,imodbits_cloth ],
 ["sarranid_cloth_robe_b", "Worn Robe", [("sar_robe_b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 33 , weight(1)|abundance(100)|head_armor(0)|body_armor(9)|leg_armor(9)|difficulty(0) ,imodbits_cloth ],
["skirmisher_armor", "Skirmisher Armor", [("skirmisher_armor",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 74 , weight(3)|abundance(100)|head_armor(0)|body_armor(15)|leg_armor(9)|difficulty(0) ,imodbits_cloth ],
["archers_vest", "Archer's Padded Vest", [("archers_vest",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 260 , weight(6)|abundance(100)|head_armor(0)|body_armor(23)|leg_armor(12)|difficulty(0) ,imodbits_cloth ],
["sarranid_leather_armor", "Sarranid Leather Armor", [("sarranid_leather_armor",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 650 , weight(9)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(12)|difficulty(0) ,imodbits_armor ],
["sarranid_cavalry_robe", "Cavalry Robe", [("arabian_armor_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs |itp_civilian,0,
 990 , weight(15)|abundance(100)|head_armor(0)|body_armor(36)|leg_armor(8)|difficulty(0) ,imodbits_armor ],
["arabian_armor_b", "Sarranid Guard Armor", [("arabian_armor_b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 1200 , weight(19)|abundance(100)|head_armor(0)|body_armor(38)|leg_armor(8)|difficulty(0) ,imodbits_armor],
 ["sarranid_mail_shirt", "Sarranid Mail Shirt", [("sarranian_mail_shirt",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0,
 1400 , weight(19)|abundance(100)|head_armor(0)|body_armor(40)|leg_armor(14)|difficulty(7) ,imodbits_armor ],
["mamluke_mail", "Mamluke Mail", [("sarranid_elite_cavalary",0)], itp_merchandise| itp_type_body_armor |itp_covers_legs|itp_civilian  ,0, 
2900 , weight(24)|abundance(100)|head_armor(0)|body_armor(48)|leg_armor(16)|difficulty(8) ,imodbits_armor ],

#Quest-specific - perhaps can be used for prisoners, 
["burlap_tunic", "Burlap Tunic", [("shirt",0)], itp_type_body_armor  |itp_covers_legs ,0,
 5 , weight(1)|abundance(100)|head_armor(0)|body_armor(3)|leg_armor(1)|difficulty(0) ,imodbits_armor ],


["heraldic_mail_with_surcoat", "Heraldic Mail with Surcoat", [("heraldic_armor_new_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 3454 , weight(22)|abundance(100)|head_armor(0)|body_armor(49)|leg_armor(17)|difficulty(7) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_armor_a", ":agent_no", ":troop_no")])]],
["heraldic_mail_with_tunic", "Heraldic Mail", [("heraldic_armor_new_b",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 3520 , weight(22)|abundance(100)|head_armor(0)|body_armor(50)|leg_armor(16)|difficulty(7) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_armor_b", ":agent_no", ":troop_no")])]],
["heraldic_mail_with_tunic_b", "Heraldic Mail", [("heraldic_armor_new_c",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 3610 , weight(22)|abundance(100)|head_armor(0)|body_armor(50)|leg_armor(16)|difficulty(7) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_armor_c", ":agent_no", ":troop_no")])]],
["heraldic_mail_with_tabard", "Heraldic Mail with Tabard", [("heraldic_armor_new_d",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs ,0,
 3654 , weight(21)|abundance(100)|head_armor(0)|body_armor(51)|leg_armor(15)|difficulty(7) ,imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_armor_d", ":agent_no", ":troop_no")])]],
["turret_hat_ruby", "Turret Hat", [("turret_hat_r",0)], itp_type_head_armor  |itp_civilian|itp_fit_to_head ,0, 70 , weight(0.5)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ], 
["turret_hat_blue", "Turret Hat", [("turret_hat_b",0)], itp_type_head_armor  |itp_civilian|itp_fit_to_head ,0, 80 , weight(0.5)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ], 
["turret_hat_green", "Barbette", [("barbette_new",0)],itp_merchandise|itp_type_head_armor|itp_civilian|itp_fit_to_head,0,70, weight(0.5)|abundance(100)|head_armor(6)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["head_wrappings","Head Wrapping",[("head_wrapping",0)],itp_type_head_armor|itp_fit_to_head,0,16, weight(0.25)|head_armor(3),imodbit_tattered | imodbit_ragged | imodbit_sturdy | imodbit_thick],
["court_hat", "Turret Hat", [("court_hat",0)], itp_type_head_armor  |itp_civilian|itp_fit_to_head ,0, 80 , weight(0.5)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ], 
["wimple_a", "Wimple", [("wimple_a_new",0)],itp_merchandise|itp_type_head_armor|itp_civilian|itp_fit_to_head,0,10, weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["wimple_with_veil", "Wimple with Veil", [("wimple_b_new",0)],itp_merchandise|itp_type_head_armor|itp_civilian|itp_fit_to_head,0,10, weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["straw_hat", "Straw Hat", [("straw_hat_new",0)],itp_merchandise|itp_type_head_armor|itp_civilian,0,9, weight(1)|abundance(100)|head_armor(2)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["common_hood", "Hood", [("hood_new",0)],itp_merchandise|itp_type_head_armor|itp_civilian,0,9, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["hood_b", "Hood", [("hood_b",0)],itp_merchandise|itp_type_head_armor|itp_civilian,0,9, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["hood_c", "Hood", [("hood_c",0)],itp_merchandise|itp_type_head_armor|itp_civilian,0,9, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["hood_d", "Hood", [("hood_d",0)],itp_merchandise|itp_type_head_armor|itp_civilian,0,9, weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],
["headcloth", "Headcloth", [("headcloth_a_new",0)], itp_merchandise| itp_type_head_armor  |itp_civilian ,0, 1 , weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["woolen_hood", "Woolen Hood", [("woolen_hood",0)], itp_merchandise| itp_type_head_armor |itp_civilian  ,0, 4 , weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["arming_cap", "Arming Cap", [("arming_cap_a_new",0)], itp_merchandise| itp_type_head_armor  |itp_civilian ,0, 5 , weight(1)|abundance(100)|head_armor(7)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["fur_hat", "Fur Hat", [("fur_hat_a_new",0)], itp_merchandise| itp_type_head_armor |itp_civilian  ,0, 4 , weight(0.5)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["nomad_cap", "Nomad Cap", [("nomad_cap_a_new",0)], itp_merchandise| itp_type_head_armor |itp_civilian  ,0, 6 , weight(0.75)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["nomad_cap_b", "Nomad Cap", [("nomad_cap_b_new",0)], itp_merchandise| itp_type_head_armor |itp_civilian  ,0, 6 , weight(0.75)|abundance(100)|head_armor(13)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["steppe_cap", "Steppe Cap", [("steppe_cap_a_new",0)], itp_merchandise| itp_type_head_armor  |itp_civilian ,0, 14 , weight(1)|abundance(100)|head_armor(14)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["padded_coif", "Padded Coif", [("padded_coif_a_new",0)], itp_merchandise| itp_type_head_armor   ,0, 6 , weight(1)|abundance(100)|head_armor(11)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["woolen_cap", "Woolen Cap", [("woolen_cap_new",0)], itp_merchandise| itp_type_head_armor  |itp_civilian ,0, 2 , weight(1)|abundance(100)|head_armor(6)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["felt_hat", "Felt Hat", [("felt_hat_a_new",0)], itp_merchandise| itp_type_head_armor |itp_civilian,0, 4 , weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["felt_hat_b", "Felt Hat", [("felt_hat_b_new",0)], itp_merchandise| itp_type_head_armor |itp_civilian,0, 4 , weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["leather_cap", "Leather Cap", [("leather_cap_a_new",0)], itp_merchandise| itp_type_head_armor|itp_civilian ,0, 8, weight(1)|abundance(100)|head_armor(18)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["female_hood", "Lady's Hood", [("ladys_hood_new",0)], itp_merchandise| itp_type_head_armor |itp_civilian  ,0, 9 , weight(1)|abundance(100)|head_armor(10)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["leather_steppe_cap_a", "Steppe Cap", [("leather_steppe_cap_a_new",0)], itp_merchandise|itp_type_head_armor   ,0, 
24 , weight(1)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["leather_steppe_cap_b", "Steppe Cap ", [("tattered_steppe_cap_b_new",0)], itp_merchandise|itp_type_head_armor   ,0, 
36 , weight(1)|abundance(100)|head_armor(16)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["leather_steppe_cap_c", "Steppe Cap", [("steppe_cap_a_new",0)], itp_merchandise|itp_type_head_armor   ,0, 51 , weight(1)|abundance(100)|head_armor(16)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["leather_warrior_cap", "Leather Warrior Cap", [("skull_cap_new_b",0)], itp_merchandise| itp_type_head_armor  |itp_civilian ,0, 14 , weight(1)|abundance(100)|head_armor(18)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["skullcap", "Skullcap", [("skull_cap_new_a",0)], itp_merchandise| itp_type_head_armor   ,0, 60 , weight(1.0)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate ],
["mail_coif", "Mail Coif", [("mail_coif_new",0)], itp_merchandise| itp_type_head_armor   ,0, 71 , weight(1.25)|abundance(100)|head_armor(22)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_armor ],
["footman_helmet", "Footman's Helmet", [("skull_cap_new",0)], itp_merchandise| itp_type_head_armor   ,0, 95 , weight(1.5)|abundance(100)|head_armor(24)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate ],
#missing...
["nasal_helmet", "Nasal Helmet", [("nasal_helmet_b",0)], itp_merchandise| itp_type_head_armor   ,0, 121 , weight(1.25)|abundance(100)|head_armor(26)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["norman_helmet", "Helmet with Cap", [("norman_helmet_a",0)], itp_merchandise| itp_type_head_armor|itp_fit_to_head ,0, 147 , weight(1.25)|abundance(100)|head_armor(28)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["segmented_helmet", "Segmented Helmet", [("segmented_helm_new",0)], itp_merchandise| itp_type_head_armor   ,0, 174 , weight(1.25)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["helmet_with_neckguard", "Helmet with Neckguard", [("neckguard_helm_new",0)], itp_merchandise| itp_type_head_armor   ,0, 
190 , weight(1.5)|abundance(100)|head_armor(32)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["flat_topped_helmet", "Flat Topped Helmet", [("flattop_helmet_new",0)], itp_merchandise| itp_type_head_armor   ,0, 
203 , weight(1.75)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["kettle_hat", "Kettle Hat", [("kettle_hat_new",0)], itp_merchandise| itp_type_head_armor,0, 
240 , weight(1.75)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["spiked_helmet", "Spiked Helmet", [("spiked_helmet_new",0)], itp_merchandise| itp_type_head_armor   ,0, 278 , weight(2)|abundance(100)|head_armor(38)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["nordic_helmet", "Nordic Helmet", [("helmet_w_eyeguard_new",0)], itp_merchandise| itp_type_head_armor   ,0, 340 , weight(2)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["khergit_lady_hat", "Khergit Lady Hat", [("khergit_lady_hat",0)],  itp_type_head_armor   |itp_civilian |itp_doesnt_cover_hair | itp_fit_to_head,0, 1 , weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["khergit_lady_hat_b", "Khergit Lady Leather Hat", [("khergit_lady_hat_b",0)], itp_type_head_armor  | itp_doesnt_cover_hair | itp_fit_to_head  |itp_civilian ,0, 1 , weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["sarranid_felt_hat", "Sarranid Felt Hat", [("sar_helmet3",0)], itp_merchandise| itp_type_head_armor   ,0, 16 , weight(2)|abundance(100)|head_armor(5)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_cloth ],
["turban", "Turban", [("tuareg_open",0)], itp_merchandise| itp_type_head_armor   ,0, 28 , weight(1)|abundance(100)|head_armor(11)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_cloth ],
["desert_turban", "Desert Turban", [("tuareg",0)], itp_merchandise| itp_type_head_armor | itp_covers_beard ,0, 38 , weight(1.50)|abundance(100)|head_armor(14)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_cloth ],
["sarranid_warrior_cap", "Sarranid Warrior Cap", [("tuareg_helmet",0)], itp_merchandise| itp_type_head_armor | itp_covers_beard  ,0, 90 , weight(2)|abundance(100)|head_armor(19)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["sarranid_horseman_helmet", "Horseman Helmet", [("sar_helmet2",0)], itp_merchandise| itp_type_head_armor   ,0, 180 , weight(2.75)|abundance(100)|head_armor(25)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["sarranid_helmet1", "Sarranid Keffiyeh Helmet", [("sar_helmet1",0)], itp_merchandise| itp_type_head_armor   ,0, 290 , weight(2.50)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["sarranid_mail_coif", "Sarranid Mail Coif", [("tuareg_helmet2",0)], itp_merchandise| itp_type_head_armor ,0, 430 , weight(3)|abundance(100)|head_armor(41)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["sarranid_veiled_helmet", "Sarranid Veiled Helmet", [("sar_helmet4",0)], itp_merchandise| itp_type_head_armor | itp_covers_beard  ,0, 810 , weight(3.50)|abundance(100)|head_armor(47)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["nordic_archer_helmet", "Nordic Leather Helmet", [("Helmet_A_vs2",0)], itp_merchandise| itp_type_head_armor    ,0, 40 , weight(1.25)|abundance(100)|head_armor(14)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["nordic_veteran_archer_helmet", "Nordic Leather Helmet", [("Helmet_A",0)], itp_merchandise| itp_type_head_armor,0, 70 , weight(1.5)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["nordic_footman_helmet", "Nordic Footman Helmet", [("Helmet_B_vs2",0)], itp_merchandise| itp_type_head_armor |itp_fit_to_head ,0, 150 , weight(1.75)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["nordic_fighter_helmet", "Nordic Fighter Helmet", [("Helmet_B",0)], itp_merchandise| itp_type_head_armor|itp_fit_to_head ,0, 240 , weight(2)|abundance(100)|head_armor(34)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["nordic_huscarl_helmet", "Nordic Huscarl's Helmet", [("Helmet_C_vs2",0)], itp_merchandise| itp_type_head_armor   ,0, 390 , weight(2)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["nordic_warlord_helmet", "Nordic Warlord Helmet", [("Helmet_C",0)], itp_merchandise| itp_type_head_armor ,0, 880 , weight(2.25)|abundance(100)|head_armor(48)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],

["vaegir_fur_cap", "Cap with Fur", [("vaeg_helmet3",0)], itp_merchandise| itp_type_head_armor   ,0, 50 , weight(1)|abundance(100)|head_armor(15)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["vaegir_fur_helmet", "Vaegir Helmet", [("vaeg_helmet2",0)], itp_merchandise| itp_type_head_armor   ,0, 110 , weight(2)|abundance(100)|head_armor(21)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["vaegir_spiked_helmet", "Spiked Cap", [("vaeg_helmet1",0)], itp_merchandise| itp_type_head_armor   ,0, 230 , weight(2.50)|abundance(100)|head_armor(32)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["vaegir_lamellar_helmet", "Helmet with Lamellar Guard", [("vaeg_helmet4",0)], itp_merchandise| itp_type_head_armor   ,0, 360 , weight(2.75)|abundance(100)|head_armor(38)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["vaegir_noble_helmet", "Vaegir Nobleman Helmet", [("vaeg_helmet7",0)], itp_merchandise| itp_type_head_armor   ,0, 710, weight(2.75)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["vaegir_war_helmet", "Vaegir War Helmet", [("vaeg_helmet6",0)], itp_merchandise| itp_type_head_armor   ,0, 820 , weight(3)|abundance(100)|head_armor(47)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["vaegir_mask", "Vaegir War Mask", [("vaeg_helmet9",0)], itp_merchandise| itp_type_head_armor |itp_covers_beard ,0, 950 , weight(3.50)|abundance(100)|head_armor(52)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
 
["bascinet", "Bascinet", [("bascinet_avt_new",0)], itp_merchandise|itp_type_head_armor   ,0, 479 , weight(2.25)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bascinet_2", "Bascinet with Aventail", [("bascinet_new_a",0)], itp_merchandise|itp_type_head_armor   ,0, 479 , weight(2.25)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["bascinet_3", "Bascinet with Nose Guard", [("bascinet_new_b",0)], itp_merchandise|itp_type_head_armor   ,0, 479 , weight(2.25)|abundance(100)|head_armor(45)|body_armor(0)|leg_armor(0)|difficulty(8) ,imodbits_plate ],
["guard_helmet", "Guard Helmet", [("reinf_helmet_new",0)], itp_merchandise| itp_type_head_armor   ,0, 555 , weight(2.5)|abundance(100)|head_armor(47)|body_armor(0)|leg_armor(0)|difficulty(9) ,imodbits_plate ],
["black_helmet", "Black Helmet", [("black_helm",0)], itp_type_head_armor,0, 638 , weight(2.75)|abundance(100)|head_armor(50)|body_armor(0)|leg_armor(0)|difficulty(9) ,imodbits_plate ],
["full_helm", "Full Helm", [("great_helmet_new_b",0)], itp_merchandise| itp_type_head_armor |itp_covers_head ,0, 811 , weight(2.5)|abundance(100)|head_armor(51)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["great_helmet", "Great Helmet", [("great_helmet_new",0)], itp_merchandise| itp_type_head_armor|itp_covers_head,0, 980 , weight(2.75)|abundance(100)|head_armor(53)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],
["winged_great_helmet", "Winged Great Helmet", [("maciejowski_helmet_new",0)], itp_merchandise|itp_type_head_armor|itp_covers_head,0, 1240 , weight(2.75)|abundance(100)|head_armor(55)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],


#WEAPONS
["wooden_stick",         "Wooden Stick", [("wooden_stick",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_wooden_parry|itp_wooden_attack, itc_scimitar, 
4 , weight(2.5)|difficulty(0)|spd_rtng(99) | weapon_length(63)|swing_damage(13 , blunt) | thrust_damage(0 ,  pierce),imodbits_none ],
["cudgel",         "Cudgel", [("club",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_wooden_parry|itp_wooden_attack, itc_scimitar, 
4 , weight(2.5)|difficulty(0)|spd_rtng(99) | weapon_length(70)|swing_damage(13 , blunt) | thrust_damage(0 ,  pierce),imodbits_none ],
["hammer",         "Hammer", [("iron_hammer_new",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar, 
7 , weight(2)|difficulty(0)|spd_rtng(100) | weapon_length(55)|swing_damage(24 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["club",         "Club", [("club",0)], itp_type_one_handed_wpn|itp_merchandise| itp_can_knock_down|itp_primary|itp_wooden_parry|itp_wooden_attack, itc_scimitar, 
11 , weight(2.5)|difficulty(0)|spd_rtng(98) | weapon_length(70)|swing_damage(20 , blunt) | thrust_damage(0 ,  pierce),imodbits_none ],
["winged_mace",         "Flanged Mace", [("flanged_mace",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 
122 , weight(3.5)|difficulty(0)|spd_rtng(103) | weapon_length(70)|swing_damage(24 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["spiked_mace",         "Spiked Mace", [("spiked_mace_new",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 
180 , weight(3.5)|difficulty(0)|spd_rtng(98) | weapon_length(70)|swing_damage(28 , blunt) | thrust_damage(0 ,  pierce),imodbits_pick ],
["military_hammer", "Military Hammer", [("military_hammer",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 
317 , weight(2)|difficulty(0)|spd_rtng(95) | weapon_length(70)|swing_damage(31 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["maul",         "Maul", [("maul_b",0)], itp_crush_through|itp_type_two_handed_wpn|itp_merchandise|itp_can_knock_down |itp_primary|itp_two_handed|itp_wooden_parry|itp_wooden_attack|itp_unbalanced, itc_nodachi|itcf_carry_spear, 
97 , weight(6)|difficulty(11)|spd_rtng(83) | weapon_length(79)|swing_damage(36 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["sledgehammer", "Sledgehammer", [("maul_c",0)], itp_crush_through|itp_type_two_handed_wpn|itp_merchandise|itp_can_knock_down|itp_primary|itp_two_handed|itp_wooden_parry|itp_wooden_attack|itp_unbalanced, itc_nodachi|itcf_carry_spear, 
101 , weight(7)|difficulty(12)|spd_rtng(81) | weapon_length(82)|swing_damage(39, blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["warhammer",         "Great Hammer", [("maul_d",0)], itp_crush_through|itp_type_two_handed_wpn|itp_merchandise|itp_can_knock_down|itp_primary|itp_two_handed|itp_wooden_parry|itp_wooden_attack|itp_unbalanced, itc_nodachi|itcf_carry_spear, 
290 , weight(9)|difficulty(14)|spd_rtng(79) | weapon_length(75)|swing_damage(45 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["pickaxe",         "Pickaxe", [("fighting_pick_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 
27 , weight(3)|difficulty(0)|spd_rtng(99) | weapon_length(70)|swing_damage(19 , pierce) | thrust_damage(0 ,  pierce),imodbits_pick ],
["spiked_club",         "Spiked Club", [("spiked_club",0)], itp_type_one_handed_wpn|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 
83 , weight(3)|difficulty(0)|spd_rtng(97) | weapon_length(70)|swing_damage(21 , pierce) | thrust_damage(0 ,  pierce),imodbits_mace ],
["fighting_pick", "Fighting Pick", [("fighting_pick_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 
108 , weight(1.0)|difficulty(0)|spd_rtng(98) | weapon_length(70)|swing_damage(22 , pierce) | thrust_damage(0 ,  pierce),imodbits_pick ],
["military_pick", "Military Pick", [("steel_pick_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 
280 , weight(1.5)|difficulty(0)|spd_rtng(97) | weapon_length(70)|swing_damage(31 , pierce) | thrust_damage(0 ,  pierce),imodbits_pick ],
["morningstar",         "Morningstar", [("mace_morningstar_new",0)], itp_crush_through|itp_type_two_handed_wpn|itp_merchandise|itp_primary|itp_wooden_parry|itp_unbalanced, itc_morningstar|itcf_carry_mace_left_hip, 
305 , weight(4.5)|difficulty(13)|spd_rtng(95) | weapon_length(85)|swing_damage(38 , pierce) | thrust_damage(0 ,  pierce),imodbits_mace ],


["sickle",         "Sickle", [("sickle",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry|itp_wooden_parry, itc_cleaver, 
9 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(40)|swing_damage(20 , cut) | thrust_damage(0 ,  pierce),imodbits_none ],
["cleaver",         "Cleaver", [("cleaver_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry|itp_wooden_parry, itc_cleaver, 
14 , weight(1.5)|difficulty(0)|spd_rtng(103) | weapon_length(35)|swing_damage(24 , cut) | thrust_damage(0 ,  pierce),imodbits_none ],
["knife",         "Knife", [("peasant_knife_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_left, 
18 , weight(0.5)|difficulty(0)|spd_rtng(110) | weapon_length(40)|swing_damage(21 , cut) | thrust_damage(13 ,  pierce),imodbits_sword ],
["butchering_knife", "Butchering Knife", [("khyber_knife_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_right, 
23 , weight(0.75)|difficulty(0)|spd_rtng(108) | weapon_length(60)|swing_damage(24 , cut) | thrust_damage(17 ,  pierce),imodbits_sword ],
["dagger",         "Dagger", [("dagger_b",0),("dagger_b_scabbard",ixmesh_carry),("dagger_b",imodbits_good),("dagger_b_scabbard",ixmesh_carry|imodbits_good)], itp_type_one_handed_wpn|itp_merchandise|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_left|itcf_show_holster_when_drawn, 
37 , weight(0.75)|difficulty(0)|spd_rtng(109) | weapon_length(47)|swing_damage(22 , cut) | thrust_damage(19 ,  pierce),imodbits_sword_high ],
#["nordic_sword", "Nordic Sword", [("viking_sword",0),("scab_vikingsw", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 142 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(98)|swing_damage(27 , cut) | thrust_damage(19 ,  pierce),imodbits_sword ],
#["arming_sword", "Arming Sword", [("b_long_sword",0),("scab_longsw_b", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 156 , weight(1.5)|difficulty(0)|spd_rtng(101) | weapon_length(100)|swing_damage(25 , cut) | thrust_damage(22 ,  pierce),imodbits_sword ],
#["sword",         "Sword", [("long_sword",0),("scab_longsw_a", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 148 , weight(1.5)|difficulty(0)|spd_rtng(100) | weapon_length(102)|swing_damage(26 , cut) | thrust_damage(23 ,  pierce),imodbits_sword ],
["falchion",         "Falchion", [("falchion_new",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip, 
105 , weight(2.5)|difficulty(8)|spd_rtng(98) | weapon_length(73)|swing_damage(30 , cut) | thrust_damage(0 ,  pierce),imodbits_sword ],
#["broadsword",         "Broadsword", [("broadsword",0),("scab_broadsword", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 122 , weight(2.5)|difficulty(8)|spd_rtng(91) | weapon_length(101)|swing_damage(27 , cut) | thrust_damage(0 ,  pierce),imodbits_sword ],
#["scimitar",         "Scimitar", [("scimeter",0),("scab_scimeter", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 
#108 , weight(1.5)|difficulty(0)|spd_rtng(100) | weapon_length(97)|swing_damage(29 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],

["scimitar",         "Scimitar", [("scimitar_a",0),("scab_scimeter_a", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 
210 , weight(1.5)|difficulty(0)|spd_rtng(101) | weapon_length(97)|swing_damage(30 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],
["scimitar_b",         "Elite Scimitar", [("scimitar_b",0),("scab_scimeter_b", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 
290 , weight(1.5)|difficulty(0)|spd_rtng(100) | weapon_length(103)|swing_damage(32 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],

["arabian_sword_a",         "Sarranid Sword", [("arabian_sword_a",0),("scab_arabian_sword_a", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 
108 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(97)|swing_damage(26 , cut) | thrust_damage(19 ,  pierce),imodbits_sword_high ],
["arabian_sword_b",         "Sarranid Arming Sword", [("arabian_sword_b",0),("scab_arabian_sword_b", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 
218 , weight(1.7)|difficulty(0)|spd_rtng(99) | weapon_length(97)|swing_damage(28 , cut) | thrust_damage(19 ,  pierce),imodbits_sword_high ],
["sarranid_cavalry_sword",         "Sarranid Cavalry Sword", [("arabian_sword_c",0),("scab_arabian_sword_c", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 
310 , weight(1.5)|difficulty(0)|spd_rtng(98) | weapon_length(105)|swing_damage(28 , cut) | thrust_damage(19 ,  pierce),imodbits_sword_high ],
["arabian_sword_d",         "Sarranid Guard Sword", [("arabian_sword_d",0),("scab_arabian_sword_d", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 
420 , weight(1.7)|difficulty(0)|spd_rtng(99) | weapon_length(97)|swing_damage(30 , cut) | thrust_damage(20 ,  pierce),imodbits_sword_high ],


#["nomad_sabre",         "Nomad Sabre", [("shashqa",0),("scab_shashqa", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 115 , weight(1.75)|difficulty(0)|spd_rtng(101) | weapon_length(100)|swing_damage(27 , cut) | thrust_damage(0 ,  pierce),imodbits_sword ],
#["bastard_sword", "Bastard Sword", [("bastard_sword",0),("scab_bastardsw", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise| itp_primary, itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 279 , weight(2.25)|difficulty(9)|spd_rtng(102) | weapon_length(120)|swing_damage(33 , cut) | thrust_damage(27 ,  pierce),imodbits_sword ],
["great_sword",         "Great Sword", [("b_bastard_sword",0),("scab_bastardsw_b", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn,
 423 , weight(2.75)|difficulty(10)|spd_rtng(95) | weapon_length(125)|swing_damage(39 , cut) | thrust_damage(31 ,  pierce),imodbits_sword_high ],
["sword_of_war", "Sword of War", [("b_bastard_sword",0),("scab_bastardsw_b", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn,
 524 , weight(3)|difficulty(11)|spd_rtng(94) | weapon_length(130)|swing_damage(40 , cut) | thrust_damage(31 ,  pierce),imodbits_sword_high ],
["hatchet",         "Hatchet", [("hatchet",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 
13 , weight(2)|difficulty(0)|spd_rtng(97) | weapon_length(60)|swing_damage(23 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["hand_axe",         "Hand Axe", [("hatchet",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 
24 , weight(2)|difficulty(7)|spd_rtng(95) | weapon_length(75)|swing_damage(27 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["fighting_axe", "Fighting Axe", [("fighting_ax",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip, 
77 , weight(2.5)|difficulty(9)|spd_rtng(92) | weapon_length(90)|swing_damage(31 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["axe", "Axe", [("iron_ax",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 
65 , weight(4)|difficulty(8)|spd_rtng(91) | weapon_length(108)|swing_damage(32 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["battle_axe", "Battle Axe", [("battle_ax",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 
240 , weight(5)|difficulty(9)|spd_rtng(88) | weapon_length(108)|swing_damage(41 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["war_axe", "War Axe", [("war_ax",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 
264 , weight(5)|difficulty(10)|spd_rtng(86) | weapon_length(110)|swing_damage(43 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
#["double_axe",         "Double Axe", [("dblhead_ax",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 359 , weight(6.5)|difficulty(12)|spd_rtng(85) | weapon_length(95)|swing_damage(43 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
#["great_axe",         "Great Axe", [("great_ax",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_nodachi|itcf_carry_axe_back, 415 , weight(7)|difficulty(13)|spd_rtng(82) | weapon_length(120)|swing_damage(45 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],

["sword_two_handed_b", "Two Handed Sword", [("sword_two_handed_b",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back,
 670 , weight(2.75)|difficulty(10)|spd_rtng(97) | weapon_length(110)|swing_damage(40 , cut) | thrust_damage(28 ,  pierce),imodbits_sword_high ],
["sword_two_handed_a", "Great Sword", [("sword_two_handed_a",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back,
 1123 , weight(2.75)|difficulty(10)|spd_rtng(96) | weapon_length(120)|swing_damage(42 , cut) | thrust_damage(29 ,  pierce),imodbits_sword_high ],


["khergit_sword_two_handed_a", "Two Handed Sabre", [("khergit_sword_two_handed_a",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_nodachi|itcf_carry_sword_back,
 523 , weight(2.75)|difficulty(10)|spd_rtng(96) | weapon_length(120)|swing_damage(40 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],
["khergit_sword_two_handed_b", "Two Handed Sabre", [("khergit_sword_two_handed_b",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_nodachi|itcf_carry_sword_back,
 920 , weight(2.75)|difficulty(10)|spd_rtng(96) | weapon_length(120)|swing_damage(44 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],

["two_handed_cleaver", "War Cleaver", [("military_cleaver_a",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary, itc_nodachi|itcf_carry_sword_back,
 640 , weight(2.75)|difficulty(10)|spd_rtng(93) | weapon_length(120)|swing_damage(45 , cut) | thrust_damage(0 ,  cut),imodbits_sword_high ],
["military_cleaver_b", "Soldier's Cleaver", [("military_cleaver_b",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip,
 193 , weight(1.5)|difficulty(0)|spd_rtng(96) | weapon_length(95)|swing_damage(31 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],
["military_cleaver_c", "Military Cleaver", [("military_cleaver_c",0)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip,
 263 , weight(1.5)|difficulty(0)|spd_rtng(96) | weapon_length(95)|swing_damage(35 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],

["military_sickle_a", "Military Sickle", [("military_sickle_a",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,
 220 , weight(1.0)|difficulty(9)|spd_rtng(100) | weapon_length(75)|swing_damage(26 , pierce) | thrust_damage(0 ,  pierce),imodbits_axe ],


["bastard_sword_a", "Bastard Sword", [("bastard_sword_a",0),("bastard_sword_a_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise| itp_primary, itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 294 , weight(2.0)|difficulty(9)|spd_rtng(98) | weapon_length(101)|swing_damage(35 , cut) | thrust_damage(26 ,  pierce),imodbits_sword_high ],
["bastard_sword_b", "Heavy Bastard Sword", [("bastard_sword_b",0),("bastard_sword_b_scabbard", ixmesh_carry)], itp_type_two_handed_wpn|itp_merchandise| itp_primary, itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 526 , weight(2.25)|difficulty(9)|spd_rtng(97) | weapon_length(105)|swing_damage(37 , cut) | thrust_damage(27 ,  pierce),imodbits_sword_high ],

["one_handed_war_axe_a", "One Handed Axe", [("one_handed_war_axe_a",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,
 87 , weight(1.5)|difficulty(9)|spd_rtng(98) | weapon_length(71)|swing_damage(32 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["one_handed_battle_axe_a", "One Handed Battle Axe", [("one_handed_battle_axe_a",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,
 142 , weight(1.5)|difficulty(9)|spd_rtng(98) | weapon_length(73)|swing_damage(34 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["one_handed_war_axe_b", "One Handed War Axe", [("one_handed_war_axe_b",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,
 190 , weight(1.5)|difficulty(9)|spd_rtng(98) | weapon_length(76)|swing_damage(34 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["one_handed_battle_axe_b", "One Handed Battle Axe", [("one_handed_battle_axe_b",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,
 230 , weight(1.75)|difficulty(9)|spd_rtng(98) | weapon_length(72)|swing_damage(36 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["one_handed_battle_axe_c", "One Handed Battle Axe", [("one_handed_battle_axe_c",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,
 550 , weight(2.0)|difficulty(9)|spd_rtng(98) | weapon_length(76)|swing_damage(37 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],


["two_handed_axe",         "Two Handed Axe", [("two_handed_battle_axe_a",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 90 , weight(4.5)|difficulty(10)|spd_rtng(96) | weapon_length(90)|swing_damage(38 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["two_handed_battle_axe_2",         "Two Handed War Axe", [("two_handed_battle_axe_b",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 152 , weight(4.5)|difficulty(10)|spd_rtng(96) | weapon_length(92)|swing_damage(44 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["shortened_voulge",         "Shortened Voulge", [("two_handed_battle_axe_c",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 228 , weight(4.5)|difficulty(10)|spd_rtng(92) | weapon_length(100)|swing_damage(45 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["great_axe",         "Great Axe", [("two_handed_battle_axe_e",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 316 , weight(4.5)|difficulty(10)|spd_rtng(94) | weapon_length(96)|swing_damage(47 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["long_axe",         "Long Axe", [("long_axe_a",0)], itp_type_polearm|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_next_item_as_melee|itp_unbalanced|itp_merchandise,itc_staff|itcf_carry_axe_back,
 390 , weight(4.75)|difficulty(10)|spd_rtng(93) | weapon_length(120)|swing_damage(46 , cut) | thrust_damage(19 ,  blunt),imodbits_axe ],
["long_axe_alt",         "Long Axe", [("long_axe_a",0)],itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 390 , weight(4.75)|difficulty(10)|spd_rtng(88) | weapon_length(120)|swing_damage(46 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
 ["long_axe_b",         "Long War Axe", [("long_axe_b",0)], itp_type_polearm| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_next_item_as_melee|itp_unbalanced|itp_merchandise, itc_staff|itcf_carry_axe_back,
 510 , weight(5.0)|difficulty(10)|spd_rtng(92) | weapon_length(125)|swing_damage(50 , cut) | thrust_damage(18 ,  blunt),imodbits_axe ],
["long_axe_b_alt",         "Long War Axe", [("long_axe_b",0)], itp_type_two_handed_wpn| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 510 , weight(5.0)|difficulty(10)|spd_rtng(87) | weapon_length(125)|swing_damage(50 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["long_axe_c",         "Great Long Axe", [("long_axe_c",0)], itp_type_polearm| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_next_item_as_melee|itp_unbalanced|itp_merchandise, itc_staff|itcf_carry_axe_back,
 660 , weight(5.5)|difficulty(10)|spd_rtng(91) | weapon_length(127)|swing_damage(54 , cut) | thrust_damage(19 ,  blunt),imodbits_axe ],
["long_axe_c_alt",      "Great Long Axe", [("long_axe_c",0)], itp_type_two_handed_wpn| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 660 , weight(5.5)|difficulty(10)|spd_rtng(85) | weapon_length(127)|swing_damage(54 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],

 ["bardiche",         "Bardiche", [("two_handed_battle_axe_d",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 291 , weight(4.75)|difficulty(10)|spd_rtng(91) | weapon_length(102)|swing_damage(47 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["great_bardiche",         "Great Bardiche", [("two_handed_battle_axe_f",0)], itp_type_two_handed_wpn|itp_merchandise|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 617 , weight(5.0)|difficulty(10)|spd_rtng(89) | weapon_length(116)|swing_damage(50 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],




["voulge",         "Voulge", [("two_handed_battle_long_axe_a",0)], itp_type_polearm|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry, itc_staff,
 120 , weight(3.0)|difficulty(10)|spd_rtng(88) | weapon_length(175)|swing_damage(40 , cut) | thrust_damage(18 ,  pierce),imodbits_axe ],
["long_bardiche",         "Long Bardiche", [("two_handed_battle_long_axe_b",0)], itp_type_polearm|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_staff,
390 , weight(4.75)|difficulty(11)|spd_rtng(89) | weapon_length(140)|swing_damage(48 , cut) | thrust_damage(17 ,  pierce),imodbits_axe ],
["great_long_bardiche",         "Great Long Bardiche", [("two_handed_battle_long_axe_c",0)], itp_type_polearm|itp_merchandise| itp_two_handed|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced, itc_staff,
 660 , weight(5.0)|difficulty(12)|spd_rtng(88) | weapon_length(155)|swing_damage(50 , cut) | thrust_damage(17 ,  pierce),imodbits_axe ],

 ["hafted_blade_b",         "Hafted Blade", [("khergit_pike_b",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_two_handed|itp_penalty_with_shield|itp_wooden_parry, itcf_carry_spear|itc_guandao,
 185 , weight(2.75)|difficulty(0)|spd_rtng(95) | weapon_length(135)|swing_damage(37 , cut) | thrust_damage(20 ,  pierce),imodbits_polearm ],
 ["hafted_blade_a",         "Hafted Blade", [("khergit_pike_a",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_two_handed|itp_penalty_with_shield|itp_wooden_parry, itcf_carry_spear|itc_guandao,
 350 , weight(3.25)|difficulty(0)|spd_rtng(93) | weapon_length(153)|swing_damage(39 , cut) | thrust_damage(19 ,  pierce),imodbits_polearm ],

["shortened_military_scythe",         "Shortened Military Scythe", [("two_handed_battle_scythe_a",0)], itp_type_two_handed_wpn|itp_merchandise| itp_two_handed|itp_primary, itc_nodachi|itcf_carry_sword_back,
 264 , weight(3.0)|difficulty(10)|spd_rtng(90) | weapon_length(112)|swing_damage(45 , cut) | thrust_damage(0 ,  pierce),imodbits_sword_high ],

["sword_medieval_a", "Sword", [("sword_medieval_a",0),("sword_medieval_a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 163 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(27 , cut) | thrust_damage(22 ,  pierce),imodbits_sword_high ],
#["sword_medieval_a_long", "Sword", [("sword_medieval_a_long",0),("sword_medieval_a_long_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 156 , weight(1.5)|difficulty(0)|spd_rtng(97) | weapon_length(105)|swing_damage(25 , cut) | thrust_damage(22 ,  pierce),imodbits_sword ],
["sword_medieval_b", "Sword", [("sword_medieval_b",0),("sword_medieval_b_scabbard", ixmesh_carry),("sword_rusty_a",imodbit_rusty),("sword_rusty_a_scabbard", ixmesh_carry|imodbit_rusty)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 243 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(28 , cut) | thrust_damage(23 ,  pierce),imodbits_sword_high ],
["sword_medieval_b_small", "Short Sword", [("sword_medieval_b_small",0),("sword_medieval_b_small_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 152 , weight(1)|difficulty(0)|spd_rtng(102) | weapon_length(85)|swing_damage(26, cut) | thrust_damage(24, pierce),imodbits_sword_high ],
["sword_medieval_c", "Arming Sword", [("sword_medieval_c",0),("sword_medieval_c_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 410 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(29 , cut) | thrust_damage(24 ,  pierce),imodbits_sword_high ],
["sword_medieval_c_small", "Short Arming Sword", [("sword_medieval_c_small",0),("sword_medieval_c_small_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 243 , weight(1)|difficulty(0)|spd_rtng(103) | weapon_length(86)|swing_damage(26, cut) | thrust_damage(24 ,  pierce),imodbits_sword_high ],
["sword_medieval_c_long", "Arming Sword", [("sword_medieval_c_long",0),("sword_medieval_c_long_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 480 , weight(1.7)|difficulty(0)|spd_rtng(99) | weapon_length(100)|swing_damage(29 , cut) | thrust_damage(28 ,  pierce),imodbits_sword_high ],
["sword_medieval_d_long", "Long Arming Sword", [("sword_medieval_d_long",0),("sword_medieval_d_long_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 550 , weight(1.8)|difficulty(0)|spd_rtng(96) | weapon_length(105)|swing_damage(33 , cut) | thrust_damage(28 ,  pierce),imodbits_sword ],
 
#["sword_medieval_d", "sword_medieval_d", [("sword_medieval_d",0),("sword_medieval_d_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
# 131 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(24 , cut) | thrust_damage(21 ,  pierce),imodbits_sword ],
#["sword_medieval_e", "sword_medieval_e", [("sword_medieval_e",0),("sword_medieval_e_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
# 131 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(24 , cut) | thrust_damage(21 ,  pierce),imodbits_sword ],

["sword_viking_1", "Nordic Sword", [("sword_viking_c",0),("sword_viking_c_scabbard ", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 147 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(94)|swing_damage(28 , cut) | thrust_damage(20 ,  pierce),imodbits_sword_high ] ,
["sword_viking_2", "Nordic Sword", [("sword_viking_b",0),("sword_viking_b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 276 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(29 , cut) | thrust_damage(21 ,  pierce),imodbits_sword_high ],
["sword_viking_2_small", "Nordic Short Sword", [("sword_viking_b_small",0),("sword_viking_b_small_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 162 , weight(1.25)|difficulty(0)|spd_rtng(103) | weapon_length(85)|swing_damage(28 , cut) | thrust_damage(21 ,  pierce),imodbits_sword_high ],
["sword_viking_3", "Nordic War Sword", [("sword_viking_a",0),("sword_viking_a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 394 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(30 , cut) | thrust_damage(21 ,  pierce),imodbits_sword_high ],
#["sword_viking_a_long", "sword_viking_a_long", [("sword_viking_a_long",0),("sword_viking_a_long_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
# 142 , weight(1.5)|difficulty(0)|spd_rtng(97) | weapon_length(105)|swing_damage(27 , cut) | thrust_damage(19 ,  pierce),imodbits_sword ],
["sword_viking_3_small", "Nordic Short War Sword", [("sword_viking_a_small",0),("sword_viking_a_small_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 280 , weight(1.25)|difficulty(0)|spd_rtng(103) | weapon_length(86)|swing_damage(29 , cut) | thrust_damage(21 ,  pierce),imodbits_sword_high ],
#["sword_viking_c_long", "sword_viking_c_long", [("sword_viking_c_long",0),("sword_viking_c_long_scabbard ", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
# 142 , weight(1.5)|difficulty(0)|spd_rtng(95) | weapon_length(105)|swing_damage(27 , cut) | thrust_damage(19 ,  pierce),imodbits_sword ] ,

["sword_khergit_1", "Nomad Sabre", [("khergit_sword_b",0),("khergit_sword_b_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 105 , weight(1.25)|difficulty(0)|spd_rtng(100) | weapon_length(97)|swing_damage(29 , cut),imodbits_sword_high ],
["sword_khergit_2", "Sabre", [("khergit_sword_c",0),("khergit_sword_c_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 191 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(97)|swing_damage(30 , cut),imodbits_sword_high ],
["sword_khergit_3", "Sabre", [("khergit_sword_a",0),("khergit_sword_a_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 294 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(98)|swing_damage(31 , cut),imodbits_sword_high ],
["sword_khergit_4", "Heavy Sabre", [("khergit_sword_d",0),("khergit_sword_d_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_merchandise|itp_primary, itc_scimitar|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
 384 , weight(1.75)|difficulty(0)|spd_rtng(98) | weapon_length(96)|swing_damage(33 , cut),imodbits_sword_high ],



["mace_1",         "Spiked Club", [("mace_d",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip,
 45 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(70)|swing_damage(19 , pierce) | thrust_damage(0 ,  pierce),imodbits_mace ],
["mace_2",         "Knobbed_Mace", [("mace_a",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip,
 98 , weight(2.5)|difficulty(0)|spd_rtng(98) | weapon_length(70)|swing_damage(21 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["mace_3",         "Spiked Mace", [("mace_c",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip,
 152 , weight(2.75)|difficulty(0)|spd_rtng(98) | weapon_length(70)|swing_damage(23 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["mace_4",         "Winged_Mace", [("mace_b",0)], itp_type_one_handed_wpn|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip,
 212 , weight(2.75)|difficulty(0)|spd_rtng(98) | weapon_length(70)|swing_damage(24 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
# Goedendag
 ["club_with_spike_head",  "Spiked Staff", [("mace_e",0)],  itp_type_two_handed_wpn|itp_merchandise|itp_can_knock_down|itp_primary|itp_wooden_parry, itc_bastardsword|itcf_carry_axe_back,
 200 , weight(2.80)|difficulty(9)|spd_rtng(95) | weapon_length(117)|swing_damage(24 , blunt) | thrust_damage(20 ,  pierce),imodbits_mace ],

["long_spiked_club",         "Long Spiked Club", [("mace_long_c",0)], itp_type_polearm|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_staff|itcf_carry_axe_back,
 264 , weight(3)|difficulty(0)|spd_rtng(96) | weapon_length(126)|swing_damage(23 , pierce) | thrust_damage(20 ,  blunt),imodbits_mace ],
["long_hafted_knobbed_mace",         "Long Hafted Knobbed Mace", [("mace_long_a",0)], itp_type_polearm| itp_can_knock_down|itp_primary|itp_wooden_parry, itc_staff|itcf_carry_axe_back,
 324 , weight(3)|difficulty(0)|spd_rtng(95) | weapon_length(133)|swing_damage(26 , blunt) | thrust_damage(23 ,  blunt),imodbits_mace ],
["long_hafted_spiked_mace",         "Long Hafted Spiked Mace", [("mace_long_b",0)], itp_type_polearm|itp_can_knock_down|itp_merchandise| itp_primary|itp_wooden_parry, itc_staff|itcf_carry_axe_back,
 310 , weight(3)|difficulty(0)|spd_rtng(94) | weapon_length(140)|swing_damage(28 , blunt) | thrust_damage(26 ,  blunt),imodbits_mace ],

["sarranid_two_handed_mace_1",         "Iron Mace", [("mace_long_d",0)], itp_type_two_handed_wpn|itp_can_knock_down|itp_two_handed|itp_merchandise| itp_primary|itp_crush_through|itp_unbalanced, itc_greatsword|itcf_carry_axe_back,
470 , weight(4.5)|difficulty(0)|spd_rtng(90) | weapon_length(95)|swing_damage(35 , blunt) | thrust_damage(22 ,  blunt),imodbits_mace ],


["sarranid_mace_1",         "Iron Mace", [("mace_small_d",0)], itp_type_one_handed_wpn|itp_merchandise|itp_can_knock_down |itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip,
 45 , weight(2.0)|difficulty(0)|spd_rtng(99) | weapon_length(73)|swing_damage(22 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["sarranid_axe_a", "Iron Battle Axe", [("one_handed_battle_axe_g",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,
 250 , weight(1.65)|difficulty(9)|spd_rtng(97) | weapon_length(71)|swing_damage(35 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["sarranid_axe_b", "Iron War Axe", [("one_handed_battle_axe_h",0)], itp_type_one_handed_wpn|itp_merchandise| itp_primary|itp_secondary|itp_bonus_against_shield|itp_wooden_parry, itc_scimitar|itcf_carry_axe_left_hip,
 360 , weight(1.75)|difficulty(9)|spd_rtng(97) | weapon_length(71)|swing_damage(38 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],

["sarranid_two_handed_axe_a",         "Sarranid Battle Axe", [("two_handed_battle_axe_g",0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 350 , weight(3.0)|difficulty(10)|spd_rtng(89) | weapon_length(95)|swing_damage(49 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["sarranid_two_handed_axe_b",         "Sarranid War Axe", [("two_handed_battle_axe_h",0)], itp_type_two_handed_wpn|itp_two_handed|itp_primary|itp_bonus_against_shield|itp_unbalanced, itc_nodachi|itcf_carry_axe_back,
 280 , weight(2.50)|difficulty(10)|spd_rtng(90) | weapon_length(90)|swing_damage(46 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],




["scythe",         "Scythe", [("scythe",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_staff|itcf_carry_spear, 43 , weight(2)|difficulty(0)|spd_rtng(97) | weapon_length(182)|swing_damage(30 , cut) | thrust_damage(14 ,  pierce),imodbits_polearm ],
["pitch_fork",         "Pitch Fork", [("pitch_fork",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry,itc_staff, 19 , weight(1.5)|difficulty(0)|spd_rtng(87) | weapon_length(154)|swing_damage(16 , blunt) | thrust_damage(22 ,  pierce),imodbits_polearm ],
["military_fork", "Military Fork", [("military_fork",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_wooden_parry,itc_staff, 153 , weight(2)|difficulty(0)|spd_rtng(95) | weapon_length(135)|swing_damage(15 , blunt) | thrust_damage(30 ,  pierce),imodbits_polearm ],
["battle_fork",         "Battle Fork", [("battle_fork",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_wooden_parry,itc_staff, 282 , weight(2.2)|difficulty(0)|spd_rtng(90) | weapon_length(144)|swing_damage(15, blunt) | thrust_damage(35 ,  pierce),imodbits_polearm ],
["boar_spear",         "Boar Spear", [("spear",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry,itc_staff|itcf_carry_spear, 
76 , weight(1.5)|difficulty(0)|spd_rtng(90) | weapon_length(157)|swing_damage(26 , cut) | thrust_damage(23 ,  pierce),imodbits_polearm ],
#["spear",         "Spear", [("spear",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear|itcf_carry_spear, 173 , weight(4.5)|difficulty(0)|spd_rtng(80) | weapon_length(158)|swing_damage(17 , blunt) | thrust_damage(23 ,  pierce),imodbits_polearm ],


["jousting_lance", "Jousting Lance", [("joust_of_peace",0)], itp_couchable|itp_type_polearm|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_greatlance, 158 , weight(5)|difficulty(0)|spd_rtng(61) | weapon_length(240)|swing_damage(0 , cut) | thrust_damage(17 ,  blunt),imodbits_polearm ],
#["lance",         "Lance", [("pike",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_spear, 196 , weight(5)|difficulty(0)|spd_rtng(72) | weapon_length(170)|swing_damage(0 , cut) | thrust_damage(20 ,  pierce),imodbits_polearm ],
["double_sided_lance", "Double Sided Lance", [("lance_dblhead",0)], itp_couchable|itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_wooden_parry, itc_staff, 261 , weight(4.0)|difficulty(0)|spd_rtng(95) | weapon_length(128)|swing_damage(25, cut) | thrust_damage(27 ,  pierce),imodbits_polearm ],
#["pike",         "Pike", [("pike",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_two_handed|itp_wooden_parry, itc_spear,
# 212 , weight(6)|difficulty(0)|spd_rtng(77) | weapon_length(167)|swing_damage(0 , blunt) | thrust_damage(23 ,  pierce),imodbits_polearm ],
["glaive",         "Glaive", [("glaive_b",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_two_handed|itp_wooden_parry, itc_staff|itcf_carry_spear,
 352 , weight(4.5)|difficulty(0)|spd_rtng(90) | weapon_length(157)|swing_damage(39 , cut) | thrust_damage(21 ,  pierce),imodbits_polearm ],
["poleaxe",         "Poleaxe", [("pole_ax",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_two_handed|itp_wooden_parry, itc_staff,
 384 , weight(4.5)|difficulty(13)|spd_rtng(77) | weapon_length(180)|swing_damage(50 , cut) | thrust_damage(15 ,  blunt),imodbits_polearm ],
["polehammer",         "Polehammer", [("pole_hammer",0)], itp_type_polearm|itp_offset_lance| itp_primary|itp_two_handed|itp_wooden_parry, itc_staff,
 169 , weight(7)|difficulty(18)|spd_rtng(50) | weapon_length(126)|swing_damage(50 , blunt) | thrust_damage(35 ,  blunt),imodbits_polearm ],
["staff",         "Staff", [("wooden_staff",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_wooden_attack, itc_staff|itcf_carry_sword_back,
 36 , weight(1.5)|difficulty(0)|spd_rtng(100) | weapon_length(130)|swing_damage(18 , blunt) | thrust_damage(19 ,  blunt),imodbits_polearm ],
["quarter_staff", "Quarter Staff", [("quarter_staff",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_wooden_parry|itp_wooden_attack, itc_staff|itcf_carry_sword_back,
 60 , weight(2)|difficulty(0)|spd_rtng(104) | weapon_length(140)|swing_damage(20 , blunt) | thrust_damage(20 ,  blunt),imodbits_polearm ],
["iron_staff",         "Iron Staff", [("iron_staff",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary, itc_staff|itcf_carry_sword_back,
 202 , weight(2)|difficulty(0)|spd_rtng(97) | weapon_length(140)|swing_damage(25 , blunt) | thrust_damage(26 ,  blunt),imodbits_polearm ],

#["glaive_b",         "Glaive_b", [("glaive_b",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_two_handed|itp_wooden_parry, itc_staff|itcf_carry_spear,
# 352 , weight(4.5)|difficulty(0)|spd_rtng(83) | weapon_length(157)|swing_damage(38 , cut) | thrust_damage(21 ,  pierce),imodbits_polearm ],


["shortened_spear",         "Shortened Spear", [("spear_g_1-9m",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_wooden_parry, itc_staff|itcf_carry_spear,
 53 , weight(2.0)|difficulty(0)|spd_rtng(102) | weapon_length(120)|swing_damage(19 , blunt) | thrust_damage(25 ,  pierce),imodbits_polearm ],
["spear",         "Spear", [("spear_h_2-15m",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_wooden_parry, itc_staff|itcf_carry_spear,
 85 , weight(2.25)|difficulty(0)|spd_rtng(98) | weapon_length(135)|swing_damage(20 , blunt) | thrust_damage(26 ,  pierce),imodbits_polearm ],

["bamboo_spear",         "Bamboo Spear", [("arabian_spear_a_3m",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_staff|itcf_carry_spear,
 80 , weight(2.0)|difficulty(0)|spd_rtng(88) | weapon_length(200)|swing_damage(15 , blunt) | thrust_damage(20 ,  pierce),imodbits_polearm ],




["war_spear",         "War Spear", [("spear_i_2-3m",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_wooden_parry, itc_staff|itcf_carry_spear,
 140 , weight(2.5)|difficulty(0)|spd_rtng(95) | weapon_length(150)|swing_damage(20 , blunt) | thrust_damage(27 ,  pierce),imodbits_polearm ],
#TODO:["shortened_spear",         "shortened_spear", [("spear_e_2-1m",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_wooden_parry, itc_staff|itcf_carry_spear,
# 65 , weight(2.0)|difficulty(0)|spd_rtng(98) | weapon_length(110)|swing_damage(17 , blunt) | thrust_damage(23 ,  pierce),imodbits_polearm ],
#TODO:["spear_2-4m",         "spear", [("spear_e_2-25m",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_staff|itcf_carry_spear,
# 67 , weight(2.0)|difficulty(0)|spd_rtng(95) | weapon_length(125)|swing_damage(17 , blunt) | thrust_damage(23 ,  pierce),imodbits_polearm ],
["military_scythe",         "Military Scythe", [("spear_e_2-5m",0),("spear_c_2-5m",imodbits_bad)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_staff|itcf_carry_spear,
 155 , weight(2.5)|difficulty(0)|spd_rtng(90) | weapon_length(155)|swing_damage(36 , cut) | thrust_damage(25 ,  pierce),imodbits_polearm ],
["light_lance",         "Light Lance", [("spear_b_2-75m",0)], itp_couchable|itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear,
 180 , weight(2.5)|difficulty(0)|spd_rtng(85) | weapon_length(175)|swing_damage(16 , blunt) | thrust_damage(27 ,  pierce),imodbits_polearm ],
["lance",         "Lance", [("spear_d_2-8m",0)], itp_couchable|itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear,
 270 , weight(2.5)|difficulty(0)|spd_rtng(80) | weapon_length(180)|swing_damage(16 , blunt) | thrust_damage(26 ,  pierce),imodbits_polearm ],
["heavy_lance",         "Heavy Lance", [("spear_f_2-9m",0)], itp_couchable|itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear,
 360 , weight(2.75)|difficulty(10)|spd_rtng(75) | weapon_length(190)|swing_damage(16 , blunt) | thrust_damage(26 ,  pierce),imodbits_polearm ],
["great_lance",         "Great Lance", [("heavy_lance",0)], itp_couchable|itp_type_polearm|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_greatlance, 
 410 , weight(5)|difficulty(11)|spd_rtng(55) | weapon_length(240)|swing_damage(0 , cut) | thrust_damage(21 ,  pierce),imodbits_polearm ],
["pike",         "Pike", [("spear_a_3m",0)], itp_type_polearm|itp_merchandise| itp_cant_use_on_horseback|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_two_handed, itc_cutting_spear,
 125 , weight(3.0)|difficulty(0)|spd_rtng(81) | weapon_length(245)|swing_damage(16 , blunt) | thrust_damage(26 ,  pierce),imodbits_polearm ],
##["spear_e_3-25m",         "Spear_3-25m", [("spear_e_3-25m",0)], itp_type_polearm|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear|itcf_carry_spear,
## 150 , weight(4.5)|difficulty(0)|spd_rtng(81) | weapon_length(225)|swing_damage(19 , blunt) | thrust_damage(23 ,  pierce),imodbits_polearm ],
["ashwood_pike", "Ashwood Pike", [("pike",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_two_handed|itp_wooden_parry, itc_cutting_spear,
 205 , weight(3.5)|difficulty(9)|spd_rtng(90) | weapon_length(170)|swing_damage(19 , blunt) | thrust_damage(29,  pierce),imodbits_polearm ],
["awlpike",    "Awlpike", [("awl_pike_b",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear|itcf_carry_spear,
 345 , weight(2.25)|difficulty(0)|spd_rtng(92) | weapon_length(165)|swing_damage(20 , blunt) | thrust_damage(33 ,  pierce),imodbits_polearm ],
["awlpike_long",  "Long Awlpike", [("awl_pike_a",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear|itcf_carry_spear,
 385 , weight(2.25)|difficulty(0)|spd_rtng(89) | weapon_length(185)|swing_damage(20 , blunt) | thrust_damage(32 ,  pierce),imodbits_polearm ],
#["awlpike",         "Awlpike", [("pike",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_two_handed|itp_wooden_parry, itc_cutting_spear|itcf_carry_spear,
# 378 , weight(3.5)|difficulty(12)|spd_rtng(92) | weapon_length(160)|swing_damage(20 ,blunt) | thrust_damage(31 ,  pierce),imodbits_polearm ],

["bec_de_corbin_a",  "War Hammer", [("bec_de_corbin_a",0)], itp_type_polearm|itp_merchandise| itp_cant_use_on_horseback|itp_primary|itp_penalty_with_shield|itp_wooden_parry|itp_two_handed, itc_cutting_spear|itcf_carry_spear,
 125 , weight(3.0)|difficulty(0)|spd_rtng(81) | weapon_length(120)|swing_damage(38, blunt) | thrust_damage(38 ,  pierce),imodbits_polearm ],



# SHIELDS

["wooden_shield", "Wooden Shield", [("shield_round_a",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|shield_width(50),imodbits_shield ],
##["wooden_shield", "Wooden Shield", [("shield_round_a",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  42 , weight(2)|hit_points(360)|body_armor(1)|spd_rtng(100)|shield_width(50),imodbits_shield,




#["round_shield", "Round Shield", [("shield_round_c",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  64 , weight(2)|hit_points(400)|body_armor(1)|spd_rtng(100)|shield_width(50),imodbits_shield ],
["nordic_shield", "Nordic Shield", [("shield_round_b",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  95 , weight(2)|hit_points(440)|body_armor(1)|spd_rtng(100)|shield_width(50),imodbits_shield ],
#["kite_shield",         "Kite Shield", [("shield_kite_a",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
#["kite_shield_", "Kite Shield", [("shield_kite_b",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
#["large_shield", "Large Shield", [("shield_kite_c",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  165 , weight(2.5)|hit_points(520)|body_armor(1)|spd_rtng(80)|shield_width(92),imodbits_shield ],
#["battle_shield", "Battle Shield", [("shield_kite_d",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  196 , weight(3)|hit_points(560)|body_armor(1)|spd_rtng(78)|shield_width(94),imodbits_shield ],
["fur_covered_shield",  "Fur Covered Shield", [("shield_kite_m",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  227 , weight(3.5)|hit_points(600)|body_armor(1)|spd_rtng(76)|shield_width(81),imodbits_shield ],
#["heraldric_shield", "Heraldric Shield", [("shield_heraldic",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  301 , weight(3.5)|hit_points(640)|body_armor(1)|spd_rtng(83)|shield_width(65),imodbits_shield ],
#["heater_shield", "Heater Shield", [("shield_heater_a",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  477 , weight(3.5)|hit_points(710)|body_armor(4)|spd_rtng(80)|shield_width(60),imodbits_shield ],
["steel_shield", "Steel Shield", [("shield_dragon",0)], itp_merchandise|itp_type_shield, itcf_carry_round_shield,  697 , weight(4)|hit_points(700)|body_armor(17)|spd_rtng(61)|shield_width(40),imodbits_shield ],
#["nomad_shield", "Nomad Shield", [("shield_wood_b",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  12 , weight(2)|hit_points(260)|body_armor(6)|spd_rtng(110)|shield_width(30),imodbits_shield ],

["plate_covered_round_shield", "Plate Covered Round Shield", [("shield_round_e",0)], itp_type_shield, itcf_carry_round_shield,  140 , weight(4)|hit_points(330)|body_armor(16)|spd_rtng(90)|shield_width(40),imodbits_shield ],
["leather_covered_round_shield", "Leather Covered Round Shield", [("shield_round_d",0)], itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  80 , weight(2.5)|hit_points(310)|body_armor(8)|spd_rtng(96)|shield_width(40),imodbits_shield ],
["hide_covered_round_shield", "Hide Covered Round Shield", [("shield_round_f",0)], itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  40 , weight(2)|hit_points(260)|body_armor(3)|spd_rtng(100)|shield_width(40),imodbits_shield ],

["shield_heater_c", "Heater Shield", [("shield_heater_c",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  277 , weight(3.5)|hit_points(410)|body_armor(2)|spd_rtng(80)|shield_width(50),imodbits_shield ],
#["shield_heater_d", "Heater Shield", [("shield_heater_d",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  477 , weight(3.5)|hit_points(710)|body_armor(4)|spd_rtng(80)|shield_width(60),imodbits_shield ],

#["shield_kite_g",         "Kite Shield g", [("shield_kite_g",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
#["shield_kite_h",         "Kite Shield h", [("shield_kite_h",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
#["shield_kite_i",         "Kite Shield i ", [("shield_kite_i",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
#["shield_kite_k",         "Kite Shield k", [("shield_kite_k",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],

["norman_shield_1",         "Kite Shield", [("norman_shield_1",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
["norman_shield_2",         "Kite Shield", [("norman_shield_2",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
["norman_shield_3",         "Kite Shield", [("norman_shield_3",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
["norman_shield_4",         "Kite Shield", [("norman_shield_4",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
["norman_shield_5",         "Kite Shield", [("norman_shield_5",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
["norman_shield_6",         "Kite Shield", [("norman_shield_6",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
["norman_shield_7",         "Kite Shield", [("norman_shield_7",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
["norman_shield_8",         "Kite Shield", [("norman_shield_8",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],

["tab_shield_round_a", "Old Round Shield", [("tableau_shield_round_5",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  
26 , weight(2.5)|hit_points(195)|body_armor(4)|spd_rtng(93)|shield_width(50),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_round_shield_5", ":agent_no", ":troop_no")])]],
["tab_shield_round_b", "Plain Round Shield", [("tableau_shield_round_3",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  
65 , weight(3)|hit_points(260)|body_armor(8)|spd_rtng(90)|shield_width(50),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_round_shield_3", ":agent_no", ":troop_no")])]],
["tab_shield_round_c", "Round Shield", [("tableau_shield_round_2",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  
105 , weight(3.5)|hit_points(310)|body_armor(12)|spd_rtng(87)|shield_width(50),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner","tableau_round_shield_2", ":agent_no", ":troop_no")])]],
["tab_shield_round_d", "Heavy Round Shield", [("tableau_shield_round_1",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  
210 , weight(4)|hit_points(350)|body_armor(15)|spd_rtng(84)|shield_width(50),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_round_shield_1", ":agent_no", ":troop_no")])]],
["tab_shield_round_e", "Huscarl's Round Shield", [("tableau_shield_round_4",0)], itp_merchandise|itp_type_shield, itcf_carry_round_shield,  
430 , weight(4.5)|hit_points(410)|body_armor(19)|spd_rtng(81)|shield_width(50),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_round_shield_4", ":agent_no", ":troop_no")])]],

["tab_shield_kite_a", "Old Kite Shield",   [("tableau_shield_kite_1" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
33 , weight(2)|hit_points(165)|body_armor(5)|spd_rtng(96)|shield_width(36)|shield_height(70),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_kite_shield_1", ":agent_no", ":troop_no")])]],
["tab_shield_kite_b", "Plain Kite Shield",   [("tableau_shield_kite_3" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
70 , weight(2.5)|hit_points(215)|body_armor(10)|spd_rtng(93)|shield_width(36)|shield_height(70),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_kite_shield_3", ":agent_no", ":troop_no")])]],
["tab_shield_kite_c", "Kite Shield",   [("tableau_shield_kite_2" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
156 , weight(3)|hit_points(265)|body_armor(13)|spd_rtng(90)|shield_width(36)|shield_height(70),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_kite_shield_2", ":agent_no", ":troop_no")])]],
["tab_shield_kite_d", "Heavy Kite Shield",   [("tableau_shield_kite_2" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
320 , weight(3.5)|hit_points(310)|body_armor(18)|spd_rtng(87)|shield_width(36)|shield_height(70),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_kite_shield_2", ":agent_no", ":troop_no")])]],
["tab_shield_kite_cav_a", "Horseman's Kite Shield",   [("tableau_shield_kite_4" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
205 , weight(2)|hit_points(165)|body_armor(14)|spd_rtng(103)|shield_width(30)|shield_height(50),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_kite_shield_4", ":agent_no", ":troop_no")])]],
["tab_shield_kite_cav_b", "Knightly Kite Shield",   [("tableau_shield_kite_4" ,0)], itp_merchandise|itp_type_shield, itcf_carry_kite_shield,  
360 , weight(2.5)|hit_points(225)|body_armor(23)|spd_rtng(100)|shield_width(30)|shield_height(50),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_kite_shield_4", ":agent_no", ":troop_no")])]],

["tab_shield_heater_a", "Old Heater Shield",   [("tableau_shield_heater_1" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
36 , weight(2)|hit_points(160)|body_armor(6)|spd_rtng(96)|shield_width(36)|shield_height(70),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heater_shield_1", ":agent_no", ":troop_no")])]],
["tab_shield_heater_b", "Plain Heater Shield",   [("tableau_shield_heater_1" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
74 , weight(2.5)|hit_points(210)|body_armor(11)|spd_rtng(93)|shield_width(36)|shield_height(70),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heater_shield_1", ":agent_no", ":troop_no")])]],
["tab_shield_heater_c", "Heater Shield",   [("tableau_shield_heater_1" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
160 , weight(3)|hit_points(260)|body_armor(14)|spd_rtng(90)|shield_width(36)|shield_height(70),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heater_shield_1", ":agent_no", ":troop_no")])]],
["tab_shield_heater_d", "Heavy Heater Shield",   [("tableau_shield_heater_1" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
332 , weight(3.5)|hit_points(305)|body_armor(19)|spd_rtng(87)|shield_width(36)|shield_height(70),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heater_shield_1", ":agent_no", ":troop_no")])]],
["tab_shield_heater_cav_a", "Horseman's Heater Shield",   [("tableau_shield_heater_2" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
229 , weight(2)|hit_points(160)|body_armor(16)|spd_rtng(103)|shield_width(30)|shield_height(50),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heater_shield_2", ":agent_no", ":troop_no")])]],
["tab_shield_heater_cav_b", "Knightly Heater Shield",   [("tableau_shield_heater_2" ,0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  
390 , weight(2.5)|hit_points(220)|body_armor(23)|spd_rtng(100)|shield_width(30)|shield_height(50),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heater_shield_2", ":agent_no", ":troop_no")])]],

["tab_shield_pavise_a", "Old Board Shield",   [("tableau_shield_pavise_2" ,0)], itp_merchandise|itp_type_shield|itp_cant_use_on_horseback|itp_wooden_parry, itcf_carry_board_shield,  
60 , weight(3.5)|hit_points(280)|body_armor(4)|spd_rtng(89)|shield_width(43)|shield_height(100),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_pavise_shield_2", ":agent_no", ":troop_no")])]],
["tab_shield_pavise_b", "Plain Board Shield",   [("tableau_shield_pavise_2" ,0)], itp_merchandise|itp_type_shield|itp_cant_use_on_horseback|itp_wooden_parry, itcf_carry_board_shield,  
114 , weight(4)|hit_points(360)|body_armor(8)|spd_rtng(85)|shield_width(43)|shield_height(100),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_pavise_shield_2", ":agent_no", ":troop_no")])]],
["tab_shield_pavise_c", "Board Shield",   [("tableau_shield_pavise_1" ,0)], itp_merchandise|itp_type_shield|itp_cant_use_on_horseback|itp_wooden_parry, itcf_carry_board_shield,  
210 , weight(4.5)|hit_points(430)|body_armor(10)|spd_rtng(81)|shield_width(43)|shield_height(100),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_pavise_shield_1", ":agent_no", ":troop_no")])]],
["tab_shield_pavise_d", "Heavy Board Shield",   [("tableau_shield_pavise_1" ,0)], itp_merchandise|itp_type_shield|itp_cant_use_on_horseback|itp_wooden_parry, itcf_carry_board_shield,  
370 , weight(5)|hit_points(550)|body_armor(14)|spd_rtng(78)|shield_width(43)|shield_height(100),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_pavise_shield_1", ":agent_no", ":troop_no")])]],

["tab_shield_small_round_a", "Plain Cavalry Shield", [("tableau_shield_small_round_3",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  
96 , weight(2)|hit_points(160)|body_armor(8)|spd_rtng(105)|shield_width(40),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_small_round_shield_3", ":agent_no", ":troop_no")])]],
["tab_shield_small_round_b", "Round Cavalry Shield", [("tableau_shield_small_round_1",0)], itp_merchandise|itp_type_shield|itp_wooden_parry, itcf_carry_round_shield,  
195 , weight(2.5)|hit_points(200)|body_armor(14)|spd_rtng(103)|shield_width(40),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_small_round_shield_1", ":agent_no", ":troop_no")])]],
["tab_shield_small_round_c", "Elite Cavalry Shield", [("tableau_shield_small_round_2",0)], itp_merchandise|itp_type_shield, itcf_carry_round_shield,  
370 , weight(3)|hit_points(250)|body_armor(22)|spd_rtng(100)|shield_width(40),imodbits_shield,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_small_round_shield_2", ":agent_no", ":troop_no")])]],


 #RANGED
["darts",         "Darts", [("dart_b",0),("dart_b_bag", ixmesh_carry)], itp_type_thrown |itp_merchandise|itp_primary ,itcf_throw_javelin|itcf_carry_quiver_right_vertical|itcf_show_holster_when_drawn, 
155 , weight(4)|difficulty(1)|spd_rtng(95) | shoot_speed(28) | thrust_damage(22 ,  pierce)|max_ammo(7)|weapon_length(32),imodbits_thrown ],
["war_darts",         "War Darts", [("dart_a",0),("dart_a_bag", ixmesh_carry)], itp_type_thrown |itp_merchandise|itp_primary ,itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 
285 , weight(5)|difficulty(1)|spd_rtng(93) | shoot_speed(27) | thrust_damage(25 ,  pierce)|max_ammo(7)|weapon_length(45),imodbits_thrown ],

["javelin",         "Javelins", [("javelin",0),("javelins_quiver_new", ixmesh_carry)], itp_type_thrown |itp_merchandise|itp_primary|itp_next_item_as_melee ,itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 
300, weight(4)|difficulty(1)|spd_rtng(91) | shoot_speed(25) | thrust_damage(34 ,  pierce)|max_ammo(5)|weapon_length(75),imodbits_thrown ],
["javelin_melee",         "Javelin", [("javelin",0)], itp_type_polearm|itp_primary|itp_wooden_parry , itc_staff, 
300, weight(1)|difficulty(0)|spd_rtng(95) |swing_damage(12, cut)| thrust_damage(14,  pierce)|weapon_length(75),imodbits_polearm ],

["throwing_spears",         "Throwing Spears", [("jarid_new_b",0),("jarid_new_b_bag", ixmesh_carry)], itp_type_thrown |itp_merchandise|itp_primary|itp_next_item_as_melee ,itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 
525 , weight(3)|difficulty(2)|spd_rtng(87) | shoot_speed(22) | thrust_damage(44 ,  pierce)|max_ammo(4)|weapon_length(65),imodbits_thrown ],
["throwing_spear_melee",         "Throwing Spear", [("jarid_new_b",0),("javelins_quiver", ixmesh_carry)],itp_type_polearm|itp_primary|itp_wooden_parry , itc_staff, 
525 , weight(1)|difficulty(1)|spd_rtng(91) | swing_damage(18, cut) | thrust_damage(23 ,  pierce)|weapon_length(75),imodbits_thrown ],

["jarid",         "Jarids", [("jarid_new",0),("jarid_quiver", ixmesh_carry)], itp_type_thrown |itp_merchandise|itp_primary|itp_next_item_as_melee ,itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 
560 , weight(2.75)|difficulty(2)|spd_rtng(89) | shoot_speed(24) | thrust_damage(45 ,  pierce)|max_ammo(4)|weapon_length(65),imodbits_thrown ],
["jarid_melee",         "Jarid", [("jarid_new",0),("jarid_quiver", ixmesh_carry)], itp_type_polearm|itp_primary|itp_wooden_parry , itc_staff,
560 , weight(1)|difficulty(2)|spd_rtng(93) | swing_damage(16, cut) | thrust_damage(20 ,  pierce)|weapon_length(65),imodbits_thrown ],

["stones", "Stones", [("throwing_stone",0)], itp_type_thrown |itp_merchandise|itp_primary ,itcf_throw_stone, 1 , weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(30) | thrust_damage(11 ,  blunt)|max_ammo(18)|weapon_length(8),imodbit_large_bag ],

["throwing_knives", "Throwing Knives", [("throwing_knife",0)], itp_type_thrown |itp_merchandise|itp_primary ,itcf_throw_knife, 76 , weight(2.5)|difficulty(0)|spd_rtng(121) | shoot_speed(25) | thrust_damage(19 ,  cut)|max_ammo(14)|weapon_length(0),imodbits_thrown ],
["throwing_daggers", "Throwing Daggers", [("throwing_dagger",0)], itp_type_thrown |itp_merchandise|itp_primary ,itcf_throw_knife, 193 , weight(2.5)|difficulty(0)|spd_rtng(110) | shoot_speed(24) | thrust_damage(25 ,  cut)|max_ammo(13)|weapon_length(0),imodbits_thrown ],

["light_throwing_axes", "Light Throwing Axes", [("francisca",0)], itp_type_thrown |itp_merchandise|itp_primary|itp_next_item_as_melee,itcf_throw_axe,
360, weight(5)|difficulty(2)|spd_rtng(99) | shoot_speed(18) | thrust_damage(35,cut)|max_ammo(4)|weapon_length(53),imodbits_thrown_minus_heavy ],
["light_throwing_axes_melee", "Light Throwing Axe", [("francisca",0)], itp_type_one_handed_wpn |itp_primary|itp_bonus_against_shield,itc_scimitar,
360, weight(1)|difficulty(2)|spd_rtng(99)|weapon_length(53)| swing_damage(26,cut),imodbits_thrown_minus_heavy ],
["throwing_axes", "Throwing Axes", [("throwing_axe_a",0)], itp_type_thrown |itp_merchandise|itp_primary|itp_next_item_as_melee,itcf_throw_axe,
490, weight(5)|difficulty(3)|spd_rtng(98) | shoot_speed(18) | thrust_damage(39,cut)|max_ammo(4)|weapon_length(53),imodbits_thrown_minus_heavy ],
["throwing_axes_melee", "Throwing Axe", [("throwing_axe_a",0)], itp_type_one_handed_wpn |itp_primary|itp_bonus_against_shield,itc_scimitar,
490, weight(1)|difficulty(3)|spd_rtng(98) | swing_damage(29,cut)|weapon_length(53),imodbits_thrown_minus_heavy ],
["heavy_throwing_axes", "Heavy Throwing Axes", [("throwing_axe_b",0)], itp_type_thrown |itp_merchandise|itp_primary|itp_next_item_as_melee,itcf_throw_axe,
620, weight(5)|difficulty(4)|spd_rtng(97) | shoot_speed(18) | thrust_damage(44,cut)|max_ammo(4)|weapon_length(53),imodbits_thrown_minus_heavy ],
["heavy_throwing_axes_melee", "Heavy Throwing Axe", [("throwing_axe_b",0)], itp_type_one_handed_wpn |itp_primary|itp_bonus_against_shield,itc_scimitar,
620, weight(1)|difficulty(4)|spd_rtng(97) | swing_damage(32,cut)|weapon_length(53),imodbits_thrown_minus_heavy ],



["hunting_bow",         "Hunting Bow", [("hunting_bow",0),("hunting_bow_carry",ixmesh_carry)],itp_type_bow |itp_merchandise|itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bow_back, 
17 , weight(1)|difficulty(0)|spd_rtng(100) | shoot_speed(52) | thrust_damage(15 ,  pierce),imodbits_bow ],
["short_bow",         "Short Bow", [("short_bow",0),("short_bow_carry",ixmesh_carry)], itp_type_bow |itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bow_back, 
58 , weight(1)|difficulty(1)|spd_rtng(97) | shoot_speed(55) | thrust_damage(18 ,  pierce  ),imodbits_bow ],
["nomad_bow",         "Nomad Bow", [("nomad_bow",0),("nomad_bow_case", ixmesh_carry)], itp_type_bow |itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 
164 , weight(1.25)|difficulty(2)|spd_rtng(94) | shoot_speed(56) | thrust_damage(20 ,  pierce),imodbits_bow ],
["long_bow",         "Long Bow", [("long_bow",0),("long_bow_carry",ixmesh_carry)], itp_type_bow |itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bow_back, 
145 , weight(1.75)|difficulty(3)|spd_rtng(79) | shoot_speed(56) | thrust_damage(22 ,  pierce),imodbits_bow ],
["khergit_bow",         "Khergit Bow", [("khergit_bow",0),("khergit_bow_case", ixmesh_carry)], itp_type_bow |itp_merchandise|itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 
269 , weight(1.25)|difficulty(3)|spd_rtng(90) | shoot_speed(57) | thrust_damage(21 ,pierce),imodbits_bow ],
["strong_bow",         "Strong Bow", [("strong_bow",0),("strong_bow_case", ixmesh_carry)], itp_type_bow |itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 
437 , weight(1.25)|difficulty(3)|spd_rtng(88) | shoot_speed(58) | thrust_damage(23 ,pierce),imodbit_cracked | imodbit_bent | imodbit_masterwork ],
["war_bow",         "War Bow", [("war_bow",0),("war_bow_carry",ixmesh_carry)],itp_type_bow|itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bow_back, 
728 , weight(1.5)|difficulty(4)|spd_rtng(84) | shoot_speed(59) | thrust_damage(25 ,pierce),imodbits_bow ],
["hunting_crossbow", "Hunting Crossbow", [("crossbow_a",0)], itp_type_crossbow |itp_merchandise|itp_primary|itp_two_handed ,itcf_shoot_crossbow|itcf_carry_crossbow_back, 
22 , weight(2.25)|difficulty(0)|spd_rtng(47) | shoot_speed(50) | thrust_damage(37 ,  pierce)|max_ammo(1),imodbits_crossbow ],
["light_crossbow", "Light Crossbow", [("crossbow_b",0)], itp_type_crossbow |itp_merchandise|itp_primary|itp_two_handed, itcf_shoot_crossbow|itcf_carry_crossbow_back, 
67 , weight(2.5)|difficulty(8)|spd_rtng(45) | shoot_speed(59) | thrust_damage(44 ,  pierce)|max_ammo(1),imodbits_crossbow ],
["crossbow",         "Crossbow",         [("crossbow_a",0)], itp_type_crossbow |itp_merchandise|itp_primary|itp_two_handed|itp_cant_reload_on_horseback ,itcf_shoot_crossbow|itcf_carry_crossbow_back, 
182 , weight(3)|difficulty(8)|spd_rtng(43) | shoot_speed(66) | thrust_damage(49,pierce)|max_ammo(1),imodbits_crossbow ],
["heavy_crossbow", "Heavy Crossbow", [("crossbow_c",0)], itp_type_crossbow |itp_merchandise|itp_primary|itp_two_handed|itp_cant_reload_on_horseback ,itcf_shoot_crossbow|itcf_carry_crossbow_back, 
349 , weight(3.5)|difficulty(9)|spd_rtng(41) | shoot_speed(68) | thrust_damage(58 ,pierce)|max_ammo(1),imodbits_crossbow ],
["sniper_crossbow", "Siege Crossbow", [("crossbow_c",0)], itp_type_crossbow |itp_merchandise|itp_primary|itp_two_handed|itp_cant_reload_on_horseback ,itcf_shoot_crossbow|itcf_carry_crossbow_back, 
683 , weight(3.75)|difficulty(10)|spd_rtng(37) | shoot_speed(70) | thrust_damage(63 ,pierce)|max_ammo(1),imodbits_crossbow ],
["flintlock_pistol", "Flintlock Pistol", [("flintlock_pistol",0)], itp_type_pistol |itp_merchandise|itp_primary ,itcf_shoot_pistol|itcf_reload_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(38) | shoot_speed(160) | thrust_damage(45 ,pierce)|max_ammo(1)|accuracy(65),imodbits_none,
 [(ti_on_weapon_attack, [(play_sound,"snd_pistol_shot"),(position_move_x, pos1,27),(position_move_y, pos1,36),(particle_system_burst, "psys_pistol_smoke", pos1, 15)])]],
["torch",         "Torch", [("club",0)], itp_type_one_handed_wpn|itp_primary, itc_scimitar, 11 , weight(2.5)|difficulty(0)|spd_rtng(95) | weapon_length(95)|swing_damage(11 , blunt) | thrust_damage(0 ,  pierce),imodbits_none,
 [(ti_on_init_item, [(set_position_delta,0,60,0),(particle_system_add_new, "psys_torch_fire"),(particle_system_add_new, "psys_torch_smoke"),(set_current_color,150, 130, 70),(add_point_light, 10, 30),
])]],

["lyre",         "Lyre", [("lyre",0)], itp_type_shield|itp_wooden_parry|itp_civilian, itcf_carry_bow_back,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|weapon_length(90),0 ],
["lute",         "Lute", [("lute",0)], itp_type_shield|itp_wooden_parry|itp_civilian, itcf_carry_bow_back,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|weapon_length(90),0 ],

##["short_sword", "Short Sword",
## [("sword_norman",0),("sword_norman_scabbard", ixmesh_carry),("sword_norman_rusty",imodbit_rusty),("sword_norman_rusty_scabbard", ixmesh_carry|imodbit_rusty)],
## itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 183 , weight(1.25)|difficulty(0)|spd_rtng(103) | weapon_length(75)|swing_damage(25 , cut) | thrust_damage(19 ,  pierce),imodbits_sword ],

["strange_armor",  "Strange Armor", [("samurai_armor",0)], itp_type_body_armor  |itp_covers_legs ,0, 1259 , weight(18)|abundance(100)|head_armor(0)|body_armor(38)|leg_armor(19)|difficulty(7) ,imodbits_armor ],
["strange_boots",  "Strange Boots", [("samurai_boots",0)], itp_type_foot_armor | itp_attach_armature,0, 465 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(21)|difficulty(0) ,imodbits_cloth ],
["strange_helmet", "Strange Helmet", [("samurai_helmet",0)], itp_type_head_armor   ,0, 824 , weight(2)|abundance(100)|head_armor(44)|body_armor(0)|leg_armor(0)|difficulty(7) ,imodbits_plate ],
["strange_sword", "Strange Sword", [("katana",0),("katana_scabbard",ixmesh_carry)], itp_type_two_handed_wpn| itp_primary, itc_bastardsword|itcf_carry_katana|itcf_show_holster_when_drawn, 679 , weight(2.0)|difficulty(9)|spd_rtng(108) | weapon_length(95)|swing_damage(32 , cut) | thrust_damage(18 ,  pierce),imodbits_sword ],
["strange_great_sword",  "Strange Great Sword", [("no_dachi",0),("no_dachi_scabbard",ixmesh_carry)], itp_type_two_handed_wpn|itp_two_handed|itp_primary, itc_nodachi|itcf_carry_sword_back|itcf_show_holster_when_drawn, 920 , weight(3.5)|difficulty(11)|spd_rtng(92) | weapon_length(125)|swing_damage(38 , cut) | thrust_damage(0 ,  pierce),imodbits_axe ],
["strange_short_sword", "Strange Short Sword", [("wakizashi",0),("wakizashi_scabbard",ixmesh_carry)], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_wakizashi|itcf_show_holster_when_drawn, 321 , weight(1.25)|difficulty(0)|spd_rtng(108) | weapon_length(65)|swing_damage(25 , cut) | thrust_damage(19 ,  pierce),imodbits_sword ],
["court_dress", "Court Dress", [("court_dress",0)], itp_type_body_armor|itp_covers_legs|itp_civilian   ,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(14)|leg_armor(4)|difficulty(0) ,imodbits_cloth ],
["rich_outfit", "Rich Outfit", [("merchant_outf",0)], itp_type_body_armor|itp_covers_legs|itp_civilian   ,0, 348 , weight(4)|abundance(100)|head_armor(0)|body_armor(16)|leg_armor(4)|difficulty(0) ,imodbits_cloth ],
["khergit_guard_armor", "Khergit Guard Armor", [("lamellar_armor_a",0)], itp_type_body_armor|itp_covers_legs   ,0, 
3048 , weight(25)|abundance(100)|head_armor(0)|body_armor(50)|leg_armor(18)|difficulty(0) ,imodbits_armor ],
#["leather_steppe_cap_c", "Leather Steppe Cap", [("leather_steppe_cap_c",0)], itp_type_head_armor   ,0, 51 , weight(2)|abundance(100)|head_armor(18)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["felt_steppe_cap", "Felt Steppe Cap", [("felt_steppe_cap",0)], itp_type_head_armor   ,0, 237 , weight(2)|abundance(100)|head_armor(16)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["khergit_war_helmet", "Khergit War Helmet", [("tattered_steppe_cap_a_new",0)], itp_type_head_armor | itp_merchandise   ,0, 200 , weight(2)|abundance(100)|head_armor(31)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["khergit_helmet", "Khergit Helmet", [("khergit_guard_helmet",0)], itp_type_head_armor   ,0, 361 , weight(2)|abundance(100)|head_armor(33)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
#["khergit_sword", "Khergit Sword", [("khergit_sword",0),("khergit_sword_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 183 , weight(1.25)|difficulty(0)|spd_rtng(100) | weapon_length(97)|swing_damage(23 , cut) | thrust_damage(14 ,  pierce),imodbits_sword ],
["khergit_guard_boots",  "Khergit Guard Boots", [("lamellar_boots_a",0)], itp_type_foot_armor | itp_attach_armature,0, 254 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(20)|difficulty(0) ,imodbits_cloth ],
["khergit_guard_helmet", "Khergit Guard Helmet", [("lamellar_helmet_a",0)], itp_type_head_armor |itp_merchandise   ,0, 433 , weight(2)|abundance(100)|head_armor(40)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["khergit_cavalry_helmet", "Khergit Cavalry Helmet", [("lamellar_helmet_b",0)], itp_type_head_armor | itp_merchandise   ,0, 333 , weight(2)|abundance(100)|head_armor(36)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],

["black_hood", "Black Hood", [("hood_black",0)], itp_type_head_armor|itp_merchandise   ,0, 193 , weight(2)|abundance(100)|head_armor(18)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["light_leather", "Light Leather", [("light_leather",0)], itp_type_body_armor|itp_covers_legs|itp_merchandise   ,0, 352 , weight(5)|abundance(100)|head_armor(0)|body_armor(26)|leg_armor(7)|difficulty(0) ,imodbits_armor ],
["light_leather_boots",  "Light Leather Boots", [("light_leather_boots",0)], itp_type_foot_armor |itp_merchandise| itp_attach_armature,0, 91 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(15)|difficulty(0) ,imodbits_cloth ],
["mail_and_plate", "Mail and Plate", [("mail_and_plate",0)], itp_type_body_armor|itp_covers_legs   ,0, 593 , weight(16)|abundance(100)|head_armor(0)|body_armor(34)|leg_armor(12)|difficulty(0) ,imodbits_armor ],
["light_mail_and_plate", "Light Mail and Plate", [("light_mail_and_plate",0)], itp_type_body_armor|itp_covers_legs   ,0, 532 , weight(10)|abundance(100)|head_armor(0)|body_armor(32)|leg_armor(12)|difficulty(0) ,imodbits_armor ],

["byzantion_helmet_a", "Byzantion Helmet", [("byzantion_helmet_a",0)], itp_type_head_armor   ,0, 278 , weight(2)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["magyar_helmet_a", "Magyar Helmet", [("magyar_helmet_a",0)], itp_type_head_armor   ,0, 278 , weight(2)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["rus_helmet_a", "Rus Helmet", [("rus_helmet_a",0)], itp_type_head_armor   ,0, 278 , weight(2)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["sipahi_helmet_a", "Sipahi Helmet", [("sipahi_helmet_a",0)], itp_type_head_armor   ,0, 278 , weight(2)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["shahi", "Shahi", [("shahi",0)], itp_type_head_armor   ,0, 278 , weight(2)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
["rabati", "Rabati", [("rabati",0)], itp_type_head_armor   ,0, 278 , weight(2)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],

["tunic_with_green_cape", "Tunic with Green Cape", [("peasant_man_a",0)], itp_merchandise| itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 6 , weight(1)|abundance(100)|head_armor(0)|body_armor(6)|leg_armor(2)|difficulty(0) ,imodbits_cloth ], 
["keys", "Ring of Keys", [("throwing_axe_a",0)], itp_type_one_handed_wpn |itp_primary|itp_bonus_against_shield,itc_scimitar,
240, weight(5)|spd_rtng(98) | swing_damage(29,cut)|max_ammo(5)|weapon_length(53),imodbits_thrown ], 
["bride_dress", "Bride Dress", [("bride_dress",0)], itp_type_body_armor  |itp_covers_legs|itp_civilian ,0, 500 , weight(3)|abundance(100)|head_armor(0)|body_armor(10)|leg_armor(10)|difficulty(0) ,imodbits_cloth],
["bride_crown", "Crown of Flowers", [("bride_crown",0)],  itp_type_head_armor | itp_doesnt_cover_hair |itp_civilian |itp_attach_armature,0, 1 , weight(0.5)|abundance(100)|head_armor(4)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
["bride_shoes", "Bride Shoes", [("bride_shoes",0)], itp_type_foot_armor |itp_civilian | itp_attach_armature ,0,
 30 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(8)|difficulty(0) ,imodbits_cloth ],

["practice_bow_2","Practice Bow", [("hunting_bow",0), ("hunting_bow_carry",ixmesh_carry)], itp_type_bow |itp_primary|itp_two_handed,itcf_shoot_bow|itcf_carry_bow_back, 0, weight(1.5)|spd_rtng(90) | shoot_speed(40) | thrust_damage(21, blunt),imodbits_bow ],
["practice_arrows_2","Practice Arrows", [("arena_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_type_arrows, itcf_carry_quiver_back, 0,weight(1.5)|weapon_length(95)|max_ammo(80),imodbits_missile],


["plate_boots", "Plate Boots", [("plate_boots",0)], itp_merchandise| itp_type_foot_armor | itp_attach_armature,0,
 1770 , weight(3.5)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(9) ,imodbits_plate ], 

["heraldic_mail_with_surcoat_for_tableau", "{!}Heraldic Mail with Surcoat", [("heraldic_armor_new_a",0)], itp_type_body_armor |itp_covers_legs ,0,
 1, weight(22)|abundance(100)|head_armor(0)|body_armor(1)|leg_armor(1),imodbits_armor,
 [(ti_on_init_item, [(store_trigger_param_1, ":agent_no"),(store_trigger_param_2, ":troop_no"),(call_script, "script_shield_item_set_banner", "tableau_heraldic_armor_a", ":agent_no", ":troop_no")])]],
["mail_boots_for_tableau", "Mail Boots", [("mail_boots_a",0)], itp_type_foot_armor | itp_attach_armature  ,0,
 1, weight(3)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(1) ,imodbits_armor ],
["warhorse_sarranid","Sarranian War Horse", [("warhorse_sarranid",0)], itp_merchandise|itp_type_horse, 0, 1811,abundance(40)|hit_points(165)|body_armor(58)|difficulty(4)|horse_speed(40)|horse_maneuver(44)|horse_charge(32)|horse_scale(112),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_6]],
["warhorse_steppe","Steppe Charger", [("warhorse_steppe",0)], itp_merchandise|itp_type_horse, 0, 1400,abundance(45)|hit_points(150)|body_armor(40)|difficulty(4)|horse_speed(40)|horse_maneuver(50)|horse_charge(28)|horse_scale(112),imodbits_horse_basic|imodbit_champion, [], [fac_kingdom_3,fac_kingdom_2]],


##INVASION MODE START
["javelin_bow",         "Javelin Bow", [("war_bow",0),("war_bow_carry",ixmesh_carry)],itp_type_bow|itp_primary|itp_two_handed ,itcf_shoot_bow|itcf_carry_bow_back, 
0 , weight(1.5)|difficulty(0)|spd_rtng(84) | shoot_speed(59) | thrust_damage(25 ,pierce), 0, [(ti_on_weapon_attack, [(play_sound,"snd_throw_javelin")])] ],
["knockdown_mace",         "Knockdown Mace", [("flanged_mace",0)], itp_type_one_handed_wpn|itp_can_knock_down| itp_primary|itp_wooden_parry, itc_scimitar|itcf_carry_mace_left_hip, 
0 , weight(3.5)|difficulty(0)|spd_rtng(103) | weapon_length(70)|swing_damage(24 , blunt) | thrust_damage(0 ,  pierce),imodbits_mace ],
["blood_drain_throwing_knives", "Blood Drain Throwing Knives", [("throwing_knife",0)], itp_type_thrown |itp_primary ,itcf_throw_knife, 0 , weight(2.5)|difficulty(0)|spd_rtng(121) | shoot_speed(25) | thrust_damage(25 ,  pierce)|max_ammo(5)|weapon_length(0),imodbits_thrown ],
["doom_javelins",         "Doom Javelins", [("jarid_new_b",0),("jarid_new_b_bag", ixmesh_carry)], itp_type_thrown |itp_primary ,itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 
0 , weight(3)|difficulty(0)|spd_rtng(87) | shoot_speed(22) | thrust_damage(44 ,  pierce)|max_ammo(2)|weapon_length(65),imodbits_thrown ],
#["unblockable_morningstar",         "Unblockable Morningstar", [("mace_morningstar_new",0)], itp_crush_through|itp_type_two_handed_wpn|itp_primary|itp_wooden_parry|itp_unbalanced, itc_morningstar|itcf_carry_mace_left_hip, 
#305 , weight(20)|difficulty(13)|spd_rtng(95) | weapon_length(85)|swing_damage(38 , pierce) | thrust_damage(0 ,  pierce),imodbits_mace ],
["disarming_throwing_axe", "Disarming Throwing Axe", [("throwing_axe_a",0)], itp_type_thrown |itp_primary,itcf_throw_axe,
0, weight(1)|difficulty(0)|spd_rtng(98) | shoot_speed(18) | thrust_damage(10,cut)|max_ammo(1)|weapon_length(53),imodbits_thrown_minus_heavy ],
["instakill_knife",         "Instakill Knife", [("peasant_knife_new",0)], itp_type_one_handed_wpn|itp_primary|itp_secondary|itp_no_parry|itp_two_handed, itc_dagger|itcf_carry_dagger_front_left, 
0 , weight(0.5)|difficulty(0)|spd_rtng(101) | weapon_length(40)|swing_damage(21 , cut) | thrust_damage(13 ,  pierce),imodbits_sword ],
["backstabber", "Backstabber", [("sword_viking_a_small",0),("sword_viking_a_small_scabbard", ixmesh_carry)], itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn,
0 , weight(1.25)|difficulty(0)|spd_rtng(103) | weapon_length(86)|swing_damage(20 , cut) | thrust_damage(13 ,  pierce),imodbits_sword_high ],
["weak_beserker_dart",         "Weak Beserker Dart", [("dart_b",0),("dart_b_bag", ixmesh_carry)], itp_type_thrown |itp_primary ,itcf_throw_javelin|itcf_carry_quiver_right_vertical|itcf_show_holster_when_drawn, 
0 , weight(4)|difficulty(0)|spd_rtng(95) | shoot_speed(28) | thrust_damage(5 ,  pierce)|max_ammo(1)|weapon_length(32),imodbits_thrown ],
["team_change_dart",         "Team Change Dart", [("dart_a",0),("dart_a_bag", ixmesh_carry)], itp_type_thrown |itp_primary ,itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 
0 , weight(5)|difficulty(0)|spd_rtng(93) | shoot_speed(27) | thrust_damage(5 ,  pierce)|max_ammo(1)|weapon_length(45),imodbits_thrown ],
["awesome_spear",         "Awesome Spear", [("spear",0)], itp_type_polearm| itp_primary|itp_penalty_with_shield|itp_wooden_parry,itc_staff|itcf_carry_spear, 
0 , weight(1.5)|difficulty(0)|spd_rtng(110) | weapon_length(157)|swing_damage(41 , cut) | thrust_damage(33 ,  pierce),imodbits_polearm ],


["running_boots",  "Running Boots", [("samurai_boots",0)], itp_type_foot_armor | itp_attach_armature,0, 0 , weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(21)|difficulty(0) ,imodbits_cloth ],
["power_gloves","Power Gloves", [("scale_gauntlets_a_L",0)], itp_type_hand_armor,0, 0, weight(0.9)|abundance(100)|body_armor(6)|difficulty(0),imodbits_armor],
#["wielding_gloves","Wielding Gloves", [("scale_gauntlets_b_L",0)], itp_type_hand_armor,0, 0, weight(0.75)|abundance(100)|body_armor(5)|difficulty(0),imodbits_armor],
["invulnerable_helmet", "Invulnerable Helmet", [("maciejowski_helmet_new",0)], itp_type_head_armor|itp_covers_head,0, 1240 , weight(2.75)|abundance(100)|head_armor(63)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_plate ],
["kicking_boots", "Kicking Boots", [("sarranid_camel_boots",0)],  itp_type_foot_armor |itp_civilian | itp_attach_armature ,0,
 0 , weight(3)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(20)|difficulty(0) ,imodbits_plate ],
["restore_health_armour",  "Restore Health Armour", [("samurai_armor",0)], itp_type_body_armor  |itp_covers_legs ,0, 0 , weight(12)|abundance(100)|head_armor(0)|body_armor(27)|leg_armor(11)|difficulty(0) ,imodbits_armor ],
#["extra_life_helmet", "Extra Life Helmet", [("byzantion_helmet_a",0)], itp_type_head_armor   ,0, 0 , weight(2)|abundance(100)|head_armor(20)|body_armor(0)|leg_armor(0) ,imodbits_cloth ],
#["scatter_crossbow", "Scatter Crossbow", [("crossbow_c",0)], itp_type_crossbow |itp_primary|itp_two_handed|itp_cant_reload_on_horseback ,itcf_shoot_crossbow|itcf_carry_crossbow_back, 
#0 , weight(3.75)|spd_rtng(20) | shoot_speed(90) | thrust_damage(90 ,pierce)|max_ammo(1),imodbits_crossbow ],

#additional items for coop
["javelin_bow_ammo",         "Shooting Javelins", [("javelin_bow_ammo",0),("javelins_quiver_new", ixmesh_carry)], itp_type_arrows ,itcf_carry_quiver_back, 
0, weight(4) | thrust_damage(34 ,  pierce)|max_ammo(15)|weapon_length(75),0 ],
#["scatter_bolts","Scatter Bolts", [("bolt",0),("flying_missile",ixmesh_flying_ammo),("bolt_bag", ixmesh_carry),("bolt_bag_b", ixmesh_carry|imodbit_large_bag)], itp_type_bolts|itp_merchandise|itp_default_ammo|itp_can_penetrate_shield, itcf_carry_quiver_right_vertical, 
#0,weight(2.25)|abundance(90)|weapon_length(63)|thrust_damage(1,pierce)|max_ammo(4),imodbits_missile],
["ccoop_new_items_end", "Items End", [("shield_round_a",0)], 0, 0, 1, 0, 0],
#INVASION MODE END

# ##################################################################
# ## Fate/rider saber Items
# ##################################################################

#Servants Items
 #Generalized Gear
 #Saber Gear
	#Artoria Pendragon
	
	# # For so-called laser sabers we'll use a script that allows you to ####
	# ## enter a width of beam and length of beam. this will allow us to ####
	# ## use agent_get_look_position to draw rectangle to check if agents are
	# ## inside and deal damage in those cases then take that info for ######
	# ## particle_system_burst to show off the laser. Look for a better #####
	# ## solutions ##########################################################
	
		#Excalibur, basic
	["excalibur", "Excalibur", [("excalibur",0)], itp_no_blur|itp_type_two_handed_wpn|itp_can_knock_down|itp_two_handed|itp_unique|itp_primary, itc_bastardsword|itcf_carry_sword_back, 526, weight(2.25)|difficulty(9)|spd_rtng(80) | weapon_length(115)|swing_damage(32, cut) | thrust_damage(27, pierce),imodbits_sword_high ],
	
		#Excalibur - Invisible Wind
	["excalibur_invis", "Excalibur", [("excalibur_invis",0)], itp_no_blur|itp_type_two_handed_wpn|itp_can_knock_down|itp_two_handed|itp_unique|itp_primary, itc_bastardsword|itcf_carry_sword_back, 526, weight(2.25)|difficulty(9)|spd_rtng(80) | weapon_length(115)|swing_damage(40, cut) | thrust_damage(37, pierce),imodbits_sword_high,
		[(ti_on_init_item, [
			(store_trigger_param_1, ":agent"),
			(neq, ":agent", -1),
            (agent_get_position, pos0, ":agent"),
			
			(try_for_range, reg4, 10, 120),
				(set_position_delta, pos0, reg4, 0, 0), #Position, X, Y, Z.
				(particle_system_add_new, "psys_invisible_air", pos0),
				(val_add, reg4, 10),
			(try_end),])]],
			
		# Excalibur - Activated
	["excalibur_gold", "Excalibur (Activated)", [("excalibur",0)], itp_no_blur|itp_type_two_handed_wpn|itp_can_knock_down|itp_two_handed|itp_unique|itp_primary, itc_bastardsword|itcf_carry_sword_back, 526 , weight(2.25)|difficulty(9)|spd_rtng(80) | weapon_length(115)|swing_damage(52, cut) | thrust_damage(42, pierce),imodbits_sword_high ],
	
		#Caliburn
	["caliburn", "Caliburn", [("caliburn",0),], itp_no_blur|itp_type_two_handed_wpn|itp_can_knock_down|itp_two_handed|itp_unique|itp_primary, itc_bastardsword|itcf_carry_sword_back,
	526 , weight(2.25)|difficulty(0)|spd_rtng(80) | weapon_length(110)|swing_damage(31, cut) | thrust_damage(27, pierce),imodbits_sword_high ],
	
		#Avalon
	["avalon", "Avalon", [("norman_shield_3",0)], itp_type_shield|itp_wooden_parry, itcf_carry_kite_shield,  118 , weight(2.5)|hit_points(480)|body_armor(1)|spd_rtng(82)|shield_width(90),imodbits_shield ],
	
		#armor
	["artoria_plate", "Plate Armor", [("artoriaarmor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],
	
	["artoria_casual_dress", "Borrowed Dress", [("artoria_casual",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(25)|leg_armor(12)|difficulty(2), imodbits_cloth],
	["artoria_casual_boots", "Borrowed Boots", [("artoria_casual_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0) ,imodbits_cloth], 
	
		#dope gauntlets
	["artoria_gauntlets","Gauntlets", [("artoria_gaunt_L",0),("artoria_gaunt_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],
	
		#shoes
	["artoria_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ], 
	
	#Nero
		# Aestus Estus
	["aestus_estus",  "Aestus Estus", [("aestus_estus_updated",0),], itp_no_blur|itp_type_two_handed_wpn|itp_two_handed|itp_unique|itp_primary, itc_nodachi|itcf_carry_sword_back, 920, weight(3.5)|difficulty(0)|spd_rtng(75) | weapon_length(152)|swing_damage(43, cut) | thrust_damage(0, pierce),imodbits_axe ],
	
		#red dress
	["nero_dress", "Red Dress", [("red_dress",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],
	
		#shoes
	["nero_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],
	
	#Siegfried
		#D-slayer sword
	["balmung", "Balmung", [("balmung",0),("scab_bastardsw_b", ixmesh_carry)], itp_no_blur|itp_type_two_handed_wpn|itp_unique|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back|itcf_show_holster_when_drawn,
	524, weight(1)|difficulty(0)|spd_rtng(94) | weapon_length(130)|swing_damage(57, cut) | thrust_damage(41,  pierce),imodbits_sword_high],
	
		#slutty Armor 
	["sieg_plate", "Plate Armor", [("full_plate_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],
	
		#gauntlets
	["sieg_gauntlets","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],
	
		#boots
	["sieg_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],
	
	#Fergus mac Roich
		#rainbow sword
	["caladbolg", "Caladbolg", [("caladbolg",0)], itp_no_blur|itp_type_two_handed_wpn|itp_unique|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back, 524, weight(1)|difficulty(0)|spd_rtng(94) | weapon_length(165)|swing_damage(57, cut) | thrust_damage(41, pierce), imodbits_sword_high ],
	
		#clothes
	["fergus_vest", "Fergus' Digs", [("archers_vest",0)], itp_unique|itp_type_body_armor|itp_covers_legs, 0, 260, weight(6)|abundance(0)|head_armor(0)|body_armor(72)|leg_armor(20)|difficulty(0) ,imodbits_cloth],
	
		#gauntlets
	["fergus_gauntlets","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],
	
		#boots
	["fergus_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],
	
	#Richard Lionheart
		#Not Excalibur but yes
	["excalibur_lion", "Excalibur", [("excalibur",0),], itp_no_blur|itp_type_two_handed_wpn|itp_can_knock_down|itp_two_handed|itp_unique|itp_primary, itc_bastardsword|itcf_carry_sword_back, 526 , weight(2.25)|difficulty(9)|spd_rtng(80) | weapon_length(115)|swing_damage(48, cut) | thrust_damage(37, pierce),imodbits_sword_high ],
	
		#Armor
	["lion_plate", "Plate Armor", [("tourn_armor_a",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],
	
		#boots
	["lion_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],
		# Gauntlets
	["lion_gauntlets","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],
	
 #Lancer Gear
	#Cu Chulainn
		# gae bolg
	["gae_bolg", "Gae Bolg", [("gae_bolg",0)], itp_no_blur|itp_type_polearm|itp_unique|itp_primary|itp_can_penetrate_shield|itp_wooden_parry, itc_staff, 76, weight(2.5)|difficulty(0)|spd_rtng(95)|weapon_length(201)|swing_damage(35, cut)|thrust_damage(45, pierce), imodbits_polearm, 
		attack_effects],
	
		# Gae Bolg - Thrown, Gotta find a way to make it change path and murder someone
	 ["gae_bolg_thrown", "Gae Bolg (Thrown)", [("gae_bolg",0)], itp_no_blur|itp_type_thrown |itp_unique|itp_extra_penetration|itp_remove_item_on_use|itp_crush_through|itp_no_pick_up_from_ground|itp_primary ,itcf_throw_javelin, 525 , weight(3)|difficulty(2)|spd_rtng(87)|shoot_speed(100)|thrust_damage(45,  pierce)|max_ammo(1)|weapon_length(201), imodbits_thrown,
			[(ti_on_missile_hit, [
				(set_fixed_point_multiplier, 100), 							# Will keep math consistant
				(store_trigger_param_1, ":agent"), 							# Store the shooting Agent
				# (store_trigger_param_2, ":hit"),
				# (eq, ":hit", 1),
			
				# (copy_position, pos10, pos1),								# Store the hit position
				# (agent_get_position, pos4, ":agent"),						# Store where the shooter is
				# (position_set_z_to_ground_level, pos10),
				# (position_set_z_to_ground_level, pos4),						# Make sure their feet are on the ground
				# (assign, ":chosen_angle", 30),
				
				# #(position_move_z, pos4, 100, 1), # around chest height

				# #(position_get_z, ":agent_z", pos4),							#
				# #(position_get_z, ":target_z", pos10),						#
				
				# #(store_sub, ":z_length", ":agent_z", ":target_z"),			# Difference in height between shooter and target
						
			    # (get_distance_between_positions, ":distance", pos10, pos4),		# How Far am I from there?
				
				# # (store_mul, ":2theta", ":chosen_angle", 2),
				# # (convert_to_fixed_point, ":2theta"),
				# # (store_tan, ":tangent", ":2theta"),
				
				# # (val_add, ":z_length", ":tangent"), #Testing my idea of how the compensation works
				
				# #(val_add, ":distance", ":z_length"),			# This compensates for differences in terrain
				
				# # ### The Next Batch is what works. ########
				
				# # (store_mul, ":speed_prime", ":distance", 981),				# Distance = (2speed^2 / gravity)
				# # ####(val_div, ":speed_prime", 2),
				# # (store_sqrt, ":speed", ":speed_prime"),					# so Speed = sqrt((distance*gravity)/2)
				
				# # ####### Based on #########################
				# # ### d = (v^2 sin(2 a))/g #################
				# # ##########################################
				
				# # ### Before this is what works ############
				
				# # ##########################################
				# # ## For Modular Speed Function ############
				# # # v = sqrt(((d)*(g))/(sin(2 a))) ###
				# # ### d = distance, g = gravity, a = angle #
				# # ##########################################
				
				# # ##########################################
				# # ## For Modular Angle Function ############
				# # a = (sin^-1((d*g)/v^2))/2
				# # ### d = distance, g = gravity, a = angle #
				# # ##########################################
			 
				
				 # (store_mul, ":numerator", ":distance", 981),
				 # (assign, reg15, ":numerator"),
				 # (store_mul, ":2theta", ":chosen_angle", 2),
				 # # (convert_to_fixed_point, ":2theta"),
				 # (store_sin, ":denominator", ":2theta"),
				 # (assign, reg16, ":denominator"),
				
				 # (store_div, ":speed_prime", ":numerator", ":denominator"),
				 # (assign, reg17, ":speed_prime"),
				 
				
				 # (store_sqrt, ":speed", ":speed_prime"),
				 # # (assign, reg18, ":speed"),
				 
				# # (display_log_message, "@{reg15}/{reg16} = {reg17}"),
				# # (display_log_message, "@sqrt{reg17} = {reg18}"),
				
				# # (assign, ":speed", 5250),
				
				# # (store_mul, ":numerator", ":distance", 981),
				# # (store_mul, ":denominator", ":speed", ":speed"),
				# # (store_div, ":prime", ":numerator", ":denominator"),
				# # (store_asin, ":intim", ":prime"),
				# # # (convert_from_fixed_point, ":intim"),
				# # (val_div, ":intim", 2),
				# # (assign, ":chosen_angle", ":intim"),
				
				# # (assign, reg17, ":intim"),
				# # (assign, reg10, ":speed"),
				# # (assign, reg11, ":distance"),
				# # (assign, reg12, 981),
				# # (display_log_message, "@asin({reg11}*981/sq{reg10})/2"),
				# # (display_log_message, "@Angle of release {reg17}"),
				
				
				
				# (position_rotate_y, pos10, 180), # Reflect the Projectile
				# (position_get_rotation_around_x, reg7, pos10), # Get new X after Y rotation
				# (val_mul, reg7, -1), 							# Get the inverse of X to cancel
				# (position_rotate_x, pos10, reg7),				# Cancel previous X
				# #(position_rotate_x, pos10, 45), 				# The ~*~Perfect~*~ Theta
				# (position_rotate_x, pos10, ":chosen_angle"),
				
				# (val_div, ":speed", 9),
								

				# #(position_move_z, pos10, 100, 1), # So it doesn't spawn inside the ground
				# (val_sub, ":agent", 1), # maybe use so that the agent "catches" the weapon
				# (add_missile, ":agent", pos10, ":speed", "itm_gae_bolg_return", 0, "itm_gae_bolg_return", 0),
				(item_get_slot, ":active", "itm_gae_bolg_thrown", fate_weapon_activated),
				
				(try_begin),
					(gt, ":active", 0),
					
					(try_for_agents, ":agent_no", pos1, 500),
					(neq, ":agent", ":agent_no"),
					
					(agent_deliver_damage_to_agent, ":agent", ":agent_no", 2500),	
					(try_end),
					
				(else_try),
					
				(try_end),
				
				]),
			],
			
			],
			
		# Gae Bolg - return,
	["gae_bolg_return", "Gae Bolg (Return)", [("gae_bolg",0)], itp_no_blur|itp_type_thrown|itp_unique|itp_ignore_friction|itp_remove_item_on_use|itp_crush_through|itp_no_pick_up_from_ground|itp_primary, itcf_throw_axe, 525 , weight(3)|difficulty(2)|spd_rtng(87)|shoot_speed(100)|thrust_damage(0,  pierce)|max_ammo(1)|weapon_length(50),imodbits_thrown,
			[(ti_on_missile_hit, [
				(set_fixed_point_multiplier, 100), 							# Will keep math consistant
		
				
				(try_for_agents, ":agent_no", 200),
				  (agent_is_alive, ":agent_no"),
				  (agent_is_human, ":agent_no"),
				  
				  (agent_is_alive, ":agent_no"),
				  (agent_get_position, pos4, ":agent_no"),
			      (get_distance_between_positions_in_meters,":distance", pos1, pos4), 
				  
				  (le, ":distance", 1),
				  (agent_equip_item, ":agent_no", "itm_gae_bolg_thrown"),
				  (agent_set_wielded_item, ":agent_no", "itm_gae_bolg_thrown"),
				  #(display_log_message, "@something in range."),
				(try_end),
				]),
			],
			
			],

		# blue spandex suit
	["cu_suit", "Cu Chulainn", [("mail_and_plate",0)], itp_unique|itp_type_body_armor  |itp_covers_legs, 0, 260, weight(6)|abundance(0)|head_armor(0)|body_armor(72)|leg_armor(20)|difficulty(0) ,imodbits_cloth],
	
		# Plate Toed Boots
	["cu_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
	
		# Steel Gloves
	["cu_gauntlets","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],
		
		# Runes - Throwing Weapons with random effects
	["cu_runes", "Runes", [("throwing_stone",0)], itp_no_blur|itp_type_thrown|itp_unique|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30) | thrust_damage(11, blunt)|max_ammo(18)|weapon_length(8),imodbit_large_bag ],
		
	#Diarmuid Ua Duibhne
		#gae dearg
	["gae_dearg", "Gae Dearg", [("gae_dearg",0)], itp_no_blur|itp_type_polearm|itp_unique|itp_primary|itp_can_penetrate_shield|itp_wooden_parry|itp_extra_penetration, itc_guandao|itc_dagger, 76, weight(2.5)|difficulty(0)|spd_rtng(85)|weapon_length(190)|swing_damage(42, cut)|thrust_damage(37, pierce),imodbits_polearm],
	
	["gae_dearg_offhand", "Gae Dearg", [("gae_dearg_offhand",0)], itp_no_blur|itp_unique|itp_type_shield, 0, 697, weight(2.5)|hit_points(700)|body_armor(17)|spd_rtng(61)|shield_width(10),imodbits_shield ],
	
		#gae buidhe
	["gae_buidhe", "Gae Buidhe", [("gae_buidhe",0)], itp_no_blur|itp_type_polearm|itp_unique|itp_primary|itp_wooden_parry, itc_dagger, 76, weight(2.5)|difficulty(0)|spd_rtng(95)|weapon_length(135)|swing_damage(35, cut)|thrust_damage(45, pierce),imodbits_polearm],
	
	["gae_buidhe_offhand", "Gae Buidhe", [("gae_buidhe_offhand",0)], itp_no_blur|itp_unique|itp_type_shield, 0, 697, weight(2.5)|hit_points(700)|body_armor(17)|spd_rtng(61)|shield_width(10),imodbits_shield ],
	
		#green spandex suit
	["diarmund_suit", "Diarmund Clothes", [("light_mail_and_plate",0)], itp_unique| itp_type_body_armor  |itp_covers_legs, 0, 260, weight(6)|abundance(0)|head_armor(0)|body_armor(72)|leg_armor(20)|difficulty(0) ,imodbits_cloth],
	
		#shoes
	["diarmund_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
	
		#long leather gloves
	["diarmund_gloves","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],
	
	#Fionn mac Cumhaill
		#Mac an Luin
	["mac_an_luin", "Mac an Luin", [("fionn",0)], itp_no_blur|itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear|itcf_carry_spear, 345, weight(2.25)|difficulty(0)|spd_rtng(92) | weapon_length(204)|swing_damage(20, blunt) | thrust_damage(45,  pierce),imodbits_polearm ],	
		
		#armor
	["fionn_armor", "Plate Armor", [("full_plate_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#gloves
	["fionn_gloves","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#boots
	["fionn_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],
		
	#Hector
		# Durindana - Spear, He liked to throw is so thrown variation is coded just in case
	["durindana_thrown", "Durindana Pilum", [("durindana",0),], itp_no_blur|itp_type_thrown |itp_unique|itp_primary|itp_remove_item_on_use|itp_extra_penetration|itp_next_item_as_melee, itcf_throw_javelin, 525, weight(3.5)|difficulty(0)|spd_rtng(87)|shoot_speed(120)|thrust_damage(95, pierce)|max_ammo(1)|weapon_length(270),imodbits_thrown],
	
		# Durindana - Spear
	["durindana", "Durindana", [("durindana",0)], itp_no_blur|itp_type_polearm|itp_unique|itp_primary|itp_can_penetrate_shield|itp_wooden_parry, itc_spear, 76, weight(3.5)|difficulty(0)|spd_rtng(85)|weapon_length(270)|thrust_damage(45, pierce),imodbits_polearm],
	
		# Durindana - sword
	["durindana_spada", "Durindana Spada", [("durindana_spada",0)], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_can_penetrate_shield, itc_longsword, 76, weight(3.5)|difficulty(0)|spd_rtng(150)|weapon_length(148)|swing_damage(45, cut)|thrust_damage(35, pierce), imodbits_polearm],
	
		#iron arm
	["hector_arm","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor|itp_force_show_left_hand, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#combat gear
	["hector_gear", "Plate Armor", [("full_plate_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#knee high boots
	["hector_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
	#Brynhildr
		# Brynhildr Romantia
	["brynhildr", "Brynhildr Romantia", [("spear_d_2-8m",0)], itp_no_blur|itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear|itcf_carry_spear, 345, weight(2.25)|difficulty(0)|spd_rtng(92)| weapon_length(165)|swing_damage(20, blunt) | thrust_damage(57, pierce),imodbits_polearm],	
		
		# Dress
	["bryn_clothes", "Plate Armor", [("full_plate_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#incredibly long hair
	["bryn_head", "Brynhildr Visage", [("full_plate_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],

		#boots
	["bryn_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],
		
 #Archer Gear
	#EMIYA
		#Bow
	["emiya_bow", "Archer's Bow", [("new_emiya_bow",0)], itp_no_blur|itp_type_bow|itp_unique|itp_primary|itp_two_handed, itcf_shoot_bow, 145, weight(1.75)|difficulty(2)|spd_rtng(99)|shoot_speed(76) | thrust_damage(40, pierce), imodbits_bow, 
	[(ti_on_weapon_attack, [
			(set_fixed_point_multiplier, 100), 							# Will keep math consistent
			# (store_trigger_param_1, ":agent"), 							# Store the shooting Agent
			(store_trigger_param_1, ":agent"),
			(gt, ":agent", 0),
		
			(agent_get_ammo, ":ammo", ":agent", 1),
			
			(lt, ":ammo", 10),
			
			#Clear his various available arrows and reequip the basic arrows
			(agent_unequip_item, ":agent", "itm_emiya_arrows"),
			(agent_unequip_item, ":agent", "itm_caladbolg_ii"),
			(agent_unequip_item, ":agent", "itm_hrunting"),
			
			
			
			(agent_equip_item, ":agent", "itm_emiya_arrows"),
			(agent_set_wielded_item, ":agent", "itm_emiya_arrows"),		
		]),	
	]],
	
	["emiya_arrows", "Projected Arrows", [("piercing_arrow", 0), ("flying_missile", ixmesh_flying_ammo)], itp_no_blur|itp_type_arrows|itp_no_pick_up_from_ground|itp_unique, 0, 350,weight(3)|abundance(0)|weapon_length(91)|thrust_damage(2, pierce)|max_ammo(20), imodbits_missile, [bullet_impact]],
	
		#Shroud of Martin - Red Jacket
	["shroud_of_martin", "Red Overcoat", [("emiya_coat",0)], itp_unique|itp_type_body_armor|itp_covers_legs, 0, 260, weight(6)|abundance(0)|head_armor(0)|body_armor(72)|leg_armor(20)|difficulty(0) ,imodbits_cloth],
		
		#Boots
	["emiya_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],

	["emiya_gloves", "emiya gloves", [("gauntlets_L",0),], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],
		
		#Kanshou - White Falchion (Thrown)
	["kanshou", "Kanshou", [("kanshou",0)], itp_no_blur|itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground|itp_next_item_as_melee, itcf_throw_axe,
	360, weight(2.5)|difficulty(0)|spd_rtng(99) | shoot_speed(35) | thrust_damage(45,cut)|max_ammo(56)|weapon_length(40),imodbits_thrown_minus_heavy, 
	 [(ti_on_weapon_attack, [
			(call_script, "script_cf_fate_double_throw", "itm_bakuya", "itm_bakuya_thrown"),
            ]),]],
			
		#Kanshou - White Falchion
	["kanshou_melee", "Kanshou", [("kanshou",0)], itp_no_blur|itp_type_one_handed_wpn|itp_no_pick_up_from_ground|itp_unique|itp_primary, itc_scimitar, 105, weight(2.5)|difficulty(8)|spd_rtng(98) | weapon_length(65)|swing_damage(45, cut) | thrust_damage(0, pierce), imodbits_sword ],
		
		#Bakuya - Black Falchion (Shield)
	["bakuya", "Bakuya", [("bakuya_offhand",0), ("bakuya", ixmesh_carry)], itp_unique|itp_type_shield|itp_no_pick_up_from_ground, 0,  697 , weight(2.5)|hit_points(700)|body_armor(17)|spd_rtng(61)|shield_width(10), imodbits_shield ],
	
		#Bakuya - Black Falchion (Thrown)
	["bakuya_thrown", "Bakuya", [("bakuya",0)], itp_no_blur|itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground, itcf_throw_axe,
	360, weight(2.5)|difficulty(0)|spd_rtng(99) | shoot_speed(35) | thrust_damage(45,cut)|max_ammo(100)|weapon_length(40),imodbits_thrown_minus_heavy,],
	
		#Kanshou - White Falchion
	["kanshou_oe", "Kanshou Overedge", [("kanshou",0)], itp_no_blur|itp_type_one_handed_wpn|itp_no_pick_up_from_ground|itp_unique|itp_primary, itc_scimitar, 105, weight(4.5)|difficulty(8)|spd_rtng(88) | weapon_length(85)|swing_damage(55, cut) | thrust_damage(40, pierce), imodbits_sword ],
		
		#Bakuya - Black Falchion (Shield)
	["bakuya_oe", "Bakuya Overedge", [("bakuya",0), ("bakuya", ixmesh_carry)], itp_unique|itp_type_shield|itp_no_pick_up_from_ground, 0,  697 , weight(2.5)|hit_points(1200)|body_armor(50)|spd_rtng(50)|shield_width(20), imodbits_shield],
		
		#unlimited blade works generic swords for ammo
		
		#Broken Phantasm - Caladbolg II
	
	["caladbolg_ii","Caladbolg II", [("caladbolg_ii",0),("caladbolg_ii",ixmesh_flying_ammo)], itp_no_blur|itp_type_arrows|itp_no_pick_up_from_ground|itp_unique, 0, 410, weight(3.5)|abundance(0)|weapon_length(95)|thrust_damage(35,pierce)|max_ammo(1), imodbits_missile, 
	[
	
	(ti_on_missile_hit, [
			 # (particle_system_burst, "psys_gourd_smoke", pos1, 100),
			 # (particle_system_burst, "psys_fireplace_fire_big", pos1, 100),
			 
			(store_trigger_param_1, ":agent"),
			
			(call_script, "script_cf_fate_explosion", pos1, ":agent", 1250, 4500, 7000),

            ]),			
	]],
			
		#Broken Phantasm - Hrunting	
	["hrunting", "Hrunting", [("hrunting",0), ("hrunting",ixmesh_flying_ammo)], itp_type_arrows|itp_no_pick_up_from_ground|itp_unique, 0, 410,weight(3.5)|abundance(0)|weapon_length(95)|thrust_damage(35,pierce)|max_ammo(1), imodbits_missile, 
	[
	
	(ti_on_missile_hit, [
			 # (particle_system_burst, "psys_gourd_smoke", pos1, 100),
			 # (particle_system_burst, "psys_fireplace_fire_big", pos1, 100),
			 
			 (store_trigger_param_1, ":agent"),
			
			 (call_script, "script_cf_fate_explosion", pos1, ":agent", 1000, 3000, 5000),
            ]),			
	]],
			
		#Rho Aias
		
		
	#Gilgamesh
		#Ea
	["gilgamesh_ea", "EA - Sword of Rapture", [("ea",0)], itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_wooden_parry|itp_unbalanced, itc_longsword, 305, weight(4.5)|difficulty(13)|spd_rtng(95) | weapon_length(85)|swing_damage(75, pierce)|thrust_damage(55, pierce),imodbits_mace ],
		
		#Gate of Babylon -empty hands that trigger crossed arm animation and summon near infinite weapons as long as he has mana
	["gate_baby", "gate open", [("ea",0)], itp_type_one_handed_wpn|itp_primary, itc_greatlance, 0 , weight(1.25)|difficulty(0)|spd_rtng(103) | weapon_length(1)|swing_damage(20, cut) | thrust_damage(13,  pierce),imodbits_sword_high, 
	 [(ti_on_weapon_attack, [
			(set_fixed_point_multiplier, 100),
			(store_trigger_param_1, ":agent"),
			(agent_set_animation, ":agent", "anim_pose_3"),
			(agent_get_look_position, pos1, ":agent"),
			
			(try_for_range, ":spawn", 0, 5),
				#(store_random_in_range, ":rand_y", 200, 2300),
				(store_random_in_range, ":rand_x", -200, 200),
				(store_random_in_range, ":rand_z", 200, 1500),
				(store_random_in_range, ":wobble", 10, -10),
				#(position_move_y, pos1, ":rand_y", 0),
				(position_move_x, pos1, ":rand_x", 0),
				(position_move_z, pos1, ":rand_z", 0),
				(position_move_y, pos1, -150, 0),
				(store_div, ":adjust_rotation", ":rand_z", -50),
				(position_rotate_x, pos1, ":adjust_rotation"),
				(position_rotate_y, pos1, ":wobble"),
				(add_missile, ":agent", pos1, 6000, "itm_babylon_sword", 0, "itm_babylon_sword", 0),
			(try_end),
            ]),]],	
		
		#Gate Stored Weapons for ammo
	["babylon_sword","Golden Treasure, Sword", [("gil_sword_shoot",0),("gil_sword_shoot",ixmesh_flying_ammo)], itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground,itcf_throw_javelin|itcf_carry_sword_left_hip,
	360, weight(2.5)|difficulty(0)|spd_rtng(99) | shoot_speed(600) | thrust_damage(45,pierce)|max_ammo(1)|weapon_length(65),imodbits_thrown_minus_heavy, 
	[
	
	(ti_on_missile_hit, [
			
			 (store_trigger_param_1, ":agent"),
			
			 (call_script, "script_cf_fate_explosion", pos1, ":agent", 1500, 2500, 3000),
            ]),			
	]],
		
		#Golden Armor
	["gil_armor", "Plate Armor", [("lamellar_armor_c",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Golden Boots
	["gil_boots", "Plate Boots", [("lamellar_boots_a",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
	#Atalanta
	
		#Tauropolos - Atalanta Bow
	["tauropolos", "Tauropolos", [("long_bow",0),("long_bow_carry",ixmesh_carry)], itp_no_blur|itp_type_bow |itp_unique|itp_primary|itp_extra_penetration|itp_two_handed ,itcf_shoot_bow|itcf_carry_bow_back, 145 , weight(1.75)|difficulty(2)|spd_rtng(99) | shoot_speed(50) | thrust_damage(40, pierce),imodbits_bow,
		[(ti_on_weapon_attack, [
			(store_trigger_param_1, ":agent"),
			
			(agent_unequip_item, ":agent", "itm_calamity_arrows"),
			(agent_equip_item, ":agent", "itm_atalanta_arrows"),
			(agent_set_wielded_item, ":agent", "itm_atalanta_arrows"),
			]),]],
	
	
		#Atalanta's Arrows
	
	["atalanta_arrows","Grecean Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_no_blur|itp_type_arrows|itp_unique, itcf_carry_quiver_back, 72, weight(3)|abundance(160)|weapon_length(95)|thrust_damage(15,pierce)|max_ammo(30),imodbits_missile],
	
	["calamity_arrows","Calamity Arrows", [("arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver", ixmesh_carry)], itp_no_blur|itp_type_arrows|itp_unique, itcf_carry_quiver_back, 72, weight(3)|abundance(160)|weapon_length(95)|thrust_damage(15,pierce)|max_ammo(1),imodbits_missile, 
			[(ti_on_missile_hit, [
							
				(set_fixed_point_multiplier, 100), 							# Will keep math consistant
				(store_trigger_param_1, ":agent"), 							# Store the shooting Agent
				
				(call_script, "script_target_missiles", ":agent", pos1, 300, 1000),
				
				# (agent_get_look_position, pos9, ":agent"), 					# Get Agent's look for math
				# (init_position, pos10), 									# Initial the Position that will be used for math 
				
				# (position_copy_origin, pos10, pos1),						# duplicate the info from the trigger Pos to math Pos
				
				# (position_get_rotation_around_z, ":look_z", pos9),			# Rip the Z-axis from the look pos
				# (val_add, ":look_z", 180),									# Rotate 180 to insure the missiles are coming towards the player
				# (position_rotate_z, pos10, ":look_z", 1),					# apply the new Z-axis to math pos
				
				# (position_move_z, pos10, 6000, 1),							# Move the missiles very high, they're from space after all
				# #(position_move_y, pos10, -5000, 1),
				# (position_move_y, pos10, -5000, 0), 						# I decided this distance through trial-and-error for perfect arcs when the next line
				# (position_rotate_x, pos10, -45, 1),							# This insures arrows fall towards the Earth, sort of a sharp angle. But if modified move_y needs to change too.
				
				# (copy_position, pos9, pos10),								# I use this so that the randomization doesn't mess with the original math
				# # (position_move_z, pos10, 200, 1),
				# # (position_get_rotation_around_y, ":orig_y", pos1),
				# # (position_get_rotation_around_z, ":orig_z", pos1),
				
				# # Get agent look position, take the y rotation ######
				# # add 180, reset all positional data, add the new ###
				# # rotational data, the original -45 and use that for#
				# # the rest of the positional math. this should insure
				# # that the arrows are spawned in coming TOWARDS you
				# # every time and not at some glbal rotation
				
				
				# (try_for_range, ":spawn", 0, 301),							# number of arrows
					 # (copy_position, pos10, pos9),							# reset the pos every new arrow so randomized pos below doesn't mess with the center
					 # (store_random_in_range, ":rand_y", -500, 500), 		# the rest creates a 10m sq that arrows fall between
					 # (store_random_in_range, ":rand_x", -500, 500),
					 # (store_random_in_range, ":rand_z", -500, 500),
					
					 # (position_move_y, pos10, ":rand_y", 1),
					 # (position_move_x, pos10, ":rand_x", 1),
					 # (position_move_z, pos10, ":rand_z", 1),										
																			# # finally add the arrows.
					# (add_missile, ":agent", pos10, 5000, "itm_tauropolos", 0, "itm_astral_arrows", 0), 
				# (try_end),
				]),
			],
	
	],
		
	
		# Astral Arrows for Calamity
	 ["astral_arrows","Barbed Arrows", [("none",0),("mystic_missile",ixmesh_flying_ammo),("quiver_d", ixmesh_carry)], itp_no_blur|itp_type_arrows|itp_unique|itp_no_pick_up_from_ground, itcf_carry_quiver_back_right, 124,weight(3)|abundance(70)|weapon_length(95)|thrust_damage(5,pierce)|max_ammo(30),imodbits_missile #,
		# [(ti_on_missile_hit, [
			 # (particle_system_burst, "psys_gourd_smoke", pos1, 1),
			 # (particle_system_burst, "psys_fireplace_fire_big", pos1, 10),
			 # (call_script, "script_cf_fate_explosion", 100, 2000, 5000),
            # ]),
		# ],
	],
		
		#dress
	["atalanta_dress", "Plate Armor", [("green_dress",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#cat ears
	["atalanta_head", "Cat Ears", [("breaker",0)], itp_type_head_armor|itp_unique, 0, 193, weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0), imodbits_cloth],
		
		#long boots
	["atalanta_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
	
		#berserk transformation - body
	["atalanta_boar_body", "Agius Body", [("tribal_warrior_outfit_a",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#berserk transformation - cat ears
	["atalanta_boar_head", "Agius Ears", [("helmet_fur_a",0)], itp_type_head_armor|itp_unique, 0, 193, weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0), imodbits_cloth],
		
		#berserk transformation - legs
	["atalanta_boar_legs", "Agius Legs", [("hide_boots_a",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
	
		# berserk transformation - right claw (build left claw into body mesh)
	["atalanta_boar_claw", "Agius Claw", [("peasant_knife_new",0)], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_left, 18, weight(0.5)|difficulty(0)|spd_rtng(110)|weapon_length(40)|swing_damage(51, cut)|thrust_damage(25, pierce),imodbits_sword],
		
	#Robin Hood
	
		#Yew Bow
	["yew_bow","Yew Bow", [("crossbow_a", 0)], itp_type_crossbow|itp_unique|itp_primary, itp_no_blur|itcf_reload_pistol|itcf_carry_pistol_front_left|itcf_shoot_pistol, 525, weight(1)|abundance(100)|spd_rtng(85)|shoot_speed(90)|max_ammo(1)|thrust_damage(55, pierce), imodbits_crossbow, [
	(ti_on_init_item, [
		(store_trigger_param_1, ":agent"),
		(neq, ":agent", -1),
		
		(try_begin),
			(agent_slot_eq, ":agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_04_01"),
			(agent_slot_ge, ":agent", slot_agent_active_np_timer, 1),
			
			(cur_item_set_material, "str_material_camo", -1),
		(else_try),
			(agent_slot_ge, ":agent", fate_agent_petrified, 1),
			
			(cur_item_set_material, "str_material_petrified", 0),
		(try_end),
		]),
		
	(ti_on_weapon_attack, [
		(store_trigger_param_1, ":agent"),
		
		(agent_get_ammo, ":ammo", ":agent", 1),
		
		(lt, ":ammo", 10),
		
		(agent_unequip_item, ":agent", "itm_yew_bolts"),
		(agent_equip_item, ":agent", "itm_yew_bolts_actual"),
		(agent_set_wielded_item, ":agent", "itm_yew_bolts_actual"),
	])]],
	 
		#Yew Tree Arrow
	["yew_bolts","Yew Tree Summoner", [("piercing_arrow",0),("flying_missile",ixmesh_flying_ammo),("none", ixmesh_carry)], itp_type_bolts|itp_no_pick_up_from_ground|itp_unique, itcf_carry_quiver_back_right, 350,weight(3)|abundance(50)|weapon_length(91)|thrust_damage(5, pierce)|max_ammo(1),imodbits_missile, 
	[	
	(ti_on_init_item, [
		(store_trigger_param_1, ":agent"),
		(neq, ":agent", -1),
        (agent_slot_eq, ":agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_04_01"),
		(agent_slot_ge, ":agent", slot_agent_active_np_timer, 1),
		
		(cur_item_set_material, "str_material_camo", -1),
		]),
	
	(ti_on_missile_hit, [
			(set_fixed_point_multiplier, 100),
			
			(store_trigger_param_1, ":agent"),
			(particle_system_burst, "psys_gourd_smoke", pos1, 100),
			(particle_system_burst, "psys_fireplace_fire_big", pos1, 100),
			 
			 
			(copy_position, pos2, pos1), # Make a duplicate of landing location
			(position_move_z, pos2, 50, 1), # Move it up
			(call_script, "script_lookat", pos2, pos1), # Look at that position
			
			(position_rotate_x, pos2, -90), # There seems to be an issue inside the ti_on_missile_hit where the axis are mislabelled somehow, requiring us to do this.
			
			(cast_ray, reg1, pos3, pos2), # Cast a ray, this should make a duplicate of pos1, but with the normals of the surface stored in the rotational values
			
			# (position_copy_rotation, pos1, pos3),
			(position_rotate_x, pos3, 90),
			
			(set_spawn_position, pos3),
			 
			(spawn_scene_prop, "spr_fate_yew_tree"),
			 
			(scene_prop_set_slot, reg0, fate_prop_owner, ":agent"),
            ]),]],
			
	["yew_bolts_actual", "Yew Bolts", [("piercing_arrow", 0), ("flying_missile", ixmesh_flying_ammo), ("none", ixmesh_carry)], itp_no_blur|itp_type_bolts|itp_no_pick_up_from_ground|itp_unique, itcf_carry_quiver_back_right, 350, weight(3)|abundance(50)|weapon_length(91)|thrust_damage(15, pierce)|max_ammo(50), imodbits_missile, [
	(ti_on_init_item, [
		(store_trigger_param_1, ":agent"),
		(neq, ":agent", -1),
        (agent_slot_eq, ":agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_04_01"),
		(agent_slot_ge, ":agent", slot_agent_active_np_timer, 1),
		
		(cur_item_set_material, "str_material_camo", -1),
		])
	]],
			
		#Dagger
	["robin_knife", "Knife", [("peasant_knife_new",0)], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_left, 18, weight(0.5)|difficulty(0)|spd_rtng(110)|weapon_length(40)|swing_damage(51, cut)|thrust_damage(25, pierce),imodbits_sword, [
	(ti_on_init_item, [
		(store_trigger_param_1, ":agent"),
		(neq, ":agent", -1),
        (agent_slot_eq, ":agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_04_01"),
		(agent_slot_ge, ":agent", slot_agent_active_np_timer, 1),
		
		(cur_item_set_material, "str_material_camo", -1),
		])
	]],
		
		#Green Hood
	["robin_hood", "Hood", [("hood_new",0)],itp_unique|itp_type_head_armor|itp_civilian,0,9, weight(1)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth, []],
	
	["robin_hood_c", "Hood", [("hood_new",0)],itp_unique|itp_type_head_armor|itp_civilian|itp_covers_head, 0, 9, weight(1)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth, [
		(ti_on_init_item, [
		(store_trigger_param_1, ":agent"),
		(neq, ":agent", -1),
        (agent_slot_eq, ":agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_04_01"),
		(agent_slot_ge, ":agent", slot_agent_active_np_timer, 1),
		
		(cur_item_set_material, "str_material_camo", -1),
		])
	]],	
		
		#Archer Gear
	["robin_clothes", "Plate Armor", [("peasant_man_a",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate, [
		(ti_on_init_item, [
		(store_trigger_param_1, ":agent"),
		(neq, ":agent", -1),
        (agent_slot_eq, ":agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_04_01"),
		(agent_slot_ge, ":agent", slot_agent_active_np_timer, 1),
		
		(cur_item_set_material, "str_material_camo", -1),
		])
	]],	
		
	["robin_gloves", "emiya gloves", [("gauntlets_L",0),], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor, []],
	["robin_gloves_c", "emiya gloves", [("gauntlets_camo_L",0),], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor, []],
	
		#Brown Leather Boots
	["robin_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature|itp_covers_legs, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0), imodbits_plate, []],
	
	["robin_boots_c", "Plate Boots", [("plate_boots_camo",0)], itp_unique|itp_type_foot_armor|itp_attach_armature|itp_covers_legs, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0), imodbits_plate, []],
	
	#Arash
		#Bow and Arrow Creation - Crimson Bow
	["arash_bow", "Arash's Bow", [("ArashBow",0)], itp_no_blur|itp_type_bow|itp_unique|itp_primary|itp_two_handed ,itcf_shoot_bow, 145 , weight(1.75)|difficulty(2)|spd_rtng(99) | shoot_speed(76) | thrust_damage(40, pierce),imodbits_bow,
		[(ti_on_weapon_attack, [
			(set_fixed_point_multiplier, 100), 							# Will keep math consistant
			# (store_trigger_param_1, ":agent"), 							# Store the shooting Agent
			(store_trigger_param_1, ":agent"),
		
			(agent_get_ammo, ":ammo", ":agent", 1),
			
			(lt, ":ammo", 10),
			
			(agent_unequip_item, ":agent", "itm_stella_arrow"),
			(agent_equip_item, ":agent", "itm_created_arrows"),
			(agent_set_wielded_item, ":agent", "itm_created_arrows"),		
		]),]
	],
	
		#Bow and Arrow Creation - Crimson Arrows
	["created_arrows", "Created Arrows", [("arash_arrow",0),("flying_missile",ixmesh_flying_ammo),("quiver_c", ixmesh_carry)], itp_no_blur|itp_type_arrows|itp_no_pick_up_from_ground|itp_unique, 0, 350,weight(3)|abundance(0)|weapon_length(91)|thrust_damage(2,pierce)|max_ammo(255),imodbits_missile,],
	
		#Stella, Summons meteor! HUGE AOE stone_ball
	["stella_arrow", "Stella", [("arash_arrow",0),("flying_missile",ixmesh_flying_ammo)], itp_no_blur|itp_type_arrows|itp_no_pick_up_from_ground|itp_unique, 0, 350,weight(3)|abundance(0)|weapon_length(91)|thrust_damage(2,pierce)|max_ammo(1),imodbits_missile,
		[(ti_on_missile_hit, [
				(set_fixed_point_multiplier, 100), 							# Will keep math consistant
				(store_trigger_param_1, ":agent"), 							# Store the shooting Agent
				(agent_get_look_position, pos9, ":agent"), 					# Get Agent's look for math
				(init_position, pos10), 									# Initial the Position that will be used for math 
				
				(position_copy_origin, pos10, pos1),						# duplicate the info from the trigger Pos to math Pos
				
				(position_get_rotation_around_z, ":look_z", pos9),			# Rip the Z-axis from the look pos
				#(val_add, ":look_z", 90),									# Rotate 180 to insure the missiles are coming towards the player
				(position_rotate_z, pos10, ":look_z", 1),					# apply the new Z-axis to math pos
				
				(position_move_z, pos10, 6000, 1),							# Move the missiles very high, they're from space after all
				(position_move_y, pos10, -5000, 0), 						# I decided this distance through trial-and-error for perfect arcs when the next line
				(position_rotate_x, pos10, -45, 1),							# This insures arrows fall towards the Earth, sort of a sharp angle. But if modified move_y needs to change too.
				
				(add_missile, ":agent", pos10, 9000, "itm_stella_meteor", 0, "itm_stella_meteor", 0), 
					 
				]),]
	],
		# Stella - The Lone Meteor
	["stella_meteor", "Stella - Lone Meteor", [("rock_a",0)], itp_no_blur|itp_type_thrown|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(30) | thrust_damage(11,  blunt)|max_ammo(18)|weapon_length(8), imodbit_large_bag,
		[(ti_on_missile_hit, [
		
			(store_trigger_param_1, ":agent"),
			
			(call_script, "script_cf_fate_explosion", pos1, ":agent", 7000, 7500, 10000),
		]),]
	],
	
		#Blue Archer Gear
	["arash_plate", "Plate Armor", [("full_plate_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Red Boots
	["arash_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
		#Yellow Hood
	["arash_hood", "Hood", [("hood_new",0)], itp_unique|itp_type_head_armor|itp_civilian,0,9, weight(1)|abundance(100)|head_armor(30)|body_armor(0)|leg_armor(0)|difficulty(0),imodbits_cloth],	
		
		#Red MMA Style Gloves
	["arash_gloves","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0), imodbits_armor],	
		
 #Rider Gear
	#Medusa
		#Fucking Pegasus
	["bellerophon","Bellerophon", [("courser",0)], itp_unique|itp_type_horse, 0, 600,abundance(0)|body_armor(50)|hit_points(110)|difficulty(2)|horse_speed(65)|horse_maneuver(60)|horse_charge(25)|horse_scale(112),imodbits_horse_basic|imodbit_champion],
		
		#Nameless Dagger, maybe find a way to do a chain mechanic? Allow throw and return?
	["medusa_dagger", "Medusa Dagger", [("medusa_dagger",0),], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary, itc_longsword, 37, weight(0.75)|difficulty(0)|spd_rtng(125) | weapon_length(47)|swing_damage(22, cut)|thrust_damage(49, pierce), imodbits_sword_high ],
		
		#Eye Cover
	["medusa_mask", "Breaker Gorgon", [("breaker",0)], itp_type_head_armor|itp_doesnt_cover_hair|itp_unique, 0, 193, weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0), imodbits_cloth],
		
		#Dress
	["medusa_dress", "Black Minidress", [("full_plate_armor",0)], itp_unique|itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Long Boots
	["medusa_boots", "Plated Heeled Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
	#Iskandar
		#Divine Bull
	["divine_bull","Divine Bull", [("courser",0)], itp_unique|itp_type_horse, 0, 600,abundance(0)|body_armor(50)|hit_points(110)|difficulty(2)|horse_speed(65)|horse_maneuver(60)|horse_charge(75)|horse_scale(175),imodbits_horse_basic|imodbit_champion],	
		
		#Gordius Wheel
		
		
		#Bucephalus - Big ass Horse
	["bucephalus","Bucephalus", [("charger_plate_1",0)], itp_unique|itp_type_horse, 0, 600,abundance(0)|body_armor(75)|hit_points(250)|difficulty(2)|horse_speed(65)|horse_maneuver(60)|horse_charge(50)|horse_scale(145),imodbits_horse_basic|imodbit_champion],
		
		#Mantle
		
		
		#Greek Styled Armor
	["iskandar_plate", "Plate Armor", [("sarranid_elite_cavalary",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Sandles with Shinguards
	["iskandar_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
		#Sword of the Kupriotes
	["kupriotes", "Sword of the Kupriotes", [("b_bastard_sword",0),("scab_bastardsw_b", ixmesh_carry)], itp_no_blur|itp_type_one_handed_wpn|itp_unique| itp_two_handed|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 524, weight(3)|difficulty(11)|spd_rtng(94) | weapon_length(130)|swing_damage(40, cut) | thrust_damage(31, pierce), imodbits_sword_high],
 
		# Jeans and Admirable Tactics T-Shirt
	["admirable_tactics_shirt", "Fucking Nerd's Clothing", [("leather_vest_a",0)], itp_unique|itp_type_body_armor|itp_covers_legs|itp_civilian ,0, 3 , weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
		
	#Achilles
		#Diatrekhon Aster Lonkhe - Hero Killing Spear
	["diatrekhon", "Diatrekhon Aster Lonkhe", [("awl_pike_b",0)], itp_no_blur|itp_type_polearm|itp_offset_lance|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_cutting_spear, 345, weight(2.25)|difficulty(0)|spd_rtng(92) | weapon_length(165)|swing_damage(20, blunt) | thrust_damage(57, pierce) ,imodbits_polearm],	
		
		#Akhilleus Kosmos - Shield
	["kosmos_shield", "Akhilleus Kosmos", [("tableau_shield_round_4",0)], itp_unique|itp_type_shield, 0, 430, weight(4.5)|hit_points(1200)|body_armor(32)|spd_rtng(81)|shield_width(50),imodbits_shield],
		
		#Side arming sword
	
		
		#flying chariot - Trois Tragoidia
	
		
		#Black and silver armor
	["achilles_plate", "Grecian Plate", [("full_plate_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#silver boots
	["achilles_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
		#matching gloves
	["achilles_gauntlets","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
	#Astolfo
		#La Black Luna, Horn that causes Rout
	
		
		#Trap of Argalia, Blunt Lance
	["argalia", "Trap of Argalia", [("joust_of_peace",0)], itp_no_blur|itp_couchable|itp_type_polearm|itp_unique|itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_greatlance, 158, weight(5)|difficulty(0)|spd_rtng(78)|weapon_length(240)|swing_damage(0, cut)|thrust_damage(71, blunt), imodbits_polearm],	
		
		#Thin Sidearming Sword
	["astolfo_sidearm", "Arming Sword", [("sword_medieval_c",0),("sword_medieval_c_scabbard", ixmesh_carry)], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 410 , weight(1.5)|difficulty(0)|spd_rtng(99) | weapon_length(95)|swing_damage(29, cut) | thrust_damage(24, pierce),imodbits_sword_high ],
		
		#Hippogriff
	["hippogriff","Hippogriff", [("warhorse_steppe",0)], itp_unique|itp_type_horse, 0, 600,abundance(0)|body_armor(35)|hit_points(100)|difficulty(2)|horse_speed(70)|horse_maneuver(75)|horse_charge(20)|horse_scale(135),imodbits_horse_basic|imodbit_champion],
		
		#Casseur de Logistille - Grants rank A magic resistance
	
		
		#Black Armor
	["astolfo_plate", "Plate Armor", [("full_plate_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#White Cape
	
		
		#White Gauntlets
	["astolfo_gauntlets","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#White Boots with Spurs
	["astolfo_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
	#Saint George
		#Ascalon - Dragon Slaying Sword
	["ascalon", "Ascalon", [("military_cleaver_c",0)], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary, itc_scimitar|itcf_carry_sword_left_hip, 263, weight(1.5)|difficulty(0)|spd_rtng(96) | weapon_length(95)|swing_damage(35, cut)|thrust_damage(0,  pierce),imodbits_sword_high],
		
		#Bayard - Horse Negates first attack
	["bayard","Bayard", [("courser",0)], itp_unique|itp_type_horse, 0, 600,abundance(0)|body_armor(255)|hit_points(255)|difficulty(2)|horse_speed(70)|horse_maneuver(45)|horse_charge(20)|horse_scale(107),imodbits_horse_basic|imodbit_champion],
		
		#Copper Armor with White Tabard
	["george_armor", "Plate Armor", [("mail_long_surcoat_new",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Copper Gauntlets
	["george_gauntlets","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#Dragon Themed Copper Boots
	["george_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
 #Caster Gear
	#Medea
		#rule breaker
	["rulebreaker", "Rule Breaker", [("rulebreaker",0),], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_left, 37, weight(0.75)|difficulty(0)|spd_rtng(135) | weapon_length(30)|swing_damage(37, cut)|thrust_damage(35, pierce),imodbits_sword_high	],
		
		#Medea's Staff
	["medea_staff",  "Medea's Staff", [("medea_staff",0)], itp_no_blur|itp_type_polearm|itp_offset_lance|itp_unique| itp_primary, itc_staff|itcf_carry_sword_back, 202, weight(2)|difficulty(0)|spd_rtng(97) | weapon_length(140)|swing_damage(25, blunt) | thrust_damage(26,  blunt),imodbits_polearm ],
		
		#Weird Black Headgarb
	["medea_cap", "Skull Mask", [("turret_hat_r",0)], itp_type_head_armor|itp_unique, 0, 193, weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0),imodbits_cloth],
		
		#Purple Cape, Dress
	["medea_robes", "Plate Armor", [("woolen_dress",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Slippers
	["medea_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
	#Gilles De Rais
		#Prelati's Spellbook
		
		
		#Cloak
	["gilles_cloak", "Plate Armor", [("pilgrim_outfit",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Mantle
		
		
		#Slippers
	["gilles_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
	#William Shakespeare
		#First Folio
		
		
		#Victorian Garb
	["shakespeare_garb", "Plate Armor", [("blue_gambeson",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Leather Gloves
	["shakespeare_gloves","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#Classic Boots
	["shakespeare_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
	#Avicebron
		#focus mask
	["avicebron_mask", "Focus Mask", [("skull_mask",0)], itp_type_head_armor|itp_unique, 0, 193, weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0),imodbits_cloth],
		
		#Jester Outfit with cape
	["avicebron_clothes", "Plate Armor", [("red_gambeson",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Strange Pen Shoes
	["avicebron_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],

		# Black Gloves
	["avicebron_gloves","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
	#Hohenheim
		#Azoth Sword - Sword of Paracelsus
	["paracelsus_sword", "Azoth Sword - Sword of Paracelsus", [("sword_viking_b_small",0),], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary, itc_longsword, 162, weight(1.25)|difficulty(0)|spd_rtng(103)|weapon_length(85)|swing_damage(55, cut)|thrust_damage(32, pierce),imodbits_sword_high ],
		
		#Some red book
	
		
		#Elemental Crystals
	
		
		#Lab Coat looking Cloak
	["hohenheim_coat", "Plate Armor", [("sar_robe_b",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Silver Greaves
	["hohenheim_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
		#Black Weird Gloves
	["hohenheim_gloves","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#Very long hair
		
		
 #Assassin Gear
		#Generalized Assassin Gear
		
	["assassin_robes", "Plate Armor", [("pilgrim_outfit",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],
	["assassin_robes_a", "Plate Armor", [("robe",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],
		
	["throwing_daggers_assassin", "The Assassin's Throwing Daggers", [("throwing_dagger",0)], itp_no_blur|itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground ,itcf_throw_knife, 193 , weight(2.5)|difficulty(0)|spd_rtng(130) | shoot_speed(100) | thrust_damage(34, cut)|max_ammo(30)|weapon_length(0),imodbits_thrown],
		
	#Cursed Arm
	
	["cursed_arm_wrapped_body", "Plate Armor", [("pilgrim_outfit",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],
	
	["cursed_arm_unwrapped_body", "Plate Armor", [("cursedarmtest",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
	
		#Skull Mask
	["skull_mask_with_hood", "Skull Mask", [("skull_mask_with_hood",0)], itp_type_head_armor|itp_unique, 0, 193, weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0),imodbits_cloth],
	
		#Arm, wrapped
	["cursedarm_wrapped","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#Arm, unwrapped
	["cursedarm_unwrapped","Gauntlets", [("cursedarmhandL",0)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#Cursed Dagger
	["cursed_dagger_thrown", "Thrown Cursed Arm Daggers", [("throwing_dagger",0)], itp_no_blur|itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground|itp_next_item_as_melee ,itcf_throw_knife, 193 , weight(2.5)|difficulty(0)|spd_rtng(130) | shoot_speed(100) | thrust_damage(34, cut)|max_ammo(30)|weapon_length(0),imodbits_thrown],
		
	["cursed_dagger", "Cursed Arm Dagger", [("dagger_b",0),], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary, itc_dagger, 37, weight(0.75)|difficulty(0)|spd_rtng(135) | weapon_length(30)|swing_damage(37, cut)|thrust_damage(35, pierce),imodbits_sword_high	],

	["cursedarm", "Cursed Arm Dagger", [("cursedarmweapon",0),], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary, itc_longsword, 37, weight(0.75)|difficulty(0)|spd_rtng(135) | weapon_length(30)|swing_damage(25, blunt)|thrust_damage(99999, pierce),imodbits_sword_high],
		#Cursed Dagger - Thrown
		
	#Serenity
		# Mask
	["serenity_mask", "Skull Mask", [("skull_mask",0)], itp_type_head_armor|itp_unique|itp_doesnt_cover_hair, 0, 193, weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0),imodbits_cloth],
		# Poison Dagger
		
	["serenity_dagger_thrown", "Thrown Cursed Arm Daggers", [("throwing_dagger",0)], itp_no_blur|itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground|itp_next_item_as_melee ,itcf_throw_knife, 193 , weight(2.5)|difficulty(0)|spd_rtng(130) | shoot_speed(100) | thrust_damage(34, cut)|max_ammo(30)|weapon_length(0),imodbits_thrown],
		
	["serenity_dagger", "Serenity Dagger", [("dagger_b",0),], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary, itc_dagger, 37, weight(0.75)|difficulty(0)|spd_rtng(135) | weapon_length(30)|swing_damage(37, cut)|thrust_damage(35, pierce),imodbits_sword_high	],
	
	
	#Shadow Peeling
		# Mask
	["shadow_mask", "Skull Mask", [("skull_mask",0)], itp_type_head_armor|itp_unique   ,0, 193 , weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0) ,imodbits_cloth ],
		# Dagger
		
	["shadow_dagger_thrown", "Thrown Cursed Arm Daggers", [("throwing_dagger",0)], itp_no_blur|itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground|itp_next_item_as_melee ,itcf_throw_knife, 193 , weight(2.5)|difficulty(0)|spd_rtng(130) | shoot_speed(100) | thrust_damage(34, cut)|max_ammo(30)|weapon_length(0),imodbits_thrown],
		
	["shadow_dagger", "Shadow-Peeling Dagger", [("dagger_b",0),], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary, itc_dagger|itcf_carry_dagger_front_left, 37, weight(0.75)|difficulty(0)|spd_rtng(135) | weapon_length(30)|swing_damage(37, cut)|thrust_damage(35, pierce),imodbits_sword_high	],
	
	#100 Face
	
		#100 Face Dagger
	
	["hundred_dagger_thrown", "Thrown Cursed Arm Daggers", [("throwing_dagger",0)], itp_no_blur|itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground|itp_next_item_as_melee ,itcf_throw_knife, 193 , weight(2.5)|difficulty(0)|spd_rtng(130) | shoot_speed(100) | thrust_damage(34, cut)|max_ammo(30)|weapon_length(0),imodbits_thrown],
		
	["hundred_dagger", "Hundred Face Dagger", [("dagger_b",0),], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary, itc_dagger|itcf_carry_dagger_front_left, 37, weight(0.75)|difficulty(0)|spd_rtng(135) | weapon_length(30)|swing_damage(37, cut)|thrust_damage(35, pierce),imodbits_sword_high	],
		#100 Face Dagger - Thrown
	
		#100 Face Mask
	["hundred_face_mask", "Skull Mask", [("skull_mask",0)], itp_type_head_armor|itp_doesnt_cover_hair|itp_unique   ,0, 193 , weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0) ,imodbits_cloth ],
		
		
	#King Hassan
		#Armor
	["king_hassan_armor", "Plate Armor", [("robe",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Skull Helmet
	["skull_helmet", "Skull Mask", [("skull_mask",0)], itp_type_head_armor|itp_unique, 0, 193, weight(2)|abundance(100)|head_armor(54)|body_armor(5)|leg_armor(0), imodbits_cloth],
		
		#Giant Sword 
	["hassan_sword", "The Sword of the Old Man of the Mountain", [("azrael",0)], itp_no_blur|itp_type_two_handed_wpn|itp_unique|itp_primary, itc_bastardsword|itcf_carry_sword_back, 263, weight(1.5)|difficulty(0)|spd_rtng(96) | weapon_length(95)|swing_damage(47, cut)|thrust_damage(0, pierce), imodbits_sword_high],
		
		#Shield
	["coffin_shield", "Heavy Board Shield", [("tableau_shield_pavise_1" ,0)], itp_unique|itp_type_shield|itp_cant_use_on_horseback|itp_wooden_parry, itcf_carry_board_shield, 370, weight(5)|hit_points(550)|body_armor(14)|spd_rtng(78)|shield_width(43)|shield_height(100), imodbits_shield],
		
 #Berserker Gear
	#Heracles
		#Nameless Axe-Sword
	["axe_sword", "Stone Axe-Sword", [("axesword",0)], itp_no_blur|itp_unique|itp_type_two_handed_wpn|itp_primary, itc_greatsword|itcf_carry_sword_back, 1123, weight(2.75)|difficulty(1)|spd_rtng(87)|weapon_length(150)|swing_damage(53, cut)|thrust_damage(40,  cut), imodbits_none],
		
		#Armored Skirt thing
	["heracles_wear", "Plate Armor", [("full_plate_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Arm Cuffs
	["heracles_armcuffs","Gauntlets", [("gauntlets_L",0)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#Foot Cuffs
	["heracles_footcuffs", "Black Greaves", [("black_greaves",0)], itp_type_foot_armor|itp_attach_armature, 0, 2361 , weight(3.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(45)|difficulty(0) ,imodbits_none],
		
	#Lancelot
		#Arondight
	["zerkalot_sword", "Arondight", [("sword_two_handed_a",0)], itp_no_blur|itp_type_two_handed_wpn|itp_two_handed|itp_primary, itc_greatsword|itcf_carry_sword_back, 1123, weight(2.75)|difficulty(10)|spd_rtng(87)|weapon_length(120)|swing_damage(53, cut) | thrust_damage(40 ,  pierce),imodbits_none],
	
		#Dark Armor
	["berserkelot_armor", "Black Armor", [("black_armor",0)], itp_type_body_armor|itp_covers_legs, 0, 9496, weight(28)|abundance(0)|head_armor(0)|body_armor(75)|leg_armor(25)|difficulty(10), imodbits_none],
	
		#Dark Helmet
	["berserkelot_helmet", "Full Helm", [("great_helmet_new_b",0)], itp_type_head_armor |itp_covers_head , 0, 811, weight(2.5)|abundance(0)|head_armor(75)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_none, 
		[(ti_on_init_item, [
		
			 (store_trigger_param_1, ":agent"),
			 (gt, ":agent", 0),
             (agent_get_position, pos0, ":agent"),
			 
			 (try_for_range, reg4, 0, 5),
				(val_sub, reg10, 120),
                (set_position_delta, pos0, reg10, 0, 0), #Position, X, Y, Z.
			    (particle_system_add_new, "psys_lancelot_obscurement_smoke", pos1),
				(store_mul, reg10, reg4, 25),
             (try_end),
			 
			]
			)
		]],
			 
		#Dark Greaves
	["berserkelot_greaves", "Black Greaves", [("black_greaves",0)], itp_type_foot_armor  | itp_attach_armature,0, 2361 , weight(3.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(45)|difficulty(0) ,imodbits_none],
	
		#Dark Gauntlets
	["berserkelot_gloves","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_type_hand_armor,0, 1040, weight(1.0)|abundance(0)|body_armor(20)|difficulty(0),imodbits_none],
	
	#Sparticus
		#Gladiator Sword
	["spart_sword", "Sword", [("spartacus_sword",0)], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary, itc_scimitar|itcf_carry_sword_left_hip, 263, weight(1.5)|difficulty(0)|spd_rtng(96) | weapon_length(95)|swing_damage(35, cut)| thrust_damage(0, pierce), imodbits_sword_high],
	
	["spart_club", "Club", [("spartacus_club",0)], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary, itc_scimitar|itcf_carry_sword_left_hip, 263, weight(1.5)|difficulty(0)|spd_rtng(96) | weapon_length(95)|swing_damage(35, pierce)| thrust_damage(0, blunt), imodbits_sword_high],
		
		#Face Gear
	["spart_mask", "Bondage Mask", [("spartacus_mask",0)], itp_type_head_armor |itp_covers_hair ,0, 811 , weight(2.5)|abundance(0)|head_armor(75)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_none],
		
		#Slave restraints
	["spart_restraints", "Plate Armor", [("man_body",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Shinguards
	["spart_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
		#Arm Guards
	["spart_gauntlets","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
	#Frankenstein
		#Weird Club - Bridal Chest
	["bridal_chest",  "Bridal Chest", [("bridal_chest",0)], itp_no_blur|itp_type_polearm|itp_offset_lance| itp_primary|itp_two_handed|itp_unique, itc_staff, 169, weight(7)|difficulty(18)|spd_rtng(78)|weapon_length(130)|swing_damage(50, blunt) | thrust_damage(35, blunt), imodbits_polearm],
		
		#Bridal Chest - Opened for Blasted Tree
	["bridal_chest_opened",  "Bridal Chest", [("bridal_chest",0)], itp_no_blur|itp_type_polearm|itp_offset_lance| itp_primary|itp_two_handed|itp_unique, itc_staff, 169 , weight(7)|difficulty(18)|spd_rtng(78) | weapon_length(130)|swing_damage(50 , blunt) | thrust_damage(35, blunt), imodbits_polearm ],
		
		#Wedding Dress
	["frankie_dress", "Plate Armor", [("bride_dress",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Heels
	["frankie_boots", "Plate Boots", [("bride_shoes",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
		
		#fran's gloves
	["fran_gloves","Gauntlets", [("gauntlets_L",0),("gauntlets_L",imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#Horn and veil
	["fran_veil", "Crown of Frankenstein", [("bride_crown",0)],  itp_type_head_armor|itp_doesnt_cover_hair|itp_unique|itp_attach_armature, 0, 1, weight(0.5)|abundance(100)|head_armor(35)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_cloth ],
		
	#Asterios
		#Left Poleaxe - Labrys
	["labrys_left", "Labrys", [("asterios_l",0)], itp_unique|itp_type_shield|itp_no_pick_up_from_ground, 0,  697 , weight(2.5)|hit_points(1700)|body_armor(0)|spd_rtng(61)|shield_width(10), imodbits_shield ],
		
		#Right Polexe - Labrys
	["labrys_right", "Labrys", [("asterios",0)], itp_no_blur|itp_type_polearm|itp_primary|itp_bonus_against_shield|itp_wooden_parry|itp_unbalanced|itp_unique, itc_bastardsword, 660, weight(5.5)|difficulty(10)|spd_rtng(91)|weapon_length(170)|swing_damage(54, cut)|thrust_damage(19, blunt),imodbits_axe],
		
		#Bullhead Mask
	["minotaur_mask", "Minotaur Mask", [("asterios_mask",0)], itp_unique|itp_type_head_armor|itp_doesnt_cover_hair,0, 1240 , weight(2.75)|abundance(100)|head_armor(55)|body_armor(0)|leg_armor(0)|difficulty(10) ,imodbits_plate ],	
		
		#Stomach-guard and Dress?
	["asterios_belt", "Plate Armor", [("man_body",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Arm Bindings
	["asterios_bindings","Gauntlets", [("gauntlets_L",0),("gauntlets_L", imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#Leg Bindings
	["asterios_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
	
	# #### Fate Noble Phantasms ############
	# ######## and Servant Weapons #########
	
	# Dainsleif
	# Durandal
	# Gram
	# Harpe
	["harpe", "Harpe", [("scythe",0)], itp_type_polearm|itp_offset_lance|itp_merchandise| itp_primary|itp_penalty_with_shield|itp_wooden_parry, itc_staff|itcf_carry_spear, 43 , weight(2)|difficulty(0)|spd_rtng(97) | weapon_length(182)|swing_damage(30 , cut) | thrust_damage(14, pierce), imodbits_polearm ],
	
	# Houtengeki
	# Vajra
	
		
		#Stomach-guard and Dress?
	["mash_armor", "Plate Armor", [("black_armor",0)], itp_unique| itp_type_body_armor|itp_covers_legs, 0, 6553, weight(1)|abundance(0)|head_armor(0)|body_armor(67)|leg_armor(32)|difficulty(2), imodbits_plate],	
		
		#Arm Bindings
	["mash_guants","Gauntlets", [("gauntlets_L",0),("gauntlets_L", imodbit_reinforced)], itp_unique|itp_type_hand_armor, 0, 1040, weight(1.0)|abundance(0)|body_armor(14)|difficulty(0),imodbits_armor],	
		
		#Leg Bindings
	["mash_boots", "Plate Boots", [("plate_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 1770, weight(0.5)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(33)|difficulty(0) ,imodbits_plate ],	
	
	["mash_shield", "Mash Shield",   [("mash",0)], itp_no_blur|itp_unique|itp_type_shield|itp_cant_use_on_horseback|itp_wooden_parry, itcf_carry_board_shield, 370, weight(75)|hit_points(2000)|body_armor(45)|spd_rtng(88)|shield_width(175)|shield_height(260),imodbits_shield],
	
	["mash_shield_bash", "invisible wpn", [("none",0)], itp_unique|itp_type_one_handed_wpn|itp_can_knock_down|itp_primary|itp_wooden_parry|itp_unbalanced, itc_cleaver|itcf_carry_axe_back, 510, weight(5.0)|difficulty(10)|spd_rtng(35)|weapon_length(125)|swing_damage(50, cut)|thrust_damage(0, pierce),imodbits_axe,
		[(ti_on_weapon_attack, [
			(store_trigger_param_1, ":agent_id"),
			(set_fixed_point_multiplier, 100),
			
			(agent_get_action_dir, ":attack_dir", ":agent_id"),
			
			(try_begin),
				(eq, ":attack_dir", 0),
				(agent_set_animation, ":agent_id", "anim_release_overswing_staff", 1),
			(else_try),
				(eq, ":attack_dir", 1),
				(agent_set_animation, ":agent_id", "anim_release_slashright_staff", 1),
			(else_try),
				(eq, ":attack_dir", 2),
				(agent_set_animation, ":agent_id", "anim_release_slashleft_staff", 1),
			(else_try),
				(eq, ":attack_dir", 3),
				(agent_set_animation, ":agent_id", "anim_release_overswing_staff", 1),
			(try_end),
		
			(call_script, "script_fate_mash_shieldbash", ":agent_id"),	
		])]],
#Master Items
 #Summoning Gear
  #Saber Catalysts
   #Tester Saber
   ["relic_saber","Saber Relic", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Artoria
   ["relic_saber01","Avalon", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Nero Claudius
   ["relic_saber02","Golden Cithara", [("lyre",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Siegfried
   ["relic_saber03","Perserved Fig Leaf", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Fergus mac Roich
   ["relic_saber04","Humming Stone Shard", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Richard Lionheart
   ["relic_saber05","Decorative Lead Sword", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
  #Lancer Catalysts
   #Tester Lancer
   ["relic_lancer","Lancer Relic", [("honey_pot",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Cu Chulainn
   ["relic_lancer01","Strange Earring", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Diarmuid Ua Duibhne
   ["relic_lancer02","Ancient Cup", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Fionn mac Cumhaill
   ["relic_lancer03","Boar-shaped Horn", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Hector
   ["relic_lancer04","Hector", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Brynhildr
   ["relic_lancer05","Brynhildr", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
  #Archer Catalysts
   #Tester Archer
   ["relic_archer","Archer Relic", [("leatherwork_inventory",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Heroic Spirit EMIYA
   ["relic_archer01","Jeweled Pendant", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Gilgamesh
   ["relic_archer02","Fossilized Snake Skin", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Atalanta
   ["relic_archer03","Longship Mast", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Robin Hood
   ["relic_archer04","Yew Tree Stump", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Arash
   ["relic_archer05","Fossilized Walnut", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
  #Rider Catalysts
   #Tester Rider
   ["relic_rider","Rider Relic", [("steppe_helmetY",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Medusa
   ["relic_rider01","Statuesque Head", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Iskandar
   ["relic_rider02","Torn Fabric", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Achilles
   ["relic_rider03","Jar of Oil", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Astolfo
   ["relic_rider04","Strange Liquid in Glass Bottle", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Saint George
   ["relic_rider05","Alleged Dragon Scales", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
  #Caster Catalysts
   #Tester Caster
   ["relic_caster","Caster Relic", [("book_a",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Medea
   ["relic_caster01","Shining Fleece", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Gilles de Rais
   ["relic_caster02","Le Mistere du Siege d'Orleans", [("book_d",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #William Shakespeare
   ["relic_caster03","Broken Pen", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Avicebron
   ["relic_caster04","Strange Stone Shards", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Hohenheim
   ["relic_caster05","Gilded Flask", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
  #Assassin Catalysts TODO: Do these get Catalysts?
   ["relic_assassin","Generic Assassin Relic", [("throwing_dagger",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_assassin01","Cursed Arm Relic", [("throwing_dagger",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_assassin02","Hundred Faces Relic", [("throwing_dagger",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_assassin03","Shadow Peeling Relic", [("throwing_dagger",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_assassin04","Serenity Relic", [("throwing_dagger",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_assassin05","King Hassan Relic", [("throwing_dagger",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
  #Berserker Catalysts
   #Tester Berserker
   ["relic_berserker","Berserker Relic", [("vaeg_helmet9",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Heracles
   ["relic_berserker01","Immense Stone Slab", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(570)|abundance(0),imodbits_none],
   #Lancelot
   ["relic_berserker02","Wrapped Elm Branch", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Sparticus
   ["relic_berserker03","Roman Manacles", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Frankenstein's Monster
   ["relic_berserker04","Bizarre Blueprints", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   #Asterios
   ["relic_berserker05","Polished Thread", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_avenger","Avenger Relic", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_avenger01","Book of Evil", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_ruler","Ruler Relic", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_ruler01","Cross", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_ruler02","Japan Cross", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_shielder","Shielder Relic", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   ["relic_shielder01","Galahad", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   
   ["relics_end","Not a thing", [("linen",0)], itp_unique|itp_type_goods, 0, 250,weight(1)|abundance(0),imodbits_none],
   
   ["gob_sword","Tresure Sword", [("scimitar_b",0),("scimitar_b",ixmesh_flying_ammo)], itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground, itcf_throw_javelin|itcf_carry_sword_left_hip,
	360, weight(2.5)|difficulty(0)|spd_rtng(99) | shoot_speed(600) | thrust_damage(20,pierce)|max_ammo(1)|weapon_length(55),imodbits_thrown_minus_heavy, 
	[
	
	(ti_on_missile_hit, [
			 # (particle_system_burst, "psys_gourd_smoke", pos1, 100),
			 # (particle_system_burst, "psys_fireplace_fire_big", pos1, 100),
			 # (call_script, "script_cf_fate_explosion", 750, 1250, 2500),
            ]),			
	]],	
	
	["gob_sword2","Tresure Sword", [("bastard_sword_b",0),("bastard_sword_b",ixmesh_flying_ammo)], itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground,itcf_throw_javelin|itcf_carry_sword_left_hip,
	360, weight(2.5)|difficulty(0)|spd_rtng(99) | shoot_speed(600) | thrust_damage(20,pierce)|max_ammo(1)|weapon_length(75),imodbits_thrown_minus_heavy, 
	[
	
	(ti_on_missile_hit, [
			 # (particle_system_burst, "psys_gourd_smoke", pos1, 100),
			 # (particle_system_burst, "psys_fireplace_fire_big", pos1, 100),
			 # (call_script, "script_cf_fate_explosion", 750, 1250, 2500),
            ]),			
	]],	
	
	["gob_spear1","Tresure Sword", [("military_sickle_a",0),("military_sickle_a",ixmesh_flying_ammo)], itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground,itcf_throw_javelin|itcf_carry_sword_left_hip,
	360, weight(2.5)|difficulty(0)|spd_rtng(99) | shoot_speed(600) | thrust_damage(20,pierce)|max_ammo(1)|weapon_length(70),imodbits_thrown_minus_heavy, 
	[
	
	(ti_on_missile_hit, [
			 # (particle_system_burst, "psys_gourd_smoke", pos1, 100),
			 # (particle_system_burst, "psys_fireplace_fire_big", pos1, 100),
			 # (call_script, "script_cf_fate_explosion", 750, 1250, 2500),
            ]),			
	]],
	
	["gob_spear2","Tresure Sword", [("khergit_pike_b",0),("khergit_pike_b",ixmesh_flying_ammo)], itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground,itcf_throw_javelin|itcf_carry_sword_left_hip,
	360, weight(2.5)|difficulty(0)|spd_rtng(99) | shoot_speed(600) | thrust_damage(20,pierce)|max_ammo(1)|weapon_length(120),imodbits_thrown_minus_heavy, 
	[
	
	(ti_on_missile_hit, [
			 # (particle_system_burst, "psys_gourd_smoke", pos1, 100),
			 # (particle_system_burst, "psys_fireplace_fire_big", pos1, 100),
			 # (call_script, "script_cf_fate_explosion", 750, 1250, 2500),
            ]),			
	]],
	
	["gob_axe","Tresure Sword", [("two_handed_battle_long_axe_c",0),("two_handed_battle_long_axe_c",ixmesh_flying_ammo)], itp_type_thrown|itp_unique|itp_primary|itp_no_pick_up_from_ground,itcf_throw_javelin|itcf_carry_sword_left_hip,
	360, weight(2.5)|difficulty(0)|spd_rtng(99) | shoot_speed(600) | thrust_damage(45,pierce)|max_ammo(1)|weapon_length(65),imodbits_thrown_minus_heavy, 
	[
	
	(ti_on_missile_hit, [
			 # (particle_system_burst, "psys_gourd_smoke", pos1, 100),
			 # (particle_system_burst, "psys_fireplace_fire_big", pos1, 100),
			 # (call_script, "script_cf_fate_explosion", 750, 1250, 2500),
            ]),			
	]],

 # Specific NPC Gear
	# Taiga
 # Specific Master Gear # Use Generalized Gear Whenever Possible
	# #######Fate/ SN - Hollow Ataraxia Masters
		# Shirou Emiya
			# Baseball Tee Shirt and Jeans
			["shirou_shirt", "Shirou Casual", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
			# Converse Style Sneakers
			["shirou_sneakers", "Hi-top Sneakers", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
			# Rolled Poster
			# Shinai
			# Right Arm Shroud (HF Route)
			["shirou_shirt_hf", "Baseball Shirt with Shroud", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
		# Rin Tohsaka
			# Red Top with Black Skirt
			["rin_outfit", "Red Top and Skirt", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
			# Knee High Socks with Shoes
			["rin_heels", "Tohsaka Dress Shoes", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
			# Bows in Hair
			["rin_ribbon", "Rin Hair Ribbons", [("felt_hat_a_new",0)],  itp_type_head_armor |itp_civilian,0, 4 , weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
			# Single Use Gem Spells
				# Freeze
				["gemstone_saph", "Charged Sapphire", [("gandr",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
				# Explosion
				["gemstone_red", "Charged Ruby", [("gandr",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
				# Stun
				["gemstone_yellow", "Charged Yellow", [("gandr",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
				# Trap
				["gemstone_lapis", "Charged Lapis", [("gandr",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
			
		# Illyasviel von Einzbern
			# Winter Cap
			["illya_cap", "Woolen Cap", [("felt_hat_a_new",0)],  itp_type_head_armor |itp_civilian,0, 4 , weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
			# Peacoat
			["illya_peacoat", "Peacoat", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
			# Mittens
			["illya_mittens","Wool Mittens", [("leather_gloves_L",0)], itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],
			# Wire Bird
			["wire_bird", "Bird Familiar", [("gandr",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
		# Sakura Matou
			# Ribbons in Hair
			# Cherry Blossom Apron
			# School Uniform
			# Casual Sweater
			# HF Shadow Outfit
		# Shinji Matou
			# Command Seal Book
			# School Uniform
			# Knife
		# Souichirou Kuzuki
			# Three Piece Suit
			# Glasses
		# Kirei Kotomine
			# Priest Garb
			# Sleeveless Combat Shirt
		# Zouken Matou
			# Kimono
			# Cane
			# BUGS
		# Bazett Fraga McRemitz
			# Fragarach - Essentially Punch Dagger
			# Seal Designation Enforcer Suit
			# Leather Gloves
	# #######Fate/ Zero Masters
		# Kiritsugu Emiya
			# Cigarettes
			# Bone Bullets
		# Kirei Kotomine
			# Shawl
			# Enhanced Black Keys
		# Tokiomi Tohsaka
			# Red Suit
			# Crystals
			# Gem Magic
			# Gemmed Cane
		# Waver Velvet
			# Sweater and Collared Shirt, Khakis
			# Brown Leather Shoes
			# Sacrificial Knife
		# Kariya Matou
			# Bugs
			# Hoodie and Dirty Jeans
			# Torn Sneakers
		# Ryuunosuke Uryuu
			# Knife
			# Casual Clothes
			# Pistol
		# Kayneth El-Melloi Archibald
			# Coat
			# Metallic ~ Autonomous Ball ~ Thingie
			# Gloves
	   
	# #######Fate/ Apocrypha Masters
		# ########Black Faction -Yggdmillennia's-
			#Yggdmillennia Suit
		# Darnic Prestone Yggdmillennia (Vlad III, Lancer)
		# Gordes Musik Yggdmillennia (Siegfried, Saber)
		# Fiore Forvedge Yggdmillennia (Chiron, Archer)
			# Iron Spider Legs
		# Celenike Icecolle Yggdmillennia (Astolfo, Rider)
			# Knives
		# Roche Frain Yggdmillennia (Avicebron, Caster)
		# Caules Forvedge Yggdmillennia (Frankenstein, Berserker)
		# Reika Rikudou (Jack the Ripper, Assassin)
			# Short Green Dress
			# Knife
			# Revolver
		# ######### Red Faction
		# Kairi Sisigou (Mordred, Saber)
		["apo_leather", "Sisigou's Leather Jacket", [("leather_jacket_new",0)], itp_unique|itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 50, weight(3)|abundance(100)|head_armor(0)|body_armor(20)|leg_armor(0)|difficulty(0), imodbits_cloth],
		["biker_boots", "Sisigou's Biker Boots", [("light_leather_boots",0)], itp_unique|itp_type_foot_armor|itp_attach_armature, 0, 91, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(15)|difficulty(0), imodbits_cloth],
		# Rottweil Berzinsky? (Atalanta, Archer)
		# Feend vor Sembren? (Karna, Lancer)
		# Cabik Pentel?? (Achilles, Rider)
		# Jean Rum? (Shakespeare, Caster)
		# Shirou Kotomine (Semiramis, Assassin)
			# Holy Katana
			# Priest Garb
			# Samurai Garb
			# Black Keys
		# Deimlet Pentel?? (Spartacus, Berserker)
	# #############Fate/ strange fake
	
	# #############Fate/ Ex-
		# Hakuno Kishinami -male NERO, NAMELESS, Fox
		# Hakuno Kishinami -female NERO or NAMELESS, Fox
		# Rin Tohsaka -extra (fancy boy Cu Chulainn)
			# Rin Extra Outfit
			# Rin Extra Ribbon
		# Rani VIII (Lu Bu, so a Berserker)
			# Atlas Outfit
			
		# Shinji Matou (Francis Drake, RIDER)
			# Knife
		# Leonard B. Harway (Gawain, SABER)
		# Dan Blackmore (Robin Hood, Archer)
			# Old Style Sniper Rifle
		# Alice (Fairytale Caster)
		# Julius B. Harway (Li Shuwen, Assassin)
		# Run Ru (Lancer, Vlad III)
		# Monji Gatou (Berserker, Arcueid Brunestud - Tskikiheme-)
		# Twice H. Pieceman (Saver, "Savior", Buddha)
	
	# #############Fate/ Grand Order
		# Ritsuka Fujimaru, both genders
			# Various Available Outfits
	# #############Fate/ Prototype
		# Ayaka Sajyou: The Seventh, Princes (Arthur (Proto), Saber)
		# The Sixth, Powers (Servant Unknown)
		# Anonymous Master: The Fifth, Virtues (Gilgamesh (Proto), Archer)
		# The Forth, Dominons (Servant Unknown)
		# Aro Isemi: The Third, Thrones (Perseus, Rider)
		# Misaya Reiroukan: The Second, Cherubim (Cu Chulainn (Proto), Lancer)
		# Sancraid Phahn: VOID, Absences (Heracles (Proto), Berserker)
		# Manaka Sajyou: The First, Seraphim (Beast VI, Beast)


   #Generic Mage Gear
	#Azoth Sword
	["azoth_sword", "Azoth Sword", [("azoth",0),], itp_no_blur|itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip, 162, weight(1.25)|difficulty(0)|spd_rtng(103)|weapon_length(85)|swing_damage(28, cut)|thrust_damage(21, pierce),imodbits_sword_high, [
	
			(ti_on_weapon_attack, [
				(store_trigger_param_1, ":agent_id"),
				(set_fixed_point_multiplier, 100),
				
				(agent_get_look_position, pos3, ":agent_id"),
				
				(agent_get_action_dir, ":attack_dir", ":agent_id"),
				
				(try_begin),
					(eq, ":attack_dir", 3),
					(position_move_y, pos3, 50),
					(set_spawn_position, pos3),
				 
					# (spawn_scene_prop, "spr_beam_attack"),
				(try_end),
			])
		]],
	
	["test_lightsaber", "Lightsaber", [("lightsaber",0),], itp_no_blur|itp_type_one_handed_wpn|itp_primary, itc_longsword|itcf_carry_sword_left_hip, 162, weight(1.25)|difficulty(0)|spd_rtng(103)|weapon_length(85)|swing_damage(28, cut)|thrust_damage(21, pierce),imodbits_sword_high],
	
	["imod_test_helmet", "Agius Ears", [("helmet_fur_a",imod_plain), ("azoth",imod_tattered), ("flintlock_pistol",imod_ragged), ("apples",imod_sturdy), ("linen",imod_thick), ("avalon", imod_hardened)], itp_type_head_armor|itp_unique, 0, 193, weight(2)|abundance(100)|head_armor(34)|body_armor(5)|leg_armor(0), imodbits_cloth],
 #Modern Weapons
	#Multiple Modern Firearms IMFDB as Source
		# Ammunition		
		["origin_rounds", "Origin Rounds", [("cartridge_a", ixmesh_inventory), ("heavy_projectile",ixmesh_flying_ammo),("bolt_bag_c", ixmesh_carry)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_consumable, 0, 41, weight(2.25)| abundance(90)| weapon_length(3)|thrust_damage(35, pierce)| max_ammo (18), imodbits_missile, [
			bullet_impact
		]],
		
		["pistol_ammo", "9mm Pistol Cartridges", [("cartridge_a", ixmesh_inventory), ("pistol_projectile",ixmesh_flying_ammo), ("bolt_bag_c", ixmesh_carry)], itp_type_bullets|itp_merchandise|itp_consumable|itp_default_ammo, 0, 41, weight(2.25)| abundance(90)| weapon_length(3)|thrust_damage(15, cut)| max_ammo (35), imodbits_missile, [
			bullet_impact
		]],
		
		["pistol_ammo_piercing", "Armor Piercing Pistol Cartridges", [("cartridge_a", ixmesh_inventory), ("pistol_projectile",ixmesh_flying_ammo),("cartridge_a", ixmesh_carry)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_consumable, 0, 41, weight(2.25)| abundance(90)| weapon_length(3)|thrust_damage(25, pierce)| max_ammo (25), imodbits_missile, [
			bullet_impact
		]],
		
		["rifle_ammo", "Rifle Cartridges", [("cartridge_a", ixmesh_inventory), ("rifle_projectile",ixmesh_flying_ammo),("cartridge_a", ixmesh_carry)], itp_type_bullets|itp_no_pick_up_from_ground|itp_merchandise|itp_can_penetrate_shield|itp_consumable, 0, 41, weight(2.25)| abundance(90)| weapon_length(3)|thrust_damage(35, cut)| max_ammo (60), imodbits_missile, [
			bullet_impact
		]],
		
		["rifle_ammo_piercing", "Armor Piercing Cartridges", [("cartridge_a", ixmesh_inventory), ("rifle_projectile",ixmesh_flying_ammo),("cartridge_a", ixmesh_carry)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_consumable, 0, 41, weight(2.25)| abundance(90)| weapon_length(3)|thrust_damage(45, pierce)| max_ammo (45), imodbits_missile, [
			bullet_impact
		]],
		
		["heavy_cartridge", "Minigun Large Cartridges", [("cartridge_a", ixmesh_inventory), ("heavy_projectile",ixmesh_flying_ammo),("cartridge_a", ixmesh_carry)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_consumable, 0, 41, weight(75)| abundance(90)| weapon_length(3)|thrust_damage(35, cut)| max_ammo (999), imodbits_missile, [
			bullet_impact
		]],
		
		["rocket_ammo", "Guided Rocket Shells", [("stinger", ixmesh_inventory), ("stinger",ixmesh_flying_ammo),("cartridge_a", ixmesh_carry)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_consumable, 0, 41, weight(75)| abundance(90)| weapon_length(3)|thrust_damage(85, pierce)| max_ammo (6), imodbits_missile, [
			(ti_on_missile_hit, [
				(store_trigger_param_1, ":shooter"),
				(store_trigger_param_2, ":struck"),
				(copy_position, pos30, pos1),
				(copy_position, pos31, pos30),
				(agent_get_position, pos10, ":shooter"),
				(get_distance_between_positions_in_meters, ":distance", pos1, pos10),
				(call_script, "script_bullet_collision_particle", ":struck", ":distance"),
				(call_script, "script_cf_fate_explosion", pos30, ":shooter", 1000, 50, 75),
				
				(eq, ":struck", 0),
				
				(position_move_z, pos31, 50),
				
				(position_rotate_x, pos30, -90),
				(call_script, "script_lookat", pos31, pos30),
				(cast_ray, reg11, pos32, pos31),
				(position_rotate_x, pos32, 90), 
				
				(set_spawn_position, pos32),
				(spawn_scene_prop, "spr_impact"),
				(particle_system_burst, "psys_stone_debris", pos32, 50),
			])
		]],
		
		["mirv_ammo", "MIRV Rocket Shells", [("stinger", ixmesh_inventory), ("stinger", ixmesh_flying_ammo),("cartridge_a", ixmesh_carry)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_consumable, 0, 41, weight(75)| abundance(90)| weapon_length(3)|thrust_damage(15, pierce)| max_ammo (6), imodbits_missile, [
			(ti_on_missile_hit, [
				(store_trigger_param_1, ":shooter"),
				(store_trigger_param_2, ":struck"),
				(copy_position, pos30, pos1),
				(copy_position, pos31, pos30),
				(agent_get_position, pos10, ":shooter"),
				(get_distance_between_positions_in_meters, ":distance", pos1, pos10),
				(call_script, "script_bullet_collision_particle", ":struck", ":distance"),
				(call_script, "script_cf_fate_explosion", pos30, ":shooter", 350, 15, 35),
				
				(eq, ":struck", 0),
				
				(position_move_z, pos31, 50),
				
				(position_rotate_x, pos30, -90),
				(call_script, "script_lookat", pos31, pos30),
				(cast_ray, reg11, pos32, pos31),
				(position_rotate_x, pos32, 90), 
				
				(set_spawn_position, pos32),
				(spawn_scene_prop, "spr_impact"),
				(prop_instance_set_scale, reg0, 100, 100, 50),
				(particle_system_burst, "psys_stone_debris", pos32, 5),
			])
		]],
		
		["gandr_shell", "Cursed Necro Shell", [("cartridge_a", ixmesh_inventory),  ("gandr", ixmesh_flying_ammo), ("bolt_bag_c", ixmesh_carry)], itp_type_bullets|itp_merchandise|itp_can_penetrate_shield|itp_consumable, 0, 41, weight(75)| abundance(90)| weapon_length(3)|thrust_damage(35, pierce)| max_ammo (6), imodbits_missile, [
			bullet_impact
		]],
		
		["buckshot_shell", "12 Gauge Buckshot Shell", [("cartridge_a", ixmesh_inventory), ("shotgun_pellet",ixmesh_flying_ammo), ("bolt_bag_c", ixmesh_carry)], itp_type_bullets|itp_merchandise|itp_consumable|itp_default_ammo, 0, 41, weight(2.25)| abundance(90)| weapon_length(3)|thrust_damage(5, cut)| max_ammo (35), imodbits_missile, [
			smaller_bullet_impact
		]],
		
		["slug_shell", "12 Gauge Slug", [("cartridge_a", ixmesh_inventory), ("shotgun_pellet",ixmesh_flying_ammo), ("bolt_bag_c", ixmesh_carry)], itp_type_bullets|itp_merchandise|itp_consumable|itp_default_ammo, 0, 41, weight(2.25)| abundance(90)| weapon_length(3)|thrust_damage(25, cut)| max_ammo (35), imodbits_missile, [
			bullet_impact
		]],
		
		["riot_shell", "12 Gauge Riot Slug", [("cartridge_a", ixmesh_inventory), ("shotgun_pellet",ixmesh_flying_ammo), ("bolt_bag_c", ixmesh_carry)], itp_type_bullets|itp_merchandise|itp_consumable|itp_default_ammo, 0, 41, weight(2.25)| abundance(90)| weapon_length(3)|thrust_damage(35, blunt)| max_ammo (35), imodbits_missile, [
			bullet_impact
		]],
		
		["50cal", "50 Caliber Rounds", [("cartridge_a", ixmesh_inventory), ("heavy_projectile",ixmesh_flying_ammo), ("bolt_bag_c", ixmesh_carry)], itp_type_bullets|itp_merchandise|itp_consumable|itp_default_ammo, 0, 41, weight(2.25)| abundance(90)| weapon_length(3)|thrust_damage(35, pierce)| max_ammo (12), imodbits_missile, [
			bullet_impact
		]],
			#pistols
			
		#Beretta 92F
		["beretta", "Beretta 92F", [("beretta92fs", 0)], itp_type_pistol|itp_primary, itc_player_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(55) | shoot_speed(1000) | thrust_damage(35 ,pierce)|max_ammo(12)|accuracy(85)|weapon_length(20), imodbits_none,
			[]
			],
		#Colt M1911A1
		["colt", "Colt M1911A1", [("1911", 0)], itp_type_pistol|itp_primary, itc_player_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(45) | shoot_speed(1000) | thrust_damage(40 ,pierce)|max_ammo(7)|accuracy(75)|weapon_length(23),imodbits_none,
			[]
			],
		#Glock 17
		["glock", "Glock 17", [("glock17", 0)], itp_type_pistol|itp_primary, itc_player_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(75) | shoot_speed(1000) | thrust_damage(30 ,pierce)|max_ammo(17)|accuracy(90)|weapon_length(18),imodbits_none,
			[]
			],
		#Contender
		["contender", "Contender", [("contender", 0)], itp_type_pistol|itp_primary , itc_player_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(55) | shoot_speed(1000) | thrust_damage(75 ,pierce)|max_ammo(1)|accuracy(150)|weapon_length(35),imodbits_none,
			[]
			],
		#Tokarev TT-33
		["tokarev", "Tokarev TT-33", [("TT33", 0)], itp_type_pistol|itp_primary, itc_player_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(65) | shoot_speed(1000) | thrust_damage(25 ,pierce)|max_ammo(8)|accuracy(85)|weapon_length(18),imodbits_none,
			[]
			],
		#Walther PP
		["walther", "Walther PP", [("waltherPPK", 0)], itp_type_pistol|itp_primary , itc_player_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(70) | shoot_speed(1000) | thrust_damage(30 ,pierce)|max_ammo(10)|accuracy(85)|weapon_length(18),imodbits_none,
			[]
			],
		#Webley Mk. IV
		["webley", "Webley Mk. IV", [("webley", 0)], itp_type_pistol|itp_primary , itc_player_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(45) | shoot_speed(1000) | thrust_damage(50, pierce)|max_ammo(6)|accuracy(90)|weapon_length(25),imodbits_none,
			[]
			],
		
			#smgs
		#Calico M950
		["calico", "Calico M950", [("flintlock_pistol",0)], itp_type_pistol|itp_primary, itc_player_smg, 230 , weight(1.5)|difficulty(0)|spd_rtng(55) | shoot_speed(1000) | thrust_damage(35 ,pierce)|max_ammo(50)|accuracy(45),imodbits_none,
			[]
			],
		#Heckler & Koch MP5A3
		["mp5", "HK MP5A3", [("mp5",0)], itp_type_musket|itp_primary|itp_two_handed, itc_player_smg, 230, weight(1.5)|difficulty(0)|spd_rtng(55)|shoot_speed(1000)|thrust_damage(45, pierce)|max_ammo(30)|accuracy(55),imodbits_none,
			[]
			],
		#Madsen M50
		["madsen", "Madsen M50", [("flintlock_pistol",0)], itp_type_musket|itp_primary|itp_two_handed, itc_player_smg, 230, weight(1.5)|difficulty(0)|spd_rtng(55)| shoot_speed(1000)|thrust_damage(35, pierce)|max_ammo(32)|accuracy(50),imodbits_none,
			[]
			],
		#AKMS
		["akms", "AKMS", [("akm",0)], itp_type_musket|itp_primary|itp_two_handed, itc_player_smg, 230, weight(1.5)|difficulty(0)|spd_rtng(55)|shoot_speed(1000)|thrust_damage(50, pierce)|max_ammo(35)|accuracy(30),imodbits_none,
			[]
			],
		
			#assault rifles
		#Steyr AUG A1
		["aug", "Steyr AUG A1", [("AugA1",0)], itp_type_musket|itp_primary|itp_two_handed, itc_player_rifle, 230, weapon_length(50) | weight(1.5)|difficulty(0)|spd_rtng(55)| shoot_speed(1000)|thrust_damage(60, pierce)|max_ammo(42)|accuracy(65),imodbits_none,
			[]
			],
		#AK-47
		["ak47", "AK47", [("Ak47",0)], itp_type_musket|itp_primary|itp_two_handed|itp_next_item_as_melee, itc_player_rifle, 230, weight(1.5)|difficulty(0)|spd_rtng(55)| shoot_speed(1000)|thrust_damage(45, pierce)|max_ammo(30)|accuracy(50)|weapon_length(65),imodbits_none,
			[]
			],
		["ak47_melee", "AK47", [("Ak47",0)], itp_no_blur|itp_type_polearm|itp_primary|itp_can_penetrate_shield|itp_wooden_parry, itc_spear, 76, weight(3.5)|difficulty(0)|spd_rtng(85)|weapon_length(65)|thrust_damage(35, pierce),imodbits_polearm],
		
		#FN FAL
		["fn_fal", "FN FAL", [("fnfal",0)], itp_type_musket|itp_primary|itp_two_handed, itc_player_rifle, 230, weight(1.5)|difficulty(0)|spd_rtng(55)| shoot_speed(1000)|thrust_damage(45, pierce)|max_ammo(20)|accuracy(65),imodbits_none,
			[]
			],
		
			#Shotgun
		#Remington 870
		["remington_shotgun", "Remington 870", [("remington870",0)], itp_type_musket|itp_primary|itp_two_handed, itc_player_rifle, 230 ,  weapon_length(125)|weight(1.5)|difficulty(0)|spd_rtng(55) | shoot_speed(1000) | thrust_damage(15 ,pierce)|max_ammo(8)|accuracy(50),imodbits_none,
			[]
			],
			
		["remington_shotgun_so", "Remington 870 (Sawed-off)", [("remington870so",0)], itp_type_musket|itp_primary|itp_two_handed, itc_player_rifle, 230 ,  weapon_length(75) | weight(1.5)|difficulty(0)|spd_rtng(55) | shoot_speed(1000) | thrust_damage(15 ,pierce)|max_ammo(8)|accuracy(50),imodbits_none,
			[]
			],
			
		#Sawed-off Savage/Stevens 311A
		["sawedoff", "Sawed-off Savage/Stevens 311A", [("sawedoff",0)], itp_type_pistol|itp_primary, itc_player_pistol, 230 , weapon_length(35)|weight(1.5)|difficulty(0)|spd_rtng(55) | shoot_speed(1000) | thrust_damage(45 ,pierce)|max_ammo(2)|accuracy(55),imodbits_none,
			[]
			],
		
			#Sniper/Hunting Rifles
		#Barrett M82A1M
		["m82", "Beretta M82A1M", [("Barrett_M82A1",0)], itp_type_musket|itp_primary|itp_two_handed, itc_player_rifle, 230, weapon_length(250)|weight(1.5)|difficulty(0)|spd_rtng(35)| shoot_speed(1000)|thrust_damage(250, pierce)|max_ammo(5)|accuracy(135),imodbits_none,
			[]
			],
		#Remington 700
		["remington_rifle", "Remington 700", [("flintlock_pistol",0)], itp_type_musket|itp_primary|itp_two_handed, itc_player_rifle, 230 , weapon_length(125)|weight(1.5)|difficulty(0)|spd_rtng(75)| shoot_speed(1000)|thrust_damage(45, pierce)|max_ammo(10)|accuracy(115),imodbits_none,
			[]
			],
		#Walther WA 2000
		["wa2000", "Walther WA 2000", [("WA2000",0)], itp_type_musket|itp_primary|itp_two_handed, itc_player_rifle, 230 , weapon_length(125)|weight(1.5)|difficulty(0)|spd_rtng(55)| shoot_speed(1000)|thrust_damage(85, pierce)|max_ammo(6)|accuracy(150),imodbits_none,
			[]
			],
		
			#Heavy Weapons
		#M61 Vulcan
		["vulcan", "M61 Vulcan", [("vulcan",0)], itp_type_musket|itp_primary|itp_two_handed, itc_player_rifle, 230 , weapon_length(150)|weight(65.5)|difficulty(0)|spd_rtng(15) | shoot_speed(1000) | thrust_damage(35 ,pierce)|max_ammo(250)|accuracy(15),imodbits_none,
			[]
			],
		#Type 91 MANPADS
		["rocket", "Type 91 MANPADS", [("flintlock_pistol",0)], itp_type_pistol|itp_primary|itp_two_handed, itc_player_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(15) | shoot_speed(50) | thrust_damage(85 ,pierce)|max_ammo(1)|accuracy(85),imodbits_none,
			[]
			],
			
		#Beretta 92F
		["npc_beretta", "Beretta 92F", [("beretta92fs", 0)], itp_type_pistol|itp_primary, itc_npc_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(55) | shoot_speed(1000) | thrust_damage(35 ,pierce)|max_ammo(12)|accuracy(85)|weapon_length(20), imodbits_none,
			[]
			],
		#Glock 17
		["npc_glock", "Glock 17", [("glock17", 0)], itp_type_pistol|itp_primary, itc_npc_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(75) | shoot_speed(100) | thrust_damage(30 ,pierce)|max_ammo(17)|accuracy(90)|weapon_length(18),imodbits_none,
			[]
			],
		#Contender
		["npc_contender", "Contender", [("contender", 0)], itp_type_pistol|itp_primary , itc_npc_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(55) | shoot_speed(250) | thrust_damage(75 ,pierce)|max_ammo(1)|accuracy(150)|weapon_length(35),imodbits_none,
			[]
			],
		#Webley Mk. IV
		["npc_webley", "Webley Mk. IV", [("webley", 0)], itp_type_pistol|itp_primary , itc_npc_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(45) | shoot_speed(75) | thrust_damage(50, pierce)|max_ammo(6)|accuracy(90)|weapon_length(25),imodbits_none,
			[]
			],
		
			#smgs
		#Calico M950
		["npc_calico", "Calico M950", [("flintlock_pistol",0)], itp_type_pistol|itp_primary, itc_npc_smg1h, 230 , weight(1.5)|difficulty(0)|spd_rtng(55) | shoot_speed(125) | thrust_damage(35 ,pierce)|max_ammo(50)|accuracy(45),imodbits_none,
			[]
			],
		#Heckler & Koch MP5A3
		["npc_mp5", "HK MP5A3", [("mp5",0)], itp_type_musket|itp_primary|itp_two_handed, itc_npc_smg2h, 230, weight(1.5)|difficulty(0)|spd_rtng(55)|shoot_speed(100)|thrust_damage(45, pierce)|max_ammo(30)|accuracy(55),imodbits_none,
			[]
			],
		#AKMS
		["npc_akms", "AKMS", [("akm",0)], itp_type_musket|itp_primary|itp_two_handed, itc_npc_smg2h, 230, weight(1.5)|difficulty(0)|spd_rtng(55)|shoot_speed(100)|thrust_damage(50, pierce)|max_ammo(35)|accuracy(30),imodbits_none,
			[]
			],
		
			#assault rifles
		#AK-47
		["npc_ak47", "AK47", [("Ak47",0)], itp_type_musket|itp_primary|itp_two_handed|itp_next_item_as_melee, itc_npc_rifle, 230, weight(1.5)|difficulty(0)|spd_rtng(55)| shoot_speed(200)|thrust_damage(45, pierce)|max_ammo(30)|accuracy(50)|weapon_length(65),imodbits_none,
			[]],
		
			#Shotgun
		#Remington 870
		["npc_remington_shotgun", "Remington 870", [("remington870",0)], itp_type_musket|itp_primary|itp_two_handed, itc_npc_rifle, 230, weapon_length(125)|weight(1.5)|difficulty(0)|spd_rtng(55)|shoot_speed(50) |thrust_damage(15, pierce)|max_ammo(8)|accuracy(50), imodbits_none, []],
			
		#Sawed-off Savage/Stevens 311A
		["npc_sawedoff", "Sawed-off Savage/Stevens 311A", [("sawedoff",0)], itp_type_pistol|itp_primary, itc_npc_pistol, 230 , weapon_length(35)|weight(1.5)|difficulty(0)|spd_rtng(55) | shoot_speed(45) | thrust_damage(45 ,pierce)|max_ammo(2)|accuracy(55),imodbits_none,
			[]
			],
		
			#Sniper/Hunting Rifles
		#Walther WA 2000
		["npc_wa2000", "Walther WA 2000", [("WA2000",0)], itp_type_musket|itp_primary|itp_two_handed, itc_npc_rifle, 230 , weapon_length(125)|weight(1.5)|difficulty(0)|spd_rtng(55)| shoot_speed(300)|thrust_damage(85, pierce)|max_ammo(6)|accuracy(150),imodbits_none,
			[]
			],
		
			#Heavy Weapons
		#M61 Vulcan
		["npc_vulcan", "M61 Vulcan", [("vulcan",0)], itp_type_musket|itp_primary|itp_two_handed, itc_npc_rifle, 230 , weapon_length(150)|weight(65.5)|difficulty(0)|spd_rtng(15) | shoot_speed(215) | thrust_damage(35 ,pierce)|max_ammo(250)|accuracy(15),imodbits_none,
			[]
			],
		#Type 91 MANPADS
		["npc_rocket", "Type 91 MANPADS", [("flintlock_pistol",0)], itp_type_pistol|itp_primary|itp_two_handed, itc_npc_pistol, 230 , weight(1.5)|difficulty(0)|spd_rtng(15) | shoot_speed(50) | thrust_damage(85 ,pierce)|max_ammo(1)|accuracy(85),imodbits_none,
			[]
			],
			
		
		
	#Swords, Daggers, Etc
		["nagimaki", "Nagimaki", [("snm_nagimaki",0),("snm_nagimaki_scabbard",ixmesh_carry)], itp_no_blur|itp_type_two_handed_wpn| itp_primary, itc_poleaxe|itcf_carry_spear|itcf_show_holster_when_drawn, 679 , weight(2.0)|difficulty(9)|spd_rtng(108) | weapon_length(1803)|swing_damage(32 , cut) | thrust_damage(18 ,  pierce),imodbits_sword ],
		#Kendo Training Sword
		["kendo_sword", "Azoth Sword", [("shinai",0),], itp_no_blur|itp_type_one_handed_wpn|itp_primary, itc_bastardsword|itcf_carry_sword_left_hip, 162, weight(1.25)|difficulty(0)|spd_rtng(103)|weapon_length(113)|swing_damage(28, blunt)|thrust_damage(21, blunt),imodbits_sword_high],
		#Taiga's Training Sword
		["taiga_sword", "Taiga's Shinai", [("shinai_taiga",0)], itp_no_blur|itp_type_one_handed_wpn|itp_primary, itc_bastardsword|itcf_carry_sword_left_hip, 162, weight(1.25)|difficulty(0)|spd_rtng(103)|weapon_length(115)|swing_damage(45, blunt)|thrust_damage(25, blunt),imodbits_sword_high],
		#Bokken
		["wooden_katana", "Bokken", [("snm_nagimaki",0),("snm_nagimaki_scabbard",ixmesh_carry)], itp_no_blur|itp_type_two_handed_wpn| itp_primary, itc_poleaxe|itcf_carry_spear|itcf_show_holster_when_drawn, 679 , weight(2.0)|difficulty(9)|spd_rtng(108) | weapon_length(115)|swing_damage(32 , cut) | thrust_damage(18 ,  pierce),imodbits_sword ],
		#Hunting Knife
		["hunting_knife", "Hunting Knife", [("peasant_knife_new",0)], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary|itp_no_parry, itc_dagger|itcf_carry_dagger_front_left, 18, weight(0.5)|difficulty(0)|spd_rtng(110)|weapon_length(40)|swing_damage(51, cut)|thrust_damage(25, pierce),imodbits_sword],
	# Grenades, High Explosives
		#M67 Grenade
		["frag_grenade", "M67 Grenade", [("throwing_stone",0)], itp_type_thrown|itp_primary ,itcf_throw_stone, 1 , weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(30) | thrust_damage(11 ,  blunt)|max_ammo(18)|weapon_length(8),imodbit_large_bag ],
		#M18 Smoke Grenade
		["smoke_grenade", "M18 Smoke Grenade", [("throwing_stone",0)], itp_type_thrown|itp_primary ,itcf_throw_stone, 1 , weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(30) | thrust_damage(11 ,  blunt)|max_ammo(18)|weapon_length(8),imodbit_large_bag ],
		#Model 7290 Flashbang
		["stun_grenade", "Model 7290 Flashbang", [("throwing_stone",0)], itp_type_thrown|itp_primary ,itcf_throw_stone, 1 , weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(30) | thrust_damage(11 ,  blunt)|max_ammo(18)|weapon_length(8),imodbit_large_bag ],
		#C4
		["placed_explosive", "C4 Plastic Explosive", [("throwing_stone",0)], itp_type_thrown|itp_primary ,itcf_throw_stone, 1 , weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(30) | thrust_damage(11 ,  blunt)|max_ammo(18)|weapon_length(8),imodbit_large_bag ],
		["placed_explosive_trigger", "C4 Plastic Explosive - Remote", [("throwing_stone",0)], itp_type_thrown|itp_primary ,itcf_throw_stone, 1 , weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(30) | thrust_damage(11 ,  blunt)|max_ammo(18)|weapon_length(8),imodbit_large_bag ],
		#Mk 2 hand grenade
		["frag_grenade_01", "Mk2 Fragmentation Grenade", [("throwing_stone",0)], itp_type_thrown|itp_primary ,itcf_throw_stone, 1 , weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(30) | thrust_damage(11 ,  blunt)|max_ammo(18)|weapon_length(8),imodbit_large_bag ],
		#Mk 2 hand grenade - Heart Variant
		["necro_grenade", "Demonic Heart Grenade", [("throwing_stone",0)], itp_type_thrown|itp_primary ,itcf_throw_stone, 1 , weight(4)|difficulty(0)|spd_rtng(97) | shoot_speed(30) | thrust_damage(11 ,  blunt)|max_ammo(18)|weapon_length(8),imodbit_large_bag ],
	# Yumi
		["yumi", "Yumi", [("long_bow",0), ("long_bow_carry", ixmesh_carry)], itp_no_blur|itp_type_bow |itp_primary|itp_two_handed, itcf_shoot_bow|itcf_carry_bow_back, 145, weight(1.75)|difficulty(3)|spd_rtng(79)|shoot_speed(56)| thrust_damage(22, pierce), imodbits_bow],
	# Training Yumi
		["school_yumi", "Yumi", [("long_bow",0), ("long_bow_carry",ixmesh_carry)], itp_no_blur|itp_type_bow |itp_primary|itp_two_handed, itcf_shoot_bow|itcf_carry_bow_back, 145, weight(1.75)|difficulty(3)|spd_rtng(79)|shoot_speed(56)| thrust_damage(22, pierce), imodbits_bow],
		
 # Church Centric Weapons
	["black_key", "Black Keys", [("black_key", ixmesh_carry), ("black_key_inv", 0)], itp_no_blur|itp_type_thrown|itp_primary|itp_next_item_as_melee, itcf_throw_knife, 193, weight(2.5)|difficulty(0)|spd_rtng(110)|shoot_speed(24)|thrust_damage(25, cut)|max_ammo(13)|weapon_length(1),imodbits_thrown ],
	
	["black_key_melee", "Black Keys", [("black_key_melee", 0)], itp_no_blur|itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary, itc_scimitar|itcf_carry_dagger_front_left, 18, weight(0.5)|difficulty(0)|spd_rtng(110)|weapon_length(60)|swing_damage(32, cut), imodbits_sword],
 
 # Combat Spells
	["gandr", "Gandr", [("gandr",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
	
	["firebolt", "Fire Bolt", [("fire_bolt",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
	
	["windbolt", "Wind Bolt", [("wind_bolt",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
	
	["waterbolt", "Water Bolt", [("water_bolt",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
	
	["icebolt", "Ice Bolt", [("ice_bolt",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
	
	["earthbolt", "Earth Bolt", [("earth_bolt",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
	
	["fireaoe", "Fire aoe", [("fire_bolt",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
	
	["windaoe", "Wind aoe", [("wasp",0), ("wind_bolt", ixmesh_inventory)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag,
		[(ti_on_missile_hit, [
				(set_fixed_point_multiplier, 100),
	
				(particle_system_burst, "psys_wasp_burst", pos1, 10),
		]),
		],
		],
	
	["wateraoe", "Water aoe", [("water_bolt",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
	
	["earthaoe", "Earth aoe", [("earth_bolt",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(0, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag,
			[(ti_on_missile_hit, [
				(set_fixed_point_multiplier, 100), 							# Will keep math consistant

				(try_for_agents, ":agents", pos1, 150),
					(agent_is_human, ":agents"),
					(agent_is_alive, ":agents"),
					
					(agent_get_slot, ":petrified", ":agents", fate_agent_petrified),
					(val_add, ":petrified", 2),
					(agent_set_slot, ":agents", fate_agent_petrified, ":petrified"),
					#(agent_set_animation, ":agents", "anim_petrify_1"),
				(try_end),
				]),
			],
			],
	["voidbolt", "Void Bolt", [("void_bolt",0)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(85, blunt)|max_ammo(18)|weapon_length(8), imodbit_large_bag],
	
	["wasp", "Wasps", [("wasp",0), ("wind_bolt", ixmesh_inventory)], itp_type_thrown|itp_ignore_gravity|itp_ignore_friction|itp_primary, itcf_throw_stone, 1, weight(4)|difficulty(0)|spd_rtng(97)|shoot_speed(30)| thrust_damage(30, pierce)|max_ammo(18)|weapon_length(8), imodbit_large_bag,
		[(ti_on_missile_hit, [
				(set_fixed_point_multiplier, 100),
	
				(particle_system_burst, "psys_wasp_burst", pos1, 10),
		]),
		],
		],
	
	# When hit with this, check against a seperate poison resistance
	["poison", "Poison Damage", [("snm_nagimaki",0),("snm_nagimaki_scabbard",ixmesh_carry)], itp_type_two_handed_wpn| itp_primary, itc_poleaxe|itcf_carry_spear|itcf_show_holster_when_drawn, 679 , weight(2.0)|difficulty(9)|spd_rtng(108) | weapon_length(115)|swing_damage(100, blunt) | thrust_damage(100,  blunt),imodbits_sword ],
	
	# When hit with this, check the attackers NP level
	["np_damage", "Noble Phantasm Damage", [("snm_nagimaki",0),("snm_nagimaki_scabbard",ixmesh_carry)], itp_type_two_handed_wpn| itp_primary, itc_poleaxe|itcf_carry_spear|itcf_show_holster_when_drawn, 679 , weight(2.0)|difficulty(9)|spd_rtng(108) | weapon_length(115)|swing_damage(100, blunt) | thrust_damage(100,  blunt),imodbits_sword ],
		
	#Mystic Codes
	
	#Heal Cast, Minor
	#Heal Cast, Major
	#Heal Cast, Perfect
	
	#Projection Magic: Sword
	#Projection Magic: Spear
	#Projection Magic: Shield
	
	#Reinforcement Magic: Legs
	#Reinforcement Magic: Weapon
	#Reinforcement Magic: Skin
	#Reinforcement Magic: Eyes
	
	#Time Alter Magic, Slow Time
	#Time Alter Magic, Double Slow Time
	#Time Alter Magic, Triple Slow Time
	
	#Mana Increase
	#Major Mana Increase
	
 # #####################################################################
 # ####### Servant Legend Books ########################################
 # #####################################################################
 
	["book_celtic_legends","Ulster Cycle", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Fergus, Cu, 
	
	["book_arthurian_legends","Historia Regum Britanniae", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Arthur, Richard, Lancelot
	
	["book_greek_history","Metamorphoses", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Medea, Achilles, Iskandar, Hector 
	
	["book_greek_legends","Mythology of Ancient Greece", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Heracles, Medusa, Atalanta, Asterios, 
	
	["book_roman_history","De vita Caesarum", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Nero, Spartacus
	
	["book_swedish_legends","Niflunga Saga", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Siegfried, Brynhildr
	
	["book_irish_legends","Fenian Cycle", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Fionn Mac Cool, Diarmund
	
	["book_french_legends","Chanson de Geste", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Astolfo, Gilles de Rais
	
	["book_biblical_legends","Acta Sanctorum", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Saint George, 
	
	["book_assassin_legends","Hashashin: Legends and Tales", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Assassins
	
	["book_modern_legends","Modern Myth: Folklore and Heroes", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Frankenstein, William Shakespeare, Hohenheim
	
	["book_ancient_legends","Ancient Myth, from Oral History to Legend", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Gilgamesh
	
	["book_philosophy_history", "Philosophy, Ethics and the Thinkers throughout History", [("book_open",0)], itp_type_book, 0, 3500,weight(2)|abundance(0),imodbits_none], # Avicebron
	  
	#Clothes
		#Casual Clothes
			#Shiro Style Baseball Tee
	["baseball_shirt", "Baseball Shirt", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
			#Tee and Jeans
	["tee_shirt", "Tee Shirt", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
			#Button-up and Jeans
	["casual_shirt", "Button-up Shirt", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
			#Office Worker Clothes
	["office_shirt", "Work Shirt", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
			#School Uniforms
				# Fuyuki High School
	["fuyuki_uniform", "Fuyuki High Uniform", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	["female_fuyuki_uniform", "Fuyuki High Uniform", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
				# Moon Cell School
	["moon_cell_uniform", "Tsukumihara Academy Uniform", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	["female_moon_cell_uniform", "Tsukumihara Academy Uniform", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
				# Teacher Suit
	["casual_suit_grey", "Cheap Suit", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["casual_suit_brown", "Cheap Suit", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["casual_suit_navy", "Cheap Suit", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
			# Assassin Clothing
				# Black Suits
	["black_suit", "Black Dress Suit", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
				# Black Leather Gloves
	["black_leather_gloves","Leather Gloves", [("leather_gloves_L",0)], itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],
	
	#Shoes, Boots, Footwear
		#Sandals, Geta
	["sandal_geta", "Geta Sandals", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_civilian|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Sandals, Clogs
	["sandal_birks", "Clog Suede Sandals", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_civilian|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Sandals, Slides
	["sandal_athletic", "Athletic Sandals", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_civilian|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Sandals, Thongs
	["sandal_thongs", "Flip-Flops", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_civilian|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
	
		#School, Inside Shoes
	["school_shoes", "School Shoes", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_civilian|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Sneakers, Converse
	["sneaker_hitop", "Hi-top Sneakers", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Sneakers, Runners
	["sneaker_running", "Running Shoes", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Sneakers, Skate
	["sneaker_skate", "Skate Shoes", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Sneakers, Knit
	["sneaker_knit", "Knit Sneakers", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		
		#Boots, Docs
	["boot_combat", "Combat Boots", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Boots, Timbs
	["boot_urban", "Wheat Work Boots", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Boots, Dress
	["boot_dress", "Dress Boots", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Boots, Dress
	["boot_dress_monkstrap", "Dress Boots", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Boots, Rain
	["boot_rain", "Rain Boots", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		
		#Dress, Cheap
	["dress_cheap", "Cheap Dress Shoes", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Dress, Oxford
	["dress_oxford", "Dress Shoes", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Dress, Monkstrap
	["dress_monk", "Monkstraps", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
		#Dress, Wingtip
	["dress_wingtip", "Wingtips", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature,0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(0)|leg_armor(3)|difficulty(0), imodbits_cloth],
	
	#Hats, Beanies
		#Beanie
	["beanie", "Beanie", [("felt_hat_a_new",0)],  itp_type_head_armor |itp_civilian,0, 4 , weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
		#Beanie with Pouf
	["ski_hat", "Ski Beanie", [("felt_hat_a_new",0)],  itp_type_head_armor |itp_civilian,0, 4 , weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
		#Baseball Cap
	["baseball_hat", "Baseball Cap", [("felt_hat_a_new",0)],  itp_type_head_armor |itp_civilian,0, 4 , weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
		#Large Floppy dumb Hipster Shit
	["widebrimmed_hat", "Wide-Brimmed Hat", [("felt_hat_a_new",0)], itp_type_head_armor |itp_civilian,0, 4 , weight(1)|abundance(100)|head_armor(8)|body_armor(0)|leg_armor(0)|difficulty(0) ,imodbits_cloth ],
	
	# Gloves
	
	["driving_gloves_black","Leather Gloves", [("leather_gloves_L",0)], itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],
	
	["driving_gloves_brown","Leather Gloves", [("leather_gloves_L",0)], itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],
	
	["white_gloves","Leather Gloves", [("leather_gloves_L",0)], itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],
	
	["mittens","Leather Gloves", [("leather_gloves_L",0)], itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],
	
	["nordic_mittens","Leather Gloves", [("leather_gloves_L",0)], itp_type_hand_armor,0, 90, weight(0.25)|abundance(120)|body_armor(2)|difficulty(0),imodbits_cloth],
 #Holy Church Gear
	#Raiments
	["priest_raiment_black", "Priest Raiment", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["priest_raiment_red", "Priest Raiment", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["priest_raiment_white", "Priest Raiment", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["executor_raiment_black", "Executor Raiment", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["executor_raiment_red", "Executor Raiment", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
 #Clock Tower Gear
	# Mage's Gear, Old Style
	["mage_outfit_old", "Mage Garments", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["mage_outfit_eastern", "Mage Garments", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["mage_outfit_ragged", "Mage Garments", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["mage_outfit_noble", "Mage Garments", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["mage_outfit_wealthy_red", "Mage Garments", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["mage_outfit_wealthy_black", "Mage Garments", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["mage_outfit_modern", "Mage Garments", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["mage_outfit_highfashion", "Mage Garments", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],

	# Wire Horse Familiar
	["wire_familiar_horse","Wire Construct - Mount", [("courser",0)], itp_unique|itp_type_horse, 0, 600,abundance(0)|body_armor(255)|hit_points(255)|difficulty(2)|horse_speed(65)|horse_maneuver(47)|horse_charge(10)|horse_scale(102),imodbits_horse_basic|imodbit_champion],
	
	["kimono_modern", "Kimono", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["kimono_traditional", "Kimono", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
	["kimono_dress", "Kimono", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs|itp_civilian, 0, 3, weight(1)|abundance(100)|head_armor(0)|body_armor(5)|leg_armor(0)|difficulty(0), imodbits_cloth ],
	
 #	Summons Items
	["sakura_shadow", "Kimono", [("shadow",0)], itp_type_body_armor|itp_covers_legs|itp_covers_head, 0, 3, weight(1)|abundance(100)|head_armor(10)|body_armor(10)|leg_armor(10)|difficulty(0), imodbits_cloth ],
	["hollow_body", "Kimono", [("shadow_invis",0)], itp_type_body_armor|itp_covers_legs|itp_covers_head, 0, 3, weight(1)|abundance(100)|head_armor(10)|body_armor(10)|leg_armor(10)|difficulty(0), imodbits_cloth ],
	["sakura_shadow_h", "Kimono", [("none",0)], itp_type_hand_armor, 0, 3, weight(1)|abundance(100)|head_armor(1)|body_armor(1)|leg_armor(1)|difficulty(0), imodbits_cloth ],
	["sakura_shadow_l", "Kimono", [("none",0)], itp_type_foot_armor, 0, 3, weight(1)|abundance(100)|head_armor(1)|body_armor(1)|leg_armor(1)|difficulty(0), imodbits_cloth ],
	
	["eldrich_eyes", "Kimono", [("eye_horror",0)], itp_type_body_armor|itp_covers_legs|itp_covers_head, 0, 3, weight(1)|abundance(100)|head_armor(10)|body_armor(10)|leg_armor(10)|difficulty(0), imodbits_cloth ],
	["eldrich_tendrils", "Kimono", [("tendril_horror",0)], itp_type_body_armor|itp_covers_legs|itp_covers_head, 0, 3, weight(1)|abundance(100)|head_armor(10)|body_armor(10)|leg_armor(10)|difficulty(0), imodbits_cloth ],
 # Fate Food Items
	## Ingredients
	### Proteins	
	# Salmon Filet
	["protein_salmon","Salmon", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(4),imodbits_none],
	# Mackerel Pike
	["protein_pike","Mackerel Pike", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(4),imodbits_none],
	# Yellow-fin Tuna
	["protein_yellowfin","Yellowfin Tuna", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(4),imodbits_none],
	# Bluefin Tuna
	["protein_bluefin","Bluefin Tuna", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(3),imodbits_none],
	# King Crab Legs
	["protein_crab","King Crab Legs", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Lobster
	["protein_lobster","Lobster", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(3),imodbits_none],
	# Shrimp
	["protein_shrimp","Raw Shrimp", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(8),imodbits_none],
	# Squid
	["protein_squid","Raw Squid", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(2),imodbits_none],
	# Chicken
	["protein_chicken","Chicken", [("chicken",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Beef
	["protein_beef","Beef", [("raw_meat",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# High Quality Beef
	["protein_quality_beef","Wagyu Beef", [("raw_meat",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(5)|max_ammo(50),imodbits_none],
	# Pork
	["protein_pork","Pork", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Tofu
	["protein_tofu","Tofu", [("smoked_fish",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Clams
	["protein_clam","Clams", [("smoked_fish",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(12),imodbits_none],
	### Vegetables
	# Carrots
	["veggie_carrot","Carrots", [("cabbage",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Taro
	["veggie_taro","Raw Taro", [("cabbage",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Spinach
	["veggie_spinach","Spinach", [("cabbage",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Mixed Greens
	["veggie_mixed_green","Mixed Greens", [("cabbage",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Mustard Greens
	["veggie_mustard_green","Mustard Greens", [("cabbage",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Potatoes
	["veggie_potato","Potatoes", [("cabbage",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Leeks
	["veggie_leek","Leeks", [("cabbage",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Napa Cabbage
	["veggie_cabbage","Napa Cabbage", [("cabbage",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Bamboo Shoots
	["veggie_bamboo","Bamboo Shoots", [("cabbage",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(4),imodbits_none],
	# Onions
	["veggie_onion","Onions", [("cabbage",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(4),imodbits_none],
	### Starches and Carbs
	# Noodles
	["carb_noodles","Ramen Noodles", [("bread_a",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Cheap Instant Noodles
	["carb_cheap_noodles","Cheap Instant Noodles", [("ramen_cheap",0), ("ramen_cheap_inv", ixmesh_inventory)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# High-Quality Noodles
	["carb_quality_noodles","Quality Ramen Noodles", [("ramen_quality",0), ("ramen_quality_inv", ixmesh_inventory)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Rice
	["carb_rice","Rice", [("bread_a",0)], itp_type_goods|itp_consumable, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Bread
	["carb_bread_store","Bread", [("bread_a",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Breadcrumbs - Made by Player (either bread or homemade bread)
	["carb_breadcrumbs","Breadcrumbs", [("bread_a",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Tempura
	["carb_tempura","Tempura", [("bread_a",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Batter - Made by player (Flour, Eggs, Beer sometimes)
	["carb_batter","Batter", [("bread_a",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	### Spices and Condiments
	# Butter
	["ingredient_butter","Butter", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# Flour
	["ingredient_flour","Flour", [("salt_sack",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# Eggs
	["ingredient_eggs","Eggs", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(12),imodbits_none],
	# Salt, Pepper, Spices
	["ingredient_spicerack","Spice Rack", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(100),imodbits_none],
	# Cooking Sake
	["ingredient_sake","Cooking Sake", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(25),imodbits_none],
	# Ginger
	["ingredient_ginger","Ginger", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(5),imodbits_none],
	# Mayonnaise - Can also be cooked, eggwhites and vinegar
	["ingredient_mayo","Mayonnaise", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# Soy Sauce
	["ingredient_soy","Soy Sauce", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(30),imodbits_none],
	# Olive Oil
	["ingredient_olive_oil", "Olive Oil", [("oil",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# vinegar
	["ingredient_vinegar", "Vinegar", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# milk
	["ingredient_milk", "Milk", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(30),imodbits_none],
	### Instant Items, still require cooking, but are the only ingredient
	# Instant Coffee
	["instant_coffee","Instant Coffee", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# CUP NOODLES, MOTHERFUCKER
	["instant_noodles","Cup Noodles (R)", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Instant Ramen
	["instant_ramen","Ramen Block", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Instant Ramen, Fancier
	["instant_quality_ramen","High-Quality Ramen Block", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Breakfast Cereal
	["instant_cereal","Box of Cereal", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# Oatmeal
	["instant_oatmeal","Oatmeal", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(20),imodbits_none],
	## Pre-Made, Either Restaurant or Convenient Stores
	# Spicy Mapo Tofu
	["mapo_tofu","Spicy Mapo Tofu", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Bento Box - Beef
	["bento_beef","Beef Bento Box", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Bento Box - Pork
	["bento_pork","Pork Bento Box", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Box Sushi
	["store_sushi","Sushi Bento", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Bento Box - Pork
	["steak","Beef Filete", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Bento Box - Pork
	["fried_chicken","Buttermilk Chicken", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Bento Box - Pork
	["veggie_medley","Vegetable Medley", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Sandwich - Eggs
	["egg_sandwich","Wrapped Egg Sandwich", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Sandwich - Turkey
	["turkey_sandwich","Wrapped Turkey Sandwich", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Ramune Soda
	["soda_ramune","Ramune Soda", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Coca Cola
	["soda_coke","Coba Cola", [("soda_01",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Pepsi
	["soda_pepsi","Pep'd", [("soda_02",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Fanta
	["soda_fanta","Fantasma", [("soda_04",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Canned coffee
	["canned_coffee","Paws Canned Coffee", [("soda_03",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Beer
	["alcohol_beer","Kyrin Beer", [("soda_05",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Sake
	["alcohol_sake","Sake", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(10),imodbits_none],
	# Whiskey, Japanese
	["alcohol_jap_whiskey","Japanese Whiskey", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# Whiskey, American
	["alcohol_usa_whiskey","American Whiskey", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# Whiskey, Scotch
	["alocohol_scotch","10 Year Scotch", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# Whiskey, Bourbon
	["alcohol_bourbon","Kentucky Bourbon", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(15),imodbits_none],
	# Latte
	["coffee_latte","Latte", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Cappuccino
	["coffee_cappuccino","Cappuccino", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Espresso
	["coffee_espresso","Espresso", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Drip Coffee
	["coffee_drip","Drip Coffee", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	## Cooked Instant Items
	# Instant Coffee
	["cooked_instant_coffee","Bitter Cup of Coffee", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# CUP NOODLES, MOTHERFUCKER
	["cooked_instant_noodles","Steaming Cup Noodles (R)", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Instant Ramen
	["cooked_instant_ramen","Instant Ramen", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Instant Ramen, Fancier
	["cooked_instant_quality_ramen","High-Quality Instant Ramen", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Breakfast Cereal
	["cooked_instant_cereal","Bowl of Breakfast Cereal", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Oatmeal
	["cooked_instant_oatmeal","Bowl of Instant Oatmeal", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	## Cooking System Items
	# Fancy Ramen - High Quality Beef, Shrimp, Pork, Chicken, Egg, Noodle
	["cooked_homemade_ramen","Homemade Ramen", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Salad with Protein - Chicken, Beef, Shrimp or Tuna, Green Leafy Veggie, Vinegar
	["cooked_salad","Fresh Salad", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Sushi - Salmon, Crab, Shrimp, Yellowfin or Blue Fin, Rice
	["cooked_sushi","Handmade Sushi", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Fried Rice - Rice, oil, carrots
	["cooked_fried_rice","Steaming Vegetable Fried Rice", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Fried Rice with Protein - rice and oil and carrots and Chicken, Beef, Pork, or Shrimp
	["cooked_meat_fried_rice","Steaming Fried Rice", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Steamed Rice - Rice and Butter
	["cooked_steamed_rice","Steamed Rice", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Steamed Rice - Rice and Butter and Chicken, Beef, Pork, or Shrimp and a veggie
	["cooked_meat_steamed_rice","Steamed Rice with Meat", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Homemade Bread - Flour, Eggs, Milk
	["cooked_bread","Homemade Bread", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Sandwich - Any protein except Squid, bread, and a green leaf veggie
	["cooked_sandwich","Sliced Sandwich", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Meatloaf - Beef, Breadcrumbs, Eggs
	["cooked_meatloaf","Meatloaf with Gravy", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Pancakes - Flour, Eggs, Milk
	["cooked_pancakes","Fluffy Pancakes", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Scrambled Eggs - Eggs
	["cooked_eggs","Scrambled Eggs", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Tempura Fried Proteins - Shrimp, Squid, Chicken
	["cooked_tempura","Tempura Fried Proteins", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Toshikoshi Soba - Episode 1
	["cooked_soba","Toshikoshi Soba", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Salmon and Mushrooms Baked In Foil - ep2
	["cooked_salmon_mushroom","Roasted Salmon with Mushrooms", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Spring Chirashizushi - ep 3
	["cooked_chirashizushi","Chirashizushi", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Spring Greens and Bacon Sandwich - ep 4
	["cooked_blt","Pressed Spring Green and Bacon Sandwich", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Bamboo Shoot Gratin -5 
	["cooked_bamboo_shoot","Tinder Bamboo Shoot Gratin", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Hamburg Steak- 6
	["cooked_humburger","German-style Hamburg Steak", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Refreshing and Easy to Eat Chilled Ochazuke - 7 
	["cooked_ochazuke","Crisp, Refreshing Ochazuke", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Tohsaka's Gomoku Fried Rice - 8
	["cooked_chinese_fried_rice","Chinese Style Fried Rice", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Taro Simmered in Broth - 9
	["cooked_taro","Taro Simmered in Broth", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Karaage, Fried Chicken - 10 # Basically Chicken Nuggeys
	["cooked_fried_chicken","Karaage Fried Chicken", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# The Specially Fluffy Omurice - 11
	["cooked_omurice","Fluffy, Omurice with Tomato Sauce", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Roast Beef Made Only With Frying Pan - 12
	["cooked_roat_beef","Single-pan Roast Beef", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	# Hot, Hot, Hot Pot - 13
	["cooked_hot_pot","Familiar Hot Pot", [("butter_pot",0)], itp_type_goods|itp_consumable|itp_food, 0, 65,weight(15)|abundance(0)|food_quality(50)|max_ammo(1),imodbits_none],
	
	["vats_ammo", "9mm Pistol Cartridges", [("cartridge_a", ixmesh_inventory), ("mystic_missile",ixmesh_flying_ammo), ("bolt_bag_c", ixmesh_carry)], itp_type_bullets|itp_ignore_gravity|itp_ignore_friction, 0, 41, weight(2.25)| abundance(90)| weapon_length(3)|thrust_damage(15, pierce)| max_ammo (35), imodbits_missile, [
			(ti_on_missile_hit, [
				(store_trigger_param_1, ":shooter"),
				(store_trigger_param_2, ":struck"),
				(agent_get_position, pos10, ":shooter"),
				(get_distance_between_positions_in_meters, ":distance", pos1, pos10),
				(call_script, "script_bullet_collision_particle", ":struck", ":distance"),

			])
		]],
		
	["cigarette", "Cigarette", [("cigarette",0), ("cigarette_pack_inv", ixmesh_inventory)], itp_type_one_handed_wpn|itp_unique|itp_primary|itp_secondary, 0, 18, weight(0.5)|difficulty(0)|spd_rtng(110)|weapon_length(10)|swing_damage(0, cut)|thrust_damage(0, pierce),imodbits_sword, 
		[
			(ti_on_init_item, [
			(set_position_delta, 25, 0, 0),
			#(particle_system_add_new, "psys_torch_fire"),
			(particle_system_add_new, "psys_gas_leak"),
			])
		]
		],
		
	["dragon_smoke","Dragon Smoke Cigarettes", [("dragon_smoke",0), ("dragon_smoke_inv", ixmesh_inventory)], itp_type_goods|itp_consumable, 0, 65, weight(0.25)|abundance(0)|food_quality(100)|max_ammo(12),imodbits_none],
	
	["morley","Morley Cigarettes", [("morley_pack",0), ("morley_inv", ixmesh_inventory)], itp_type_goods|itp_consumable, 0, 65, weight(0.35)|abundance(0)|food_quality(70)|max_ammo(20),imodbits_none],
	
	["skeleton_body", "Kimono", [("barf_skeleton",0)], itp_type_body_armor|itp_covers_legs|itp_covers_head, 0, 3, weight(1)|abundance(100)|head_armor(10)|body_armor(10)|leg_armor(10)|difficulty(0), imodbits_cloth ],
	["skeleton_hand", "Kimono", [("barf_skeleton_handL",0)], itp_type_hand_armor, 0, 3, weight(1)|abundance(100)|head_armor(1)|body_armor(1)|leg_armor(1)|difficulty(0), imodbits_cloth ],
	["skeleton_foot", "Kimono", [("barf_skeleton_calf_l",0)], itp_type_foot_armor, 0, 3, weight(1)|abundance(100)|head_armor(1)|body_armor(1)|leg_armor(1)|difficulty(0), imodbits_cloth ],
	["skeleton_head", "Kimono", [("barf_skull",0)], itp_type_head_armor, 0, 3, weight(1)|abundance(100)|head_armor(1)|body_armor(1)|leg_armor(1)|difficulty(0), imodbits_cloth ],
	
	["laser_rifle", "Secret Agent X-9's Moonrifle", [("flintlock_pistol",0)], itp_type_musket|itp_primary|itp_two_handed, itc_npc_rifle, 230 , weapon_length(125)|weight(1.5)|difficulty(0)|spd_rtng(75)| shoot_speed(50)|thrust_damage(25, pierce)|max_ammo(100)|accuracy(115), imodbits_none, []],
	
	["laser_rounds", "Laser Rounds", [("cartridge_a", ixmesh_inventory), ("mystic_missile",ixmesh_flying_ammo), ("bolt_bag_c", ixmesh_carry)], itp_type_bullets|itp_ignore_gravity|itp_ignore_friction, 0, 41, weight(2.25)| abundance(90)| weapon_length(3)|thrust_damage(15, pierce)| max_ammo (350), imodbits_missile, []],
	
	["items_end", "Items End", [("shield_round_a",0)], 0, 0, 1, 0, 0],
]
