# -*- coding: cp1254 -*-
from header_common import *
from header_operations import *
from module_constants import *
from header_parties import *
from header_skills import *
from header_mission_templates import *
from header_items import *
from header_triggers import *
from header_terrain_types import *
from header_music import *
from header_map_icons import *
from header_presentations import *
from ID_animations import *


####################################################################################################################
# scripts is a list of script records.
# Each script record contns the following two fields:
# 1) Script id: The prefix "script_" will be inserted when referencing scripts.
# 2) Operation block: This must be a valid operation block. See header_operations.py for reference.
####################################################################################################################

# My memory isn't exactly the best, so instead of remember what rank is what integer I made this

rank_ex = 0
rank_a	= 1
rank_b	= 2
rank_c	= 3
rank_d	= 4
rank_e	= 5
rank_f	= 6
rank_na	= 7


fate_scripts = [
# ################################################################################
# ### Fate/ Throne of Heroes SCRIPTS
# ################################################################################

# ##Game Start Scripts
#fate_assign_servant_variables
#This will assign slots, constants for each of the servants such as personality, motivation, etc
  ("fate_assign_servant_variables",
   [
	####### Assigns Individual and General Character Slots ################################################
	########## This only works for slot ranges  with identical length of Servants #########################
	(try_for_range, ":cur_troop", servants_begin, servants_end),
		(troop_set_slot, ":cur_troop", slot_fate_true_name_revealed, 0),
		(troop_set_slot, ":cur_troop", slot_fate_info_tier, 0),
		(troop_set_slot, ":cur_troop", slot_fate_noble_phantasm_used, 0),
		(troop_set_slot, ":cur_troop", slot_fate_permanent_injury, 0),
		(troop_set_slot, ":cur_troop", slot_fate_has_magic_armor, 0),
		(troop_set_slot, ":cur_troop",  slot_fate_classification, "str_classification_servant"),
		(troop_set_slot, ":cur_troop",  fate_magic_attribute, "str_dmg_type_normal"),
		(store_sub, ":relic", ":cur_troop", servants_begin),
		(assign, ":name", ":relic"),
		(assign, ":speech", ":relic"),
		(val_add, ":relic", servant_catalysts_begin), 
		(val_add, ":name", servant_true_names_begin),
		(val_add, ":speech", servant_summon_speech_begin),
		(item_set_slot, ":relic",  slot_fate_catalyst_servant, ":cur_troop"), #Assigns the current Servant to their respective Relic
		(troop_set_slot, ":cur_troop",  slot_fate_true_name, ":name"), #Assigns the Servant's True Name
		(troop_set_slot, ":cur_troop",  slot_fate_summon_speech, ":speech"), #Assigns Servant's customized Summon speech
	(try_end),
	
	# First 30 slots are the various magecraft categories, by default elemental categories are unlocked from go.
	(try_for_range, ":category", magecraft_cat_begin, magecraft_cat_end),
		(try_begin),
			(lt, ":category", 4),
			(troop_set_slot, "trp_fate_spell_array", ":category", 1),
		(else_try),
			(troop_set_slot, "trp_fate_spell_array", ":category", 0),
		(try_end),
	(try_end),
	# Assign all spells to level 0
	(try_for_range, ":spell", spells_begin, spells_end),
		(val_add, ":spell", 30),
		(val_sub, ":spell", spells_begin),
		(troop_set_slot, "trp_fate_spell_array", ":spell", 0),
	(try_end),
	

		
	#################### Assigns Individual Servant and Specific Master Slots #############################
	############# This sets the Alignment slot so must be run before Alignment based systems ##############
	#######################################################################################################
	(troop_set_slot, "$g_player_troop",  slot_fate_alignment, "str_alignment_neutral"),
	(troop_set_slot, "$g_player_troop",  slot_fate_height, 160),
	(troop_set_slot, "$g_player_troop",  slot_fate_weight, 70),
	(troop_set_slot, "$g_player_troop",  slot_fate_max_mana, 1000),
	(troop_set_slot, "$g_player_troop",  slot_fate_classification, "str_classification_master"),
	(troop_set_slot, "$g_player_troop",  fate_personality_type, "str_personality_generic"),
	(troop_set_slot, "$g_player_troop",  fate_parents_bg, "str_parent_bg_generic"),
	(troop_set_slot, "$g_player_troop",  fate_plyr_background, "str_player_bg_generic"),
	(troop_set_slot, "$g_player_troop",  fate_education, "str_player_edu_generic"),
	(troop_set_slot, "$g_player_troop",  fate_training, "str_player_train_generic"),
	
	(assign, "$g_grail_war", "str_war_type"),
		
	(try_for_range, ":cur_troop", servants_begin, servants_end),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_saber_generic"), #Test Saber
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_lawful_good"),	
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_saber"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 160),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 160),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, rank_d),
		(troop_set_slot, ":cur_troop",  fate_param_end, rank_d),
		(troop_set_slot, ":cur_troop",  fate_param_agi, rank_d),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, rank_d),
		(troop_set_slot, ":cur_troop",  fate_param_lck, rank_d),
		(troop_set_slot, ":cur_troop",  fate_param_npm, rank_d),
		
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_saber_01"), #King Arthur
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_saber"),
		(troop_set_slot, ":cur_troop",  fate_magic_attribute, "str_dmg_type_dragon"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 154),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 42),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, rank_b),			
		(troop_set_slot, ":cur_troop",  fate_param_end, rank_c),
		(troop_set_slot, ":cur_troop",  fate_param_agi, rank_c),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, rank_b),
		(troop_set_slot, ":cur_troop",  fate_param_lck, rank_b),
		(troop_set_slot, ":cur_troop",  fate_param_npm, rank_c),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, rank_a),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_riding"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, rank_b),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_mana_burst"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, rank_a),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_charisma"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, rank_b),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_instinct"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, rank_a),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_saber_01_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, rank_a),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),

		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_saber_01_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, rank_c),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 300),
		
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_saber_02"), #Nero Claudius
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_chaotic_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_saber"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 150),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 42),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, rank_d),
		(troop_set_slot, ":cur_troop",  fate_param_end, rank_d),
		(troop_set_slot, ":cur_troop",  fate_param_agi, rank_a),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, rank_b),
		(troop_set_slot, ":cur_troop",  fate_param_lck, rank_a),
		(troop_set_slot, ":cur_troop",  fate_param_npm, rank_b),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, rank_c),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_imperial_privilege"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, rank_ex),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_migraine"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, rank_b),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_saber_02_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 2300),

		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_saber_02_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 5),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 300),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_saber_03"), #Siegfried
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_chaotic_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_saber"),
		(troop_set_slot, ":cur_troop",  fate_magic_attribute, "str_dmg_type_dragon"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 190),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 80),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 4),
		(troop_set_slot, ":cur_troop",  fate_param_end, 4),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 5),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 6),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 8),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 4),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 9),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_riding"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_golden_rule"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 7),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_disengage"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 4),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_saber_03_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 2000),

		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_saber_03_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 5),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 500),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_03, "str_noble_phantasm_saber_03_03"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_03_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_03_cost, 300),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_saber_04"), #Fergus mac Roich
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_neutral"),
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_saber"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 184),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 90),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 4),
		(troop_set_slot, ":cur_troop",  fate_param_end, 4),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 5),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 8),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 6),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 3),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_riding"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_eye_of_the_mind_true"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_bravery"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_nature_of_a_rebellious_spirit"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 5),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_saber_04"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1000),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_saber_05"), #Richard Lionheart
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_neutral"),
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_saber"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 154),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 46),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 2),
		(troop_set_slot, ":cur_troop",  fate_param_end, 2),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 2),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 2),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 3),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 1),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_riding"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 2),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_all_kinds_of_talents"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 2),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_godspeed"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 2),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_lionheart"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 2),
		# (troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_military_tactics"),
		# (troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 3),
		# (troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_mental_pollution"),
		# (troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 6),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_saber_05_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 5),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1500),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_saber_05_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 2),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 900),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_lancer_generic"), #test Lancer
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_neutral"),
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_lancer"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 160),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 160),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 4),
		(troop_set_slot, ":cur_troop",  fate_param_end, 4),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 4),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 4),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 4),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 4),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 1),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_mana_burst"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 2),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_charisma"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 2),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_instinct"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 3),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_lancer_01"), #Cu Chulainn
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_lawful_neutral"),
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_lancer"),
		(troop_set_slot, ":cur_troop",  fate_magic_attribute, "str_dmg_type_divine"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 185),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 70),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 5),
		(troop_set_slot, ":cur_troop",  fate_param_end, 6),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 4),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 6),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 8),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 5),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 6),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_disengage"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 6),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_divinity"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_rune_magic"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_protection_from_arrows"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_battle_continuation"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 4),
		
		(store_troop_health, ":max_health", ":cur_troop", 1),
		(troop_set_slot, ":cur_troop",  slot_fate_cur_battle_continuations, ":max_health"),
		(troop_set_slot, ":cur_troop",  slot_fate_max_battle_continuations, ":max_health"),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_lancer_01_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 300),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_lancer_01_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 250),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_lancer_02"), #Diarmuid Ua Duibhne
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_lawful_neutral"),
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_lancer"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 184),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 85),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 5),
		(troop_set_slot, ":cur_troop",  fate_param_end, 6),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 3),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 7),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 8),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 5),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_eye_of_the_mind_true"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_love_spot"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 6),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_lancer_02_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 150),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_lancer_02_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 150),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_lancer_03"), #Fionn mac Cumhaill
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_neutral"),		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_lancer"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 181),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 63),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 4),
		(troop_set_slot, ":cur_troop",  fate_param_end, 5),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 3),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 6),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 6),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 4),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_clairvoyance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_magecraft"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 6),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_divinity"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 7),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_lancer_03_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 100),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_lancer_03_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 500),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_03, "str_noble_phantasm_lancer_03_03"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_03_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_03_cost, 1200),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_lancer_04"), #Hector
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_lawful_neutral"),
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_lancer"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 180),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 82),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),

		(troop_set_slot, ":cur_troop",  fate_param_str, 5),
		(troop_set_slot, ":cur_troop",  fate_param_end, 5),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 4),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 5),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 5),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 5),		
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_riding"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_disengage"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_military_tactics"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_proof_of_friendship"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 6),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_lancer_04_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 0),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_lancer_04_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 0),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_lancer_05"), #Brynhildr
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_neutral_good"),	
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_lancer"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 172),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 52),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 4),
		(troop_set_slot, ":cur_troop",  fate_param_end, 4),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 4),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 6),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 8),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 4),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_mana_burst_flames"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_primordial_rune"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 1),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_heros_bridesmaid"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 6),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_divinity"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 8),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_06, "str_skill_riding"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_06_level, 4),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_lancer_05_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 0),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_lancer_05_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 0),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_archer_generic"), #Test Archer
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_neutral"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_archer"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 160),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 160),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 4),
		(troop_set_slot, ":cur_troop",  fate_param_end, 4),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 4),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 4),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 4),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 4),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_independent_action"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 2),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_eye_of_the_mind_true"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 2),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_magecraft"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 3),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_clairvoyance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 3),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_archer_01"), #EMIYA
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_neutral"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_archer"),
		(troop_set_slot, ":cur_troop",  fate_magic_attribute, "str_dmg_type_fire"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 187),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 78),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 7),
		(troop_set_slot, ":cur_troop",  fate_param_end, 6),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 6),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 5),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 8),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 9),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 7),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_independent_action"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_eye_of_the_mind_true"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_magecraft"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 6),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_clairvoyance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 6),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_archer_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_archer_02"), #Gilgamesh
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_chaotic_good"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_archer"),
		(troop_set_slot, ":cur_troop",  fate_magic_attribute, "str_dmg_type_divine"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 182),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 68),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 5),
		(troop_set_slot, ":cur_troop",  fate_param_end, 5),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 5),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 4),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 4),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 1),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 6),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_independent_action"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_charisma"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 3),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_golden_rule"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_divinity"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 5),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_archer_02_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 3000),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_archer_02_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 120),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_03, "str_noble_phantasm_archer_02_03"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_03_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_03_cost, 5000),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_archer_03"), #Atalanta
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_neutral"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_archer"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 166),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 57),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 7),
		(troop_set_slot, ":cur_troop",  fate_param_end, 8),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 4),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 5),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 6),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 6),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 7),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_independent_action"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_crossing_arcadia"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_aesthetics_of_the_last_spurt"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 6),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_archer_03_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 700),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_archer_03_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 1200),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_archer_04"), #Robin Hood
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_neutral_good"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_archer"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 185),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 65),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 6),
		(troop_set_slot, ":cur_troop",  fate_param_end, 6),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 5),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 5),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 5),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 7),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 7),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_independent_action"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_subversive_activities"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_golden_rule"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 8),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_archer_04_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_archer_04_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 700),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_archer_05"), #Arash
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_chaotic_neutral"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_archer"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 185),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 75),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 5),
		(troop_set_slot, ":cur_troop",  fate_param_end, 4),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 4),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 8),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 7),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 4),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 6),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_independent_action"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 6),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_robust_health"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 1),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_bow_and_arrow_creation"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_clairvoyance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 4),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_archer_05"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_rider_generic"), #Test Rider
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_neutral"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_rider"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 160),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 160),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 4),
		(troop_set_slot, ":cur_troop",  fate_param_end, 4),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 4),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 4),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 4),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 4),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_riding"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 2),
		
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_rider_01"), #Medusa
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_chaotic_good"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_rider"),
		(troop_set_slot, ":cur_troop",  fate_magic_attribute, "str_dmg_type_demonic"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 172),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 57),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 6),
		(troop_set_slot, ":cur_troop",  fate_param_end, 8),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 5),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 5),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 7),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 3),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_riding"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_independent_action"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 6),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_divinity"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 9),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_mystic_eyes"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 3),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_06, "str_skill_monstrous_strength"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_06_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_07, "str_skill_independent_action"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_07_level, 6),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_rider_01_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_rider_01_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 200),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_03, "str_noble_phantasm_rider_01_03"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_03_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_03_cost, 1500),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_04, "str_noble_phantasm_rider_01_04"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_04_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_04_cost, 120),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_rider_02"), #Iskandar
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_neutral_good"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_rider"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 212),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 130),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 5),
		(troop_set_slot, ":cur_troop",  fate_param_end, 4),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 7),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 6),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 3),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 2),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_riding"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 7),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_military_tactics"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_divinity"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 6),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_charisma"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 4),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_rider_02_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 3500),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_rider_02_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 500),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_03, "str_noble_phantasm_rider_02_03"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_03_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_03_cost, 1000),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_rider_03"), #Achilles
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_neutral"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_rider"),
		(troop_set_slot, ":cur_troop",  fate_magic_attribute, "str_dmg_type_divine"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 185),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 97),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 4),
		(troop_set_slot, ":cur_troop",  fate_param_end, 4),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 3),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 6),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 7),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 3),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_riding"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 6),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_bravery"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 3),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_divinity"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 6),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_battle_continuation"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_06, "str_skill_affections_of_the_goddess"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_06_level, 5),
		
		(store_troop_health, ":max_health", ":cur_troop", 1),
		(troop_set_slot, ":cur_troop",  slot_fate_cur_battle_continuations, ":max_health"),
		(troop_set_slot, ":cur_troop",  slot_fate_max_battle_continuations, ":max_health"),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_rider_03_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_rider_03_03"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 200),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_03, "str_noble_phantasm_rider_03_04"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_03_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_03_cost, 1500),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_04, "str_noble_phantasm_rider_03_05"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_04_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_04_cost, 120),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_rider_04"), #Astolfo
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_neutral"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_rider"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 164),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 56),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 7),
		(troop_set_slot, ":cur_troop",  fate_param_end, 7),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 5),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 6),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 3),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 6),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_riding"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 7),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_evaporation_of_sanity"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 7),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_monstrous_strength"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 7),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_independent_action"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 5),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_rider_04_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_rider_04_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 200),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_03, "str_noble_phantasm_rider_04_03"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_03_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_03_cost, 1500),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_04, "str_noble_phantasm_rider_04_04"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_04_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_04_cost, 120),		
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_rider_05"), #Saint George
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_neutral"),
		(troop_set_slot, ":cur_troop",  fate_magic_attribute, "str_dmg_type_divine"),
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_rider"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 180),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 95),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 7),
		(troop_set_slot, ":cur_troop",  fate_param_end, 3),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 6),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 7),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 3),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 6),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_riding"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_battle_continuation"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_divinity"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 6),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_guardian_knight"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 3),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_06, "str_skill_soul_of_a_martyr"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_06_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_07, "str_skill_instinct"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_07_level, 6),
		
		(store_troop_health, ":max_health", ":cur_troop", 1),
		(troop_set_slot, ":cur_troop",  slot_fate_cur_battle_continuations, ":max_health"),
		(troop_set_slot, ":cur_troop",  slot_fate_max_battle_continuations, ":max_health"),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_rider_05_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_rider_05_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 200),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_03, "str_noble_phantasm_rider_05_03"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_03_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_03_cost, 1500),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_04, "str_noble_phantasm_rider_05_04"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_04_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_04_cost, 120),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_caster_generic"), #Test Caster
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_neutral"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_caster"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 160),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 160),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 9000),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 4),
		(troop_set_slot, ":cur_troop",  fate_param_end, 4),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 4),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 4),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 4),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 4),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_caster_01"), #Medea
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_chaotic_neutral"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_caster"),
		(troop_set_slot, ":cur_troop",  fate_magic_attribute, "str_dmg_type_dragon"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 163),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 51),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 9000),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 8),
		(troop_set_slot, ":cur_troop",  fate_param_end, 7),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 6),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 3),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 5),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 6),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_item_construction"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_territory_creation"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_highspeed_divine_words"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_golden_fleece"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 1),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_caster_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 500),
		
		(troop_set_slot, ":cur_troop",  slot_fate_max_minions, 6),
		
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_special_caster_01_01"),
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_special_caster_01_02"),
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_special_caster_01_03"),
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_special_caster_01_04"),
		
		# ("special_caster_01_01", "Flight"),
		# ("special_caster_01_02", "Beam"),
		# ("special_caster_01_03", "Threads of Fate"),
		# ("special_caster_01_04", "Dragon Tooth Warriors"),
		
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_caster_02"), #Gilles de Rais
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_chaotic_evil"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_caster"),
		(troop_set_slot, ":cur_troop",  fate_magic_attribute, "str_dmg_type_demonic"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 196),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 70),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 9000),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 7),
		(troop_set_slot, ":cur_troop",  fate_param_end, 8),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 7),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 6),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 8),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 3),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_item_construction"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 9),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_territory_creation"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_eye_for_art"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 9),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_mental_pollution"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 4),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_caster_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
		
		(troop_set_slot, ":cur_troop",  slot_fate_max_minions, 12),
		
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_special_caster_02_01"),
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_special_caster_02_02"),
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_special_caster_02_03"),
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_special_caster_02_04"),
		
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_caster_03"), #William Shakespeare
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_neutral"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_caster"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 180),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 75),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 9000),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 8),
		(troop_set_slot, ":cur_troop",  fate_param_end, 8),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 7),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 6),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 5),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 6),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_item_construction"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 9),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_territory_creation"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 6),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_enchant"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_selfpreservation"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 5),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_caster_03_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_caster_03_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 200),
		
		(troop_set_slot, ":cur_troop",  slot_fate_max_minions, 4),
		
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_special_caster_03_01"),
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_special_caster_03_02"),
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_special_caster_03_03"),
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_special_caster_03_04"),
		
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_caster_04"), #Avicebron
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_lawful_neutral"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_caster"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 161),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 52),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 9000),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 8),
		(troop_set_slot, ":cur_troop",  fate_param_end, 8),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 7),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 4),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 5),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 3),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_item_construction"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_territory_creation"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_numerology"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 5),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_caster_04"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
		
		(troop_set_slot, ":cur_troop",  slot_fate_max_minions, 10),
		
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_special_caster_04_01"),
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_special_caster_04_02"),
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_special_caster_04_03"),
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_special_caster_04_04"),
		
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_caster_05"), #Hohenheim
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_chaotic_good"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_caster"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 183),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 65),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 9000),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 7),
		(troop_set_slot, ":cur_troop",  fate_param_end, 8),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 6),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 4),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 5),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 3),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_item_construction"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 1),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_territory_creation"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_highspeed_incantation"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 4),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_caster_05"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
		
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_special_caster_05_01"),
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_special_caster_05_02"),
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_special_caster_05_03"),
		(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_special_caster_05_04"),
		
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_assassin_generic"), #Test Assassin
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_lawful_evil"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_assassin"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 160),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 160),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 4),
		(troop_set_slot, ":cur_troop",  fate_param_end, 4),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 4),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 4),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 4),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 4),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_presence_concealment"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_projectile_dagger"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 2),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_protection_from_wind"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 2),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_selfmodification"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 3),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_assassin_01"), #Cursed Arm
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_lawful_evil"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_assassin"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 215),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 65),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 5),
		(troop_set_slot, ":cur_troop",  fate_param_end, 6),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 4),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 6),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 8),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 6),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_presence_concealment"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_projectile_dagger"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_protection_from_wind"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_selfmodification"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 6),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_assassin_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_assassin_02"), #Hundred Faces
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_lawful_evil"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_assassin"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 152),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 56),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 6),
		(troop_set_slot, ":cur_troop",  fate_param_end, 7),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 4),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 6),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 8),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 5),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_presence_concealment"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_expert_of_many_specializations"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 3),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_librarian_of_stored_knowledge"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 6),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_assassin_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_assassin_03"), #Shadow Peeling
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_lawful_evil"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_assassin"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 160),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 160),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 6),
		(troop_set_slot, ":cur_troop",  fate_param_end, 6),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 3),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 5),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 5),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 4),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_presence_concealment"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 1),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_projectile_dagger"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 6),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_shadow_lantern"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 4),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_assassin_03"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_assassin_04"), #Hassan of Serenity
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_lawful_evil"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_assassin"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 161),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 42),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 7),
		(troop_set_slot, ":cur_troop",  fate_param_end, 7),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 3),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 6),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 4),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 6),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_presence_concealment"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_projectile_dagger"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 6),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_shapeshift"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 6),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_poison_res"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_independent_action"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 4),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_assassin_04"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_assassin_05"), #King Hassan-i-Sabbah
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_lawful_evil"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_assassin"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 220),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 160),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 5),
		(troop_set_slot, ":cur_troop",  fate_param_end, 4),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 5),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 8),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 8),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 4),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_presence_concealment"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_protection_of_the_faith"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 2),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_evening_bell"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 1),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_natural_body"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 6),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_uncrowned_martial_arts"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 1),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_06, "str_skill_battle_continuation"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_06_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_07, "str_skill_independent_action"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_07_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_08, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_08_level, 5),
		
		(store_troop_health, ":max_health", ":cur_troop", 1),
		(troop_set_slot, ":cur_troop",  slot_fate_cur_battle_continuations, ":max_health"),
		(troop_set_slot, ":cur_troop",  slot_fate_max_battle_continuations, ":max_health"),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_assassin_05"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_berserker_generic"), #Test Berserker
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_insane"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_berserker"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 160),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 160),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 4),
		(troop_set_slot, ":cur_troop",  fate_param_end, 4),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 4),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 4),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 4),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 4),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_mad_enhancement"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_bravery"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 3),
		
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_berserker_01"), #Heracles
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_insane"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_berserker"),
		(troop_set_slot, ":cur_troop",  fate_magic_attribute, "str_dmg_type_divine"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 253),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 311),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 3),
		(troop_set_slot, ":cur_troop",  fate_param_end, 4),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 4),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 4),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 5),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 4),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_mad_enhancement"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_battle_continuation"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_bravery"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 3),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_divinity"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05, "str_skill_eye_of_the_mind_false"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_05_level, 5),
		
		(store_troop_health, ":max_health", ":cur_troop", 1),
		(val_mul, ":max_health", 9),
		(troop_set_slot, ":cur_troop",  slot_fate_cur_battle_continuations, ":max_health"),
		(troop_set_slot, ":cur_troop",  slot_fate_max_battle_continuations, ":max_health"),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_berserker_01_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_berserker_01_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 200),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_berserker_02"), #Lancelot
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_insane"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_berserker"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 191),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 81),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 4),
		(troop_set_slot, ":cur_troop",  fate_param_end, 4),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 3),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 6),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 5),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 4),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_mad_enhancement"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 6),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_eternal_arms_mastery"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 3),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_protection_of_the_fairies"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04, "str_skill_magic_resistance"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_04_level, 8),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_berserker_02_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_berserker_02_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 200),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_03, "str_noble_phantasm_berserker_02_03"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_03_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_03_cost, 1500),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_berserker_03"), #Spartacus
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_neutral"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_berserker"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 221),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 165),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 4),
		(troop_set_slot, ":cur_troop",  fate_param_end, 1),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 7),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 8),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 7),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 6),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_mad_enhancement"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 1),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_honor_of_the_battered"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 5),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_berserker_03"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_berserker_04"), #Frankenstein's Monster
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_chaotic_neutral"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_berserker"),
		(troop_set_slot, ":cur_troop",  fate_magic_attribute, "str_dmg_type_wind"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 172),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 48),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 6),
		(troop_set_slot, ":cur_troop",  fate_param_end, 5),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 7),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 7),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 5),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 6),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_mad_enhancement"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 7),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_galvanism"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_hollow_lament_of_the_falsely_living"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 7),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_berserker_04_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_berserker_04_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 3),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 200),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_berserker_05"), #Asterios
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_chaotic_evil"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_berserker"),
		(troop_set_slot, ":cur_troop",  fate_magic_attribute, "str_dmg_type_earth"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 298),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 150),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 2),
		(troop_set_slot, ":cur_troop",  fate_param_end, 2),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 6),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 7),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 8),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 4),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_mad_enhancement"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_monstrous_strength"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_natural_monster"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 2),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_avyssos_of_labrys"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 6),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_berserker_05"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 5),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_avenger_01"), #Angra Manyu
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_chaotic_evil"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_avenger"),
		(troop_set_slot, ":cur_troop",  fate_magic_attribute, "str_dmg_type_demonic"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 298),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 150),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 2),
		(troop_set_slot, ":cur_troop",  fate_param_end, 2),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 6),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 7),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 8),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 4),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_mad_enhancement"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_monstrous_strength"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_natural_monster"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 2),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_avyssos_of_labrys"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 6),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_avenger_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 5),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
	(else_try),
		
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_ruler_01"), #Jeanne
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_lawful_good"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_ruler"),
		(troop_set_slot, ":cur_troop",  fate_magic_attribute, "str_dmg_type_holy"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 298),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 150),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 2),
		(troop_set_slot, ":cur_troop",  fate_param_end, 2),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 6),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 7),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 8),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 4),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_mad_enhancement"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_monstrous_strength"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_natural_monster"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 2),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_avyssos_of_labrys"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 6),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_ruler_01_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 5),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_ruler_01_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 5),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 1200),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_ruler_02"), #Shirou Amakusa
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_chaotic_evil"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_ruler"),
		(troop_set_slot, ":cur_troop",  fate_magic_attribute, "str_dmg_type_holy"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 298),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 150),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 2),
		(troop_set_slot, ":cur_troop",  fate_param_end, 2),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 6),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 7),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 8),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 4),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_mad_enhancement"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_monstrous_strength"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_natural_monster"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 2),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_avyssos_of_labrys"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 6),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_ruler_02_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 5),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_ruler_02_02"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 5),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 1200),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_02, "str_noble_phantasm_ruler_02_03"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_level, 5),
		(troop_set_slot, ":cur_troop",	fate_np_slot_02_cost, 1200),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_true_name, "str_servant_shielder_01"), #Mash
		(troop_set_slot, ":cur_troop",  slot_fate_alignment, "str_alignment_chaotic_evil"),
		
		(troop_set_slot, ":cur_troop",  slot_fate_servant_class, "str_class_shielder"),
		#(troop_set_slot, ":cur_troop",  slot_fate_master, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_height, 298),
		(troop_set_slot, ":cur_troop",  slot_fate_weight, 150),
		(troop_set_slot, ":cur_troop",  slot_fate_max_mana, 3500),
		
		(troop_set_slot, ":cur_troop",  fate_param_str, 2),
		(troop_set_slot, ":cur_troop",  fate_param_end, 2),
		(troop_set_slot, ":cur_troop",  fate_param_agi, 6),
		(troop_set_slot, ":cur_troop",  fate_param_mgc, 7),
		(troop_set_slot, ":cur_troop",  fate_param_lck, 8),
		(troop_set_slot, ":cur_troop",  fate_param_npm, 4),
		
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01, "str_skill_mad_enhancement"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_01_level, 5),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02, "str_skill_monstrous_strength"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_02_level, 4),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_natural_monster"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 2),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03, "str_skill_avyssos_of_labrys"),
		(troop_set_slot, ":cur_troop",  fate_skill_slot_03_level, 6),
		
		(troop_set_slot, ":cur_troop",	fate_np_slot_01, "str_noble_phantasm_shielder_01_01"),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_level, 5),
		(troop_set_slot, ":cur_troop",	fate_np_slot_01_cost, 1200),
	(try_end),

		################And Now for Masters (Only ones with canon alignments)

	(troop_set_slot, "trp_master_staynight_03",  slot_fate_alignment, "str_alignment_neutral_good"),

	
	###### Assigns Alignment Based Slots ##########################################################
	
	(try_for_range, ":cur_troop", servants_begin, servants_end),
		(troop_slot_eq, ":cur_troop", slot_fate_alignment, "str_alignment_lawful_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_greet, "str_lawful_good_greet"),
		(troop_set_slot, ":cur_troop",  slot_fate_reveal_greet, "str_lawful_good_reveal_greet"),
		(troop_set_slot, ":cur_troop",  slot_fate_initial_greet, "str_lawful_good_initial_greet"),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_alignment, "str_alignment_lawful_neutral"),
		(troop_set_slot, ":cur_troop",  slot_fate_greet, "str_lawful_neutral_greet"),
		(troop_set_slot, ":cur_troop",  slot_fate_reveal_greet, "str_lawful_neutral_reveal_greet"),
		(troop_set_slot, ":cur_troop",  slot_fate_initial_greet, "str_lawful_neutral_initial_greet"),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_alignment, "str_alignment_lawful_evil"),
		(troop_set_slot, ":cur_troop",  slot_fate_greet, "str_lawful_evil_greet"),
		(troop_set_slot, ":cur_troop",  slot_fate_reveal_greet, "str_lawful_evil_reveal_greet"),
		(troop_set_slot, ":cur_troop",  slot_fate_initial_greet, "str_lawful_evil_initial_greet"),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_alignment, "str_alignment_neutral_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_greet, "str_neutral_good_greet"),
		(troop_set_slot, ":cur_troop",  slot_fate_reveal_greet, "str_neutral_good_reveal_greet"),
		(troop_set_slot, ":cur_troop",  slot_fate_initial_greet, "str_neutral_good_initial_greet"),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_alignment, "str_alignment_neutral"),
		(troop_set_slot, ":cur_troop",  slot_fate_greet, "str_true_neutral_greet"),
		(troop_set_slot, ":cur_troop",  slot_fate_reveal_greet, "str_true_neutral_reveal_greet"),
		(troop_set_slot, ":cur_troop",  slot_fate_initial_greet, "str_true_neutral_initial_greet"),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_alignment, "str_alignment_neutral_evil"),
		(troop_set_slot, ":cur_troop",  slot_fate_greet, "str_neutral_evil_greet"),
		(troop_set_slot, ":cur_troop",  slot_fate_reveal_greet, "str_neutral_evil_reveal_greet"),
		(troop_set_slot, ":cur_troop",  slot_fate_initial_greet, "str_neutral_evil_initial_greet"),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_alignment, "str_alignment_chaotic_good"),
		(troop_set_slot, ":cur_troop",  slot_fate_greet, "str_chaotic_good_greet"),
		(troop_set_slot, ":cur_troop",  slot_fate_reveal_greet, "str_chaotic_good_reveal_greet"),
		(troop_set_slot, ":cur_troop",  slot_fate_initial_greet, "str_chaotic_good_initial_greet"),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_alignment, "str_alignment_chaotic_neutral"),
		(troop_set_slot, ":cur_troop",  slot_fate_greet, "str_chaotic_neutral_greet"),
		(troop_set_slot, ":cur_troop",  slot_fate_reveal_greet, "str_chaotic_neutral_reveal_greet"),
		(troop_set_slot, ":cur_troop",  slot_fate_initial_greet, "str_chaotic_neutral_initial_greet"),
	(else_try),
		(troop_slot_eq, ":cur_troop", slot_fate_alignment, "str_alignment_chaotic_evil"),
		(troop_set_slot, ":cur_troop",  slot_fate_greet, "str_chaotic_evil_greet"),
		(troop_set_slot, ":cur_troop",  slot_fate_reveal_greet, "str_chaotic_evil_reveal_greet"),
		(troop_set_slot, ":cur_troop",  slot_fate_initial_greet, "str_chaotic_evil_initial_greet"),
	(else_try),
		(troop_set_slot, ":cur_troop",  slot_fate_greet, "str_insane_greet"), #If Servant doesn't have an assigned Alignment, they are now insane. Makes sense.
		(troop_set_slot, ":cur_troop",  slot_fate_reveal_greet, "str_insane_reveal_greet"),
		(troop_set_slot, ":cur_troop",  slot_fate_initial_greet, "str_insane_initial_greet"),
	(try_end),
	
	(try_for_range, ":cur_troop", magus_begin, magus_end),
		(troop_set_slot, ":cur_troop", slot_fate_max_mana, 1000),
		(troop_set_slot, ":cur_troop", slot_fate_classification, "str_classification_magus"),
		(troop_set_slot, ":cur_troop",  fate_magic_attribute, "str_dmg_type_normal"),
		(troop_set_slot, ":cur_troop",  fate_magic_origin, "str_dmg_type_normal"),
		
		(eq, ":cur_troop", "trp_master_staynight_01"),	# Emiya Shirou
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_reinforcement_enhance"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1_target_modifier, target_eyes),
		
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_projection_gradation"),
			
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_reinforcement_enhance"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_legs),
			
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_reinforcement_enhance"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_weapon),
			
			(troop_set_slot, ":cur_troop", fate_magic_projection_item, "itm_kanshou_melee"),
			
			(call_script, "script_define_personality", ":cur_troop", 3, 7, 4, 10, 8, 2, 8),
		(else_try),
			(eq, ":cur_troop", "trp_master_staynight_02"), # Rin Tohsaka
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2_target_modifier, target_legs),
			
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(call_script, "script_define_personality", ":cur_troop", 6, 6, 6, 8, 4, 7, 4),
		(else_try),
			(eq, ":cur_troop", "trp_master_staynight_03"), # Illyasviel
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1_strength_modifier, "str_magecraft_spell_enhance_double"),
			
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_wind_bolt"),
			
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_puppet_build"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_puppet_automate"),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 8),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		3),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		6),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		6),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	3),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 7),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	4),
		(else_try),
			(eq, ":cur_troop", "trp_master_staynight_04"), # sakura
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_special_dsakura_shadow"),
			
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_curse_cripple"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_special_dsakura_mine"),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		6),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		5),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_staynight_05"), 	# Shinji Matou
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_wind_unique4"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1_target_modifier, target_lookpos),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1_strength_modifier, "str_magecraft_spell_enhance_quarter"),
			
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_curse_blind"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2_target_modifier, target_lookpos),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2_strength_modifier, "str_magecraft_spell_enhance_quarter"),
			
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_curse_wound"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_lookpos),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_quarter"),
			
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_curse_cripple"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_strength_modifier, "str_magecraft_spell_enhance_quarter"),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 7),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		2),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	2),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 7),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	3),
		(else_try),
			(eq, ":cur_troop", "trp_master_staynight_06"),	# Souichirou Kuzuki
			
			(troop_set_slot, ":cur_troop", slot_fate_classification, "str_classification_nonmagus_master"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, 0),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, 0),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, 0),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, 0),
			(troop_set_slot, ":cur_troop", slot_fate_max_mana, 0),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 5),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		5),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		8),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	2),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 5),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	5),
		(else_try),
			(eq, ":cur_troop", "trp_master_staynight_07"), # Kirei Kotomine
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_sacrament_baptism"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_sacrament_wound"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_sacrament_purify"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_sacrament_miracle"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 7),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		2),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		8),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		8),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	2),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 9),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	2),
		(else_try),
			(eq, ":cur_troop", "trp_master_staynight_08"), # Zouken Matou
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_curse_wound"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_curse_blind"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_spatial_teleport"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_curse_cripple"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 5),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		8),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10), # 500 Years with the same goal? Yeah, for sure.
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	1),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 9),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	1),
		(else_try),
			(eq, ":cur_troop", "trp_master_staynight_09"), # Bazett Fraga McRemitz
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		8),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		8),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		9),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 8),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_zero_01"), # Kiritsugu
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_time_stagnate"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_reinforcement_enhance"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2_target_modifier, target_eyes),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_time_accel"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_time_haste"),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 7),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		2),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	4),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 9),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_zero_02"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 5),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		2),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		9),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	2),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 9),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	5),
		(else_try),
			(eq, ":cur_troop", "trp_master_zero_03"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 4),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		6),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		8),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		7),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	3),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 8),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	3),
		(else_try),
			(eq, ":cur_troop", "trp_master_zero_04"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		8),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		8),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		8),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 3),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	7),
		(else_try),
			(eq, ":cur_troop", "trp_master_zero_05"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 5),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		3),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		7),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	10),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 1),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_zero_06"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 9),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		0),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		5),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	0),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 5),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	1),
		(else_try),
			(eq, ":cur_troop", "trp_master_zero_07"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 5),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		6),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		8),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		6),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	4),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 8),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	5),
		(else_try),
			(eq, ":cur_troop", "trp_master_apocrypha_01"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_apocrypha_02"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_apocrypha_03"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_apocrypha_04"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_apocrypha_05"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_apocrypha_06"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_apocrypha_07"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_apocrypha_08"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_extra_01a"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		7),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		6),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		8),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	7),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 5),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	6),
		(else_try),
			(eq, ":cur_troop", "trp_master_extra_01b"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		6),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		6),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 5),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_extra_02"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_extra_03"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_extra_04"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_extra_05"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_extra_06"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_extra_07"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_extra_08"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_extra_09"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_extra_10"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_extra_11"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_fgo_male"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 5),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		5),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		5),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		5),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	5),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 5),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	5),
		(else_try),
			(eq, ":cur_troop", "trp_master_fgo_female"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 5),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		5),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		5),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		5),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	5),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 5),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	5),
		(else_try),
			(eq, ":cur_troop", "trp_master_proto_01"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_proto_02"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_proto_03"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_proto_04"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_proto_05"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 3),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		4),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		1),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		10),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	8),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 2),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	8),
		(else_try),
			(eq, ":cur_troop", "trp_master_proto_06"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 5),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		5),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		5),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		5),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	5),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 5),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	5),
		(else_try),
			(eq, ":cur_troop", "trp_master_proto_07"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 5),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		5),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		5),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		5),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	5),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 5),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	5),
		(else_try),
			(eq, ":cur_troop", "trp_master_proto_08"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":cur_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
			(troop_set_slot, ":cur_troop", slot_personality_aggression, 5),
			(troop_set_slot, ":cur_troop", slot_personality_honor, 		5),
			(troop_set_slot, ":cur_troop", slot_personality_logic, 		5),
			(troop_set_slot, ":cur_troop", slot_personality_drive, 		5),
			(troop_set_slot, ":cur_troop", slot_personality_empathy, 	5),
			(troop_set_slot, ":cur_troop", slot_personality_pragmatism, 5),
			(troop_set_slot, ":cur_troop", slot_personality_selfless, 	5),
	(try_end),
	
	(try_for_range, ":mage_troop", "trp_summoner", "trp_church_overseer"),
	
		(store_random_in_range, ":rand", 1, 5),
	
		(troop_set_slot, ":mage_troop", slot_fate_max_mana, 1000),
		(troop_set_slot, ":mage_troop", slot_fate_classification, "str_classification_magus"),
		(troop_set_slot, ":mage_troop",  fate_magic_attribute, "str_dmg_type_normal"),
		(troop_set_slot, ":mage_troop",  fate_magic_origin, "str_dmg_type_normal"),
		
		(try_begin),
			(eq, ":rand", 1),
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_1, "str_magecraft_reinforcement_enhance"),
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_1_target_modifier, target_eyes),
		
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_2, "str_magecraft_projection_gradation"),
			
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3, "str_magecraft_reinforcement_enhance"),
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_target_modifier, target_legs),
			
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_4, "str_magecraft_reinforcement_enhance"),
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_4_target_modifier, target_weapon),
			
			(troop_set_slot, ":mage_troop", fate_magic_projection_item, "itm_kanshou_melee"),
			
		(else_try),
			(eq, ":rand", 2),
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuzeihwaz"),
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_2_target_modifier, target_legs),
			
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_4, "str_magecraft_fire_unique2"),
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_4_target_modifier, target_lookpos),
			
		(else_try),
			(eq, ":rand", 3),
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_1, "str_magecraft_healing"),
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_1_target_modifier, target_self),
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_1_strength_modifier, "str_magecraft_spell_enhance_double"),
			
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_2, "str_magecraft_wind_bolt"),
			
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3, "str_magecraft_puppet_build"),
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_4, "str_magecraft_puppet_automate"),

		(else_try),
			(eq, ":rand", 4),
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
			
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_2, "str_magecraft_curse_cripple"),
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_target_modifier, target_lookpos),
			
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
			
			(troop_set_slot, ":mage_troop", slot_fate_spell_slot_4, "str_special_dsakura_mine"),

		(try_end),
	
	(try_end),
	
	(try_begin),
	
		(try_for_range, ":mage_troop", "trp_kingdom_1_magus", "trp_kingdom_1_magus_combat"),
			(troop_set_slot, ":mage_troop", slot_fate_max_mana, 1000),
			(troop_set_slot, ":mage_troop", slot_fate_classification, "str_classification_magus"),
			(troop_set_slot, ":mage_troop",  fate_magic_origin, "str_dmg_type_normal"),
			
			(try_begin),
				(eq, ":mage_troop", "trp_kingdom_1_magus"),	# Swadia, Fire Based
				(troop_set_slot, ":mage_troop",  fate_magic_attribute, "str_dmg_type_fire"),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_1, "str_magecraft_fire_bolt"),	# Shoots Fireball
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_1_strength_modifier, "str_magecraft_spell_enhance_half"), # Weaken + Make Cheaper
			
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_2, "str_magecraft_fire_unique2"),	# Ignite, causes target to burst into flames
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_2_strength_modifier, "str_magecraft_spell_enhance_quarter"), # Weaken + Make Cheaper
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3, "str_magecraft_necromancy_bomb"),	# Targets nearby corpse, 'splodes it
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_4, "str_magecraft_healing"),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_4_strength_modifier, "str_magecraft_spell_enhance_quarter"),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_4_target_modifier, target_self),				
			(else_try),
				(eq, ":mage_troop", "trp_kingdom_2_magus"),	# Vaegir, Golems
				(troop_set_slot, ":mage_troop",  fate_magic_attribute, "str_dmg_type_earth"),
				(troop_set_slot, ":mage_troop",  slot_fate_max_minions, 2),
				(troop_set_slot, ":mage_troop", fate_magic_projection_item, "itm_spiked_club"),
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_1, "str_magecraft_golemancy_create"),	# Spawn Golem
			
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_2, "str_magecraft_projection_gradation"),	# spawn item
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3, "str_magecraft_reinforcement_strengthen"),	# targets equipped item, makes stronger
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_target_modifier, target_weapon),
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_4, "str_magecraft_healing"),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_4_strength_modifier, "str_magecraft_spell_enhance_quarter"),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_4_target_modifier, target_self),
			(else_try),
				(eq, ":mage_troop", "trp_kingdom_3_magus"),	# Khergit, Wind and Curses
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_1, "str_magecraft_healing"),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_1_target_modifier, target_self),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_1_strength_modifier, "str_magecraft_spell_enhance_double"),
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_2, "str_magecraft_runes_gandr"),
				# (troop_set_slot, ":mage_troop", slot_fate_spell_slot_1_target_modifier, target_enemy),
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3, "str_magecraft_curse_wound"),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_target_modifier, target_enemy),
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_4, "str_magecraft_wind_buff"),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_4_target_modifier, target_self),

			(else_try),
				(eq, ":mage_troop", "trp_kingdom_4_magus"),	# Nord, Water and Runes
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_1, "str_magecraft_water_bolt"),
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_2, "str_magecraft_runes_ansuz"),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_target_modifier, target_lookpos),
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_target_modifier, target_self),
				# (troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_"),
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_4, "str_magecraft_water_trap"),
			(else_try),
				(eq, ":mage_troop", "trp_kingdom_5_magus"),	# Rhodok, Earth and Astromancy
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_1, "str_magecraft_earth_bolt"),
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_2, "str_magecraft_curse_cripple"),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_target_modifier, target_lookpos),
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_4, "str_special_dsakura_mine"),
			
			(else_try),
				(eq, ":mage_troop", "trp_kingdom_6_magus"),	# Sarranid, Gems and A
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_2, "str_magecraft_curse_cripple"),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_target_modifier, target_lookpos),
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_4, "str_special_dsakura_mine"),
			
			(else_try),
				(eq, ":mage_troop", "trp_kingdom_c_magus"),	# Unaffiliated, Random
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_1, "str_magecraft_runes_gandr"),
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_2, "str_magecraft_curse_cripple"),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_target_modifier, target_lookpos),
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3, "str_magecraft_healing"),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_target_modifier, target_servant),
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_3_strength_modifier, "str_magecraft_spell_enhance_double"),
				
				(troop_set_slot, ":mage_troop", slot_fate_spell_slot_4, "str_special_dsakura_mine"),

			(try_end),
		(try_end),
	
	(try_end),
	]),
	
	
	
	# ("fate_assign_grail_war_variables",
   # [
	# ####### Assigns Slots, Servants and Masters based on chosen Grail War ################
	# (str_store_string, ":grailwar", "$g_grail_war"),
	
	# (try_begin),
	
		# (eq, ":grailwar", "str_war_type")
		# #this is the Calradian grail war
		# #set total global servants to seven
		# #set masters to seven randoms
		# #match masters to ideal servants
		# #sort masters by strength and give them two weeks to summon servants
		# #if player attempts to summon existing servant allow failure states
		# #have it so that AI will not engage with Masters
		# #change factions to be more fate lore fitting -this is just in general, not just this mode-
	# (else_try),
		# (eq, ":grailwar", "str_war_type_fuyuki")
		# #set total global servants to seven
		# #set masters to seven randoms
		# #match masters to ideal servants
		# #instead of starting on world map, start in city with tab disable
	# (else_try),
		# (eq, ":grailwar", "str_war_type_tokyo")
		# #set total global servants to seven
		# #set masters to seven randoms
		# #match masters to ideal servants
		# #like fuyuki, no world map, third person exploration
	# (else_try),
		# (eq, ":grailwar", "str_war_type_great")
		# #set total global servants to two teams of seven -red, black-
		# #set masters to two groups of seven randoms
		# #match masters to ideal servants
	# (else_try),
		# (eq, ":grailwar", "str_war_type_false")
		# #set total global servants to seven
		# #set masters to seven randoms
		# #match masters to ideal servants
	# (else_try),
		# (eq, ":grailwar", "str_war_type_moon")
		# #no limit on servants, use the full 35
		# #set masters to 31 randoms
		# #like Fuyuki and Tokyo, disable world map
		# #set map to school building with a tournament style
		# #progression through story
	# (else_try),
		# (eq, ":grailwar", "str_war_type_grand")
		# #allow all servants to be summoned via gacha style nonsense
		# #no opposing masters, but shadow servants
		# #make it fake grand order in calradia
		# #has world map like Calradian War just without AI masters
		# #instead has parties of golems, zombies, etc based on FGO
	# (try_end),
	# ]),


	# USAGE: Allows Double Throw, you must declare both the shield version and thrown version of whatever item you would like to double throw
	# INPUT: arg1 = offhand itm_name, arg2 = thrown itm_name
	# OUTPUT: none
	("cf_fate_double_throw", [
		(set_fixed_point_multiplier, 100),
		(store_trigger_param_1, ":agent_no"),
		(store_script_param_1, ":itm_shield"),
		(store_script_param_2, ":itm_thrown"),
			(agent_get_horse, ":horse", ":agent_no"),
			(agent_get_ammo, ":ammo", ":agent_no", 1),
			(agent_get_wielded_item, ":right", ":agent_no", 0),
			(agent_get_wielded_item, ":left", ":agent_no", 1),
			(item_get_missile_speed, ":missile_speed", ":right"),
			
			(eq, ":left", ":itm_shield"),
			

			
			(gt, ":ammo", 1),
				(agent_get_look_position, pos1, ":agent_no"),
				(position_rotate_z, pos1, -2),
				
				(try_begin),
					(ge, ":horse", 0),
					(position_move_z, pos1, 250),
				(else_try),
					(position_move_z, pos1, 170),
				(try_end),

				(position_move_y, pos1, 130, 0),
				(position_move_x, pos1, -40, 0),
				#(position_move_z, pos1, 170, 0),
				(val_mul, ":missile_speed", 100),
				
				(agent_get_speed, pos6, ":agent_no"),
				(init_position, pos7),
				(get_distance_between_positions_in_meters, ":velocity", pos6, pos7),
				
				(val_add, ":missile_speed", ":velocity"),
				
				(add_missile, ":agent_no", pos1, ":missile_speed", ":itm_thrown", 0, ":itm_thrown", 0),
				
				(val_sub, ":ammo", 1),

				(agent_set_ammo, ":agent_no", ":right", ":ammo"),
				
				(lt, ":ammo", 2),
				(agent_unequip_item, ":agent_no", ":itm_shield"),
		]
	),
	
	# (script_get_charge_time, ":agent"),
	# USAGE: To determine how long an agent's attack had been charged.
	# INPUT: arg1 = agent_id
	# OUTPUT: reg50 = charge_time_in_seconds
	("get_charge_time", [
		(store_trigger_param_1, ":agent"),
	
		(agent_get_slot, ":charge_time", ":agent", slot_agent_active_charge),
		(convert_from_fixed_point, ":charge_time"),	
		(assign, reg50, ":charge_time"),
		]
	),
	
	
	# USAGE: Allows Double Throw, you must declare both the shield version 
	# and thrown version of whatever item you would like to double throw
	# INPUT: arg1 = offhand itm_name, arg2 = thrown itm_name
	# OUTPUT: none
	("cf_fate_throw_weapon_return", [
		(store_trigger_param_1, ":agent"),
		
		(store_script_param_1, ":itm_return"),
		(store_script_param_2, ":return_angle"),
		
		(set_fixed_point_multiplier, 100), 							# Will keep math consistant
		
		(copy_position, pos10, pos1),
		(agent_get_position, pos4, ":agent"),	
		
		(position_set_z_to_ground_level, pos10),
		(position_set_z_to_ground_level, pos4),
				
		#(position_move_z, pos4, 100, 1), # around chest height

		(position_get_z, ":agent_z", pos4),
		(position_get_z, ":target_z", pos10),
		
		(store_sub, ":z_length", ":agent_z", ":target_z"),
				
		(get_distance_between_positions, ":distance", pos10, pos4),		# How Far am I from there?
		
		(val_add, ":distance", ":z_length"),
		
		(store_mul, ":2theta", ":return_angle", 2),
		(store_sin, ":sin", ":2theta"),
		(store_mul, ":speed_prime", ":distance", 981),							# Distance = (2speed^2 / gravity) * cos * sin
		(val_div, ":speed_prime", ":sin"),
		(store_sqrt, ":speed", ":speed_prime"),									# so Speed = sqrt(((distance/(cos*sin))/2)*gravity)
		
		# (store_mul, ":speed_prime", ":distance", 981),
		# (val_div, ":speed_prime", 2),
		# (store_sqrt, ":speed", ":speed_prime"),	
		
		(assign, reg11, ":speed"),
		(display_log_message, "@Determined Speed = ({reg11})!"),
		
		(position_rotate_y, pos10, 180), 					# Reflect the Projectile
		(position_get_rotation_around_x, reg7, pos10), 		# Get new X after Y rotation
		(val_mul, reg7, -1), 								# Get the inverse of X to cancel
		(position_rotate_x, pos10, reg7),					# Cancel previous X
		(position_rotate_x, pos10, ":return_angle"), 		# The return angle
		(position_get_rotation_around_x, reg11, pos10),
		(display_log_message, "@Firing Angle = ({reg11})!"),
		
		(val_div, ":speed", 10),			

		(position_move_z, pos10, 100, 1), # So it doesn't spawn inside the ground
		#(val_sub, ":agent", 1), # maybe use so that the agent "catches" the weapon
		(add_missile, ":agent", pos10, ":speed", ":itm_return", 0, ":itm_return", 0),
		]
	),
	
	# Borrowed from Urist, https://forums.taleworlds.com/index.php?threads/crude-fragmentation-grenade.231097/#post-5529542
	# USAGE: Cause AOE damage with realistic falloff
	# INPUT: Explosion Pos, Attacking Agent, Radius, Minimum Damage, Maximum Damage
	# OUTPUT: none
	("cf_fate_explosion", [
		# (store_trigger_param_1, ":frag_thrower_agent"),
		
		(store_script_param, ":target_pos", 1),
		(store_script_param, ":frag_thrower_agent", 2),
		(store_script_param, ":explosion_radius", 3),
		(store_script_param, ":explosion_dmg_min", 4),
		(store_script_param, ":explosion_dmg_max", 5),
		
		(set_fixed_point_multiplier, 100),
		
		(copy_position, pos15, ":target_pos"),
		
		(play_sound_at_position, "snd_pistol_shot", pos15),
		(particle_system_burst, "psys_village_fire_big", pos15, 12),	
		(particle_system_burst, "psys_dummy_smoke_big", pos15, 5),				
		#blast effect part
		
		(get_player_agent_no, ":player"),
		
		(try_begin),
			(gt, ":player", 0),
			(agent_get_position, pos40, ":player"),
			(get_distance_between_positions_in_meters, ":meters", pos40, pos15),
			(lt, ":meters", 25),
			(val_max, ":meters", 1),
			(store_div, ":shake_strength", 100, ":meters"),
			(val_add, "$g_fate_cam_shake_frames", ":shake_strength"),
		(try_end),
		
		# For some reason, the below loop can include the player even if they are way out of the expected range.
		
		(try_for_prop_instances, ":cur_inst"),
			(prop_instance_get_position, pos16, ":cur_inst"),
			(get_distance_between_positions, ":distance", pos16, pos15),
			
			(le, ":distance", ":explosion_radius"),

			(store_random_in_range, ":blast_damage", ":explosion_dmg_min", ":explosion_dmg_max"), #randomised damage part			
			# (val_sub, ":blast_damage", ":distance"),
			
			(prop_instance_receive_damage, ":cur_inst", ":frag_thrower_agent", ":blast_damage"),
		(try_end),

		(try_for_agents, ":cur_agent", pos15, ":explosion_radius"),
			(agent_is_alive, ":cur_agent"),
			(agent_get_position, pos4, ":cur_agent"),
			
			(get_distance_between_positions, ":blast_distance", pos15, pos4), 
			(assign, reg10, ":blast_distance"),
			(convert_from_fixed_point, reg10),
			
			#(assign, reg10, ":blast_distance"), 
			#(display_message, "@Distance2={reg10}cm2.", 0xCCCCCC),		
			# (lt, ":blast_distance", ":explosion_radius"), #within 6m radius(using squared distance)
			(store_random_in_range, ":blast_damage", ":explosion_dmg_min", ":explosion_dmg_max"), #randomised damage part			
			(val_sub, ":blast_damage", ":blast_distance"),
			#(val_div, ":blast_damage", 20),		
			#(gt, ":blast_damage", 0),#max dam at distance 0= 40; max dam at 6m = 4;	min dam at distance 0 = 34; min dam at 6m = 0;		
			# (agent_deliver_damage_to_agent, ":frag_thrower_agent", ":cur_agent", ":blast_damage"),
			
			# (neq, ":frag_thrower_agent", ":cur_agent"),
			
			(agent_deliver_damage_to_agent_advanced, ":blast_damage", ":frag_thrower_agent", ":cur_agent", ":blast_damage"),
			
			(gt, ":blast_damage", 5),#if blast damage is big enough, knock agent down	
			
			(agent_is_human, ":cur_agent"),	#prevent horses from knockdown
			(agent_get_horse, ":cur_agent_mount_id", ":cur_agent"),#prevent mounted agents form knockdown
			
			(lt, ":cur_agent_mount_id", 0),		
			
			(try_begin),
				(position_is_behind_position, pos15, pos4),#blast from behind ->fall forward
				(agent_set_animation, ":cur_agent", "anim_rider_fall_roll"), 			
			(else_try),
				(agent_set_animation, ":cur_agent", "anim_strike_fall_back_rise"), 	#blast infront ->fall backwards	
				(agent_set_animation, ":cur_agent", "anim_strike_fall_back_rise_upper", 1), 				
			(try_end),
		(try_end),
		]
	),
	
("fate_battle_continuation",
[
	(store_script_param, ":dead_agent", 1),
	(store_script_param, ":dead_team", 2),
	
	(agent_get_troop_id, ":dead_troop", ":dead_agent"),
	
	(store_mission_timer_a, ":tod"),
	
	(set_fixed_point_multiplier, 100),
	(agent_is_human, ":dead_agent"),
	
	(try_begin),
		(neq, ":dead_team", -1),
		(troop_set_slot, ":dead_troop", slot_fate_death_time, ":tod"),
		(troop_set_slot, ":dead_troop", slot_fate_death_team, ":dead_team"),
	(else_try),
		(troop_set_slot, ":dead_troop", slot_fate_death_time, ":dead_team"),
		(troop_set_slot, ":dead_troop", slot_fate_death_team, ":dead_team"),
	(try_end),
]),
	
	
("fate_assign_item_slot", 
	[
	
	# Assign special weapon variables 
		# fate_weapon_element 
		# fate_weapon_enhancement 
		# fate_weapon_piercing 
		# fate_weapon_rank 
		# fate_weapon_description 
		# # fate_weapon_level 
		# # fate_weapon_name 

		# fate_weapon_mystic_code_spell_01 
		# fate_weapon_mystic_code_spell_02 
		# fate_weapon_mystic_code_spell_03 

		# fate_weapon_additional_attack 
		# fate_weapon_additional_defense 
		# fate_weapon_additional_mana 
		# fate_weapon_additional_damage
	
	(try_for_range, ":cur_item", weapons_begin, "itm_items_end"),
		(item_set_slot, ":cur_item", fate_weapon_element, "str_dmg_type_normal"),
	(try_end),
	
	(try_for_range, ":cur_item", fate_master_weapons_begin, fate_master_weapons_end),
		(try_begin),
			(this_or_next|eq, ":cur_item", "itm_aestus_estus"),
			(this_or_next|eq, ":cur_item", "itm_cu_runes"),
			(this_or_next|eq, ":cur_item", "itm_excalibur_lion"),
			(this_or_next|eq, ":cur_item", "itm_firebolt"),
			(eq, ":cur_item", "itm_fireaoe"),
			
			(item_set_slot, ":cur_item", fate_weapon_element, "str_dmg_type_fire"),
		(else_try),
			(this_or_next|eq, ":cur_item", "itm_wateraoe"),
			(this_or_next|eq, ":cur_item", "itm_waterbolt"),
			(this_or_next|eq, ":cur_item", "itm_zerkalot_sword"),
			(eq, ":cur_item", "itm_icebolt"),
			
			(item_set_slot, ":cur_item", fate_weapon_element, "str_dmg_type_water"),
		(else_try),
			(this_or_next|eq, ":cur_item", "itm_yew_bolts"),
			(this_or_next|eq, ":cur_item", "itm_excalibur_invis"),
			(this_or_next|eq, ":cur_item", "itm_windbolt"),
			(eq, ":cur_item", "itm_windaoe"),
			
			(item_set_slot, ":cur_item", fate_weapon_element, "str_dmg_type_wind"),
		(else_try),
			(this_or_next|eq, ":cur_item", "itm_caladbolg"),
			(this_or_next|eq, ":cur_item", "itm_caladbolg_ii"),
			(this_or_next|eq, ":cur_item", "itm_earthbolt"),
			(eq, ":cur_item", "itm_earthaoe"),
			
			(item_set_slot, ":cur_item", fate_weapon_element, "str_dmg_type_earth"),
		(else_try),
			(this_or_next|eq, ":cur_item", "itm_harpe"),
			(this_or_next|eq, ":cur_item", "itm_stella_meteor"),
			(this_or_next|eq, ":cur_item", "itm_astral_arrows"),
			(this_or_next|eq, ":cur_item", "itm_excalibur"),
			(this_or_next|eq, ":cur_item", "itm_caliburn"),
			(this_or_next|eq, ":cur_item", "itm_excalibur_gold"),
			(eq, ":cur_item", "itm_rulebreaker"),
			
			(item_set_slot, ":cur_item", fate_weapon_element, "str_dmg_type_divine"),
		(else_try),
			(this_or_next|eq, ":cur_item", "itm_cursedarm"),
			(this_or_next|eq, ":cur_item", "itm_gandr_shell"),
			(this_or_next|eq, ":cur_item", "itm_gandr"),
			(this_or_next|eq, ":cur_item", "itm_gae_dearg"),
			(this_or_next|eq, ":cur_item", "itm_gae_bolg_thrown"),
			(eq, ":cur_item", "itm_gae_bolg"),
			
			(item_set_slot, ":cur_item", fate_weapon_element, "str_dmg_type_demonic"),
		(else_try),
			(this_or_next|eq, ":cur_item", "itm_balmung"),
			(eq, ":cur_item", "itm_ascalon"),
			
			(item_set_slot, ":cur_item", fate_weapon_element, "str_dmg_type_dragon"),
		(else_try),
			(this_or_next|eq, ":cur_item", "itm_gae_buidhe"),
			(eq, ":cur_item", "itm_necro_grenade"),
			
			(item_set_slot, ":cur_item", fate_weapon_element, "str_dmg_type_undead"),
		(else_try),
			(this_or_next|eq, ":cur_item", "itm_black_key"),
			(this_or_next|eq, ":cur_item", "itm_cursed_dagger"),
			(this_or_next|eq, ":cur_item", "itm_throwing_daggers_assassin"),
			(this_or_next|eq, ":cur_item", "itm_cursed_dagger_thrown"),
			(this_or_next|eq, ":cur_item", "itm_cursed_dagger"),
			(this_or_next|eq, ":cur_item", "itm_cursed_dagger"),
			(eq, ":cur_item", "itm_black_key_melee"),
			
			(item_set_slot, ":cur_item", fate_weapon_element, "str_dmg_type_holy"),
		(try_end),
	(try_end),
	
	(try_for_range, ":cur_item", "itm_beretta", "itm_nagimaki"),
		# Defaults, to prevent bugs
		(item_set_slot, ":cur_item", accuracy_recovery, 10),
		(item_set_slot, ":cur_item", max_accuracy_mod, 700),
		(item_set_slot, ":cur_item", min_accuracy_mod, 2),
		(item_set_slot, ":cur_item", clip_size, 10),
		(item_set_slot, ":cur_item", accuracy_lost_per, 5),
		(item_set_slot, ":cur_item", max_effective_range, 30),
		(item_set_slot, ":cur_item", rounds_per_second, 1),
		
		(item_set_slot, ":cur_item", first_clip, 1),
		(item_set_slot, ":cur_item", aim_correction_rot_x, 0),
		(item_set_slot, ":cur_item", aim_correction_rot_z, 0),
		(item_set_slot, ":cur_item", ejection_port_x, 0),
		(item_set_slot, ":cur_item", ejection_port_y, 0),	
		(item_set_slot, ":cur_item", ejection_port_z, 0),	
		(item_set_slot, ":cur_item", clip_ejection_x, 0),	
		(item_set_slot, ":cur_item", clip_ejection_y, 0),	
		(item_set_slot, ":cur_item", clip_ejection_z, 0),	
		(item_set_slot, ":cur_item", reload_type, 0),
		
		(item_get_type, ":type", ":cur_item"),
		
		(assign, ":equip_animation", -1),	
		(assign, ":ready_animation", "anim_ready_pistol"),
		(assign, ":aim_loop_animation", "anim_ready_pistol"),
		(assign, ":reload_animation", "anim_reload_pistol"),
		(assign, ":fire_animation", "anim_release_pistol"),
		(assign, ":hipfire_animation", -1),
		(assign, ":hip_ready_animation", -1),
		(assign, ":end_animation", "anim_release_pistol"),
		 
		# Assign Animation Slots
		(try_begin),
			(this_or_next|is_between, ":cur_item", "itm_beretta", "itm_mp5"),
			(eq, ":type", itp_type_pistol),
			
			# (assign, ":equip_animation", "anim_minigun_equip"),	
			(assign, ":ready_animation", "anim_pistol_ready"),
			(assign, ":aim_loop_animation", "anim_pistol_aim"),
			(assign, ":reload_animation", "anim_pistol_reload"),
			(assign, ":fire_animation", "anim_pistol_fire"),
			(assign, ":hipfire_animation","anim_pistol_hip_shoot"),
			(assign, ":hip_ready_animation", "anim_pistol_hip"),
			(assign, ":end_animation", "anim_pistol_unready"),
		(else_try),
			(is_between, ":cur_item", "itm_remington_shotgun", "itm_m82"),
			
			(assign, ":ready_animation", "anim_shotgun_shoulder_ready"),
			(assign, ":fire_animation", "anim_shotgun_shoulder_shoot"),
			(assign, ":aim_loop_animation", "anim_shotgun_shoulder_aim"),
			(assign, ":reload_animation", "anim_shotgun_reload"),
			(assign, ":end_animation", "anim_shotgun_shoulder_to_idle"),
			(assign, ":hipfire_animation","anim_shotgun_hip_shoot"),
			(assign, ":hip_ready_animation", "anim_shotgun_hip_pose"),
			
		(else_try),
			(eq, ":cur_item", "itm_vulcan"),
			
			(assign, ":equip_animation", "anim_minigun_equip"),	
			(assign, ":ready_animation", "anim_ready_minigun"),
			(assign, ":aim_loop_animation", "anim_aim_minigun"),
			(assign, ":fire_animation", "anim_fire_minigun"),
			(assign, ":end_animation", "anim_unready_minigun"),
			(assign, ":hipfire_animation", -1),
			(assign, ":hip_ready_animation", -1),
		(else_try),
			(assign, ":ready_animation", "anim_rifle_ready"),
			(assign, ":fire_animation", "anim_rifle_fire"),
			(assign, ":equip_animation", -1),	
			(assign, ":aim_loop_animation", "anim_rifle_aim"),
			(assign, ":reload_animation", "anim_rifle_reload"),
			(assign, ":hipfire_animation", "anim_rifle_hip_shoot"),
			(assign, ":hip_ready_animation", "anim_rifle_hip"),
			(assign, ":end_animation", "anim_rifle_unready"),
		(try_end),
		
		(try_begin),
			(this_or_next|eq, ":cur_item", "itm_contender"),
			(this_or_next|eq, ":cur_item", "itm_webley"),
			(eq, ":cur_item", "itm_sawedoff"),
			
			(try_begin),
				(eq, ":cur_item", "itm_webley"),
				(assign, ":reload_animation", "anim_pistol_revolver_reload"),
			(else_try),
				(assign, ":ready_animation", "anim_1h_pistol_ready"),
				(assign, ":aim_loop_animation", "anim_1h_pistol_aim"),
				(assign, ":fire_animation", "anim_1h_pistol_fire"),
				(assign, ":reload_animation", "anim_pistol_breakaction_reload"),
				(assign, ":hipfire_animation", -1),
				(assign, ":hip_ready_animation", -1),
			(try_end),
			
			(item_set_slot, ":cur_item", reload_type, 1),
		(try_end),
					
		
		(try_begin),
			(eq, ":cur_item", "itm_vulcan"),
			(item_set_slot, ":cur_item", rounds_per_second, 1000),
			(item_set_slot, ":cur_item", accuracy_recovery, 10),
			(item_set_slot, ":cur_item", max_accuracy_mod, 850),
			(item_set_slot, ":cur_item", min_accuracy_mod, 100),
			(item_set_slot, ":cur_item", accuracy_lost_per, 5),
			# vulcan
			# (57.3048 , 8.82404 , -12.0216)
			(item_set_slot, ":cur_item", ejection_port_x, 573),
			(item_set_slot, ":cur_item", ejection_port_y, 88),	
			(item_set_slot, ":cur_item", ejection_port_z, -120),
			# (38.45 , -2.74264 , -4.1931)
			(item_set_slot, ":cur_item", clip_ejection_x, 385),	
			(item_set_slot, ":cur_item", clip_ejection_y, -27),	
			(item_set_slot, ":cur_item", clip_ejection_z, -42),	
			
		(else_try),
			(eq, ":cur_item", "itm_ak47"),
			(item_set_slot, ":cur_item", rounds_per_second, 10),
			(item_set_slot, ":cur_item", accuracy_recovery, 25),
			(item_set_slot, ":cur_item", max_accuracy_mod, 450),
			(item_set_slot, ":cur_item", min_accuracy_mod, 10),
			(item_set_slot, ":cur_item", accuracy_lost_per, 20),
			# ak
			# ep	(-4.62671 , 24.8191 , 1.75658)
			(item_set_slot, ":cur_item", ejection_port_x, -46),
			(item_set_slot, ":cur_item", ejection_port_y, 248),	
			(item_set_slot, ":cur_item", ejection_port_z, 18),
			# clip eject(-0.241916 , 19.104 , 1.9935)	
			(item_set_slot, ":cur_item", clip_ejection_x, 0),	
			(item_set_slot, ":cur_item", clip_ejection_y, 191),	
			(item_set_slot, ":cur_item", clip_ejection_z, 0),	
			
		(else_try),
			(eq, ":cur_item", "itm_calico"),
			(item_set_slot, ":cur_item", rounds_per_second, 13),
			(item_set_slot, ":cur_item", accuracy_recovery, 35),
			(item_set_slot, ":cur_item", max_accuracy_mod, 725),
			(item_set_slot, ":cur_item", min_accuracy_mod, 35),
			(item_set_slot, ":cur_item", accuracy_lost_per, 30),
		(else_try),
			(eq, ":cur_item", "itm_mp5"),
			(item_set_slot, ":cur_item", rounds_per_second, 13),
			(item_set_slot, ":cur_item", accuracy_recovery, 45),
			(item_set_slot, ":cur_item", max_accuracy_mod, 500),
			(item_set_slot, ":cur_item", min_accuracy_mod, 100),
			(item_set_slot, ":cur_item", accuracy_lost_per, 25),
			# mp5
			# (-6.13647 , 13.5992 , 1.89504)
			(item_set_slot, ":cur_item", ejection_port_x, -61),
			(item_set_slot, ":cur_item", ejection_port_y, 136),	
			(item_set_slot, ":cur_item", ejection_port_z, 19),	
			# (1.30362 , 14.4309 , 1.07397)
			(item_set_slot, ":cur_item", clip_ejection_x, 13),	
			(item_set_slot, ":cur_item", clip_ejection_y, 144),	
			(item_set_slot, ":cur_item", clip_ejection_z, 0),	
		(else_try),
			(eq, ":cur_item", "itm_aug"),
			(item_set_slot, ":cur_item", rounds_per_second, 12),
			(item_set_slot, ":cur_item", accuracy_recovery, 45),
			(item_set_slot, ":cur_item", max_accuracy_mod, 350),
			(item_set_slot, ":cur_item", min_accuracy_mod, 10),
			(item_set_slot, ":cur_item", accuracy_lost_per, 15),
			# Aug
			# (-14.7614 , -19.606 , 1.13921)
			(item_set_slot, ":cur_item", ejection_port_x, -147),
			(item_set_slot, ":cur_item", ejection_port_y, -196),	
			(item_set_slot, ":cur_item", ejection_port_z, 11),	
			# (-3.75755 , -19.0177 , 1.06928)
			(item_set_slot, ":cur_item", clip_ejection_x, -38),	
			(item_set_slot, ":cur_item", clip_ejection_y, -190),	
			(item_set_slot, ":cur_item", clip_ejection_z, 0),	
		(else_try),
			(eq, ":cur_item", "itm_fn_fal"),
			(item_set_slot, ":cur_item", rounds_per_second, 11),
			(item_set_slot, ":cur_item", accuracy_recovery, 10),
			(item_set_slot, ":cur_item", max_accuracy_mod, 850),
			(item_set_slot, ":cur_item", min_accuracy_mod, 10),
			(item_set_slot, ":cur_item", accuracy_lost_per, 15),
			# fn fal
			# (-4.7619 , 21.4358 , -1.90207)
			(item_set_slot, ":cur_item", ejection_port_x, -48),
			(item_set_slot, ":cur_item", ejection_port_y, 214),	
			(item_set_slot, ":cur_item", ejection_port_z, -19),
			# (1.13714 , 18.8233 , 0.597614)			
			(item_set_slot, ":cur_item", clip_ejection_x, 11),	
			(item_set_slot, ":cur_item", clip_ejection_y, 188),	
			(item_set_slot, ":cur_item", clip_ejection_z, 0),	
		(else_try),
			(eq, ":cur_item", "itm_akms"),
			(item_set_slot, ":cur_item", rounds_per_second, 10),
			(item_set_slot, ":cur_item", accuracy_recovery, 35),
			(item_set_slot, ":cur_item", max_accuracy_mod, 850),
			(item_set_slot, ":cur_item", min_accuracy_mod, 75),
			(item_set_slot, ":cur_item", accuracy_lost_per, 17),
			# akm
			# (-2.78604 , 20.5506 , 1.28912)
			(item_set_slot, ":cur_item", ejection_port_x, -28),
			(item_set_slot, ":cur_item", ejection_port_y, 206),	
			(item_set_slot, ":cur_item", ejection_port_z, 13),	
			# (0.485799 , 15.6073 , 1.14792)
			(item_set_slot, ":cur_item", clip_ejection_x, 5),	
			(item_set_slot, ":cur_item", clip_ejection_y, 156),	
			(item_set_slot, ":cur_item", clip_ejection_z, 0),	
		(try_end),
		
		(try_begin),
			(eq, ":cur_item", "itm_beretta"),
			(item_set_slot, ":cur_item", accuracy_recovery, 25),
			(item_set_slot, ":cur_item", max_accuracy_mod, 850),
			(item_set_slot, ":cur_item", min_accuracy_mod, 100),
			(item_set_slot, ":cur_item", accuracy_lost_per, 35),
			# baretta pistol
			# (1.37354 , 12.3214 , 0.985402)
			(item_set_slot, ":cur_item", ejection_port_x, 14),
			(item_set_slot, ":cur_item", ejection_port_y, 123),	
			(item_set_slot, ":cur_item", ejection_port_z, 9),	
			# (2.704 , -6.69242 , 0.0347638)
			(item_set_slot, ":cur_item", clip_ejection_x, 27),	
			(item_set_slot, ":cur_item", clip_ejection_y, -67),	
			(item_set_slot, ":cur_item", clip_ejection_z, 0),	
		(else_try),
			(eq, ":cur_item", "itm_colt"),
			(item_set_slot, ":cur_item", accuracy_recovery, 15),
			(item_set_slot, ":cur_item", max_accuracy_mod, 450),
			(item_set_slot, ":cur_item", min_accuracy_mod, 50),
			(item_set_slot, ":cur_item", accuracy_lost_per, 25),
			# 1911
			# (3.03636 , 15.7366 , 0.0668827)
			(item_set_slot, ":cur_item", ejection_port_x, 30),
			(item_set_slot, ":cur_item", ejection_port_y, 157),	
			(item_set_slot, ":cur_item", ejection_port_z, 0),	
			# (4.31754 , -4.49395 , -0.14444)
			(item_set_slot, ":cur_item", clip_ejection_x, 43),	
			(item_set_slot, ":cur_item", clip_ejection_y, -45),	
			(item_set_slot, ":cur_item", clip_ejection_z, 0),	
		(else_try),
			(eq, ":cur_item", "itm_glock"),
			(item_set_slot, ":cur_item", accuracy_recovery, 45),
			(item_set_slot, ":cur_item", max_accuracy_mod, 575),
			(item_set_slot, ":cur_item", min_accuracy_mod, 35),
			(item_set_slot, ":cur_item", accuracy_lost_per, 50),
			# glock
			# (-0.622037 , 11.0401 , 0.555694)
			(item_set_slot, ":cur_item", ejection_port_x, 0),
			(item_set_slot, ":cur_item", ejection_port_y, 110),	
			(item_set_slot, ":cur_item", ejection_port_z, 6),	
			# (0.740297 , -4.99687 , 0.0738605)
			(item_set_slot, ":cur_item", clip_ejection_x, 0),	
			(item_set_slot, ":cur_item", clip_ejection_y, 0),	
			(item_set_slot, ":cur_item", clip_ejection_z, 0),	
		(else_try),
			(eq, ":cur_item", "itm_contender"),
			(item_set_slot, ":cur_item", accuracy_recovery, 35),
			(item_set_slot, ":cur_item", max_accuracy_mod, 500),
			(item_set_slot, ":cur_item", min_accuracy_mod, 0),
			(item_set_slot, ":cur_item", accuracy_lost_per, 100),
			# contender
			# (4.96312 , 17.2617 , 0.172201)	
			(item_set_slot, ":cur_item", clip_ejection_x, 49),	
			(item_set_slot, ":cur_item", clip_ejection_y, 173),	
			(item_set_slot, ":cur_item", clip_ejection_z, 0),	
		(else_try),
			(eq, ":cur_item", "itm_tokarev"),
			(item_set_slot, ":cur_item", accuracy_recovery, 35),
			(item_set_slot, ":cur_item", max_accuracy_mod, 375),
			(item_set_slot, ":cur_item", min_accuracy_mod, 45),
			(item_set_slot, ":cur_item", accuracy_lost_per, 35),
			# tt33
			# (-0.113218 , 11.2822 , 0.78322)
			(item_set_slot, ":cur_item", ejection_port_x, -1),
			(item_set_slot, ":cur_item", ejection_port_y, 113),	
			(item_set_slot, ":cur_item", ejection_port_z, 8),
			# (1.25107 , -4.99823 , -0.0349029)			
			(item_set_slot, ":cur_item", clip_ejection_x, 125),	
			(item_set_slot, ":cur_item", clip_ejection_y, -50),	
			(item_set_slot, ":cur_item", clip_ejection_z, 0),	
		(else_try),
			(eq, ":cur_item", "itm_walther"),
			(item_set_slot, ":cur_item", accuracy_recovery, 45),
			(item_set_slot, ":cur_item", max_accuracy_mod, 350),
			(item_set_slot, ":cur_item", min_accuracy_mod, 10),
			(item_set_slot, ":cur_item", accuracy_lost_per, 90),
			# ppk
			# (0.0300722 , 13.2697 , 0.420867)
			(item_set_slot, ":cur_item", ejection_port_x, 0),
			(item_set_slot, ":cur_item", ejection_port_y, 133),	
			(item_set_slot, ":cur_item", ejection_port_z, 4),	
			# (1.77895 , -5.13927 , 0.0128674)
			(item_set_slot, ":cur_item", clip_ejection_x, 18),	
			(item_set_slot, ":cur_item", clip_ejection_y, -51),	
			(item_set_slot, ":cur_item", clip_ejection_z, 0),	
		(else_try),
			(eq, ":cur_item", "itm_webley"),
			(item_set_slot, ":cur_item", accuracy_recovery, 35),
			(item_set_slot, ":cur_item", max_accuracy_mod, 850),
			(item_set_slot, ":cur_item", min_accuracy_mod, 75),
			(item_set_slot, ":cur_item", accuracy_lost_per, 50),
			# webley
			# (-0.908688 , 10.1509 , 0)	
			(item_set_slot, ":cur_item", clip_ejection_x, 0),	
			(item_set_slot, ":cur_item", clip_ejection_y, 102),	
			(item_set_slot, ":cur_item", clip_ejection_z, 0),	
		(else_try),
			(eq, ":cur_item", "itm_remington_shotgun"),
			(item_set_slot, ":cur_item", accuracy_recovery, 50),
			(item_set_slot, ":cur_item", max_accuracy_mod, 850),
			(item_set_slot, ":cur_item", min_accuracy_mod, 100),
			(item_set_slot, ":cur_item", accuracy_lost_per, 100),
			# remington 870s
			# (-1.55879 , 22.9726 , 1.69243)
			(item_set_slot, ":cur_item", ejection_port_x, -16),
			(item_set_slot, ":cur_item", ejection_port_y, 230),	
			(item_set_slot, ":cur_item", ejection_port_z, 17),	
			# (2.07685 , 20.3206 , -0.299719)
			(item_set_slot, ":cur_item", clip_ejection_x, 21),	
			(item_set_slot, ":cur_item", clip_ejection_y, 203),	
			(item_set_slot, ":cur_item", clip_ejection_z, 0),	
		(else_try),
			(eq, ":cur_item", "itm_remington_shotgun_so"),
			(item_set_slot, ":cur_item", accuracy_recovery, 100),
			(item_set_slot, ":cur_item", max_accuracy_mod, 875),
			(item_set_slot, ":cur_item", min_accuracy_mod, 200),
			(item_set_slot, ":cur_item", accuracy_lost_per, 200),
			# remington 870s
			# (-1.55879 , 22.9726 , 1.69243)
			(item_set_slot, ":cur_item", ejection_port_x, -16),
			(item_set_slot, ":cur_item", ejection_port_y, 230),	
			(item_set_slot, ":cur_item", ejection_port_z, 17),	
			# (2.07685 , 20.3206 , -0.299719)
			(item_set_slot, ":cur_item", clip_ejection_x, 21),	
			(item_set_slot, ":cur_item", clip_ejection_y, 203),	
			(item_set_slot, ":cur_item", clip_ejection_z, 0),	
			
		(else_try),
			(eq, ":cur_item", "itm_sawedoff"),
			(item_set_slot, ":cur_item", accuracy_recovery, 100),
			(item_set_slot, ":cur_item", max_accuracy_mod, 850),
			(item_set_slot, ":cur_item", min_accuracy_mod, 350),
			(item_set_slot, ":cur_item", accuracy_lost_per, 200),
			# sawnoff
			# (7.52098 , 11.705 , -0.0559335)	
			(item_set_slot, ":cur_item", clip_ejection_x, 75),	
			(item_set_slot, ":cur_item", clip_ejection_y, 117),	
			(item_set_slot, ":cur_item", clip_ejection_z, 0),	
			
			# (assign, ":hipfire_animation","anim_shotgun_hip_shoot"),
			# (assign, ":hip_ready_animation", "anim_shotgun_hip_pose"),
		(else_try),
			(eq, ":cur_item", "itm_m82"),
			(item_set_slot, ":cur_item", accuracy_recovery, 200),
			(item_set_slot, ":cur_item", max_accuracy_mod, 725),
			(item_set_slot, ":cur_item", min_accuracy_mod, 0),
			(item_set_slot, ":cur_item", accuracy_lost_per, 300),
			# Barrett
			# (-6.29164 , 30.0155 , 2.22924)
			(item_set_slot, ":cur_item", ejection_port_x, -63),
			(item_set_slot, ":cur_item", ejection_port_y, 300),	
			(item_set_slot, ":cur_item", ejection_port_z, 23),	
			# (0.796399 , 26.6657 , 2.0265)
			(item_set_slot, ":cur_item", clip_ejection_x, 1),	
			(item_set_slot, ":cur_item", clip_ejection_y, 267),	
			(item_set_slot, ":cur_item", clip_ejection_z, 0),	
		(else_try),
			(eq, ":cur_item", "itm_remington_rifle"),
			(item_set_slot, ":cur_item", accuracy_recovery, 200),
			(item_set_slot, ":cur_item", max_accuracy_mod, 500),
			(item_set_slot, ":cur_item", min_accuracy_mod, 25),
			(item_set_slot, ":cur_item", accuracy_lost_per, 100),
		(else_try),
			(eq, ":cur_item", "itm_wa2000"),
			(item_set_slot, ":cur_item", accuracy_recovery, 200),
			(item_set_slot, ":cur_item", max_accuracy_mod, 500),
			(item_set_slot, ":cur_item", min_accuracy_mod, 0),
			(item_set_slot, ":cur_item", accuracy_lost_per, 100),
			# WA2000
			# ep	(-9.95157 , -17.0997 , 0.160434)
			(item_set_slot, ":cur_item", ejection_port_x, -100),
			(item_set_slot, ":cur_item", ejection_port_y, -171),	
			(item_set_slot, ":cur_item", ejection_port_z, 0),	
			# clip eject(0.677939 , -16.7632 , 0.778388)
			(item_set_slot, ":cur_item", clip_ejection_x, 7),	
			(item_set_slot, ":cur_item", clip_ejection_y, -168),	
			(item_set_slot, ":cur_item", clip_ejection_z, 0),	
		(else_try),
			(eq, ":cur_item", "itm_rocket"),
			(item_set_slot, ":cur_item", accuracy_recovery, 25),
			(item_set_slot, ":cur_item", max_accuracy_mod, 450),
			(item_set_slot, ":cur_item", min_accuracy_mod, 200),
			(item_set_slot, ":cur_item", accuracy_lost_per, 1000),
		(try_end),
		
		(item_set_slot, ":cur_item", equip_animation, ":equip_animation"),	
		(item_set_slot, ":cur_item", ready_animation, ":ready_animation"),
		(item_set_slot, ":cur_item", aim_loop_animation, ":aim_loop_animation"),
		(item_set_slot, ":cur_item", reload_animation, ":reload_animation"),
		(item_set_slot, ":cur_item", fire_animation, ":fire_animation"),
		(item_set_slot, ":cur_item", hipfire_animation, ":hipfire_animation"),
		(item_set_slot, ":cur_item", hip_ready_animation, ":hip_ready_animation"),
		(item_set_slot, ":cur_item", end_animation, ":end_animation"),

	(try_end),
	]),
	
#fate_assign_based_on_game_mode
#assign masters, servants based on chosen game mode, then

#fate_modify_player_servant
#This will modify the player character's servant in order to empower them or weaken them base on the player characters stats and allow for RPG style growth based on FATE/EXTRA in the freeplay mode

#fate_assign_servant
("fate_assign_servant", 
	[
		(store_script_param, ":summoner", 1),
		(store_script_param, ":catalyst", 2),
		 #(store_script_param, ":class_request", 3), # Not Currently Supported
		
		# (troop_get_slot, ":summoner_mana", ":summoner", slot_fate_max_mana),
		# (troop_get_slot, ":summoner_personality", ":summoner", fate_personality_type),
		
		# ###############################################################
		# ## Turns out, despite a summoners mana, they can pull any #####
		# ## servant in the Throne, and that it is entirely about #######
		# ## compatibility and catalysts. Mana will just determine what #
		# ## sort of performance you will get from your servant #########
		# ###############################################################
		
		
		(call_script, "script_summoning_weight_adjustment", ":summoner"),
		# ###############################################################
		# ## This script determines a summoners odds for each servant! ##
		# ###############################################################
		
		(try_begin),
			(is_between, ":catalyst", servant_catalysts_begin, servant_catalysts_end),
			(item_get_slot, ":catalyst_servant", ":catalyst", slot_fate_catalyst_servant),
			(troop_get_slot, ":catalyst_servant_class", ":catalyst_servant", slot_fate_servant_class),
		(try_end),
		
		# ###############################################################
		# ## 1st check to see if any servants exist.  ###################
		# ## Make sure their class is pulled from the pool ##############
		# ## 2nd, check player catalyst. If it belongs to  ##############
		# ## a single servant, rather than a pool like the ##############
		# ## knights of the round table or something, and  ##############
		# ## that servant class has already been used, ignore ###########
		# ## the catalyst entirely ######################################
		# ## If the servant class is not in use, summon that ############
		# ## servant. ###################################################
		# ###############################################################
		# ## Finally, if no catalyst exists, based on playe #############
		# ## personality type, choose from the pool of servants. ########
		# ###############################################################
		
		
		(assign, ":classes_left", 1),
		# ###############################################################
		# ## When doing random_from_range, it's really range-1, so by ###
		# ## adding from 1, you get the actual range. Dumb, I know. #####
		# ###############################################################
		
		(try_begin),
			(gt, "$g_saber_summoned", 0),
			(assign, ":saber_available", 0),
		(else_try),
			(assign, ":saber_available", 1),
			(val_add, ":classes_left", 1),
		(try_end),
		
		(try_begin),
			(gt, "$g_lancer_summoned", 0),
			(assign, ":lancer_available", 0),
		(else_try),
			(assign, ":lancer_available", 1),
			(val_add, ":classes_left", 1),
		(try_end),
		
		(try_begin),
			(gt, "$g_archer_summoned", 0),
			(assign, ":archer_available", 0),
		(else_try),
			(assign, ":archer_available", 1),
			(val_add, ":classes_left", 1),
		(try_end),
		
		(try_begin),
			(gt, "$g_caster_summoned", 0),
			(assign, ":caster_available", 0),
		(else_try),
			(assign, ":caster_available", 1),
			(val_add, ":classes_left", 1),
		(try_end),
		
		(try_begin),
			(gt, "$g_rider_summoned", 0),
			(assign, ":rider_available", 0),
		(else_try),
			(assign, ":rider_available", 1),
			(val_add, ":classes_left", 1),
		(try_end),
		
		(try_begin),
			(gt, "$g_assassin_summoned", 0),
			(assign, ":assassin_available", 0),
		(else_try),
			(assign, ":assassin_available", 1),
			(val_add, ":classes_left", 1),
		(try_end),
		
		(try_begin),
			(gt, "$g_berserker_summoned", 0),
			(assign, ":berserker_available", 0),
		(else_try),
			(assign, ":berserker_available", 1),
			(val_add, ":classes_left", 1),
		(try_end),
		
		(assign, reg15, ":classes_left"),
		(val_sub, reg15, 1),
		(display_log_message, "@There are {reg15} available classes"),
		
		(try_begin),
			(gt, ":classes_left", 1),
			(is_between, ":catalyst", servant_catalysts_begin, servant_catalysts_end),
			(try_begin),
				(eq, ":catalyst_servant_class", "str_class_saber"),
				(eq, ":saber_available", 1),
					(party_force_add_members, "p_main_party", ":catalyst_servant", 1),
					(troop_set_slot, ":catalyst_servant", slot_fate_master, ":summoner"), 
					(troop_set_slot, ":summoner", slot_fate_servant,  ":catalyst_servant"),
					(assign, "$g_saber", ":catalyst_servant"),
					(assign, "$g_saber_summoned", 1),
					(assign, "$g_master_of_saber", ":summoner"),
			(else_try),
				(eq, ":catalyst_servant_class", "str_class_lancer"),
				(eq, ":lancer_available", 1),
					(party_force_add_members, "p_main_party", ":catalyst_servant", 1),
					(troop_set_slot, ":catalyst_servant", slot_fate_master, ":summoner"), 
					(troop_set_slot, ":summoner", slot_fate_servant, ":catalyst_servant"),
					(assign, "$g_lancer", ":catalyst_servant"),
					(assign, "$g_lancer_summoned", 1),
					(assign, "$g_master_of_lancer", ":summoner"),
			(else_try),
				(eq, ":catalyst_servant_class", "str_class_archer"),
				(eq, ":archer_available", 1),
					(party_force_add_members, "p_main_party", ":catalyst_servant", 1),
					(troop_set_slot, ":catalyst_servant", slot_fate_master, ":summoner"), 
					(troop_set_slot, ":summoner", slot_fate_servant, ":catalyst_servant"),
					(assign, "$g_archer", ":catalyst_servant"),
					(assign, "$g_archer_summoned", 1),
					(assign, "$g_master_of_archer", ":summoner"),
			(else_try),
				(eq, ":catalyst_servant_class", "str_class_rider"),
				(eq, ":rider_available", 1),
					(party_force_add_members, "p_main_party", ":catalyst_servant", 1),
					(troop_set_slot, ":catalyst_servant", slot_fate_master, ":summoner"), 
					(troop_set_slot, ":summoner", slot_fate_servant, ":catalyst_servant"),
					(assign, "$g_rider", ":catalyst_servant"),
					(assign, "$g_rider_summoned", 1),
					(assign, "$g_master_of_rider", ":summoner"),
			(else_try),
				(eq, ":catalyst_servant_class", "str_class_caster"),
				(eq, ":caster_available", 1),
					(party_force_add_members, "p_main_party", ":catalyst_servant", 1),
					(troop_set_slot, ":catalyst_servant", slot_fate_master, ":summoner"), 
					(troop_set_slot, ":summoner", slot_fate_servant, ":catalyst_servant"),
					(assign, "$g_caster", ":catalyst_servant"),
					(assign, "$g_caster_summoned", 1),
					(assign, "$g_master_of_caster", ":summoner"),
			(else_try),
				(eq, ":catalyst_servant_class", "str_class_assassin"),
				(eq, ":assassin_available", 1),
					(party_force_add_members, "p_main_party", ":catalyst_servant", 1),
					(troop_set_slot, ":catalyst_servant", slot_fate_master, ":summoner"), 
					(troop_set_slot, ":summoner", slot_fate_servant, ":catalyst_servant"),
					(assign, "$g_assassin", ":catalyst_servant"),
					(assign, "$g_assassin_summoned", 1),
					(assign, "$g_master_of_assassin", ":summoner"),
			(else_try),
				(eq, ":catalyst_servant_class", "str_class_berserker"),
				(eq, ":berserker_available", 1),
					(party_force_add_members, "p_main_party", ":catalyst_servant", 1),
					(troop_set_slot, ":catalyst_servant", slot_fate_master, ":summoner"), 
					(troop_set_slot, ":summoner", slot_fate_servant, ":catalyst_servant"),
					(assign, "$g_berserker", ":catalyst_servant"),
					(assign, "$g_berserker_summoned", 1),
					(assign, "$g_master_of_berserker", ":summoner"),
			(try_end),
		(else_try),
			(gt, ":classes_left", 1),
			(assign,":i_end",100),
			(try_for_range, ":i", 0, ":i_end"),
				(store_random_in_range, ":class_assigned", 0, 7),
				# (store_random_in_range, ":r_saber", saber_begin, saber_end),
				# (store_random_in_range, ":r_lancer", lancer_begin, lancer_end),
				# (store_random_in_range, ":r_archer", archer_begin, archer_end),
				# (store_random_in_range, ":r_rider", rider_begin, rider_end),
				# (store_random_in_range, ":r_caster", caster_begin, caster_end),
				# (store_random_in_range, ":r_assassin", assassin_begin, assassin_end),
				# (store_random_in_range, ":r_berserker", berserker_begin, berserker_end),
				# (troop_get_slot, ":randos_class", ":rando_servant", slot_fate_servant_class),
				(try_begin),
					(eq, ":class_assigned", 0),
					(eq, ":saber_available", 1),
					(call_script, "script_summoning_lottery", ":class_assigned"),
					(party_force_add_members, "p_main_party", reg0, 1),
					(troop_set_slot, reg0, slot_fate_master, ":summoner"), 
					(troop_set_slot, ":summoner", slot_fate_servant, reg0),
					(assign, "$g_saber", reg0),
					(assign, "$g_saber_summoned", 1),
					(assign, "$g_master_of_saber", ":summoner"),
					(assign, ":i_end", 0),
				(else_try),
					(eq, ":class_assigned", 1),
					(eq, ":lancer_available", 1),
					(call_script, "script_summoning_lottery", ":class_assigned"),
					(party_force_add_members, "p_main_party", reg0, 1),
					(troop_set_slot, reg0, slot_fate_master, ":summoner"), 
					(troop_set_slot, ":summoner", slot_fate_servant, reg0),
					(assign, "$g_lancer", reg0),
					(assign, "$g_lancer_summoned", 1),
					(assign, "$g_master_of_lancer", ":summoner"),
					(assign, ":i_end", 0),
				(else_try),
					(eq, ":class_assigned", 2),
					(eq, ":archer_available", 1),
					(call_script, "script_summoning_lottery", ":class_assigned"),
					(party_force_add_members, "p_main_party", reg0, 1),
					(troop_set_slot, reg0, slot_fate_master, ":summoner"), 
					(troop_set_slot, ":summoner", slot_fate_servant, reg0),
					(assign, "$g_archer", reg0),
					(assign, "$g_archer_summoned", 1),
					(assign, "$g_master_of_archer", ":summoner"),
					(assign, ":i_end", 0),
				(else_try),
					(eq, ":class_assigned", 3),
					(eq, ":rider_available", 1),
					(call_script, "script_summoning_lottery", ":class_assigned"),
					(party_force_add_members, "p_main_party", reg0, 1),
					(troop_set_slot, reg0, slot_fate_master, ":summoner"), 
					(troop_set_slot, ":summoner", slot_fate_servant, reg0),
					(assign, "$g_rider", reg0),
					(assign, "$g_rider_summoned", 1),
					(assign, "$g_master_of_rider", ":summoner"),
					(assign, ":i_end", 0),
				(else_try),
					(eq, ":class_assigned", 4),
					(eq, ":caster_available", 1),
					(call_script, "script_summoning_lottery", ":class_assigned"),
					(party_force_add_members, "p_main_party", reg0, 1),
					(troop_set_slot, reg0, slot_fate_master, ":summoner"), 
					(troop_set_slot, ":summoner", slot_fate_servant, reg0),
					(assign, "$g_caster", reg0),
					(assign, "$g_caster_summoned", 1),
					(assign, "$g_master_of_caster", ":summoner"),
					(assign, ":i_end", 0),
				(else_try),
					(eq, ":class_assigned", 5),
					(eq, ":assassin_available", 1),
					(call_script, "script_summoning_lottery", ":class_assigned"),
					(party_force_add_members, "p_main_party", reg0, 1),
					(troop_set_slot, reg0, slot_fate_master, ":summoner"), 
					(troop_set_slot, ":summoner", slot_fate_servant, reg0),
					(assign, "$g_assassin", reg0),
					(assign, "$g_assassin_summoned", 1),
					(assign, "$g_master_of_assassin", ":summoner"),
					(assign, ":i_end", 0),
				(else_try),
					(eq, ":class_assigned", 6),
					(eq, ":berserker_available", 1),
					(call_script, "script_summoning_lottery", ":class_assigned"),
					(party_force_add_members, "p_main_party", reg0, 1),
					(troop_set_slot, reg0, slot_fate_master, ":summoner"), 
					(troop_set_slot, ":summoner", slot_fate_servant, reg0),
					(assign, "$g_berserker", reg0),
					(assign, "$g_berserker_summoned", 1),
					(assign, "$g_master_of_berserker", ":summoner"),
					(assign, ":i_end", 0),					
				(try_end),
				(assign, reg18, ":i"),
				(display_log_message, "@{reg18} tries"),
				(eq, "$g_fate_battle_servants_on", 1),
			(try_end),
		(else_try),
			(display_log_message, "@No more classes available"),
		(try_end),
		
	]),
	
# script_summoning_weight_adjustment
# Input:  summoner_troop, essentially who is currently summoning
# Output: Their odds, based on Alignment, of pulling a particular servant
#		The way it works, is every servant has a weight of 20 from the start,
#		then based on their compatibility, the odds will either be raised or
#		lowered. Worst case scenerio their weight becomes 5, best case, 80.
#		Plug this into the summoning script to pull their odds.
# I HAVE A SPREADSHEET! https://docs.google.com/spreadsheets/d/1k8AEwp4_zfLjVLW76AQ-jVjLmj0rHvqKdO9X-LS1xaE/edit?usp=sharing
("summoning_weight_adjustment",
	[
		(store_script_param, ":summoner_troop", 1),
		
		(troop_get_slot, ":summoner_alignment", ":summoner_troop", slot_fate_alignment),
		(try_for_range, ":i_servant", servants_begin, servants_end),
			(assign, ":summoning_odds", 20),
			(troop_get_slot, ":servant_alignment", ":i_servant", slot_fate_alignment),

			(try_begin),
				(eq, ":summoner_alignment", "str_alignment_lawful_good"),
				(try_begin),
					(eq, ":servant_alignment", "str_alignment_lawful_good"),
					(val_mul, ":summoning_odds", 4),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_lawful_neutral"),
					(val_mul, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_lawful_evil"),
					(val_mul, ":summoning_odds", 1),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_neutral_good"),
					(val_mul, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_neutral"),
					(val_mul, ":summoning_odds", 1),
				(else_try),		
					(eq, ":servant_alignment", "str_alignment_neutral_evil"),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_good"),
					(val_mul, ":summoning_odds", 1),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_neutral"),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_evil"),
					(val_div, ":summoning_odds", 4),
				(try_end),
			(else_try),
				(eq, ":summoner_alignment", "str_alignment_lawful_neutral"),
				(try_begin),
					(eq, ":servant_alignment", "str_alignment_lawful_good"),
					(val_mul, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_lawful_neutral"),
					(val_mul, ":summoning_odds", 3),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_lawful_evil"),
					(val_mul, ":summoning_odds", 1),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_neutral_good"),
					(val_mul, ":summoning_odds", 1),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_neutral"),
					(val_mul, ":summoning_odds", 15),
					(val_div, ":summoning_odds", 10),
				(else_try),		
					(eq, ":servant_alignment", "str_alignment_neutral_evil"),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_good"),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_neutral"),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_evil"),
					(val_div, ":summoning_odds", 4),
				(try_end),
			(else_try),
				(eq, ":summoner_alignment", "str_alignment_lawful_evil"),
				(try_begin),
					(eq, ":servant_alignment", "str_alignment_lawful_good"),
					(val_mul, ":summoning_odds", 1),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_lawful_neutral"),
					(val_mul, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_lawful_evil"),
					(val_mul, ":summoning_odds", 4),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_neutral_good"),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_neutral"),
					(val_mul, ":summoning_odds", 1),
				(else_try),		
					(eq, ":servant_alignment", "str_alignment_neutral_evil"),
					(val_mul, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_good"),
					(val_div, ":summoning_odds", 4),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_neutral"),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_evil"),
					(val_div, ":summoning_odds", 4),
				(try_end),
			(else_try),
				(eq, ":summoner_alignment", "str_alignment_neutral_good"),
				(try_begin),
					(eq, ":servant_alignment", "str_alignment_lawful_good"),
					(val_mul, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_lawful_neutral"),
					(val_mul, ":summoning_odds", 1),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_lawful_evil"),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_neutral_good"),
					(val_div, ":summoning_odds", 3),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_neutral"),
					(val_mul, ":summoning_odds", 15),
					(val_div, ":summoning_odds", 10),
				(else_try),		
					(eq, ":servant_alignment", "str_alignment_neutral_evil"),
					(val_mul, ":summoning_odds", 15),
					(val_div, ":summoning_odds", 10),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_good"),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_neutral"),
					(val_mul, ":summoning_odds", 15),
					(val_div, ":summoning_odds", 10),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_evil"),
					(val_div, ":summoning_odds", 2),
				(try_end),
			(else_try),
				(eq, ":summoner_alignment", "str_alignment_neutral"),
				(try_begin),
					(eq, ":servant_alignment", "str_alignment_lawful_good"),
					(val_mul, ":summoning_odds", 1),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_lawful_neutral"),
					(val_mul, ":summoning_odds", 15),
					(val_div, ":summoning_odds", 10),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_lawful_evil"),
					(val_div, ":summoning_odds", 1),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_neutral_good"),
					(val_mul, ":summoning_odds", 15),
					(val_div, ":summoning_odds", 10),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_neutral"),
					(val_mul, ":summoning_odds", 3),
				(else_try),		
					(eq, ":servant_alignment", "str_alignment_neutral_evil"),
					(val_mul, ":summoning_odds", 15),
					(val_div, ":summoning_odds", 10),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_good"),
					(val_div, ":summoning_odds", 1),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_neutral"),
					(val_mul, ":summoning_odds", 15),
					(val_div, ":summoning_odds", 10),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_evil"),
					(val_div, ":summoning_odds", 1),
				(try_end),
			(else_try),		
				(eq, ":summoner_alignment", "str_alignment_neutral_evil"),
				(try_begin),
					(eq, ":servant_alignment", "str_alignment_lawful_good"),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_lawful_neutral"),
					(val_div, ":summoning_odds", 1),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_lawful_evil"),
					(val_mul, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_neutral_good"),
					(val_mul, ":summoning_odds", 15),
					(val_div, ":summoning_odds", 10),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_neutral"),
					(val_mul, ":summoning_odds", 3),
				(else_try),		
					(eq, ":servant_alignment", "str_alignment_neutral_evil"),
					(val_mul, ":summoning_odds", 3),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_good"),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_neutral"),
					(val_mul, ":summoning_odds", 1),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_evil"),
					(val_mul, ":summoning_odds", 2),
				(try_end),
			(else_try),
				(eq, ":summoner_alignment", "str_alignment_chaotic_good"),
				(try_begin),
					(eq, ":servant_alignment", "str_alignment_lawful_good"),
					(val_mul, ":summoning_odds", 1),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_lawful_neutral"),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_lawful_evil"),
					(val_div, ":summoning_odds", 4),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_neutral_good"),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_neutral"),
					(val_mul, ":summoning_odds", 1),
				(else_try),		
					(eq, ":servant_alignment", "str_alignment_neutral_evil"),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_good"),
					(val_mul, ":summoning_odds", 4),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_neutral"),
					(val_mul, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_evil"),
					(val_mul, ":summoning_odds", 1),
				(try_end),
			(else_try),
				(eq, ":summoner_alignment", "str_alignment_chaotic_neutral"),
				(try_begin),
					(eq, ":servant_alignment", "str_alignment_lawful_good"),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_lawful_neutral"),
					(val_mul, ":summoning_odds", 15),
					(val_div, ":summoning_odds", 10),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_lawful_evil"),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_neutral_good"),
					(val_div, ":summoning_odds", 1),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_neutral"),
					(val_mul, ":summoning_odds", 15),
					(val_div, ":summoning_odds", 10),
				(else_try),		
					(eq, ":servant_alignment", "str_alignment_neutral_evil"),
					(val_mul, ":summoning_odds", 1),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_good"),
					(val_mul, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_neutral"),
					(val_mul, ":summoning_odds", 3),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_evil"),
					(val_mul, ":summoning_odds", 2),
				(try_end),
			(else_try),
				(eq, ":summoner_alignment", "str_alignment_chaotic_evil"),
				(try_begin),
					(eq, ":servant_alignment", "str_alignment_lawful_good"),
					(val_div, ":summoning_odds", 4),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_lawful_neutral"),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_lawful_evil"),
					(val_div, ":summoning_odds", 1),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_neutral_good"),
					(val_div, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_neutral"),
					(val_mul, ":summoning_odds", 1),
				(else_try),		
					(eq, ":servant_alignment", "str_alignment_neutral_evil"),
					(val_mul, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_good"),
					(val_mul, ":summoning_odds", 1),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_neutral"),
					(val_mul, ":summoning_odds", 2),
				(else_try),
					(eq, ":servant_alignment", "str_alignment_chaotic_evil"),
					(val_mul, ":summoning_odds", 4),
				(try_end),
			(try_end),
		(troop_set_slot, ":i_servant", slot_fate_summoning_chance, ":summoning_odds"),
		(try_end),
	]),

# script_summoning_lottery
# Input: Param1: class, the class the summoner is pulling from
#		Values: 0: Saber, 1: Lancer, 2: Archer, 3: Rider, 4: Caster
#				5: Assassin, 6: Berserker
# Output: reg0: summoning_trp based on class and weight. 
# NOTE: ALWAYS CALL THIS AFTER script_summoning_weight_adjustment !!!! # TODO: 3am Dustin Here! Fold that script into the top of this so it literally cannot be fired without it, dummy.
("summoning_lottery",
	[
	# ###############################################################
	# ## So, way this works is, script_summoning_weight_adjustment ##
	# ## assigns a weight to each troop, this script then uses that #
	# ## to determine who is pulled by adding together their ########
	# ## weights+1, store_random_in_range then using is_between #####
	# ## to determine who got called. Pretty clever, tbh. ###########
	# ###############################################################
	(store_script_param, ":class", 1),
	
	(assign, ":total_weight", 1),
	
	(assign, ":servant_1", 0),
	(assign, ":servant_2", 0),
	(assign, ":servant_3", 0),
	(assign, ":servant_4", 0),
	(assign, ":servant_5", 0),
	
	(try_begin),
		# (eq, ":class", "str_class_saber"),
		(eq, ":class", 0),
		(assign, ":range_min", saber_begin),
		(assign, ":range_max", saber_end),
	(else_try),
		# (eq, ":class", "str_class_lancer"),
		(eq, ":class", 1),
		(assign, ":range_min", lancer_begin),
		(assign, ":range_max", lancer_end),
	(else_try),
		# (eq, ":class", "str_class_archer"),
		(eq, ":class", 2),
		(assign, ":range_min", archer_begin),
		(assign, ":range_max", archer_end),
	(else_try),
		# (eq, ":class", "str_class_rider"),
		(eq, ":class", 3),
		(assign, ":range_min", rider_begin),
		(assign, ":range_max", rider_end),
	(else_try),
		# (eq, ":class", "str_class_caster"),
		(eq, ":class", 4),
		(assign, ":range_min", caster_begin),
		(assign, ":range_max", caster_end),
	(else_try),
		# (eq, ":class", "str_class_assassin"),
		(eq, ":class", 5),
		(assign, ":range_min", assassin_begin),
		(assign, ":range_max", assassin_end),
	(else_try),
		# (eq, ":class", "str_class_berserker"),
		(eq, ":class", 6),
		(assign, ":range_min", berserker_begin),
		(assign, ":range_max", berserker_end),
	(try_end),
	
	(try_for_range, ":cur_servant", ":range_min", ":range_max"),
		(troop_get_slot, ":weight", ":cur_servant", slot_fate_summoning_chance),
		(val_add, ":total_weight", ":weight"),
		(try_begin),
			(eq, ":servant_1", 0),
			(assign, ":servant_1", ":cur_servant"),
			(assign, ":servant_1_min", 0),
			(store_sub, ":servant_1_max", ":total_weight", 1),
		(else_try),
			(eq, ":servant_2", 0),
			(assign, ":servant_2", ":cur_servant"),
			(store_add, ":servant_2_min", ":servant_1_max", 1),
			(store_sub, ":servant_2_max", ":total_weight", 1),
		(else_try),
			(eq, ":servant_3", 0),
			(assign, ":servant_3", ":cur_servant"),
			(store_add, ":servant_3_min", ":servant_2_max", 1),
			(store_sub, ":servant_3_max", ":total_weight", 1),
		(else_try),
			(eq, ":servant_4", 0),
			(assign, ":servant_4", ":cur_servant"),
			(store_add, ":servant_4_min", ":servant_3_max", 1),
			(store_sub, ":servant_4_max", ":total_weight", 1),
		(else_try),
			(eq, ":servant_5", 0),
			(assign, ":servant_5", ":cur_servant"),
			(store_add, ":servant_5_min", ":servant_4_max", 1),
			(store_sub, ":servant_5_max", ":total_weight", 1),
		(try_end),
	(try_end),
	
	
	(store_random_in_range, ":lottery_ticket", 0, ":total_weight"),
	
	(try_begin),
		(is_between, ":lottery_ticket", ":servant_1_min", ":servant_1_max"),
		(assign, reg0, ":servant_1"),
	(else_try),
		(is_between, ":lottery_ticket", ":servant_2_min", ":servant_2_max"),
		(assign, reg0, ":servant_2"),
	(else_try),
		(is_between, ":lottery_ticket", ":servant_3_min", ":servant_3_max"),
		(assign, reg0, ":servant_3"),
	(else_try),
		(is_between, ":lottery_ticket", ":servant_4_min", ":servant_4_max"),
		(assign, reg0, ":servant_4"),
	(else_try),
		(is_between, ":lottery_ticket", ":servant_5_min", ":servant_5_max"),
		(assign, reg0, ":servant_5"),
	(try_end),
	
	(try_begin),
		(is_edit_mode_enabled),
		(str_store_troop_name, s10, ":servant_1"),
		(str_store_troop_name, s11, ":servant_2"),
		(str_store_troop_name, s12, ":servant_3"),
		(str_store_troop_name, s13, ":servant_4"),
		(str_store_troop_name, s14, ":servant_5"),
		
		(assign, reg20, ":total_weight"),
		(troop_get_slot, reg21, ":servant_1", slot_fate_summoning_chance),
		(troop_get_slot, reg22, ":servant_2", slot_fate_summoning_chance),
		(troop_get_slot, reg23, ":servant_3", slot_fate_summoning_chance),
		(troop_get_slot, reg24, ":servant_4", slot_fate_summoning_chance),
		(troop_get_slot, reg25, ":servant_5", slot_fate_summoning_chance),
		
		(display_message, "@Total Weight: {reg20}"),
		(display_message, "@{s10} has weight: {reg21}"),
		(display_message, "@{s11} has weight: {reg22}"),
		(display_message, "@{s12} has weight: {reg23}"),
		(display_message, "@{s13} has weight: {reg24}"),
		(display_message, "@{s14} has weight: {reg25}"),
	(try_end),
	]),
#After the player completes their summon, assign the remaining six -or however many based on game mode- to the AI masters

#USAGE: For Calradian and Great Grail Wars this summons masters to roam map
#INPUTS: param 1 war_type
("cf_fate_spawn_enemy_master_parties",
     [
      # (store_script_param, ":war_type", 1),
      # (store_script_param, ":center_no", 2),
	  
	  # master_begin, trp_master_staynight_09 # Fate/ SN
	  # trp_master_zero_01, trp_master_zero_07 # Fate/ Zero
	  # trp_master_apocrypha_01, trp_master_apocrypha_08 Fate/ Apocrypha
	  # trp_master_extra_01a, trp_master_extra_11 Fate/ Ex-
	  # trp_master_proto_01, master_end Fate/ Prototype
	 
      # (store_troop_faction, ":troop_faction_no", ":troop_no"),
		# (assign, ":masters_left", 6),
		(assign, ":classes_left", 1),
		
		(try_begin),
			(gt, "$g_saber_summoned", 0),
			(assign, ":saber_available", 0),
		(else_try),
			(assign, ":saber_available", 1),
			(val_add, ":classes_left", 1),
		(try_end),
		
		(try_begin),
			(gt, "$g_lancer_summoned", 0),
			(assign, ":lancer_available", 0),
		(else_try),
			(assign, ":lancer_available", 1),
			(val_add, ":classes_left", 1),
		(try_end),
		
		(try_begin),
			(gt, "$g_archer_summoned", 0),
			(assign, ":archer_available", 0),
		(else_try),
			(assign, ":archer_available", 1),
			(val_add, ":classes_left", 1),
		(try_end),
		
		(try_begin),
			(gt, "$g_caster_summoned", 0),
			(assign, ":caster_available", 0),
		(else_try),
			(assign, ":caster_available", 1),
			(val_add, ":classes_left", 1),
		(try_end),
		
		(try_begin),
			(gt, "$g_rider_summoned", 0),
			(assign, ":rider_available", 0),
		(else_try),
			(assign, ":rider_available", 1),
			(val_add, ":classes_left", 1),
		(try_end),
		
		(try_begin),
			(gt, "$g_assassin_summoned", 0),
			(assign, ":assassin_available", 0),
		(else_try),
			(assign, ":assassin_available", 1),
			(val_add, ":classes_left", 1),
		(try_end),
		
		(try_begin),
			(gt, "$g_berserker_summoned", 0),
			(assign, ":berserker_available", 0),
		(else_try),
			(assign, ":berserker_available", 1),
			(val_add, ":classes_left", 1),
		(try_end),
		
		(try_for_range, ":max_masters", 0, 6),
			#(ge, ":masters_left", 1),
			(set_spawn_radius, 5),
			(store_random_in_range, ":city", 21, 42),
			# Randomize Six Masters, maybe Seven if player refuses to summon Servant
			(store_random_in_range, ":master", master_begin, master_end), #for Calradian GW
			(spawn_around_party, ":city", "pt_hero_party"),
			(store_random_in_range, ":saber", saber_begin, saber_end),
			(store_random_in_range, ":lancer", lancer_begin, lancer_end),
			(store_random_in_range, ":archer", archer_begin, archer_end),
			(store_random_in_range, ":rider", rider_begin, rider_end),
			(store_random_in_range, ":caster", caster_begin, caster_end),
			(store_random_in_range, ":assassin", assassin_begin, assassin_end),
			(store_random_in_range, ":berserker", berserker_begin, berserker_end),
			
			(troop_get_slot, ":master_mana", ":master", slot_fate_max_mana),
			
			(try_begin),
				(eq, ":master_mana", 0),
				(troop_set_slot, ":master", slot_fate_max_mana, 3000), # This is a weird little stop gap measure to insure that there isn't a divide by zero when a master is chosen
				(troop_set_slot, ":master", slot_fate_cur_mana, 3000),
			(end_try),
      
			(assign, "$pout_party", reg0),
		  
			(party_set_faction, "$pout_party", "fac_outlaws"),
			# (party_set_slot, "$pout_party", slot_party_type, spt_kingdom_hero_party),
			# (call_script, "script_party_set_ai_state", "$pout_party", spai_undefined, -1),
			# (troop_set_slot, ":master", slot_troop_leaded_party, "$pout_party"),
			(party_add_leader, "$pout_party", ":master"),
			(str_store_troop_name, s5, ":master"),
			(str_store_party_name, s6, ":city"),
			
			(try_begin),
				# (str_store_troop_name, s6, ":saber"),
				(eq, ":saber_available", 1),	
				(troop_get_slot, ":saber_tn", ":saber", slot_fate_true_name),
				(str_store_string, s7, ":saber_tn"),
				(party_force_add_members, "$pout_party", ":saber", 1),
				(troop_set_slot, ":saber", slot_fate_master, ":master"), 
				(assign, "$g_saber", ":saber"),
				(assign, "$g_saber_summoned", 1),
				(assign, ":saber_available", 0),
			(else_try),
				(eq, ":lancer_available", 1),
				# (str_store_troop_name, s7, ":lancer"),
				(troop_get_slot, ":lancer_tn", ":lancer", slot_fate_true_name),
				(party_force_add_members, "$pout_party", ":lancer", 1),
				(troop_set_slot, ":lancer", slot_fate_master, ":master"), 
				(assign, "$g_lancer", ":lancer"),
				(str_store_string, s7, ":lancer_tn"),
				(assign, "$g_lancer_summoned", 1),
				(assign, ":lancer_available", 0),
			(else_try),
				(eq, ":archer_available", 1),	
				# (str_store_troop_name, s8, ":archer"),
				(troop_get_slot, ":archer_tn", ":archer", slot_fate_true_name),
				(str_store_string, s7, ":archer_tn"),
				(party_force_add_members, "$pout_party", ":archer", 1),
				(troop_set_slot, ":archer", slot_fate_master, ":master"), 
				(assign, "$g_archer", ":archer"),
				(assign, "$g_archer_summoned", 1),
				(assign, ":archer_available", 0),
			(else_try),
				(eq, ":rider_available", 1),	
				# (str_store_troop_name, s9, ":rider"),
				(troop_get_slot, ":rider_tn", ":rider", slot_fate_true_name),
				(str_store_string, s7, ":rider_tn"),
				(party_force_add_members, "$pout_party", ":rider", 1),
				(troop_set_slot, ":rider", slot_fate_master, ":master"), 
				(assign, "$g_rider", ":rider"),
				(assign, "$g_rider_summoned", 1),
				(assign, ":rider_available", 0),
			(else_try),
				(eq, ":caster_available", 1),	
				# (str_store_troop_name, s10, ":caster"),
				(troop_get_slot, ":caster_tn", ":caster", slot_fate_true_name),
				(str_store_string, s7, ":caster_tn"),
				(party_force_add_members, "$pout_party", ":caster", 1),
				(troop_set_slot, ":caster", slot_fate_master, ":master"), 
				(assign, "$g_caster", ":caster"),
				(assign, "$g_caster_summoned", 1),
				(assign, ":caster_available", 0),
			(else_try),
				(eq, ":assassin_available", 1),	
				# (str_store_troop_name, s11, ":assassin"),
				(troop_get_slot, ":assassin_tn", ":assassin", slot_fate_true_name), 
				(str_store_string, s7, ":assassin_tn"),
				(party_force_add_members, "$pout_party", ":assassin", 1),
				(troop_set_slot, ":assassin", slot_fate_master, ":master"),
				(assign, "$g_assassin", ":assassin"),
				(assign, "$g_assassin_summoned", 1),
				(assign, ":assassin_available", 0),
			(else_try),
				(eq, ":berserker_available", 1),	
				# (str_store_troop_name, s12, ":berserker"),
				(troop_get_slot, ":berserker_tn", ":berserker", slot_fate_true_name),
				(str_store_string, s7, ":berserker_tn"),
				(party_force_add_members, "$pout_party", ":berserker", 1),
				(troop_set_slot, ":berserker", slot_fate_master, ":master"), 
				(assign, "$g_berserker", ":berserker"),
				(assign, "$g_berserker_summoned", 1),
				(assign, ":berserker_available", 0),
			(try_end),
			
			(party_set_name, "$pout_party", "str_s5_s_party"),
			# (party_set_slot, "$pout_party", slot_party_commander_party, -1),
			(display_log_message, "@Master {s5} has been chosen! They're by {s6}, {s7}"),
			#(main_party_has_troop, <troop_id>),
					
			
			# (try_begin),
				# (eq, ":max_masters", 5),
					
					
			# (else_try),
				# (eq, ":max_masters", 4),
					
					# (display_log_message, "@Master {s5} has been chosen! They're by {s6}, {s8}"),
			# (else_try),
				# (eq, ":max_masters", 3),
					
					# (display_log_message, "@Master {s5} has been chosen! They're by {s6}, {s9}"),
			# (else_try),
				# (eq, ":max_masters", 2),
					
			# (else_try),
				# (eq, ":max_masters", 1),
					
					# (display_log_message, "@Master {s5} has been chosen! They're by {s6}, {s11}"),
			# (else_try),
					
					# (display_log_message, "@Master {s5} has been chosen! They're by {s6}. {s12}"),
			# (try_end),
	  (try_end),
   ]),

("fate_fuyuki_town_scripts",
	[
	(store_script_param, ":scene_name", 1),
	
	(try_begin),
	  (eq, ":scene_name", scn_fuyuki_1),
	  (modify_visitors_at_site, ":scene_name"),
	  (set_visitors, 25, "trp_master_staynight_01", 1),
      (set_visitors, 26, "trp_master_staynight_07", 1),
      (set_visitors, 27, "trp_fuyuki_walker_m", 2),
      (set_visitors, 28, "trp_fuyuki_walker_m", 1),
	  (set_jump_mission, "mt_town_default"),
	(else_try),
	  (eq, ":scene_name", scn_fate_storymode_train),
	  (modify_visitors_at_site, ":scene_name"),
	  (mission_tpl_entry_set_override_flags, "mt_town_default", 0, af_override_weapons),
	  (mission_tpl_entry_set_override_flags, "mt_town_default", 1, af_override_weapons),
      (set_visitors, 26, "trp_master_staynight_07", 1),
	  (set_jump_mission, "mt_storymode_introduction"),
	(else_try),
	  (eq, ":scene_name", scn_fate_storymode_train_station),
	  (modify_visitors_at_site, ":scene_name"),
	  (mission_tpl_entry_set_override_flags, "mt_town_default", 0, af_override_weapons),
	  (mission_tpl_entry_set_override_flags, "mt_town_default", 1, af_override_weapons),
      (set_visitors, 26, "trp_master_staynight_07", 1),
	  (set_visitors, 27, "trp_fuyuki_policeman", 1),
	  (set_jump_mission, "mt_storymode_introduction"),
	 # (set_jump_entry, 51),
	(else_try),
	  (set_jump_mission, "mt_town_default"),
	(try_end),

	  (jump_to_scene,":scene_name"),
	  (change_screen_mission),
	]),
	
("fate_quick_battle_setup",
	[
	(store_script_param, ":scene_name", 1),
	
	(troop_set_health, "$g_fate_battle_master_1", 100),
	(troop_set_health, "$g_fate_battle_servant_1", 100),
	(troop_set_health, "$g_fate_battle_master_2", 100),
	(troop_set_health, "$g_fate_battle_servant_2", 100),
	
	(troop_get_slot, ":max_mana1", "$g_fate_battle_master_1", slot_fate_max_mana),
	(troop_get_slot, ":max_mana2", "$g_fate_battle_master_2", slot_fate_max_mana),
	(troop_set_slot, "$g_fate_battle_master_1", slot_fate_cur_mana, ":max_mana1"),
	(troop_set_slot, "$g_fate_battle_master_2", slot_fate_cur_mana, ":max_mana2"),
	
	(troop_get_slot, ":max_mana1", "$g_fate_battle_servant_1", slot_fate_max_mana),
	(troop_get_slot, ":max_mana2", "$g_fate_battle_servant_2", slot_fate_max_mana),
	(troop_set_slot, "$g_fate_battle_servant_1", slot_fate_cur_mana, ":max_mana1"),
	(troop_set_slot, "$g_fate_battle_servant_2", slot_fate_cur_mana, ":max_mana2"),
	
	(troop_set_slot, "$g_fate_battle_servant_1", slot_fate_master, "$g_fate_battle_master_1"),
	(troop_set_slot, "$g_fate_battle_servant_2", slot_fate_master, "$g_fate_battle_master_2"),
	
	(troop_set_slot, "$g_fate_battle_master_1", slot_fate_servant, "$g_fate_battle_servant_1"),
	(troop_set_slot, "$g_fate_battle_master_2", slot_fate_servant, "$g_fate_battle_servant_2"),
	
	(modify_visitors_at_site, ":scene_name"),
	(reset_visitors),
	
	(try_begin),
		(eq, "$g_fate_battle_player_is_master", 1),
		(set_player_troop, "$g_fate_battle_master_1"),
	(else_try),
		(set_player_troop, "$g_fate_battle_servant_1"),
	(try_end),
	
	
	(try_begin),
		(eq, "$g_fate_battle_masters_on", 0),
	(else_try),
		(set_visitor, 0, "$g_fate_battle_master_1"),
		(set_visitor, 2, "$g_fate_battle_master_2"),
	(try_end),
	
	(try_begin),
		(eq, "$g_fate_battle_servants_on", 0),
	(else_try),
		(set_visitor, 1, "$g_fate_battle_servant_1"),
		(set_visitor, 3, "$g_fate_battle_servant_2"),
	(try_end),
	
	(set_jump_mission,"mt_fate_quick_battle"),
	(jump_to_scene, ":scene_name"),
	(jump_to_menu, "mnu_fate_battle_end"),
	(change_screen_mission),
	]),
	
	
	# This one hurts, I've made a 1v1 fight cam inside Unity 100 times,
	# Yet I fail every time inside Warband.
("fate_battle_spectate",
	[
	# (store_script_param, ":tracked_agent", 1),
	(set_fixed_point_multiplier, 1000),
	(try_begin),
		(get_player_agent_no, ":player"),
		
		(this_or_next|main_hero_fallen),
		(le, ":player", 0),
		
		(assign, ":servant", "$g_fate_battle_servant_1_agent_id"),
		(assign, ":enemy", "$g_fate_battle_servant_2_agent_id"),
					
		(mission_cam_set_mode, 1, 0, 0),
						
						
		(agent_get_position, pos2, ":enemy"),
		(agent_get_position, pos3, ":servant"),
		
		(mission_cam_get_position, pos30),
				
		(position_get_x, ":a_x", pos3),
		(position_get_y, ":a_y", pos3),
		(position_get_z, ":a_z", pos3),
		
		(position_get_x, ":e_x", pos2),
		(position_get_y, ":e_y", pos2),
		(position_get_z, ":e_z", pos2),
		
		(store_add, ":av_x", ":a_x", ":e_x"),
		(store_add, ":av_y", ":a_y", ":e_y"),
		(store_add, ":av_z", ":a_z", ":e_z"),
		
		(val_div, ":av_x", 2),
		(val_div, ":av_y", 2),
		(val_div, ":av_z", 2),
		
		(init_position, pos1),
		
		(position_set_x, pos1, ":av_x"),
		(position_set_y, pos1, ":av_y"),
		(position_set_z, pos1, ":av_z"),
		
		(copy_position, pos4, pos1),
		
		(position_move_z, pos1, 150),
		
		(try_begin),
			(call_script, "script_lookat", pos4, pos3),
		(try_end),
		
		(position_rotate_z, pos4, 90),
		(position_rotate_x, pos4, -25),
		
		(get_distance_between_positions, ":move", pos3, pos2),
		
		#(store_div, ":move_z", ":move", 3),
		#(store_div, ":move_x", ":move", -2),
		
		#(val_max, ":move_z", 500),
		#(val_max, ":move_x", 500),
		(val_max, ":move", 500),
		(val_mul, ":move", -1),
		
		# (position_move_x, pos4, 50),
		# (position_move_z, pos4, 100),
		
		# (position_move_x, pos4, ":move_x"),
		# (position_move_z, pos4, ":move_z"),
		(position_move_y, pos4, ":move"),
		
		(try_begin),
			(call_script, "script_lookat", pos4, pos1),
		(try_end),
		
		# (position_rotate_z, pos4, ":z_angle"),
		# (position_rotate_x, pos4, ":x_angle"),
		
		#(mission_cam_set_position, pos4),
		(mission_cam_animate_to_position, pos4, 100, 0),
	(try_end),
	
	]),
   
("fate_servant_combat",  
[
	(store_script_param, ":servant", 1),
	(store_script_param, ":closest_enemy", 2),
	
	(set_fixed_point_multiplier, 100),
	
	(try_begin),	# Enclosing the whole boy to prevent cf_ declaration
	
		(gt, ":servant", 0),
		(gt, ":closest_enemy", 0),
		
		(agent_is_non_player, ":servant"),
		
		(agent_is_alive, ":closest_enemy"),
		(neq, ":servant", ":closest_enemy"),

		(init_position, pos10),
		(init_position, pos11),
		
		(agent_get_troop_id, ":servant_troop", ":servant"),
		
		(agent_get_speed, pos45, ":closest_enemy"),
		(position_get_y, ":forward", pos45),
		
		#(store_mission_timer_a_msec, ":timer"),
		#(val_mod, ":timer", 50),

		# (agent_get_troop_id, ":servant_troop", ":servant"),

		(agent_get_attack_action, ":servant_attack", ":servant"),
		(agent_get_attack_action, ":enemy_attack", ":closest_enemy"),
		(agent_get_action_dir, ":enemy_attack_direction", ":closest_enemy"),

		(agent_get_position, pos10, ":servant"),
		(agent_get_position, pos11, ":closest_enemy"),
		(get_distance_between_positions, ":cur_spacing", pos10, pos11),

		# (agent_get_number_of_enemies_following, ":number_of_opponents", ":servant"),

		(agent_get_wielded_item, ":servant_weapon", ":servant", 0),
		(agent_get_wielded_item, ":enemy_weapon", ":closest_enemy", 0),

		(try_begin),
			(gt, ":servant_weapon", 0),
			(item_get_weapon_length, ":servant_weapon_length", ":servant_weapon"),
		(else_try),
			(assign, ":servant_weapon_length", 25),
		(try_end),

		(try_begin),
			(gt, ":enemy_weapon", 0),
			(item_get_weapon_length, ":enemy_weapon_length", ":enemy_weapon"),	
		(else_try),
			(assign, ":enemy_weapon_length", 25),
		(try_end),

		(store_sub, ":weapon_length_adv", ":servant_weapon_length", ":enemy_weapon_length"),
		
		# (assign, reg20, ":weapon_length_adv"),
		# (display_message, "@My advantage is {reg20}cm"),


		(store_add, ":servant_weapon_p50", ":servant_weapon_length", 50),
		(store_add, ":servant_weapon_p10", ":servant_weapon_length", 10),
		(store_sub, ":servant_weapon_m10", ":servant_weapon_length", 10),

		#(store_add, ":enemy_weapon_p10", ":enemy_weapon_length", 10),

		# Script Intention:
		# Servants should look at their nearest opponent, if they still can, block any incoming attack
		# Is this fight 1 on 1, or are they at a numbers disadvantage?
		#	- Do I have allies? Can I space with them to keep the numbers fair?
		#	- Can I backpedal to keep all my enemies infront of me?
		#	- Can I space the enemies?
		# Determine combat advantage, are you stronger / weaker than your opponent
		# What actions can be done to take the advantage?
		#	-If you're stronger, should you completely disregard a block to tank a hit to get an advantage?
		#	- Is your weapon longer / shorter? Should you move in or out to take advantage of that?
		#	- Do you have magic attacks you can use to stun or disable an opponent?
		#	- Do you have permission to use your Noble Phantasm?
		#		- If so, is it advantagous to do so?
		#	- How is your health? Should you flee and allow yourself to live another day?
		#	- Are any of your skills useful in this context?
		#	- Do you / your master know enough about this servant to determine their skills?
		#		- If so, should you
		#	- If you're faster than them, can you use footwork to maneveur around them to strike their sides?
		#	- Can you end this battle now, or should you lick your wounds and strategize?

		# I had an issue with these super fast dudes darting in and 
		# slamming into each other before the other could react, ending
		# battles far too quickly.
		
		(try_begin),
			(lt, ":cur_spacing", 500),
			(agent_slot_eq, ":closest_enemy", slot_agent_is_running_away, 0),
			(agent_set_speed_limit, ":servant", 5),
		(else_try),
			(agent_set_speed_limit, ":servant", 50),
		(try_end),

		(store_add, ":servant_weapon_p1", ":servant_weapon", 1),
		(store_sub, ":servant_weapon_m1", ":servant_weapon", 1),

		# (agent_get_animation, ":anim", ":servant", 1),

		(try_begin),
			(lt, ":cur_spacing", 300),
			(gt, ":servant_weapon", 0),
			(item_has_property, ":servant_weapon", itp_next_item_as_melee),
			(agent_unequip_item, ":servant", ":servant_weapon"),
			(agent_equip_item, ":servant", ":servant_weapon_p1"),
			(agent_set_wielded_item, ":servant", ":servant_weapon_p1"),
			(agent_set_animation, ":servant", "anim_quick_equip", 1),
			# (agent_set_animation_progress, ":servant", 2000),
		(else_try),
			(gt, ":cur_spacing", 500),
			(gt, ":servant_weapon", 0),
			(item_has_property, ":servant_weapon_m1", itp_next_item_as_melee),
			(agent_unequip_item, ":servant", ":servant_weapon"),
			(agent_equip_item, ":servant", ":servant_weapon_m1"),
			(agent_set_wielded_item, ":servant", ":servant_weapon_m1"),
			(agent_set_animation, ":servant", "anim_quick_equip", 1),
			# (agent_set_animation_progress, ":servant", 2000),
		(try_end),
		
		(try_begin),
			(eq, ":servant_troop", "trp_archer_01"),
			(try_begin),
				(lt, ":cur_spacing", 900),
				(eq, ":servant_weapon", "itm_emiya_bow"),
				
				(agent_unequip_item, ":servant", "itm_emiya_bow"),
				(agent_unequip_item, ":servant", "itm_emiya_arrows"),
				
				(agent_equip_item, ":servant", "itm_kanshou"),
				(agent_set_wielded_item, ":servant", "itm_kanshou"),
			(else_try),
				(gt, ":cur_spacing", 1000),
				(neq, ":servant_weapon", "itm_emiya_bow"),
				(agent_equip_item, ":servant", "itm_emiya_bow"),
				(agent_set_wielded_item, ":servant", "itm_emiya_bow"),
			(try_end),
		(try_end),

		(assign, ":backpedalling", 0),

		(try_begin),
			(agent_force_rethink, ":servant"),
			(le, ":cur_spacing", 300),
			#(eq, ":timer", 0),
			(try_begin),
				(ge, ":weapon_length_adv", 0),
					(try_begin),
						(gt, ":cur_spacing", ":servant_weapon_p10"),
						#(agent_force_rethink, ":servant"),
						(agent_set_scripted_destination, ":servant", pos11, 1),
					#	(display_message, "@I have been told to move forward"),
					(else_try),
						(is_between, ":cur_spacing", ":enemy_weapon_length", ":servant_weapon_length"),	
					#	(display_message, "@I am where I want to be"),
						(try_begin),
							# (store_random_in_range, ":footwork", 0, 11),
							# (le, ":footwork", 2),
							(store_random_in_range, ":footwork", -150, 150),
							(position_move_y, pos10, ":footwork", 0),
							(store_random_in_range, ":footwork", -300, 300),
							(position_move_x, pos10, ":footwork", 0),
						(try_end),
						(agent_set_scripted_destination, ":servant", pos10, 1),
					(else_try),
					#	(display_message, "@I have been told to backpedal."),
						(assign, ":backpedalling", 1),
						(val_mul, ":servant_weapon_p10", -1),
						(position_move_y, pos10, ":servant_weapon_p10", 0),
						# (position_move_y, pos10, -150, 0),
						#(agent_force_rethink, ":servant"),
						(agent_set_scripted_destination_no_attack, ":servant", pos10, 1),
					(try_end),
			(else_try),
					(try_begin),
						(gt, ":cur_spacing", ":servant_weapon_m10"),
						#(agent_force_rethink, ":servant"),
						(agent_set_scripted_destination, ":servant", pos11, 1),
					(else_try),
						(is_between, ":cur_spacing", 75, ":servant_weapon_m10"),
						(try_begin),
							# (store_random_in_range, ":footwork", 0, 11),
							# (le, ":footwork", 2),
							(store_random_in_range, ":footwork", -200, 10),
							(position_move_y, pos10, ":footwork", 0),
							(store_random_in_range, ":footwork", -300, 300),
							(position_move_x, pos10, ":footwork", 0),
						(try_end),
						(agent_set_scripted_destination, ":servant", pos10, 1),
					(else_try),
						(val_mul, ":servant_weapon_p10", -1),
						(position_move_y, pos10, ":servant_weapon_p10", 0),
						#(agent_force_rethink, ":servant"),
						(agent_set_scripted_destination_no_attack, ":servant", pos10, 1),
					(try_end),			
			(try_end),
			#(agent_clear_scripted_mode, ":servant"),
			#(agent_force_rethink, ":servant"),
		(else_try),
			(is_between, ":cur_spacing", 300, 1000),
			(agent_set_scripted_destination, ":servant", pos11, 1, 0),
		(else_try),
			(agent_clear_scripted_mode, ":servant"),
		(try_end),
		
		(agent_get_scripted_destination, pos13, ":servant"),
		#(particle_system_burst, "psys_laser", pos13, 100),

		(store_random_in_range, ":chance", 0, 400),

		(le, ":cur_spacing", ":servant_weapon_p50"),
		
		(eq, ":backpedalling", 0),
		
		(try_begin),
				(eq, ":servant_attack", 0),
					(try_begin),
							(eq, ":enemy_attack", 0),
							(gt, ":chance", 100),
							(call_script, "script_randomize_attack", ":servant"),
					(else_try),
							(eq, ":enemy_attack", 1),
							(call_script, "script_servant_defend", ":servant", ":enemy_attack_direction"),
							# (agent_set_attack_action, ":servant", 1, 0),
					(else_try),
							(eq, ":enemy_attack", 2),
							(call_script, "script_servant_defend", ":servant", ":enemy_attack_direction"),
					(else_try),
							(eq, ":enemy_attack", 3),
							(gt, ":chance", 100),
							(call_script, "script_randomize_attack", ":servant"),
					(else_try),
							(ge, ":enemy_attack", 4),
							(gt, ":chance", 100),
							(call_script, "script_randomize_attack", ":servant"),
					(try_end),
		(else_try),
				(eq, ":servant_attack", 1),
					(try_begin),
							(eq, ":enemy_attack", 0),
					(else_try),
							(eq, ":enemy_attack", 1),
							(gt, ":chance", 200),
							(call_script, "script_servant_defend", ":servant", ":enemy_attack_direction"),
					(else_try),
							(eq, ":enemy_attack", 2),
							(gt, ":chance", 300),
							(call_script, "script_servant_defend", ":servant", ":enemy_attack_direction"),
					(else_try),
							(ge, ":enemy_attack", 3),
							#(call_script, "script_randomize_attack", ":servant"),
					(try_end),
		(else_try),
				(eq, ":servant_attack", 2),
					(try_begin),
							(eq, ":enemy_attack", 0),
					(else_try),
							(eq, ":enemy_attack", 1),
							(gt, ":chance", 200),
							(call_script, "script_servant_defend", ":servant", ":enemy_attack_direction"),
					(else_try),
							(eq, ":enemy_attack", 2),
							(gt, ":chance", 300),
							(call_script, "script_servant_defend", ":servant", ":enemy_attack_direction"),
					(else_try),
							(eq, ":enemy_attack", 3),
							# (agent_set_attack_action, ":servant", 1, 0),
					(else_try),
							(ge, ":enemy_attack", 4),
							# (agent_set_attack_action, ":servant", 1, 0),
					(try_end),
		(else_try),
				(eq, ":servant_attack", 3),
					(try_begin),
							(eq, ":enemy_attack", 0),
							(gt, ":chance", 350),
							(call_script, "script_randomize_attack", ":servant"),
					(else_try),
							(eq, ":enemy_attack", 1),
							# (agent_set_attack_action, ":servant", 2, 0),
							(call_script, "script_servant_defend", ":servant", ":enemy_attack_direction"),
					(else_try),
							(eq, ":enemy_attack", 2),
							(call_script, "script_servant_defend", ":servant", ":enemy_attack_direction"),
					(else_try),
							(eq, ":enemy_attack", 3),
							(gt, ":chance", 350),
							(call_script, "script_randomize_attack", ":servant"),
					(else_try),
							(ge, ":enemy_attack", 4),
							(gt, ":chance", 350),
							(call_script, "script_randomize_attack", ":servant"),
					(try_end),
		(else_try),
				(eq, ":servant_attack", 4),
					(try_begin),
							(eq, ":enemy_attack", 0),
							(gt, ":chance", 350),
							(call_script, "script_randomize_attack", ":servant"),
					(else_try),
							(eq, ":enemy_attack", 1),
							# (agent_set_attack_action, ":servant", 1, 0),
							(call_script, "script_servant_defend", ":servant", ":enemy_attack_direction"),
					(else_try),
							(eq, ":enemy_attack", 2),
							(call_script, "script_servant_defend", ":servant", ":enemy_attack_direction"),
					(else_try),
							(eq, ":enemy_attack", 3),
							(gt, ":chance", 350),
							(call_script, "script_randomize_attack", ":servant"),
					(else_try),
							(ge, ":enemy_attack", 4),
							(gt, ":chance", 350),
							(call_script, "script_randomize_attack", ":servant"),
					(try_end),
		(try_end),
	(try_end),
	        
 ]),
 
("randomize_attack", [
	(store_script_param, ":attacker", 1),
	
	(store_random_in_range, ":rand_dir", 0, 4),
	(agent_set_attack_action, ":attacker", ":rand_dir", 0),
]),
 
("servant_defend",  
  [
(store_script_param, ":servant", 1),
(store_script_param, ":enemy_attack_direction", 2),
                (try_begin),
                        (eq, ":enemy_attack_direction", 0),
                        (agent_set_defend_action, ":servant", 0, 50),
                (else_try),
                        (eq, ":enemy_attack_direction", 1),
                        (agent_set_defend_action, ":servant", 1, 50),
                (else_try),
                        (eq, ":enemy_attack_direction", 2),
                        (agent_set_defend_action, ":servant", 2, 50),
                (else_try),
                        (eq, ":enemy_attack_direction", 3),
                        (agent_set_defend_action, ":servant", 3, 50),
				(else_try),
                        (agent_set_defend_action, ":servant", -2, 1),
                (try_end),

  ]),
  
  # (script_determine_per_agent_stats, "agent_id"),
  # Input: Agent ID
  # Output: None, sets agent_slot slot_agent_magic_resist
  # 		Used to determine the efficacy of magic,
  #			Call on ti_on_agent_spawn
  ("determine_per_agent_stats",
  [
	(store_script_param, ":agent", 1),
	(agent_get_troop_id, ":troop", ":agent"),
	
	# First Up, Magic Resistance
	
	# The math may ~look~ weird, but remember that a skill level of 1
	# is the best e.g. A-rank, so by dividing 100 by the resistance
	# we end up with the inverse i.e. lower numbers producing higher
	# numbers.
	
	# Example:
	# B rank Magic, D Rank Endurance
	# 2 + 4 * 4 = 18
	# 100 / 18 = ~11
	
	(try_begin),
		(is_between, ":troop", servants_begin, servants_end),
		(troop_get_slot, ":magic", ":troop", fate_param_mgc),
		(troop_get_slot, ":endur", ":troop", fate_param_end), 
		(val_mul, ":endur", 2),
		(val_add, ":magic", ":endur"),
		(store_div, ":magic_res", 100, ":magic"),
	(else_try),
		(assign, ":magic_res", 2),
	(try_end),
	
	# And this Clause runs through all their skill slots, and if
	# it has magic_resistance, check the skill level, invert it
	# (same story where less is better), then add it directly to
	# the magic resistance
	
	(try_for_range, ":slot", fate_skill_slot_begin, fate_skill_slot_end),
		(troop_get_slot, ":skill", ":troop", ":slot"),
		(eq, ":skill", "str_skill_magic_resistance"),
		(val_add, ":slot", 1),
		(troop_get_slot, ":skill_lvl", ":troop", ":slot"),
		(store_div, ":magic_res_skill", 10, ":skill_lvl"),
		(val_add, ":magic_res", ":magic_res_skill"),
	(try_end),
	
	(agent_set_slot, ":agent", slot_agent_magic_resist, ":magic_res"),
	
	# Next Up, Armor Level
	
	(try_begin),
		(is_between, ":troop", servants_begin, servants_end),
		(troop_get_slot, ":endur", ":troop", fate_param_end),
		(troop_get_slot, ":agili", ":troop", fate_param_agi),
		(val_mul, ":agili", 4),
		(val_add, ":endur", ":agili"),
		(store_div, ":armor_level", 200, ":endur"),
	(else_try),
		(assign, ":armor_level", 4),
	(try_end),
	
	# (assign, ":equipment_armor", 10),
	(try_for_range, ":item_slot", 4, 8),
		(agent_get_item_slot, ":item", ":agent", ":item_slot"),
		(gt, ":item", 0),
		(item_get_slot, ":rank", ":item", fate_weapon_rank),
		(gt, ":rank", 0),
		(store_div, ":equipment_rank", 10, ":rank"),
		(val_add, ":armor_level", ":equipment_rank"),
	(try_end),
	
	(agent_set_slot, ":agent", slot_agent_armor_level, ":armor_level"),
	# Now Agent Modifiers
	
	# You might ask, Why keep the original values? To answer: I want to.
	
	(agent_get_damage_modifier, ":damage_mod", ":agent"),
	(agent_get_accuracy_modifier, ":acc_mod", ":agent"),
	(agent_get_speed_modifier, ":speed_mod", ":agent"),
	(agent_get_reload_speed_modifier, ":rspeed_mod", ":agent"),
	
	(try_begin),
		(get_player_agent_no, ":player"),
		(eq, ":agent", ":player"),
		(assign, ":speed_mod", 100),
	(try_end),
	
	(agent_set_slot, ":agent", slot_agent_original_damage, ":damage_mod"),
	(agent_set_slot, ":agent", slot_agent_original_accuracy, ":acc_mod"),
	(agent_set_slot, ":agent", slot_agent_original_speed, ":speed_mod"),
	(agent_set_slot, ":agent", slot_agent_original_rspeed, ":rspeed_mod"),
	
	(agent_set_slot, ":agent", slot_agent_reset_equip_timer, 0),
	(agent_set_slot, ":agent", fate_agent_action_timer, 0),
	(agent_set_slot, ":agent", fate_agent_cur_timed_action, 0),
	(agent_set_slot, ":agent", fate_agent_cur_spell_time, 0),
	
	
	
	# slot_agent_charge_melee_type = 5	# 1 - Charge Damage, 2 - Elemental, 3 - Status Inflict, 4 - Beam Attack
	# slot_agent_charge_range_type = 6	# 1 - Rapid-fire, 2 - Mult-Shot, 3 - Charge Damage, 4 - Status Inflict
	# slot_agent_range_attack_type = 7	# 1 - Accurate, 2 - Rapid, 
	(try_for_range, ":servant", servants_begin, servants_end),
		
		(try_begin),
			(eq, ":troop", "trp_archer_01"),
			(assign, ":charge_melee_type", charge_melee_damage),
			(assign, ":charge_ranged_type", charge_range_damage),
			(assign, ":ranged_attack_type", 2),
		(else_try),
			(eq, ":troop", "trp_caster_01"),
			(assign, ":charge_melee_type", 3),
			(assign, ":charge_ranged_type", 3),
			(assign, ":ranged_attack_type", 2),
		(else_try),
			(eq, ":troop", "trp_saber_01"),
			(assign, ":charge_melee_type", 4),
			(assign, ":charge_ranged_type", 3),
			(assign, ":ranged_attack_type", 1),
		(else_try),
			(assign, ":charge_melee_type", 1),
			(assign, ":charge_ranged_type", 1),
			(assign, ":ranged_attack_type", 1),
		(try_end),
	
	(try_end),
	
	(try_begin),
		(neg|is_between, ":troop", servants_begin, servants_end),
		(assign, ":charge_ranged_type", 0),
		(assign, ":ranged_attack_type", 0),
		(assign, ":charge_melee_type", 0),
	(try_end),
	
	(agent_set_slot, ":agent", slot_agent_charge_melee_type, ":charge_melee_type"),
	(agent_set_slot, ":agent", slot_agent_charge_range_type, ":charge_ranged_type"),
	(agent_set_slot, ":agent", slot_agent_range_attack_type, ":ranged_attack_type"),
  ]),
    
  # (script_determine_spell_tags, "str_spell"),
  # Input: A spell name string
  # Output:	s10: Spell Element
  #			reg9: Spell Effect
  #			(heal = 0, enhance = 1, damage = 2, status effect = 3, summon = 4)
  #			reg10: Spell Effective Distance / Target
  #			(close = 0, mid, = 1 far = 2, servant = 4, self = -1)
  #			Mostly used for AI decision-making but also lends itself
  #			for certain types of spells. Namely healing and
  #			enhancement spells.
  # Usage:	Meant to be used in the AI magic selection process, also used in
  #			magic_cast to determine who to target based on the spell
  #			Also used for the magic dictionary.
  
("determine_spell_tags",
[
# Inputs
(store_script_param, ":spell", 1),

(is_between, ":spell", spells_begin, spells_end),

(try_begin),
	(eq, ":spell", "str_magecraft_fire_bolt"),
	(assign, ":spell_cost", 20),
(else_try),
	(eq, ":spell", "str_magecraft_fire_aoe"),
	(assign, ":spell_cost", 40),
(else_try),
	(eq, ":spell", "str_magecraft_fire_buff"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_fire_unique1"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_fire_unique2"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_fire_unique3"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_fire_unique4"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_water_bolt"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_water_aoe"),
	(assign, ":spell_cost", 40),
(else_try),
	(eq, ":spell", "str_magecraft_water_trap"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_water_unique1"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_water_unique2"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_water_unique3"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_wind_bolt"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_wind_aoe"),
	(assign, ":spell_cost", 40),
(else_try),
	(eq, ":spell", "str_magecraft_wind_buff"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_wind_unique1"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_wind_unique2"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_wind_unique3"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_wind_unique4"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_wind_unique5"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_earth_bolt"),
	(assign, ":spell_cost", 20),
(else_try),
	(eq, ":spell", "str_magecraft_earth_aoe"),
	(assign, ":spell_cost", 40),
(else_try),
	(eq, ":spell", "str_magecraft_earth_buff"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_earth_trap"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_earth_unique1"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_earth_unique2"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_earth_unique3"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_earth_unique4"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_earth_unique5"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_alchemy_flashair"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_alchemy_memory"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_alchemy_thought"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_alchemy_transmutation"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_alchemy_soulseal"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_astromancy_horoscope"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_astromancy_starview"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_astromancy_starfire"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_astromancy_starcall"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_sacrament_baptism"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_sacrament_bless"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_sacrament_wound"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_sacrament_purify"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_sacrament_miracle"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_countermagic_rebound"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_countermagic_block"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_countermagic_mana"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_curse_geis"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_curse_sgeis"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_curse_wound"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_curse_cripple"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_curse_blind"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_transfer_mana"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_transfer_crest"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_transfer_crest_add"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_healing"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_healing_wound"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_healing_venom"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_healing_curse"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_jewel_storage"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_jewel_bind"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_reincarnation_familiar"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_reincarnation_reborn"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_perception_other"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_perception_share"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_consciousness_remote"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_consciousness_possess"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_necromancy_raise"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_necromancy_bomb"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_necromancy_commune"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_golemancy_create"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_golemancy_collapse"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_golemancy_detonate"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_numerology_starbow"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_numerology_starmine"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_numerology_fortune"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_numerology_number"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_projection_gradation"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_projection_trace"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_reinforcement_enhance"),
	(assign, ":spell_cost", 10),
(else_try),
	(eq, ":spell", "str_magecraft_reinforcement_strengthen"),
	(assign, ":spell_cost", 10),
(else_try),
	(eq, ":spell", "str_magecraft_alteration_element"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_alteration_spell"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_alteration_effect"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_memory_relationship"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_memory_personality"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_memory_alter"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_memory_thought"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_command_dictate"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_command_restrict"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_runes_ansuz"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_runes_athngabla"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_runes_berkana"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_runes_ehwaz"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_runes_gandr"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_runes_kenaz"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_runes_ansuzeihwaz"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_spatial_teleport"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_spatial_gravity"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_spatial_warp"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_invocation_possess"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_evocation_commune"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_time_stagnate"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_time_accel"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_time_haste"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_time_slow"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_puppet_build"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_puppet_automate"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_formal_barrier"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_formal_trap"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_magecraft_formal_workshop"),
	(assign, ":spell_cost", 100),
(else_try),
	(eq, ":spell", "str_special_emiya_projection_hrunting"),
	(assign, ":spell_cost", 500),
(else_try),
	(eq, ":spell", "str_special_emiya_projection_caladbolg"),
	(assign, ":spell_cost", 500),
(else_try),
	(eq, ":spell", "str_special_emiya_projection_rho_aias"),
	(assign, ":spell_cost", 2500),
(else_try),
	(eq, ":spell", "str_special_emiya_projection_caliburn"),
	(assign, ":spell_cost", 3000),
(else_try),
	(eq, ":spell", "str_special_dsakura_shadow"),
	(assign, ":spell_cost", 500),
(try_end),	


(str_clear, s10),

# (assign, s10, ":spell_element"),
#(str_store_string, s10, ":spell_element"),
#(assign, reg9, ":spell_effect"),
#(assign, reg10, ":spell_target"),
(assign, reg11, ":spell_cost"),
]),


  # (script_magic_cast, "str_spell"),
  # Input: Caster Agent
  # Output:	None
  # Usage:	Called by various scripts to cast a spell.
  #
  #			Doesn't have a fail-state, but requires set-up to work
  #			properly. The agent (or troop) requires slots to be set
  #			up to determine what spell they are casting, how much mana
  #			they have (maximum), as well as how much mana they have
  #			currently.
  #
  #			Makes 
("magic_cast",
[
(set_fixed_point_multiplier, 100),

# Inputs
(store_script_param, ":caster_agent", 1),

(gt, ":caster_agent", 0),

# Used Variable Declarations
# (assign, ":spell_target", 0),
(agent_get_troop_id, ":caster_agent_troop", ":caster_agent"),	# Who are they?
(troop_get_slot, ":max_minions", ":caster_agent_troop", slot_fate_max_minions),

(agent_get_wielded_item, ":equipped_item", ":caster_agent", 0), # Right Hand
(agent_get_team, ":caster_team", ":caster_agent"),
(agent_get_look_position, pos2, ":caster_agent"),
(agent_get_position, pos3, ":caster_agent"),
(mission_get_time_speed, ":time_speed"),
(agent_get_slot, ":flying", ":caster_agent", fate_agent_is_flying),
(agent_get_slot, ":minions", ":caster_agent", fate_agent_current_minions),
(agent_get_animation, ":cur_anim", ":caster_agent", 1),
(store_mission_timer_a, ":time"),

(assign, ":nearest_enemy", 0),
(assign, ":nearest_e_dist", 1000),

(try_for_agents, ":enemy"),
	(neq, ":enemy", ":caster_agent"),
	(agent_get_team, ":enemy_team", ":enemy"),
	(teams_are_enemies, ":caster_team", ":enemy_team"),
	(agent_get_position, pos4, ":enemy"),
	(get_distance_between_positions, ":e_dist", pos3, pos4),
	(le, ":e_dist", ":nearest_e_dist"),
	(assign, ":nearest_e_dist", ":e_dist"),
	(assign, ":nearest_enemy", ":enemy"),
	(copy_position, pos5, pos4),
(try_end),

(try_begin),
	(eq, "$g_fate_battle_servants_on", 1),
	(gt, "$g_fate_battle_servant_1_agent_id", 0),
	(agent_get_slot, ":np", "$g_fate_battle_servant_1_agent_id", slot_agent_active_noble_phantasm),
(try_end),

(agent_get_speed_modifier, ":current_speed_mod", ":caster_agent"),
(agent_get_damage_modifier, ":current_damage_mod", ":caster_agent"),
(agent_get_accuracy_modifier, ":current_accuracy_mod", ":caster_agent"),

(agent_get_slot, ":current_armor_mod", ":caster_agent", slot_agent_armor_boost),

(agent_get_slot, ":max_damage", ":caster_agent", slot_agent_original_damage),
(agent_get_slot, ":max_accuracy", ":caster_agent", slot_agent_original_accuracy),
(agent_get_slot, ":max_speed", ":caster_agent", slot_agent_original_speed),
#(agent_get_slot, ":max_rspeed", ":caster_agent", slot_agent_original_rspeed),

(val_mul, ":max_damage", 250),
(val_mul, ":max_accuracy", 250),
(val_mul, ":max_speed", 250),
#(val_mul, ":max_rspeed", 250),

(val_div, ":max_damage", 100),
(val_div, ":max_accuracy", 100),
(val_div, ":max_speed", 100),
#(val_div, ":max_rspeed", 100),

# (agent_get_slot, ":silenced", ":caster_agent", fate_agent_silenced),
# (agent_get_slot, ":stunned", ":caster_agent", fate_agent_stunned),
# (agent_get_slot, ":petrified", ":caster_agent", fate_agent_petrified),
# (agent_get_slot, ":confused", ":caster_agent", fate_agent_confused),
# (agent_get_slot, ":enraged", ":caster_agent", fate_agent_enraged),
# (agent_get_slot, ":frozen", ":caster_agent", fate_agent_frozen),

(neg|agent_slot_ge, ":caster_agent", fate_agent_silenced, 1),
(neg|agent_slot_ge, ":caster_agent", fate_agent_stunned, 1),
(neg|agent_slot_ge, ":caster_agent", fate_agent_petrified, 1),
(neg|agent_slot_ge, ":caster_agent", fate_agent_confused, 1),
(neg|agent_slot_ge, ":caster_agent", fate_agent_enraged, 1),
(neg|agent_slot_ge, ":caster_agent", fate_agent_frozen, 1),


# (le, ":silenced", 0),
# (le, ":stunned", 0),
# (le, ":petrified", 0),
# (le, ":confused", 0),
# (le, ":enraged", 0),
# (le, ":frozen", 0),

(copy_position, pos5, pos3),
(copy_position, pos4, pos3),

(position_move_y, pos5, 100),

(assign, ":target_servant", -1),

(try_for_agents, ":servant"),
	(agent_get_troop_id, ":servant_troop", ":servant"),
	(troop_slot_eq, ":servant_troop", slot_fate_classification, "str_classification_servant"),
	(troop_slot_eq, ":servant_troop", slot_fate_master, ":caster_agent_troop"),
	(assign, ":target_servant", ":servant"),
	(store_agent_hit_points, ":servant_health", ":target_servant", 0),
(try_end),


(store_agent_hit_points, ":health", ":caster_agent", 0),

(position_set_z_to_ground_level, pos5),
(agent_get_team, ":caster_team", ":caster_agent"),
(set_spawn_position, pos5),
			
(try_begin),
	(troop_is_hero, ":caster_agent_troop"), # Is this agent a mob or someone important
	(troop_get_slot, ":current_mana", ":caster_agent_troop", slot_fate_cur_mana),
	(troop_get_slot, ":spell_slot", ":caster_agent_troop", slot_fate_chosen_spell),
(else_try),
	(agent_get_slot, ":current_mana", ":caster_agent", fate_agnt_cur_mana),
	(agent_get_slot, ":spell_slot", ":caster_agent", fate_agnt_chosen_spell),
(try_end),

(try_begin),
	(eq, ":spell_slot", 1),
	(troop_get_slot, ":chosen_spell", ":caster_agent_troop", slot_fate_spell_slot_1),
	(troop_get_slot, ":spell_strength", ":caster_agent_troop", slot_fate_spell_slot_1_strength_modifier),
	(troop_get_slot, ":spell_target", ":caster_agent_troop", slot_fate_spell_slot_1_target_modifier),
(else_try),
	(eq, ":spell_slot", 2),
	(troop_get_slot, ":chosen_spell", ":caster_agent_troop", slot_fate_spell_slot_2),
	(troop_get_slot, ":spell_strength", ":caster_agent_troop", slot_fate_spell_slot_2_strength_modifier),
	(troop_get_slot, ":spell_target", ":caster_agent_troop", slot_fate_spell_slot_2_target_modifier),
(else_try),
	(eq, ":spell_slot", 3),
	(troop_get_slot, ":chosen_spell", ":caster_agent_troop", slot_fate_spell_slot_3),
	(troop_get_slot, ":spell_strength", ":caster_agent_troop", slot_fate_spell_slot_3_strength_modifier),
	(troop_get_slot, ":spell_target", ":caster_agent_troop", slot_fate_spell_slot_3_target_modifier),
(else_try),
	(eq, ":spell_slot", 4),
	(troop_get_slot, ":chosen_spell", ":caster_agent_troop", slot_fate_spell_slot_4),
	(troop_get_slot, ":spell_strength", ":caster_agent_troop", slot_fate_spell_slot_4_strength_modifier),
	(troop_get_slot, ":spell_target", ":caster_agent_troop", slot_fate_spell_slot_4_target_modifier),
(try_end),

(try_begin),
	(eq, ":spell_target", 0),
	(val_max, ":spell_target", target_self),
(try_end),

(call_script, "script_determine_spell_tags", ":chosen_spell"),

# (assign, ":spell_element", s10),
# (assign, ":spell_effect", reg9),
# (assign, ":spell_target", reg10),
(assign, ":spell_cost", reg11),

	(try_begin),
		(eq, ":spell_strength", "str_magecraft_spell_enhance_double"),
		(assign, ":multiplier", 200),
	(else_try),
		(eq, ":spell_strength", "str_magecraft_spell_enhance_triple"),
		(assign, ":multiplier", 300),
	(else_try),
		(eq, ":spell_strength", "str_magecraft_spell_enhance_square"),
		(assign, ":multiplier", 400),
	(else_try),
		(eq, ":spell_strength", "str_magecraft_spell_enhance_half"),
		(assign, ":multiplier", 50),
	(else_try),
		(eq, ":spell_strength", "str_magecraft_spell_enhance_quarter"),
		(assign, ":multiplier", 25),
	(else_try),
		(assign, ":multiplier", 100),
	(try_end),
	
(val_mul, ":spell_cost", ":multiplier"),
(val_div, ":spell_cost", 100),


 (str_store_string, s11, ":chosen_spell"),
 # (display_message, "@I should be using the spell {s11}"),

(try_begin),
	(gt, ":spell_cost", ":current_mana"),
	(try_begin),
		(agent_is_non_player, ":caster_agent"),
	(else_try),
		(display_message, "@You fail to conjure the energy for this spell", 0x55ffff),
	(try_end),
(try_end),

(le, ":spell_cost", ":current_mana"),

(try_begin),
	(eq, ":chosen_spell", "str_magecraft_fire_bolt"),
	(position_move_z, pos2, 150),
	(position_move_y, pos2, 100, 0),
	(add_missile, ":caster_agent", pos2, 5000, "itm_firebolt", 0, "itm_firebolt", 0),
	(agent_set_animation, ":caster_agent", "anim_magecraft_cast_forward_right", 1),
(else_try),
	(eq, ":chosen_spell", "str_magecraft_fire_aoe"),
	(position_move_z, pos2, 150),
	(position_move_y, pos2, 100, 0),
	(add_missile, ":caster_agent", pos2, 5000, "itm_fireaoe", 0, "itm_fireaoe", 0),
	(agent_set_animation, ":caster_agent", "anim_magecraft_cast_raise_right", 1),
(else_try),
	(eq, ":chosen_spell", "str_magecraft_fire_buff"),
	(try_begin),
		(neq, ":equipped_item", -1),
			# (assign, ":spell_cost", 50),
		(item_set_slot, ":equipped_item", fate_weapon_enhancement, "str_dmg_type_fire"),
	(else_try),
		(try_begin),
			(agent_is_non_player, ":caster_agent"),
		(else_try),
			(display_message, "@You fail to buff your weapon"),
		(try_end),
	(try_end),
(else_try),
	(eq, ":chosen_spell", "str_magecraft_fire_unique1"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(else_try),
		
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_fire_unique2"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
			(agent_get_slot, ":burn_time", ":nearest_enemy", fate_agent_burned),
			(val_add, ":burn_time", 3),
			(agent_set_slot, ":nearest_enemy", fate_agent_burned, ":burn_time"),
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
		(position_move_z, pos2, 150),
		(position_move_y, pos2, 100, 0),
		(cast_ray, reg0, pos10, pos2),
		# (get_distance_between_positions, ":dist", pos2, pos10),
		# (try_for_agents, ":agent", pos2, ":dist"),
			# (neq, ":agent", ":caster_agent"),
			# (agent_get_position, pos8, ":agent"),
			# (position_transform_position_to_local, pos7, pos8, pos2),
			# (position_get_x, ":x", pos7),
			# (position_get_y, ":y", pos7),
			# (is_between, ":x", -50, 50),
		# (try_end),
		
		(try_for_agents, ":agent", pos10, 250),
			(neq, ":agent", ":caster_agent"),
			(agent_get_slot, ":burn_time", ":agent", fate_agent_burned),
			(val_add, ":burn_time", 10),
			(agent_set_slot, ":agent", fate_agent_burned, ":burn_time"),
		(try_end),
		(set_spawn_position, pos10),
		
		(spawn_scene_prop, "spr_sphere_marker"),
		(prop_instance_set_scale, reg0, 250, 250, 250),
		(scene_prop_fade_out, reg0, 100),
		(scene_prop_set_prune_time, reg0, 0),
		
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_fire_unique3"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_fire_unique4"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_water_bolt"),
	(position_move_z, pos2, 150),
	(position_move_y, pos2, 100, 0),
	
	(val_div, ":multiplier", 100),
	
	(try_for_range, reg0, 0, ":multiplier"),
		(add_missile, ":caster_agent", pos2, 5000, "itm_waterbolt", 0, "itm_waterbolt", 0),
	(try_end),
	
	(agent_set_animation, ":caster_agent", "anim_magecraft_cast_forward_right", 1),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_water_aoe"),
	(position_move_z, pos2, 150),
	(position_move_y, pos2, 100, 0),
	(add_missile, ":caster_agent", pos2, 5000, "itm_wateraoe", 0, "itm_wateraoe", 0),
	(agent_set_animation, ":caster_agent", "anim_magecraft_cast_forward_right", 1),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_water_trap"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_water_unique1"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_water_unique2"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_water_unique3"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_wind_bolt"),
	# (position_move_z, pos2, 150),
	# (position_move_y, pos2, 100, 0),
	# (add_missile, ":caster_agent", pos2, 5000, "itm_windbolt", 0, "itm_windbolt", 0),
	# (agent_set_animation, ":caster_agent", "anim_magecraft_cast_forward_right", 1),
	(position_move_z, pos2, 150),
	(position_move_y, pos2, 100, 0),
	(cast_ray, reg0, pos30, pos2, 8000),
	(call_script, "script_fate_beam_attack", ":caster_agent", pos30, 200, "itm_gandr", 5),
	#(assign, ":spell_cost", 250),
	(val_add, "$g_fate_cam_shake_frames", 50),
	(agent_set_animation, ":caster_agent", "anim_magecraft_cast_forward_right", 1),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_wind_aoe"),
	(position_move_z, pos2, 150),
	(position_move_y, pos2, 100, 0),
	(add_missile, ":caster_agent", pos2, 1000, "itm_windaoe", 0, "itm_windaoe", 0),
	(agent_set_animation, ":caster_agent", "anim_magecraft_cast_forward_right", 1),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_wind_buff"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_wind_unique1"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_wind_unique2"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_wind_unique3"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_wind_unique4"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_wind_unique5"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
	(try_begin),
		(agent_slot_ge, ":caster_agent", slot_agent_has_enhanced_legs, 1),
		(display_message, "@My legs are already enhanced!"),
	(else_try),
		(agent_set_slot, ":caster_agent", slot_agent_has_enhanced_legs, 3),
	(try_end),
(else_try),
	(eq, ":chosen_spell", "str_magecraft_earth_bolt"),
	(position_move_z, pos2, 150),
	(position_move_y, pos2, 100, 0),
	(add_missile, ":caster_agent", pos2, 5000, "itm_earthbolt", 0, "itm_earthbolt", 0),
	(agent_set_animation, ":caster_agent", "anim_magecraft_cast_forward_right", 1),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_earth_aoe"),
	(position_move_z, pos2, 150),
	(position_move_y, pos2, 100, 0),
	(add_missile, ":caster_agent", pos2, 5000, "itm_earthaoe", 0, "itm_earthaoe", 0),
	(agent_set_animation, ":caster_agent", "anim_magecraft_cast_forward_right", 1),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_earth_buff"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_earth_trap"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_earth_unique1"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_earth_unique2"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_earth_unique3"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_earth_unique4"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_earth_unique5"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_alchemy_flashair"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_alchemy_memory"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_alchemy_thought"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_alchemy_transmutation"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_alchemy_soulseal"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_astromancy_horoscope"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_astromancy_starview"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_astromancy_starfire"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_astromancy_starcall"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_sacrament_baptism"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_sacrament_bless"),
	
	(try_begin),
		(neq, ":equipped_item", -1),
			# (assign, ":spell_cost", 50),
		(item_set_slot, ":equipped_item", fate_weapon_enhancement, "str_dmg_type_divine"),
	(else_try),
		(display_message, "@FAILED CAST!"),
	(try_end),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_sacrament_wound"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_sacrament_purify"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_sacrament_miracle"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_countermagic_rebound"),
(else_try),
	(eq, ":chosen_spell", "str_magecraft_countermagic_block"),
(else_try),
	(eq, ":chosen_spell", "str_magecraft_countermagic_mana"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_curse_geis"),
(else_try),
	(eq, ":chosen_spell", "str_magecraft_curse_sgeis"),
(else_try),
	(eq, ":chosen_spell", "str_magecraft_curse_wound"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
		(gt, ":target_servant", 0),
		(val_add, ":servant_health", 10),
		(agent_set_hit_points, ":target_servant", ":servant_health"),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_curse_cripple"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_curse_blind"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_transfer_mana"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_transfer_crest"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_transfer_crest_add"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_healing"),
	
	(assign, ":healing_amount", 10),
	(val_mul, ":healing_amount", ":multiplier"),
	(val_div, ":healing_amount", 100),
	
	
	(try_begin),
		(eq, ":spell_target", target_self),
		(val_add, ":health", ":healing_amount"),
		(agent_set_hit_points, ":caster_agent", ":health"),
	(else_try),
		(eq, ":spell_target", target_servant),
		(gt, ":target_servant", 0),
		(val_add, ":servant_health", ":healing_amount"),
		(agent_set_hit_points, ":target_servant", ":servant_health"),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_healing_wound"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
		# (eq, "$g_fate_battle_servants_on", 1),
		# (eq, "$g_fate_battle_player_is_master", 1),
		(gt, ":target_servant", 0),
		(try_for_range, ":body_part", 0, 6),
			(store_add, ":limb_slot", fate_agent_wounds_larm, ":body_part"),
			(agent_get_slot, ":limb_health", ":target_servant", ":limb_slot"),
			(val_sub, ":limb_health", 10),
			(val_max, ":limb_health", 0),
			(agent_set_slot, ":target_servant", ":limb_slot", ":limb_health"),
		(try_end),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(else_try),
		(display_message, "@No Target"),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_healing_venom"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
		(gt, ":target_servant", 0),
		(val_add, ":servant_health", 10),
		(agent_set_hit_points, ":target_servant", ":servant_health"),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
			(eq, ":nearest_enemy", 0),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_healing_curse"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
		(gt, ":target_servant", 0),
		(val_add, ":servant_health", 10),
		(agent_set_hit_points, ":target_servant", ":servant_health"),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_jewel_storage"),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_jewel_bind"),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_reincarnation_familiar"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_reincarnation_reborn"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_perception_other"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_perception_share"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_consciousness_remote"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_consciousness_possess"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_necromancy_raise"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
	(spawn_agent, "trp_puppet"),
	(agent_set_team, reg0, ":caster_team"),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_necromancy_bomb"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_necromancy_commune"),
(else_try),
	(eq, ":chosen_spell", "str_magecraft_golemancy_create"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
	(try_begin),
		(lt, ":minions", ":max_minions"),
		
		(spawn_agent, "trp_stone_golem"),
		(agent_set_team, reg0, ":caster_team"),
		(agent_set_is_alarmed, reg0, 1),
		(agent_add_relation_with_agent, reg0, ":caster_agent", 1),
		(val_add, ":minions", 1),
	(else_try),
		(agent_is_non_player, ":caster_agent"),
	(else_try),
		(display_message, "@You have too many golems as it is!"),
	(try_end),
	
	
	(agent_set_team, reg0, ":caster_team"),
(else_try),
	(eq, ":chosen_spell", "str_magecraft_golemancy_collapse"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_golemancy_detonate"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_numerology_starbow"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_numerology_starmine"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_numerology_fortune"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_numerology_number"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_projection_gradation"),
	(call_script, "script_fate_projection_casts", ":caster_agent", -1),
	# (play_sound_at_position, "snd_fate_spell_trace", pos2),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_projection_trace"),
	(call_script, "script_fate_projection_casts", ":caster_agent", -1),
	(play_sound_at_position, "snd_fate_spell_trace", pos2),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_reinforcement_enhance"),
	
	(try_begin),
		(eq, ":spell_target", target_weapon),
		
		(try_begin),
		(le, ":current_damage_mod", ":max_damage"),
			(val_add, ":current_damage_mod", ":spell_cost"),
			(val_mul, ":spell_cost", 5),
			
			(agent_set_damage_modifier, ":caster_agent", ":current_damage_mod"),
		(else_try),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@Weapon is already as enhanced as it can go!"),
			(try_end),
			
		(try_end),
	(else_try),
		(eq, ":spell_target", target_eyes),
		
		(try_begin),
		(le, ":current_accuracy_mod", ":max_accuracy"),
			
			(val_add, ":current_accuracy_mod", ":spell_cost"),
			(val_mul, ":spell_cost", 5),
			
			
			(agent_set_accuracy_modifier, ":caster_agent", ":current_accuracy_mod"),
		(else_try),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@My eyes are already as enhanced as they can go!"),
			(try_end),
			
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_legs),
		
		(try_begin),
		(le, ":current_speed_mod", ":max_speed"),
			
			(val_add, ":current_speed_mod", ":spell_cost"),
			(val_mul, ":spell_cost", 5),
			
			(agent_set_slot, ":caster_agent", 1, slot_agent_has_enhanced_legs),
			(agent_set_speed_modifier, ":caster_agent", ":current_speed_mod"),
		(else_try),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@Legs are already as enhanced as it can go!"),
			(try_end),
			
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_skin),
		
		(try_begin),
		(le, ":current_armor_mod", 250),
			
			(val_add, ":current_armor_mod", ":spell_cost"),
			(val_mul, ":spell_cost", 5),
			
			(agent_set_slot, ":caster_agent", ":current_armor_mod", slot_agent_armor_boost),
		(else_try),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@My skin cannot be tempered any further!"),
			(try_end),
			
		(try_end),
		
	(else_try),
		(display_message, "@No valid target for Reinforcement"),
	(try_end),
(else_try),
	(eq, ":chosen_spell", "str_magecraft_reinforcement_strengthen"),
	
	(try_begin),
		(eq, ":spell_target", target_weapon),
		
		(try_begin),
		(le, ":current_damage_mod", ":max_damage"),
			
			(val_add, ":current_damage_mod", ":spell_cost"),
			(val_mul, ":spell_cost", 5),
			
			
			(agent_set_damage_modifier, ":caster_agent", ":current_damage_mod"),
		(else_try),
			(assign, ":spell_cost", 0),
			
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@Weapon is already as enhanced as it can go!"),	
			(try_end),
		(try_end),
	(else_try),
		(eq, ":spell_target", target_eyes),
		
		(try_begin),
		(le, ":current_accuracy_mod", ":max_accuracy"),
			
			(val_add, ":current_accuracy_mod", ":spell_cost"),
			(val_mul, ":spell_cost", 5),
			
			
			(agent_set_accuracy_modifier, ":caster_agent", ":current_accuracy_mod"),
		(else_try),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@My eyes are already as enhanced as they can go!"),
			(try_end),
			
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_legs),
		
		(try_begin),
		(le, ":current_speed_mod", ":max_speed"),
			
			(val_add, ":current_speed_mod", ":spell_cost"),
			(val_mul, ":spell_cost", 5),
			
			(agent_set_slot, ":caster_agent", 1, slot_agent_has_enhanced_legs),
			(agent_set_speed_modifier, ":caster_agent", ":current_speed_mod"),
		(else_try),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@Legs are already as enhanced as it can go!"),
			(try_end),
			
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_skin),
		
		(try_begin),
		(le, ":current_armor_mod", 250),
			
			(val_add, ":current_armor_mod", ":spell_cost"),
			(val_mul, ":spell_cost", 5),
			
			(agent_set_slot, ":caster_agent", ":current_armor_mod", slot_agent_armor_boost),
		(else_try),
			(assign, ":spell_cost", 0),
			(neq|agent_is_non_player, ":caster_agent"),
			
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@My skin cannot be tempered any further!"),
			(try_end),
			
		(try_end),
		
	(else_try),
		(display_message, "@No valid target for Reinforcement"),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_alteration_element"),
	
	(try_begin),
		(eq, ":spell_target", target_weapon),
		
		(try_begin),
		(le, ":current_damage_mod", ":max_damage"),
			
			(val_add, ":current_damage_mod", ":spell_cost"),
			(val_mul, ":spell_cost", 5),
			
			
			(agent_set_damage_modifier, ":caster_agent", ":current_damage_mod"),
		(else_try),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@Weapon is already as enhanced as it can go!"),
			(try_end),

		(try_end),
		
	(else_try),
		(display_message, "@No valid target for the Alteration"),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_alteration_spell"),
	
	(try_begin),
		(eq, ":spell_target", target_weapon),
		
		(try_begin),
		(le, ":current_damage_mod", ":max_damage"),
			
			(val_add, ":current_damage_mod", ":spell_cost"),
			(val_mul, ":spell_cost", 5),
			
			
			(agent_set_damage_modifier, ":caster_agent", ":current_damage_mod"),
		(else_try),
			(assign, ":spell_cost", 0),
			
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@Weapon is already as enhanced as it can go!"),
			(try_end),
		(try_end),
		
	(else_try),
		(display_message, "@No valid target for the Alteration"),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_alteration_effect"),
	
	(try_begin),
		(eq, ":spell_target", target_weapon),
		
		(try_begin),
		(le, ":current_damage_mod", ":max_damage"),
			
			(val_add, ":current_damage_mod", ":spell_cost"),
			(val_mul, ":spell_cost", 5),
			
			
			(agent_set_damage_modifier, ":caster_agent", ":current_damage_mod"),
		(else_try),
			(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@Weapon is already as enhanced as it can go!"),
			(try_end),	
		(try_end),
		
	(else_try),
		(display_message, "@No valid target for the Alteration"),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_memory_relationship"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_memory_personality"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_memory_alter"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_memory_thought"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_command_dictate"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_command_restrict"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_runes_ansuz"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_runes_athngabla"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_runes_berkana"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_runes_ehwaz"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_runes_gandr"),
	
	(position_move_z, pos2, 150),
	(position_move_y, pos2, 100, 0),
	# (particle_system_burst, "psys_fireplace_fire_big", pos2, 1),
	
	(add_missile, ":caster_agent", pos2, 5000, "itm_gandr", 0, "itm_gandr", 0),
	
	(agent_set_animation, ":caster_agent", "anim_magecraft_cast_forward_right", 1),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_runes_kenaz"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_runes_ansuzeihwaz"),
	
	
	(try_begin),
		(eq, ":spell_target", target_self),
		(lt, ":current_speed_mod", 200),
		
		(val_add, ":current_speed_mod", ":spell_cost"),
		(agent_set_speed_modifier, ":caster_agent", 200),
		(agent_set_slot, ":caster_agent", slot_agent_has_enhanced_legs, 2),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_spatial_teleport"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_spatial_gravity"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_spatial_warp"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_invocation_possess"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_evocation_commune"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_time_stagnate"),
	(try_begin),
		(eq, ":time_speed", 100),
		(mission_set_time_speed, 400),
		#(agent_set_visibility, ":caster_agent", 0),
		(agent_set_animation, ":caster_agent", "anim_fall_chest_front"),
		(agent_set_no_dynamics, ":caster_agent", 1),
		(agent_set_speed_modifier, ":caster_agent", 25),
		(play_sound_at_position, "snd_fate_spell_stagnate", pos3),
		# (assign, ":spell_cost", 200),
		(try_for_agents, ":agent"),
			(agent_get_team, ":a_team", ":agent"),
			(teams_are_enemies, ":a_team", ":caster_team"),
			(agent_add_relation_with_agent, ":agent", ":caster_agent", 0),
		(try_end),
	(else_try),
		(mission_set_time_speed, 100),
		#(agent_set_visibility, ":caster_agent", 1),
		(agent_set_no_dynamics, ":caster_agent", 0),
		(agent_set_animation, ":caster_agent", "anim_resurrection"),
		#(agent_set_animation_progress, ":caster_agent", 1),
		(agent_set_speed_modifier, ":caster_agent", 100),
		(try_for_agents, ":agent"),
			(agent_get_team, ":a_team", ":agent"),
			(teams_are_enemies, ":a_team", ":caster_team"),
			(agent_add_relation_with_agent, ":agent", ":caster_agent", -1),
		(try_end),
		(assign, ":spell_cost", 0),
	(try_end),
(else_try),
	(eq, ":chosen_spell", "str_magecraft_time_accel"),
	(try_begin),
		(eq, ":time_speed", 100),
		
		(try_begin),
			(gt, ":multiplier", 100),
			
			(assign, ":mod_speed", ":multiplier"),
			(store_div, ":multiplier_100", ":multiplier", 100),
			(store_div, ":mod_time", 100, ":multiplier_100"),
			
		(else_try),
			(eq, ":multiplier", 100),
			(assign, ":mod_speed", 150),
			(assign, ":mod_time", 75),
		(else_try),
			(store_sub, ":mod_time", 100, ":multiplier"),
			(val_add, ":mod_speed", 100, ":multiplier"),
		(try_end),
		
		
		(mission_set_time_speed, ":mod_time"), 
		
		(agent_set_speed_modifier, ":caster_agent", ":mod_speed"),
		
		(play_sound_at_position, "snd_fate_spell_double_accel", pos3),
		
	(else_try),
		(mission_set_time_speed, 100),
		(agent_set_speed_modifier, ":caster_agent", 100),
		(assign, ":spell_cost", 0),
	(try_end),
	

(else_try),
	(eq, ":chosen_spell", "str_magecraft_time_haste"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_time_slow"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
		
		(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
			(try_begin),
				(agent_is_non_player, ":caster_agent"),
			(else_try),
				(display_message, "@You failed to locate an enemy"),
			(try_end),
		(else_try),
		
		(try_end),
		
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_puppet_build"),
	
	(spawn_agent, "trp_puppet"),
	(agent_set_team, reg0, ":caster_team"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_puppet_automate"),
	
	(try_begin),
		(eq, ":spell_target", target_self),
	(else_try),
		(eq, ":spell_target", target_servant),
	(else_try),
		(eq, ":spell_target", target_enemy),
	(else_try),
		(eq, ":spell_target", target_area),
	(else_try),
		(eq, ":spell_target", target_lookpos),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_magecraft_formal_barrier"),
(else_try),
	(eq, ":chosen_spell", "str_magecraft_formal_trap"),
(else_try),
	(eq, ":chosen_spell", "str_magecraft_formal_workshop"),
(else_try),
	(eq, ":chosen_spell", "str_special_emiya_projection_hrunting"),
	#(call_script, "script_fate_projection_casts", ":caster_agent", "itm_hrunting"),
	(agent_unequip_item, ":caster_agent", "itm_emiya_arrows"),
	(agent_equip_item, ":caster_agent", "itm_hrunting"),
(else_try),
	(eq, ":chosen_spell", "str_special_emiya_projection_caladbolg"),
	#(call_script, "script_fate_projection_casts", ":caster_agent", "itm_caladbolg_ii"),
	(agent_unequip_item, ":caster_agent", "itm_emiya_arrows"),
	(agent_equip_item, ":caster_agent", "itm_caladbolg_ii"),
(else_try),
	(eq, ":chosen_spell", "str_special_emiya_projection_rho_aias"),
	#(call_script, "script_fate_projection_casts", ":caster_agent", "itm_caladbolg_ii"),
	(position_move_y, pos3, 125),
	(position_move_z, pos3, 200),
	# (position_set_z_to_ground_level, pos3),
	(position_rotate_x, pos3, -90),
	(set_spawn_position, pos3),
	(spawn_scene_prop, "spr_rho_seven"),
	(scene_prop_set_slot, reg0, fate_prop_owner, ":caster_agent"),
	(scene_prop_set_slot, reg0, fate_prop_activated, 0),
	(scene_prop_set_slot, reg0, fate_prop_spawn_time, ":time"),
(else_try),
	(eq, ":chosen_spell", "str_special_emiya_projection_caliburn"),
	#(call_script, "script_fate_projection_casts", ":caster_agent", "itm_caliburn"),
	
	(agent_get_wielded_item, ":equipped_item", ":caster_agent", 0),
	(try_begin),
		(gt, ":equipped_item", 0),
		(agent_unequip_item, ":caster_agent", ":equipped_item"),
	(try_end),
	(agent_equip_item, ":caster_agent", "itm_caliburn", 3),
	(agent_set_wielded_item, ":caster_agent", "itm_caliburn"),
(else_try),
	(eq, ":chosen_spell", "str_special_dsakura_shadow"),
	
	(try_begin),
		(lt, ":minions", ":max_minions"),
		(store_random_in_range, ":chance", 0, 500),
		(val_mod, ":chance", 5),
		
		(try_for_range, reg0, 0, ":chance"),
			(spawn_agent, "trp_sakura_shadow"),
			(agent_set_team, reg0, ":caster_team"),
			(agent_set_is_alarmed, reg0, 1),
			(agent_add_relation_with_agent, reg0, ":caster_agent", 1),
			(val_add, ":minions", 1),
		(try_end),
	(else_try),
		(agent_is_non_player, ":caster_agent"),
	(else_try),
		(display_message, "@You have too many minions as it is!"),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_01_01"),
	
	(try_begin),
		(eq, ":flying", 0),
		(agent_set_no_dynamics, ":caster_agent", 1),
		
		(position_move_z, 100, pos3),
		
		(agent_set_position, ":caster_agent", pos3),
		(agent_set_animation, ":caster_agent", "anim_flying_start"),
		(agent_set_slot, ":caster_agent", fate_agent_is_flying, 1),
		
		(troop_set_slot, ":caster_agent_troop", slot_fate_spell_slot_1, "str_special_caster_01_01_flying"),
		(troop_set_slot, ":caster_agent_troop", slot_fate_spell_slot_2, "str_special_caster_01_02_flying"),
		(troop_set_slot, ":caster_agent_troop", slot_fate_spell_slot_3, "str_special_caster_01_03_flying"),
		(troop_set_slot, ":caster_agent_troop", slot_fate_spell_slot_4, "str_special_caster_01_04_flying"),
		
	(else_try),
		(agent_is_non_player, ":caster_agent"),
	(else_try),
		(display_message, "@You are already flying! Change your spell slot!"),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_01_02"),
	
	(position_move_z, pos2, 150),
	(position_move_y, pos2, 100, 0),
	(cast_ray, reg0, pos30, pos2, 8000),
	(call_script, "script_fate_beam_attack", ":caster_agent", pos30, 200, "itm_gandr"),
	(assign, ":spell_cost", 250),
(else_try),
	(eq, ":chosen_spell", "str_special_caster_01_03"),
	
	(try_begin),
	(eq, ":nearest_enemy", 0),
	(assign, ":spell_cost", 0),
		(try_begin),
			(agent_is_non_player, ":caster_agent"),
		(else_try),
			(display_message, "@You failed to locate an enemy"),
		(try_end),
	(else_try),
		(agent_set_slot, ":nearest_enemy", fate_agent_ensnared, 15),
	(try_end),

(else_try),
	(eq, ":chosen_spell", "str_special_caster_01_04"),
	(try_begin),
		(lt, ":minions", ":max_minions"),
		(store_random_in_range, ":chance", 0, 500),
		(val_mod, ":chance", 2),
		
		(try_begin),
			(eq, ":chance", 0),
			(assign, ":summon", "trp_dtooth_warrior"),
		(else_try),
			(assign, ":summon", "trp_dtooth_archer"),
		(try_end),
		
		(spawn_agent, ":summon"),
		(agent_set_team, reg0, ":caster_team"),
		(agent_set_is_alarmed, reg0, 1),
		(agent_add_relation_with_agent, reg0, ":caster_agent", 1),
		
		(val_add, ":minions", 1),
	(else_try),
		(agent_is_non_player, ":caster_agent"),
	(else_try),
		(display_message, "@You have too many minions as it is!"),
	(try_end),
	
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_01_01_flying"),
	(try_begin),
		(eq, ":flying", 1),
		(agent_set_no_dynamics, ":caster_agent", 0),
		(agent_set_slot, ":caster_agent", fate_agent_is_flying, 0),
		
		(position_set_z_to_ground_level, pos2),
		(agent_set_position, ":caster_agent", pos2),
		
		(agent_set_animation, ":caster_agent", "anim_flying_break"),
		
		(troop_set_slot, ":caster_agent_troop", slot_fate_spell_slot_1, "str_special_caster_01_01"),
		(troop_set_slot, ":caster_agent_troop", slot_fate_spell_slot_2, "str_special_caster_01_02"),
		(troop_set_slot, ":caster_agent_troop", slot_fate_spell_slot_3, "str_special_caster_01_03"),
		(troop_set_slot, ":caster_agent_troop", slot_fate_spell_slot_4, "str_special_caster_01_04"),
		
		(assign, ":spell_cost", 0),
	(else_try),
		(display_message, "@You have already landed! Change your spell slot!"),
	(try_end),
(else_try),
	(eq, ":chosen_spell", "str_special_caster_01_02_flying"),
	
	(position_move_z, pos2, 150),
	(position_move_y, pos2, 150, 0),
	(cast_ray, reg0, pos30, pos2, 8000),
	(call_script, "script_fate_beam_attack", ":caster_agent", pos30, 150, "itm_gandr"),
	(assign, ":spell_cost", 350),
(else_try),
	(eq, ":chosen_spell", "str_special_caster_01_03_flying"),
	
	(position_move_z, pos2, 150),
	(position_move_y, pos2, 150, 0),
	(cast_ray, reg0, pos30, pos2, 8000),
	(particle_system_burst, "psys_fireplace_fire_big", pos30, 1),
	
	(try_for_agents, ":target", pos30, 500),
		(agent_get_team, ":tteam", ":target"),
		(teams_are_enemies, ":tteam", ":caster_team"),
		
		(agent_set_slot, ":target", fate_agent_ensnared, 10),
		
	(try_end),
	
	(init_position, pos45),
	(position_copy_rotation, pos30, pos45),
	
	(set_spawn_position, pos30),
	
	(spawn_scene_prop, "spr_sphere_marker"),
	(prop_instance_set_scale, reg0, 500, 500, 500),
	(scene_prop_fade_out, reg0, 500),
	(scene_prop_set_prune_time, reg0, 0),
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_01_04_flying"),
	
	(store_random_in_range, ":rand", 4000, 8000),
	(val_div, ":rand", 1000),
	
	(cast_ray, reg0, pos8, pos2, 8000),
	
	(try_for_range, ":beam", 0, ":rand"),
		(copy_position, pos10, pos2),
		(store_random_in_range, ":rx", -500, 500),
		(store_random_in_range, ":rz", -100, 100),
		(position_move_x, pos10, ":rx"),
		(position_move_z, pos10, ":rz"),
		(call_script, "script_lookat", pos10, pos8),
		(cast_ray, reg0, pos30, pos10, 8000),
		
		(get_distance_between_positions, ":attack_range", pos10, pos30),
		(store_div, ":burst_range", ":attack_range", 100),
		
		(try_for_range, ":burst", 0, ":burst_range"),
			(copy_position, pos7, pos10),
			(val_mul, ":burst", 100),
			(position_move_y, pos7, ":burst"),
			(particle_system_burst, "psys_laser", pos7, 75),
		(try_end),
		
		(try_for_agents, ":agent", pos30, ":attack_range"),
		  (agent_is_alive, ":agent"),
		  (neq, ":agent", ":caster_agent"),
		  
		  (val_add, ":attack_range", 200),
		  
		  (agent_get_position, pos3, ":agent"),
		  (position_transform_position_to_local, pos4, pos30, pos3),
		  
		  (position_get_x, ":val", pos4),
		  (is_between, ":val", -100, 100), #horizontal body size, 1m
		  (position_get_y, ":val", pos4),
		  (is_between, ":val", 0, ":attack_range"),#check forward only. from 15cm in front of agent
		  (position_get_z, ":val", pos4),
		  (is_between, ":val", -150, 150),#vertical body size
		  
		  
			(agent_deliver_damage_to_agent, ":caster_agent", ":agent", 100, "itm_gandr"),
		(try_end),
		
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_02_01"),	# Horror
	
	(try_begin),
		(lt, ":minions", ":max_minions"),
		(store_random_in_range, ":chance", 0, 500),
		(val_mod, ":chance", 3),
		
		(try_begin),
			(eq, ":chance", 0),
			(assign, ":summon", "trp_starfish_a"),
		(else_try),
			(eq, ":chance", 1),
			(assign, ":summon", "trp_starfish_b"),
		(else_try),
			(assign, ":summon", "trp_starfish_c"),
		(try_end),
		
		(spawn_agent, ":summon"),
		(agent_set_team, reg0, ":caster_team"),
		(agent_set_is_alarmed, reg0, 1),
		(agent_add_relation_with_agent, reg0, ":caster_agent", 1),
		
		(val_add, ":minions", 1),
	(else_try),
		(agent_is_non_player, ":caster_agent"),
	(else_try),
		(display_message, "@You have too many minions as it is!"),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_02_02"),	# Curse
	
	(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
		(try_begin),
			(agent_is_non_player, ":caster_agent"),
		(else_try),
			(display_message, "@You failed to locate an enemy"),
		(try_end),
		
	(else_try),
		(try_for_range, ":body_part", 0, 6),
			(store_add, ":limb_slot", fate_agent_wounds_larm, ":body_part"),
			(agent_get_slot, ":limb_health", ":nearest_enemy", ":limb_slot"),
			(val_add, ":limb_health", 15),
			(val_min, ":limb_health", 100),
			(agent_set_slot, ":nearest_enemy", ":limb_slot", ":limb_health"),
		(try_end),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_02_03"),	# Holy Chant
	
	(try_begin),
	(eq, ":nearest_enemy", 0),
	(assign, ":spell_cost", 0),
		(try_begin),
			(agent_is_non_player, ":caster_agent"),
		(else_try),
			(display_message, "@You failed to locate an enemy"),
		(try_end),
	(else_try),
		(agent_set_slot, ":nearest_enemy", fate_agent_confused, 15),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_02_04"),	# Void Bolt
	
	(position_move_z, pos2, 150),
	(position_move_y, pos2, -50, 0),
	
	(add_missile, ":caster_agent", pos2, 450, "itm_voidbolt", 0, "itm_voidbolt", 0),
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_02_01_prelati"),	# Giant Horror
	
	(assign, ":continue", 1),
	
	(try_for_agents, ":all"),
		(eq, ":continue", 1),
		(agent_get_troop_id, ":troop", ":all"),
		(eq, ":troop", "trp_giant_starfish"),
		(assign, ":continue", 0),
	(try_end),
	
	(try_begin),
		(eq, ":continue", 1),
		
		(spawn_agent, "trp_giant_starfish"),
		(agent_set_team, reg0, ":caster_team"),
		(agent_set_is_alarmed, reg0, 1),
		(agent_add_relation_with_agent, reg0, ":caster_agent", 1),
		
	(else_try),
		(agent_is_non_player, ":caster_agent"),
	(else_try),
		(display_message, "@You have already summoned the Giant Horror!"),
	(try_end),
	
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_02_02_prelati"),	# Abyssal Gaze, Cone AOE curse
	
	(try_for_agents, ":target", pos2, 1000),
		(agent_get_team, ":tteam", ":target"),
		(teams_are_enemies, ":tteam", ":caster_team"),
		
		(agent_get_position, pos30, ":target"),
		(call_script, "script_get_angle_between_points_180", pos2, pos30),
		
		(is_between, reg26, -1, 45),
		(is_between, reg27, -1, 45),
		
		(agent_deliver_damage_to_agent, ":target", ":caster_agent", 35, "itm_voidbolt"),
		
		(try_for_range, ":body_part", fate_agent_wounds_larm, fate_agent_wounds_larm + 6),
			(agent_get_slot, ":limb_health", ":target", ":body_part"),
			(val_add, ":limb_health", 35),
			(val_min, ":limb_health", 100),
			(agent_set_slot, ":target", ":body_part", ":limb_health"),
		(try_end),
		
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_02_03_prelati"),	# Blasphemer's Chant
	
	(try_for_agents, ":target", pos2, 1000),
		(agent_get_team, ":tteam", ":target"),
		(teams_are_enemies, ":tteam", ":caster_team"),
		(agent_set_slot, ":nearest_enemy", fate_agent_confused, 30),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_02_04_prelati"),	# Void Barrage
	
	(position_move_z, pos2, 150),
	(position_move_y, pos2, 10, 0),
	
	(store_random_in_range, ":rand", 4000, 8000),
	(val_div, ":rand", 1000),
	
	(try_for_range, ":bolts", 0, ":rand"),
		(copy_position, pos10, pos2),
		
		(store_random_in_range, ":rx", -7, 7),
		(store_random_in_range, ":rz", -7, 7),
		
		(position_rotate_x, pos10, ":rx"),
		(position_rotate_z, pos10, ":rz"),
		
		(add_missile, ":caster_agent", pos10, 900, "itm_voidbolt", 0, "itm_voidbolt", 0),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_03_01"),	# Boosts Master by two attribute levels, giving their weapon rank-C NP status
	
	
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_03_02"),	# Summon actors, wooden dolls equipped with random servant equipment
	
	(try_begin),
		(lt, ":minions", ":max_minions"),
		
		(spawn_agent, "trp_actor"),
		(agent_set_team, reg0, ":caster_team"),
		(agent_set_is_alarmed, reg0, 1),
		(agent_add_relation_with_agent, reg0, ":caster_agent", 1),
		
		(val_add, ":minions", 1),
	(else_try),
		(agent_is_non_player, ":caster_agent"),
	(else_try),
		(display_message, "@There are too many actors on stage for this play!"),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_03_03"),	# Curse damage to all limbs
	
	(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
		(try_begin),
			(agent_is_non_player, ":caster_agent"),
		(else_try),
			(display_message, "@You failed to locate an enemy"),
		(try_end),
		
	(else_try),
		(try_for_range, ":body_part", 0, 6),
			(store_add, ":limb_slot", fate_agent_wounds_larm, ":body_part"),
			(agent_get_slot, ":limb_health", ":nearest_enemy", ":limb_slot"),
			(val_add, ":limb_health", 10),
			(val_min, ":limb_health", 100),
			(agent_set_slot, ":nearest_enemy", ":limb_slot", ":limb_health"),
		(try_end),
		
		# (agent_get_slot, ":poison", ":nearest_enemy", fate_agent_poison_damage),
		# (val_add, ":poison", 5),
		# (agent_set_slot, ":nearest_enemy", fate_agent_poison_damage, ":poison"),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_03_04"),	# Silence, Rage, Confusion
	
	(try_begin),
		(eq, ":nearest_enemy", 0),
		(assign, ":spell_cost", 0),
		(try_begin),
			(agent_is_non_player, ":caster_agent"),
		(else_try),
			(display_message, "@You failed to locate an enemy"),
		(try_end),
		
	(else_try),
		(agent_set_slot, ":nearest_enemy", fate_agent_confused, 15),
		(agent_set_slot, ":nearest_enemy", fate_agent_silenced, 15),
		(agent_set_slot, ":nearest_enemy", fate_agent_enraged, 30),
		(agent_set_slot, ":nearest_enemy", fate_agent_weakened, 15),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_04_01"),	# Defensive Golem, extremely slow, very high defensive stats
	
	(try_begin),
		(lt, ":minions", ":max_minions"),
		
		(spawn_agent, "trp_golem_defense"),
		(agent_set_team, reg0, ":caster_team"),
		(agent_set_is_alarmed, reg0, 1),
		(agent_add_relation_with_agent, reg0, ":caster_agent", 1),
		
		(val_add, ":minions", 1),
	(else_try),
		(agent_is_non_player, ":caster_agent"),
	(else_try),
		(display_message, "@There are too many actors on stage for this play!"),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_04_02"),  # Ranged Golem, cannot move, but can unleash a barrage, very weak
	
	(try_begin),
		(lt, ":minions", ":max_minions"),
		
		(spawn_agent, "trp_golem_ranged"),
		(agent_set_team, reg0, ":caster_team"),
		(agent_set_is_alarmed, reg0, 1),
		(agent_add_relation_with_agent, reg0, ":caster_agent", 1),
		
		(val_add, ":minions", 1),
	(else_try),
		(agent_is_non_player, ":caster_agent"),
	(else_try),
		(display_message, "@There are too many actors on stage for this play!"),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_04_03"),	# Power Golem, Melee based
	
	(try_begin),
		(lt, ":minions", ":max_minions"),
		
		(spawn_agent, "trp_golem_power"),
		(agent_set_team, reg0, ":caster_team"),
		(agent_set_is_alarmed, reg0, 1),
		(agent_add_relation_with_agent, reg0, ":caster_agent", 1),
		
		(val_add, ":minions", 1),
	(else_try),
		(agent_is_non_player, ":caster_agent"),
	(else_try),
		(display_message, "@There are too many actors on stage for this play!"),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_04_04"),	# Magic Golem, Casts spells in order to buff allies and 
	
	(try_begin),
		(lt, ":minions", ":max_minions"),
		
		(spawn_agent, "trp_golem_magic"),
		(agent_set_team, reg0, ":caster_team"),
		(agent_set_is_alarmed, reg0, 1),
		(agent_add_relation_with_agent, reg0, ":caster_agent", 1),
		
		(val_add, ":minions", 1),
	(else_try),
		(agent_is_non_player, ":caster_agent"),
	(else_try),
		(display_message, "@There are too many actors on stage for this play!"),
	(try_end),
	
(else_try),
	(eq, ":chosen_spell", "str_special_caster_05_01"),	# Wind Blast, 1.5 m wide channel of wind 10m long
(else_try),
	(eq, ":chosen_spell", "str_special_caster_05_02"),  # Spawns 2 orbs that turn into missiles if enemies get too close
	(try_for_range, reg10, 0, 2),
		(copy_position, pos3, pos2),
		(store_random_in_range, ":rand_x", -360000, 360000),
		#(val_div, ":rand_x", 100),
		(store_random_in_range, ":rand_z", -7500, 7500),
		(val_div, ":rand_z", 100),
		
		(store_random_in_range, ":rand_y", 0, 4500),
		(val_div, ":rand_y", 100),
		
		
		# (val_add, ":rand_z", 150),
		# (position_move_x, pos3, ":rand_x"),
		(position_move_y, pos3, 125),
		
		(position_move_z, pos3, 150),
		(position_rotate_y_floating, pos3, ":rand_x"),
		(position_move_z, pos3, ":rand_z"),
		(position_move_y, pos3, ":rand_y"),
		(set_spawn_position, pos3),
		(spawn_scene_prop, "spr_water_orb"),
		(scene_prop_set_slot, reg0, fate_prop_owner, ":caster_agent"),
		(scene_prop_set_slot, reg0, fate_prop_activated, 0),
		(scene_prop_set_slot, reg0, fate_prop_spawn_time, ":time"),	 
	(try_end),
(else_try),
	(eq, ":chosen_spell", "str_special_caster_05_03"),	# Flame Tornado, 1m diameter flame tornado shoots forward
	
		#(copy_position, pos3, pos2),
		(position_move_y, pos3, 125),
		(position_set_z_to_ground_level, pos3),
		(set_spawn_position, pos3),
		(spawn_scene_prop, "spr_fire_tornado"),
		(scene_prop_set_slot, reg0, fate_prop_owner, ":caster_agent"),
		(scene_prop_set_slot, reg0, fate_prop_activated, 0),
		(scene_prop_set_slot, reg0, fate_prop_spawn_time, ":time"),	 
(else_try),
	(eq, ":chosen_spell", "str_special_caster_05_04"),	# Fissure, fissures appear, causing damage 1.5m around the prop, if they are in the prop, they are trapped
(try_end),	
		
(try_begin),
	(gt, "$g_fate_battle_infinite_mana", 0),
	
	(try_begin),
		(agent_is_non_player, ":caster_agent"),
	(else_try),
		(assign, ":spell_cost", 0),
	(try_end),
	
(else_try),
	(eq, ":np", "str_noble_phantasm_caster_02"),
	
	(assign, ":spell_cost", 0),
(try_end),

(agent_set_slot, ":caster_agent", fate_agent_current_minions, ":minions"),

(neq, ":spell_cost", 0),



(store_sub, ":cur_min_cost", ":current_mana", ":spell_cost"),
(val_max, ":cur_min_cost", 0),
		
(try_begin),
	(troop_is_hero, ":caster_agent_troop"),
	(troop_set_slot, ":caster_agent_troop", slot_fate_cur_mana, ":cur_min_cost"),
(else_try),
	(agent_set_slot, ":caster_agent", fate_agnt_cur_mana, ":cur_min_cost"),
(try_end),

(agent_set_animation, ":caster_agent", "anim_magecraft_cast_forward_right", 1),

]),

#fate_fuyuki_grail_war script
	#Assign Six Masters
#fate_great_grail_war script
	#Read Player Team Choice
	#Assign -remaining- Red Team members
	#Assign -remaining- Black Team members
#fate_moon_cell_grail_war script
	#Assign 31 Masters, for a full 32 total masters -right now we have 24
 
 #Golden Rule
  # INPUT: arg1 = troop_no, arg2 = amount
  # OUTPUT: none
  # ("fate_skill_golden_rule",
    # [
      # (store_script_param, ":troop_no", 1),
      # (store_script_param, ":amount", 2),
      
      # (troop_add_gold, ":troop_no", ":amount"),
      # (try_begin),
        # (eq, ":troop_no", "trp_player"),
        # (play_sound, "snd_money_received"),
      # (try_end),
     # ]), 
	
("fate_start_assign_skills",
    [
       (set_show_messages, 0),
	   
	   (troop_get_slot, ":mana_level", slot_fate_max_mana, "trp_player"),
		
		(troop_get_slot, ":mc_level_fire", magecraft_cat_fire, "trp_fate_spell_array"),		
		(troop_get_slot, ":mc_level_water", magecraft_cat_water, "trp_fate_spell_array"),		
		(troop_get_slot, ":mc_level_wind", magecraft_cat_wind, "trp_fate_spell_array"),	
		(troop_get_slot, ":mc_level_earth", magecraft_cat_earth, "trp_fate_spell_array"),	
		(troop_get_slot, ":mc_level_alchemy", magecraft_cat_alchemy, "trp_fate_spell_array"),		
		(troop_get_slot, ":mc_level_astromancy", magecraft_cat_astromancy, "trp_fate_spell_array"),
		(troop_get_slot, ":mc_level_sacrament", magecraft_cat_sacrament,  "trp_fate_spell_array"),	   
		(troop_get_slot, ":mc_level_countermagic", magecraft_cat_countermagic, "trp_fate_spell_array"),		 
		(troop_get_slot, ":mc_level_curse", magecraft_cat_curse, "trp_fate_spell_array"),		
		(troop_get_slot, ":mc_level_transfer", magecraft_cat_transfer, "trp_fate_spell_array"),		 
		(troop_get_slot, ":mc_level_healing", magecraft_cat_healing, "trp_fate_spell_array"),		
		(troop_get_slot, ":mc_level_jewel", magecraft_cat_jewel, "trp_fate_spell_array"),
		(troop_get_slot, ":mc_level_reincarnation", magecraft_cat_reincarnation, "trp_fate_spell_array"),
		(troop_get_slot, ":mc_level_perception", magecraft_cat_perception, "trp_fate_spell_array"),
		(troop_get_slot, ":mc_level_consciousness", magecraft_cat_consciousness, "trp_fate_spell_array"),
		(troop_get_slot, ":mc_level_necromancy", magecraft_cat_necromancy, "trp_fate_spell_array"),
		(troop_get_slot, ":mc_level_golemancy", magecraft_cat_golemancy, "trp_fate_spell_array"),
		(troop_get_slot, ":mc_level_numerology", magecraft_cat_numerology, "trp_fate_spell_array"),
		(troop_get_slot, ":mc_level_projection", magecraft_cat_projection, "trp_fate_spell_array"),
		(troop_get_slot, ":mc_level_reinforcement", magecraft_cat_reinforcement, "trp_fate_spell_array"),
		(troop_get_slot, ":mc_level_alteration", magecraft_cat_alteration, "trp_fate_spell_array"),
		(troop_get_slot, ":mc_level_memory", magecraft_cat_memory, "trp_fate_spell_array"),
		(troop_get_slot, ":mc_level_command", magecraft_cat_command, "trp_fate_spell_array"),
		(troop_get_slot, ":mc_level_rune", magecraft_cat_rune, "trp_fate_spell_array"),
		(troop_get_slot, ":mc_level_spatial", magecraft_cat_spatial, "trp_fate_spell_array"),
		(troop_get_slot, ":mc_level_invocation", magecraft_cat_invocation, "trp_fate_spell_array"),
		(troop_get_slot, ":mc_level_evocation", magecraft_cat_evocation, "trp_fate_spell_array"),
		(troop_get_slot, ":mc_level_time", magecraft_cat_time, "trp_fate_spell_array"),
		(troop_get_slot, ":mc_level_puppet", magecraft_cat_puppet, "trp_fate_spell_array"),
		(troop_get_slot, ":mc_level_formalcraft", magecraft_cat_formalcraft, "trp_fate_spell_array"),
		
		
	   (troop_get_slot, ":knowledge_celtic", fate_kwldge_celtic, "trp_player"),		
	   (troop_get_slot, ":knowledge_english", fate_kwldge_english, "trp_player"),		
	   (troop_get_slot, ":knowledge_greek_hist", fate_kwldge_greek_hist, "trp_player"),		 
	   (troop_get_slot, ":knowledge_greek_myth", fate_kwldge_greek_myth, "trp_player"),		 
	   (troop_get_slot, ":knowledge_roman", fate_kwldge_roman, "trp_player"),		
	   (troop_get_slot, ":knowledge_swedish", fate_kwldge_swedish, "trp_player"),
	   (troop_get_slot, ":knowledge_french", fate_kwldge_french,  "trp_player"),	   
	   (troop_get_slot, ":knowledge_bible", fate_kwldge_bible, "trp_player"),		 
	   (troop_get_slot, ":knowledge_assassin", fate_kwldge_assassin, "trp_player"),		 
	   (troop_get_slot, ":knowledge_modern", fate_kwldge_modern, "trp_player"),		 
	   (troop_get_slot, ":knowledge_ancient", fate_kwldge_ancient, "trp_player"),		 
	   (troop_get_slot, ":knowledge_philosophy", fate_kwldge_philosophy, "trp_player"),
	   
       (try_begin),
         (eq,"$character_gender",0),
         (troop_raise_attribute, "trp_player",ca_strength,1),
         (troop_raise_attribute, "trp_player",ca_charisma,1),
       (else_try),
         (troop_raise_attribute, "trp_player",ca_agility,1),
         (troop_raise_attribute, "trp_player",ca_intelligence,1),
       (try_end),
	   
		# fate_personality_type conferred bonuses
		(try_begin),
				(troop_slot_eq, "trp_player", fate_personality_type, "str_personality_selfless"),
					#str, int, leader skills
					(troop_raise_attribute, "trp_player",ca_strength,1),
					(troop_raise_attribute, "trp_player",ca_intelligence,1),
					(val_add, ":mc_level_healing", 1),
			(else_try),
				(troop_slot_eq, "trp_player", fate_personality_type, "str_personality_selfish"),
					#str, agi, personal skills
					(troop_raise_attribute, "trp_player",ca_strength,1),
					(troop_raise_attribute, "trp_player",ca_agility,1),
					(val_add, ":mc_level_command", 1),
			(else_try),
				(troop_slot_eq, "trp_player", fate_personality_type, "str_personality_honorable"),
					#cha, int, party skills
					(troop_raise_attribute, "trp_player",ca_intelligence,1),
					(troop_raise_attribute, "trp_player",ca_charisma,1),
					(val_add, ":mc_level_formalcraft", 1),
			(else_try),
				(troop_slot_eq, "trp_player", fate_personality_type, "str_personality_pragmatic"),
					#int, cha, leader skills
					(troop_raise_attribute, "trp_player",ca_intelligence,1),
					(troop_raise_attribute, "trp_player",ca_charisma,1),
					(val_add, ":mc_level_countermagic", 1),
			(else_try),
				(troop_slot_eq, "trp_player", fate_personality_type, "str_personality_realist"),
					#str, cha, leader skills
					(troop_raise_attribute, "trp_player",ca_strength,1),
					(troop_raise_attribute, "trp_player",ca_charisma,1),
					(val_add, ":mc_level_reincarnation", 1),
			(else_try),
				(troop_slot_eq, "trp_player", fate_personality_type, "str_personality_sadist"),
					#cha, str, personal skills
					(troop_raise_attribute, "trp_player",ca_strength,1),
					(troop_raise_attribute, "trp_player",ca_charisma,1),
					(val_add, ":mc_level_curse", 1),
			(else_try),
				(troop_slot_eq, "trp_player", fate_personality_type, "str_personality_zealot"),
					#cha, int, personal skills
					(troop_raise_attribute, "trp_player",ca_intelligence,1),
					(troop_raise_attribute, "trp_player",ca_charisma,1),
					(val_add, ":mc_level_sacrament", 1),
		(try_end),
		# fate_parents_bg conferred bonuses
		(try_begin),
				(troop_slot_eq, "trp_player", fate_parents_bg, "str_parent_bg_regular"),
					#basic money, basic stats
					(troop_add_gold, "trp_player", 300),
					(troop_raise_proficiency_linear, "trp_player", wpt_archery, 12),
					(troop_raise_proficiency_linear, "trp_player", wpt_crossbow, 12),
					(troop_raise_proficiency_linear, "trp_player", wpt_one_handed_weapon, 13),
					(troop_raise_proficiency_linear, "trp_player", wpt_two_handed_weapon, 13),
					(troop_raise_proficiency_linear, "trp_player", wpt_throwing, 12),
					(troop_raise_proficiency_linear, "trp_player", wpt_polearm, 13),
			(else_try),
				(troop_slot_eq, "trp_player", fate_parents_bg, "str_parent_bg_magus"),
					#int, mystic codes,
					(troop_raise_attribute, "trp_player",ca_intelligence,1),
					(troop_add_gold, "trp_player", 800),
					
					(val_add, ":mc_level_transfer", 1),
					(val_add, ":mc_level_healing", 1),
					
					(val_add, ":mana_level", 300),
			(else_try),
				(troop_slot_eq, "trp_player", fate_parents_bg, "str_parent_bg_assassin"),
					#guns, agi, 
					(troop_raise_attribute, "trp_player",ca_agility,1),
					(troop_add_item, "trp_player", "itm_webley", 8),
					(troop_add_item, "trp_player", "itm_pistol_ammo", 3),
					(troop_add_gold, "trp_player", 500),
			(else_try),
				(troop_slot_eq, "trp_player", fate_parents_bg, "str_parent_bg_church"),
					#poverty, cha
					(troop_raise_attribute, "trp_player",ca_charisma,1),
					(troop_remove_gold, "trp_player", 1000),
					
					(val_add, ":mc_level_sacrament", 1),
					
					(val_add, ":knowledge_bible", 1),
			(else_try),
				(troop_slot_eq, "trp_player", fate_parents_bg, "str_parent_bg_travelers"),
					#bonus money, add relic-hunter in contacts, cha
					(troop_raise_attribute, "trp_player",ca_intelligence,1),
					(troop_raise_attribute, "trp_player",ca_charisma,1),
					(troop_add_gold, "trp_player", 2000),
		(try_end),
		
		# fate_plyr_background conferred bonuses
		(try_begin),
				(troop_slot_eq, "trp_player", fate_plyr_background, "str_player_bg_magus"),
					#int, cha, 
					(troop_add_gold, "trp_player", 700),
					
					(troop_add_item, "trp_player", "itm_azoth_sword", 0),
					
					(troop_add_item, "trp_player", "itm_casual_suit_grey", 0),
					(troop_add_item, "trp_player", "itm_dress_cheap", 0),
					
					(val_add, ":mc_level_alchemy", 1),
					(val_add, ":mc_level_transfer", 1),
					(val_add, ":mc_level_puppet", 1),
					
					(troop_raise_skill, "trp_player", skl_engineer, 1),
					(troop_raise_skill, "trp_player", skl_persuasion, 1),
			(else_try),
				(troop_slot_eq, "trp_player", fate_plyr_background, "str_player_bg_church_combat"),
					#str, agi, wp
					(troop_raise_proficiency_linear, "trp_player", wpt_one_handed_weapon, 30),
					(troop_raise_proficiency_linear, "trp_player", wpt_throwing, 45),
					
					(troop_add_item, "trp_player", "itm_black_key", 0),
					
					(troop_add_item, "trp_player", "itm_executor_raiment_black", 0),
					(troop_add_item, "trp_player", "itm_boot_dress", 0),
					
					(val_add, ":mc_level_curse", 1),
					(val_add, ":mc_level_rune", 1),
					
					(troop_raise_skill, "trp_player", skl_power_throw, 1),
					(troop_raise_skill, "trp_player", skl_athletics, 1),
					
					(val_add, ":knowledge_bible", 2),
			(else_try),
				(troop_slot_eq, "trp_player", fate_plyr_background, "str_player_bg_church_leader"),
					#int, cha, leader skills
					
					(troop_add_item, "trp_player", "itm_black_key", 0),
					
					(troop_add_item, "trp_player", "itm_priest_raiment_black", 0),
					(troop_add_item, "trp_player", "itm_dress_cheap", 0),
					
					(val_add, ":mc_level_healing", 1),
					(val_add, ":mc_level_transfer", 1),
					
					(troop_raise_skill, "trp_player", skl_leadership, 1),
					(troop_raise_skill, "trp_player", skl_first_aid, 1),
					
					(val_add, ":knowledge_bible", 2),
			(else_try),
				(troop_slot_eq, "trp_player", fate_plyr_background, "str_player_bg_assassin"),
					#agi, str, looting, firearms
					(troop_raise_proficiency_linear, "trp_player", wpt_firearm, 45),
					(troop_raise_proficiency_linear, "trp_player", wpt_one_handed_weapon, 30),
					(troop_add_gold, "trp_player", 850),
					
					(troop_add_item, "trp_player", "itm_fn_fal", 0),
					(troop_add_item, "trp_player", "itm_rifle_ammo", 0),
					
					(troop_add_item, "trp_player", "itm_black_suit", 0),
					(troop_add_item, "trp_player", "itm_black_leather_gloves", 0),
					(troop_add_item, "trp_player", "itm_dress_monk", 0),
					
					(val_add, ":mc_level_time", 1),
					
					(troop_raise_skill, "trp_player", skl_athletics, 1),
					(troop_raise_skill, "trp_player", skl_ironflesh, 1),
					
			(else_try),
				(troop_slot_eq, "trp_player", fate_plyr_background, "str_player_bg_educator"),
					#int, book, easier servant reveals
					(troop_add_gold, "trp_player", 500),
					
					(troop_add_item, "trp_player", "itm_azoth_sword", 0),
					
					(troop_add_item, "trp_player", "itm_casual_suit_navy", 0),
					(troop_add_item, "trp_player", "itm_sneaker_hitop", 0),
					
					(troop_raise_skill, "trp_player", skl_trainer, 2),
					
					(val_add, ":knowledge_modern", 1),
					(val_add, ":knowledge_greek_myth", 1),
			(else_try),
				(troop_slot_eq, "trp_player", fate_plyr_background, "str_player_bg_detective"),
					#int, book, easier servant reveals
					(troop_add_gold, "trp_player", 500),
					
					(troop_add_item, "trp_player", "itm_beretta", 0),
					(troop_add_item, "trp_player", "itm_pistol_ammo", 0),
					
					(troop_add_item, "trp_player", "itm_casual_suit_navy", 0),
					(troop_add_item, "trp_player", "itm_dress_cheap", 0),
					
					(val_add, ":mc_level_memory", 1),
					(val_add, ":mc_level_command", 1),
					
					(troop_raise_skill, "trp_player", skl_prisoner_management, 2),
		(try_end),
		
		# fate_education conferred bonuses
		(try_begin),
				(troop_slot_eq, "trp_player", fate_education, "str_player_edu_high"),
					#int, cha, int skills up
					(troop_raise_attribute, "trp_player",ca_intelligence,1),
					(troop_raise_attribute, "trp_player",ca_charisma,1),

					(val_add, ":knowledge_greek_hist", 1),
					(val_add, ":knowledge_french", 1),
					
			(else_try),
				(troop_slot_eq, "trp_player", fate_education, "str_player_edu_magus"),
					#int, cha, magus gear, bonus money
					(troop_remove_gold, "trp_player", 500),
					(troop_raise_attribute, "trp_player",ca_intelligence,1),
					
					(val_add, ":mc_level_healing", 1),
					(val_add, ":mc_level_formalcraft", 1),
					(val_add, ":mc_level_projection", 1),
					
			(else_try),
				(troop_slot_eq, "trp_player", fate_education, "str_player_edu_church_magic"),
					#int, cha, church priest gear
					(troop_raise_attribute, "trp_player",ca_intelligence,1),
					(troop_raise_attribute, "trp_player",ca_charisma,1),
					
					(val_add, ":mc_level_sacrament", 1),
					(val_add, ":mc_level_healing", 1),
					(val_add, ":mc_level_curse", 1),
					(val_add, ":knowledge_bible", 1),
			(else_try),
				(troop_slot_eq, "trp_player", fate_education, "str_player_edu_church_combat"),
					#agi, cha, church exacutor gear
					(troop_raise_attribute, "trp_player",ca_agility,1),
					(troop_raise_attribute, "trp_player",ca_charisma,1),
					
					(troop_raise_proficiency_linear, "trp_player", wpt_one_handed_weapon, 35),
					(troop_raise_proficiency_linear, "trp_player", wpt_throwing, 40),
					
					(val_add, ":mc_level_sacrament", 1),
					(val_add, ":mc_level_countermagic", 1),
					(val_add, ":knowledge_bible", 1),
			(else_try),
				(troop_slot_eq, "trp_player", fate_education, "str_player_edu_home"),
					#int easier servant reveals
					(troop_raise_attribute, "trp_player",ca_intelligence,1),
					
					(val_add, ":mc_level_projection", 1),
					(val_add, ":mc_level_reinforcement", 1),

					(val_add, ":knowledge_roman", 1),
					(val_add, ":knowledge_greek_hist", 1),
					(val_add, ":knowledge_english", 1),
					(val_add, ":knowledge_modern", 1),
					
			(else_try),
				(troop_slot_eq, "trp_player", fate_education, "str_player_edu_soldier"),
					#str, agi, firearms, archery and one hand, bonus money
					(troop_raise_attribute, "trp_player",ca_strength,1),
					(troop_raise_attribute, "trp_player",ca_agility,1),
					
					(troop_raise_proficiency_linear, "trp_player", wpt_firearm, 25),
					(troop_raise_proficiency_linear, "trp_player", wpt_archery, 25),
					(troop_raise_proficiency_linear, "trp_player", wpt_one_handed_weapon, 25),
					
					(troop_add_item, "trp_player", "itm_ak47", 8),
		(try_end),
		
		# fate_training conferred bonuses
		(try_begin),
				(troop_slot_eq, "trp_player", fate_training, "str_player_train_archery"),
					#gets training yumi, boosted agi, boosted wpt_archery
					(troop_raise_proficiency_linear, "trp_player", wpt_archery, 75),
					(troop_raise_skill, "trp_player", skl_power_draw, 1),
			(else_try),
				(troop_slot_eq, "trp_player", fate_training, "str_player_train_sword"),
					#gets kendo sword, boosted str, boosted wp_one_handed, two_handed
					(troop_raise_proficiency_linear, "trp_player", wpt_one_handed_weapon, 35),
					(troop_raise_proficiency_linear, "trp_player", wpt_two_handed_weapon, 40),
					(troop_raise_skill, "trp_player", skl_ironflesh, 1),
			(else_try),
				(troop_slot_eq, "trp_player", fate_training, "str_player_train_magic"),
					#boosted mana, learned spells
					(val_add, ":mana_level", 300),
					(troop_raise_skill, "trp_player", skl_tactics, 1),
			(else_try),
				(troop_slot_eq, "trp_player", fate_training, "str_player_train_firearms"),
					#add a gun, boosted wp_firearms
					(troop_raise_proficiency_linear, "trp_player", wpt_firearm, 75),
					(troop_raise_skill, "trp_player", skl_inventory_management, 1),
			(else_try),
				(troop_slot_eq, "trp_player", fate_training, "str_player_train_spy"),
					#add looting, tracking
					(troop_raise_proficiency_linear, "trp_player", wpt_throwing, 75),
					(troop_raise_skill, "trp_player", skl_tracking, 1),
			(else_try),
				(troop_slot_eq, "trp_player", fate_training, "str_player_train_leader"),
					#add leadership, int
					(troop_raise_skill, "trp_player", skl_leadership, 1),
			(else_try),
				(troop_slot_eq, "trp_player", fate_training, "str_player_train_religious"),
				
				(troop_set_slot, "trp_player", fate_kwldge_bible, 1),
				(troop_raise_skill, "trp_player", skl_weapon_master, 1),
				
		(try_end),
		
		# fate_family_magus_history conferred bonuses
		(try_begin),
				(troop_slot_eq, "trp_player", fate_family_magus_history, "str_family_bloodline_ancient"),
				#Large Mana Reserves, Lossa Moneies, start with artifacts, spells
				(val_add, ":mana_level", 4000),
				(troop_add_gold, "trp_player", 8000),
				(val_add, ":mc_level_jewel", 1),
				(val_add, ":mc_level_rune", 1),
			(else_try),
				(troop_slot_eq, "trp_player", fate_family_magus_history, "str_family_bloodline_mature"),
				# Average mana reserve, good money, some spells, one artifact
				(val_add, ":mana_level", 4000),
				(troop_add_gold, "trp_player", 2000),
			(else_try),
				(troop_slot_eq, "trp_player", fate_family_magus_history, "str_family_bloodline_young"),
				# Low Mana reserve, few spells
				(val_add, ":mana_level", 1000),
			(else_try),
				(troop_slot_eq, "trp_player", fate_family_magus_history, "str_family_bloodline_none"),
				#Weak Mana Reserves, but no church or association following
				(val_add, ":mana_level", 350),
				(troop_raise_attribute, "trp_player", ca_strength, 1),
				(troop_raise_attribute, "trp_player", ca_agility, 1),
		(try_end),
		
	(troop_set_slot, "trp_player", slot_fate_max_mana, ":mana_level"),
	(troop_set_slot, "trp_player", slot_fate_cur_mana, ":mana_level"),
	   
	(troop_set_slot, "trp_player", fate_kwldge_celtic, ":knowledge_celtic"),		
	(troop_set_slot, "trp_player", fate_kwldge_english, ":knowledge_english"),
	(troop_set_slot, "trp_player", fate_kwldge_greek_hist, ":knowledge_greek_hist"),	
	(troop_set_slot, "trp_player", fate_kwldge_greek_myth, ":knowledge_greek_myth"),		
	(troop_set_slot, "trp_player", fate_kwldge_roman, ":knowledge_roman"),		 
	(troop_set_slot, "trp_player", fate_kwldge_swedish, ":knowledge_swedish"),	 
	(troop_set_slot, "trp_player", fate_kwldge_french, ":knowledge_french"),
	(troop_set_slot, "trp_player", fate_kwldge_bible, ":knowledge_bible"),	
	(troop_set_slot, "trp_player", fate_kwldge_assassin, ":knowledge_assassin"),		
	(troop_set_slot, "trp_player", fate_kwldge_modern, ":knowledge_modern"),			
	(troop_set_slot, "trp_player", fate_kwldge_ancient, ":knowledge_ancient"),	
	(troop_set_slot, "trp_player", fate_kwldge_philosophy, ":knowledge_philosophy"),
	
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_fire, ":mc_level_fire"),		
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_water, ":mc_level_water"),		
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_wind, ":mc_level_wind"),	
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_earth, ":mc_level_earth"),	
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_alchemy, ":mc_level_alchemy"),		
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_astromancy, ":mc_level_astromancy"),
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_sacrament, ":mc_level_sacrament"),	   
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_countermagic, ":mc_level_countermagic"),		 
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_curse, ":mc_level_curse"),		
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_transfer, ":mc_level_transfer"),		 
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_healing, ":mc_level_healing"),		
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_jewel, ":mc_level_jewel"),
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_reincarnation, ":mc_level_reincarnation"),
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_perception, ":mc_level_perception"),
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_consciousness, ":mc_level_consciousness"),
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_necromancy, ":mc_level_necromancy"),
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_golemancy, ":mc_level_golemancy"),
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_numerology, ":mc_level_numerology"),
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_projection, ":mc_level_projection"),
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_reinforcement, ":mc_level_reinforcement"),
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_alteration, ":mc_level_alteration"),
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_memory, ":mc_level_memory"),
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_command, ":mc_level_command"),
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_rune, ":mc_level_rune"),
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_spatial, ":mc_level_spatial"),
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_invocation, ":mc_level_invocation"),
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_evocation, ":mc_level_evocation"),
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_time, ":mc_level_time"),
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_puppet, ":mc_level_puppet"),
	(troop_set_slot, "trp_fate_spell_array", magecraft_cat_formalcraft, ":mc_level_formalcraft"),
      
      (set_show_messages, 1),
    ]),
	
	
("fate_background_determine",
    [
      (str_clear,s1),
      (assign, reg3, "$character_gender"),
	  
		# family history
		(try_begin),
			(troop_slot_eq, "trp_player", fate_family_magus_history, "str_family_bloodline_ancient"),
			(str_store_string,s2,"@is an ancient Magus family"),
			(str_store_string,s3,"@We have produced master Magus since the Age of Gods. We may have even achieved a True Magic, albeit one lost to time. Thankfully out Magic Crest has crystallized into something magnificent and our Magic Patents have paid dividends."),
		(else_try),
			(troop_slot_eq, "trp_player", fate_family_magus_history, "str_family_bloodline_mature"),
			(str_store_string,s2,"@has produced many great Magus"),
			(str_store_string,s3,"@place holder place holder place holder place holder place holder place holder place holder place holder place holder place holder place holder place holder holder place holder place holder place holder place holder place holder place holder"),
		(else_try),
			(troop_slot_eq, "trp_player", fate_family_magus_history, "str_family_bloodline_young"),
			(str_store_string,s2,"@is a young Magus family"),
			(str_store_string,s3,"@place holder place holder place holder place holder place holder place holder place holder place holder place holder place holder place holder place holder holder place holder place holder place holder place holder place holder place holder"),
		(else_try),
			(troop_slot_eq, "trp_player", fate_family_magus_history, "str_family_bloodline_none"),
			(str_store_string,s2,"@has never produced a Magus before"),
			(str_store_string,s3,"@My Magic Circuits are a huge suprise to myself and those who know my history. While few and weak, my Magic Circuits do not hold me back as a Magus. In fact my will to learn makes me more powerful than those born with the fortune to be have come from a more storied family."),
		(try_end),
		(str_store_string,s1,"@Your family {s2}. {s3}"),
		
		(try_begin),
			(troop_slot_eq, "trp_player", fate_parents_bg, "str_parent_bg_regular"),
			(str_store_string,s2,"@a traditional, loving Nuclear Family."),
			(str_store_string,s3,"@For the most part, we were a pretty normal family. Sure we had our problems, who doesn't, but nothing we couldn't talk through. This definately have given me the leg up when speaking to other Magus."),
		(else_try),
			(troop_slot_eq, "trp_player", fate_parents_bg, "str_parent_bg_magus"),
			(str_store_string,s2,"@a Magus family"),
			(str_store_string,s3,"@Like any other Magus Family we had to be intensely secretive to outside society due to the rules of the Association. This did allow me to focus on my Magecraft, allowing me to be passed on my Family's Crest and Magical Patents."),
		(else_try),
			(troop_slot_eq, "trp_player", fate_parents_bg, "str_parent_bg_assassin"),
			(str_store_string,s2,"@a mysterious, lonely person"),
			(str_store_string,s3,"@During my parent's life, they kept to themselves. They were kind of cold, but kept me safe when they were home. I didn't know until long after they had died that they were a Freelance Assassin working for both The Church and Mage's Association. They clearly cared about me by hiding this from me, but in a way I always knew. The long trips out of country, the unexplained wealth, their mere demeanor. After I discovered their true history, I found their workshop with their 'tools'. I keep them dear to me to this day."),
		(else_try),
			(troop_slot_eq, "trp_player", fate_parents_bg, "str_parent_bg_church"),
			(str_store_string,s2,"@a leader in The Church"),
			(str_store_string,s3,"@The Church, unlike the traditional Catholic Church, allows, and even encourages, their leadership to marry and have children. My father was a Priest, my mother stayed at home with the family. When we were young she died and I was stuck with just my father. Unfortunately for me this meant much more time alone with the dreadful man, accidentally revealing to me the actual purpose of The Church. I did, however, learn the scripture well, and was able to study much more than my peers."),
		(else_try),
			(troop_slot_eq, "trp_player", fate_parents_bg, "str_parent_bg_travelers"),
			(str_store_string,s2,"@enormously wealthy Travellers"),
			(str_store_string,s3,"@My parents were supposedly 'Big Shots' back in their day, but spent all my life travelling. Leaving me home alone with a nanny and money. I never learned to love them, or people in general for that matter. Wealth, however, is something I grew fond of. Besides, with my riches, I was afforded the best education availible."),
		(try_end),
		(str_store_string,s1,"@{s1}.^	You were born to {s2}. {s3}"),
	  
		# school
		(try_begin),
			(troop_slot_eq, "trp_player", fate_education, "str_player_edu_high"),
			(str_store_string,s2,"@in a normal High School"),
			(str_store_string,s3,"@Normal Classes. Bonuses to Int, Cha and knows can identify the Legends Servants come from Easier."),
		(else_try),
			(troop_slot_eq, "trp_player", fate_education, "str_player_edu_magus"),
			(str_store_string,s2,"@a Clock Tower run Magus School"),
			(str_store_string,s3,"@Mage School. Bonus to Int, Spells and start with Magic Codes or wtf they're called."),
		(else_try),
			(troop_slot_eq, "trp_player", fate_education, "str_player_edu_church_magic"),
			(str_store_string,s2,"@a Church-led Magus School"),
			(str_store_string,s3,"@Mage School, same bonuses as Clock Tower but availible only to those with Church Background. Also start with Priest Gear instead of school gear."),
		(else_try),
			(troop_slot_eq, "trp_player", fate_education, "str_player_edu_church_combat"),
			(str_store_string,s2,"@a secret Church run Executor School"),
			(str_store_string,s3,"@Combat School. Cha, Agi, start with Crimson Keys/knife, Priest Gear, throwing/one-hand bonus, basic combat spell."),
		(else_try),
			(troop_slot_eq, "trp_player", fate_education, "str_player_edu_home"),
			(str_store_string,s2,"@at home, without fellow students"),
			(str_store_string,s3,"@Home School. Int, Legends."),
		(else_try),
			(troop_slot_eq, "trp_player", fate_education, "str_player_edu_soldier"),
			(str_store_string,s2,"@only in war, as a Child Soldier"),
			(str_store_string,s3,"@Str, Agi, Firearms, One-handed, Gun/Machete."),
		(try_end),
		(str_store_string,s1,"@{s1}^	As a teen, you were educated {s2}. {s3}"),	  
	  
	  # personality
		(try_begin),
          (troop_slot_eq, "trp_player", fate_training, "str_player_train_archery"),
		  (str_store_string,s2,"@Archery Club"),
          (str_store_string,s3,"@agi, archery bonus, yumi."),
        (else_try),
          (troop_slot_eq, "trp_player", fate_training, "str_player_train_sword"),
		  (str_store_string,s2,"@the Kendo Dojo"),
          (str_store_string,s3,"@str, one-handed, two-handed, kendo sword"),
        (else_try),
          (troop_slot_eq, "trp_player", fate_training, "str_player_train_magic"),
		  (str_store_string,s2,"@a private study, learning Magecraft from a Tutor"),
          (str_store_string,s3,"@int, spells, mystic code"),
        (else_try),
          (troop_slot_eq, "trp_player", fate_training, "str_player_train_firearms"),
		  (str_store_string,s2,"@a local Shooting Club"),
          (str_store_string,s3,"@agi, firearms bonus, broken contender"),
		(else_try),
          (troop_slot_eq, "trp_player", fate_training, "str_player_train_spy"),
		  (str_store_string,s2,"@stalking the halls, Spying on other students"),
          (str_store_string,s3,"@int, looting, tracking"),
		(else_try),
          (troop_slot_eq, "trp_player", fate_training, "str_player_train_leader"),
		  (str_store_string,s2,"@running Student Government"),
          (str_store_string,s3,"@cha, leadership"),
		(else_try),
          (troop_slot_eq, "trp_player", fate_training, "str_player_train_religious"),
		  (str_store_string,s2,"@the local Temple, studying Religion"),
          (str_store_string,s3,"@int, "),
        (try_end),
		(str_store_string,s1,"@{s1}^	In your school years you often spent your freetime in {s2}. {s3}"),
		
		# fate_plyr_background conferred bonuses
		(try_begin),
			(troop_slot_eq, "trp_player", fate_plyr_background, "str_player_bg_magus"),
			(str_store_string,s2,"@an independant Magus"),
			(str_store_string,s3,"@int, spells"),
		(else_try),
			(troop_slot_eq, "trp_player", fate_plyr_background, "str_player_bg_church_combat"),
			(str_store_string,s2,"@an Executor for the Church"),
			(str_store_string,s3,"@str, keys"),
		(else_try),
			(troop_slot_eq, "trp_player", fate_plyr_background, "str_player_bg_church_leader"),
			(str_store_string,s2,"@a Priest in the Church"),
			(str_store_string,s3,"@cha, spells"),
		(else_try),
			(troop_slot_eq, "trp_player", fate_plyr_background, "str_player_bg_assassin"),
			(str_store_string,s2,"@a freelance Assassin"),
			(str_store_string,s3,"@agi, gun"),
		(else_try),
			(troop_slot_eq, "trp_player", fate_plyr_background, "str_player_bg_educator"),
			(str_store_string,s2,"@a totally normal, not weird at all, Teacher"),
			(str_store_string,s3,"@int, legends,"),
		(try_end),
		(str_store_string,s1,"@{s1}^	In your Young Adult years you became {s2}. {s3}"),
	  
		# personality
		(try_begin),
		  (troop_slot_eq, "trp_player", fate_personality_type, "str_personality_selfless"),
		  (str_store_string,s2,"@selfless {reg3?heroine:hero}"),
		  (str_store_string,s3,"@You take upon the pain of the world and without thought of your wellbeing unfortunately this isn't always a benefit to yourself and those around you. Thankfully this does make for a strength and level of leadership not found in most other people."),
		(else_try),
		  (troop_slot_eq, "trp_player", fate_personality_type, "str_personality_selfish"),
		  (str_store_string,s2,"@a selflish {reg3?woman:jerk}"),
		  (str_store_string,s3,"@You often think only of yourself, only working for your own benefit. This can actually work against you when working with a group, but thankfully you don't need any one else. Your own strength and speed in combat will be all you ever need."),
		(else_try),
		  (troop_slot_eq, "trp_player", fate_personality_type, "str_personality_honorable"),
		  (str_store_string,s2,"@honorable {reg3?lady:gentleman}"),
		  (str_store_string,s3,"@place holder place holder place holder place holder place holder place holder place holder place holder place holder place holder place holder place holder holder place holder place holder place holder place holder place holder place holder"),
		(else_try),
		  (troop_slot_eq, "trp_player", fate_personality_type, "str_personality_pragmatic"),
		  (str_store_string,s2,"@the pragmatist"),
		  (str_store_string,s3,"@place holder place holder place holder place holder place holder place holder place holder place holder place holder place holder place holder place holder holder place holder place holder place holder place holder place holder place holder"),
		(else_try),
		  (troop_slot_eq, "trp_player", fate_personality_type, "str_personality_realist"),
		  (str_store_string,s2,"@the realist"),
		  (str_store_string,s3,"@place holder place holder place holder place holder place holder place holder place holder place holder place holder place holder place holder place holder holder place holder place holder place holder place holder place holder place holder"),
		(else_try),
		  (troop_slot_eq, "trp_player", fate_personality_type, "str_personality_sadist"),
		  (str_store_string,s2,"@a disgusting sadist"),
		  (str_store_string,s3,"@str, "),
		(else_try),
		  (troop_slot_eq, "trp_player", fate_personality_type, "str_personality_zealot"),
		  (str_store_string,s2,"@the zealot"),
		  (str_store_string,s3,"@cha, legends"),
		(try_end),
		(str_store_string,s1,"@{s1}^	Your life experience has molded you into a unique person. You're known to be quite {s2}. {s3}"),
	  
    ]),
	
("fate_projection_casts",
    [
	(store_script_param_1, ":agent"),
	(store_script_param_2, ":optional_weapon"),
	# (get_player_agent_no, ":player_agent"), # Who are you?
	(agent_get_troop_id, ":troop", ":agent"),
	(agent_is_alive, ":agent"), # Are you alive? If so. . .
	(agent_get_wielded_item, ":equipped_item", ":agent", 0),
	
	# (player_get_troop_id, ":player_troop", ":player_agent"), #future proofing for multiplayer
	
	# Check player's inventory slots, if completely full, drop current item
	# for bow, drop two items
	# if not, check what is in the right and left hands
	# if occupied unequip the currently equipped item for the respective spell
	# right for sword, left for shield, both for bow and spear
	
	
    (try_begin),
		(gt, ":optional_weapon", 0),
		(assign, ":projection_item", ":optional_weapon"),
	(else_try),
		(troop_get_slot, ":projection_item", ":troop", fate_magic_projection_item), # What weapon is going to be projected?
	(try_end),
	
	(agent_get_item_slot, ":slot_3", ":agent", 2), # Store Slot 3 for later checks
	(agent_get_item_slot, ":slot_4", ":agent", 3), # Store Slot 4 for later checks
	
	(try_begin),
		(neq, ":slot_4", -1),
		(agent_unequip_item, ":agent", ":slot_4"),
	(try_end),

	# This is the new way
	
	(try_begin),
		(item_has_property, ":projection_item", itp_type_bow),
		(try_begin),
			(neq, ":slot_3", -1),
			(agent_unequip_item, ":agent", ":slot_3"),
		(try_end),
		#(agent_set_wielded_item, ":agent", -1),
		#(agent_unequip_item, ":agent", ":equipped_item"),
		(agent_equip_item, ":agent", ":projection_item", 2),
		#(agent_equip_item, ":agent", "itm_created_arrows", 3),
		(agent_set_wielded_item, ":agent", ":projection_item"),
	(else_try),
		(item_has_property, ":projection_item", itp_type_arrows),
		(try_begin),
			(neq, ":slot_4", -1),
			(agent_unequip_item, ":agent", ":slot_4"),
		(try_end),
		(agent_equip_item, ":agent", ":projection_item", 3),
	(else_try),
		(eq, ":projection_item", "itm_caliburn"),
		# (agent_unequip_item, ":agent", "itm_bakuya"),
		# (agent_unequip_item, ":agent", "itm_kanshou_melee"),
		# (agent_unequip_item, ":agent", "itm_kanshou"),
		#(agent_unequip_item, ":agent", ":equipped_item"),
		(agent_equip_item, ":agent", ":projection_item", 3),
		(agent_set_wielded_item, ":agent", ":projection_item"),
	(else_try),
		(neq, ":projection_item", "itm_caliburn"),
		#(agent_set_wielded_item, ":agent", -1),
		(agent_unequip_item, ":agent", ":equipped_item"),
		(agent_equip_item, ":agent", ":projection_item", 3),
		(agent_set_wielded_item, ":agent", ":projection_item"),
	(try_end),
	
    ]),
# ## This is the script for AOE type NP such as#####
# # Artoria's Excalibur and Fergus' Thing###########
# ## Parameter 1: User Agent #######################
# ## Parameter 2: Attack Range for Blast ###########
# ## Parameter 3: Trigger Attack Dir ###############
# ############### 0 = down, 1 = right, 2 = left#####
# ############### 3 = up, -1 is invalid and how? ###
# ## Parameter 4: AOE Style ########################
# ################ 1 = Straight Ahead, 2 = Circle ##
# ##################################################
# ## Parameter 5: Damage ###########################
# ################ Any INT to determine damage #####
# ## Parameter 6: Elemental Type ###################
# ###############  < 0 = Fire, 1 = Air, 2 = Earth ##
# ###############  3 = Holy/Divine, 4 = Demonic ####
# ###############  These just determine particles ##
# ##################################################
("fate_aoe_attack",
    [
	(store_script_param, ":agent_id", 1),
	(store_script_param, ":attack_range", 2),
	(store_script_param, ":trigger_attack_dir", 3),
	(store_script_param, ":attack_type", 4),
	(store_script_param, ":damage", 5),
	
	(set_fixed_point_multiplier, 100),
	
	# (agent_get_action_dir, ":attack_dir", ":agent_id"),
	(try_begin),
		(agent_is_non_player, ":agent_id"),
		(agent_set_attack_action, ":agent_id", ":trigger_attack_dir", 0),
	(else_try),
		(agent_set_animation, ":agent_id", "anim_release_overswing_twohanded", 1),
	(try_end),
	# (eq, ":attack_dir", ":trigger_attack_dir"),
	
	(agent_get_look_position, pos2, ":agent_id"),
	(copy_position, pos7, pos2),
	
	(store_div, ":particle_range", ":attack_range", 1000),
	(val_add, ":particle_range", 100),
	
	(try_begin),
		(eq, ":attack_type", 1),
		
		# (try_for_range, reg5, 100, ":particle_range"),
			# (position_move_y, pos7, reg5),
			# (position_set_z_to_ground_level, pos7),
			# (particle_system_burst, "psys_larger_explosion", pos7, 75),
			# (val_add, reg5, 100),
		# (end_try),
		
		
		# do a single burst only, adjust the particle system accordingly, 100 is max burst btw
		#(assign, ":attack_range", 5000),#500 meters, adjust the particle life and emit speed accordingly
		#(position_move_z, pos2, -90, 1),#pre applied center of body adjustment
		(position_set_z_to_ground_level, pos2),
		(try_for_agents, ":agent", pos2, ":attack_range"),
		  (agent_is_alive, ":agent"),
		  #
		  (agent_get_position, pos3, ":agent"),
		  (position_transform_position_to_local, pos4, pos2, pos3),
		  (position_get_x, ":val", pos4),
		  (is_between, ":val", -50, 50), #horizontal body size, 1m
		  (position_get_y, ":val", pos4),
		  (is_between, ":val", 15, ":attack_range"),#check forward only. from 15cm in front of agent
		 # (position_get_z, ":val", pos4),
		  #(is_between, ":val", 0, 250),#vertical body size
		  
		  (agent_deliver_damage_to_agent, ":agent_id", ":agent", ":damage"),
		  
		  (agent_is_human, ":agent"),
		  (agent_set_animation, ":agent", "anim_strike_fall_back_rise"),
		(try_end),
	(else_try),
	
		(eq, ":attack_type", 2),
		#(try_for_range, reg10, 0, 36),
			(try_for_range, reg5, 100, ":particle_range"),
				(position_move_y, pos7, reg5),
				(position_rotate_z, pos7, 10, 1),
				(position_set_z_to_ground_level, pos7),
				(particle_system_burst, "psys_larger_explosion", pos7, 75),
				(val_add, reg5, 100),
				
			(end_try),
		# (val_add, reg10, 10),
		# (position_copy_origin, pos7, pos2),
		# (end_try),
		
		
		# do a single burst only, adjust the particle system accordingly, 100 is max burst btw
		#(assign, ":attack_range", 5000),#500 meters, adjust the particle life and emit speed accordingly
		#(position_move_z, pos2, -90, 1),#pre applied center of body adjustment
		(position_set_z_to_ground_level, pos2),
		(try_for_agents, ":agent", pos2, ":attack_range"),
		  (agent_is_alive, ":agent"),
		  
		  (agent_get_position, pos3, ":agent"),
		  (position_transform_position_to_local, pos4, pos2, pos3),
		  # (position_get_x, ":val", pos4),
		  # (is_between, ":val", -50, 50), #horizontal body size, 1m
		  (position_get_y, ":val", pos4),
		  (is_between, ":val", 15, ":attack_range"),#check forward only. from 15cm in front of agent
		  
		  (agent_deliver_damage_to_agent, ":agent_id", ":agent", ":damage"),
		  
		  (agent_is_human, ":agent"),
		  (agent_set_animation, ":agent", "anim_strike_fall_back_rise"),
		(try_end),
	(try_end),
	
    ]),
	
("fate_beam_attack",
    [
	(store_script_param, ":agent_id", 1),
	(store_script_param, ":target_pos", 2),
	(store_script_param, ":damage", 3),
	(store_script_param, ":item_to_scale_from", 4),
	(store_script_param, ":element", 5),
	
	(set_fixed_point_multiplier, 100),
	
	(agent_get_position, pos2, ":agent_id"),
	(position_move_z, pos2, 100),
	(position_move_y, pos2, 15),
	(call_script, "script_lookat", pos2, ":target_pos"),
	
	
	(get_distance_between_positions, ":attack_range", pos2, ":target_pos"),
	
	#(assign, reg1, ":attack_range"),
	#(display_message, "@{reg1}cm Laser Blast!"),
	#(store_div, ":burst_range", ":attack_range", 100),
	
	# (try_for_range, ":burst", 0, ":burst_range"),
		# (copy_position, pos7, pos2),
		# (val_mul, ":burst", 100),
		# (position_move_y, pos7, ":burst"),
		# (particle_system_burst, "psys_beam", pos7, 75),
	# (try_end),
	
	(set_spawn_position, pos2),
	
	(try_begin),
		(eq, ":element", 5),
		(store_random_in_range, ":rot", -12, 13),
		(val_mul, ":rot", 15),
		(position_rotate_y, pos2, ":rot"),
		(set_spawn_position, pos2),
		(spawn_scene_prop, "spr_lightning_beam"),
	(else_try),
		(spawn_scene_prop, "spr_alignment_test"),
	(try_end),
	
	(prop_instance_set_scale, reg0, 100, ":attack_range", 100),
	(scene_prop_fade_out, reg0, 50),

	(try_for_agents, ":agent", pos2, ":attack_range"),
	  (agent_is_alive, ":agent"),
	  (neq, ":agent", ":agent_id"),
	  
	  (val_add, ":attack_range", 200),
	  
	  (agent_get_position, pos3, ":agent"),
	  (position_transform_position_to_local, pos4, pos2, pos3),
	  
	  (position_get_x, ":val", pos4),
	  (is_between, ":val", -100, 100), #horizontal body size, 1m
	  (position_get_y, ":val", pos4),
	  (is_between, ":val", 0, ":attack_range"),#check forward only. from 15cm in front of agent
	  (position_get_z, ":val", pos4),
	  (is_between, ":val", -150, 150),#vertical body size
	  
	  (try_begin),
		(eq, ":item_to_scale_from", 0),
		(agent_deliver_damage_to_agent, ":agent_id", ":agent", ":damage"),
	  (else_try),
		(agent_deliver_damage_to_agent, ":agent_id", ":agent", ":damage", ":item_to_scale_from"),
	  (try_end),
	  
	  #(agent_is_human, ":agent"),
	  #(agent_set_animation, ":agent", "anim_strike_fall_back_rise"),
	(try_end),
	
    ]),
	
("fate_angular_attack",
    [
	(store_script_param, ":agent_id", 1),
	(store_script_param, ":target_pos", 2),
	(store_script_param, ":effective_angle", 3),
	(store_script_param, ":damage", 4),
	(store_script_param, ":item_to_scale_from", 5),
	
	(set_fixed_point_multiplier, 100),
	
	(agent_get_position, pos2, ":agent_id"),
	(position_move_z, pos2, 100),
	(position_move_y, pos2, 15),
	(call_script, "script_lookat", pos2, ":target_pos"),
	
	
	(get_distance_between_positions, ":attack_range", pos2, ":target_pos"),
	(store_div, ":burst_range", ":attack_range", 100),
	(try_for_range, ":burst", 0, ":burst_range"),
		(copy_position, pos7, pos2),
		(val_mul, ":burst", 100),
		(position_move_y, pos7, ":burst"),
		(particle_system_burst, "psys_laser", pos7, 75),
	(try_end),

	(try_for_agents, ":agent", pos2, ":attack_range"),
	  (agent_is_alive, ":agent"),
	  (neq, ":agent", ":agent_id"),
	  
	  (val_add, ":attack_range", 200),
	  
	  (agent_get_position, pos3, ":agent"),
	  (position_transform_position_to_local, pos4, pos2, pos3),
	  
	  (call_script, "script_get_angle_between_points_180", pos2, pos3),
	
	  # (display_message, "@Angle from attacker to victim: h{reg26}, v{reg27}"),
	  
	  (is_between, reg26, -1, ":effective_angle"),
	  (is_between, reg27, -1, ":effective_angle"),
	  (position_get_y, ":val", pos4),
	  (is_between, ":val", 0, ":attack_range"),#check forward only. from 15cm in front of agent
	  
	  (try_begin),
		(eq, ":item_to_scale_from", 0),
		(agent_deliver_damage_to_agent, ":agent_id", ":agent", ":damage"),
	  (else_try),
		(agent_deliver_damage_to_agent, ":agent_id", ":agent", ":damage", ":item_to_scale_from"),
	  (try_end),
	  
	  (agent_is_human, ":agent"),
	  (agent_set_animation, ":agent", "anim_strike_fall_back_rise"),
	(try_end),
	
    ]),
	
("fate_mash_shieldbash",
    [
	(store_script_param, ":agent_id", 1),
	(agent_get_team, ":user_team", ":agent_id"),
	(set_fixed_point_multiplier, 100),
	
	# (agent_get_action_dir, ":attack_dir", ":agent_id"),
	
	(agent_get_look_position, pos2, ":agent_id"),
	(copy_position, pos7, pos2),
	
		(position_set_z_to_ground_level, pos2),
		(try_for_agents, ":agent", pos2, 250),
		  (agent_is_alive, ":agent"),
		  (agent_get_team, ":victim_team", ":agent"),
		  (neq, ":user_team", ":victim_team"),
		  (agent_get_position, pos3, ":agent"),
		  (position_transform_position_to_local, pos4, pos2, pos3),
		  (position_get_x, ":val", pos4),
		  (is_between, ":val", -100, 100), #horizontal body size, 2m
		  (position_get_y, ":val", pos4),
		  (is_between, ":val", 15, 250), #check forward only. from 15cm in front of agent
		 # (position_get_z, ":val", pos4),
		  #(is_between, ":val", 0, 250),#vertical body size
		  
		  (agent_deliver_damage_to_agent, ":agent_id", ":agent", 100),
		  
		  #(agent_is_human, ":agent"),
		  #(agent_set_animation, ":agent", "anim_strike_fall_back_rise"),
		(try_end),
	
    ]),

("fate_magic_decision", [
	# Look at the troops spell load out
	# Determine the situation they are in
	
	# Are they at a disadvantage

	# How are thier mana reserves
	# Are they equipped and enhanced enough
	# Are they near enough for a combat spell
	
]),

# ######################################
# # This is Servant Skill Scripts ######
# ## We'll see how many stay this way ##
# # I believe very few will be script- #
# # -ed like this, but they're here ####
# ######### just in case, y'know? ######

# ####### Class Skills #################

  # Archers, some Assassin
  # ("c_skill_independent_action", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # # Casters
  # ("c_skill_item_construction", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Item Contruction"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # # Casters
  # ("c_skill_territory_creation", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Territory Creation"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # # Berserker
  # ("c_skill_mad_enhancement", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Mad Enhancement"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # # Assassin
  # ("c_skill_presence_concealment", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Presence Concelement"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # # Rider, Saber
  # ("c_skill_riding", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]),			
  # # The Knight Classes Saber, Lancer, Archer
  # ("c_skill_magic_resistance", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]),

# ####### Personal Skills ############## 
  #Atalanta
  ("p_skill_aesthetics_of_the_last_spurt", 
  [
	(store_script_param, ":agent", 1),
	
	(agent_set_speed_limit, ":agent", 45),
	(agent_set_speed_modifier, ":agent", 280),
	(display_debug_message, "@Aesthetics of the Last Spurt, called"),
	(display_debug_message, "@When in a losing situation, Atalanta"),
	(display_debug_message, "@becomes basically the fastest Servant"),
  ]), 
  #Achilles
  # ("p_skill_affections_of_the_goddess", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]), 	
  # #Asterios
  # ("p_skill_avyssos_of_labrys", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]), 		
  # # Has basic implementation, works okay. Part of Death Handling Script.
  # #Cu Chulainn, King Hassan, Heracles, Achilles, Saint George
  # ("p_skill_battle_continuation", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]), 				
  # #Arash
  # ("p_skill_bow_and_arrow_creation", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]), 			
  # #Fergus, Achilles, Heracles
  # ("p_skill_bravery", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]), 						
  # #Artoria, Richard, Gilgamesh, Iskandar
  # ("p_skill_charisma", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]), 					
  # #Fionn, EMIYA, Arash
  # ("p_skill_clairvoyance", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]), 			
  #Atalanta
  ("p_skill_crossing_arcadia", 
  [
	(store_script_param, ":agent", 1),
  
	(agent_set_speed_limit, ":agent", 42), # Crossing Arcadia
	(agent_set_speed_modifier, ":agent", 250), # Crossing Arcadia
	
	(display_debug_message, "@ Crossing Arcadia, called"),
	(display_debug_message, "@ Basically, She's Faster Now."),	
  ]), 		
  # #Cu Chulainn, Hector, Siegfried
  # ("p_skill_disengage", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]), 				
  # #Cu Chulainn, Gilgamesh, Heracles, Medusa, Iskandar, Achilles, Saint George
  # ("p_skill_divinity", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]), 				
  # #William Shakespeare
  # ("p_skill_enchant", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]), 				
  # #Lancelot
  # ("p_skill_eternal_arms_mastery", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]), 
  # #Astolfo
  # ("p_skill_evaporation_of_sanity", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # #King Hassan
  # ("p_skill_evening_bell", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # #Hundred Faces
  # ("p_skill_expert_of_many_specializations", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # #Gilles de Rais
  # ("p_skill_eye_for_art", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # #Heracles
  # ("p_skill_eye_of_the_mind_false", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # #Fergus, Diarmuid, EMIYA
  # ("p_skill_eye_of_the_mind_true", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # #Frankenstein
  # ("p_skill_galvanism", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # #Medea
  # ("p_skill_golden_fleece", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # #Siegfried, Gilgamesh, Robin Hood
  # ("p_skill_golden_rule", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]), 													
  # #Saint George
  # ("p_skill_guardian_knight", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # #Brynhildr
  # ("p_skill_heros_bridesmaid", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # #Medea
  # ("p_skill_highspeed_divine_words", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # #Hohenheim
  # ("p_skill_highspeed_incantation", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # #Frankenstein
  # ("p_skill_hollow_lament_of_the_falsely_living", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # #Spartacus
  # ("p_skill_honor_of_the_battered", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # #Nero
  # ("p_skill_imperial_privilege", [
	# (store_script_param, ":agent", 1),
  
	# (str_store_string, s1, "@Independent Action"),
	# (display_debug_message, "@{s1}, called"),
  # ]),
  # #Artoria, Saint George
  # ("p_skill_instinct", []),
  # #Hundred Faces
  # ("p_skill_librarian_of_stored_knowledge", []),
  # #Diarmuid
  # ("p_skill_love_spot", []),
  # #Fionn, EMIYA
  # ("p_skill_magecraft", []),
  # #Artoria
  # ("p_skill_mana_burst", []),
  # #Brynhildr
  # ("p_skill_mana_burst_flames", []),
  # #Gilles de Rais
  # ("p_skill_mental_pollution", []),
  # #Nero, Debuff
  # ("p_skill_migraine", []),
  # #Hector, Iskandar
  # ("p_skill_military_tactics", []),
  # #Asterios, Medusa, Astolfo
  # ("p_skill_monstrous_strength", []),
  # #Medusa
  # ("p_skill_mystic_eyes", []),
  # #King Hassan
  # ("p_skill_natural_body", []),
  # #Asterios
  # ("p_skill_natural_monster", []),
  # #Fergus
  # ("p_skill_nature_of_a_rebellious_spirit", []),
  # #Avicebron
  # ("p_skill_numerology", []),
  # #Serenity
  # ("p_skill_poison_res", []),
  # #Brynhildr
  # ("p_skill_primordial_rune", []),
  # #Cursed Arm, Serenity, Shadow Peeling
  # ("p_skill_projectile_dagger", []),
  # #Hector
  # ("p_skill_proof_of_friendship", []),
  # #Cu Chulainn
  # ("p_skill_protection_from_arrows", []),
  # #Cursed Arm
  # ("p_skill_protection_from_wind", []),
  # #Lancelot
  # ("p_skill_protection_of_the_fairies", []),
  # #King Hassan
  # ("p_skill_protection_of_the_faith", []),
  # #Arash
  # ("p_skill_robust_health", []),
  # #Cu Chulainn
  # ("p_skill_rune_magic", []),
  # #Cursed Arm
  # ("p_skill_selfmodification", []),
  # #William Shakespeare
  # ("p_skill_selfpreservation", []),
  # #Shadow Peeling
  # ("p_skill_shadow_lantern", []),
  # #Serenity
  # ("p_skill_shapeshift", []),
  # #Saint George
  # ("p_skill_soul_of_a_martyr", []),
  # #Robin Hood
  # ("p_skill_subversive_activities", []),
  # #King Hassan
  # ("p_skill_uncrowned_martial_arts", []),
  
  # script_update_fate_hud
  # Input: none
  # Output: none
  ("update_fate_hud",
   [
    # (set_fixed_point_multiplier, 1000),
  
		# (try_begin),
			# (is_presentation_active, "prsnt_fate_hud"),
			# (presentation_set_duration, 0),
		# (try_end),
  
		# (get_player_agent_no, ":player_agent"),
		# (gt, ":player_agent", 0),
		# (agent_get_troop_id, ":player_troop", ":player_agent"),
		
		# (try_begin),
			# (troop_is_hero, ":player_troop"), # Is this agent a mob or someone important
			# (troop_get_slot, ":current_mana", ":player_troop", slot_fate_cur_mana),
		# (else_try),
			# (agent_get_slot, ":current_mana", ":player_agent", fate_agnt_cur_mana),
		# (try_end),
		
		
		# (assign, reg20, ":current_mana"),
		
		 # #(create_text_overlay, reg1, "@Mana: {reg20}          ", tf_double_space|tf_center_justify),
		 # (overlay_set_text, "$g_ui_player_cur_mana", "@Mana: {reg20}          "),
		 (val_add, "$g_hud_update", 1),

  ]),
  
  
  ("fate_battle_variables", [
  
	(eq, "$g_fate_battle_loaded", 0),
	
	(assign, "$g_fate_battle_master_1", "trp_master_staynight_01"),
	(assign, "$g_fate_battle_master_2", "trp_master_staynight_07"),
	
	(assign, "$g_fate_battle_servant_1", "trp_saber_01"),
	(assign, "$g_fate_battle_servant_2", "trp_lancer_01"),

	(str_store_troop_name, "$g_fate_battle_master_1_name", "trp_master_staynight_01"),
	(str_store_troop_name, "$g_fate_battle_master_2_name", "trp_master_staynight_07"),
	(str_store_troop_name, "$g_fate_battle_servant_1_name", "trp_saber_01"),
	(str_store_troop_name, "$g_fate_battle_servant_2_name", "trp_lancer_01"),

	(assign, "$g_fate_battle_master_offset", 0),
	(assign, "$g_fate_battle_masters_on", 1),
	(assign, "$g_fate_battle_servants_on", 1),
	(assign, "$g_fate_battle_player_is_master", 1),
	 
	(assign, "$g_fate_battle_team_selected", 0),
	(assign, "$g_fate_battle_infinite_mana", 0),
	(assign, "$g_fate_battle_show_healthbars", 0),
	(assign, "$g_fate_battle_god_mode", 0),

	(assign, "$g_fate_exit", 0),
	
	(assign, "$g_grail_war", 1),

	(val_add, "$g_fate_battle_loaded", 1),
  ]),
  
  #script_add_troop_to_cur_tableau
  # INPUT: troop_no
  # OUTPUT: none
  ("add_fate_troop_to_tableau",
    [
       (store_script_param, ":troop_no",1),

       (set_fixed_point_multiplier, 100),
       (assign, ":banner_mesh", -1),
       # (troop_get_slot, ":banner_spr", ":troop_no", slot_troop_banner_scene_prop),
       # (store_add, ":banner_scene_props_end", banner_scene_props_end_minus_one, 1),
       # (try_begin),
         # (is_between, ":banner_spr", banner_scene_props_begin, ":banner_scene_props_end"),
         # (val_sub, ":banner_spr", banner_scene_props_begin),
         # (store_add, ":banner_mesh", ":banner_spr", banner_meshes_begin),
       # (try_end),

       (cur_tableau_clear_override_items),
       
	   (try_begin),
			(this_or_next|is_between, ":troop_no", assassin_begin, assassin_end),
			(this_or_next|eq, ":troop_no", "trp_berserker_05"),
			(this_or_next|eq, ":troop_no", "trp_rider_01"),
			(this_or_next|eq, ":troop_no", "trp_berserker_02"),
			(this_or_next|eq, ":troop_no", "trp_caster_01"),
			(eq, ":troop_no", "trp_caster_04"),
		(else_try),
			(cur_tableau_set_override_flags, af_override_head),
	   (try_end),
       
       (init_position, pos2),
       (cur_tableau_set_camera_parameters, 1, 6, 6, 10, 10000),

       (init_position, pos5),
       (assign, ":eye_height", 162),
	   
	   (assign, ":camera_distance", 75),
       (assign, ":camera_yaw", -15),
       (assign, ":camera_pitch", -18),
       (assign, ":animation", "anim_stand_man"),
       

       (position_set_z, pos5, ":eye_height"),

       # camera looks towards -z axis
       (position_rotate_x, pos5, -90),
       (position_rotate_z, pos5, 180),

       # now apply yaw and pitch
       (position_rotate_y, pos5, ":camera_yaw"),
       (position_rotate_x, pos5, ":camera_pitch"),
       (position_move_z, pos5, ":camera_distance", 0),
       (position_move_x, pos5, 5, 0),

       (try_begin),
         (ge, ":banner_mesh", 0),

         (init_position, pos1),
         (position_set_z, pos1, -1500),
         (position_set_x, pos1, 265),
         (position_set_y, pos1, 400),
         (position_transform_position_to_parent, pos3, pos5, pos1),
         (cur_tableau_add_mesh, ":banner_mesh", pos3, 400, 0),
       (try_end),
	   
       (cur_tableau_add_troop, ":troop_no", pos2, ":animation" , 0),

       (cur_tableau_set_camera_position, pos5),

       (copy_position, pos8, pos5),
       (position_rotate_x, pos8, -90), #y axis aligned with camera now. z is up
       (position_rotate_z, pos8, 30), 
       (position_rotate_x, pos8, -30), 
       (cur_tableau_add_sun_light, pos8, 30, 28, 25),
     ]),
  # The game looks for these are well
	# game_check_party_sees_party 
	# game_get_party_speed_multiplier 
	# game_missile_dives_into_water 
	# game_troop_upgrades_button_clicked 
	# game_character_screen_requested 
	
  
  ("rapid_fire", [
  (store_script_param, ":agent", 1),  

	(agent_get_look_position, pos9, ":agent"), 					# Get Agent's look for math
	(agent_get_bone_position, pos10, ":agent", 13, 1),
	(position_copy_rotation, pos10, pos9),

	(store_random_in_range, ":rand_x", -10, 10),
	(store_random_in_range, ":rand_y", -10, 10), 
	(store_random_in_range, ":rand_z", -10, 10),

	(position_rotate_x, pos10, ":rand_x"),
	(position_rotate_y, pos10, ":rand_y"),
	(position_rotate_z, pos10, ":rand_z"),
	
	(agent_set_animation, ":agent", "anim_release_bow", 1),
	(add_missile, ":agent", pos10, 7600, "itm_arash_bow", 0, "itm_created_arrows", 0),

  ]),
  
  ("gate_of_babylon", [
	(set_fixed_point_multiplier, 100),
	(store_script_param, ":agent", 1),  
  
	(agent_get_position, pos30, ":agent"),
	
	(try_begin),
		(agent_is_non_player, ":agent"), # Is this an AI?
		(agent_ai_get_look_target, ":enemy_agent", ":agent"),
		(gt, ":enemy_agent", 0),
		# (agent_get_position, pos11, ":enemy_agent"),
		(store_random_in_range, ":bone", 7, 10),
		(agent_get_bone_position, pos31, ":enemy_agent", ":bone", 1),
	(else_try),
		(agent_get_look_position, pos10, ":agent"), 	# Get Agent's look for math
		(position_move_z, pos10, 180, 1),
		(cast_ray, reg35, pos31, pos10),
		
		(assign, ":enemy_agent", 1),

		(try_for_agents, ":enemy_agent", pos30, 10000),
			(neq, ":agent", ":enemy_agent"),
			(agent_is_alive, ":enemy_agent"),
			(agent_is_human, ":enemy_agent"),
			# (agent_is_non_player, ":enemy_agent"),
			
			(agent_get_position, pos3, ":enemy_agent"),
			(position_transform_position_to_local, pos4, pos30, pos3),
			(position_get_x, ":val", pos4),
			(is_between, ":val", -250, 250), #horizontal body size, 1m
			(position_get_y, ":val", pos4),
			(is_between, ":val", 15, 10000),
			
			(store_random_in_range, ":bone", 7, 10),
			(agent_get_bone_position, pos31, ":enemy_agent", ":bone", 1),
		(try_end),
	(try_end),
	
	(gt, ":enemy_agent", 0),
	#(particle_system_burst, "psys_laser", pos31, 10),
	
	(position_copy_rotation, pos30, pos10),
	
	# Randomize the height. Somewhere between 1m (shoulder height) and 15m
	(store_random_in_range, ":height_var", 150, 1650),
	# Randomize the horizontal somewhere around 5m either side of agent
	(store_random_in_range, ":horizontal_var", -500, 500),
	(store_random_in_range, ":depth_var", -150, -250),

	(position_move_z, pos30, ":height_var", 1),
	(position_move_y, pos30, ":depth_var"),
	(position_move_x, pos30, ":horizontal_var"),
	
	
	
	(call_script, "script_lookat", pos30, pos31),
	
	(agent_get_troop_id, ":troop", ":agent"),
	
	(store_random_in_range, ":weapon", "itm_gob_sword", "itm_shirou_shirt"),
	
	(try_begin),
		(eq, ":troop", "trp_archer_02"),
		(agent_set_animation, ":agent", "anim_gilgamesh_stand", 1),
		(particle_system_burst, "psys_pistol_smoke", pos30, 1),
	(try_end),

	(add_missile, ":agent", pos30, 7200, ":weapon", 0, ":weapon", 0),

  ]),
	("define_personality", 
	[
		(store_script_param, ":troop", 1),
		
		(store_script_param, ":aggression", 2),
		(store_script_param, ":honor", 3),
		(store_script_param, ":logic", 4),
		(store_script_param, ":drive", 5),
		(store_script_param, ":empathy", 6),
		(store_script_param, ":pragmatism", 7),
		(store_script_param, ":selfless", 8),
		
		
		(troop_set_slot, ":troop", slot_personality_aggression, ":aggression"),
		(troop_set_slot, ":troop", slot_personality_honor, 		":honor"),
		(troop_set_slot, ":troop", slot_personality_logic, 		":logic"),
		(troop_set_slot, ":troop", slot_personality_drive, 		":drive"),
		(troop_set_slot, ":troop", slot_personality_empathy, 	":empathy"),
		(troop_set_slot, ":troop", slot_personality_pragmatism, ":pragmatism"),
		(troop_set_slot, ":troop", slot_personality_selfless, 	":selfless"),
	]),	
	
	
	
	("noble_phantasm_activation", 
	[
		(set_fixed_point_multiplier, 100),
	
		(store_script_param, ":servant_agent", 1),
		(store_script_param, ":noble_phantasm", 2),
		(agent_get_wielded_item, ":equipment", ":servant_agent", 0),
		
		(agent_get_wielded_item, ":lequipment", ":servant_agent", 1),
		(agent_get_position, pos10, ":servant_agent"),
		(store_mission_timer_a, ":time"), # Time in seconds
		(agent_get_horse, ":horse", ":servant_agent"),
		(agent_get_troop_id, ":servant_troop", ":servant_agent"),
		
		(try_begin),
			(neq, ":horse", -1),
			(agent_get_item_id, ":horse_type", ":horse"),
		(try_end),
		
		(try_begin),
			(agent_slot_ge, ":servant_agent", slot_agent_active_np_timer, 1),
			(display_message, "@This Servant already has an active NP"),
		(try_end),
		
		(neg|agent_slot_ge, ":servant_agent", slot_agent_active_np_timer, 1),
		
		(set_spawn_position, pos10),

		# (agent_get_troop_id, ":servant", ":servant_agent"),
		
		(str_store_agent_name, s10, ":servant_agent"),
		(str_store_string, s11, ":noble_phantasm"),

		(display_message, "@{s10} is being told to use {s11}"),
		
		(try_begin),
		#Saber Noble Phantasms
			(eq, ":noble_phantasm", "str_noble_phantasm_saber_01_01"), # "Excalibur", Artoria Pendragon
			# Excalibur!
			
			(try_begin),
				(eq, ":equipment", "itm_excalibur_gold"),
				#(call_script, "script_fate_aoe_attack", ":servant_agent", 50000, 3, 1, 500),
				(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_saber_01_01"),
				(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 99999999),
				(start_presentation, "prsnt_fate_rts_camera"),
			(else_try),
				(eq, ":equipment", "itm_excalibur_invis"),
				(agent_unequip_item, ":servant_agent", "itm_excalibur_invis"),
				(agent_equip_item, ":servant_agent", "itm_excalibur_gold"),
				(agent_set_wielded_item, ":servant_agent", "itm_excalibur_gold"),
				(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_saber_01_01"),
				(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 99999999),
				(start_presentation, "prsnt_fate_rts_camera"),
				# (call_script, "script_fate_aoe_attack", ":servant_agent", 50000, 3, 1, 500),
			(else_try),
				(display_message, "@Saber must be using Excalibur!"),
			(try_end),
			
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_saber_01_02"), # "Invisible Air" #Artoria Pendragon
			# Reactivate invisible air, and if active, do a aoe burst
			(try_begin),
				(eq, ":equipment", "itm_excalibur_invis"),
				(call_script, "script_fate_aoe_attack", ":servant_agent", 3000, 3, 1, 50),
			(else_try),
				(eq, ":equipment", "itm_excalibur_gold"),
				(agent_unequip_item, ":servant_agent", "itm_excalibur_gold"),
				(agent_equip_item, ":servant_agent", "itm_excalibur_invis"),
				(agent_set_wielded_item, ":servant_agent", "itm_excalibur_invis"),
				# (call_script, "script_fate_aoe_attack", ":servant_agent", 3000, 3, 1, 50),
				
			(else_try),
				(display_message, "@Saber must be using Excalibur!"),
			(try_end),
			
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_saber_02_01"), # "Aestus Domus Aurea" #Nero Claudius
			# Summon Arena, heal a bit, infinite mana and skill usages, and limits enemies skills
			(scene_prop_get_num_instances, ":i", "spr_aestus"),
			(try_begin),
				(le, ":i", 0),
				
				(copy_position, pos11, pos10),
				(copy_position, pos13, pos10),
				# (position_set_z_to_ground_level, pos13),
				(position_get_distance_to_terrain, ":altitude", pos13),
				# (val_div, ":altitude", -10),
				
				(try_for_prop_instances, ":prop"),
					(prop_instance_get_position, pos12, ":prop"),
					 
					(get_distance_between_positions, ":dist", pos12, pos13),
					(le, ":dist", 2300),
					 
					(position_get_rotation_around_z, ":o_pos_rot", pos10),
					(position_get_x, ":o_pos_x", pos12),
					(position_get_y, ":o_pos_y", pos12),
					(position_get_z, ":o_pos_z", pos12),
					
					(scene_prop_set_slot, ":prop", fate_prop_original_pos_x, ":o_pos_x"),
					(scene_prop_set_slot, ":prop", fate_prop_original_pos_y, ":o_pos_y"),
					(scene_prop_set_slot, ":prop", fate_prop_original_pos_z, ":o_pos_z"),
					(scene_prop_set_slot, ":prop", fate_prop_original_pos_rot, ":o_pos_rot"),
					 
					 (position_move_z, pos12, -1500),
					 (prop_instance_enable_physics, ":prop", 0),
					 (prop_instance_animate_to_position, ":prop", pos12, 500),
					 # (scene_prop_set_visibility, ":prop", 0),
					 # (scene_prop_fade_out, ":prop", 1),
				(try_end),
				
				(val_mul, ":altitude", -1),
				(store_sub, ":hidden", ":altitude", 1500),
				(position_move_z, pos13, ":altitude"),
				(position_move_z, pos11, ":hidden"),
				
				(set_spawn_position, pos11),
				(spawn_scene_prop, "spr_aestus"),
				(scene_prop_get_instance, ":instance", "spr_aestus", ":i"),
				(prop_instance_animate_to_position, ":instance", pos13, 500),
				
				
				(position_get_rotation_around_z, ":o_pos_rot", pos13),
				(position_get_x, ":o_pos_x", pos13),
				(position_get_y, ":o_pos_y", pos13),
				(position_get_z, ":o_pos_z", pos13),
				
				(scene_prop_set_slot, ":instance", fate_prop_original_pos_x, ":o_pos_x"),
				(scene_prop_set_slot, ":instance", fate_prop_original_pos_y, ":o_pos_y"),
				(scene_prop_set_slot, ":instance", fate_prop_original_pos_z, ":o_pos_z"),
				(scene_prop_set_slot, ":instance", fate_prop_original_pos_rot, ":o_pos_rot"),
				
				(scene_prop_set_slot, ":instance", fate_prop_owner, ":servant_agent"),
				(scene_prop_set_slot, ":instance", fate_prop_spawn_time, ":time"),
				#(assign, reg15, ":time"),
				#(display_message, "@{reg15}"),
			(else_try),
				(display_message, "@The Golden Theatre has already been deployed!"),
			(try_end),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_saber_02_02"), # "Laus Saint Claudius" #Nero Claudius
			# Dash forwards and huge strike with floral particles.
			(agent_set_attack_action, ":servant_agent", 1, 0),
			(item_set_slot, "itm_aestus_estus", fate_weapon_activated, 3),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_saber_03_01"), # "Balmung" #Siegfried
			
			(try_begin),
				(eq, ":equipment", "itm_balmung"),
				(call_script, "script_fate_aoe_attack", ":servant_agent", 50000, 3, 1, 500),
			(else_try),
				(display_message, "@Saber must be using Balmung!"),
			(try_end),
			
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_saber_03_02"), # "Armor of Fafnir" #Siegfried
			# Passive, but can be activated to enhance armor rating by 1 stage for 45 seconds
			(agent_set_slot, ":servant_agent", slot_agent_armor_boost, 5),
			(agent_set_slot, ":servant_agent", slot_agent_armor_boost_timer, 45),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_saber_03_03"), # "Das Rheingold" #Siegfried
			# Heals a large amount, but also adds gold to player team per week
			
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_saber_04"), # "Caladbolg" #Fergus mac Roich
			(try_begin),
				# AOE attack underneath enemies after stabbing ground
				(eq, ":equipment", "itm_caladbolg"),
				# (call_script, "script_fate_aoe_attack", ":servant_agent", 25000, 1, 2, 500),
				
				(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_saber_04"),
				(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 99999999),
				(start_presentation, "prsnt_fate_rts_camera"),
				
			(else_try),
				(display_message, "@Saber must equip his sword"),
			(try_end),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_saber_05_01"), # "Excalibur" #Richard Lionheart
			(neq, ":equipment", -1), # So long as he has anything, he can use it as Excalibur
			(call_script, "script_fate_aoe_attack", ":servant_agent", 25000, 3, 1, 500),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_saber_05_02"), # "Wandering King Does Not Conquer Alone"
		(else_try),
		#Lancer Noble Phantasms
			(eq, ":noble_phantasm", "str_noble_phantasm_lancer_01_01"), 
			# "Gáe Bolg: Barbed Spear that Pierces with Death" #Cu Chulainn (Single Target)
			(try_begin),
				(eq, ":equipment", "itm_gae_bolg"),
				(agent_unequip_item, ":servant_agent", "itm_gae_bolg"),
				(agent_equip_item, ":servant_agent", "itm_gae_bolg_thrown"),
			(else_try),
				(display_message, "@Lancer must equip his spear"),
			(try_end),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_lancer_01_02"), 
			# "Gáe Bolg: Soaring Spear that Strikes with Death" #Cu Chulainn Anti Army Huge mass
			(try_begin),
				(eq, ":equipment", "itm_gae_bolg"),
				# (agent_unequip_item, ":servant_agent", "itm_gae_bolg"),
				# (agent_equip_item, ":servant_agent", "itm_gae_bolg_thrown"),
				# (item_set_slot, "itm_gae_bolg_thrown", fate_weapon_activated, 25),
				
				(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_lancer_01_02"),
				(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 99999999),
				(start_presentation, "prsnt_fate_rts_camera"),
				
			(else_try),
				(display_message, "@Lancer must equip his spear"),
			(try_end),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_lancer_02_01"), 
			# "Gáe Dearg: Crimson Rose of Exorcism" #Diarmuid Ua Duibhne
			(try_begin),
				(item_slot_ge, "itm_gae_buidhe", fate_weapon_activated, 1),
				(display_message, "@Gáe Buidhe is already active!"),
			(else_try),
				(agent_set_wielded_item, ":servant_agent", "itm_gae_dearg"),
				(item_set_slot, "itm_gae_dearg", fate_weapon_activated, 25),
			(try_end),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_lancer_02_02"), 
			# "Gáe Buidhe: Yellow Rose of Mortality" #Diarmuid Ua Duibhne
			(try_begin),
				(item_slot_ge, "itm_gae_dearg", fate_weapon_activated, 1),
				(display_message, "@Gáe Dearg is already active!"),
			(else_try),
				(agent_set_wielded_item, ":servant_agent", "itm_gae_buidhe"),
				(item_set_slot, "itm_gae_buidhe", fate_weapon_activated, 25),
			(try_end),

		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_lancer_03_01"), 
			# "Mac an Luin" #Fionn mac Cumhaill
			# Shoots water beam?
			(try_begin),
				(item_slot_ge, "itm_mac_an_luin", fate_weapon_activated, 1),
				(display_message, "@Mac an Luin is already active!"),
			(else_try),
				(agent_set_wielded_item, ":servant_agent", "itm_mac_an_luin"),
				(item_set_slot, "itm_mac_an_luin", fate_weapon_activated, 5),
			(try_end),

		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_lancer_03_02"), # "Fintan Ginegas" #Fionn mac Cumhaill
			# Boosts wisdom and intelligence
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_lancer_03_03"), # "Uisce Beatha" #Fionn mac Cumhaill
			# Healing magic, can heal nearly any wound
			(agent_set_hit_points, ":servant_agent", 100),
			
			(try_for_range, ":body_part", fate_agent_wounds_larm, fate_agent_dodges),
				(agent_get_slot, ":wounds", ":servant_agent", ":body_part"),
				(val_sub, ":wounds", 40),
				(val_max, ":wounds", 0),
				(agent_set_slot, ":servant_agent", slot_fate_permanent_injury, ":wounds"),
			(try_end),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_lancer_04_01"), # "" #Hector
			# Throws for a huge AOE
			(try_begin),
				(eq, ":equipment", "itm_durindana"),
				(agent_unequip_item, ":servant_agent", "itm_durindana"),
				(agent_equip_item, ":servant_agent", "itm_durindana_thrown"),
				(agent_set_wielded_item, ":servant_agent", "itm_durindana_thrown"),
			(else_try),
				(eq, ":equipment", "itm_durindana_thrown"),
				(agent_unequip_item, ":servant_agent", "itm_durindana_thrown"),
				(agent_equip_item, ":servant_agent", "itm_durindana"),
				(agent_set_wielded_item, ":servant_agent", "itm_durindana"),
			(else_try),
				(eq, ":equipment", "itm_durindana_spada"),
				(agent_unequip_item, ":servant_agent", "itm_durindana_spada"),
				(agent_equip_item, ":servant_agent", "itm_durindana_thrown"),
				(agent_set_wielded_item, ":servant_agent", "itm_durindana_thrown"),
			(try_end),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_lancer_04_02"), # "Durindana Spada" #Hector
			# converts to a sword for a lot of quick damage
			(try_begin),
				(eq, ":equipment", "itm_durindana"),
				(agent_unequip_item, ":servant_agent", "itm_durindana"),
				(agent_equip_item, ":servant_agent", "itm_durindana_spada"),
				(agent_set_wielded_item, ":servant_agent", "itm_durindana_spada"),
			(else_try),
				(eq, ":equipment", "itm_durindana_spada"),
				(agent_unequip_item, ":servant_agent", "itm_durindana_spada"),
				(agent_equip_item, ":servant_agent", "itm_durindana"),
				(agent_set_wielded_item, ":servant_agent", "itm_durindana"),
			(try_end),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_lancer_05_01"), # "Brynhildr Romantia" #Brynhildr
			# Gigantic Spear with AoE
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_lancer_05_02"), # "Brynhildr Komédia" #Brynhildr
		(else_try),
		#Archer Noble Phantasms
			(eq, ":noble_phantasm", "str_noble_phantasm_archer_01"), # "Unlimited Blade Works" #EMIYA
			# Teleport into a small scene that gives EMIYA unlimited mana and flings weapons at the enemy
						
			(call_script, "script_reality_marble", ":servant_agent", 25),
			(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_01"),
			(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 30),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_archer_02_01"), 
			# "Enuma Elish: The Star of Creation that Split Heaven and Earth" #Gilgamesh
			# Destroy props and agents in front of 
			(eq, ":equipment", "itm_gilgamesh_ea"),
			(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_02_01"),
			(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 99999999),
			(start_presentation, "prsnt_fate_rts_camera"),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_archer_02_02"), # "Enkidu: Chains of Heaven" #Gilgamesh
			# Shoots chain props at the enemy, freeze in place, with longer times for divinity
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_archer_02_03"), # "Potion of Youth" #Gilgamesh
			# Heals entirely, but maybe switch to young gil with only Gate of Babylon
			(agent_set_hit_points, ":servant_agent", 100),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_archer_03_01"), # "Phoebus Catastrophe" #Atalanta
			# Shoots arrow into sky, several hundred arrows are launched at enemies.
			#(agent_unequip_item, ":servant_agent", "itm_atalanta_arrows"),
			#(agent_equip_item, ":servant_agent", "itm_calamity_arrows"),
			#(agent_set_wielded_item, ":servant_agent", "itm_calamity_arrows"),
			(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_03_01"),
			(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 99999999),
			(start_presentation, "prsnt_fate_rts_camera"),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_archer_03_02"), # "Agrius Metamorphosis" #Atalanta
			# Changes armor, makes Atalanta berserker, gives melee claw weapons
			(try_begin),
				(agent_slot_ge, ":servant_agent", slot_agent_reset_equip_timer, 1),
				(display_message, "@Atalanta is already feral!"),
				
			(else_try),	
			
				(try_for_range, ":slot", 0, 8),
					(agent_get_item_slot, ":item", ":servant_agent", ":slot"),
					(gt, ":item", 0),
					(agent_unequip_item, ":servant_agent", ":item"),
				(try_end),
				
				(try_begin),
					(agent_equip_item, ":servant_agent", "itm_atalanta_boar_body"),
					(agent_equip_item, ":servant_agent", "itm_atalanta_boar_head"),
					(agent_equip_item, ":servant_agent", "itm_atalanta_boar_legs"),
					(agent_equip_item, ":servant_agent", "itm_atalanta_boar_claw"),
					(agent_set_slot, ":servant_agent", slot_agent_armor_boost, 1),
					(agent_set_slot, ":servant_agent", slot_agent_armor_boost_timer, 30),
					(agent_set_slot, ":servant_agent", slot_agent_magic_res_boost, 2),
					(agent_set_slot, ":servant_agent", slot_agent_magic_res_boost_timer, 30),
					(agent_set_slot, ":servant_agent", slot_agent_reset_equip_timer, 30),
				(try_end),
			(try_end),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_archer_04_01"), # "No Face May King" #Robin Hood
			# Turns Invisible to get a better position
			# (agent_set_visibility, ":servant_agent", 0),
			(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_04_01"),
			(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 30),
			
			(try_for_range, ":slot", 0, 8),
					(agent_get_item_slot, ":item", ":servant_agent", ":slot"),
					(gt, ":item", 0),
					(agent_unequip_item, ":servant_agent", ":item", ":slot"),
					(try_begin),
						(this_or_next|eq, ":item", "itm_robin_boots"),
						(this_or_next|eq, ":item", "itm_robin_hood"),
						(eq, ":item", "itm_robin_gloves"),
						(val_add, ":item", 1),
					(try_end),
					(agent_equip_item, ":servant_agent", ":item", ":slot"),
			(try_end),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_archer_04_02"), # "Yew Bow" #Robin Hood
			# Makes the yew tree and causes damage over time while weakening attacks
			# (agent_set_ammo, ":servant_agent", "itm_yew_bolts", 1),
			#(agent_set_wielded_item, ":servant_agent", "itm_yew_bolts"),
			(agent_unequip_item, ":servant_agent", "itm_yew_bolts_actual"),
			(agent_equip_item, ":servant_agent", "itm_yew_bolts"),
			(agent_set_wielded_item, ":servant_agent", "itm_yew_bolts"),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_archer_05"), # "Stella" #Arash
			# Kills Arash, Meteor targets where arrow lands, destroys all props and agents within a range
			# (agent_unequip_item, ":servant_agent", "itm_created_arrows"),
			# (agent_equip_item, ":servant_agent", "itm_stella_arrow"),
			# (agent_set_wielded_item, ":servant_agent", "itm_stella_arrow"),
			(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_archer_05"),
			(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 99999999),
			(start_presentation, "prsnt_fate_rts_camera"),
		(else_try),
		#Rider Noble Phantasms
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_01_01"), # "Blood Fort Andromeda" #Medusa
			# Barrier that disables healing, mana regen, slowly damages while enhancing medusa
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_01_02"), # "Breaker Gorgon" #Medusa
			# Releases / Equips mask, Petrifies enemies within a certain time of removal
			# Petrification lasts 10sec * Magic Resistance
			(agent_get_item_slot, ":head", ":servant_agent", 4),
			(try_begin),
				(eq, ":head", "itm_medusa_mask"),
				# (display_message, "@Wearing mask, I should take it off."),
				(agent_unequip_item, ":servant_agent", "itm_medusa_mask"),
				(assign, ":petrification", 30),
				
				(try_for_agents, ":enemies", pos10, 1010), # within ten meters
					(agent_is_alive, ":servant_agent"),
					(agent_is_human, ":enemies"),
					(neq, ":enemies", ":servant_agent"),
					
					(agent_get_slot, ":magic_res", ":enemies", slot_agent_magic_resist),
					
					(agent_get_position, pos4, ":enemies"),
					(get_distance_between_positions_in_meters, ":distance", pos10, pos4), #distance from user
					(val_max, ":distance", 1),
					
					(position_transform_position_to_local, pos6, pos10, pos4),
					(position_get_x, ":val", pos6),
					(is_between, ":val", -50, 50), #horizontal body size, 1m
					(position_get_y, ":val", pos6),
					(is_between, ":val", 0, 1000),#check forward only. from 15cm in front of agent
					(position_get_z, ":val", pos6),
					(is_between, ":val", -150, 250),#vertical body size
					
					
					(val_div, ":petrification", ":magic_res"),
					(val_div, ":petrification", ":distance"),
					
					(assign, reg10, ":petrification"), 
					(display_message, "@Enemy Petrified for {reg10} Seconds!", 0xff00ff),
					
					(agent_get_slot, ":petrified", ":enemies", fate_agent_petrified),
					(val_add, ":petrified", ":petrification"),
					(agent_set_slot, ":enemies", fate_agent_petrified, ":petrified"),
					
					(particle_system_burst, "psys_game_hoof_dust_mud", pos4, ":petrification"),
				(try_end),
			(else_try),
				(le, ":head", 0),
				(agent_equip_item, ":servant_agent", "itm_medusa_mask"),
				# (display_message, "@ I should equip my mask now", 0xff00ff),
			(try_end),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_01_03"), # "Bellerophon" #Medusa
			# Summons Pegusus, and does a huge AOE
			(try_begin),
				(eq, ":horse", -1),
				(spawn_horse, "itm_bellerophon"),
			(else_try),
				(eq, ":horse_type", "itm_bellerophon"),
				(call_script, "script_fate_aoe_attack", ":servant_agent", 50000, 3, 1, 50),
			(else_try),
				(display_message, "@Rider must be unmounted, or on Bellerophon for this to work!", 0x55ffff),
			(try_end),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_01_04"), # "Harpe" #Medusa 
			# Equips spear, huge bonus damage to monster servants, ignores all damage nullification
			# harpe
			(try_begin),
				(eq, ":equipment", "itm_medusa_dagger"),
				(agent_unequip_item, ":servant_agent", "itm_medusa_dagger"),
				(agent_equip_item, ":servant_agent", "itm_harpe"),
				(agent_set_wielded_item, ":servant_agent", "itm_harpe"),
			(else_try),
				(eq, ":equipment", "itm_harpe"),
				(agent_unequip_item, ":servant_agent", "itm_harpe"),
				(agent_equip_item, ":servant_agent", "itm_medusa_dagger"),
				(agent_set_wielded_item, ":servant_agent", "itm_medusa_dagger"),
			(try_end),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_02_01"), # "Ionioi Hetairoi" #Iskandar
			# Summons a large army of class c servants to fight with Iskandar.
			# Maybe do huge reality marble that just fires volleys of spears and arrows at intervals.
			(agent_get_team, ":servant_team", ":servant_agent"),
			(add_visitors_to_current_scene, 42, "trp_ionioi_hetairoi_footman", 15, ":servant_team", 1),
			(add_visitors_to_current_scene, 42, "trp_ionioi_hetairoi_horseman", 15, ":servant_team", 1),
			(add_visitors_to_current_scene, 42, "trp_ionioi_hetairoi_archer", 15, ":servant_team", 1),
			(call_script, "script_reality_marble", ":servant_agent", 50),
			
			(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_rider_03_02"),
			(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 99999),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_02_02"), # "Gordius Wheel" #Iskandar
			# Summons / Desummons his Chariot, large mana cost when in use.
			(display_message, "@Mountable Scene Props are not yet implemented"),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_02_03"), # "Via Expugnatio" #Iskandar
			# His Chariot gains a lot of speed and does a ton of damage as it passes by them
			(display_message, "@This requires Rider to be on his Chariot"),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_03_01"), # "Andreias Amarantos" #Achilles
			# Gives 45sec of immunity vs non-divine servants and any attack under B
			# Attacks on left leg also will end this effect and slow him by 30%
			(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_rider_03_01"),
			(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 45),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_03_02"), # "Diatrekhon Aster Lonkhe" #Achilles
			# So long as he isn't in his chariot, he can create a duel field that eliminates immortality
			# and cannot be interrupted by outsiders
			(try_begin),
				(eq, ":equipment", "itm_diatrekhon"),
				
				(try_for_agents, ":agent"),
					(agent_is_human, ":agent"),
					(agent_is_alive, ":agent"),
					
					(agent_get_troop_id, ":troop", ":agent"),
					(troop_slot_ge, ":troop", slot_fate_servant_class, 1),
					
					(troop_set_slot, ":troop", slot_fate_cur_battle_continuations, 0),
					
					(agent_get_position, pos10, ":agent"),
					
					(try_for_range, ":slots", 0, 4),
						(agent_get_item_slot, ":item", ":agent", ":slots"),
						(gt, ":item", 0),
						(agent_unequip_item, ":agent", ":item", ":slots"),
						(set_spawn_position, pos10),
						(spawn_item, ":item", 1, 0),
					(try_end),
					
					(agent_set_slot, ":agent", slot_agent_armor_boost, 0),
					(agent_set_slot, ":agent", slot_agent_magic_res_boost, 0),
					(agent_set_slot, ":agent", slot_agent_invulnerability_timer, 0),
					(agent_set_slot, ":agent", fate_agent_dodges, 0),
					(agent_set_slot, ":agent", fate_agent_disable_skills, 1),
					
					(try_begin),
						(agent_get_slot, ":noble_phantasm", ":agent", slot_agent_active_noble_phantasm),
						(agent_set_slot, ":agent", slot_agent_active_np_timer, 0),
					(try_end),
				(try_end),
				(call_script, "script_reality_marble", ":servant_agent", 10),
				
				(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_rider_03_02"),
				(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 99999),
			(else_try),
				(display_message, "@Achilles needs his spear to summon the duel field"),
			(try_end),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_03_03"), # "Dromeus Kometes" #Achilles
			# If he's outside of his chariot, can be activated for instantanious movement for 45 seconds.
			(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_rider_03_03"),
			(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 45),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_03_04"), # "Akhilleus Kosmos" #Achilles
			# Shield that can block any NP up to Anti-Country
			(try_begin),
				(eq, ":lequipment", "itm_kosmos_shield"),
				(agent_set_animation, ":servant_agent", "anim_defend_shield_up", 1),
			(try_end),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_03_05"), # "Troias Tragoidia" #Achilles
			# Summons his Chariot, takes HUGE amounts of mana to use, Uses it to slam into opponents
			(display_message, "@Mountable Scene Props are not yet implemented"),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_04_01"), # "Hippogriff" #Astolfo
			# So long as it isn't a full moon, can summon his mount.
			# If used again, grants 10 seconds of teleportation and intangibility at huge mana cost
			(try_begin),
				(eq, ":horse", -1),
				(spawn_horse, "itm_hippogriff"),
			(else_try),
				(eq, ":horse_type", "itm_hippogriff"),
				(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_rider_04_01"),
				(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 10),
			(else_try),
				(display_message, "@Rider must be unmounted, or on Hippogriff for this to work!", 0x55ffff),
			(try_end),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_04_02"), # "Casseur de Logistille" #Astolfo
			# Nullify all magic under A-rank when called, excluding Reality Marbles
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_04_03"), # "La Black Luna" #Astolfo
			# Disables the ability to give orders outside of command seals, Weak enemies are evaporated
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_04_04"), # "Trap of Argalia" #Astolfo
			# Gives the next strike the power to trip an opponent. Instantly dismounts any mounted foe,
			# makes it so that the opponent cannot walk for 30 seconds check every 3 seconds vs luck stat.
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_05_01"), # "Interfectum Dracones" #Saint George
			# Allows St George to shoot a javelin of light from his sword
			(try_begin),
				(eq, ":equipment", "itm_ascalon"),
				(agent_set_animation, ":servant_agent", "anim_ready_pistol", 1),
				(agent_get_bone_position, pos0, ":servant_agent", 19, 1),

			
				(item_get_weapon_length, ":item_length", ":equipment"),
				(position_move_y, pos0, ":item_length"),
				(add_missile, ":servant_agent", pos0, 10000, "itm_astral_arrows", 0, "itm_astral_arrows", 0),
			(try_end),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_05_02"), # "Bayard" #Saint George
			# Summons horse, 
			# if mounted when invoked
			# Nullifies damage and will complete prevent a letal attack at the cost of the mount
			(try_begin),
				(eq, ":horse", -1),
				(spawn_horse, "itm_bayard"),
			(else_try),
				(eq, ":horse_type", "itm_bayard"),
				(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_rider_05_02"),
				(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 99999),
			(else_try),
				(display_message, "@Rider must be unmounted, or on Bayard for this to work!", 0x55ffff),
			(try_end),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_05_03"), # "Ascalon" #Saint George
			# Upon being invoked, ignores all damage resistance.
			(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_rider_05_03"),
			(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 15),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_rider_05_04"), # "Abyssus Draconis" #Saint George\
			# Turns the target to a draconic being for a time, giving St George a huge damage boost
			# St George inflicts slight stunlock on hitting Dragons
			(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_rider_05_03"),
			(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 30),
		(else_try),
		# Caster Noble Phantasms
			(eq, ":noble_phantasm", "str_noble_phantasm_caster_01"), # "Rule Breaker" #Medea
			# Nullifies all spells, takes the servant and turns them against their master
			(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_caster_01"),
			(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 30),
			(item_set_slot, "itm_rulebreaker", fate_weapon_activated, 30),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_caster_02"), # "Prelati's Spellbook" #Gilles de Rais
			# When Invoked, gives Caster infinite mana, and allows summoning of horrors
			(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_caster_02"),
			(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 30),
			
			(troop_set_slot, ":servant_troop", slot_fate_spell_slot_1, "str_special_caster_02_01_prelati"),
			(troop_set_slot, ":servant_troop", slot_fate_spell_slot_2, "str_special_caster_02_02_prelati"),
			(troop_set_slot, ":servant_troop", slot_fate_spell_slot_3, "str_special_caster_02_03_prelati"),
			(troop_set_slot, ":servant_troop", slot_fate_spell_slot_4, "str_special_caster_02_04_prelati"),
			
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_caster_03_01"), # "First Folio" #William Shakespeare
			# Summons shadow actors that can inflict death if servant is weak to it
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_caster_03_02"), # "The Globe" #William Shakespeare
			# Summons the globe theatre to make the servant face their truest enemy
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_caster_04"), # Golem Keter Malkuth #Avicebron
			# Summons a huge golem, requires the sacrifice of a magus, but makes a lumbering animated
			# scene prop that summons grass and plants around it, and does random beam attacks.
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_caster_05"), # "Sword of Paracelsus" #Hohenheim
			# Neutralizes magical properties, and allows the user to take on the property themselves, and shoots a beam
			# of Pure Ether, a substance v damaging to servants
			(agent_set_hit_points, ":servant_agent", 100),
			(try_begin),
				(eq, ":equipment", "itm_paracelsus_sword"),
				(agent_set_animation, ":servant_agent", "anim_ready_pistol", 1),
				(agent_get_bone_position, pos0, ":servant_agent", 19, 1),

			
				(item_get_weapon_length, ":item_length", ":equipment"),
				(position_move_y, pos0, ":item_length"),
				(add_missile, ":servant_agent", pos0, 10000, "itm_astral_arrows", 0, "itm_astral_arrows", 0),
			(try_end),
		(else_try),
		#Asssassin Noble Phantasms
			(eq, ":noble_phantasm", "str_noble_phantasm_assassin_generic"), # "Zabaniya" #Cursed Arm
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_assassin_01"), # "Zabaniya: Delusional Heartbeat" #Cursed Arm
			# Coverts are to weird demon arm that can inflict instant death if hit in the chest
			(agent_equip_item, ":servant_agent", "itm_cursed_arm_unwrapped_body"),
			(agent_unequip_item, ":servant_agent", "itm_cursedarm_wrapped"),
			(agent_equip_item, ":servant_agent", "itm_cursedarm_unwrapped"),
			(agent_equip_item, ":servant_agent", "itm_cursedarm"),
			(agent_set_wielded_item, ":servant_agent", "itm_cursedarm"),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_assassin_02"), # "Zabaniya: Delusional Illusion" #Hundred Faces
			# Allows the servant to split into multiple people with different skills but fractioned health
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_assassin_03"), # "Zabaniya: Febrile Inspiration" #Shadow Peeling
			# Allows assassin to nullify any damage less than a certain rank
			# Maybe do delusional Illusion?
			# Allows them to teleport upon taking damage from multiple attacks
			(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_assassin_03"),
			(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 30),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_assassin_04"), # "Zabaniya: Delusional Poison Body" #Serenity
			# Releases a poisonous mist, and any damage that hits them get's poison build up.
			(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_assassin_04"),
			(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 30),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_assassin_05"), 
			# "Azrael: The Angel That Announces Death" #King Hassan
			# Freezes an enemy and can cause death vs luck stat
		(else_try),
		#Berserker Noble Phantasms
			(eq, ":noble_phantasm", "str_noble_phantasm_berserker_01_01"), # "God Hand" #Heracles
			# Nullifies attacks under a A rank for 60 seconds
			(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_berserker_01_01"),
			(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 60),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_berserker_01_02"), # "Nine Lives" #Heracles
			# Allows him to unleash multiple attacks very, very quickly. Very Efficient.
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_berserker_02_01"), # "For Someone's Glory" #Lancelot
			# Produces smoke, doesn't allow stats to be revealed, and occasionally makes shadow clones
			# When Invoked, can appear as someone else,
			(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_berserker_02_01"),
			(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 15),
			(agent_set_slot, ":servant_agent", slot_agent_reset_equip_timer, 16),
			
			(store_random_in_range, ":random_class", 0, 7),
			
			(try_begin),
				(eq, ":random_class", 0),
				(store_random_in_range, ":random_servant", saber_begin, saber_end),
			(else_try),
				(eq, ":random_class", 1),
				(store_random_in_range, ":random_servant", archer_begin, archer_end),
			(else_try),
				(eq, ":random_class", 2),
				(store_random_in_range, ":random_servant", lancer_begin, lancer_end),
			(else_try),
				(eq, ":random_class", 3),
				(store_random_in_range, ":random_servant", rider_begin, rider_end),
			(else_try),
				(eq, ":random_class", 4),
				(store_random_in_range, ":random_servant", assassin_begin, assassin_end),
			(else_try),
				(eq, ":random_class", 5),
				(store_random_in_range, ":random_servant", caster_begin, caster_end),
			(else_try),
				(store_random_in_range, ":random_servant", berserker_begin, berserker_end),
			(try_end),
			
			(str_store_troop_face_keys, s34, ":random_servant"),
			(troop_set_face_keys, ":servant_troop", s34),
			
			(try_for_range, ":slot", 0, 8),
				(agent_get_item_slot, ":item", ":servant_agent", ":slot"),
				
				(try_begin),
					(gt, ":item", 0),
					(agent_unequip_item, ":servant_agent", ":item"),
				(try_end),
				
				(troop_get_inventory_slot, ":newitem", ":random_servant", ":slot"),
				
				(try_begin),
					(gt, ":newitem", 0),
					(agent_equip_item, ":servant_agent", ":newitem", ":slot"),
				(try_end),
				
			(try_end),
			
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_berserker_02_02"), # "Knight of Owner" #Lancelot
			# Makes any weapon a certain level of NP 
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_berserker_02_03"), # "Arondight" #Lancelot
			# On being Invoked, servant gains 1 level in all stats, deals extra damage to dragon attributes
			# Doubles saving throw, Turns off NP1 and NP2
			(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_berserker_02_03"),
			(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 60),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_berserker_03"), # "Crying Warmonger" #Spartacus
			# When Invoked turns damage into mana instead of taking damage.
			# % of mana over max gets turned into damage boost, but if reach over certain limit starts causing damage
			# then explosion
			(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_berserker_03"),
			(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 60),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_berserker_04_01"), # "Blasted Tree" #Frankenstein
			# Suicide blast that releases all mana as AOE attack
			(agent_set_hit_points, ":servant_agent", 1, 1),
			(agent_deliver_damage_to_agent, ":servant_agent", ":servant_agent", 10000),
			(agent_get_slot, ":mana", ":servant_agent", fate_agnt_cur_mana),
			(val_mul, ":mana", 100),
			(call_script, "script_fate_aoe_attack", ":servant_agent", 10000, 1, 2, ":mana"),
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_berserker_04_02"), # "Bridal Chest" #Frankenstein
			(agent_set_slot, ":servant_agent", slot_agent_active_noble_phantasm, "str_noble_phantasm_berserker_04_02"),
			(agent_set_slot, ":servant_agent", slot_agent_active_np_timer, 60),
			# Takes incoming energy damage, converts to mana and health
			# Vampirically reflects inflicted damage into mana and health when invoked 
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_berserker_05"), # "Chaos Labyrinthos" #Asterios
			# Summons labrynth, gives berserker bounus damage and infinite mana.
			(scene_prop_get_num_instances, ":i", "spr_maze"),
			(try_begin),
				(le, ":i", 0),
				
				(copy_position, pos11, pos10),
				(copy_position, pos13, pos10),
				# (position_set_z_to_ground_level, pos13),
				(position_get_distance_to_terrain, ":altitude", pos13),
				# (val_div, ":altitude", -10),
				
				(try_for_prop_instances, ":prop"),
					 (prop_instance_get_position, pos12, ":prop"),
					 
					 (get_distance_between_positions, ":dist", pos12, pos13),
					 (le, ":dist", 3500),
					 
					 (position_get_rotation_around_z, ":o_pos_rot", pos10),
					(position_get_x, ":o_pos_x", pos12),
					(position_get_y, ":o_pos_y", pos12),
					(position_get_z, ":o_pos_z", pos12),
					
					(scene_prop_set_slot, ":prop", fate_prop_original_pos_x, ":o_pos_x"),
					(scene_prop_set_slot, ":prop", fate_prop_original_pos_y, ":o_pos_y"),
					(scene_prop_set_slot, ":prop", fate_prop_original_pos_z, ":o_pos_z"),
					(scene_prop_set_slot, ":prop", fate_prop_original_pos_rot, ":o_pos_rot"),
					 
					 (position_move_z, pos12, -1500),
					 (prop_instance_enable_physics, ":prop", 0),
					 (prop_instance_animate_to_position, ":prop", pos12, 500),
					 # (scene_prop_set_visibility, ":prop", 0),
					 # (scene_prop_fade_out, ":prop", 1),
				(try_end),
				
				(val_mul, ":altitude", -1),
				(store_sub, ":hidden", ":altitude", 750),
				(position_move_z, pos13, ":altitude"),
				(position_move_z, pos11, ":hidden"),
				
				(set_spawn_position, pos11),
				(spawn_scene_prop, "spr_maze"),
				(scene_prop_get_instance, ":instance", "spr_maze", ":i"),
				(prop_instance_animate_to_position, ":instance", pos13, 500),
				
				
				(position_get_rotation_around_z, ":o_pos_rot", pos13),
				(position_get_x, ":o_pos_x", pos13),
				(position_get_y, ":o_pos_y", pos13),
				(position_get_z, ":o_pos_z", pos13),
				
				(scene_prop_set_slot, ":instance", fate_prop_original_pos_x, ":o_pos_x"),
				(scene_prop_set_slot, ":instance", fate_prop_original_pos_y, ":o_pos_y"),
				(scene_prop_set_slot, ":instance", fate_prop_original_pos_z, ":o_pos_z"),
				(scene_prop_set_slot, ":instance", fate_prop_original_pos_rot, ":o_pos_rot"),
				
				(scene_prop_set_slot, ":instance", fate_prop_owner, ":servant_agent"),
				(scene_prop_set_slot, ":instance", fate_prop_spawn_time, ":time"),
				#(assign, reg15, ":time"),
				#(display_message, "@{reg15}"),
			(else_try),
				(display_message, "@The Maze has already been deployed!"),
			(try_end),
		(else_try),
		#Avenger Noble Phantasms 
			(eq, ":noble_phantasm", "str_noble_phantasm_avenger_01"), # "Verg Avesta" #Angra Mainyu
		(else_try),
		#Shielders Noble Phantasms
			(eq, ":noble_phantasm", "str_noble_phantasm_shielder_01_01"), # "Lord Chaldeas" #Mashu Kyrielight
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_shielder_01_02"), # "Lord Camelot" #Mashu Kyrielight
		(else_try),
		#Rulers Noble Phantasms
			(eq, ":noble_phantasm", "str_noble_phantasm_ruler_01_01"), # "La Pucelle", #Jeanne d'Arc
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_ruler_01_02"), # "Luminosité Eternelle", #Jeanne d'Arc
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_ruler_02_01"), # "Evil Eater" #Amakusa Shirou Tokisada"
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_ruler_02_02"), # "Xanadu Matrix" #Amakusa Shirou Tokisada
		(else_try),
			(eq, ":noble_phantasm", "str_noble_phantasm_ruler_02_03"), # "Big Crunch" #Amakusa Shirou Tokisada
		(else_try),
			(display_message, "@This is an error!? How did you fire this NP? Lemme know!"),
		(try_end),

	]),
	
	("servant_skill_activation", 
	[
		(set_fixed_point_multiplier, 100),
	
		(store_script_param, ":servant_agent", 1),
		(store_script_param, ":skill", 2),
		# (agent_get_wielded_item, ":equipment", ":servant_agent", 0),
		(agent_get_position, pos10, ":servant_agent"),
		# (store_mission_timer_a, ":time"), # Time in seconds
		(agent_get_slot, ":dodges", ":servant_agent", fate_agent_dodges),

		# (agent_get_troop_id, ":servant", ":servant_agent"),
		
		(str_store_agent_name, s10, ":servant_agent"),
		(str_store_string, s11, ":skill"),

		(display_message, "@{s10} is being told to use {s11}"),
		
		(try_begin),
			(eq, ":skill", "str_skill_presence_concealment"),	# Assassin
		(else_try),
			(eq, ":skill", "str_skill_item_construction"), 	# Casters
		(else_try),
			(eq, ":skill", "str_skill_clairvoyance"),
			# Give 3 dodge chances
			(val_add, ":dodges", 3),
		(else_try),
			(eq, ":skill", "str_skill_disengage"),
			# Reset battle conditions without repairing curses
		(else_try),
			(eq, ":skill", "str_skill_evening_bell"),
		(else_try),
			(eq, ":skill", "str_skill_eye_of_the_mind_false"),
			# Give 1 dodge chances
			(val_add, ":dodges", 1),
		(else_try),
			(eq, ":skill", "str_skill_eye_of_the_mind_true"),
			# Give 3 dodge chances
			(val_add, ":dodges", 3),
		(else_try),
			(eq, ":skill", "str_skill_golden_fleece"),
		(else_try),
			(eq, ":skill", "str_skill_honor_of_the_battered"),
			# 30 seconds of turning damage to max health bonus and bonus strength at a rate 0f 10:1
		(else_try),
			(eq, ":skill", "str_skill_imperial_privilege"),
			# Pass any skill check, take any building, vehicle or weapon as your own.
		(else_try),
			(eq, ":skill", "str_skill_instinct"),
			# Give 2 Dodge Chances
			(val_add, ":dodges", 2),
		(else_try),
			(eq, ":skill", "str_skill_mana_burst"),
			# Coat weapon in an aura, give bonus damage, with charge attacks being beams
		(else_try),
			(eq, ":skill", "str_skill_mana_burst_flames"),
			# Coat weapon in a flame aura, give bonus damage, with charge attacks being aoe fans
		(else_try),
			(eq, ":skill", "str_skill_shadow_lantern"),
		(else_try),
			(eq, ":skill", "str_skill_shapeshift"),
		(else_try),
			(gt, ":skill", 0),
			(display_message, "@This is not an active combat skill."),
		(else_try),
			(display_message, "@This is an error!? How did you fire this Skill? Lemme know!"),
		(try_end),
		
		(agent_set_slot, ":servant_agent", fate_agent_dodges, ":dodges"),

	]),
	
	("rts_camera",
	[
	(set_fixed_point_multiplier, 100),

	(mission_cam_set_mode, 1, 2000, 0),
	
	(init_position, pos10),
	
	(mission_cam_get_position, pos30),
	(position_get_rotation_around_z, ":look_z", pos30),
	
	(position_copy_rotation, pos30, pos10),
	
	(position_move_z, pos30, 500),
	(position_rotate_z, pos30, ":look_z", 1),
	(position_move_y, pos30, -500),
	(position_rotate_x, pos30, -25),
	
	
	(mission_cam_set_position, pos30),
	]),
	
	#("script_lookat", pos##, pos##),
		#This script will take a position and rotate it such that +Y will face the target exactly.
		#It does this with some basic trig, I'll explain it in the comments inside the script.
		#INPUT:
		#	Param 1: Positional register of the pos## that will be rotated
		#	Param 2: Positional register of the pos## that will be targeted
		#OUTPUT: 
		#	N/A
	
	("lookat", 
	[
	(set_fixed_point_multiplier, 100),
	(store_script_param, ":looker", 1),		# This is the positional register that will turn to face the :target (+Y being forward)
	(store_script_param, ":target", 2),		# This is the positional register that will be targeted
	
	(copy_position, pos_looker, ":looker"),
	(copy_position, pos_target, ":target"),
	
	(init_position, pos_local),
	(assign, ":local", pos_local),				# This just makes the local_var :local equivalent to pos_local
	
	# (init_position, pos1),						# Get a clean positional register so. . . 
	(position_copy_rotation, ":looker", pos_local),	# We can scrub the rotational data from the :looker to simplify the math
	
	# (copy_position_origin, ":looker", pos30),	# This fixes a bug where if pos1 or pos13 was used, things got messed up
	# (copy_position, ":target", pos31),			# This fixes a bug where if pos1 or pos13 was used, things got messed up
	
	# The maths are pretty easy once we figure it out. Essentially, make two triangles with the two positions
	# Use those triangles to determine the angles that the :looker will need to rotate to face the :target
	
	(position_transform_position_to_local, ":local", ":looker", ":target"),	# We will use the local data to determine side lengths

	(position_get_z, ":l_z", ":looker"),		# :looker z value
	
	(position_get_z, ":t_z", ":target"),		# :target z value
	
	(position_get_x, ":local_x", ":local"),		# The opposite side of the first triangle
	(position_get_y, ":local_y", ":local"),		# The adjacent side of the first triangle
	
	(store_sub, ":z_dis", ":l_z", ":t_z"),		# The adjacent side of the second triangle
	
	(get_distance_between_positions, ":hypo", ":looker", ":target"),	# Distance between the positions will be the hypotenuse of both triangles

	(convert_to_fixed_point, ":z_dis"),		# When doing fixed point maths only one needs to be fixed point or else it won't convert from the fp correctly
	(convert_to_fixed_point, ":local_x"),	# When doing fixed point maths only one needs to be fixed point or else it won't convert from the fp correctly

		# SOH CAH TOA, sin(opp/hyp), cos(adj/hyp), tan(opp/adj)
	
	(gt, ":hypo", 0),
	
	(store_div, ":adj_hyp", ":z_dis", ":hypo"),	# Use the second triangle's adjacent / hypotenuse
	
	(store_acos, ":x_angle", ":adj_hyp"),		# To get the angle we need to adjust the pitch
	(convert_from_fixed_point, ":x_angle"),		# Convert from fixed point to make it usable by the rotation
	(val_add, ":x_angle", 270),					# Move it by 270 degrees to convert the angle into the correct quadrant
	
	(try_begin),
		(eq, ":local_y", 0),					# If the adjacent side's length would be 0
		(assign, ":z_angle", 0),				# Set the yaw to 0 to prevent division by zero errors 
	(else_try),
		(store_div, ":opp_adj", ":local_x", ":local_y"),	# Use the second triangle's opposite side / adjacent side
		(store_atan, ":z_angle", ":opp_adj"),				# To get the angle of the yaw
		(convert_from_fixed_point, ":z_angle"),				# Convert from fixed point to make it usable by the rotation
		(val_mul, ":z_angle", -1),							# Mirror the yaw to change it from CCW to CW
	(try_end),
	
	(try_begin),
		(lt, ":local_y", 0),			# If the :target is behind the :looker
		(val_add, ":z_angle", 180),		# Move the yaw 180 degrees
	(try_end),
	
	
	(position_rotate_z, ":looker", ":z_angle"),	# Rotate left/right (yaw) first
	(position_rotate_x, ":looker", ":x_angle"),	# Then rotate up/down (pitch) last
	]),
	
	# script_get_angle_between_points_180
	# Inputs: Reference Position, Target Postion 
	# Ouputs: Reg26: Difference in Angle on the X axis
	
	("get_angle_between_points_180", 
	[
	(set_fixed_point_multiplier, 100),
	(store_script_param, ":anchor", 1),			# This is the positional register that is looking
	(store_script_param, ":reference", 2),		# This is the positional register that will be targeted
	#(store_script_param, ":output", 3),
	
	(position_transform_position_to_local, pos13, ":anchor", ":reference"),
	
	(position_get_x, ":mx", pos13),
	(position_get_y, ":my", pos13),
	(position_get_z, ":mz", pos13),
	
	(store_atan2, reg26, ":my", ":mx"),
	
	(store_atan2, reg27, ":mz", ":my"),
	
	(convert_from_fixed_point, reg26),
	(convert_from_fixed_point, reg27),

	(try_begin),
		(lt, reg27, 0),			# If the angle is negative
		(val_mul, reg27, -1),	# Then make it positive
	(try_end),
	
	(try_begin),
		(ge, ":my", 0),
		(val_sub, reg26, 90),
	(try_end),
	
	(try_begin),
		(le, ":my", 0),
		(val_add, reg26, 90),
	(try_end),
	
	(try_begin),
		(gt, ":mx", 0),
		(val_mul, reg26, -1),
	(try_end),
	]),
	
	("get_angle_between_points_360", 
	[
	(set_fixed_point_multiplier, 100),
	(store_script_param, ":anchor", 1),			# This is the positional register that will turn to face the :target (+Y being forward)
	(store_script_param, ":reference", 2),		# This is the positional register that will be targeted
	#(store_script_param, ":output", 3),
	
	(position_transform_position_to_local, pos13, ":anchor", ":reference"),
	
	(position_get_x, ":mx", pos13),
	(position_get_y, ":my", pos13),
	(position_get_z, ":mz", pos13),
	
	(store_atan2, reg26, ":my", ":mx"),
	(store_atan2, reg27, ":mz", ":my"),
	
	(convert_from_fixed_point, reg26),
	(convert_from_fixed_point, reg27),
	
	(val_sub, reg26, 90),
	  
	(try_begin),
		(lt, reg26, 0),			# If the angle is negative
		(val_add, reg26, 360),	# This fixes negative ranges
	(try_end)
	]),
	
	("offscreen_volley",
	[
		(set_fixed_point_multiplier, 100),

		(store_script_param, ":servant_agent", 1),
		(store_script_param, ":enemy_agent", 2),
		(store_script_param, ":volley_amount", 3),
		
		(agent_get_position, pos31, ":enemy_agent"),
		
		
		(try_for_range, ":i", 0, ":volley_amount"),
			(agent_get_position, pos30, ":servant_agent"),
			(position_move_y, pos30, -10000),
			(position_set_z, pos30, 500),
			(store_random_in_range, ":rand_y", -1000, 1000),
			(store_random_in_range, ":rand_x", -1000, 1000),
					
			(position_move_y, pos30, ":rand_y", 0),
			(position_move_x, pos30, ":rand_x", 0),		
			
			# (call_script, "script_pos30_look_at_pos31"),
			(call_script, "script_lookat", pos30, pos31),
			
			(position_get_rotation_around_x, ":x", pos30),
			(val_mul, ":x", -1),
			
			(position_rotate_x, pos30, 15),
			(position_rotate_x, pos30, ":x"),
			(add_missile, ":servant_agent", pos30, 5200, "itm_tauropolos", 0, "itm_astral_arrows", 0), 
		(try_end),
	]),
	
	("reality_marble",
	[		
	(set_fixed_point_multiplier, 100),
	(store_script_param, ":marble_owner", 1),
	(store_script_param, ":range", 2),
	
	(convert_to_fixed_point, ":range"),
	
	(entry_point_get_position, pos51, 42),
	(entry_point_get_position, pos52, 43),
	
	(try_begin),
		(neq, ":marble_owner", -1),
	
		(mission_cam_set_screen_color, 0xFFFFFFFF),
		(mission_cam_animate_to_screen_color, 0x00FFFFFF, 300),
	
		(agent_get_position, pos40, ":marble_owner"),
		
		(try_for_prop_instances, ":marble", "spr_reality_marble"),
			(scene_prop_set_visibility, ":marble", 1),
			(prop_instance_enable_physics, ":marble", 1),
		(try_end),
		
		(try_for_prop_instances, ":marble", "spr_marble_cloudy"),
			(scene_prop_set_visibility, ":marble", 1),
		(try_end),
		
		(try_for_agents, ":agent", pos40, ":range"),
			(agent_is_human, ":agent"),
			(agent_is_alive, ":agent"),
			(agent_get_team, ":team", ":agent"),
			(try_begin),
				(eq, ":team", 0),
				(agent_set_position, ":agent", pos51),
			(else_try),
				(eq, ":team", 1),
				(agent_set_position, ":agent", pos52),			
			(try_end),
		(try_end),
		
	(else_try),
	
		(mission_cam_set_screen_color, 0xFFFFFFFF),
		(mission_cam_animate_to_screen_color, 0x00FFFFFF, 300),
	
		(try_for_prop_instances, ":marble", "spr_reality_marble"),
			(scene_prop_set_visibility, ":marble", 1),
			(prop_instance_enable_physics, ":marble", 1),
		(try_end),
		
		(try_for_prop_instances, ":marble", "spr_marble_cloudy"),
			(scene_prop_set_visibility, ":marble", 1),
		(try_end),
		
		(try_for_agents, ":agent"),
			(agent_is_human, ":agent"),
			(agent_is_alive, ":agent"),
			
			(agent_get_position, pos41, ":agent"),
			(position_set_z_to_ground_level, pos41),
			(agent_set_position, ":agent", pos41),
		(try_end),

	(try_end),

	(rebuild_shadow_map),
	]),
	
	("target_missiles",
	[
	
		(set_fixed_point_multiplier, 100),
		(store_script_param, ":shooter_agent", 1),
		(store_script_param, ":target_location", 2),
		(store_script_param, ":number_of_missiles", 3),
		(store_script_param, ":radius", 4),
		(store_script_param, ":missile_item", 5),
		
		(try_begin),
			(eq, ":missile_item", -2),
			(store_random_in_range, ":missile_item", "itm_gob_sword", "itm_shirou_shirt"),
		(try_end),
		
		(assign, ":launcher", ":missile_item"),
		
		(item_get_type, ":type", ":missile_item"),
		(agent_get_wielded_item, ":equipped_item", ":shooter_agent", 0),
		
		(try_begin),
			(this_or_next|eq, ":type", itp_type_arrows),
			(this_or_next|eq, ":type", itp_type_bolts),
			(eq, ":type", itp_type_bullets),
			
			(gt, ":equipped_item", 0),
			(assign, ":launcher", ":equipped_item"),
		(try_end),
		
		(agent_get_look_position, pos9, ":shooter_agent"), 			# Get Agent's look for math
		(copy_position, pos1, ":target_location"),
		(init_position, pos10), 									# Initial the Position that will be used for math 
				
		(position_copy_origin, pos10, pos1),						# duplicate the info from the trigger Pos to math Pos
				
		(position_get_rotation_around_z, ":look_z", pos9),			# Rip the Z-axis from the look pos
		(val_add, ":look_z", 180),									# Rotate 180 to ensure the missiles are coming towards the player
		(position_rotate_z, pos10, ":look_z", 1),					# apply the new Z-axis to math pos
				
		(position_move_z, pos10, 6000, 1),							# Move the missiles very high, they're from space after all
		(position_move_y, pos10, -5000, 0), 						# I decided this distance through trial-and-error for perfect arcs when the next line
		(position_rotate_x, pos10, -45, 1),							# This insures arrows fall towards the Earth, sort of a sharp angle. But if modified move_y needs to change too.
				
		(copy_position, pos9, pos10),								# I use this so that the randomization doesn't mess with the original math
				
		(val_add, ":number_of_missiles", 1),
		
		# (store_div, ":depth", -10000, ":radius"),
		
		(val_div, ":radius", 2),
		(store_mul, ":neg_radius", ":radius", -1),
		
		(try_for_range, ":spawn", 1, ":number_of_missiles"),							# number of arrows
			(copy_position, pos10, pos9),							# reset the pos every new arrow so randomized pos below doesn't mess with the center
			(store_random_in_range, ":rand_y", ":neg_radius", 0), 		# the rest creates a 10m sq that arrows fall between
			(store_random_in_range, ":rand_x", ":neg_radius", ":radius"),
			(store_random_in_range, ":rand_z", ":neg_radius", ":radius"),
					
			(position_move_y, pos10, ":rand_y", 0),
			(position_move_x, pos10, ":rand_x", 0),
			(position_move_z, pos10, ":rand_z", 0),										
																# finally add the arrows.
			(add_missile, ":shooter_agent", pos10, 5000, ":launcher", 0, ":missile_item", 0), 
		(try_end),
	]),
	
	
		#script_script-id:
		#This is a discription of the script
		#INPUT:
		#	Param 1: 
		#	Param 2:
		#OUTPUT: 
		#	What the output would be.
	
		# First Up, Magic Resistance
	
	# The math may ~look~ weird, but remember that a skill level of 1
	# is the best e.g. A-rank, so by dividing 100 by the resistance
	# we end up with the inverse i.e. lower numbers producing higher
	# numbers.
	
	# Example:
	# B rank Magic, D Rank Endurance
	# 2 + 4 * 4 = 18
	# 100 / 18 = ~11
	("reset_armor", [
	
	(store_script_param, ":agent", 1),
	(agent_get_troop_id, ":troop", ":agent"),
	
	(try_begin),
		(is_between, ":troop", servants_begin, servants_end),
		(troop_get_slot, ":endur", ":troop", fate_param_end),
		(troop_get_slot, ":agili", ":troop", fate_param_agi),
		(val_mul, ":agili", 4),
		(val_add, ":endur", ":agili"),
		(store_div, ":armor_level", 200, ":endur"),
	(else_try),
		(assign, ":armor_level", 4),
	(try_end),
	
	(assign, reg13, ":armor_level"),
	
	# (assign, ":equipment_armor", 10),
	(try_for_range, ":item_slot", 4, 8),
		(agent_get_item_slot, ":item", ":agent", ":item_slot"),
		(gt, ":item", 0),
		(item_get_slot, ":rank", ":item", fate_weapon_rank),
		(gt, ":rank", 0),
		(store_div, ":equipment_rank", 10, ":rank"),
		(val_add, ":armor_level", ":equipment_rank"),
	(try_end),
	
	(try_begin),
		(agent_slot_ge, ":agent", slot_agent_armor_boost_timer, 1),
		(agent_get_slot, ":boost", ":agent", slot_agent_armor_boost),
		(gt, ":boost", 0),
		
		(val_add, ":armor_level", ":boost"),
	(try_end),
	
	(assign, reg14, ":armor_level"), 
	
	(try_begin),
		(agent_get_slot, ":melt", ":agent", fate_agent_armor_debuff),
		(gt, ":melt", 0),
		
		(val_sub, ":armor_level", ":melt"),
	(try_end),
	
	(assign, reg15, ":armor_level"), 
	
	(display_message, "@Armor level: {reg13} + Boost = {reg14}^ {reg14} - armor damage = {reg15}"),
	
	(agent_set_slot, ":agent", slot_agent_armor_level, ":armor_level"),
	]),
	
	("reset_magicres", [
	
	(store_script_param, ":agent", 1),
	(agent_get_troop_id, ":troop", ":agent"),
	
	(try_begin),
		(is_between, ":troop", servants_begin, servants_end),
		(troop_get_slot, ":magic", ":troop", fate_param_mgc),
		(troop_get_slot, ":endur", ":troop", fate_param_end), 
		(val_mul, ":endur", 2),
		(val_add, ":magic", ":endur"),
		(store_div, ":magic_res", 100, ":magic"),
	(else_try),
		(assign, ":magic_res", 2),
	(try_end),
	
	# And this Clause runs through all their skill slots, and if
	# it has magic_resistance, check the skill level, invert it
	# (same story where less is better), then add it directly to
	# the magic resistance
	
	(try_for_range, ":slot", fate_skill_slot_begin, fate_skill_slot_end),
		(troop_get_slot, ":skill", ":troop", ":slot"),
		(eq, ":skill", "str_skill_magic_resistance"),
		(val_add, ":slot", 1),
		(troop_get_slot, ":skill_lvl", ":troop", ":slot"),
		(store_div, ":magic_res_skill", 10, ":skill_lvl"),
		(val_add, ":magic_res", ":magic_res_skill"),
	(try_end),
	
	(try_begin),
		(agent_slot_ge, ":agent", slot_agent_magic_res_boost_timer, 1),
		(agent_get_slot, ":boost", ":agent", slot_agent_magic_res_boost),
		(gt, ":boost", 0),
		
		(val_add, ":magic_res", ":boost"),
	(try_end),
	
	(agent_set_slot, ":agent", slot_agent_magic_resist, ":magic_res"),
	]),
   
   ("bullet_collision_particle", [
   
	(store_script_param, ":struck", 1),
	(store_script_param, ":distance", 2),
   
	(try_begin),
		(this_or_next|eq, ":struck", 1),
		(this_or_next|eq, ":struck", 8),
		(eq, ":struck", 9),
		
		# Blood Particle
		(assign, ":particle", "psys_bullet_distance_blood"),	# This fired even when the missile was blocked, move to damage trigger
	(else_try),
		(eq, ":struck", 0),
		(assign, ":particle", "psys_bullet_hit_ground"),
	(else_try),
		(is_between, ":struck", 2, 5),
		(assign, ":particle", "psys_bullet_hit_prop"),
	(else_try),
		(eq, ":struck", 10),
		(assign, ":particle", "psys_game_hoof_dust_mud"),
	(else_try),
		(assign, ":particle", "psys_bullet_hit_ground"),
	(try_end),				

	(this_or_next|eq, ":struck", 0),
	(this_or_next|is_between, ":struck", 2, 5),
	(eq, ":struck", 10),
	
	(particle_system_burst, ":particle", pos1, ":distance"),
   ]),
   
   ("fate_elemental_damage", [
	
	(store_script_param, ":weapon_element", 1),
	(store_script_param, ":troop", 2),
	(store_script_param, ":agent_id", 3),
	(store_script_param, ":charge_time", 4),
	
	(try_begin),
		(eq, ":charge_time", 0),
		(val_add, ":charge_time", 100),
	(try_end),
	
	(troop_get_slot, ":classification", ":troop", slot_fate_classification),	# Servants will take additional damage from Divine, Demon and Holy elements
	(troop_get_slot, ":troop_element", ":troop", fate_magic_attribute),			# Allows certain resistances/weaknesses based on origins
	
	(agent_get_slot, ":poison", ":agent_id", fate_agent_poison_build_up),
	(agent_get_slot, ":petrify", ":agent_id", fate_agent_petrify_build_up),
	(agent_get_slot, ":stun", ":agent_id", fate_agent_stun_build_up),
	(agent_get_slot, ":slow", ":agent_id", fate_agent_slow_build_up),
	(agent_get_slot, ":burn", ":agent_id", fate_agent_burn_build_up),
	(agent_get_slot, ":frost", ":agent_id", fate_agent_frost_build_up),
	(agent_get_slot, ":confuse", ":agent_id", fate_agent_confusion_build_up),
	(agent_get_slot, ":rage", ":agent_id", fate_agent_rage_build_up),
	(agent_get_slot, ":blind", ":agent_id", fate_agent_blind_build_up),
	(agent_get_slot, ":silent", ":agent_id", fate_agent_silence_build_up),
	(agent_get_slot, ":snare", ":agent_id", fate_agent_snare_build_up),
	(agent_get_slot, ":weak", ":agent_id", fate_agent_weaken_build_up),
	(agent_get_slot, ":armor", ":agent_id", fate_agent_armor_debuff_build_up),
	
	(assign, ":addtl_damage", 0),
	
	(try_begin),
		(eq, ":weapon_element", "str_dmg_type_normal"),
		
		(neq, ":troop_element", "str_dmg_type_earth"),
		(neq, ":troop_element", "str_dmg_type_divine"),
		(neq, ":troop_element", "str_dmg_type_dragon"),
		(neq, ":troop_element", "str_dmg_type_demonic"),
		(neq, ":troop_element", "str_dmg_type_undead"),
		
		(store_div, ":addtl_damage", ":charge_time", 10),
		(store_div, ":status_eff", ":charge_time", 2),
		(val_add, ":stun", ":status_eff"),
		
	(else_try),
		(eq, ":weapon_element", "str_dmg_type_fire"),
		
		(neq, ":troop_element", "str_dmg_type_water"),
		(neq, ":troop_element", "str_dmg_type_dragon"),
		(neq, ":troop_element", "str_dmg_type_demonic"),
		
		(store_div, ":addtl_damage", ":charge_time", 10),
		(store_div, ":status_eff", ":charge_time", 2),
		(val_add, ":burn", ":status_eff"),
		
		(eq, ":troop_element", "str_dmg_type_wind"),
		(val_mul, ":addtl_damage", 2),
		(val_add, ":weak", ":status_eff"),
		
	(else_try),
		(eq, ":weapon_element", "str_dmg_type_water"),
		
		(neq, ":troop_element", "str_dmg_type_earth"),
		(neq, ":troop_element", "str_dmg_type_demonic"),
		
		(store_div, ":addtl_damage", ":charge_time", 10),
		(store_div, ":status_eff", ":charge_time", 2),
		(val_add, ":frost", ":status_eff"),
		
		(this_or_next|eq, ":troop_element", "str_dmg_type_fire"),
		(eq, ":troop_element", "str_dmg_type_dragon"),
		
		(val_mul, ":addtl_damage", 2),
		(val_add, ":slow", ":status_eff"),
		
	(else_try),
		(eq, ":weapon_element", "str_dmg_type_wind"),
		
		(neq, ":troop_element", "str_dmg_type_fire"),
		
		(store_div, ":addtl_damage", ":charge_time", 10),
		(store_div, ":status_eff", ":charge_time", 2),
		(val_add, ":silent", ":status_eff"),
		
		(this_or_next|eq, ":troop_element", "str_dmg_type_earth"),
		(eq, ":troop_element", "str_dmg_type_demonic"),
		
		(val_mul, ":addtl_damage", 2),
		(val_add, ":weak", ":status_eff"),
		
	(else_try),
		(eq, ":weapon_element", "str_dmg_type_earth"),
		
		(neq, ":troop_element", "str_dmg_type_wind"),
		(neq, ":troop_element", "str_dmg_type_divine"),
		
		(store_div, ":addtl_damage", ":charge_time", 10),
		(store_div, ":status_eff", ":charge_time", 2),
		(val_add, ":snare", ":status_eff"),
		
		(this_or_next|eq, ":troop_element", "str_dmg_type_fire"),
		(eq, ":troop_element", "str_dmg_type_demonic"),
		
		(val_mul, ":addtl_damage", 2),
		(val_add, ":armor", ":status_eff"),
		
	(else_try),
		(eq, ":weapon_element", "str_dmg_type_divine"),
		
		(neq, ":troop_element", "str_dmg_type_earth"),
		(neq, ":troop_element", "str_dmg_type_dragon"),
		
		(store_div, ":addtl_damage", ":charge_time", 10),
		(store_div, ":status_eff", ":charge_time", 2),
		(val_add, ":confuse", ":status_eff"),
		
		(this_or_next|eq, ":troop_element", "str_dmg_type_divine"),
		(eq, ":troop_element", "str_dmg_type_demonic"),
		
		(val_mul, ":addtl_damage", 2),
		(val_add, ":burn", ":status_eff"),
		
	(else_try),
		(eq, ":weapon_element", "str_dmg_type_demonic"),
		
		# Effective on Undead, Normal and Holy, Cannot Harm Divine, Flat Increase 10% Charge Time, Increase Rage, Weakness and Burn Buildup by Charge Time
		
		(neq, ":troop_element", "str_dmg_type_dragon"),
		(neq, ":troop_element", "str_dmg_type_holy"),
		
		(store_div, ":addtl_damage", ":charge_time", 10),
		(store_div, ":status_eff", ":charge_time", 2),
		(val_add, ":rage", ":status_eff"),
		
		(this_or_next|eq, ":troop_element", "str_dmg_type_divine"),
		(this_or_next|eq, ":troop_element", "str_dmg_type_demonic"),
		(eq, ":troop_element", "str_dmg_type_undead"),
		
		(val_mul, ":addtl_damage", 2),
		(val_add, ":burn", ":status_eff"),
		
	(else_try),
		(eq, ":weapon_element", "str_dmg_type_dragon"),
		
		(store_div, ":addtl_damage", ":charge_time", 10),
		(store_div, ":status_eff", ":charge_time", 2),
		(val_add, ":armor", ":status_eff"),
		
		(this_or_next|eq, ":troop_element", "str_dmg_type_earth"),
		(eq, ":troop_element", "str_dmg_type_dragon"),
		
		(val_mul, ":addtl_damage", 2),
		(val_add, ":burn", ":status_eff"),
		
	(else_try),
		(eq, ":weapon_element", "str_dmg_type_holy"),
		
		(neq, ":troop_element", "str_dmg_type_holy"),
		(neq, ":troop_element", "str_dmg_type_divine"),
		
		(store_div, ":addtl_damage", ":charge_time", 10),
		(store_div, ":status_eff", ":charge_time", 3),
		(val_add, ":silent", ":status_eff"),
		(val_add, ":blind", ":status_eff"),
		
		(this_or_next|eq, ":troop_element", "str_dmg_type_earth"),
		(this_or_next|eq, ":troop_element", "str_dmg_type_demonic"),
		(eq, ":troop_element", "str_dmg_type_undead"),
		
		(val_mul, ":addtl_damage", 2),
		(val_add, ":petrify", ":status_eff"),
		
	(else_try),
		(eq, ":weapon_element", "str_dmg_type_undead"),
		
		(neq, ":troop_element", "str_dmg_type_holy"),
		(neq, ":troop_element", "str_dmg_type_divine"),
		
		(store_div, ":addtl_damage", ":charge_time", 10),
		(store_div, ":status_eff", ":charge_time", 3),
		(val_add, ":poison", ":status_eff"),
		(val_add, ":slow", ":status_eff"),
		
		(this_or_next|eq, ":troop_element", "str_dmg_type_normal"),
		(eq, ":troop_element", "str_dmg_type_earth"),
		
		(val_mul, ":addtl_damage", 2),
		(val_add, ":weak", ":status_eff"),
		
	(try_end),
	
	(try_begin),
		(eq, ":classification", "str_classification_servant"),

		(this_or_next|eq, ":weapon_element", "str_dmg_type_holy"),
		(this_or_next|eq, ":weapon_element", "str_dmg_type_divine"),
		(eq, ":weapon_element", "str_dmg_type_demonic"),
		(val_mul, ":addtl_damage", 2),
	(try_end),
	
	(assign, reg40, ":addtl_damage"),
	(agent_set_slot, ":agent_id", fate_agent_poison_build_up, ":poison"),
	(agent_set_slot, ":agent_id", fate_agent_petrify_build_up, ":petrify"),
	(agent_set_slot, ":agent_id", fate_agent_stun_build_up, ":stun"), 	
	(agent_set_slot, ":agent_id", fate_agent_slow_build_up, ":slow"), 	
	(agent_set_slot, ":agent_id", fate_agent_burn_build_up, ":burn"), 	
	(agent_set_slot, ":agent_id", fate_agent_frost_build_up, ":frost"), 	
	(agent_set_slot, ":agent_id", fate_agent_confusion_build_up, ":confuse"),
	(agent_set_slot, ":agent_id", fate_agent_rage_build_up, ":rage"), 	
	(agent_set_slot, ":agent_id", fate_agent_blind_build_up, ":blind"), 	
	(agent_set_slot, ":agent_id", fate_agent_silence_build_up, ":silent"), 
	(agent_set_slot, ":agent_id", fate_agent_snare_build_up, ":snare"), 	
	(agent_set_slot, ":agent_id", fate_agent_weaken_build_up, ":weak"), 	
	(agent_set_slot, ":agent_id", fate_agent_armor_debuff_build_up, ":armor"), 	
   
   ]),
   
	#("script_sort_agents_by_distance", agent_id, pos##),
	# This script produce and sort an array of all agents in a scene, based on distance from
	# the position provided by the scripter. This will skip over the anchor agent if provided
	# by leaving the agent_id parameter as 0, it will include all human agents in the scene
	#INPUT:
	#    Param 1: Anchor Agent (Leave as 0 to skip)
	#    Param 2: Anchor Position
	#OUTPUT:
	#    Reg45	: Total Agents (not counting Anchor Agent)
	# To sort, first store all agents in a scene and thier distance from anchor, unsorted. 
	# Iterate through all agent distances and swap the nearest with the lowest unsorted slot.
	# Then repeat until done. Store total agent count in reg45 to allow scripter to make
	# try_for_range loops using the total count as the upper limit.
	
   
	("sort_agents_by_distance",
		[
		# Input Variables
		(store_script_param, ":anchor_agent", 1),
		(store_script_param, ":anchor_pos", 2),
		
		# Initializing Other Variables
		(assign, ":array_slot", 0),						# Assign Minimum Slot Range
		(assign, ":agent_array", "trp_temp_array_a"),	# For Readability, create :LocalVar
		(assign, ":adist_array", "trp_temp_array_b"),	# For Readability, create :LocalVar

        (try_for_agents, ":agent"),													# Iterate through all agents
			(neq, ":agent", ":anchor_agent"),										# That is not the anchor
			(agent_is_human, ":agent"),												# And that are human
			
			(agent_get_position, pos45, ":agent"),									# Where are they?
			(get_distance_between_positions, ":distance", ":anchor_pos", pos45),	# How far is that from the anchor?
			
			(troop_set_slot, ":agent_array", ":array_slot", ":agent"),				# Store this agent in the first available blank slot
			(troop_set_slot, ":adist_array", ":array_slot", ":distance"),			# Store this distnace in the first available blank slot
			
			(val_add, ":array_slot", 1),											# Iterate that slot after this, moving to the next empty slot
		(try_end),
		
		(assign, reg45, ":array_slot"),
		
		(try_for_range, ":i", 0, ":array_slot"),									# Use the count from above to iterate through the array slots
			
			(assign, ":comp_dist", 999999),											# Large number to check against
			
			(try_for_range, ":j", ":i", ":array_slot"),								# Loops in Loops
				(troop_get_slot, ":adist", ":adist_array", ":j"),					# Check the distance stored in slot numbered :j
				(lt, ":adist", ":comp_dist"),										# If less than the previous comparison number
				(neq, ":adist", 0),													# And not a blank slot
				(assign, ":comp_dist", ":adist"),									# Have it replace the comparison number, and,
				(assign, ":nearest", ":j"),											# Remember what slot I used to be in
			(try_end),
			
			(troop_get_slot, ":nearest_agent", ":agent_array", ":nearest"),			# Take the remembered slot and pull the assoc. agent
			(troop_get_slot, ":nearest_distance", ":adist_array", ":nearest"),		# Take the remembered slot and pull the assoc. distance
			
			(troop_get_slot, ":swap_agent", ":agent_array", ":i"),					# Take the previously stored agent
			(troop_get_slot, ":swap_distance", ":adist_array", ":i"),				# Take the previously stored distance
				
			(troop_set_slot, ":agent_array", ":nearest", ":swap_agent"),			# Store this swapped agent to whereever the new nearest used to live
			(troop_set_slot, ":adist_array", ":nearest", ":swap_distance"), 		# Store this swapped distance to whereever the new nearest used to live
			
			(troop_set_slot, ":agent_array", ":i", ":nearest_agent"),				# Store this nearest agent as the newest member of the sorted part of the array
			(troop_set_slot, ":adist_array", ":i", ":nearest_distance"), 			# Store this nearest distance as the newest member of the sorted part of the array
		(try_end),
       
        ]
    ),
	
	("camera_shake", [
		(assign, ":save", 1),
		(convert_to_fixed_point, ":save"),
		(set_fixed_point_multiplier, 100),
		
		(mission_cam_get_position, pos45),
		(get_player_agent_no, ":agent"),
		(agent_get_position, pos46, ":agent"),
		(agent_get_look_position, pos50, ":agent"),
			
		(try_begin),
			(init_position, pos0),
			(get_distance_between_positions, ":test", pos0, pos_game_cam_store),
			(eq, ":test", 0),
			(try_begin),
				(is_camera_in_first_person),
				(position_transform_position_to_local, pos47, pos46, pos45),
				(copy_position, pos_game_cam_store, pos47),
			(else_try),
				(position_transform_position_to_local, pos47, pos50, pos45),
				(copy_position, pos_game_cam_store, pos47),
			(try_end),
		(try_end),
		
		(mission_cam_set_mode, 1, 0, 0),
		
		(position_get_x, ":offset_x", pos_game_cam_store),
		(position_get_y, ":offset_y", pos_game_cam_store),
		(position_get_z, ":offset_z", pos_game_cam_store),
		
		
		
		(try_begin),
			(neg|is_camera_in_first_person),
			(copy_position_rotation, pos46, pos50), 
			(copy_position, pos47, pos46),
		(else_try),
			(position_get_rotation_around_x, ":ra_x", pos50),
			(copy_position, pos47, pos45),
		(try_end),
		
		(copy_position, pos47, pos46),
		
		(position_move_x, pos47, ":offset_x"),
		(position_move_y, pos47, ":offset_y"),
		(position_move_z, pos47, ":offset_z"),
		
		(position_rotate_x, pos47, ":ra_x"),
		
		(store_mul, ":neg_range", "$g_fate_cam_shake_frames", -1),

		(store_random_in_range, ":rand_01", ":neg_range", "$g_fate_cam_shake_frames"),
		(store_random_in_range, ":rand_02", ":neg_range", "$g_fate_cam_shake_frames"),
		(store_random_in_range, ":rand_03", ":neg_range", "$g_fate_cam_shake_frames"),
		
		(val_clamp, ":rand_02", -150, 151),
		
		(try_begin),
			(is_camera_in_first_person),
			(store_random_in_range, ":factor", -10, 11),
			(val_mul, ":rand_01", ":factor"),
			(val_mul, ":rand_03", ":factor"),
			
			(val_clamp, ":rand_01", -150, 151),
			(val_clamp, ":rand_03", -150, 151),
			(position_rotate_x_floating, pos47, ":rand_01"),
			(position_rotate_y_floating, pos47, ":rand_02"),
			(position_rotate_z_floating, pos47, ":rand_03"),
		(else_try),
			(position_move_x, pos47, ":rand_01"),
			(position_rotate_y_floating, pos47, ":rand_02"),
			(position_move_z, pos47, ":rand_03", 1),
		(try_end),
		
		(position_get_distance_to_ground_level, ":altitude", pos47),
		
		(try_begin),
			(lt, ":altitude", 50),
			#(store_sub, ":change", 50, ":altitude"),
			(position_set_z_to_ground_level, pos47),
			(position_move_z, pos47, 50),
		(try_end),
		
		(mission_cam_set_position, pos47),
		
		(set_fixed_point_multiplier, ":save"),
	]),
	
	#("script_player_ammo_check"),
	# This script will check to insure the player has appropriate ammo types equipped
	# based on their firearms. I.E. If they are wielding a rifle, pistol ammo will be
	# unequipped. If the player has multiple firearms equipped, they can equip both ammo
	# types, however, they will receive a warning if they do not equip any ammo.
	#INPUT:
	#    NONE
	#OUTPUT:
	#    NONE
	("player_ammo_check", [
	
		(assign, ":pistol", 0),
		(assign, ":rifle", 0),
		(assign, ":shottie", 0),
		(assign, ":rlauncher", 0),
		(assign, ":heavyw", 0),
		(assign, ":50cal", 0),
		
		# (assign, ":pistol_a", 0),
		# (assign, ":rifle_a", 0),
		# (assign, ":shottie_a", 0),
		# (assign, ":rlauncher_a", 0),
		# (assign, ":heavyw_a", 0),
	
		(try_for_range, ":inv_slot", 0, 4),
			(troop_get_inventory_slot, ":equipment", "trp_player", ":inv_slot"),
			(gt, ":equipment", 0),
			
			(try_begin),
				(is_between, ":equipment", "itm_beretta", "itm_aug"),
				(assign, ":pistol", 1),
			(else_try),
				(this_or_next|is_between, ":equipment", "itm_aug", "itm_remington_shotgun"),
				(is_between, ":equipment", "itm_remington_rifle", "itm_vulcan"),
				(assign, ":rifle", 1),
			(else_try),
				(eq, ":equipment", "itm_m82"),
				(assign, ":50cal", 1),
			(else_try),
				(is_between, ":equipment", "itm_remington_shotgun", "itm_m82"),
				(assign, ":shottie", 1),
			(else_try),
				(eq, ":equipment", "itm_rocket"),
				(assign, ":rlauncher", 1),
			(else_try),
				(eq, ":equipment", "itm_vulcan"),
				(assign, ":heavyw", 1),
			(try_end),
		(try_end),

		(try_for_range, ":inv_slot", 0, 4),
			(assign, ":unequip", 0),
			(troop_get_inventory_slot, ":equipment", "trp_player", ":inv_slot"),
			(gt, ":equipment", 0),
			(troop_get_inventory_slot_modifier, ":e_mod", "trp_player", ":inv_slot"),
			
			(try_begin),
				(is_between, ":equipment", "itm_origin_rounds", "itm_rifle_ammo"),
				(neq, ":pistol", 1),
				(str_store_string, s10, "@a Pistol"),
				(assign, ":unequip", 1),
			(else_try),
				(is_between, ":equipment", "itm_rifle_ammo", "itm_heavy_cartridge"),
				(neq, ":rifle", 1),
				(str_store_string, s10, "@a Rifle"),
				(assign, ":unequip", 1),
			(else_try),
				(is_between, ":equipment", "itm_gandr_shell", "itm_50cal"),
				(neq, ":shottie", 1),
				(str_store_string, s10, "@a Shotgun"),
				(assign, ":unequip", 1),
			(else_try),
				(is_between, ":equipment", "itm_rocket_ammo", "itm_gandr_shell"),
				(neq, ":rlauncher", 1),
				(str_store_string, s10, "@a Type 91 MANPADS Launcher"),
				(assign, ":unequip", 1),
			(else_try),
				(eq, ":equipment", "itm_heavy_cartridge"),
				(neq, ":heavyw", 1),
				(str_store_string, s10, "@a Vulcan 50mm Cannon"),
				(assign, ":unequip", 1),
			(else_try),
				(eq, ":equipment", "itm_50cal"),
				(neq, ":50cal", 1),
				(str_store_string, s10, "@a Beretta M82A1M"),
				(assign, ":unequip", 1),
			(try_end),
			
			(try_begin),
				(neg|map_free),
				(eq, ":unequip", 1),
				(eq, "$g_ammo_unequip", 0),
				(display_message, "@You will need to equip {s10} to use this ammo."),
				(display_message, "@Any incompatibile ammo will be unequipped when you leave this screen."),
				(assign, "$g_ammo_unequip", 1),
			(try_end),
			
			(eq, ":unequip", 1),
			(map_free),
			
			(set_show_messages, 0),
			(troop_set_inventory_slot, "trp_player", ":inv_slot", -1),
			(troop_add_item, "trp_player", ":equipment", ":e_mod"),
			(set_show_messages, 1),
			
		(try_end),

		(eq, "$g_ammo_unequip", 1),
		(map_free),
		(assign, "$g_ammo_unequip", 0),
		
	]),
	
# script_store_weekday
("store_weekday",
	[
		(store_script_param_1, ":reg"),
		(store_script_param_2, ":s_reg"),
		
		(assign, ":offset", 3), # This is an offset to make dates match realworld weekdays, tweak as you need.
		
		(store_current_day, ":day"),
		(val_add, ":day", ":offset"),
		(store_mod, ":weekday", ":day", 7),
		
		(assign, ":reg", ":weekday"),
		
		(try_begin),
		(eq, ":weekday", 0),
		(str_store_string, s3, "@Sunday"),
	  (else_try),
		(eq, ":weekday", 1),
		(str_store_string, s3, "@Monday"),
	  (else_try),
		(eq, ":weekday", 2),
		(str_store_string, s3, "@Tuesday"),
	  (else_try),
		(eq, ":weekday", 3),
		(str_store_string, s3, "@Wednesday"),
	  (else_try),
		(eq, ":weekday", 4),
		(str_store_string, s3, "@Thursday"),
	  (else_try),
		(eq, ":weekday", 5),
		(str_store_string, s3, "@Friday"),
	  (else_try),
		(str_store_string, s3, "@Saturday"),
	  (try_end),
	  
	  (str_store_string, ":s_reg", s3),
	]),
	
# script_assign_spellslot_icon
("assign_spellslot_icon",
[
	(store_script_param, ":icon", 1),

	(try_begin),
		(eq, "$current_slot", 1),
		(overlay_get_position, pos1, "$g_fate_spellbook_slot_1"),
		(overlay_set_display, "$g_fate_spellbook_slot_1", 0),
		(create_mesh_overlay, "$g_fate_spellbook_slot_1", ":icon"),
		(overlay_set_position, "$g_fate_spellbook_slot_1", pos1),
		(overlay_set_color, "$g_fate_spellbook_slot_1", highlighted_spell_col),
		(position_set_x, pos1, 400),
		(position_set_y, pos1, 400),
		(overlay_set_size, "$g_fate_spellbook_slot_1", pos1),
	(else_try),
		(eq, "$current_slot", 2),
		(overlay_get_position, pos1, "$g_fate_spellbook_slot_2"),
		(overlay_set_display, "$g_fate_spellbook_slot_2", 0),
		(create_mesh_overlay, "$g_fate_spellbook_slot_2", ":icon"),
		(overlay_set_position, "$g_fate_spellbook_slot_2", pos1),
		(overlay_set_color, "$g_fate_spellbook_slot_2", highlighted_spell_col),
		(position_set_x, pos1, 400),
		(position_set_y, pos1, 400),
		(overlay_set_size, "$g_fate_spellbook_slot_2", pos1),
	(else_try),
		(eq, "$current_slot", 3),
		(overlay_get_position, pos1, "$g_fate_spellbook_slot_3"),
		(overlay_set_display, "$g_fate_spellbook_slot_3", 0),
		(create_mesh_overlay, "$g_fate_spellbook_slot_3", ":icon"),
		(overlay_set_position, "$g_fate_spellbook_slot_3", pos1),
		(overlay_set_color, "$g_fate_spellbook_slot_3", highlighted_spell_col),
		(position_set_x, pos1, 400),
		(position_set_y, pos1, 400),
		(overlay_set_size, "$g_fate_spellbook_slot_3", pos1),
	(else_try),
		(overlay_get_position, pos1, "$g_fate_spellbook_slot_4"),
		(overlay_set_display, "$g_fate_spellbook_slot_4", 0),
		(create_mesh_overlay, "$g_fate_spellbook_slot_4", ":icon"),
		(overlay_set_position, "$g_fate_spellbook_slot_4", pos1),
		(overlay_set_color, "$g_fate_spellbook_slot_4", highlighted_spell_col),
		(position_set_x, pos1, 400),
		(position_set_y, pos1, 400),
		(overlay_set_size, "$g_fate_spellbook_slot_4", pos1),
	(try_end),
]),

	#script_circular_particle_burst:
	#Causes a radial burst of particles for explosions. 
	#INPUT:
	#	Param 1: particle system
	#	Param 2: number of particles in burst
	#	Param 3: burst position
	#	Param 4: optional, radius of burst, in cm
	#OUTPUT: 
	#	None, outside of the burst
("circular_particle_burst", [
	
	(assign, ":o_fp", 1),
	(convert_to_fixed_point, ":o_fp"),
	
	# Establish Inputs
	(store_script_param, ":particle", 1),
	(store_script_param, ":number", 2),
	(store_script_param, ":burst_pos", 3),
	(store_script_param, ":radius", 4),
	
	# Establish Variables
	(set_fixed_point_multiplier, 10000),
	
	(assign, ":angle", 360),
	(convert_to_fixed_point, ":angle"),
	
	(val_div, ":angle", ":number"),
	
	# Script
	(try_for_range, ":count", 0, ":number"),
		(copy_position_origin, pos31, ":burst_pos"),
		
		(val_mul, ":count", ":angle"),
		(position_rotate_z_floating, pos31, ":count", 1),
		(position_move_y, pos31, ":radius"),
		(particle_system_burst, ":particle", pos31, 1),
		(init_position, pos31),
	(try_end),
	
	(set_fixed_point_multiplier, ":o_fp"),
]),

	#script_missile_pos_to_pos:
	#Fires the provided missile from the first position towards the second position in the provided time, in seconds
	#INPUT:
	#	Param 1: missile weapon
	#	Param 2: start position register
	#	Param 3: end position register
	#	Param 4: firing agent id
	#	Param 5: type variable, 1 for time, 2 for speed
	#	Param 6: if p4 = 1, missile flight time in seconds, if p4 = 2, missile fixed speed
	#OUTPUT: 
	#	What the output would be.
("missile_pos_to_pos", [
	(set_fixed_point_multiplier, 1000), 						# Will keep math consistant
	
	(store_script_param, ":missile_id", 1),		# ID for missile item, will be used for both firing and missile id
	(store_script_param, ":firing_pos", 2),		# Position from which the missile will be shot, this will be manipulated to be able to hit target pos if possible
	(store_script_param, ":target_pos", 3),		# Position the missile will hit, if possible
	(store_script_param, ":agent_id", 4),		# Firing agent id
	(store_script_param, ":firing_type", 5),	# Swap variable, 1 for time, 2 for speed
	(store_script_param, ":missile_var", 6),	# Either missile flight time in seconds, or missile speed 
	
	# (store_trigger_param_2, ":hit"),
	# (eq, ":hit", 1),

	# (copy_position, pos10, pos1),								# Store the hit position
	# (agent_get_position, pos4, ":agent"),						# Store where the shooter is
	# (position_set_z_to_ground_level, pos10),
	# (position_set_z_to_ground_level, pos4),						# Make sure their feet are on the ground
	# (assign, ":chosen_angle", 30),
	
	# #(position_move_z, pos4, 100, 1), # around chest height

	# #(position_get_z, ":agent_z", pos4),							#
	# #(position_get_z, ":target_z", pos10),						#
	
	# #(store_sub, ":z_length", ":agent_z", ":target_z"),			# Difference in height between shooter and target
			
	# (get_distance_between_positions, ":distance", pos10, pos4),		# How Far am I from there?
	
	# # (store_mul, ":2theta", ":chosen_angle", 2),
	# # (convert_to_fixed_point, ":2theta"),
	# # (store_tan, ":tangent", ":2theta"),
	
	# # (val_add, ":z_length", ":tangent"), #Testing my idea of how the compensation works
	
	# #(val_add, ":distance", ":z_length"),			# This compensates for differences in terrain
	
	# # ### The Next Batch is what works. ########
	
	# # (store_mul, ":speed_prime", ":distance", 981),				# Distance = (2speed^2 / gravity)
	# # ####(val_div, ":speed_prime", 2),
	# # (store_sqrt, ":speed", ":speed_prime"),					# so Speed = sqrt((distance*gravity)/2)
	
	# # ####### Based on #########################
	# # ### d = (v^2 sin(2 a))/g #################
	# # ##########################################
	
	# # ### Before this is what works ############
	
	# # ##########################################
	# # ## For Modular Speed Function ############
	# # # v = sqrt(((d)*(g))/(sin(2 a))) ###
	# # ### d = distance, g = gravity, a = angle #
	# # ##########################################
	
	# # ##########################################
	# # ## For Modular Angle Function ############
	# # a = (sin^-1((d*g)/v^2))/2
	# # ### d = distance, g = gravity, a = angle #
	# # ##########################################
 
	
	 # (store_mul, ":numerator", ":distance", 981),
	 # (assign, reg15, ":numerator"),
	 # (store_mul, ":2theta", ":chosen_angle", 2),
	 # # (convert_to_fixed_point, ":2theta"),
	 # (store_sin, ":denominator", ":2theta"),
	 # (assign, reg16, ":denominator"),
	
	 # (store_div, ":speed_prime", ":numerator", ":denominator"),
	 # (assign, reg17, ":speed_prime"),
	 
	
	 # (store_sqrt, ":speed", ":speed_prime"),
	 # # (assign, reg18, ":speed"),
	 
	# # (display_log_message, "@{reg15}/{reg16} = {reg17}"),
	# # (display_log_message, "@sqrt{reg17} = {reg18}"),
	
	# # (assign, ":speed", 5250),
	
	# # (store_mul, ":numerator", ":distance", 981),
	# # (store_mul, ":denominator", ":speed", ":speed"),
	# # (store_div, ":prime", ":numerator", ":denominator"),
	# # (store_asin, ":intim", ":prime"),
	# # # (convert_from_fixed_point, ":intim"),
	# # (val_div, ":intim", 2),
	# # (assign, ":chosen_angle", ":intim"),
	
	# # (assign, reg17, ":intim"),
	# # (assign, reg10, ":speed"),
	# # (assign, reg11, ":distance"),
	# # (assign, reg12, 981),
	# # (display_log_message, "@asin({reg11}*981/sq{reg10})/2"),
	# # (display_log_message, "@Angle of release {reg17}"),
	
	
	
	# (position_rotate_y, pos10, 180), # Reflect the Projectile
	# (position_get_rotation_around_x, reg7, pos10), # Get new X after Y rotation
	# (val_mul, reg7, -1), 							# Get the inverse of X to cancel
	# (position_rotate_x, pos10, reg7),				# Cancel previous X
	# #(position_rotate_x, pos10, 45), 				# The ~*~Perfect~*~ Theta
	# (position_rotate_x, pos10, ":chosen_angle"),
	
	# (val_div, ":speed", 9),
					

	# #(position_move_z, pos10, 100, 1), # So it doesn't spawn inside the ground
	# (val_sub, ":agent", 1), # maybe use so that the agent "catches" the weapon

]),

("dodge_particle_burst", [
	(set_fixed_point_multiplier, 100),
	(store_script_param, ":agent", 1),
	
	(try_begin),
		(try_for_range, ":bone", hb_abdomen, hb_item_r),
		
			(agent_get_bone_position, pos15, ":agent", ":bone", 1),
			
			(try_begin),
				(eq, ":bone", hb_abdomen),
				(particle_system_burst, "psys_body_particle_torso", pos15, 1),
			(else_try),
				(eq, ":bone", hb_thigh_l),
				(particle_system_burst, "psys_body_particle_thigh_left", pos15, 1),
			(else_try),
				(eq, ":bone", hb_calf_l),
				(particle_system_burst, "psys_body_particle_foot_left", pos15, 1),
			(else_try),
				(eq, ":bone", hb_thigh_r),
				(particle_system_burst, "psys_body_particle_thigh_right", pos15, 1),
			(else_try),
				(eq, ":bone", hb_calf_r),
				(particle_system_burst, "psys_body_particle_foot_right", pos15, 1),
			(else_try),
				(eq, ":bone", hb_head),
				(particle_system_burst, "psys_body_particle_head", pos15, 1),
			(else_try),
				(eq, ":bone", hb_upperarm_l),
				(particle_system_burst, "psys_body_particle_arm_upper_left", pos15, 1),
			(else_try),
				(eq, ":bone", hb_forearm_l),
				(particle_system_burst, "psys_body_particle_forearm_left", pos15, 1),
			(else_try),
				(eq, ":bone", hb_hand_l),
				(particle_system_burst, "psys_body_particle_left_hand", pos15, 1),
			(else_try),
				(eq, ":bone", hb_upperarm_r),
				(particle_system_burst, "psys_body_particle_arm_upper_right", pos15, 1),
			(else_try),
				(eq, ":bone", hb_forearm_r),
				(particle_system_burst, "psys_body_particle_forearm_right", pos15, 1),
			(else_try),
				(eq, ":bone", hb_hand_r),
				(particle_system_burst, "psys_body_particle_right_hand", pos15, 1),
			(try_end),
		(try_end),
		
	(try_end),
]),

	#spell_slot_icon:
	#Fires the provided missile from the first position towards the second position in the provided time, in seconds
	#INPUT:
	#	Param 1: Selected Spell
	#OUTPUT: 
	#	N/A

("spell_slot_icon", [
	(store_script_param, ":object", 1),
	
	(try_begin),
		(eq, ":object", "$g_spellbook_fire_bolt"),

		(assign, ":icon", "mesh_ui_magic_fireball"),
	(else_try),
		(eq, ":object", "$g_spellbook_fire_aoe"),
		(assign, ":icon", "mesh_ui_magic_fire_wall"),	# Sea of Flames
		
	(else_try),
		(eq, ":object", "$g_spellbook_fire_buff"),
		(assign, ":icon", "mesh_ui_magic_sword_infuse"),	# Ignite Weapon
		
	(else_try),
		(eq, ":object", "$g_spellbook_fire_unique1"),
		(assign, ":icon", "mesh_mp_inventory_choose"),	# Melt Armor
		
	(else_try),
		(eq, ":object", "$g_spellbook_fire_unique2"),
		(assign, ":icon", "mesh_ui_magic_burn"), # Combustion
		
	(else_try),
		(eq, ":object", "$g_spellbook_fire_unique3"),
		(assign, ":icon", "mesh_ui_magic_invisible"), # Mirage Clone
		
	(else_try),
		(eq, ":object", "$g_spellbook_fire_unique4"),
		(assign, ":icon", "mesh_ui_magic_volcano"), # Lava Plume
		
	(else_try),
		(eq, ":object", "$g_spellbook_water_bolt"),
		(assign, ":icon", "mesh_ui_magic_ice_missile"), # Iceshot
		
	(else_try),
		(eq, ":object", "$g_spellbook_water_aoe"),
		(assign, ":icon", "mesh_ui_magic_wave"), # Tidalwave
		
	(else_try),
		(eq, ":object", "$g_spellbook_water_trap"),
		(assign, ":icon", "mesh_mp_inventory_choose"), # Liquify Ground
		
	(else_try),
		(eq, ":object", "$g_spellbook_water_unique1"),
		(assign, ":icon", "mesh_mp_inventory_choose"), # Healing Mist
		
	(else_try),
		(eq, ":object", "$g_spellbook_water_unique2"),
		(assign, ":icon", "mesh_mp_inventory_choose"), # Storm Cloud
		
	(else_try),
		(eq, ":object", "$g_spellbook_water_unique3"),
		(assign, ":icon", "mesh_mp_inventory_choose"), # Frost Spray
		
	(else_try),
		(eq, ":object", "$g_spellbook_wind_bolt"),
		(assign, ":icon", "mesh_ui_magic_lightning"), # Thunderbolt
		
	(else_try),
		(eq, ":object", "$g_spellbook_wind_aoe"),
		(assign, ":icon", "mesh_ui_magic_wind_trap"), # Stormwind
		
	(else_try),
		(eq, ":object", "$g_spellbook_wind_buff"),
		(assign, ":icon", "mesh_mp_inventory_choose"), # Fleetfoot
		
	(else_try),
		(eq, ":object", "$g_spellbook_wind_unique1"),
		(assign, ":icon", "mesh_ui_magic_sword_tornado"), # Aerostrike
		
	(else_try),
		(eq, ":object", "$g_spellbook_wind_unique2"),
		(assign, ":icon", "mesh_mp_inventory_choose"), # Featherfall
		
	(else_try),
		(eq, ":object", "$g_spellbook_wind_unique3"),
		(assign, ":icon", "mesh_ui_magic_wave"), # Blastwave
		
	(else_try),
		(eq, ":object", "$g_spellbook_wind_unique4"),
		(assign, ":icon", "mesh_ui_magic_silence"), # Mute
		
	(else_try),
		(eq, ":object", "$g_spellbook_wind_unique5"),
		(assign, ":icon", "mesh_ui_magic_double_jump"), # Air Jump
		
	(else_try),
		(eq, ":object", "$g_spellbook_earth_bolt"),
		(assign, ":icon", "mesh_mp_inventory_choose"), # Stoneshot
		
	(else_try),
		(eq, ":object", "$g_spellbook_earth_aoe"),
		(assign, ":icon", "mesh_mp_inventory_choose"), # Earthquake
		
	(else_try),
		(eq, ":object", "$g_spellbook_earth_buff"),
		(assign, ":icon", "mesh_ui_magic_stone_shield"), # Stoneskin
		
	(else_try),
		(eq, ":object", "$g_spellbook_earth_trap"),
		(assign, ":icon", "mesh_mp_inventory_choose"), # Quicksand
		
	(else_try),
		(eq, ":object", "$g_spellbook_earth_unique1"),
		(assign, ":icon", "mesh_mp_inventory_choose"), # Petrification
		
	(else_try),
		(eq, ":object", "$g_spellbook_earth_unique2"),
		(assign, ":icon", "mesh_mp_inventory_choose"), # Stonewall
		
	(else_try),
		(eq, ":object", "$g_spellbook_earth_unique3"),
		(assign, ":icon", "mesh_mp_inventory_choose"), # Stonewave
		
	(else_try),
		(eq, ":object", "$g_spellbook_earth_unique4"),
		(assign, ":icon", "mesh_mp_inventory_choose"), # Meteorcall
		
	(else_try),
		(eq, ":object", "$g_spellbook_earth_unique5"),
		(assign, ":icon", "mesh_mp_inventory_choose"), # Encase
	(try_end),
	
	(try_begin),
		(eq, "$current_slot", 1),
		(overlay_get_position, pos1, "$g_fate_spellbook_slot_1"),
		(overlay_set_display, "$g_fate_spellbook_slot_1", 0),
		(create_mesh_overlay, "$g_fate_spellbook_slot_1", ":icon"),
		(overlay_set_position, "$g_fate_spellbook_slot_1", pos1),
		(overlay_set_color, "$g_fate_spellbook_slot_1", highlighted_spell_col),
		(position_set_x, pos1, 400),
		(position_set_y, pos1, 400),
		(overlay_set_size, "$g_fate_spellbook_slot_1", pos1),
	(else_try),
		(eq, "$current_slot", 2),
		(overlay_get_position, pos1, "$g_fate_spellbook_slot_2"),
		(overlay_set_display, "$g_fate_spellbook_slot_2", 0),
		(create_mesh_overlay, "$g_fate_spellbook_slot_2", ":icon"),
		(overlay_set_position, "$g_fate_spellbook_slot_2", pos1),
		(overlay_set_color, "$g_fate_spellbook_slot_2", highlighted_spell_col),
		(position_set_x, pos1, 400),
		(position_set_y, pos1, 400),
		(overlay_set_size, "$g_fate_spellbook_slot_2", pos1),
	(else_try),
		(eq, "$current_slot", 3),
		(overlay_get_position, pos1, "$g_fate_spellbook_slot_3"),
		(overlay_set_display, "$g_fate_spellbook_slot_3", 0),
		(create_mesh_overlay, "$g_fate_spellbook_slot_3", ":icon"),
		(overlay_set_position, "$g_fate_spellbook_slot_3", pos1),
		(overlay_set_color, "$g_fate_spellbook_slot_3", highlighted_spell_col),
		(position_set_x, pos1, 400),
		(position_set_y, pos1, 400),
		(overlay_set_size, "$g_fate_spellbook_slot_3", pos1),
	(else_try),
		(overlay_get_position, pos1, "$g_fate_spellbook_slot_4"),
		(overlay_set_display, "$g_fate_spellbook_slot_4", 0),
		(create_mesh_overlay, "$g_fate_spellbook_slot_4", ":icon"),
		(overlay_set_position, "$g_fate_spellbook_slot_4", pos1),
		(overlay_set_color, "$g_fate_spellbook_slot_4", highlighted_spell_col),
		(position_set_x, pos1, 400),
		(position_set_y, pos1, 400),
		(overlay_set_size, "$g_fate_spellbook_slot_4", pos1),
	(try_end),
]),

("spell_charge_duration", [
	(store_script_param, ":agent", 1),
	(store_script_param, ":spell", 2),
	(store_script_param, ":return", 3),
	
	(agent_get_troop_id, ":troop", ":agent"),
	# Spell and its duration
	
	# Agent modifier for faster/slower casting.
	
	#Final Duration
]),

("fate_character_creation", [
	(store_script_param, ":troop", 1),
	
	(troop_get_type, ":type", ":troop"),
	
	# (troop_get_slot, ":attribute", ":troop", fate_magic_attribute),
	# (troop_get_slot, ":origin", ":troop", fate_magic_origin),
	
	# (troop_get_slot, ":parent_bg", ":troop", fate_parents_bg),
	# (troop_get_slot, ":occupation", ":troop", fate_plyr_background),
	# (troop_get_slot, ":education", ":troop", fate_education),
	# (troop_get_slot, ":training", ":troop", fate_training),
	# (troop_get_slot, ":mage_hist", ":troop", fate_family_magus_history),
	
	# ca_strength    
	# ca_agility     
	# ca_intelligence
	# ca_charisma    
	
	(try_begin),
		(eq, ":type", tf_masc_short),
		(troop_raise_attribute, ":troop", ca_strength, -1),
		(troop_raise_attribute, ":troop", ca_agility, 2),
		(troop_raise_attribute, ":troop", ca_charisma, -1),
	(else_try),
		(eq, ":type", tf_masc_avrg),
	(else_try),
		(eq, ":type", tf_masc_tall),
		(troop_raise_attribute, ":troop", ca_strength, 1),
		(troop_raise_attribute, ":troop", ca_agility, -2),
		(troop_raise_attribute, ":troop", ca_charisma, 1),
	(else_try),
		(eq, ":type", tf_masc_huge),
		(troop_raise_attribute, ":troop", ca_strength, 2),
		(troop_raise_attribute, ":troop", ca_agility, -3),
		(troop_raise_attribute, ":troop", ca_charisma, 1),
	(else_try),
		(eq, ":type", tf_fem_v_short),
		(troop_raise_attribute, ":troop", ca_strength, -3),
		(troop_raise_attribute, ":troop", ca_agility, -2),
		(troop_raise_attribute, ":troop", ca_intelligence, 2),
		(troop_raise_attribute, ":troop", ca_charisma, 3),
	(else_try),
		(eq, ":type", tf_fem_short),
		(troop_raise_attribute, ":troop", ca_strength, -2),
		(troop_raise_attribute, ":troop", ca_agility, -1),
		(troop_raise_attribute, ":troop", ca_intelligence, 1),
		(troop_raise_attribute, ":troop", ca_charisma, 2),
	(else_try),
		(eq, ":type", tf_fem_avrg),
		(troop_raise_attribute, ":troop", ca_strength, -1),
		(troop_raise_attribute, ":troop", ca_charisma, 1),
	(else_try),
        (eq, ":type", tf_fem_tall),
		(troop_raise_attribute, ":troop", ca_strength, 1),
		(troop_raise_attribute, ":troop", ca_agility, -2),
		(troop_raise_attribute, ":troop", ca_charisma, 1),
	(try_end),
	
	# (troop_set_slot, ":troop", fate_magic_projection_level, ":projection_level"),
	# (troop_set_slot, ":troop", fate_magic_projection_item, ":projection_item"),
	# (troop_set_slot, ":troop", fate_magic_reinforcement_level, ":reinforcement_level"),
	# (troop_set_slot, ":troop", fate_magic_familiar_level, ":familiar_level"),
	# (troop_set_slot, ":troop", fate_magic_familiar_type, ":familiar_type"),
	# (troop_set_slot, ":troop", fate_magic_heal_level, ":heal_level"),
	# (troop_set_slot, ":troop", fate_magic_sacrament_level, ":sacrament_level"),
	# (troop_set_slot, ":troop", fate_magic_rune_level, ":rune_level"),
	# (troop_set_slot, ":troop", fate_magic_necro_level, ":necro_level"),
	# (troop_set_slot, ":troop", fate_magic_fire_level, ":fire_level"),
	# (troop_set_slot, ":troop", fate_magic_earth_level, ":earth_level"),
	# (troop_set_slot, ":troop", fate_magic_wind_level, ":wind_level"),
	# (troop_set_slot, ":troop", fate_magic_special_level, ":special_level"),
	# (troop_set_slot, ":troop", fate_magic_water_align, ":water_align"),
	# (troop_set_slot, ":troop", fate_magic_fire_align, ":fire_align"),
	# (troop_set_slot, ":troop", fate_magic_wind_align, ":wind_align"),
	# (troop_set_slot, ":troop", fate_magic_earth_align, ":earth_align"),
	# (troop_set_slot, ":troop", fate_magic_ether_align, ":ether_align"),
	# (troop_set_slot, ":troop", fate_kwldge_celtic, ":celtic"),
	# (troop_set_slot, ":troop", fate_kwldge_english, ":english"),
	# (troop_set_slot, ":troop", fate_kwldge_greek_hist, ":greek_hist"),
	# (troop_set_slot, ":troop", fate_kwldge_greek_myth, ":greek_myth"),
	# (troop_set_slot, ":troop", fate_kwldge_roman, ":roman"),
	# (troop_set_slot, ":troop", fate_kwldge_swedish, ":swedish"),
	# (troop_set_slot, ":troop", fate_kwldge_french, ":french"),
	# (troop_set_slot, ":troop", fate_kwldge_bible, ":bible"),
	# (troop_set_slot, ":troop", fate_kwldge_assassin, ":assassin"),
	# (troop_set_slot, ":troop", fate_kwldge_modern, ":modern"),
	# (troop_set_slot, ":troop", fate_kwldge_ancient, ":ancient"),
	# (troop_set_slot, ":troop", fate_kwldge_philosophy, ":philosophy"),
	# (troop_set_slot, ":troop", slot_fate_chosen_spell, ":slot_1"),
	# (troop_set_slot, ":troop", slot_fate_spell_slot_1, ":slot_1"),
	# (troop_set_slot, ":troop", slot_fate_spell_slot_2, ":slot_2"),
	# (troop_set_slot, ":troop", slot_fate_spell_slot_3, ":slot_3"),
	# (troop_set_slot, ":troop", slot_fate_spell_slot_4, ":slot_4"),
	# (troop_set_slot, ":troop", slot_fate_max_minions, ":minions"),
]),

	#spell_slot_icon:
	#Fires the provided missile from the first position towards the second position in the provided time, in seconds
	#INPUT:
	#	Param 1: Agent
	#	Param 2: Equipped Item
	#OUTPUT: 
	#	reg30: Speed Modifier

("agent_speed_change", [
	# (store_script_param, ":agent", 1),
	# (store_script_param, ":spell", 2),
	# 
	# (agent_get_troop_id, ":troop", ":agent"),
	
]),

]
