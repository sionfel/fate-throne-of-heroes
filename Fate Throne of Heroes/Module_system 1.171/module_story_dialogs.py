# -*- coding: cp1254 -*-
from header_common import *
from header_dialogs import *
from header_operations import *
from header_parties import *
from header_item_modifiers import *
from header_skills import *
from header_triggers import *
from ID_troops import *
from ID_party_templates import *

from module_constants import *


####################################################################################################################
# During a dialog, the dialog lines are scanned from top to bottom.
# If the dialog-line is spoken by the player, all the matching lines are displayed for the player to pick from.
# If the dialog-line is spoken by another, the first (top-most) matching line is selected.
#
#  Each dialog line contains the following fields:
# 1) Dialogue partner: This should match the person player is talking to.
#    Usually this is a troop-id.
#    You can also use a party-template-id by appending '|party_tpl' to this field.
#    Use the constant 'anyone' if you'd like the line to match anybody.
#    Appending '|plyr' to this field means that the actual line is spoken by the player
#    Appending '|other(troop_id)' means that this line is spoken by a third person on the scene.
#       (You must make sure that this third person is present on the scene)
#
# 2) Starting dialog-state:
#    During a dialog there's always an active Dialog-state.
#    A dialog-line's starting dialog state must be the same as the active dialog state, for the line to be a possible candidate.
#    If the dialog is started by meeting a party on the map, initially, the active dialog state is "start"
#    If the dialog is started by speaking to an NPC in a town, initially, the active dialog state is "start"
#    If the dialog is started by helping a party defeat another party, initially, the active dialog state is "party_relieved"
#    If the dialog is started by liberating a prisoner, initially, the active dialog state is "prisoner_liberated"
#    If the dialog is started by defeating a party led by a hero, initially, the active dialog state is "enemy_defeated"
#    If the dialog is started by a trigger, initially, the active dialog state is "event_triggered"
# 3) Conditions block (list): This must be a valid operation block. See header_operations.py for reference.  
# 4) Dialog Text (string):
# 5) Ending dialog-state:
#    If a dialog line is picked, the active dialog-state will become the picked line's ending dialog-state.
# 6) Consequences block (list): This must be a valid operation block. See header_operations.py for reference.
# 7) Voice-over (string): sound filename for the voice over. Leave here empty for no voice over
####################################################################################################################

story_dialogs = [

[anyone|plyr, "start", [(eq, "$talk_context", 1)], "Where should I head?", "train_station_leaving", []],

[anyone|plyr, "train_station_leaving", [(eq, "$talk_context", 1)], "To my Hotel.", "close_window", [
	(assign, "$station_tutorial_stage", 5),
	(assign, "$station_kirei_follow", 0),
	(modify_visitors_at_site, "scn_fuyuki_11"),
	(reset_visitors),
	(set_visitors, 25, "trp_player", 1),
	(set_jump_entry, 25),
    (set_visitors, 26, "trp_master_staynight_07", 1),
	(set_visitors, 28, "trp_saber", 1),
	(set_jump_mission, "mt_storymode_introduction"),
	(jump_to_scene, "scn_fuyuki_11"),
	(mission_tpl_entry_set_override_flags, "mt_storymode_introduction", 25, 0x0000000f),
	(change_screen_mission),
	(assign, "$talk_context", 0),
]],

[anyone|plyr, "train_station_leaving", [(eq, "$talk_context", 1)], "To the Church.", "close_window", [
	(assign, "$station_tutorial_stage", 6),
	(assign, "$station_kirei_follow", 0),
	(modify_visitors_at_site, "scn_fuyuki_11"),
	(reset_visitors),
	(set_visitors, 0, "trp_player", 1),
	(set_jump_entry, 0),
    (set_visitors, 26, "trp_master_staynight_07", 1),
	(set_visitors, 27, "trp_saber", 1),
	(set_jump_mission, "mt_storymode_introduction"),
	(jump_to_scene, "scn_fuyuki_11"),
	(change_screen_mission),
	(assign, "$talk_context", 0),
]],

[trp_summoner, "start", [
	(troop_slot_eq, "$g_talk_troop", slot_troop_met_previously, 0),
	], "Good day to you, {young man/lassie}.", "summoner_intro_1",[]
	],
	
[trp_summoner, "start", [
	], "Good day to you, {playername}. Got any new summons for me?", "summoner_convo_1",[]
	],
	
[trp_summoner|plyr, "summoner_intro_1", [], 
	"Forgive me, rumor is that you can help me obtain a guaranteed servant.",
	"summoner_intro_2",
	[(troop_set_slot, "$g_talk_troop", slot_troop_met_previously, 1),]
	],
	
[trp_summoner, "summoner_intro_2", [
	], "You know, I think I can, assuming you have the right items.", "summoner_convo_1",[]
	],
	
   
[trp_summoner|plyr|repeat_for_100,"summoner_convo_1", [
	(store_repeat_object, ":catalysts"),
      (val_add,":catalysts", servant_catalysts_begin),
      (is_between,":catalysts", servant_catalysts_begin, servant_catalysts_end),
      (player_has_item, ":catalysts"),
      (str_store_item_name, s5, ":catalysts"),
	(player_has_item, ":catalysts"),
   ],
   "I have the {s5}.", "summon_servant",[
   (store_repeat_object, ":catalysts"),
	  (val_add, ":catalysts", servant_catalysts_begin),
      (str_store_item_name, s6, ":catalysts"),
	  (item_get_slot, ":summoned_servant", ":catalysts", slot_fate_catalyst_servant),
	  (troop_remove_item, "trp_player", ":catalysts"),
	  (party_add_members, "p_main_party", ":summoned_servant", 1),
	  (str_store_troop_name, s10, ":summoned_servant"),
	  (troop_set_slot, ":summoned_servant", slot_fate_master, "trp_player"),
   ]], 
   
[trp_summoner,"summon_servant", [],
   "You gave away a {s6} summoning the servant called {s10}", "close_window",[
   (display_message, "@{!}{s10} Joins the Party"),
   ]],    
   
[trp_summoner|plyr, "summoner_convo_1", [], 
	"I will be seeing you, dear friend.", "close_window",[]
	],
	
[anyone, "start", [
	# (store_current_scene, ":current_scene"),
	# (eq, ":current_scene", "scn_fate_storymode_train_station"),
	(eq, "$g_talk_troop", "trp_saber"),
	(eq, "$station_tutorial_stage", 5),
	(eq, "$walk_home", 0),
	#(eq, "$body_inspection_fail", -1),
	 ], 
	 ". . .", "servant_assault",
	 []
	 ],
	
[anyone, "start", [
	 (is_between, "$g_talk_troop", servants_begin, servants_end),
	 ], 
	 "Hey, Master {playername}, I understand the need to conversate but can't it wait? We do not know if an enemy servant might attack at any moment and I would prefer to not be caught off guard.", "close_window",
	 [(troop_set_slot, "$g_talk_troop", slot_troop_met_previously, 1),]
	 ],	

# member_chat is talking from the party window

[anyone, "member_chat", [
	  (is_between, "$g_talk_troop", servants_begin, servants_end),
	  (troop_slot_eq, "$g_talk_troop", slot_troop_met_previously, 1),
	  (str_clear, s12),
	  (str_clear, s13),
	  (str_store_troop_name, s13, "$g_talk_troop"),
	  (troop_get_slot, ":greet", "$g_talk_troop", slot_fate_greet),
	  (str_store_string, s12, ":greet"),
	 ], 
	 "{s12}", "servant_dialog",
	 []
	 ],
	 
[anyone, "member_chat", [
	 (is_between, "$g_talk_troop", servants_begin, servants_end),
	 (troop_slot_eq, "$g_talk_troop", slot_troop_met_previously, 0),
	 (str_clear, s12),
	 (str_clear, s13),
	 (str_store_troop_name, s13, "$g_talk_troop"),
	 (troop_get_slot, ":greet", "$g_talk_troop", slot_fate_initial_greet),
	 (str_store_string, s12, ":greet"),
	 ], 
	 "{s12}", "servant_intro",
	 [(troop_set_slot, "$g_talk_troop", slot_troop_met_previously, 1),]
	 ],

[anyone|plyr, "servant_intro", [
	 ], 
	 "Hello, {s13}. I am {playername}, lets do this vow bs.", "servant_dialog",
	 []
	 ],
	 
  
[anyone, "servant_dialog", [],
	 "This is generic servant dialog.", "servant_dialog2",
	 []
	],	
	
[anyone|plyr, "servant_dialog2", [
	(troop_slot_eq, "$g_talk_troop", slot_fate_true_name_revealed, 0),
	],
	 "Tell me your name, loser", "servant_name_reveal",
	 [
	 (str_clear, s12),
	 (str_clear, s13),
	 (str_clear, s14),
	 (troop_get_slot, ":truename", "$g_talk_troop", slot_fate_true_name),
	 (troop_get_slot, ":greet", "$g_talk_troop", slot_fate_reveal_greet),
	 (troop_get_slot, ":speech", "$g_talk_troop", slot_fate_summon_speech),
	 (troop_set_name, "$g_talk_troop", ":truename"),
	 (str_store_string, s12, ":greet"),
	 (str_store_troop_name, s13, "$g_talk_troop"),
	 (str_store_string, s14, ":speech"),]
	 ],
	 
	 
[anyone|plyr, "servant_dialog2", [],
	"I need to test inventories", "servant_dialog2",
	[(change_screen_equip_other),]
	],
	 
[anyone|plyr, "servant_dialog2", [],
	 "Tell me your slots", "servant_debug",
	 [
	 (str_clear, s10),
	 (str_clear, s11),
	 (str_clear, s12),
	 
	 (troop_get_slot, ":truename", "$g_talk_troop", slot_fate_true_name),
	 (troop_get_slot, ":alignment", "$g_talk_troop", slot_fate_alignment),
	 (troop_get_slot, ":class", "$g_talk_troop", slot_fate_servant_class),
	 (troop_get_slot, ":height", "$g_talk_troop", slot_fate_height),
	 (troop_get_slot, ":weight", "$g_talk_troop", slot_fate_weight),
	 (troop_get_slot, ":mana", "$g_talk_troop", slot_fate_max_mana),
	 
	 (troop_get_slot, ":truenamerevealed", "$g_talk_troop", slot_fate_true_name_revealed),
	 (troop_get_slot, ":infotier", "$g_talk_troop", slot_fate_info_tier),
	 (troop_get_slot, ":nobleused", "$g_talk_troop", slot_fate_noble_phantasm_used),
	 (troop_get_slot, ":classification", "$g_talk_troop", slot_fate_classification),
	 
	 (str_store_string, s10, ":truename"),
	 (str_store_string, s11, ":alignment"),
	 (str_store_string, s12, ":class"),
	 (str_store_string, s13, ":classification"),
	 (assign, reg10, ":height"),
	 (assign, reg11, ":weight"),
	 (assign, reg12, ":mana"),
	 (assign, reg13, ":truenamerevealed"),
	 (assign, reg14, ":infotier"),
	 (assign, reg15, ":nobleused"),
	 ]
	],
[anyone|plyr, "servant_dialog2", [],
	 "Tell me your servant skills", "servant_debug2",
	 [
	 	 
	 (troop_get_slot, ":skill1", "$g_talk_troop", fate_skill_slot_01),
	 (troop_get_slot, ":skill2", "$g_talk_troop", fate_skill_slot_02),
	 (troop_get_slot, ":skill3", "$g_talk_troop", fate_skill_slot_03),
	 (troop_get_slot, ":skill4", "$g_talk_troop", fate_skill_slot_04),
	 (troop_get_slot, ":skill5", "$g_talk_troop", fate_skill_slot_05),
	 (troop_get_slot, ":skill6", "$g_talk_troop", fate_skill_slot_06),
	 (troop_get_slot, ":skill7", "$g_talk_troop", fate_skill_slot_07),
	 (troop_get_slot, ":skill8", "$g_talk_troop", fate_skill_slot_08),
	 (troop_get_slot, ":skill1lvl", "$g_talk_troop", fate_skill_slot_01_level),
	 (troop_get_slot, ":skill2lvl", "$g_talk_troop", fate_skill_slot_02_level),
	 (troop_get_slot, ":skill3lvl", "$g_talk_troop", fate_skill_slot_03_level),
	 (troop_get_slot, ":skill4lvl", "$g_talk_troop", fate_skill_slot_04_level),
	 (troop_get_slot, ":skill5lvl", "$g_talk_troop", fate_skill_slot_05_level),
	 (troop_get_slot, ":skill6lvl", "$g_talk_troop", fate_skill_slot_06_level),
	 (troop_get_slot, ":skill7lvl", "$g_talk_troop", fate_skill_slot_07_level),
	 (troop_get_slot, ":skill8lvl", "$g_talk_troop", fate_skill_slot_08_level),
	 
	 (str_store_string, s10, ":skill1"),
	 (str_store_string, s11, ":skill2"),
	 (str_store_string, s12, ":skill3"),
	 (str_store_string, s13, ":skill4"),
	 (str_store_string, s14, ":skill5"),
	 (str_store_string, s15, ":skill6"),
	 (str_store_string, s16, ":skill7"),
	 (str_store_string, s17, ":skill8"),
	 
	 (assign, reg10, ":skill1lvl"),
	 (assign, reg11, ":skill2lvl"),
	 (assign, reg12, ":skill3lvl"),
	 (assign, reg13, ":skill4lvl"),
	 (assign, reg14, ":skill5lvl"),
	 (assign, reg15, ":skill6lvl"),
	 (assign, reg16, ":skill7lvl"),
	 (assign, reg17, ":skill8lvl"),

	 ]
	],
	
[anyone|plyr, "servant_dialog2", [],
	 "Tell me your servant parameters", "servant_debug3",
	 [
	 	 
	 (troop_get_slot, ":str", "$g_talk_troop", fate_param_str),
	 (troop_get_slot, ":end", "$g_talk_troop", fate_param_end),
	 (troop_get_slot, ":agi", "$g_talk_troop", fate_param_agi),
	 (troop_get_slot, ":mgc", "$g_talk_troop", fate_param_mgc),
	 (troop_get_slot, ":lck", "$g_talk_troop", fate_param_lck),
	 (troop_get_slot, ":npm", "$g_talk_troop", fate_param_npm),
	 
	 (assign, reg10, ":str"),
	 (assign, reg11, ":end"),
	 (assign, reg12, ":agi"),
	 (assign, reg13, ":mgc"),
	 (assign, reg14, ":lck"),
	 (assign, reg15, ":npm"),

	 ]
	],
	
[anyone|plyr, "servant_dialog2", [(troop_slot_eq, "$g_talk_troop", slot_fate_info_tier, 0),],
	 "I'm going to trick you to think you know me more", "servant_dialog5",
	 [
	 (troop_set_slot, "$g_talk_troop", slot_fate_info_tier, 1),
	 ]
	],
	
[anyone|plyr, "servant_dialog2", [(troop_slot_eq, "$g_talk_troop", slot_fate_info_tier, 1),],
	 "I'm going to trick you to think you know me even more", "servant_dialog5",
	 [
	 (troop_set_slot, "$g_talk_troop", slot_fate_info_tier, 2),
	 ]
	],
[anyone|plyr, "servant_dialog2", [(troop_slot_eq, "$g_talk_troop", slot_fate_info_tier, 2),],
	 "I'm going to trick you to think you know me even, even more", "servant_dialog5",
	 [
	 (troop_set_slot, "$g_talk_troop", slot_fate_info_tier, 3),
	 ]
	],
[anyone|plyr, "servant_dialog2", [(troop_slot_eq, "$g_talk_troop", slot_fate_info_tier, 3),],
	 "I'm going to trick you to think you know the most", "servant_dialog5",
	 [
	 (troop_set_slot, "$g_talk_troop", slot_fate_info_tier, 4),
	 ]
	],
[anyone|plyr, "servant_dialog2", [(troop_slot_eq, "$g_talk_troop", slot_fate_info_tier, 4),],
	 "I'm going to reset how much I know you", "servant_dialog5",
	 [
	 (troop_set_slot, "$g_talk_troop", slot_fate_info_tier, 0),
	 ]
	],
[anyone, "servant_dialog5", [],
	 "okay", "servant_dialog2",
	 []],
	 
  # [anyone|plyr,"servant_dialog2", [], #I Added this for debuging EMiya being a punk and not equipping shit

   # "Let me see your equipment.", "member_trade",[]],
	
[anyone|plyr, "servant_dialog2", [],
	 "BYE!", "close_window",
	 []
	],
[anyone, "servant_name_reveal", [],
	   "{s12} {s13}, {s14}.", "servant_dialog2",
	   [
	   (troop_set_slot, "$g_talk_troop", slot_fate_true_name_revealed, 1),
	   ]
	  ],
[anyone, "servant_debug", [],
	   "Hey, {playername}^ I am {s10}^ I am a {s13}^ I have {s11} alignment^ I am a {s12} class servant^ I am {reg10}cm tall^ I weigh {reg11}kg^ I have {reg12} units of mana^ I {reg13?have:have not} revealed my name^ I {reg15?have:have not} used my Noble Phantasm^ You are at the {reg14} info tier with me.", 
	   "servant_dialog2",
	   []
	  ],
[anyone, "servant_debug2", [],
	   "Hey, {playername}^ My Skills are as follows^ {s10} : Rank {reg10}^ {s11} : Rank {reg11}^ {s12} : Rank {reg12}^ {s13} : Rank {reg13}^ {s14} : Rank {reg14}^ {s15} : Rank {reg15}^ {s16} : Rank {reg16}^ {s17} : Rank {reg17}", 
	   "servant_dialog2",
	   []
	  ],
[anyone, "servant_debug3", [],
	   "Hey, {playername}^ My Parameters are as follows^ Strength: {reg10}^ Endurance: {reg11}^ Agility: {reg12}^ Magic: {reg13}^ Luck: {reg14}^ Noble Phantasm: {reg15}", 
	   "servant_dialog2",
	   []
	  ],
	  
[anyone|plyr|auto_proceed, "start", [
	  #(eq, "$g_talk_troop", "trp_master_staynight_07"),
	  (eq, "$station_tutorial_stage", 5),
	  (eq, "$walk_home", 0),
	  (eq, "$alleyway_noticed", 0),
	  (eq, "$station_kirei_follow", 0),
	 ], 
	 "[skip]", "alleyway",
	 []
	 ],
	  
[anyone|auto_proceed, "start", [
	  (store_current_scene, ":current_scene"),
	  (eq, ":current_scene", "scn_fate_storymode_train"),
	  (eq, "$g_talk_troop", "trp_master_staynight_07"),
	  (troop_slot_eq, "$g_talk_troop", slot_troop_met_previously, 0),
	  (eq, "$came_to", 0),
	 ], 
	 "[skip]", "train_dialog_pre_pre",
	 [
	 #(troop_set_slot, "$g_talk_troop", slot_troop_met_previously, 1),
	 (assign, "$came_to", 1),
	 ]
	 ],
	 
[anyone, "start", [
	  (store_current_scene, ":current_scene"),
	  (eq, ":current_scene", "scn_fate_storymode_train"),
	  (eq, "$g_talk_troop", "trp_master_staynight_07"),
	  (troop_slot_eq, "$g_talk_troop", slot_troop_met_previously, 0),
	  (eq, "$came_to", 1),
	 ], 
	 "It's awfully late for someone like you to be riding the train alone. My name is Kirei Kotomine, a priest at the Kotomine Holy Church in our destination, Fuyuki", "train_dialog_pre",
	 [
	 (troop_set_slot, "$g_talk_troop", slot_troop_met_previously, 1),
	 ]
	 ],
	 
[anyone|plyr, "train_dialog_pre_pre", [
	 ], 
	 "Ugh, my head really hurts. . .", "train_dialog_pre_pre_1",
	 []
	 ],
	 
[anyone|plyr, "train_dialog_pre_pre_1", [
	 ], 
	 "Wait, how did I end up on this train? ^Where am I headed.", "train_dialog_pre_pre_2",
	 []
	 ],
	 
[anyone|plyr, "train_dialog_pre_pre_2", [
	(str_store_troop_name, s10, "trp_player"),
	(eq, "$name_remembered", 0),
	 ], 
	 "Well, I do remember my name is {s10}", "train_dialog_pre_pre_2",
	 [(assign, "$name_remembered", 1),]
	 ],
	 
[anyone|plyr, "train_dialog_pre_pre_2", [
	(str_store_troop_name, s10, "trp_player"),
	(eq, "$name_remembered", 1),
	(eq, "$remembered_else", 0),
	 ], 
	 "Wait, I remember everything! [Skips Tutorializations]", "train_dialog_pre_pre_2",
	 [
	 (assign, "$remembered_else", 2),
	 (assign, "$tutorials", 0),
	 ]
	 ],
	 
[anyone|plyr, "train_dialog_pre_pre_2", [
	(str_store_troop_name, s10, "trp_player"),
	(eq, "$name_remembered", 1),
	(eq, "$remembered_else", 0),
	 ], 
	 "I genuinely can not remember anything else. [Recommended for 1st Time Players of Fate]", "train_dialog_pre_pre_2",
	 [
	 (assign, "$remembered_else", 1),
	 (assign, "$tutorials", 1),
	 (try_for_range, ":slot", slot_fate_chosen_spell, slot_fate_max_minions),
		(troop_set_slot, "trp_player", ":slot", 0),
	 (try_end),
	 ]
	 ],
	 
[anyone|plyr, "train_dialog_pre_pre_2", [
	(str_store_troop_name, s10, "trp_player"),
	(eq, "$name_remembered", 1),
	(gt, "$remembered_else", 0),
	 ], 
	 "Wait a second, has that man been watching me since I came to?", "train_dialog_pre_pre_3",
	 [
	 ]
	 ],
	 
[anyone, "train_dialog_pre_pre_3", [
	 ], 
	 ". . .", "train_dialog_pre_pre_4",
	 [
	 ]
	 ],
	 
[anyone|plyr, "train_dialog_pre_pre_4", [
	 ], 
	 "He's coming this way.", "close_window",
	 [
	 (try_for_agents, ":agent"),
		(agent_get_troop_id, ":troop", ":agent"),
		(eq, ":troop", "trp_master_staynight_07"),
		(entry_point_get_position, pos50, 50),
		# (agent_set_target_position, ":agent", pos50),
		(agent_set_scripted_destination, ":agent", pos50, 0),
	(try_end),
	(assign, "$priest_in_hallway", 1),
	 ]
	 ],
	 
[anyone, "train_dialog_pre", [
	(str_store_troop_name, s10, "trp_player"),
	(troop_get_slot, ":job", "trp_player", fate_plyr_background),
	(try_begin),
		(this_or_next|eq, ":job", "str_player_bg_church_combat"),
		(eq, ":job", "str_player_bg_church_leader"),
		(str_store_string, s11, "@Though, a fellow member of the Holy Church shouldn't be surprised to I hear that I knew they were coming"),
		(str_store_string, s12, "@I understand the Leadership in the Holy Church felt it necessary to enter a new piece on my game board. One that can be more direct in its manipulations, I just wish they had consulted me first"),
	(else_try),
		(eq, ":job", "str_player_bg_magus"),
		(str_store_string, s11, "@I am almost offended the Clocktower chose to send another one of their dogs into this fray. Normally we would only allow one of their playthings to enter the War. This time, however, I would like to see where this goes."),
		(str_store_string, s12, "@I promise you will find me a hospitable host, despite your employer. May we begin our discussion? This is a long train ride."),
	(else_try),
		(eq, ":job", "str_player_bg_assassin"),
		(str_store_string, s11, "@A paid killer is a rare sight on this train. I wonder if you aren't here to see me. My, I have a lot of enemies, you see"),
		(str_store_string, s12, "@I find it incredibly interesting that the Clocktower hired someone like you to fight in the war, though, men of your background seem to perform well in such contests."),
	(else_try),
		(str_store_string, s11, "@Awakening on a strange train with a strange man must be a jarring experience. I don't mean to frighten you."),
		(str_store_string, s12, "@In the coming weeks I hope you begin to see me as a mentor of sorts. Someone you may come to for advice."),
	(try_end),
	 ], 
	 "{s11}", "train_dialog_pre2",
	 []
	 ],
	 
[anyone, "train_dialog_pre2", [], 
	 "I suppose I should be more forward with you, {s10}, I've been long expecting your arrival. I'm the overseer in the upcoming Holy Grail War. It's my job to stay neutral and to ensure the rules are followed and the participants do not involve the people of my city too much.", "train_dialog_pre3",
	 []
	 ],
	 
[anyone, "train_dialog_pre3", [
	(str_store_troop_name, s10, "trp_player"),
	(troop_get_slot, ":job", "trp_player", fate_plyr_background),
	(try_begin),
		(this_or_next|eq, ":job", "str_player_bg_church_combat"),
		(eq, ":job", "str_player_bg_church_combat"),
		(str_store_string, s11, "@Though, I suppose a fellow member of the Holy Church shouldn't be surprised to I hear that I knew they were coming"),
		(str_store_string, s12, "@It has been so long since I spoke to another member of the Holy Church. Would you be so generous as to join me in a short conversation?"),
	(else_try),
		(eq, ":job", "str_player_bg_magus"),
		(str_store_string, s11, "@I am almost offended the Clocktower chose to send another one of their dogs into this fray. Normally we would only allow one of their playthings to enter the War. Don't mind me"),
		(str_store_string, s12, "@I promise you will find me a hospitable host, despite your employer. May we begin our discussion? This is a long train ride."),
	(else_try),
		(eq, ":job", "str_player_bg_assassin"),
		(str_store_string, s11, "@A paid killer is a rare sight on this train. I wonder if you aren't here to see me. My, I have a lot of enemies, you see"),
		(str_store_string, s12, "@From one cold-hearted Killer of Mages to another, would you care to make small talk while we wait for this train ride to end?"),
	(else_try),
		(str_store_string, s11, "@I suppose you might wonder how I knew to expect you"),
		(str_store_string, s12, "@In the coming weeks I hope you begin to see me as a mentor of sorts. Someone you may come to for advice. In fact, did you have any questions now?"),
	(try_end),
	 ], 
	 "{s12}", "train_dialog",
	 []
	 ],
	 
[anyone|plyr, "train_dialog", [
	 ], 
	 "I do have a few questions.", "kirei_dialog",
	 []
	 ],

[anyone|plyr, "train_dialog", [
	 ], 
	 "I'm sorry sir, I just wish to be left alone.", "train_dialog_close",
	 []
	 ],
	 
[anyone, "start", [
	(store_current_scene, ":current_scene"),
	(eq, ":current_scene", "scn_fate_storymode_train_station"),
	(eq, "$g_talk_troop", "trp_master_staynight_07"),
	(eq, "$station_tutorial_stage", 0),
	(str_store_troop_name, s10, "trp_player"),
	
	(str_clear, s11),
	(try_begin),
		(this_or_next|troop_slot_eq, "trp_player", fate_plyr_background, "str_player_bg_church_combat"),
		(troop_slot_eq, "trp_player", fate_plyr_background, "str_player_bg_church_leader"),
		(str_store_string, s11, "@I wonder if the officer would allow us to perform last rites."),
	(else_try),
		(troop_slot_eq, "trp_player", fate_plyr_background, "str_player_bg_detective"),
		(str_store_string, s11, "@You're an Investigator of sorts, right? Maybe you can help crack the case."),
	(else_try),
		(str_store_string, s11, "@Seems like terrible luck has befallen you today. Almost seems irresponsible to not go over and take a peek."),
	(try_end),
	
	 ], 
	 "{s10}, well would you look at that miserable scene. Seems as if another murder has occurred in little Fuyuki. ^^Such events are unfortunately becoming increasingly more common in my poor little city. ^^{s11}", "kirei_station_tutorial",
	 []
	 ],
	 
[anyone|auto_proceed, "start", [
	(store_current_scene, ":current_scene"),
	(eq, ":current_scene", "scn_fate_storymode_train_station"),
	(eq, "$g_talk_troop", "trp_master_staynight_07"),
	(eq, "$station_tutorial_stage", 3),
	(eq, "$station_kirei_follow", 1),
	(eq, "$body_inspection_fail", -1),
	 ], 
	 "[skip]", "kirei_station_body_inspection",
	 []
	 ],
	 
[anyone|plyr|auto_proceed, "start", [
	(store_current_scene, ":current_scene"),
	(eq, ":current_scene", "scn_fate_storymode_train_station"),
	(eq, "$station_tutorial_stage", 3),
	(eq, "$station_kirei_follow", 0),
	(eq, "$body_inspection_fail", -1),
	 ], 
	 "[skip]", "self_station_body_inspection",
	 []
	 ],
	 
[anyone|auto_proceed, "start", [
	(store_current_scene, ":current_scene"),
	(eq, ":current_scene", "scn_fate_storymode_train_station"),
	(eq, "$g_talk_troop", "trp_master_staynight_07"),
	 ], 
	 "[skip]", "kirei_station_tutorial_2",
	 []
	 ],

[anyone, "start", [
	 # (store_current_scene, ":current_scene"),
	  (eq, "$g_talk_troop", "trp_master_staynight_07"),
	  (neq, "$station_tutorial_stage", 5),
	 # (eq, ":current_scene", "scn_fate_storymode_train_station"),
	 ], 
	 "Hello, anything troubling you?", "kirei_dialog",
	 []
	 ],
	 
[anyone, "kirei_dialog", [
	 ], 
	 "Go ahead with your questions", "kirei_dialog",
	 []
	 ],

[anyone|plyr, "kirei_dialog", [
	(eq, "$dialog_tutorial_memory", 0),
	(eq, "$remembered_else", 1),
	 ], 
	 "How did I end up on that train?", "kirei_tutorial_dialog_memory",
	 [(assign, "$dialog_tutorial_memory", 1),]
	 ],

[anyone|plyr, "kirei_dialog", [
	(eq, "$dialog_tutorial_grail", 0),
	(eq, "$tutorials", 1),
	 ], 
	 "What is a Holy Grail War?", "kirei_tutorial_dialog_grail",
	 [(assign, "$dialog_tutorial_grail", 1),]
	 ],
	 
[anyone|plyr, "kirei_dialog", [
	(eq, "$dialog_tutorial_travel", 0),
	(eq, "$tutorials", 1),
	 ], 
	 "How do I get around Fuyuki?", "kirei_tutorial_dialog_travel",
	 [
	 (assign, "$dialog_tutorial_travel", 1),
	 ]
	 ],
	 
[anyone|plyr, "kirei_dialog", [
	(eq, "$dialog_tutorial_activities", 0),
	(eq, "$tutorials", 1),
	 ], 
	 "Outside of the War, what can I do while I am in Fuyuki?", "kirei_tutorial_dialog_activities",
	 [
	 (assign, "$dialog_tutorial_activities", 1),
	 ]
	 ],
	 
[anyone|plyr, "kirei_dialog", [
	(eq, "$dialog_tutorial_next", 0),
	(eq, "$tutorials", 1),
	 ], 
	 "What should my next step be?", "kirei_tutorial_dialog_next",
	 [
	 (assign, "$dialog_tutorial_next", 1),
	 ]
	 ],

[anyone|plyr, "kirei_dialog", [
	(eq, "$dialog_tutorial_advice", 0),
	(eq, "$tutorials", 1),
	 ], 
	 "What advice do you have for me?", "kirei_tutorial_dialog_advice",
	 [
	 (assign, "$dialog_tutorial_advice", 1),
	 ]
	 ],
	 
[anyone|plyr, "kirei_dialog", [
	(eq, "$dialog_tutorial_forfeit", 0),
	(eq, "$tutorials", 1),
	 ], 
	 "What if I decide I don't want to be in this contest?", "kirei_tutorial_dialog_forfeit",
	 [
	 (assign, "$dialog_tutorial_forfeit", 1),
	 ]
	 ],
	 
[anyone|plyr, "kirei_dialog", [
	(eq, "$dialog_tutorial_notut", 0),
	(eq, "$tutorials", 0),
	 ], 
	 "I thought I could remember everything, but, would you feel comfortable going over it with me. Just in case.", "kirei_tutorial_dialog_notut",
	 [
	 (assign, "$dialog_tutorial_notut", 1),
	 ]
	 ],
	 
[anyone, "kirei_tutorial_dialog_notut", [
	 ], 
	 "Yes, of course. Is that what you would like?", "kirei_tutorial_dialog_notut_2",
	 [
	 ]
	 ],
	 
[anyone|plyr, "kirei_tutorial_dialog_notut_2", [
	 ], 
	 "Wait, I changed my mind. I don't need my hand held through this. [Tutorials Off]", "kirei_dialog",
	 []
	 ],
	 
[anyone|plyr, "kirei_tutorial_dialog_notut_2", [
	 ], 
	 "Yes, of course. Thank you for your paitence. [Tutorials On]", "kirei_dialog",
	 [
	 (assign, "$tutorials", 1),
	 (assign, "$remembered_else", 1),
	 ]
	 ],

[anyone, "kirei_tutorial_dialog_memory", [
	 ], 
	 "Well, to be quite honest. I have no idea. But, based on your apparent memory-loss, I would hazard a guess that you ran into an opposing Master who either used Alchemy or Memory Manipulation magecraft in order to take you out of the War before you even got an opportunity to find your footing. ^^I was told by the Mage's Association that they you had landed in a nearby airport and you were taking the commuter train to Fuyuki and that I would need to retreive you. I had only gotten on the train at the previous station.", "kirei_dialog",
	 []
	 ],
 
[anyone, "kirei_tutorial_dialog_grail", [
	 ], 
	 "Essentially a way to grant the Wish from the most deserving magus", "kirei_dialog",
	 []
	 ],
	 
[anyone, "kirei_tutorial_dialog_magic", [
	 ], 
	 "I can show you, if you'd like to step into the courtyard.", "kirei_tutorial_dialog_magic_1",
	 []
	 ],

[anyone|plyr, "kirei_tutorial_dialog_magic_1", [
	 ], 
	 "No sir, I'm fine without a lesson today.", "kirei_dialog",
	 []
	 ],
	 
[anyone|plyr, "kirei_tutorial_dialog_magic_1", [
	 ], 
	 "Sure, lead the way.", "kirei_dialog",
	 []
	 ],
	 
[anyone|plyr, "kirei_tutorial_dialog_magic_1", [
	 ], 
	 "Can you just tell me without having to show me?", "kirei_dialog",
	 []
	 ],
	 
[anyone|plyr, "kirei_dialog", [
	 ], 
	 "I think those are all the questions I had.", "train_dialog_close",
	 []
	 ],
	 
[anyone, "kirei_tutorial_dialog_travel", [
	 ], 
	 "Outside of walking, you can find bus stops along major roads, or use a taxi cab for some yen.", "kirei_dialog",
	 []
	 ],
	 
[anyone, "kirei_tutorial_dialog_activities", [
	 ], 
	 "You can find a shopping opportunities, fishing at the docks, collectable figures, books, back alley vendors, and potential freelancers inside cafes and bars", "kirei_dialog",
	 []
	 ],
	 
[anyone, "kirei_tutorial_dialog_next", [
	 ], 
	 "Prepare for the war by setting up your Workshop, gathering resources, shore up your defenses and be prepared,", "kirei_dialog",
	 []
	 ],

[anyone, "kirei_tutorial_dialog_advice", [
	 ], 
	 "As per the rules, no one is permitted to fight where civilians may see you, so be careful at night when the streets are empty. If you do find yourself face-to-face with an antagonistic Master during the day, make sure you kill all witnesses, or else you may find yourself chased after Holy Church Executors, Clock Tower Seal Designators, or Hired Killers.", "kirei_dialog",
	 []
	 ],

[anyone, "kirei_tutorial_dialog_forfeit", [
	 ], 
	 "As per the rules, you may forfeit your Command Seals to me and reside in Kotomine Church for the duration of the War. The Church is established as Neutral Ground and as such, no Servants are permitted to join us inside and no Master can openly attack another.", "kirei_tutorial_dialog_forfeit_1",
	 []
	 ],
	 
[anyone, "kirei_tutorial_dialog_forfeit_1", [
	 ], 
	 "Outside of these grounds, however, I cannot ensure your safety. Should the Grail seek a new Master, as a former Candidate, you would be sure to be selected again.", "kirei_tutorial_dialog_forfeit_2",
	 []
	 ],
	 
[anyone, "kirei_tutorial_dialog_forfeit_2", [
	 ], 
	 "As such, any wise master would kill you even if you were to willingly leave the fray. Your best bet would be to stay in the War if you wish to live a life larger than these grounds.", "kirei_dialog",
	 []
	 ],
	 
[anyone, "train_dialog_close", [
	 ], 
	 "I believe the train is pulling to its destination as is.", "close_window",
	 [
	 (call_script, "script_fate_fuyuki_town_scripts", "scn_fate_storymode_train_station"),
	 ]
	 ],
	 
[anyone, "kirei_station_tutorial", [
	#(str_store_troop_name, s10, "trp_player"),
	 ], 
	 "Before we part ways, I would like for you to accompany me to my humble Church. I think I can enlighten you on any information you may be missing.", "kirei_station_tutorial_2",
	 []
	 ],
	 
[anyone, "kirei_station_tutorial_2", [
	(str_store_troop_name, s10, "trp_player"),
	 ], 
	 "Well, {s10}, what is it? Follow me? Or tempt Fate by going straight to your Hotel?", "kirei_station_tutorial_3",
	 []
	 ],
	 
[anyone|plyr, "kirei_station_tutorial_3", [
	(eq, "$station_magecraft_tutorial", 1),
	(eq, "$tutorials", 1),
	(eq, "$station_kirei_follow", 1),
], 
	 "I don't remember how to cast spells. . . ", "kirei_station_tutorial_magecraft",
	 [(assign, "$station_magecraft_tutorial", 2),]
	 ],
	 
[anyone|plyr, "kirei_station_tutorial_3", [
	(lt, "$station_tutorial_stage", 4),
	(neq, "$body_inspection_fail", 999),
], 
	 "Let me think about it, I want to walk around the Station for now. Stick with me for a bit.", "close_window",
	 [(assign, "$station_tutorial_stage", 1),
	 (assign, "$station_kirei_follow", 1),]
	 ],
	 
[anyone|plyr, "kirei_station_tutorial_3", [
	(lt, "$station_tutorial_stage", 4),
	(neq, "$body_inspection_fail", 999),
], 
	 "You can go on ahead, I want to hang around the station for a while. I'll find my own way.", "close_window",
	 [
	 (assign, "$station_tutorial_stage", 1),
	 (assign, "$station_kirei_follow", 0),
	 (store_conversation_agent, ":agent"),
	 (entry_point_get_position, pos10, 60),
	 (agent_set_scripted_destination, ":agent", pos10),
	 ]
	 ],
	 
[anyone|plyr, "kirei_station_tutorial_3", [], 
	 "I'm beat from all this traveling, I'm going straight to my Hotel. I'll stop by your Church tomorrow.", "close_window",
	 [(assign, "$station_tutorial_stage", 5),
	  (assign, "$station_kirei_follow", 0),
	 
	  (modify_visitors_at_site, "scn_fuyuki_11"),
	  (reset_visitors),
	  #(mission_tpl_entry_set_override_flags, "mt_town_default", 0, af_override_weapons),
	  (set_visitors, 25, "trp_player", 1),
	  (set_jump_entry, 25),
      (set_visitors, 26, "trp_master_staynight_07", 1),
	  (set_visitors, 28, "trp_saber", 1),
	  (set_jump_mission, "mt_storymode_introduction"),
	  (jump_to_scene, "scn_fuyuki_11"),
	  (mission_tpl_entry_set_override_flags, "mt_storymode_introduction", 25, 0x0000000f),
	  (change_screen_mission),
	 ]
	 ],
	 
[anyone|plyr, "kirei_station_tutorial_3", [], 
	 "Let's go ahead and leave the station, I'll follow you to your Church.", "close_window",
	 [
	 (assign, "$station_tutorial_stage", 6),
	 (assign, "$station_kirei_follow", 1),
	 
	 (modify_visitors_at_site, "scn_fuyuki_11"),
	 (reset_visitors),
	  #(mission_tpl_entry_set_override_flags, "mt_town_default", 0, af_override_weapons),
	  #(mission_tpl_entry_set_override_flags, "mt_town_default", 1, af_override_weapons),
	  (set_visitors, 0, "trp_player", 1),
	  (set_jump_entry, 0),
      (set_visitors, 26, "trp_master_staynight_07", 1),
	  (set_visitors, 27, "trp_saber", 1),
	  (set_jump_mission, "mt_storymode_introduction"),
	  (jump_to_scene, "scn_fuyuki_11"),
	  (change_screen_mission),
	 ]
	 ],
	 
[anyone, "kirei_station_tutorial_magecraft", [], 
	 "Oh! How interesting. A master-candidate who can't use magecraft? It's fairly simple use [M] to open the magecraft window, cursor over the spell you want to use, then click [Middle-Mouse] to fire. ^^I'll teach you a beginner spell so you can distract him.", "kirei_station_tutorial_magecraft_2",
	 [(assign, "$station_magecraft_tutorial", 3),
	  (troop_set_slot, "trp_player", slot_fate_spell_slot_1, "str_magecraft_fire_bolt"),
	 ]
	 ],
	 
[anyone, "kirei_station_tutorial_magecraft_2", [], 
	 "That spell is easy to aim. ^^When the Policeman faces the body, hit the trashcan with it. The can go up in flames after a few seconds.", "close_window",
	 []
	 ],

[anyone, "start", [
	(store_current_scene, ":current_scene"),
	(eq, ":current_scene", "scn_fate_storymode_train_station"),
	(eq, "$g_talk_troop", "trp_fuyuki_policeman"),
	(eq, "$station_distaction_stage", 2),
	
	(get_player_agent_no, ":player_agent"),
    (agent_get_position, pos0, ":player_agent"),
	(store_conversation_agent, ":agent"),
	(agent_get_position, pos10, ":agent"),
		
	(get_distance_between_positions, ":dist", pos0, pos10),
	(le, ":dist", 200),
	 ], 
	 "What in the hell! ^^Fire!", "close_window",
	 [(assign, "$body_inspection_fail", -1),
	 
	 (store_conversation_agent, ":agent_no"),
	 (entry_point_get_position, pos51, 51),
	 (agent_set_look_target_position, ":agent_no", pos51),
	 (assign, "$station_tutorial_stage", 3),
	 # (val_add, "$station_tutorial_stage", 2),
	 ]
	 ],

[anyone, "start", [
	(store_current_scene, ":current_scene"),
	(eq, ":current_scene", "scn_fate_storymode_train_station"),
	(eq, "$g_talk_troop", "trp_fuyuki_policeman"),
	(troop_slot_eq, "$g_talk_troop", slot_troop_met_previously, 0),
	 ], 
	 "Please don't come any further, this is an active crime scene.", "detective_chat",
	 [(troop_set_slot, "$g_talk_troop", slot_troop_met_previously, 1),]
	 ],
	 
[anyone, "start", [
	(store_current_scene, ":current_scene"),
	(eq, ":current_scene", "scn_fate_storymode_train_station"),
	(eq, "$g_talk_troop", "trp_fuyuki_policeman"),
	(eq, "$station_tutorial_stage", 1),
	(eq, "$station_kirei_follow", 1),
	(neq, "$body_inspection_fail", -1),
	 ], 
	 "Look, kid, I've already got my hands full. Plus you and Father Kotomine obviously got better things to do, right? Go about your day, the Medical Examiner should be here soon to haul him away.", "detective_chat",
	 []
	 ],
	 
[anyone, "start", [
	(store_current_scene, ":current_scene"),
	(eq, ":current_scene", "scn_fate_storymode_train_station"),
	(eq, "$g_talk_troop", "trp_fuyuki_policeman"),
	(neq, "$body_inspection_fail", -1),
	 ], 
	 "Look, kid, I've already got my hands full. You just got to the station, so you're obviously on your way somewhere, right? Go about your day, the Medical Examiner should be here soon to haul him away.", "detective_thoughts",
	 []
	 ],
	 
[anyone, "start", [
	(store_current_scene, ":current_scene"),
	(eq, ":current_scene", "scn_fate_storymode_train_station"),
	(eq, "$g_talk_troop", "trp_fuyuki_policeman"),
	(eq, "$body_inspection_fail", -1),
	 ], 
	 "Please leave me alone, I'm just trying to have a smoke and calm my nerves.^^This town is going to shit. I think it's time to leave the Force.", "close_window",
	 []
	 ],
	 
[anyone|plyr, "detective_thoughts", [
	(eq, "$body_inspection_fail", 0),
], 
	"This Policeman looks tired and weary. He's clearly having a long night.", "detective_chat", []],
	
[anyone|plyr, "detective_thoughts", [
], 
	"At least my poking around can't be making his night any worse, right?", "detective_chat", []],
	
[anyone|plyr, "detective_chat", [
	(ge, "$body_inspection_fail", 1),
	 ], 
	 "[Try to distract the officer]", "detective_chat_distract",
	 [
	 (val_add, "$body_inspection_fail", 1),
	 (assign, reg10, "$body_inspection_fail"),
	 (display_message, "@You have failed {reg10} times."),
	 ]
	 ],

[anyone|other(trp_master_staynight_07), "detective_chat", [
	(ge, "$body_inspection_fail", 2),
	(eq, "$station_tutorial_stage", 1),
	(eq, "$station_kirei_follow", 1),
],
	"Maybe try making use of some basic Magecraft to distract him?", "close_window", [
	(assign, "$station_magecraft_tutorial", 1),
	]],
	
[anyone|plyr, "detective_chat", [
	(ge, "$body_inspection_fail", 2),
	(eq, "$station_tutorial_stage", 1),
	(eq, "$station_kirei_follow", 0),
],
	"Maybe I could use Magecraft distract him?", "close_window", [
	]],

[anyone|plyr, "detective_chat", [
	(eq, "$body_inspection_fail", 0),
	 ], 
	 "Officer, what has happened here?", "detective_chat_2",
	 []
	 ],
	 
[anyone|plyr, "detective_chat_distract", [
	(str_store_troop_name, s1, "trp_player"),
	
	(store_random_in_range, ":distraction", 0, 1000),
	(val_mod, ":distraction", 10),
	
	(try_begin),
		(eq, ":distraction", 1),
		(str_store_string, s10, "@Look over there! A Unicorn!"),
	(else_try),
		(eq, ":distraction", 2),
		(str_store_string, s10, "@Oh, I just got a text from your mom. She said you need to go home right away."),
	(else_try),
		(eq, ":distraction", 3),
		(str_store_string, s10, "@Bro, for real, no lie, like. . . no joke. On god, bro? Like keeping it 100? There's someone jaywalking outside."),
	(else_try),
		(eq, ":distraction", 4),
		(str_store_string, s10, "@Officer! Go! Quick! There's a band of old timey bank robbers behind the vending machines, and they've got brown sacks with yen signs on them!"),
	(else_try),
		(eq, ":distraction", 5),
		(str_store_string, s10, "@I heard there's a puff-puff lady in that bathroom. Only 100 yen to help you relax to face the Demon King!"),
	(else_try),
		(eq, ":distraction", 6),
		(str_store_string, s10, "@Oh. . . No! ^The body is getting up! Run! ^^I'll hold the shambling undead back so you can escape! Tell everyone the selfless exploits of the Great Hero {s1}!"),
	(else_try),
		(eq, ":distraction", 7),
		(str_store_string, s10, "@Sir! There's a sponge, a starfish, a squirrel and a crab all trying to escape the fishtank around the corner!"),
	(else_try),
		(eq, ":distraction", 8),
		(str_store_string, s10, "@People die when they are killed!"),
	(else_try),
		(eq, ":distraction", 9),
		(str_store_string, s10, "@[you stare at the man, saying nothing]"),
	(else_try),
		(str_store_string, s10, "@I heard that there's a polygonal monkey head in every scene in this mod!"),
	(try_end),
	 ], 
	 "{s10}", "detective_chat_fail",
	 []
	 ],
	 
[anyone, "detective_chat_fail", [
	 ], 
	 "What are you talking about?! Do you think this is funny? ^^There is a dead man, mere feet away from you, show some respect.", "detective_chat",
	 []
	 ],
	 
[anyone|plyr, "detective_chat", [
	#(troop_get_slot, ":job", "trp_player", fate_plyr_background),
	(troop_slot_eq, "trp_player", fate_plyr_background, "str_player_bg_detective"),
	 ], 
	 "Hello, I'm an Interpol detective, may I take a look at the victim?", "detective_chat_officer",
	 []
	 ],
	 
[anyone|plyr, "detective_chat", [
	#(troop_get_slot, ":job", "trp_player", fate_plyr_background),
	(this_or_next|troop_slot_eq, "trp_player", fate_plyr_background, "str_player_bg_church_combat"),
	(troop_slot_eq, "trp_player", fate_plyr_background, "str_player_bg_church_leader"),
	(eq, "$station_tutorial_stage", 1),
	(eq, "$station_kirei_follow", 1),
	 ], 
	 "Officer, I am sorry to trouble you, but we are members of the Clergy, this man was in our Parish, may we perform his final rites?", "detective_chat_church",
	 []
	 ],
	 
[anyone|plyr, "detective_chat", [
	(this_or_next|troop_slot_eq, "trp_player", fate_plyr_background, "str_player_bg_church_combat"),
	(troop_slot_eq, "trp_player", fate_plyr_background, "str_player_bg_church_leader"),
	(eq, "$station_tutorial_stage", 1),
	(eq, "$station_kirei_follow", 0),
	 ], 
	 "Officer, I am sorry to trouble you, but I am of the Clergy. May I perform his final rites?", "detective_chat_church_fail",
	 []
	 ],
	 
[anyone|plyr, "detective_chat_3", [], 
	 "If I want to get closer, I need to convince him to let me examine the body or distract him long enough that I can inspect it up close.", "detective_chat",
	 []
	 ],
	 
[anyone|plyr, "detective_chat", [], 
	 "Sorry to trouble you Officer. I hope the rest of your night improves", "close_window",
	 []
	 ],

[anyone, "detective_chat_2", [], 
	 "Sorry to tell you, kid, but this is just some regular old motorcycle gang street violence. This delinquent was the leader of a local group of Yanki toughs who must've been caught in someone else turf. Nothing more to see here.", "detective_chat_3",
	 [(val_add, "$body_inspection_fail", 1),]
	 ],
	 
	 
[anyone, "detective_chat_officer", [], 
	 "What would this have to do with Interpol? Besides, we both know the brass would have my balls in a vice if I let every well-dressed Tom, Dick and Harry with a badge saunter all over my crime scene.", "detective_chat_officer_2",
	 []
	 ],

[anyone, "detective_chat_officer_2", [], 
	 "But honestly, I cannot make heads or tails of this.", "detective_chat_officer_3",
	 []
	 ],

[anyone, "detective_chat_officer_3", [], 
	 "I've seen gangs tear one another apart. Legs broken so bad you couldn't tell which bend in it was the knee, eyes ruptured by barbed wire, motherfuckers who had blow torches taken to their face. ^Real nasty shit. ^But I have never seen something like this. Ever.", "detective_chat_officer_4",
	 []
	 ],
	 
[anyone, "detective_chat_officer_4", [], 
	 "Whatever gang did this shit to this kid oughta be brought out to the docks and given a one-way ticket to the seabed if you catch my meaning. ^So, I'll give you ten minutes with the Stiff. Any longer than that, the clean up crew'll be here and if they see you, my badge'll be on the line.", "detective_chat_officer_5",
	 []
	 ],
	 
[anyone, "detective_chat_officer_5", [], 
	 "I can't look at that shit for even a second longer. I'm going to duck out for a smoke and to seriously question what I did to deserve this beat tonight.", "close_window",
	 [
	 (store_conversation_agent, ":agent"),
	 (entry_point_get_position, pos50, 50),
	 (agent_set_scripted_destination, ":agent", pos50, 0),
	 (agent_set_speed_limit, ":agent", 1),
	 (agent_set_speed_modifier, ":agent", 50),
	 (assign, "$body_inspection_fail", -1),
	 (assign, "$station_tutorial_stage", 3),
	 
	 (agent_equip_item, ":agent", "itm_cigarette"),
	 (agent_set_wielded_item, ":agent", "itm_cigarette"),
	 ]
	 ],
	 
[anyone, "detective_chat_church", [], 
	 "Parish? You want me to believe this kid was a member of some Catholic Church? Get real. . .", "detective_chat_church_1",
	 []
	 ],
	 
[anyone, "detective_chat_church_fail", [], 
	 "Get real. . . ^If this guy is Catholic, then I'm from Atlantis. Gotta try harder than that, kid.", "detective_chat",
	 []
	 ],

[anyone|plyr, "detective_chat_church_1", [], 
	 "He seems intent on blowing you off, but, after a glance to Kirei, he suddenly seems like he believes you.", "detective_chat_church_2",
	 []
	 ],

[anyone|other(trp_master_staynight_07), "detective_chat_church_2", [], 
	 "Please, allow us some time with the departed.", "detective_chat_church_3",
	 [
	 (assign, "$station_tutorial_stage", 3),
	 (assign, "$body_inspection_fail", -1),
	 ]
	 ],
	 
[anyone, "detective_chat_church_3", [], 
	 "I'm going to duck out for a smoke. You have ten minutes. ^Perform your rites and please, get out of here before the Medical Examiner arrives.", "close_window",
	 [
	 (store_conversation_agent, ":agent"),
	 (entry_point_get_position, pos50, 50),
	 (agent_set_scripted_destination, ":agent", pos50, 0),
	 (agent_set_speed_limit, ":agent", 1),
	 (agent_set_speed_modifier, ":agent", 50),
	 
	 (agent_equip_item, ":agent", "itm_cigarette"),
	 (agent_set_wielded_item, ":agent", "itm_cigarette"),
	 ]
	 ],
	 
[anyone|plyr, "kirei_station_body_inspection", [
	 ], 
	 "You crouch by the body and pull back the shroud. ^You hardly recognize this mass as a man any longer, the vicera and gore look more like a display from a supermarket meat counter. ^The wound is tremendous, the amount of force required to do such a thing boggles the mind. ^'There's no way this was from a human.'", "kirei_station_body_inspection_1",
	 []
	 ],
	 
[anyone, "kirei_station_body_inspection_1", [
	(str_store_troop_name, s10 ,"trp_player"),
	 ], 
	 "Well, {s10}, what else could have done it?", "kirei_station_body_inspection_2",
	 []
	 ],
	 
[anyone|plyr, "kirei_station_body_inspection_2", [
	 ], 
	 "Some sort of Ghoul or Vampire.", "kirei_station_body_inspection_vampire",
	 []
	 ],
	 
[anyone|plyr, "kirei_station_body_inspection_2", [
	 ], 
	 "A magus! The thug must've harrassed him, and since there were no witnesses, they must've popped off a particularly violent curse.", "kirei_station_body_inspection_mage",
	 []
	 ],
	 
[anyone|plyr, "kirei_station_body_inspection_2", [
	 ], 
	 "Y'know, I take that back. A group of 12 or so with pipes and chains could have easily commited this level of violence.", "kirei_station_body_inspection_rival",
	 []
	 ],
[anyone|plyr, "kirei_station_body_inspection_2", [
	 ], 
	 "Whatever it is, it's gone now.", "kirei_station_body_inspection_forfeit",
	 []
	 ],
	 
[anyone, "kirei_station_body_inspection_forfeit", [
	 ], 
	 "I suspect you're right. Hopefully this means we are safe. Speaking of. . .", "kirei_station_tutorial_2",
	 [(assign, "$body_inspection_fail", 999),]
	 ],

[anyone, "kirei_station_body_inspection_vampire", [
	 ], 
	 "While either might have the strength, no way a Vampire would have left behind such a mess and this much blood, and a Ghoul would have consumed all but this man's sneakers. ^^I cannot fathom why you would have assumed that.", "kirei_station_body_inspection_vampire_2",
	 []
	 ],

[anyone|plyr, "kirei_station_body_inspection_vampire_2", [
	 ], 
	 "You turn back to the body to examine it further.", "kirei_station_body_inspection_2",
	 []
	 ],
	 
[anyone, "kirei_station_body_inspection_mage", [
	 ], 
	 "Magi are disgusting, self-serving, blasphemous creatures, but I think this is beyond them. ^^Magi are also extremely secretive, I feel that one powerful enough to execute an annoying pion like this would have at least done me the professional curtoesy of covering the attack up instead of requiring me to do so. ^^Then again, a Mage should never be trusted to do anything but act in their own self-interest.", "kirei_station_body_inspection_mage_2",
	 []
	 ],
	 
[anyone|plyr, "kirei_station_body_inspection_mage_2", [], 
	 "After his tirade, you catch him looking at you in disgust. As a Master-candidate, you know how he feels about you. You can't help but wonder what sort of Mage hurt this man so deeply.", "kirei_station_body_inspection_2",
	 []
	 ],
	 
[anyone, "kirei_station_body_inspection_rival", [
	 ], 
	 "Mundanity often is the best choice.^ But in this case, I think we can rule out a motorbike gang. The wound looks to have been done in one, single, tremendous blow, and by something incredibly sharp.^^ From my estimate, he was standing there, at the lockers with that. The one with the torn off door was still open. Probably picking up some illicit dead drop. When whatever it was struck him. The force of the blow caused the lockers to become damaged as they are, and the blade of the attacker cleaved through him and is what damaged the tile wall and flooring. ^^Of course, this is all speculation.", "kirei_station_body_inspection_rival_2",
	 []
	 ],

[anyone|plyr, "kirei_station_body_inspection_rival_2", [
	 ], 
	 "You can see him smirk. You can feel he knows more than he is letting on. ^^'Speculation' my ass.", "kirei_station_body_inspection_2",
	 []
	 ],
	 
[anyone|plyr, "self_station_body_inspection", [
	 ], 
	 "You crouch by the body and pull back the shroud. ^You hardly recognize this mass as a man any longer, the vicera and gore look more like a display from a supermarket meat counter. ^The wound is tremendous, the amount of force required to do such a thing boggles the mind. ^'There's no way this was from a human.'", "self_station_body_inspection_1",
	 []
	 ],
	 
[anyone|plyr, "self_station_body_inspection_1", [
	 ], 
	 "What should I look at in more detail?", "self_station_body_inspection_2",
	 []
	 ],
	 
[anyone|plyr, "self_station_body_inspection_2", [
	 ], 
	 "[Check around the body]", "self_station_body_inspection_area",
	 []
	 ],
	 
[anyone|plyr, "self_station_body_inspection_2", [
	 ], 
	 "[Check the wound itself]", "self_station_body_inspection_wound",
	 []
	 ],
	 
[anyone|plyr, "self_station_body_inspection_2", [
	 ], 
	 "[Try to detect signs of residual mana]", "self_station_body_inspection_magic",
	 []
	 ],
[anyone|plyr, "self_station_body_inspection_2", [
	 ], 
	 "I've done all I can here.", "self_station_body_inspection_forfeit",
	 []
	 ],
	 
[anyone|plyr, "self_station_body_inspection_forfeit", [
	 ], 
	 "After pulling the covering back over the body, you realize this corpse is only the third person you've seen in this city. ^^That cannot simply be a coincidence.", "close_window",
	 [(assign, "$body_inspection_fail", 999),]
	 ],

[anyone|plyr, "self_station_body_inspection_area", [
	 ], 
	 "There isn't much to go on. ^^First you notice a large, single, deep gouge on the ground beneath the body. It is cleanly cut at least three inches deep into the concrete beneath the tiles, shattering the floor tiles a good ways around it. ^^The lockers behind the body look like they were crushed under a tremendous amount of weight, but, there are no scuff marks to suggest anything pressed against them. Well, any that you can make out through the blood and spinal fluid. ^^A single locker door was torn off, maybe the assailant forced it open? Or perhaps this man was grabbing something from it? He does have a satchel that doesn't seem to fit in with the rest of his aesthetic.", "self_station_body_inspection_area_2",
	 []
	 ],

[anyone|plyr, "self_station_body_inspection_area_2", [
	 ], 
	 "You turn back to the body to examine it further.", "self_station_body_inspection_2",
	 []
	 ],
	 
[anyone|plyr, "self_station_body_inspection_wound", [
	 ], 
	 "Even though the body is basically crushed and folded into itself, you can clearly see there is only a single wound. A singular neat cut from his right shoulder, down through to his pelvis. ^^In a different context you would have thought construction equipment fell from a building and he was pulvarized under the weight, this was clearly performed right here on this platform. ^^The only think you can think of, is something cleaved him in a single blow, and the immense force of the swing is what crushed his body around the strike.", "self_station_body_inspection_wound_2",
	 []
	 ],
	 
[anyone|plyr, "self_station_body_inspection_wound_2", [], 
	 "'Nothing could have imparted such force into a single strike though.' You say, trying to reassure yourself.", "self_station_body_inspection_2",
	 []
	 ],
	 
[anyone|plyr, "self_station_body_inspection_magic", [
	 ], 
	 "Trying to sense mana left in the world by the use of magecraft is like trying to navigate your way through the Arctic by the heat left in the snow from the mice underneath it. ^^Surprisingly, however, you notice a lot of residual mana. The prana the world is usually filled with is noticably diminished here, making it much easier to sense the bizarre amount of mana this wound holds. ^^It's typical for mage's to convert the world's prana into usable mana for their magecraft, but to have converted this much, and left it in a wound. It's almost unthinkable. ^^Maybe the attacker had a powerful, ancient mystic code?", "self_station_body_inspection_magic_2",
	 []
	 ],

[anyone|plyr, "self_station_body_inspection_magic_2", [
	 ], 
	 "You transfer your magical energy from sensing mana back to masking your presence, to keep other mages from being able to detect you.", "self_station_body_inspection_2",
	 []
	 ],
	 
[anyone|plyr, "alleyway", [], 
	 "Maybe I can cut through this alleyway to get to my hotel.", "close_window",
	 [
	 (assign, "$alleyway_noticed", 1),
	 ]
	 ],
	 
[anyone|plyr, "servant_assault", [], 
	 "The murderous intent emenating off this being is terrifying. ^^Every cell in my body is telling me to run away. ^^Could this be a Servant?", "servant_assault_1",
	 []
	 ],
	 
[anyone|plyr, "servant_assault_1", [], 
	 "I have to try to fight this thing. There's no other way.", "close_window",
	 [
	 (assign, "$walk_home", 1),
	 (get_player_agent_no, ":agent"),
	 (try_for_range, ":slot", 0, 4),
			(agent_get_item_slot, ":item", ":agent", ":slot"),

			(try_begin),
				(gt, ":item", 0),
				(agent_unequip_item, ":agent", ":item"),
			(try_end),

			(troop_get_inventory_slot, ":newitem", "trp_player", ":slot"),

			(try_begin),
				(gt, ":newitem", 0),
				(agent_equip_item, ":agent", ":newitem", ":slot"),
			(try_end),

		(try_end),
	 (store_conversation_agent, ":servant"),
	 (agent_set_is_alarmed, ":servant", 1),
	 (agent_clear_relations_with_agents, ":servant"),
	 (agent_add_relation_with_agent, ":servant", ":agent", -1),
	 (agent_add_relation_with_agent, ":agent", ":servant", -1),
	 (assign, "$servant_encounter", 1),
	 ]
	 ],
	 
[anyone|plyr, "servant_assault_1", [], 
	 "It's impossible to win, I have to try to get away.", "close_window",
	 [
	 (assign, "$walk_home", 1),
	 (get_player_agent_no, ":agent"),
	 (store_conversation_agent, ":servant"),
	 
	 (get_player_agent_no, ":agent"),
	 (agent_get_position, pos10, ":agent"),
	 (position_move_y, pos10, 125),
	 (position_rotate_z, pos10, 180),
	 
	
	 (agent_set_position, ":servant", pos10),
	 
	 (agent_set_is_alarmed, ":servant", 1),
	 (agent_clear_relations_with_agents, ":servant"),
	 
	 (agent_add_relation_with_agent, ":servant", ":agent", -1),
	 (agent_add_relation_with_agent, ":agent", ":servant", -1),
	 
	 (assign, "$servant_encounter", 2),
	 ]
	 ],
	 
]