SCRIPT IDEAS

ti_on_agent_hit               = -28.0 # Agent has been damaged with a weapon attack. Does not fire on client side in multiplayer missions.
    # trigger param 1 = damage inflicted agent_id
    # trigger param 2 = attacker agent_id
    # trigger param 3 = inflicted damage
    # trigger param 4 = hit bone
    # trigger param 5 = item_id of missile used to attack (if attack was with a ranged weapon)
    # reg0            = weapon item_id
    # pos0            = position of the hit area, rotation fields contain the direction of the blow
    # If (set_trigger_result) is used in the code with operation parameter equal or greater than zero, it will override the inflicted damage.

missle_blocking
	[(ti_on_agent_hit, 
		[
		 (set_fixed_point_multiplier, 100),
		 (store_trigger_param_1, ":hit_agent"),
         (store_trigger_param_3, ":inflicted_damage"),
		 (store_trigger_param, ":missile", 5),
		 (position_get_z, ":hit_height", pos0),
		 (agent_get_position, pos1, ":hit_agent"),
		 (position_get_z, ":hit_agent_height", pos1),
		 
		 (store_sub, ":local_hit_location", ":hit_height", ":hit_agent_height"),
		 
		 (neq, ":missile", 0),
		 (agent_is_in_parried_animation, ":hit_agent"),
		 (agent_get_action_dir, ":block_dir", ":hit_agent"), #1 is down, 3 is up
			(try_begin),
				(eq, ":block_dir", 3),
				(ge, ":local_hit_location", 130),
				(agent_set_animation, ":hit_agent", <anim_id>, [channel_no]),
				(set_trigger_result, 0),
			(else_try),
				(eq, ":block_dir", 1),
				(is_between, ":local_hit_location", 40, 130)
				(agent_set_animation, <agent_id>, <anim_id>, [channel_no]),
				(set_trigger_result, 0),
			(try_end),
			
			
			
			# if it is a missile, check if hit_agent is blocking
			# if the agent is blocking compare where the agent on the vertical axis
			# to where the hit_agent is standing. Subtract these two values to get the 
			# height of the hit locally on the agent.

		If it's between thighs and mid chest, and the agent was in a low block, ignore damage and fire low-block anim
		if it's between mid chest and head and agent is in a high block, ignore damage and fire high-block animation.
])].

LOOK AT THIS IDIOT THESE ARE COMMENTS THAT WEREN'T EVEN USED - - - - DUSTIN!!!

#(val_mul, reg8, -1),
				#(val_mul, reg9, -1),
				#(val_add, reg8, -45, 0),
				# (position_get_rotation_around_x, reg7, pos10),
				# (assign, ":additional_angle", 0),
				# (display_log_message, "@Missile Landing angle is ({reg7})!"),
				# (try_begin),
					# (is_between, reg7, 180, 360),
				# (store_mul, ":additional_angle", reg7, -1),
				# (else_try),
					# (is_between, reg7, 0, 90),
				# (store_mul, ":additional_angle", reg7, 1),
				# (try_end),
				
				# #####################################################
				# ##### Alright Dustin, Time for TRIANGLE MATH#########
				# ########## Take the Positions subtract their X, Y and Zs respectively
				# ###### Use thsese to determine the angles between agent and target
				# ##########add 45 to that to get the optimal angle to launch from
				# (position_get_x, ":agent_x", pos4),
				# (position_get_x, ":target_x", pos10),
				
				# (store_sub, ":x_length", ":agent_x", ":target_x"),
				# (val_abs, ":x_length"),
				# (assign, reg10, ":agent_x"),
				# (assign, reg11, ":target_x"),
				# (assign, reg12, ":x_length"),
				# (display_log_message, "@X Data: (Ag,Tar,Diff) - ({reg10},{reg11},{reg12})"),
				
				# (position_get_y, ":agent_y", pos4),
				# (position_get_y, ":target_y", pos10),
				
				# (store_sub, ":y_length", ":agent_y", ":target_y"),
				# (val_abs, ":y_length"),
				# (assign, reg10, ":agent_y"),
				# (assign, reg11, ":target_y"),
				# (assign, reg12, ":y_length"),
				# (display_log_message, "@y Data: (Ag,Tar,Diff) - ({reg10},{reg11},{reg12})"),
				
				#(val_abs, ":z_length"),
				 # (assign, reg10, ":agent_z"),
				 # (assign, reg11, ":target_z"),
				 # (assign, reg12, ":z_length"),
				 # (display_log_message, "@z Data: (Ag,Tar,Diff) - ({reg10},{reg11},{reg12})"),
				
				# arccos((":agent_x"*":target_x"+":agent_y"*":target_y"+":agent_z"*":target_z")/
						# sqrt(":agent_x"^2+":agent_y"^2+":agent_z"^2)*(":target_x"^2+":target_y"^2+":target_z"^2))
				# (store_mul, ":combined_x", ":agent_x", ":target_x"),
				# (store_mul, ":combined_y", ":agent_y", ":target_y"),
				# (store_mul, ":combined_z", ":agent_z", ":target_z"),
				
				# (store_mul, ":combined_num", ":combined_x", ":combined_y"),
				# (val_mul, ":combined_num", ":combined_z"),
				
				# (store_mul, ":agent_x_sq", ":agent_x", ":agent_x"),
				# (store_mul, ":agent_y_sq", ":agent_y", ":agent_y"),
				# (store_mul, ":agent_z_sq", ":agent_z", ":agent_z"),
				
				# (store_add, ":combined_agent_sq", ":agent_x_sq", ":agent_y_sq"),
				# (val_add, ":combined_agent_sq", ":agent_z_sq"),
				
				# (store_mul, ":target_x_sq", ":target_x", ":target_x"),
				# (store_mul, ":target_y_sq", ":target_y", ":target_y"),
				# (store_mul, ":target_z_sq", ":target_z", ":target_z"),
				
				# (store_add, ":combined_target_sq", ":target_x_sq", ":target_y_sq"),
				# (val_add, ":combined_target_sq", ":target_z_sq"),
				
				# (store_mul, ":combined_sq", ":combined_target_sq", ":combined_agent_sq"),
				# (store_sqrt, ":combined_sqrt", ":combined_sq"),
				
				# (val_div, ":combined_agent_sq", 10000),
				# (val_div, ":combined_sqrt", 10000),
				
				# (store_div, ":beast", ":combined_num", ":combined_sqrt"),
				
				# (store_acos, ":angle_new", ":beast"),
				
				
				# (assign, reg13, ":combined_agent_sq"),
				# (assign, reg14, ":combined_sqrt"),
				# (assign, reg15, ":angle_new"),
				
				# (display_log_message, "@{reg15} = ({reg13}/{reg14})"),		